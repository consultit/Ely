/**
 * \file deliveryNotificationManager.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_DELIVERYNOTIFICATIONMANAGER_H_
#define NETWORK_SOURCE_DELIVERYNOTIFICATIONMANAGER_H_

#include "network_includes.h"

#include "ackRange.h"
#include <deque>

#ifndef CPPPARSER
#include "support/StringUtils.h"
#endif //CPPPARSER

using std::deque;

/**
 * DeliveryNotificationManager class.
 */
class EXPCL_NETWORK DeliveryNotificationManager
{
PUBLISHED:

	INLINE DeliveryNotificationManager(bool inShouldSendAcks,
			bool inShouldProcessAcks);
	INLINE ~DeliveryNotificationManager();
	INLINE InFlightPacket* write_state(OutputMemoryBitStream& inOutputStream);
	INLINE bool read_and_process_state(InputMemoryBitStream& inInputStream);
	void process_timed_out_packets();
	INLINE uint32_t get_dropped_packet_count() const;
	INLINE uint32_t get_delivered_packet_count() const;
	INLINE uint32_t get_dispatched_packet_count() const;
	INLINE const deque<InFlightPacket>& get_in_flight_packets() const;
	INLINE void set_delay_before_ack_timeout(float tmo);
	INLINE float get_delay_before_ack_timeout() const;
	// Python Properties
	MAKE_PROPERTY(dropped_packet_count, get_dropped_packet_count);
	MAKE_PROPERTY(delivered_packet_count, get_delivered_packet_count);
	MAKE_PROPERTY(dispatched_packet_count, get_dispatched_packet_count);
	MAKE_PROPERTY(in_flight_packets, get_in_flight_packets);
	MAKE_PROPERTY(delay_before_ack_timeout, get_delay_before_ack_timeout, set_delay_before_ack_timeout);

public:
	DeliveryNotificationManager() = delete;

#ifndef CPPPARSER
private:
	InFlightPacket* do_write_sequence_number(OutputMemoryBitStream& inOutputStream);
	void do_write_ack_data(OutputMemoryBitStream& inOutputStream);
	//returns wether to drop the packet- if sequence number is too low!
	bool do_process_sequence_number(InputMemoryBitStream& inInputStream);
	void do_process_acks(InputMemoryBitStream& inInputStream);
	void do_add_pending_ack(PacketSequenceNumber inSequenceNumber);
	void do_handle_packet_delivery_failure(const InFlightPacket& inFlightPacket);
	void do_handle_packet_delivery_success(const InFlightPacket& inFlightPacket);
	PacketSequenceNumber mNextOutgoingSequenceNumber;
	PacketSequenceNumber mNextExpectedSequenceNumber;
	deque<InFlightPacket> mInFlightPackets;
	deque<AckRange> mPendingAcks;
	bool mShouldSendAcks;
	bool mShouldProcessAcks;
	uint32_t mDeliveredPacketCount;
	uint32_t mDroppedPacketCount;
	uint32_t mDispatchedPacketCount;
	float mDelayBeforeAckTimeout;
#endif //CPPPARSER
};

///inline
#include "deliveryNotificationManager.I"

#endif /* NETWORK_SOURCE_DELIVERYNOTIFICATIONMANAGER_H_ */
