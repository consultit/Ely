/**
 * \file networkWorld.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_NETWORKWORLD_H_
#define NETWORK_SOURCE_NETWORKWORLD_H_

#include "network_includes.h"

#include "networkObject.h"
#include "networkTools.h"

/**
 * NetworkWorld class.
 *
 * The world tracks all the live game objects. Fairly inefficient for now, but
 * not that much of a problem.
 */
class EXPCL_NETWORK NetworkWorld: public TypedReferenceCount, public Singleton<NetworkWorld>
{
PUBLISHED:

	INLINE NetworkWorld();
	INLINE virtual ~NetworkWorld();
	INLINE static NetworkWorld* get_global_ptr();
	void add_network_object(PT(NetworkObject) inNetworkObject);
	void remove_network_object(PT(NetworkObject) inNetworkObject);
	void update();
	INLINE ValueList_NetworkObjectPtr get_network_objects() const;
	// Python Properties
	MAKE_PROPERTY(network_objects, get_network_objects);

#ifndef CPPPARSER
private:
	pvector<PT(NetworkObject)> mNetworkObjects;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(NetworkWorld,TypedReferenceCount)
};

///inline
#include "networkWorld.I"

#endif /* NETWORK_SOURCE_NETWORKWORLD_H_ */
