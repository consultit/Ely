/**
 * \file replicationCommand.h
 *
 * \date 2018-07-24
 * \author consultit
 */

#ifndef NETWORK_SOURCE_REPLICATIONCOMMAND_H_
#define NETWORK_SOURCE_REPLICATIONCOMMAND_H_

#include "network_includes.h"

#include "memoryBitStream.h"

enum ReplicationAction
{
	RA_Create, RA_Update, RA_Destroy, RA_RPC, RA_MAX
};

class ReplicationManagerTransmissionData;

/**
 * ReplicationCommand class.
 */
struct EXPCL_NETWORK ReplicationCommand
{
PUBLISHED:

	INLINE ReplicationCommand();
	INLINE ReplicationCommand(uint32_t inInitialDirtyState);
	//if the create is ack'd, we can demote to just an update...
	INLINE void handle_create_ackd();
	INLINE void add_dirty_state(uint32_t inState);
	INLINE void set_destroy();
	INLINE bool get_has_dirty_state() const;
	INLINE void set_action(ReplicationAction inAction);
	INLINE ReplicationAction get_action() const;
	INLINE uint32_t get_dirty_state() const;
	INLINE void clear_dirty_state(uint32_t inStateToClear);
	//write is not const because we actually clear the dirty state after writing it....
	INLINE void write(OutputMemoryBitStream& inStream, int inNetworkId,
			ReplicationManagerTransmissionData* ioTransactionData);
	INLINE void read(InputMemoryBitStream& inStream, int inNetworkId);
	// Python Properties
	MAKE_PROPERTY(has_dirty_state, get_has_dirty_state);
	MAKE_PROPERTY(action, get_action, set_action);
	MAKE_PROPERTY(dirty_state, get_dirty_state);

#ifndef CPPPARSER
private:
	uint32_t mDirtyState;
	ReplicationAction mAction;
#endif //CPPPARSER
};

///inline
#include "replicationCommand.I"

#endif /* NETWORK_SOURCE_REPLICATIONCOMMAND_H_ */
