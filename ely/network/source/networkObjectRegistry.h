/**
 * \file networkObjectRegistry.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_NETWORKOBJECTREGISTRY_H_
#define NETWORK_SOURCE_NETWORKOBJECTREGISTRY_H_

#include "network_includes.h"

#include "networkObject.h"
#include <unordered_map>

using std::unordered_map;

typedef PT(NetworkObject) (*NetworkObjectCreationFunc)();

/**
 * NetworkObjectRegistry class.
 */
class EXPCL_NETWORK NetworkObjectRegistry: public TypedReferenceCount, public Singleton<NetworkObjectRegistry>
{
PUBLISHED:

	NetworkObjectRegistry();
	~NetworkObjectRegistry();
	INLINE static NetworkObjectRegistry* get_global_ptr();
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	void register_creation_function(PyObject* name, PyObject* func);
#else
	void register_creation_function(uint32_t name, NetworkObjectCreationFunc func);
#endif // PYTHON_BUILD
	PT(NetworkObject) create_network_object(uint32_t inFourCCName);
	// Python Properties

#ifndef CPPPARSER
private:
#if defined(PYTHON_BUILD)
	unordered_map<uint32_t, PyObject *> mNameToNetworkObjectCreationFunctionMap;
#else
	unordered_map<uint32_t, NetworkObjectCreationFunc> mNameToNetworkObjectCreationFunctionMap;
#endif // PYTHON_BUILD
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(NetworkObjectRegistry,TypedReferenceCount)
};

///inline
#include "networkObjectRegistry.I"

#endif /* NETWORK_SOURCE_NETWORKOBJECTREGISTRY_H_ */
