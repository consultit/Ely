/**
 * \file tcpSocket.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "tcpSocket.h"
#include "socketUtil.h"

///TCPSocket definitions

/**
 *
 */
int TCPSocket::connect_at(const SocketAddress& inAddress)
{
	int result = connect(mSocket, &inAddress.mSockAddr, inAddress.get_size());
	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::Connect");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	return NO_ERROR;
}

/**
 *
 */
int TCPSocket::listen_at(int inBackLog)
{
	int result = listen(mSocket, inBackLog);
	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::Listen");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	return NO_ERROR;
}

/**
 *
 */
PT(TCPSocket) TCPSocket::accept_at(SocketAddress& inFromAddress)
{
	socklen_t length = inFromAddress.get_size();
	SOCKET newSocket = accept(mSocket, &inFromAddress.mSockAddr, &length);

	if (newSocket != INVALID_SOCKET)
	{
		return PT(TCPSocket)(new TCPSocket(newSocket));
	}
	else
	{
		string msg("TCPSocket::Accept");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::report_error(msg.c_str());
		return nullptr;
	}
}

/**
 *
 */
int32_t TCPSocket::send_to(const void* inData, size_t inLen)
{
	int bytesSentCount = send(mSocket,
#ifdef _WIN32
			static_cast<const char*>(inData),
#else
			inData,
#endif
			inLen, 0);
	if (bytesSentCount == SOCKET_ERROR)
	{
		string msg("TCPSocket::Send");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	return bytesSentCount;
}

/**
 *
 */
int32_t TCPSocket::receive_from(void* inData, size_t inLen)
{
	int bytesReceivedCount = recv(mSocket,
#ifdef _WIN32
			static_cast<char*>(inData),
#else
			inData,
#endif
			inLen, 0);
	if (bytesReceivedCount == SOCKET_ERROR)
	{
		string msg("TCPSocket::Receive");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	return bytesReceivedCount;
}

/**
 *
 */
int TCPSocket::shutdown_connection(int how = SocketUtil::BOTH)
{
	int result = shutdown(mSocket, how);

	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::Shutdown");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}

	return NO_ERROR;
}

/**
 *
 */
int TCPSocket::bind_at(const SocketAddress& inBindAddress)
{
	int result = bind(mSocket, &inBindAddress.mSockAddr,
			inBindAddress.get_size());
	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::Bind");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}

	return NO_ERROR;
}

/**
 *
 */
int TCPSocket::get_connected_peer(SocketAddress& address) const
{
#ifdef _WIN32
	int addrlen;
#else
	socklen_t addrlen = sizeof(address.mSockAddr);
#endif
	int result = getpeername(mSocket, &address.mSockAddr, &addrlen);

	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::GetConnectedPeer");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	else
	{
		return result;
	}
}

/**
 *
 */
PT(SocketAddress) TCPSocket::get_connected_peer() const
{
	PT(SocketAddress) address(new SocketAddress());
	int result = get_connected_peer(*address);
	if (result < 0)
	{
		return nullptr;
	}
	else
	{
		return address;
	}
}

/**
 *
 */
int TCPSocket::set_socket_opt(int level, int optname, const char *optval,
		int optlen)
{
	int result = setsockopt(mSocket, level, optname,
#ifdef _WIN32
			optval, optlen
#else
			(const void*) optval, (socklen_t) optlen
#endif
			);

	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::SetSocketOpt");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	else
	{
		return result;
	}
}

/**
 *
 */
int TCPSocket::get_socket_opt(int level, int optname, char *optval, int *optlen) const
{
	int result = getsockopt(mSocket, level, optname,
#ifdef _WIN32
			optval, optlen
#else
			(void*) optval, (socklen_t*) optlen
#endif
			);

	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::GetSocketOpt");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	else
	{
		return result;
	}
}

TYPED_OBJECT_API_DEF(TCPSocket)
