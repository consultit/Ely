/**
 * \file inputManager.h
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_INPUTMANAGER_H_
#define NETWORK_SOURCE_INPUTMANAGER_H_

#include "network_includes.h"

#include "moveList.h"

/**
 * InputManager class.
 */
class EXPCL_NETWORK InputManager: public TypedReferenceCount, public Singleton<InputManager>
{
PUBLISHED:

	INLINE InputManager();
	INLINE virtual ~InputManager();
	INLINE static InputManager* get_global_ptr();
	INLINE InputState* get_input_state();
	INLINE MoveList* get_move_list();
	INLINE Move* get_pending_move();
	INLINE void clear_pending_move();
	INLINE const Move* get_and_clear_pending_move();
	INLINE void set_time_between_input_samples(float tmo);
	INLINE float get_time_between_input_samples() const;
	void update();
	// Python Properties
	MAKE_PROPERTY(input_state, get_input_state);
	MAKE_PROPERTY(move_list, get_move_list);
	MAKE_PROPERTY(pending_move, get_pending_move);
	MAKE_PROPERTY(time_between_input_samples, get_time_between_input_samples, set_time_between_input_samples);

#ifndef CPPPARSER
private:
	InputState* mCurrentState;
	bool do_is_time_to_sample_input();
	const Move& do_sample_input_as_move();
	MoveList mMoveList;
	float mNextTimeToSampleInput;
	const Move* mPendingMove;
	float mTimeBetweenInputSamples;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(InputManager,TypedReferenceCount)
};

///inline
#include "inputManager.I"

#endif /* NETWORK_SOURCE_INPUTMANAGER_H_ */
