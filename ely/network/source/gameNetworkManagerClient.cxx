/**
 * \file networkManagerClient.cxx
 *
 * \date 2018-07-22
 * \author consultit
 */

#include "gameNetworkManagerClient.h"
#include "inputManager.h"

#include "support/StringUtils.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
extern Dtool_PyTypedObject Dtool_InputMemoryBitStream;
#endif //PYTHON_BUILD

///GameNetworkManagerClient definitions

/**
 *
 */
GameNetworkManagerClient::GameNetworkManagerClient() :
		mState(NCS_Uninitialized), mDeliveryNotificationManager(true, false), mLastRoundTripTime(
				0.f), mTimeOfLastHello(0.f), mPlayerId(0), mTimeOfLastInputPacket(
				0.f), mLastMoveProcessedByServerTimestamp(0.f), mAvgRoundTripTime(
				5.f), mDelayBeforeAckTimeout(0.5), mTimeBetweenHellos(1.0), mTimeBetweenInputPackets(
				0.033)
{
	mHandleWelcomePacketClbk = nullptr;
	mHandleStatePacketClbk = nullptr;
}

/**
 *
 */
GameNetworkManagerClient::~GameNetworkManagerClient()
{
#ifdef PYTHON_BUILD
	//Python callback
	Py_XDECREF(mHandleWelcomePacketClbk);
	Py_XDECREF(mHandleStatePacketClbk);
#endif //PYTHON_BUILD
	mHandleWelcomePacketClbk = nullptr;
	mHandleStatePacketClbk = nullptr;
}

/**
 *
 */
bool GameNetworkManagerClient::init(const SocketAddress& inServerAddress,
		const string& inName)
{
	if (GameNetworkManager::init(0))
	{
		mServerAddress = inServerAddress;
		mState = NCS_SayingHello;
		mTimeOfLastHello = 0.f;
		mName = inName;

		mAvgRoundTripTime = WeightedTimedMovingAverage(1.f);
		return true;
	}
	return false;
}

/**
 *
 */
void GameNetworkManagerClient::process_packet(InputMemoryBitStream& inInputStream,
		const SocketAddress& inFromAddress)
{
	uint32_t packetType;
	inInputStream.read(packetType);
	switch (packetType)
	{
	case kWelcomeCC:
		do_handle_welcome_packet(inInputStream);
		break;
	case kStateCC:
		if (mDeliveryNotificationManager.read_and_process_state(inInputStream))
		{
			do_handle_state_packet(inInputStream);
		}
		break;
	}
}

/**
 *
 */
void GameNetworkManagerClient::send_outgoing_packets()
{
	switch (mState)
	{
	case NCS_SayingHello:
		do_update_saying_hello();
		break;
	case NCS_Welcomed:
		do_update_sending_input_packet();
		break;
	case NCS_Uninitialized:
	default:
		break;
	}
}

#ifdef PYTHON_BUILD
/**
 *
 */
void GameNetworkManagerClient::set_handle_welcome_packet_callback(PyObject* clbk)
{
	PY_SET_FUNC_BODY(mHandleWelcomePacketClbk, clbk,
			"GameNetworkManagerClient.set_handle_welcome_packet_callback()");
}

/**
 *
 */
void GameNetworkManagerClient::set_handle_state_packet_callback(PyObject* clbk)
{
	PY_SET_FUNC_BODY(mHandleStatePacketClbk, clbk,
			"GameNetworkManagerClient.set_handle_state_packet_callback()");
}
#else
/**
 *
 */
void GameNetworkManagerClient::set_handle_welcome_packet_callback(
		HandleWelcomePacketCallback clbk)
{
	mHandleWelcomePacketClbk = clbk;
}

/**
 *
 */
void GameNetworkManagerClient::set_handle_state_packet_callback(
		HandleStatePacketCallback clbk)
{
	mHandleStatePacketClbk = clbk;
}
#endif //PYTHON_BUILD

/**
 *
 */
void GameNetworkManagerClient::do_update_saying_hello()
{
	float time = Timing::get_global_ptr()->get_timef();

	if (time > mTimeOfLastHello + mTimeBetweenHellos)
	{
		do_send_hello_packet();
		mTimeOfLastHello = time;
	}
}

/**
 *
 */
void GameNetworkManagerClient::do_send_hello_packet()
{
	OutputMemoryBitStream helloPacket;

	helloPacket.write(kHelloCC);
	helloPacket.write(mName);

	send_packet(helloPacket, mServerAddress);
}

/**
 *
 */
void GameNetworkManagerClient::do_handle_welcome_packet(
		InputMemoryBitStream& inInputStream)
{
	if (mState == NCS_SayingHello)
	{
		//if we got a player id, we've been welcomed!
		int playerId;
		inInputStream.read(playerId);
		mPlayerId = playerId;
		mState = NCS_Welcomed;
		LOG("'%s' was welcomed on client as player %d", mName.c_str(),
				mPlayerId)
		if (mHandleWelcomePacketClbk)
		{
#ifdef PYTHON_BUILD
			PyObject *inInputStreamObj, *result;
			// create the argument object
			inInputStreamObj = DTool_CreatePyInstance(&inInputStream,
					Dtool_InputMemoryBitStream, false, false);
			// call the callback
			result = PyObject_CallFunctionObjArgs(mHandleWelcomePacketClbk,
					inInputStreamObj, nullptr);
			Py_DECREF(inInputStreamObj);
			if (!result)
			{
				string errStr =
						string(
								"GameNetworkManagerClient::do_handle_welcome_packet(): Error calling '")
								+ get_py_object_name(mHandleWelcomePacketClbk)
								+ string("' callback function - ")
								+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
				PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
				PRINT_ERR(errStr);
#endif
				return;
			}
			Py_DECREF(result);
#else
			mHandleWelcomePacketClbk(inInputStream);
#endif //PYTHON_BUILD
		}
	}
}

/**
 *
 */
void GameNetworkManagerClient::do_handle_state_packet(
		InputMemoryBitStream& inInputStream)
{
	if (mState == NCS_Welcomed)
	{
		do_read_last_move_processed_on_server_timestamp(inInputStream);

		if (mHandleStatePacketClbk)
		{
#ifdef PYTHON_BUILD
			PyObject *inInputStreamObj,*result;
			// create the argument object
			inInputStreamObj = DTool_CreatePyInstance(&inInputStream,
					Dtool_InputMemoryBitStream, false, false);
			// call the callback
			result = PyObject_CallFunctionObjArgs(mHandleStatePacketClbk,
					inInputStreamObj, nullptr);
			Py_DECREF(inInputStreamObj);
			if (!result)
			{
				string errStr =
						string(
								"GameNetworkManagerClient::do_handle_state_packet(): Error calling '")
								+ get_py_object_name(mHandleStatePacketClbk)
								+ string("' callback function - ")
								+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
				PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
				PRINT_ERR(errStr);
#endif
				return;
			}
			Py_DECREF(result);
#else
			mHandleStatePacketClbk(inInputStream);
#endif //PYTHON_BUILD
		}

		//tell the replication manager to handle the rest...
		mReplicationManagerClient.read(inInputStream);
	}
}

/**
 *
 */
void GameNetworkManagerClient::do_read_last_move_processed_on_server_timestamp(
		InputMemoryBitStream& inInputStream)
{
	bool isTimestampDirty;
	inInputStream.read(isTimestampDirty);
	if (isTimestampDirty)
	{
		inInputStream.read(mLastMoveProcessedByServerTimestamp);

		float rtt = Timing::get_global_ptr()->get_frame_start_time()
				- mLastMoveProcessedByServerTimestamp;
		mLastRoundTripTime = rtt;
		mAvgRoundTripTime.update(rtt);

		InputManager::get_global_ptr()->get_move_list()->removed_processed_moves(
				mLastMoveProcessedByServerTimestamp);

	}
}

/**
 *
 */
void GameNetworkManagerClient::do_destroy_network_objects_in_map(
		const IntToNetworkObjectMap& inObjectsToDestroy)
{
	for (auto& pair : inObjectsToDestroy)
	{
		pair.second->set_does_want_to_die(true);
		//and remove from our map!
		mNetworkIdToNetworkObjectMap.erase(pair.first);
	}

}

/**
 *
 */
void GameNetworkManagerClient::do_update_sending_input_packet()
{
	float time = Timing::get_global_ptr()->get_timef();

	if (time > mTimeOfLastInputPacket + mTimeBetweenInputPackets)
	{
		do_send_input_packet();
		mTimeOfLastInputPacket = time;
	}
}

/**
 *
 */
void GameNetworkManagerClient::do_send_input_packet()
{
	//only send if there's any input to sent!
	const MoveList& moveList = *InputManager::get_global_ptr()->get_move_list();

	if (moveList.get_has_moves())
	{
		OutputMemoryBitStream inputPacket;
		inputPacket.write(kInputCC);

		mDeliveryNotificationManager.write_state(inputPacket);

		//eventually write the 3 latest moves so they have three chances to get through...
		int moveCount = moveList.get_move_count();
		int firstMoveIndex = moveCount - 3;
		if (firstMoveIndex < 3)
		{
			firstMoveIndex = 0;
		}
		auto move = moveList.begin() + firstMoveIndex;

		//only need two bits to write the move count, because it's 0, 1, 2 or 3
		inputPacket.write(moveCount - firstMoveIndex, 2);

		for (; firstMoveIndex < moveCount; ++firstMoveIndex, ++move)
		{
			///would be nice to optimize the time stamp...
			move->write(inputPacket);
		}

		send_packet(inputPacket, mServerAddress);
	}
}

TYPED_OBJECT_API_DEF(GameNetworkManagerClient)
