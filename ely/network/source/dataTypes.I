/**
 * \file dataTypes.I
 *
 * \date 2018-08-29
 * \author consultit
 */

#ifndef NETWORK_SOURCE_DATATYPES_I_
#define NETWORK_SOURCE_DATATYPES_I_

///Data types inline definitions
template<typename basetype>
INLINE DataType<basetype>::DataType(): mVar()
{
}
template<typename basetype>
INLINE DataType<basetype>::DataType(const basetype& var): mVar(var)
{
}
template<typename basetype>
INLINE DataType<basetype>::~DataType()
{
}
template<typename basetype>
INLINE DataType<basetype>& DataType<basetype>::operator=(const DataType& var)
{
	mVar = var.mVar;
	return *this;
}
template<typename basetype>
INLINE DataType<basetype>& DataType<basetype>::operator=(const basetype& var)
{
	mVar = var;
	return *this;
}
template<typename basetype>
INLINE bool DataType<basetype>::operator==(const DataType& oth) const
{
	return mVar == oth.mVar;
}
template<typename basetype>
INLINE bool DataType<basetype>::operator==(const basetype& oth) const
{
	return mVar == oth;
}
template<typename basetype>
INLINE const basetype& DataType<basetype>::get_value() const
{
	return mVar;
}
template<typename basetype>
inline DataType<basetype>::operator basetype&()
{
	return mVar;
}
template<typename basetype>
inline basetype& DataType<basetype>::get_value()
{
	return mVar;
}

#endif /* NETWORK_SOURCE_DATATYPES_I_ */
