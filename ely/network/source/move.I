/**
 * \file move.I
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_MOVE_I_
#define NETWORK_SOURCE_MOVE_I_

///Move inline definitions

/**
 *
 */
INLINE Move::Move() :
		mDeltaTime(0.f), mTimestamp(0.f), mInputState(
				*InputStateFactory::get_global_ptr()->get_input_state())
{
}

/**
 *
 */
INLINE Move::Move(const InputState& inInputState, float inTimestamp,
		float inDeltaTime) :
		mTimestamp(inTimestamp), mDeltaTime(inDeltaTime), mInputState(
				inInputState)
{
}

/**
 *
 */
INLINE const InputState& Move::get_input_state() const
{
	return mInputState;
}

/**
 *
 */
INLINE InputState& Move::get_input_state()
{
	return mInputState;
}

/**
 *
 */
INLINE float Move::get_timestamp() const
{
	return mTimestamp;
}

/**
 *
 */
INLINE float Move::get_delta_time() const
{
	return mDeltaTime;
}

#endif /* NETWORK_SOURCE_MOVE_I_ */
