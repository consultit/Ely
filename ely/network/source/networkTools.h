/**
 * \file networkTools.h
 *
 * \date 2018-08-14
 * \author consultit
 */

#ifndef NETWORK_SOURCE_NETWORKTOOLS_H_
#define NETWORK_SOURCE_NETWORKTOOLS_H_

#include "tools_includes.h"
#include "clientProxy.h"
#include "networkObject.h"
#include "tcpSocket.h"
#include "bitMask.h"

///NetBitMask
class EXPCL_NETWORK NetBitMask: public BitMask32
{
PUBLISHED:
	INLINE NetBitMask(const BitMask32& mask);

public:
	static TypeHandle get_class_type()
	{
		return _type_handle;
	}
	static void init_type()
	{
		register_type(_type_handle, "NetBitMask");
	}

#ifndef CPPPARSER
private:
	static TypeHandle _type_handle;
#endif //CPPPARSER
};

///network valueLists' declarations

//NETWORK VALUELISTS: templated types preliminary defines
#define ValueList_ClientProxyPtr_base ValueList<PT(ClientProxy)>
#define ValueList_NetworkObjectPtr_base ValueList<PT(NetworkObject)>
#define ValueList_TCPSocketPtr_base ValueList<PT(TCPSocket)>

//NETWORK VALUELISTS: types to be expanded
#define NETWORKVALUELIST_EXPANDABLE \
EXPAND(ValueList_ClientProxyPtr, ValueList_ClientProxyPtr_base, PT(ClientProxy))\
EXPAND(ValueList_NetworkObjectPtr, ValueList_NetworkObjectPtr_base, PT(NetworkObject))\
EXPAND(ValueList_TCPSocketPtr, ValueList_TCPSocketPtr_base, PT(TCPSocket))\

///NETWORK VALUELISTS' (EXPANDED) DECLARATIONS
#define PAIR_EXPANDABLE_PLACEHOLDER
#define VALUELIST_EXPANDABLE_PLACEHOLDER NETWORKVALUELIST_EXPANDABLE

#include "../../libtools/source/support/pairs_valueLists_src.h"

#undef PAIR_EXPANDABLE_PLACEHOLDER
#undef VALUELIST_EXPANDABLE_PLACEHOLDER

///inline
#include "networkTools.I"

#endif /* NETWORK_SOURCE_NETWORKTOOLS_H_ */
