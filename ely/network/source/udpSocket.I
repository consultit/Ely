/**
 * \file udpSocket.I
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_UDPSOCKET_I_
#define NETWORK_SOURCE_UDPSOCKET_I_

///UDPSocket inline definitions

/**
 *
 */
INLINE UDPSocket::~UDPSocket()
{
#if _WIN32
	closesocket( mSocket );
#else
	close(mSocket);
#endif
}

/**
 *
 */
inline UDPSocket::UDPSocket(SOCKET inSocket) :
		mSocket(inSocket)
{
}

#endif /* NETWORK_SOURCE_UDPSOCKET_I_ */
