/**
 * \file dataTypes.cxx
 *
 * \date 2018-08-29
 * \author consultit
 */

#include "dataTypes.h"

template class DataType<int8_t>;
template class DataType<int16_t>;
template class DataType<int32_t>;
template class DataType<int64_t>;
template class DataType<uint8_t>;
template class DataType<uint16_t>;
template class DataType<uint32_t>;
template class DataType<uint64_t>;
template class DataType<float>;
template class DataType<double>;
template class DataType<bool>;
template class DataType<string>;
