#ifndef NETWORK_SOURCE_CONFIG_NETWORK_H_
#define NETWORK_SOURCE_CONFIG_NETWORK_H_

#pragma once

#include "networksymbols.h"
#include "pandabase.h"
#include "notifyCategoryProxy.h"
#include "configVariableDouble.h"
#include "configVariableString.h"
#include "configVariableInt.h"


NotifyCategoryDecl(p3network, EXPCL_NETWORK, EXPTP_NETWORK);

extern void init_libp3network();

#endif //NETWORK_SOURCE_CONFIG_NETWORK_H_
