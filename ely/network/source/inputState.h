/**
 * \file inputState.h
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_INPUTSTATE_H_
#define NETWORK_SOURCE_INPUTSTATE_H_

#include "network_includes.h"

#include "memoryBitStream.h"
#include "dataVariables.h"

/**
 * InputState class.
 */
class EXPCL_NETWORK InputState
{
PUBLISHED:

	INLINE InputState();
	INLINE explicit InputState(const InputState& right);
	INLINE ~InputState();
	INLINE bool write(OutputMemoryBitStream& inOutputStream) const;
	INLINE bool read(InputMemoryBitStream& inInputStream);
	INLINE DataVariables& get_members();
	INLINE const DataVariables& get_members() const;
	// Python Properties
	MAKE_PROPERTY(members, get_members);

public:
	InputState(InputState&& right) = delete;
	InputState& operator=(const InputState& right) = delete;
	InputState& operator=(InputState&& right) = delete;

#ifndef CPPPARSER
private:
	DataVariables mMembers;
#endif //CPPPARSER
};

/**
 * InputStateFactory singleton class.
 */
class EXPCL_NETWORK InputStateFactory: public ReferenceCount, public Singleton<InputStateFactory>
{
PUBLISHED:

	INLINE InputStateFactory();
	INLINE virtual ~InputStateFactory();
	INLINE static InputStateFactory* get_global_ptr();
	INLINE InputState* get_input_state();

#ifndef CPPPARSER
private:
	InputState mInputState;
#endif //CPPPARSER
};

///inline
#include "inputState.I"

#endif /* NETWORK_SOURCE_INPUTSTATE_H_ */
