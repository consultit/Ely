/**
 * \file clientProxy.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_CLIENTPROXY_H_
#define NETWORK_SOURCE_CLIENTPROXY_H_

#include "network_includes.h"

#include "socketAddress.h"
#include "moveList.h"
#include "deliveryNotificationManager.h"
#include "replicationManagerServer.h"
#include "typedReferenceCount.h"

class GameNetworkManagerServer;
typedef void (*RespawnClientObjectsCallback)(int);

/**
 * ClientProxy class.
 */
class EXPCL_NETWORK ClientProxy: public TypedReferenceCount
{
PUBLISHED:

	ClientProxy(const SocketAddress& inSocketAddress, const string& inName,
			int inPlayerId);
	virtual ~ClientProxy();
	INLINE const SocketAddress& get_socket_address() const;
	INLINE int get_player_id() const;
	INLINE const string& get_name() const;
	INLINE void update_last_packet_time();
	INLINE float get_last_packet_from_client_time() const;
	INLINE const MoveList& get_unprocessed_move_list() const;
	INLINE MoveList& get_unprocessed_move_list();
	INLINE void set_last_move_timestamp_dirty(bool inIsDirty);
	INLINE bool get_last_move_timestamp_dirty() const;
	INLINE void compute_time_to_respawn();
	void respawn_objects_if_necessary();
	INLINE DeliveryNotificationManager& get_delivery_notification_manager();
	INLINE ReplicationManagerServer& get_replication_manager_server();
	// Python Properties
	MAKE_PROPERTY(socket_address, get_socket_address);
	MAKE_PROPERTY(player_id, get_player_id);
	MAKE_PROPERTY(name, get_name);
	MAKE_PROPERTY(last_packet_from_client_time, get_last_packet_from_client_time);
	MAKE_PROPERTY(unprocessed_move_list, get_unprocessed_move_list);
	MAKE_PROPERTY(last_move_timestamp_dirty, get_last_move_timestamp_dirty, set_last_move_timestamp_dirty);
	MAKE_PROPERTY(delivery_notification_manager, get_delivery_notification_manager);
	MAKE_PROPERTY(replication_manager_server, get_replication_manager_server);

public:
	ClientProxy() = delete;

#ifndef CPPPARSER
private:
	friend GameNetworkManagerServer;

	DeliveryNotificationManager mDeliveryNotificationManager;
	ReplicationManagerServer mReplicationManagerServer;
	SocketAddress mSocketAddress;
	string mName;
	int mPlayerId;
	float mLastPacketFromClientTime;
	float mTimeToRespawn;
	MoveList mUnprocessedMoveList;
	bool mIsLastMoveTimestampDirty;
	float mRespawnClientObjectsDelay;
#if defined(PYTHON_BUILD)
	PyObject *mRespawnClientObjectsClbk;
#else
	RespawnClientObjectsCallback mRespawnClientObjectsClbk;
#endif // PYTHON_BUILD
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(ClientProxy,TypedReferenceCount)
};

///inline
#include "clientProxy.I"

#endif /* NETWORK_SOURCE_CLIENTPROXY_H_ */
