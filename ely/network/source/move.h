/**
 * \file move.h
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_MOVE_H_
#define NETWORK_SOURCE_MOVE_H_

#include "network_includes.h"

#include "inputState.h"
#include "memoryBitStream.h"

/**
 * Move class.
 */
class EXPCL_NETWORK Move
{
PUBLISHED:

	INLINE Move();
	INLINE Move(const InputState& inInputState, float inTimestamp, float inDeltaTime);
	INLINE const InputState& get_input_state() const;
	INLINE InputState& get_input_state();
	INLINE float get_timestamp() const;
	INLINE float get_delta_time() const;
	bool write(OutputMemoryBitStream& inOutputStream) const;
	bool read(InputMemoryBitStream& inInputStream);
	// Python Properties
	MAKE_PROPERTY(input_state, get_input_state);
	MAKE_PROPERTY(timestamp, get_timestamp);
	MAKE_PROPERTY(delta_time, get_delta_time);

public:
	Move& operator=(const Move& right) = delete;

#ifndef CPPPARSER
private:
	InputState mInputState;
	float mTimestamp;
	float mDeltaTime;
#endif //CPPPARSER
};

///inline
#include "move.I"

#endif /* NETWORK_SOURCE_MOVE_H_ */
