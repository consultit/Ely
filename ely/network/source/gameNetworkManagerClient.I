/**
 * \file networkManagerClient.I
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_GAMENETWORKMANGERCLIENT_I_
#define NETWORK_SOURCE_GAMENETWORKMANGERCLIENT_I_

///GameNetworkManagerClient inline definitions

/**
 *
 */
INLINE GameNetworkManagerClient* GameNetworkManagerClient::get_global_ptr()
{
	return Singleton<GameNetworkManagerClient>::GetSingletonPtr();
}

/**
 *
 */
INLINE const WeightedTimedMovingAverage& GameNetworkManagerClient::get_avg_round_trip_time() const
{
	return mAvgRoundTripTime;
}

/**
 *
 */
INLINE float GameNetworkManagerClient::get_round_trip_time() const
{
	return mAvgRoundTripTime.get_value();
}

/**
 *
 */
INLINE int GameNetworkManagerClient::get_player_id() const
{
	return mPlayerId;
}

/**
 *
 */
INLINE float GameNetworkManagerClient::get_last_move_processed_by_server_timestamp() const
{
	return mLastMoveProcessedByServerTimestamp;
}

/**
 *
 */
INLINE void GameNetworkManagerClient::set_delay_before_ack_timeout(float tmo)
{
	mDelayBeforeAckTimeout = tmo;
}

/**
 *
 */
INLINE float GameNetworkManagerClient::get_delay_before_ack_timeout() const
{
	return mDelayBeforeAckTimeout;
}

/**
 *
 */
INLINE void GameNetworkManagerClient::set_time_between_hellos(float tmo)
{
	mTimeBetweenHellos = tmo;
}

/**
 *
 */
INLINE float GameNetworkManagerClient::get_time_between_hellos() const
{
	return mTimeBetweenHellos;
}

/**
 *
 */
INLINE void GameNetworkManagerClient::set_time_between_input_packets(float tmo)
{
	mTimeBetweenInputPackets = tmo;
}

/**
 *
 */
INLINE float GameNetworkManagerClient::get_time_between_input_packets() const
{
	return mTimeBetweenInputPackets;
}

#endif /* NETWORK_SOURCE_GAMENETWORKMANGERCLIENT_I_ */
