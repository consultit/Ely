/**
 * \file socketUtil.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "socketUtil.h"

#include "support/StringUtils.h"

///SocketUtil definitions

/**
 *
 */
bool SocketUtil::static_init()
{
#if _WIN32
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if ( iResult != NO_ERROR )
	{
		report_error ("Starting Up");
		return false;
	}
#endif
	return true;
}

/**
 *
 */
void SocketUtil::clean_up()
{
#if _WIN32
	WSACleanup();
#endif
}

/**
 *
 */
void SocketUtil::report_error(const char* inOperationDesc)
{
#if _WIN32
	LPVOID lpMsgBuf;
	DWORD errorNum = get_last_error();

	FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			errorNum,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR) &lpMsgBuf,
			0, NULL );

	LOG( "Error %s: %d- %s", inOperationDesc, errorNum, lpMsgBuf )
#else
	LOG("Error: %hs", inOperationDesc)
#endif
}

/**
 *
 */
int SocketUtil::get_last_error()
{
#if _WIN32
	return WSAGetLastError();
#else
	return errno;
#endif

}

/**
 *
 */
PT(UDPSocket) SocketUtil::create_udp_socket(SocketAddressFamily inFamily)
{
	SOCKET s = socket(inFamily, SOCK_DGRAM, IPPROTO_UDP);

	if (s != INVALID_SOCKET)
	{
		return PT(UDPSocket)(new UDPSocket(s));
	}
	else
	{
		string msg("SocketUtil::CreateUDPSocket");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		report_error(msg.c_str());
		return nullptr;
	}
}

/**
 *
 */
PT(TCPSocket) SocketUtil::create_tcp_socket(SocketAddressFamily inFamily)
{
	SOCKET s = socket(inFamily, SOCK_STREAM, IPPROTO_TCP);

	if (s != INVALID_SOCKET)
	{
		return PT(TCPSocket)(new TCPSocket(s));
	}
	else
	{
		string msg("SocketUtil::CreateTCPSocket");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		report_error(msg.c_str());
		return nullptr;
	}
}

/**
 *
 */
int SocketUtil::select_sockets(const ValueList_TCPSocketPtr* inReadSet,
		ValueList_TCPSocketPtr* outReadSet,
		const ValueList_TCPSocketPtr* inWriteSet,
		ValueList_TCPSocketPtr* outWriteSet,
		const ValueList_TCPSocketPtr* inExceptSet,
		ValueList_TCPSocketPtr* outExceptSet)
{
	//build up some sets from our vectors
	fd_set read, write, except;

	int nfds = 0;

	fd_set *readPtr = do_fill_set_from_vector(read, inReadSet, nfds);
	fd_set *writePtr = do_fill_set_from_vector(write, inWriteSet, nfds);
	fd_set *exceptPtr = do_fill_set_from_vector(except, inExceptSet, nfds);

	int toRet = select(nfds + 1, readPtr, writePtr, exceptPtr, nullptr);

	if (toRet > 0)
	{
		do_fill_vector_from_set(outReadSet, inReadSet, read);
		do_fill_vector_from_set(outWriteSet, inWriteSet, write);
		do_fill_vector_from_set(outExceptSet, inExceptSet, except);
	}
	if (toRet < 0)
	{
		string msg("SocketUtil::Select");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		report_error(msg.c_str());
	}
	return toRet;
}
