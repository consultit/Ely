/**
 * \file socketUtil.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_SOCKETUTIL_H_
#define NETWORK_SOURCE_SOCKETUTIL_H_

#include "network_includes.h"

#include "udpSocket.h"
#include "networkTools.h"

enum SocketAddressFamily
{
	INET = AF_INET, INET6 = AF_INET6
};

/**
 * SocketUtil class.
 */
class EXPCL_NETWORK SocketUtil
{
PUBLISHED:
	enum ShutdownHow
	{
#if _WIN32
		SEND = SD_SEND,
		RECEIVE = SD_RECEIVE,
		BOTH = SD_BOTH
#else
		SEND = SHUT_WR, RECEIVE = SHUT_RD, BOTH = SHUT_RDWR
#endif
	};

	INLINE SocketUtil();
	class SocketLib
	{
	public:
		SocketLib() :
				_setup
				{ true }
		{
			//init (never executed for posix)
			if (!SocketUtil::static_init())
			{
				string msg("SocketUtil::StaticInit -> ");
#ifndef _WIN32
				msg += string(strerror(errno));
#endif //_WIN32
				SocketUtil::report_error(msg.c_str());
				_setup = false;
			}
		}
		~SocketLib()
		{
			if (_setup)
			{
				//cleanup
				SocketUtil::clean_up();
			}
		}
		bool get_is_setup() const
		{
			return _setup;
		}
		;
	private:
		bool _setup;
	};

	static bool static_init();
	static void clean_up();
	static void report_error(const char* inOperationDesc);
	static int get_last_error();
	static PT(UDPSocket) create_udp_socket(SocketAddressFamily inFamily);
	static PT(TCPSocket) create_tcp_socket(SocketAddressFamily inFamily);
	static int select_sockets(const ValueList_TCPSocketPtr* inReadSet,
			ValueList_TCPSocketPtr* outReadSet,
			const ValueList_TCPSocketPtr* inWriteSet,
			ValueList_TCPSocketPtr* outWriteSet,
			const ValueList_TCPSocketPtr* inExceptSet,
			ValueList_TCPSocketPtr* outExceptSet);
	// Python Properties
	MAKE_PROPERTY(last_error, get_last_error);

#ifndef CPPPARSER
private:
	inline static fd_set* do_fill_set_from_vector(fd_set& outSet,
			const ValueList_TCPSocketPtr* inSockets, int& ioNaxNfds);
	inline static void do_fill_vector_from_set(ValueList_TCPSocketPtr* outSockets,
			const ValueList_TCPSocketPtr* inSockets, const fd_set& inSet);
#endif //CPPPARSER
};

///inline
#include "socketUtil.I"

#endif /* NETWORK_SOURCE_SOCKETUTIL_H_ */
