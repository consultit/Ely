/**
 * \file dataVariables.cxx
 *
 * \date 2018-08-20
 * \author consultit
 */

#include "dataVariables.h"

///DataVariables's definitions

/**
 *
 */
DataVariables::DataVariables(const DataVariables& right):
	mVariables(), mIndex()
{
	for (auto& item : right.mVariables)
	{
		// copy only owned variables
		if (item.owned)
		{
			add_variable(item.name, item.prim);
			int idx = mIndex[item.name];
			(this->*mDataHandlers.at(item.prim).setter)(mVariables[idx].content,
					item.content);
		}
	}
}

/**
 *
 */
void DataVariables::add_variable(const string& inName,
		EPrimitiveType inPrimitiveType)
{
	switch (inPrimitiveType)
	{
#define EXPAND(type, dummy) \
	case EPT_##type:\
		mIndex[inName] = mVariables.size();\
		mVariables.emplace_back(inName, EPT_##type,\
				static_cast<void*>(new type()), true);\
		if (mDataHandlers.find(EPT_##type) == mDataHandlers.end())\
		{\
			mDataHandlers.emplace(EPT_##type, DataHandlers{\
				&DataVariables::do_set_var<type>,\
				&DataVariables::do_delete_var<type>,\
				&DataVariables::do_write_var<type>,\
				&DataVariables::do_read_var<type>\
			});\
		}\
		break;\

		DATAVARIABLESEXPAND
#undef EXPAND
	default:
		break;
	}
}

/**
 *
 */
#define EXPAND(type, typesuffix) \
void DataVariables::add_variable_##typesuffix(const string& inName, type* value)\
{\
	mIndex[inName] = mVariables.size();\
	mVariables.emplace_back(inName, EPT_##type, static_cast<void*>(value),\
			false);\
	if (mDataHandlers.find(EPT_##type) == mDataHandlers.end())\
	{\
		mDataHandlers.emplace(EPT_##type, DataHandlers{\
			&DataVariables::do_set_var<type>,\
			nullptr,\
			&DataVariables::do_write_var<type>,\
			&DataVariables::do_read_var<type>\
		});\
	}\
}\

	DATAVARIABLESEXPAND
#undef EXPAND

/**
 *
 */
void DataVariables::write(OutputMemoryBitStream& inOutputStream,
		const BitMask32& inDirtyState) const
{
	inOutputStream.write(inDirtyState);
	for (auto& item : mVariables)
	{
		if (item.dirtyState.get_word() & inDirtyState.get_word())
		{
			(this->*mDataHandlers.at(item.prim).writer)(inOutputStream,
					item.content);
		}
	}
}

/**
 *
 */
BitMask32 DataVariables::read(InputMemoryBitStream& inInputStream)
{
	BitMask32 inDirtyState;
	inInputStream.read(inDirtyState);
	for (auto& item : mVariables)
	{
		if (item.dirtyState.get_word() & inDirtyState.get_word())
		{
			(this->*mDataHandlers.at(item.prim).reader)(inInputStream,
					item.content);
		}
	}
	return inDirtyState;
}

/**
 *
 */
void DataVariables::do_delete_variables()
{
	for (auto& item : mVariables)
	{
		// delete only owned variables
		if (item.owned)
		{
			(this->*mDataHandlers.at(item.prim).deleter)(item.content);
		}
	}
}
