/**
 * \file deliveryNotificationManager.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "deliveryNotificationManager.h"

#include "timing.h"

///DeliveryNotificationManager definitions

/**
 *
 */
InFlightPacket* DeliveryNotificationManager::do_write_sequence_number(
		OutputMemoryBitStream& inOutputStream)
{
	//write the sequence number, but also create an inflight packet for this...
	PacketSequenceNumber sequenceNumber = mNextOutgoingSequenceNumber++;
	inOutputStream.write(sequenceNumber);

	++mDispatchedPacketCount;

	if (mShouldProcessAcks)
	{
		mInFlightPackets.emplace_back(sequenceNumber);

		return &mInFlightPackets.back();
	}
	else
	{
		return nullptr;
	}
}

/**
 *
 */
void DeliveryNotificationManager::do_write_ack_data(
		OutputMemoryBitStream& inOutputStream)
{
	//we usually will only have one packet to ack
	//so we'll follow that with a 0 bit if that's the case
	//however, if we have more than 1, we'll make that 1 bit a 1 and then write 8 bits of how many packets
	//we could do some statistical analysis to determine if this is the best strategy but we'll use it for now

	//do we have any pending acks?
	//if so, write a 1 bit and write the first range
	//otherwise, write 0 bit
	bool hasAcks = (mPendingAcks.size() > 0);

	inOutputStream.write(hasAcks);
	if (hasAcks)
	{
		//note, we could write all the acks
		mPendingAcks.front().write(inOutputStream);
		mPendingAcks.pop_front();
	}
}

/**
 * Returns wether to drop the packet- if sequence number is too low!
 */
bool DeliveryNotificationManager::do_process_sequence_number(
		InputMemoryBitStream& inInputStream)
{
	PacketSequenceNumber sequenceNumber;

	inInputStream.read(sequenceNumber);
	if (sequenceNumber == mNextExpectedSequenceNumber)
	{
		mNextExpectedSequenceNumber = sequenceNumber + 1;
		//is this what we expect? great, let's add an ack to our pending list
		if (mShouldSendAcks)
		{
			do_add_pending_ack(sequenceNumber);
		}
		//and let's continue processing this packet...
		return true;
	}
	//is the sequence number less than our current expected sequence? silently drop it.
	//if this is due to wrapping around, we might fail to ack some packets that we should ack, but they'll get resent, so it's not a big deal
	//note that we don't have to re-ack it because our system doesn't reuse sequence numbers
	else if (sequenceNumber < mNextExpectedSequenceNumber)
	{
		return false;
	}
	else if (sequenceNumber > mNextExpectedSequenceNumber)
	{
		//we missed a lot of packets!
		//so our next expected packet comes after this one...
		mNextExpectedSequenceNumber = sequenceNumber + 1;
		//we should nack the missing packets..this will happen automatically inside do_add_pending_ack because
		//we're adding an unconsequitive ack
		//and then we can ack this and process it
		if (mShouldSendAcks)
		{
			do_add_pending_ack(sequenceNumber);
		}
		return true;
	}

	//drop packet if we couldn't even read sequence number!
	return false;
}

/**
 * In each packet we can ack a range: anything in flight before the range will
 * be considered nackd by the other side immediately.
 */
void DeliveryNotificationManager::do_process_acks(
		InputMemoryBitStream& inInputStream)
{

	bool hasAcks;
	inInputStream.read(hasAcks);
	if (hasAcks)
	{
		AckRange ackRange;
		ackRange.read(inInputStream);

		//for each InfilghtPacket with a sequence number less than the start, handle delivery failure...
		PacketSequenceNumber nextAckdSequenceNumber = ackRange.get_start();
		uint32_t onePastAckdSequenceNumber = nextAckdSequenceNumber
				+ ackRange.get_count();
		while (nextAckdSequenceNumber < onePastAckdSequenceNumber
				&& !mInFlightPackets.empty())
		{
			const auto& nextInFlightPacket = mInFlightPackets.front();
			//if the packet has a lower sequence number, we didn't get an ack for it, so it probably wasn't delivered
			PacketSequenceNumber nextInFlightPacketSequenceNumber =
					nextInFlightPacket.get_sequence_number();
			if (nextInFlightPacketSequenceNumber < nextAckdSequenceNumber)
			{
				//copy this so we can remove it before handling the failure- we don't want to find it when checking for state
				auto copyOfInFlightPacket = nextInFlightPacket;
				mInFlightPackets.pop_front();
				do_handle_packet_delivery_failure(copyOfInFlightPacket);
			}
			else if (nextInFlightPacketSequenceNumber == nextAckdSequenceNumber)
			{
				do_handle_packet_delivery_success(nextInFlightPacket);
				//received!
				mInFlightPackets.pop_front();
				//decrement count, advance nextAckdSequenceNumber
				++nextAckdSequenceNumber;
			}
			else if (nextInFlightPacketSequenceNumber > nextAckdSequenceNumber)
			{
				//we've already ackd some packets in here.
				//keep this packet in flight, but keep going through the ack...
				++nextAckdSequenceNumber;
			}
		}
	}
}

/**
 *
 */
void DeliveryNotificationManager::process_timed_out_packets()
{
	float timeoutTime = Timing::get_global_ptr()->get_timef() - mDelayBeforeAckTimeout;

	while (!mInFlightPackets.empty())
	{
		const auto& nextInFlightPacket = mInFlightPackets.front();

		//was this packet dispatched before the current time minus the timeout duration?
		if (nextInFlightPacket.get_time_dispatched() < timeoutTime)
		{
			//it failed! let us know about that
			do_handle_packet_delivery_failure(nextInFlightPacket);
			mInFlightPackets.pop_front();
		}
		else
		{
			//it wasn't, and packets are all in order by time here, so we know we don't have to check farther
			break;
		}
	}
}

/**
 *
 */
void DeliveryNotificationManager::do_add_pending_ack(
		PacketSequenceNumber inSequenceNumber)
{
	//if you don't have a range yet, or you can't correctly extend the final range with the sequence number,
	//start a new range
	if (mPendingAcks.size() == 0
			|| !mPendingAcks.back().extend_if_should(inSequenceNumber))
	{
		mPendingAcks.emplace_back(inSequenceNumber);
	}
}

/**
 *
 */
void DeliveryNotificationManager::do_handle_packet_delivery_failure(
		const InFlightPacket& inFlightPacket)
{
	++mDroppedPacketCount;
	inFlightPacket.handle_delivery_failure(this);

}

/**
 *
 */
void DeliveryNotificationManager::do_handle_packet_delivery_success(
		const InFlightPacket& inFlightPacket)
{
	++mDeliveredPacketCount;
	inFlightPacket.handle_delivery_success(this);
}
