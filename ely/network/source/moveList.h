/**
 * \file moveList.h
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_MOVELIST_H_
#define NETWORK_SOURCE_MOVELIST_H_

#include "network_includes.h"

#include "move.h"
#include <deque>

using std::deque;

/**
 * MoveList class.
 */
class EXPCL_NETWORK MoveList
{
public:
	typedef deque<Move>::const_iterator const_iterator;
	typedef deque<Move>::const_reverse_iterator const_reverse_iterator;
	typedef deque<Move>::iterator iterator;
	typedef deque<Move>::reverse_iterator reverse_iterator;

PUBLISHED:

	INLINE MoveList();
	const Move& add_move(const InputState& inInputState, float inTimestamp);
	bool add_move_if_new(const Move& inMove);
	void removed_processed_moves(float inLastMoveProcessedOnServerTimestamp);
	INLINE float get_last_move_timestamp() const;
	INLINE const Move& get_latest_move() const;
	INLINE bool get_has_moves() const;
	INLINE const Move& get_move(int index) const;
	INLINE int get_move_count() const;
	MAKE_SEQ(get_moves, get_move_count, get_move);
	//for for each, we have to match stl calling convention
	INLINE const Move& operator[](size_t i) const;
	INLINE int size() const;
	INLINE void clear();
	// Python Properties
	MAKE_PROPERTY(last_move_timestamp, get_last_move_timestamp);
	MAKE_PROPERTY(latest_move, get_latest_move);
	MAKE_PROPERTY(has_moves, get_has_moves);
	MAKE_PROPERTY(move_count, get_move_count);
	MAKE_SEQ_PROPERTY(moves, get_move_count, get_move);

public:
	inline const_iterator begin() const;
	inline const_iterator end() const;
	inline iterator begin();
	inline iterator end();

#ifndef CPPPARSER
private:
	float mLastMoveTimestamp;
	deque<Move> mMoves;
#endif //CPPPARSER
};

///inline
#include "moveList.I"

#endif /* NETWORK_SOURCE_MOVELIST_H_ */
