/**
 * \file tcpSocket.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_TCPSOCKET_H_
#define NETWORK_SOURCE_TCPSOCKET_H_

#include "network_includes.h"

#include "socketAddress.h"
#include "typedReferenceCount.h"
#include "pointerTo.h"

/**
 * TCPSocket class.
 */
class EXPCL_NETWORK TCPSocket: public TypedReferenceCount
{
PUBLISHED:

	INLINE ~TCPSocket();
	int connect_at(const SocketAddress& inAddress);
	int bind_at(const SocketAddress& inToAddress);
	int listen_at(int inBackLog = 32);
	PT(TCPSocket) accept_at(SocketAddress& inFromAddress);
	int32_t send_to(const void* inData, size_t inLen);
	int32_t receive_from(void* inBuffer, size_t inLen);
	int shutdown_connection(int how);
	int get_connected_peer(SocketAddress& address) const;
	PT(SocketAddress) get_connected_peer() const;
	int set_socket_opt(int level, int optname, const char *optval, int optlen);
	int get_socket_opt(int level, int optname, char *optval, int *optlen) const;
	// Python Properties
	MAKE_PROPERTY(connected_peer, get_connected_peer);

public:
	TCPSocket() = delete;

#ifndef CPPPARSER
private:
	friend class SocketUtil;
	inline explicit TCPSocket(SOCKET inSocket);
	SOCKET mSocket;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(TCPSocket,TypedReferenceCount)
};

///inline
#include "tcpSocket.I"

#endif /* NETWORK_SOURCE_TCPSOCKET_H_ */
