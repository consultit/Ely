/**
 * \file networkManager.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_GAMENETWORKMANGER_H_
#define NETWORK_SOURCE_GAMENETWORKMANGER_H_

#include "network_includes.h"

#include "networkObject.h"
#include "udpSocket.h"
#include "weightedTimedMovingAverage.h"
#include <unordered_map>

using std::unordered_map;
using std::deque;

typedef unordered_map<int, PT(NetworkObject)> IntToNetworkObjectMap;

BEGIN_PUBLISH
constexpr EXPCL_NETWORK uint32_t constUInt32(char c3, char c2, char c1, char c0)
{
	return (static_cast<uint32_t>(c3) & 0x000000FF) << 24 |
			(static_cast<uint32_t>(c2) & 0x000000FF) << 16 |
			(static_cast<uint32_t>(c1) & 0x000000FF) << 8 |
			(static_cast<uint32_t>(c0) & 0x000000FF);
}
END_PUBLISH

/**
 * GameNetworkManager class.
 */
class EXPCL_NETWORK GameNetworkManager: public TypedReferenceCount
{
public:
	static constexpr uint32_t kHelloCC = constUInt32('H','E','L','O');
	static constexpr uint32_t kWelcomeCC = constUInt32('W','L','C','M');
	static constexpr uint32_t kStateCC = constUInt32('S','T','A','T');
	static constexpr uint32_t kInputCC = constUInt32('I','N','P','T');
	static constexpr uint32_t kReplCC = constUInt32('R','P','L','M');
	static constexpr int kMaxPacketsPerFrameCount = 10;

PUBLISHED:

	INLINE GameNetworkManager();
	INLINE virtual ~GameNetworkManager();
	bool init(uint16_t inPort);
	void process_incoming_packets();
	void send_packet(const OutputMemoryBitStream& inOutputStream,
			const SocketAddress& inFromAddress);
	INLINE const WeightedTimedMovingAverage& get_bytes_received_per_second() const;
	INLINE const WeightedTimedMovingAverage& get_bytes_sent_per_second() const;
	INLINE PT(NetworkObject) get_network_object(int inNetworkId) const;
	INLINE void add_to_network_id_to_network_object_map(PT(NetworkObject) inNetworkObject);
	INLINE void remove_from_network_id_to_network_object_map(PT(NetworkObject) inNetworkObject);
	// Python Properties
	MAKE_PROPERTY(bytes_received_per_second, get_bytes_received_per_second);
	MAKE_PROPERTY(bytes_sent_per_second, get_bytes_sent_per_second);

	/**
	 * \name DEBUG NETWORK UTILITIES
	 */
	///@{
	INLINE void set_drop_packet_chance(float inChance);
	INLINE float get_drop_packet_chance() const;
	INLINE void set_simulated_latency(float inLatency);
	INLINE float get_simulated_latency() const;
	INLINE void set_simulated_max_jitter(float inJitter);
	INLINE float get_simulated_max_jitter() const;
	// Python Properties
	MAKE_PROPERTY(drop_packet_chance, get_drop_packet_chance, set_drop_packet_chance);
	MAKE_PROPERTY(simulated_latency, get_simulated_latency, set_simulated_latency);
	MAKE_PROPERTY(simulated_max_jitter, get_simulated_max_jitter, set_simulated_max_jitter);
	///@}

public:
	virtual void process_packet(InputMemoryBitStream& inInputStream,
			const SocketAddress& inFromAddress) = 0;
	inline virtual void handle_connection_reset(const SocketAddress& inFromAddress);

#ifndef CPPPARSER
protected:
	IntToNetworkObjectMap mNetworkIdToNetworkObjectMap;

private:
	class ReceivedPacket
	{
	public:
		ReceivedPacket(float inReceivedTime,
				InputMemoryBitStream& inInputMemoryBitStream,
				const SocketAddress& inAddress);

		const SocketAddress& get_from_address() const
		{
			return mFromAddress;
		}
		float get_received_time() const
		{
			return mReceivedTime;
		}
		InputMemoryBitStream& get_packet_buffer()
		{
			return mPacketBuffer;
		}

	private:
		float mReceivedTime;
		InputMemoryBitStream mPacketBuffer;
		SocketAddress mFromAddress;

	};

	void do_update_bytes_sent_last_frame();
	void do_read_incoming_packets_into_queue();
	void do_process_queued_packets();
	deque<ReceivedPacket> mPacketQueue;
	PT(UDPSocket) mSocket;
	WeightedTimedMovingAverage mBytesReceivedPerSecond;
	WeightedTimedMovingAverage mBytesSentPerSecond;
	int mBytesSentThisFrame;

#ifdef ELY_DEBUG
	// simulate packet's drop, latency, jitter
	float mDropPacketChance;
	float mSimulatedLatency;
	float mDoubleSimulatedMaxJitter;
#endif //ELY_DEBUG
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(GameNetworkManager,TypedReferenceCount)
};

///inline
#include "gameNetworkManager.I"

#endif /* NETWORK_SOURCE_GAMENETWORKMANGER_H_ */
