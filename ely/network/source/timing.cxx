/**
 * \file timing.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "timing.h"

#include <chrono>

#if !_WIN32
#include <chrono>
using namespace std::chrono;
#endif

#ifdef LOCKTIMEFRAME
float kDesiredFrameTime = 0.0166f;
#endif

namespace
{
#if _WIN32
LARGE_INTEGER sStartTime =
{	0};
#else
high_resolution_clock::time_point sStartTime;
#endif
}

///Timing definitions

/**
 *
 */
Timing::Timing() :
		mPerfCountDuration(0.f), mDeltaTime(0.f), mFrameStartTimef(0.f), mLastFrameStartTime(
				0.f), mDeltaTick(0)
{
#if _WIN32
	LARGE_INTEGER perfFreq;
	QueryPerformanceFrequency( &perfFreq );
	mPerfCountDuration = 1.0 / perfFreq.QuadPart;

	QueryPerformanceCounter( &sStartTime );

	mLastFrameStartTime = get_time();
#else
	sStartTime = high_resolution_clock::now();
#endif
}

/**
 *
 */
void Timing::update()
{

	double currentTime = get_time();

	mDeltaTime = (float) (currentTime - mLastFrameStartTime);
#ifdef LOCKTIMEFRAME

	//frame lock at 60fps
	while( mDeltaTime < kDesiredFrameTime )
	{
		currentTime = get_time();

		mDeltaTime = (float)( currentTime - mLastFrameStartTime );
	}
#endif

	mLastFrameStartTime = currentTime;
	mFrameStartTimef = static_cast<float>(mLastFrameStartTime);

}

/**
 *
 */
double Timing::get_time() const
{
#if _WIN32
	LARGE_INTEGER curTime, timeSinceStart;
	QueryPerformanceCounter( &curTime );

	timeSinceStart.QuadPart = curTime.QuadPart - sStartTime.QuadPart;

	return timeSinceStart.QuadPart * mPerfCountDuration;
#else
	auto now = high_resolution_clock::now();
	auto ms = duration_cast<milliseconds>(now - sStartTime).count();
	//a little uncool to then convert into a double just to go back, but oh well.
	return static_cast<double>(ms) / 1000;
#endif
}
