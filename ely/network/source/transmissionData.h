/**
 * \file transmissionData.h
 *
 * \date 2018-07-24
 * \author consultit
 */

#ifndef NETWORK_SOURCE_TRANSMISSIONDATA_H_
#define NETWORK_SOURCE_TRANSMISSIONDATA_H_

#include "network_includes.h"

#include "typedReferenceCount.h"
#include "pointerTo.h"

class DeliveryNotificationManager;

/**
 * TransmissionData class.
 */
class EXPCL_NETWORK TransmissionData: public TypedReferenceCount
{
PUBLISHED:

	INLINE TransmissionData();
	INLINE virtual ~TransmissionData();
	INLINE virtual void handle_delivery_failure(
			DeliveryNotificationManager* inDeliveryNotificationManager) const;
	INLINE virtual void handle_delivery_success(
			DeliveryNotificationManager* inDeliveryNotificationManager) const;
	// Python Properties

TYPED_OBJECT_API_DECL(TransmissionData,TypedReferenceCount)
};

///inline
#include "transmissionData.I"

#endif /* NETWORK_SOURCE_TRANSMISSIONDATA_H_ */
