/**
 * \file move.cxx
 *
 * \date 2018-07-22
 * \author consultit
 */

#include "move.h"

///Move definitions

/**
 *
 */
bool Move::write(OutputMemoryBitStream& inOutputStream) const
{
	mInputState.write(inOutputStream);
	inOutputStream.write(mTimestamp);
	inOutputStream.write(mDeltaTime);
	return true;
}

/**
 *
 */
bool Move::read(InputMemoryBitStream& inInputStream)
{
	mInputState.read(inInputStream);
	inInputStream.read(mTimestamp);
	inInputStream.read(mDeltaTime);
	return true;
}
