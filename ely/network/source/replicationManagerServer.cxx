/**
 * \file replicationManagerServer.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "replicationManagerServer.h"

#include "gameNetworkManagerServer.h"
#include "networkObject.h"
#include "replicationManagerTransmissionData.h"

/**
 *
 */
void ReplicationManagerServer::replicate_create(int inNetworkId,
		uint32_t inInitialDirtyState)
{
	mNetworkIdToReplicationCommand[inNetworkId] = ReplicationCommand(
			inInitialDirtyState);
}

/**
 *
 */
void ReplicationManagerServer::replicate_destroy(int inNetworkId)
{
	//it's broken if we don't find it...
	mNetworkIdToReplicationCommand[inNetworkId].set_destroy();
}

/**
 *
 */
void ReplicationManagerServer::remove_from_replication(int inNetworkId)
{
	mNetworkIdToReplicationCommand.erase(inNetworkId);
}

/**
 *
 */
void ReplicationManagerServer::set_state_dirty(int inNetworkId,
		uint32_t inDirtyState)
{
	mNetworkIdToReplicationCommand[inNetworkId].add_dirty_state(inDirtyState);
}

/**
 *
 */
void ReplicationManagerServer::handle_create_ackd(int inNetworkId)
{
	mNetworkIdToReplicationCommand[inNetworkId].handle_create_ackd();
}

/**
 *
 */
void ReplicationManagerServer::write(OutputMemoryBitStream& inOutputStream,
		ReplicationManagerTransmissionData* ioTransmissinData)
{
	//run through each replication command and do something...
	for (auto& pair : mNetworkIdToReplicationCommand)
	{
		ReplicationCommand& replicationCommand = pair.second;
		if (replicationCommand.get_has_dirty_state())
		{
			int networkId = pair.first;

			//well, first write the network id...
			inOutputStream.write(networkId);

			//only need 2 bits for action...
			ReplicationAction action = replicationCommand.get_action();
			inOutputStream.write(action, 2);

			uint32_t writtenState = 0;
			uint32_t dirtyState = replicationCommand.get_dirty_state();

			//now do what?
			switch (action)
			{
			case RA_Create:
				writtenState = do_write_create_action(inOutputStream, networkId,
						dirtyState);
				break;
			case RA_Update:
				writtenState = do_write_update_action(inOutputStream, networkId,
						dirtyState);
				break;
			case RA_Destroy:
				//don't need anything other than state!
				writtenState = do_write_destroy_action(inOutputStream, networkId,
						dirtyState);
				break;
			case RA_RPC:
			case RA_MAX:
			default:
				break;
			}

			ioTransmissinData->add_transmission(networkId, action, writtenState);

			//let's pretend everything was written- don't make this too hard
			replicationCommand.clear_dirty_state(writtenState);

		}
	}
}

/**
 *
 */
uint32_t ReplicationManagerServer::do_write_create_action(
		OutputMemoryBitStream& inOutputStream, int inNetworkId,
		uint32_t inDirtyState)
{
	//need object
	PT(NetworkObject) networkObject = GameNetworkManagerServer::get_global_ptr()->get_network_object(
			inNetworkId);
	//need 4 cc
	inOutputStream.write(networkObject->get_class_id());
	return networkObject->write(inOutputStream, inDirtyState);
}

/**
 *
 */
uint32_t ReplicationManagerServer::do_write_update_action(
		OutputMemoryBitStream& inOutputStream, int inNetworkId,
		uint32_t inDirtyState)
{
	//need object
	PT(NetworkObject) networkObject = GameNetworkManagerServer::get_global_ptr()->get_network_object(
			inNetworkId);

	//if we can't find the networkObject on the other side, we won't be able to read the written data ( since we won't know which class wrote it )
	//so we need to know how many bytes to skip.

	//this means we need byte sand each new object needs to be byte aligned

	uint32_t writtenState = networkObject->write(inOutputStream, inDirtyState);

	return writtenState;
}

/**
 *
 */
uint32_t ReplicationManagerServer::do_write_destroy_action(
		OutputMemoryBitStream& inOutputStream, int inNetworkId,
		uint32_t inDirtyState)
{
	(void) inOutputStream;
	(void) inNetworkId;
	(void) inDirtyState;
	//don't have to do anything- action already written

	return inDirtyState;
}
