/**
 * \file transmissionData.I
 *
 * \date 2018-07-24
 * \author consultit
 */

#ifndef NETWORK_SOURCE_TRANSMISSIONDATA_I_
#define NETWORK_SOURCE_TRANSMISSIONDATA_I_

///TransmissionData inline definitions

/**
 *
 */
INLINE TransmissionData::TransmissionData()
{
}

/**
 *
 */
INLINE TransmissionData::~TransmissionData()
{
}

/**
 *
 */
INLINE void TransmissionData::handle_delivery_failure(
			DeliveryNotificationManager* inDeliveryNotificationManager) const
{
}

/**
 *
 */
INLINE void TransmissionData::handle_delivery_success(
			DeliveryNotificationManager* inDeliveryNotificationManager) const
{
}

#endif /* NETWORK_SOURCE_TRANSMISSIONDATA_I_ */
