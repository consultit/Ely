/**
 * \file networkManagerServer.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_GAMENETWORKMANGERSERVER_H_
#define NETWORK_SOURCE_GAMENETWORKMANGERSERVER_H_

#include "gameNetworkManager.h"
#include "network_includes.h"

#include "networkObject.h"
#include "socketAddress.h"
#include "networkTools.h"

/**
 * GameNetworkManagerServer class.
 *
 * \note Callbacks:
 *	- void (*SendWelcomePacketCallback)(OutputMemoryBitStream&);
 *	- void (*SendStatePacketToClientCallback)(OutputMemoryBitStream&);
 *	- void (*HandlePacketFromNewClientCallback)(PT(ClientProxy));
 *	- void (*HandleClientDisconnectedCallback)(PT(ClientProxy));
 *	- void (*RespawnClientObjectsCallback)(int);
 */
class EXPCL_NETWORK GameNetworkManagerServer: public GameNetworkManager,
		public Singleton<GameNetworkManagerServer>
{
PUBLISHED:
	typedef void (*SendWelcomePacketCallback)(OutputMemoryBitStream&);
	typedef void (*SendStatePacketToClientCallback)(OutputMemoryBitStream&);
	typedef void (*HandlePacketFromNewClientCallback)(PT(ClientProxy));
	typedef void (*HandleClientDisconnectedCallback)(PT(ClientProxy));

	GameNetworkManagerServer();
	~GameNetworkManagerServer();
	INLINE static GameNetworkManagerServer* get_global_ptr();
	void send_outgoing_packets();
	void check_for_disconnects();
	void register_network_object(PT(NetworkObject) inNetworkObject);
	INLINE PT(NetworkObject) register_and_return(PT(NetworkObject) inNetworkObject);
	void unregister_network_object(PT(NetworkObject) inNetworkObject);
	void set_state_dirty(int inNetworkId, uint32_t inDirtyState);
	void respawn_objects_for_clients();
	INLINE void set_client_disconnect_timeout(float tmo);
	INLINE float get_client_disconnect_timeout() const;
	INLINE int get_clients_num() const;
	PT(ClientProxy) get_client_proxy(int inPlayerId) const;
	ValueList_ClientProxyPtr get_all_client_proxies() const;
	void clear_all_client_moves();
	INLINE void set_respawn_client_objects_delay(float tmo);
	INLINE float get_respawn_client_objects_delay() const;
	INLINE void set_delay_before_ack_timeout(float tmo);
	INLINE float get_delay_before_ack_timeout() const;
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	void set_send_welcome_packet_callback(PyObject* clbk);
	void set_send_state_packet_to_client_callback(PyObject* clbk);
	void set_handle_packet_from_new_client_callback(PyObject* clbk);
	void set_handle_client_disconnected_callback(PyObject* clbk);
	void set_respawn_client_objects_callback(PyObject* clbk);
#else
	void set_send_welcome_packet_callback(SendWelcomePacketCallback clbk);
	void set_send_state_packet_to_client_callback(SendStatePacketToClientCallback clbk);
	void set_handle_packet_from_new_client_callback(HandlePacketFromNewClientCallback clbk);
	void set_handle_client_disconnected_callback(HandleClientDisconnectedCallback clbk);
	void set_respawn_client_objects_callback(RespawnClientObjectsCallback clbk);
#endif // PYTHON_BUILD
	// Python Properties
	MAKE_PROPERTY(client_disconnect_timeout, get_client_disconnect_timeout, set_client_disconnect_timeout);
	MAKE_PROPERTY(clients_num, get_clients_num);
	MAKE_PROPERTY(respawn_client_objects_delay, get_respawn_client_objects_delay, set_respawn_client_objects_delay);
	MAKE_PROPERTY(delay_before_ack_timeout, get_delay_before_ack_timeout, set_delay_before_ack_timeout);

public:
	virtual void process_packet(InputMemoryBitStream& inInputStream,
			const SocketAddress& inFromAddress) override;
	void handle_connection_reset(const SocketAddress& inFromAddress)
			override;

#ifndef CPPPARSER
private:
	typedef unordered_map<int, PT(ClientProxy)> IntToClientMap;
	typedef unordered_map<SocketAddress, PT(ClientProxy)> AddressToClientMap;
	AddressToClientMap mAddressToClientMap;
	IntToClientMap mPlayerIdToClientMap;
	int mNewPlayerId;
	int mNewNetworkId;
/*fixme not used:remove?
	float mTimeOfLastStatePacket;
	float mTimeBetweenStatePackets;
*/
	float mClientDisconnectTimeout;
	float mRespawnClientObjectsDelay;
	float mDelayBeforeAckTimeout;
	void do_process_packet(PT(ClientProxy) inClientProxy,
			InputMemoryBitStream& inInputStream);
	void do_update_all_clients();
	void do_send_state_packet_to_client(PT(ClientProxy) inClientProxy);
	void do_write_last_move_timestamp_if_dirty(OutputMemoryBitStream& inOutputStream,
			PT(ClientProxy) inClientProxy);
	void do_handle_input_packet(PT(ClientProxy) inClientProxy,
			InputMemoryBitStream& inInputStream);
	int do_get_new_network_id();
	void do_handle_packet_from_new_client(InputMemoryBitStream& inInputStream,
			const SocketAddress& inFromAddress);
	void do_send_welcome_packet(PT(ClientProxy) inClientProxy);
	void do_add_world_state_to_packet(OutputMemoryBitStream& inOutputStream);
	void do_handle_client_disconnected(PT(ClientProxy) inClientProxy);
#if defined(PYTHON_BUILD)
	PyObject *mHandlePacketFromNewClientClbk, *mSendWelcomePacketClbk,
			*mSendStatePacketToClientClbk, *mHandleClientDisconnectedClbk,
			*mRespawnClientObjectsClbk;
#else
	HandlePacketFromNewClientCallback mHandlePacketFromNewClientClbk;
	SendWelcomePacketCallback mSendWelcomePacketClbk;
	SendStatePacketToClientCallback mSendStatePacketToClientClbk;
	HandleClientDisconnectedCallback mHandleClientDisconnectedClbk;
	RespawnClientObjectsCallback mRespawnClientObjectsClbk;
#endif // PYTHON_BUILD
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(GameNetworkManagerServer,GameNetworkManager)
};

///inline
#include "gameNetworkManagerServer.I"

#endif /* NETWORK_SOURCE_GAMENETWORKMANGERSERVER_H_ */
