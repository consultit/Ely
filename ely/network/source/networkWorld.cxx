/**
 * \file networkWorld.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "networkWorld.h"

///NetworkWorld definitions

/**
 *
 */
void NetworkWorld::add_network_object(PT(NetworkObject) inNetworkObject)
{
	mNetworkObjects.push_back(inNetworkObject);
	inNetworkObject->set_index_in_world(mNetworkObjects.size() - 1);
}

/**
 *
 */
void NetworkWorld::remove_network_object(PT(NetworkObject) inNetworkObject)
{
	int index = inNetworkObject->get_index_in_world();

	int lastIndex = mNetworkObjects.size() - 1;
	if (index != lastIndex)
	{
		mNetworkObjects[index] = mNetworkObjects[lastIndex];
		mNetworkObjects[index]->set_index_in_world(index);
	}

	inNetworkObject->set_index_in_world(-1);

	mNetworkObjects.pop_back();
}

/**
 *
 */
void NetworkWorld::update()
{
	//update all game objects- sometimes they want to die, so we need to treat carefully...

	for (int i = 0, c = mNetworkObjects.size(); i < c; ++i)
	{
		PT(NetworkObject) go = mNetworkObjects[i];

		if (!go->get_does_want_to_die())
		{
			go->update();
		}
		//you might suddenly want to die after your update, so check again
		if (go->get_does_want_to_die())
		{
			remove_network_object(go);
			go->handle_dying();
			--i;
			--c;
		}
	}
}

TYPED_OBJECT_API_DEF(NetworkWorld)
