/**
 * \file inFlightPacket.cxx
 *
 * \date 2018-07-24
 * \author consultit
 */

#include "inFlightPacket.h"
#include "timing.h"

///InFlightPacket definitions

/**
 *
 */
void InFlightPacket::handle_delivery_failure(
		DeliveryNotificationManager* inDeliveryNotificationManager) const
{
	for (const auto& pair : mTransmissionDataMap)
	{
		pair.second->handle_delivery_failure(inDeliveryNotificationManager);
	}
}

/**
 *
 */
void InFlightPacket::handle_delivery_success(
		DeliveryNotificationManager* inDeliveryNotificationManager) const
{
	for (const auto& pair : mTransmissionDataMap)
	{
		pair.second->handle_delivery_success(inDeliveryNotificationManager);
	}
}
