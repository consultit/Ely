/**
 * \file timing.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_TIMING_H_
#define NETWORK_SOURCE_TIMING_H_

#include "network_includes.h"

#include <cstdint>

/**
 * Timing class.
 */
class EXPCL_NETWORK Timing: public Singleton<Timing>
{
PUBLISHED:

	Timing();
	INLINE static Timing* get_global_ptr();
	void update();
	INLINE float get_delta_time() const;
	double get_time() const;
	INLINE float get_timef() const;
	INLINE float get_frame_start_time() const;
	// Python Properties
	MAKE_PROPERTY(delta_time, get_delta_time);
	MAKE_PROPERTY(time, get_time);
	MAKE_PROPERTY(timef, get_timef);
	MAKE_PROPERTY(frame_start_time, get_frame_start_time);

#ifndef CPPPARSER
private:
	float mDeltaTime;
	uint64_t mDeltaTick;
	double mLastFrameStartTime;
	float mFrameStartTimef;
	double mPerfCountDuration;
#endif //CPPPARSER
};

///inline
#include "timing.I"

#endif /* NETWORK_SOURCE_TIMING_H_ */
