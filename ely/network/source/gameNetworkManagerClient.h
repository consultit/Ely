/**
 * \file networkManagerClient.h
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_GAMENETWORKMANGERCLIENT_H_
#define NETWORK_SOURCE_GAMENETWORKMANGERCLIENT_H_

#include "network_includes.h"

#include "memoryBitStream.h"
#include "socketAddress.h"
#include "deliveryNotificationManager.h"
#include "gameNetworkManager.h"
#include "replicationManagerClient.h"
#include "weightedTimedMovingAverage.h"

/**
 * GameNetworkManagerClient class.
 *
 * \note Callbacks:
 *	- void (*HandleWelcomePacketCallback)(InputMemoryBitStream&);
 *	- void (*HandleStatePacketCallback)(InputMemoryBitStream&);
 */
class EXPCL_NETWORK GameNetworkManagerClient: public GameNetworkManager,
		public Singleton<GameNetworkManagerClient>
{
	enum NetworkClientState
	{
		NCS_Uninitialized, NCS_SayingHello, NCS_Welcomed
	};

PUBLISHED:
	typedef void (*HandleWelcomePacketCallback)(InputMemoryBitStream&);
	typedef void (*HandleStatePacketCallback)(InputMemoryBitStream&);

	GameNetworkManagerClient();
	~GameNetworkManagerClient();
	INLINE static GameNetworkManagerClient* get_global_ptr();
	bool init(const SocketAddress& inServerAddress, const string& inName);
	void send_outgoing_packets();
	INLINE const WeightedTimedMovingAverage& get_avg_round_trip_time() const;
	INLINE float get_round_trip_time() const;
	INLINE int get_player_id() const;
	INLINE float get_last_move_processed_by_server_timestamp() const;
	INLINE void set_delay_before_ack_timeout(float tmo);
	INLINE float get_delay_before_ack_timeout() const;
	INLINE void set_time_between_hellos(float tmo);
	INLINE float get_time_between_hellos() const;
	INLINE void set_time_between_input_packets(float tmo);
	INLINE float get_time_between_input_packets() const;
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	void set_handle_welcome_packet_callback(PyObject* clbk);
	void set_handle_state_packet_callback(PyObject* clbk);
#else
	void set_handle_welcome_packet_callback(HandleWelcomePacketCallback clbk);
	void set_handle_state_packet_callback(HandleStatePacketCallback clbk);
#endif // PYTHON_BUILD
	// Python Properties
	MAKE_PROPERTY(avg_round_trip_time, get_avg_round_trip_time);
	MAKE_PROPERTY(round_trip_time, get_round_trip_time);
	MAKE_PROPERTY(player_id, get_player_id);
	MAKE_PROPERTY(last_move_processed_by_server_timestamp, get_last_move_processed_by_server_timestamp);
	MAKE_PROPERTY(delay_before_ack_timeout, get_delay_before_ack_timeout, set_delay_before_ack_timeout);
	MAKE_PROPERTY(time_between_hellos, get_time_between_hellos, set_time_between_hellos);
	MAKE_PROPERTY(time_between_input_packets, get_time_between_input_packets, set_time_between_input_packets);

public:
	virtual void process_packet(InputMemoryBitStream& inInputStream,
			const SocketAddress& inFromAddress) override;

#ifndef CPPPARSER
private:
	DeliveryNotificationManager mDeliveryNotificationManager;
	ReplicationManagerClient mReplicationManagerClient;
	SocketAddress mServerAddress;
	NetworkClientState mState;
	float mTimeOfLastHello;
	float mTimeOfLastInputPacket;
	string mName;
	int mPlayerId;
	float mLastMoveProcessedByServerTimestamp;
	WeightedTimedMovingAverage mAvgRoundTripTime;
	float mLastRoundTripTime;
	float mDelayBeforeAckTimeout;
	float mTimeBetweenHellos;
	float mTimeBetweenInputPackets;
	void do_update_saying_hello();
	void do_send_hello_packet();
	void do_read_last_move_processed_on_server_timestamp(
			InputMemoryBitStream& inInputStream);
	void do_update_sending_input_packet();
	void do_send_input_packet();
	void do_destroy_network_objects_in_map(const IntToNetworkObjectMap& inObjectsToDestroy);
	void do_handle_welcome_packet(InputMemoryBitStream& inInputStream);
	void do_handle_state_packet(InputMemoryBitStream& inInputStream);
#if defined(PYTHON_BUILD)
	PyObject *mHandleWelcomePacketClbk, *mHandleStatePacketClbk;
#else
	HandleWelcomePacketCallback mHandleWelcomePacketClbk;
	HandleStatePacketCallback mHandleStatePacketClbk;
#endif // PYTHON_BUILD
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(GameNetworkManagerClient,GameNetworkManager)
};

///inline
#include "gameNetworkManagerClient.I"

#endif /* NETWORK_SOURCE_GAMENETWORKMANGERCLIENT_H_ */
