/**
 * \file clientProxy.I
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_CLIENTPROXY_I_
#define NETWORK_SOURCE_CLIENTPROXY_I_

///ClientProxy inline definitions

/**
 *
 */
INLINE const SocketAddress& ClientProxy::get_socket_address() const
{
	return mSocketAddress;
}

/**
 *
 */
INLINE int ClientProxy::get_player_id() const
{
	return mPlayerId;
}

/**
 *
 */
INLINE const string& ClientProxy::get_name() const
{
	return mName;
}

/**
 *
 */
INLINE void ClientProxy::update_last_packet_time()
{
	mLastPacketFromClientTime = Timing::get_global_ptr()->get_timef();
}

/**
 *
 */
INLINE void ClientProxy::compute_time_to_respawn()
{
	mTimeToRespawn = Timing::get_global_ptr()->get_frame_start_time() + mRespawnClientObjectsDelay;
}

/**
 *
 */
INLINE float ClientProxy::get_last_packet_from_client_time() const
{
	return mLastPacketFromClientTime;
}

/**
 *
 */
INLINE const MoveList& ClientProxy::get_unprocessed_move_list() const
{
	return mUnprocessedMoveList;
}

/**
 *
 */
INLINE MoveList& ClientProxy::get_unprocessed_move_list()
{
	return mUnprocessedMoveList;
}

/**
 *
 */
INLINE void ClientProxy::set_last_move_timestamp_dirty(bool inIsDirty)
{
	mIsLastMoveTimestampDirty = inIsDirty;
}

/**
 *
 */
INLINE bool ClientProxy::get_last_move_timestamp_dirty() const
{
	return mIsLastMoveTimestampDirty;
}

/**
 *
 */
INLINE DeliveryNotificationManager& ClientProxy::get_delivery_notification_manager()
{
	return mDeliveryNotificationManager;
}

/**
 *
 */
INLINE ReplicationManagerServer& ClientProxy::get_replication_manager_server()
{
	return mReplicationManagerServer;
}

#endif /* NETWORK_SOURCE_CLIENTPROXY_I_ */
