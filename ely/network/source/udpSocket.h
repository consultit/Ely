/**
 * \file udpSocket.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_UDPSOCKET_H_
#define NETWORK_SOURCE_UDPSOCKET_H_

#include "network_includes.h"

#include "socketAddress.h"
#include "typedReferenceCount.h"
#include "pointerTo.h"

/**
 * UDPSocket class.
 */
class EXPCL_NETWORK UDPSocket: public TypedReferenceCount
{
PUBLISHED:

	INLINE ~UDPSocket();
	int bind_at(const SocketAddress& inToAddress);
	int send_to(const void* inToSend, int inLength,
			const SocketAddress& inToAddress);
	int receive_from(void* inToReceive, int inMaxLength,
			SocketAddress& outFromAddress);
	int set_non_blocking_mode(bool inShouldBeNonBlocking);
	int set_socket_opt(int level, int optname, const char *optval, int optlen);
	int get_socket_opt(int level, int optname, char *optval, int *optlen) const;
	// Python Properties

public:
	UDPSocket() = delete;

#ifndef CPPPARSER
private:
	friend class SocketUtil;
	inline explicit UDPSocket(SOCKET inSocket);
	SOCKET mSocket;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(UDPSocket,TypedReferenceCount)
};

///inline
#include "udpSocket.I"

#endif /* NETWORK_SOURCE_UDPSOCKET_H_ */
