#include "config_module.h"
#include "dconfig.h"

#include "clientProxy.h"
#include "gameNetworkManager.h"
#include "gameNetworkManagerClient.h"
#include "gameNetworkManagerServer.h"
#include "inputManager.h"
#include "networkObject.h"
#include "networkObjectRegistry.h"
#include "networkWorld.h"
#include "replicationManagerTransmissionData.h"
#include "socketAddress.h"
#include "tcpSocket.h"
#include "transmissionData.h"
#include "udpSocket.h"
#include "networkTools.h"

Configure(config_p3network);
NotifyCategoryDef(p3network, "");

ConfigureFn( config_p3network )
{
	init_libp3network();
}

void init_libp3network()
{
	static bool initialized = false;
	if (initialized)
	{
		return;
	}
	initialized = true;

	// Init your dynamic types here, e.g.:
	// MyDynamicClass::init_type();
	ClientProxy::init_type();
	InputManager::init_type();
	GameNetworkManager::init_type();
	GameNetworkManagerClient::init_type();
	GameNetworkManagerServer::init_type();
	NetworkObject::init_type();
	NetworkObjectRegistry::init_type();
	ReplicationManagerTransmissionData::init_type();
	SocketAddress::init_type();
	TCPSocket::init_type();
	TransmissionData::init_type();
	UDPSocket::init_type();
	NetworkWorld::init_type();
	NetBitMask::init_type();
	ValueList_ClientProxyPtr::init_type();
	ValueList_NetworkObjectPtr::init_type();
	ValueList_TCPSocketPtr::init_type();

	return;
}

