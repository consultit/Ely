/**
 * \file networkObject.cxx
 *
 * \date 2018-07-22
 * \author consultit
 */

#include "networkObject.h"
#include "networkTools.h"
#ifdef PYTHON_BUILD
#include "py_panda.h"
extern Dtool_PyTypedObject Dtool_NetworkObject;
extern Dtool_PyTypedObject Dtool_NetBitMask;
#endif //PYTHON_BUILD

///NetworkObject definitions

/**
 *
 */
NetworkObject::NetworkObject(uint32_t inCode) :
		kClassId(inCode)
{
	do_reset();
	do_init();
}

/**
 *
 */
NetworkObject::NetworkObject() :
		kClassId(get_class_id_int("GOBJ"))
{
	do_reset();
	do_init();
}

/**
 *
 */
NetworkObject::~NetworkObject()
{
	if (!mNodePath.is_empty())
	{
		mNodePath.remove_node();
	}
#ifdef PYTHON_BUILD
	//Python callback
	PY_SELF_ARG_DECREF(mUpdateCallback, mSelf);
	PY_SELF_ARG_DECREF(mAllStateMaskClbk, mSelf);
	PY_SELF_ARG_DECREF(mHandleDyingClbk, mSelf);
	PY_SELF_ARG_DECREF(mPreWriteClbk, mSelf);
	PY_SELF_ARG_DECREF(mPostWriteClbk, mSelf);
	PY_SELF_ARG_DECREF(mPreReadClbk, mSelf);
	PY_SELF_ARG_DECREF(mPostReadClbk, mSelf);
#endif //PYTHON_BUILD
	do_reset();
}

string NetworkObject::get_class_id_str(uint32_t classId)
{
	string classIdStr(4, '\0');
	for (int l = 0; l < 4; l++)
	{
		classIdStr[l] = (char)((classId >> 8 * (3 - l)) & 0x000000FF);
	}
	return classIdStr;
}
uint32_t NetworkObject::get_class_id_int(const string& classIdStr)
{
	uint32_t classIdInt = 0;
	for (int l = 0; l < 4; l++)
	{
		classIdInt |= ((uint32_t)classIdStr[l] & 0x000000FF) << 8 * (3 - l);
	}
	return classIdInt;
}

/**
 *
 */
void NetworkObject::update()
{
	//
	if (mUpdateCallback)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_CALL(mUpdateCallback, mSelf,
				mNodePath.get_name() + " NetworkObject::update()");
#else
		// call c++ callback
		mUpdateCallback(this);
#endif //PYTHON_BUILD
	}
}

/**
 *
 */
uint32_t NetworkObject::get_all_state_mask()
{
	uint32_t result = 0;
	if (mAllStateMaskClbk)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PyObject *resultObj;
		resultObj = PyObject_CallFunctionObjArgs(mAllStateMaskClbk, mSelf,
				nullptr);
		if (!resultObj)
		{
			string errStr =
					mNodePath.get_name()
							+ string(
									"::NetworkObject::get_all_state_mask(): Error calling '")
							+ get_py_object_name(mAllStateMaskClbk)
							+ string("' callback function - ")
							+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
			PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
			PRINT_ERR(errStr);
#endif
		}
		else
		{
			result =
					static_cast<uint32_t>(PyNumber_AsSsize_t(resultObj, nullptr));
			Py_DECREF(resultObj);
		}
#else
		// call c++ callback
		result = mAllStateMaskClbk(this);
#endif //PYTHON_BUILD
	}
	return result;
}

/**
 *
 */
void NetworkObject::handle_dying()
{
	if (mHandleDyingClbk)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_CALL(mHandleDyingClbk, mSelf,
				mNodePath.get_name() + " NetworkObject::handle_dying()");
#else
		// call c++ callback
		mHandleDyingClbk(this);
#endif //PYTHON_BUILD
	}
}

/**
 *
 */
uint32_t NetworkObject::write(OutputMemoryBitStream& inOutputStream,
		const BitMask32& inDirtyState)
{
	if(mPreWriteClbk)
	{
#ifdef PYTHON_BUILD
		NetBitMask mask(inDirtyState);
		PY_CALLBACK_SELF_ONE_OTHER_ARGS_CALL(mPreWriteClbk, mask, Dtool_NetBitMask,
				mSelf,	mNodePath.get_name() + " Pre - NetworkObject::write()");
#else
		mPreWriteClbk(this, inDirtyState);
#endif //PYTHON_BUILD
	}
	mMembers.write(inOutputStream, inDirtyState);
	if(mPostWriteClbk)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_CALL(mPostWriteClbk, mSelf,
				mNodePath.get_name() + " Post - NetworkObject::write()");
#else
		// call c++ callback
		mPostWriteClbk(this);
#endif //PYTHON_BUILD
	}
	return inDirtyState.get_word();
}

/**
 *
 */
void NetworkObject::read(InputMemoryBitStream& inInputStream)
{
	if (mPreReadClbk)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_CALL(mPreReadClbk, mSelf,
				mNodePath.get_name() + " Pre - NetworkObject::read()");
#else
		// call c++ callback
		mPreReadClbk(this);
#endif //PYTHON_BUILD
	}
	NetBitMask mask(static_cast<BitMask32>(mMembers.read(inInputStream)));
	if (mPostReadClbk)
	{
#ifdef PYTHON_BUILD
		PY_CALLBACK_SELF_ONE_OTHER_ARGS_CALL(mPostReadClbk, mask, Dtool_NetBitMask,
				mSelf, mNodePath.get_name() + " Post - NetworkObject::read()");
#else
		mPostReadClbk(this, mask);
#endif //PYTHON_BUILD
	}
}

#ifdef PYTHON_BUILD
/**
 *
 */
void NetworkObject::set_update_callback(PyObject* clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mUpdateCallback, clbk, Dtool_NetworkObject,
			mNodePath.get_name() + " NetworkObject.set_update_callback()",
			mSelf, this);
}

/**
 *
 */
void NetworkObject::set_all_state_mask_callback(PyObject* clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mAllStateMaskClbk, clbk, Dtool_NetworkObject,
			mNodePath.get_name() + " NetworkObject.set_all_state_mask_callback()",
			mSelf, this);
}

/**
 *
 */
void NetworkObject::set_handle_dying_callback(PyObject* clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY( mHandleDyingClbk, clbk, Dtool_NetworkObject,
			mNodePath.get_name() + " NetworkObject.set_handle_dying_callback()",
			mSelf, this);
}

/**
 *
 */
void NetworkObject::set_pre_write_callback(PyObject* clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mPreWriteClbk, clbk, Dtool_NetworkObject,
			mNodePath.get_name() + " NetworkObject.set_pre_write_callback()",
			mSelf, this);
}

/**
 *
 */
void NetworkObject::set_post_write_callback(PyObject* clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mPostWriteClbk, clbk, Dtool_NetworkObject,
			mNodePath.get_name() + " NetworkObject.set_post_write_callback()",
			mSelf, this);
}

/**
 *
 */
void NetworkObject::set_pre_read_callback(PyObject* clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mPreReadClbk, clbk, Dtool_NetworkObject,
			mNodePath.get_name() + " NetworkObject.set_pre_read_callback()",
			mSelf, this);
}

/**
 *
 */
void NetworkObject::set_post_read_callback(PyObject* clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mPostReadClbk, clbk, Dtool_NetworkObject,
			mNodePath.get_name() + " NetworkObject.set_post_read_callback()",
			mSelf, this);
}
#else
/**
 *
 */
void NetworkObject::set_update_callback(UpdateCallback clbk)
{
	mUpdateCallback = clbk;
}

/**
 *
 */
void NetworkObject::set_all_state_mask_callback(AllStateMaskCallback clbk)
{
	mAllStateMaskClbk = clbk;
}

/**
 *
 */
void NetworkObject::set_handle_dying_callback(HandleDyingCallback clbk)
{
	mHandleDyingClbk = clbk;
}

/**
 *
 */
void NetworkObject::set_pre_write_callback(PreWriteCallback clbk)
{
	mPreWriteClbk = clbk;
}

/**
 *
 */
void NetworkObject::set_post_write_callback(PostWriteCallback clbk)
{
	mPostWriteClbk = clbk;
}

/**
 *
 */
void NetworkObject::set_pre_read_callback(PreReadCallback clbk)
{
	mPreReadClbk = clbk;
}

/**
 *
 */
void NetworkObject::set_post_read_callback(PostReadCallback clbk)
{
	mPostReadClbk = clbk;
}
#endif //PYTHON_BUILD

TYPED_OBJECT_API_DEF(NetworkObject)
