/**
 * \file weightedTimedMovingAverage.h
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_WEIGHTEDTIMEDMOVINGAVERAGE_H_
#define NETWORK_SOURCE_WEIGHTEDTIMEDMOVINGAVERAGE_H_

#include "network_includes.h"

#include "timing.h"

/**
 * WeightedTimedMovingAverage class.
 */
class EXPCL_NETWORK WeightedTimedMovingAverage
{
PUBLISHED:

	INLINE explicit WeightedTimedMovingAverage(float inDuration);
	INLINE void update_per_second(float inValue);
	INLINE void update(float inValue);
	INLINE float get_value() const;
	// Python Properties
	MAKE_PROPERTY(value, get_value);

public:
	WeightedTimedMovingAverage() = delete;

#ifndef CPPPARSER
private:
	float mTimeLastEntryMade;
	float mValue;
	float mDuration;
#endif //CPPPARSER
};

///inline
#include "weightedTimedMovingAverage.I"

#endif /* NETWORK_SOURCE_WEIGHTEDTIMEDMOVINGAVERAGE_H_ */
