/**
 * \file dataVariables.h
 *
 * \date 2018-08-20
 * \author consultit
 */

#ifndef NETWORK_SOURCE_DATAVARIABLES_H_
#define NETWORK_SOURCE_DATAVARIABLES_H_

#include "tools_includes.h"
#include "memoryBitStream.h"
#include <unordered_map>

///Data variables

#define DATAVARIABLESEXPAND \
EXPAND(Int8, int8_t)\
EXPAND(Int16, int16_t)\
EXPAND(Int32, int32_t)\
EXPAND(Int64, int64_t)\
EXPAND(UInt8, uint8_t)\
EXPAND(UInt16, uint16_t)\
EXPAND(UInt32, uint32_t)\
EXPAND(UInt64, uint64_t)\
EXPAND(Float, float)\
EXPAND(Double, double)\
EXPAND(Bool, bool)\
EXPAND(String, string)\
EXPAND(LVecBase3f, lvecbase3f)\
EXPAND(LPoint3f, lpoint3f)\
EXPAND(LVector3f, lvector3f)\
EXPAND(LVecBase4f, lvecbase4f)\
EXPAND(LQuaternionf, lquaternionf)

/**
 * DataVariables class.
 *
 * An object of this class represents a set of data variables, such as those
 * constituted by the members of a class, and which can be written/read
 * (serialized) to/from I/O streams.
 * Each variable consists of a reference to a content of a given type, together
 * with other meta-information useful for the management of the same.
 * Furthermore, a variable may be 'owned', in which case the object is
 * responsible for the creation/destruction of its storage, or 'free', in which
 * case the responsibility of its life time is elsewhere.
 */
class EXPCL_NETWORK DataVariables
{
PUBLISHED:
	enum EPrimitiveType
	{
#define EXPAND(type, dummy) \
		EPT_##type,\

		DATAVARIABLESEXPAND
#undef EXPAND
	};

	INLINE DataVariables();
	INLINE ~DataVariables();
	void add_variable(const string& inName, EPrimitiveType inPrimitiveType);
	INLINE void clear();
#define EXPAND(type, typesuffix) \
	void add_variable_##typesuffix(const string& inName, type* value);\

	DATAVARIABLESEXPAND
#undef EXPAND
	// Set/Get
#define EXPAND(type, typesuffix) \
	INLINE void set_value_##typesuffix(const string& inName, const type& value);\
	INLINE void get_value_##typesuffix(const string& inName, type& value) const;\

	DATAVARIABLESEXPAND
#undef EXPAND
	INLINE void set_dirty_state(const string& inName, const BitMask32& inMask);
	INLINE const BitMask32& get_dirty_state(const string& inName);
	void write(OutputMemoryBitStream& inOutputStream,
			const BitMask32& inDirtyState = BitMask32::all_on()) const;
	BitMask32 read(InputMemoryBitStream& inInputStream);
	// Python Properties

public:
	explicit DataVariables(const DataVariables& right);
	DataVariables(const DataVariables&& right) = delete;
	DataVariables& operator=(const DataVariables& right) = delete;
	DataVariables& operator=(const DataVariables&& right) = delete;

private:
	struct Variable
	{
		explicit Variable(const string& _name, EPrimitiveType _prim,
				void* _content, bool _owned) :
				name(_name), prim(_prim), content(_content), owned(_owned), dirtyState(
						BitMask32::all_on())
		{
		}
		Variable(const Variable&) = delete;
		Variable(Variable&& other)
		{
			name = move(other.name);
			prim = other.prim;
			content = other.content;
			owned = other.owned;
			dirtyState = other.dirtyState;
			other.content = nullptr;
			other.owned = false;
		}
		~Variable()
		{
		}
		Variable& operator=(const Variable&) = delete;
		Variable& operator=(Variable&&) = delete;
		string name;
		EPrimitiveType prim;
		void* content;
		bool owned;
		BitMask32 dirtyState;
	};
	pvector<Variable> mVariables;
	void do_delete_variables();
	/// to speed up search
	unordered_map<string, size_t> mIndex;
	/// to speed up data handling
	struct DataHandlers
	{
		void (DataVariables::*setter)(void*, void*);
		void (DataVariables::*deleter)(void*);
		void (DataVariables::*writer)(OutputMemoryBitStream&, void*) const;
		void (DataVariables::*reader)(InputMemoryBitStream&, void*);
	};
	unordered_map<EPrimitiveType, DataHandlers> mDataHandlers;
	template<typename T> void do_set_var(void* content, void* valuePtr)
	{
		*static_cast<T*>(content) = *static_cast<T*>(valuePtr);
	}
	template<typename T> void do_delete_var(void* content)
	{
		delete static_cast<T*>(content);
	}
	template<typename T> void do_write_var(OutputMemoryBitStream& inOutputStream,
			void* content) const
	{
		inOutputStream.write(*static_cast<T*>(content));
	}
	template<typename T> void do_read_var(InputMemoryBitStream& inInputStream,
			void* content)
	{
		inInputStream.read(*static_cast<T*>(content));
	}
};

///inline
#include "dataVariables.I"

#endif /* NETWORK_SOURCE_DATAVARIABLES_H_ */
