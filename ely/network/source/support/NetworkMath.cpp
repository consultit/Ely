//
//  RoboMath.cpp
//  RoboCat
//
//  Created by Joshua Glazer on 6/13/15.
//  Copyright (c) 2015 com.JoshuaGlazer.Book. All rights reserved.
//

#include "NetworkMath.h"

#include <random>

using std::random_device;
using std::mt19937;
using std::uniform_real_distribution;

float Math::GetRandomFloat()
{
	static random_device rd;
	static mt19937 gen(rd());
	static uniform_real_distribution<float> dis(0.f, 1.f);
	return dis(gen);
}

LVecBase3f Math::GetRandomVector(const LVecBase3f& inMin,
		const LVecBase3f& inMax)
{
	LVecBase3f r = LVecBase3f(GetRandomFloat(), GetRandomFloat(),
			GetRandomFloat());
	LVecBase3f d = inMax - inMin;
	d.componentwise_mult(r);
	return inMin + d;
}
