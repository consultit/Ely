#ifndef Net_StringUtils_h
#define Net_StringUtils_h

#include <string>

using std::string;

namespace StringUtils
{

string Sprintf(const char* inFormat, ...);

void Log(const char* inFormat);
void Log(const char* inFormat, ...);
}

#define LOG( ... ) StringUtils::Log( __VA_ARGS__ );

#endif //Net_StringUtils_h
