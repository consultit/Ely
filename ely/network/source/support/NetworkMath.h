#ifndef Net_NetworkMath_h
#define Net_NetworkMath_h

#include <cmath>
#include <lvecBase3.h>
#include <lvecBase4.h>

template<typename T, typename F> inline T Lerp(const T& inA, const T& inB, F t)
{
	return inA + (inB - inA) * t;
}

template<int tValue, int tBits>
struct GetRequiredBitsHelper
{
	enum
	{
		Value = GetRequiredBitsHelper<(tValue >> 1), tBits + 1>::Value
	};
};

template<int tBits>
struct GetRequiredBitsHelper<0, tBits>
{
	enum
	{
		Value = tBits
	};
};

template<int tValue>
struct GetRequiredBits
{
	enum
	{
		Value = GetRequiredBitsHelper<tValue, 0>::Value
	};
};

namespace Math
{
const float PI = 3.1415926535f;
float GetRandomFloat();

LVecBase3f GetRandomVector(const LVecBase3f& inMin, const LVecBase3f& inMax);

inline bool Is2DVectorEqual(const LVecBase3f& inA, const LVecBase3f& inB)
{
	return (inA == inB);
}

inline float ToDegrees(float inRadians)
{
	return inRadians * 180.0f / PI;
}
}

namespace Colors
{
static const LVecBase4f Black(0.0f, 0.0f, 0.0f, 1.0f);
static const LVecBase4f White(1.0f, 1.0f, 1.0f, 1.0f);
static const LVecBase4f Red(1.0f, 0.0f, 0.0f, 1.0f);
static const LVecBase4f Green(0.0f, 1.0f, 0.0f, 1.0f);
static const LVecBase4f Blue(0.0f, 0.0f, 1.0f, 1.0f);
static const LVecBase4f LightYellow(1.0f, 1.0f, 0.88f, 1.0f);
static const LVecBase4f LightBlue(0.68f, 0.85f, 0.9f, 1.0f);
static const LVecBase4f LightPink(1.0f, 0.71f, 0.76f, 1.0f);
static const LVecBase4f LightGreen(0.56f, 0.93f, 0.56f, 1.0f);
}

#endif //Net_NetworkMath_h

