/**
 * \file networksymbols.h
 *
 * \date 2018-10-03
 * \author consultit
 */
#ifndef NETWORK_SOURCE_NETWORKSYMBOLS_H_
#define NETWORK_SOURCE_NETWORKSYMBOLS_H_

#ifdef PYTHON_BUILD_network
#	define EXPCL_NETWORK EXPORT_CLASS
#	define EXPTP_NETWORK EXPORT_TEMPL
#else
#	define EXPCL_NETWORK IMPORT_CLASS
#	define EXPTP_NETWORK IMPORT_TEMPL
#endif

#endif /* NETWORK_SOURCE_NETWORKSYMBOLS_H_ */
