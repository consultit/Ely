/**
 * \file networkManager.I
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "gameNetworkManager.h"

#include "socketUtil.h"

#include "support/StringUtils.h"
#include "support/NetworkMath.h"

///GameNetworkManager definitions

/**
 *
 */
bool GameNetworkManager::init(uint16_t inPort)
{
	mSocket = SocketUtil::create_udp_socket(INET);
	SocketAddress ownAddress( INADDR_ANY, inPort);
	mSocket->bind_at(ownAddress);

	LOG("Initializing GameNetworkManager at port %d", inPort)

	mBytesReceivedPerSecond = WeightedTimedMovingAverage(1.f);
	mBytesSentPerSecond = WeightedTimedMovingAverage(1.f);

	//did we bind okay?
	if (mSocket == nullptr)
	{
		return false;
	}

	if (mSocket->set_non_blocking_mode(true) != NO_ERROR)
	{
		return false;
	}

	return true;
}

/**
 *
 */
void GameNetworkManager::process_incoming_packets()
{
	do_read_incoming_packets_into_queue();

	do_process_queued_packets();

	do_update_bytes_sent_last_frame();

}

/**
 *
 */
void GameNetworkManager::do_read_incoming_packets_into_queue()
{
	//should we just keep a static one?
	char packetMem[1500];
	int packetSize = sizeof(packetMem);
	InputMemoryBitStream inputStream(packetMem, packetSize * 8);
	SocketAddress fromAddress;

	//keep reading until we don't have anything to read ( or we hit a max number that we'll process per frame )
	int receivedPackedCount = 0;
	int totalReadByteCount = 0;

	while (receivedPackedCount < kMaxPacketsPerFrameCount)
	{
		int readByteCount = mSocket->receive_from(packetMem, packetSize,
				fromAddress);
		if (readByteCount == 0)
		{
			//nothing to read
			break;
		}
		else if (readByteCount == -WSAECONNRESET)
		{
			//port closed on other end, so DC this person immediately
			handle_connection_reset(fromAddress);
		}
		else if (readByteCount > 0)
		{
			inputStream.reset_to_capacity(readByteCount);
			++receivedPackedCount;
			totalReadByteCount += readByteCount;

#ifdef ELY_DEBUG
			//now, should we drop the packet?
			if (Math::GetRandomFloat() >= mDropPacketChance)
			{
				//we made it
				//shove the packet into the queue and we'll handle it as soon as we should...
				//we'll pretend it wasn't received until simulated latency from now
				//this doesn't sim jitter, for that we would need to.....

				//we made it, queue packet for later processing
				float simulatedReceivedTime = Timing::get_global_ptr()->get_timef()
						+ mSimulatedLatency
						+ (Math::GetRandomFloat() - 0.5f)
								* mDoubleSimulatedMaxJitter;
				//keep list sorted by simulated receive time
				auto it = mPacketQueue.end();
				while (it != mPacketQueue.begin())
				{
					--it;
					if (it->get_received_time() < simulatedReceivedTime)
					{
						//time comes after this element, so inc and break
						++it;
						break;
					}
				}

				mPacketQueue.emplace(it, simulatedReceivedTime, inputStream,
						fromAddress);
			}
			else
			{
				LOG("Dropped packet!", 0)
				//dropped!
			}
#else //ELY_DEBUG
			mPacketQueue.emplace_back( Timing::get_global_ptr()->get_timef(), inputStream, fromAddress );
#endif //ELY_DEBUG
		}
		else
		{
			//uhoh, error? exit or just keep going?
		}
	}

	if (totalReadByteCount > 0)
	{
		mBytesReceivedPerSecond.update_per_second(
				static_cast<float>(totalReadByteCount));
	}
}

/**
 *
 */
void GameNetworkManager::do_process_queued_packets()
{
	//look at the front packet...
	while (!mPacketQueue.empty())
	{
		ReceivedPacket& nextPacket = mPacketQueue.front();
#ifdef ELY_DEBUG
		if (Timing::get_global_ptr()->get_timef() > nextPacket.get_received_time())
		{
#endif //ELY_DEBUG
			process_packet(nextPacket.get_packet_buffer(),
					nextPacket.get_from_address());
			mPacketQueue.pop_front();
#ifdef ELY_DEBUG
		}
		else
		{
			break;
		}
#endif //ELY_DEBUG
	}
}

/**
 *
 */
void GameNetworkManager::send_packet(const OutputMemoryBitStream& inOutputStream,
		const SocketAddress& inFromAddress)
{
	int sentByteCount = mSocket->send_to(inOutputStream.get_buffer_ptr(),
			inOutputStream.get_byte_length(), inFromAddress);
	if (sentByteCount > 0)
	{
		mBytesSentThisFrame += sentByteCount;
	}
}

/**
 *
 */
void GameNetworkManager::do_update_bytes_sent_last_frame()
{
	if (mBytesSentThisFrame > 0)
	{
		mBytesSentPerSecond.update_per_second(
				static_cast<float>(mBytesSentThisFrame));

		mBytesSentThisFrame = 0;
	}

}

/**
 *
 */
GameNetworkManager::ReceivedPacket::ReceivedPacket(float inReceivedTime,
		InputMemoryBitStream& ioInputMemoryBitStream,
		const SocketAddress& inFromAddress) :
		mReceivedTime(inReceivedTime), mFromAddress(inFromAddress), mPacketBuffer(
				ioInputMemoryBitStream)
{
}

TYPED_OBJECT_API_DEF(GameNetworkManager)
