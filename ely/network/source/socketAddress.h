/**
 * \file socketAddress.h
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_SOCKETADDRESS_H_
#define NETWORK_SOURCE_SOCKETADDRESS_H_

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

#include "Windows.h"
#include "WinSock2.h"
#include "Ws2tcpip.h"
typedef int socklen_t;
//typedef char* receiveBufer_t;
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
//typedef void* receiveBufer_t;
typedef int SOCKET;
const int NO_ERROR = 0;
const int INVALID_SOCKET = -1;
const int WSAECONNRESET = ECONNRESET;
const int WSAEWOULDBLOCK = EAGAIN;
const int SOCKET_ERROR = -1;
#endif

#include <cstring>
#include <string>

///SocketAddress & SocketAddressFactory
#include "network_includes.h"

#include "typedReferenceCount.h"
#include "pointerTo.h"

using std::string;

/**
 * SocketAddress class.
 */
class EXPCL_NETWORK SocketAddress: public TypedReferenceCount
{
PUBLISHED:

	INLINE SocketAddress();
	INLINE SocketAddress(uint32_t inAddress, uint16_t inPort);
	INLINE SocketAddress(const sockaddr& inSockAddr);
	INLINE virtual ~SocketAddress();
	INLINE bool operator==(const SocketAddress& inOther) const;
	INLINE size_t get_hash() const;
	INLINE uint32_t get_size() const;
	string get_to_string() const;
	// Python Properties
	MAKE_PROPERTY(hash, get_hash);
	MAKE_PROPERTY(size, get_size);
	MAKE_PROPERTY(to_string, get_to_string);

#ifndef CPPPARSER
private:
	friend class UDPSocket;
	friend class TCPSocket;

	sockaddr mSockAddr;
#if _WIN32
	inline uint32_t& do_get_ipv4_ref();
	inline const uint32_t& do_get_ipv4_ref() const;
#else
	inline uint32_t& do_get_ipv4_ref();
	inline const uint32_t& do_get_ipv4_ref() const;
#endif
	inline sockaddr_in* do_get_as_sock_addr_in();
	inline const sockaddr_in* do_get_as_sock_addr_in() const;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(SocketAddress,TypedReferenceCount)
};

namespace std
{
template<> struct hash<SocketAddress>
{
	size_t operator()(const SocketAddress& inAddress) const
	{
		return inAddress.get_hash();
	}
};
} // namespace std

/**
 * SocketAddressFactory class.
 */
class EXPCL_NETWORK SocketAddressFactory
{
PUBLISHED:

	static PT(SocketAddress) create_ipv4_from_string(const string& inString);
	// Python Properties
};

///inline
#include "socketAddress.I"

#endif /* NETWORK_SOURCE_SOCKETADDRESS_H_ */

