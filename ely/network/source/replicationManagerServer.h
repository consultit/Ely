/**
 * \file replicationManagerServer.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_REPLICATIONMANAGERSERVER_H_
#define NETWORK_SOURCE_REPLICATIONMANAGERSERVER_H_

#include "network_includes.h"

#include "memoryBitStream.h"
#include "replicationCommand.h"
#include <unordered_map>

using std::unordered_map;

/**
 * ReplicationManagerServer class.
 */
class EXPCL_NETWORK ReplicationManagerServer
{
PUBLISHED:

	INLINE ReplicationManagerServer();
	void replicate_create(int inNetworkId, uint32_t inInitialDirtyState);
	void replicate_destroy(int inNetworkId);
	void set_state_dirty(int inNetworkId, uint32_t inDirtyState);
	void handle_create_ackd(int inNetworkId);
	void remove_from_replication(int inNetworkId);
	void write(OutputMemoryBitStream& inOutputStream,
			ReplicationManagerTransmissionData* ioTransmissinData);
	// Python Properties

#ifndef CPPPARSER
private:
	uint32_t do_write_create_action(OutputMemoryBitStream& inOutputStream,
			int inNetworkId, uint32_t inDirtyState);
	uint32_t do_write_update_action(OutputMemoryBitStream& inOutputStream,
			int inNetworkId, uint32_t inDirtyState);
	uint32_t do_write_destroy_action(OutputMemoryBitStream& inOutputStream,
			int inNetworkId, uint32_t inDirtyState);
	unordered_map<int, ReplicationCommand> mNetworkIdToReplicationCommand;
	pvector<int> mNetworkIdsToRemove;
#endif //CPPPARSER
};

///inline
#include "replicationManagerServer.I"

#endif /* NETWORK_SOURCE_REPLICATIONMANAGERSERVER_H_ */
