/**
 * \file moveList.cxx
 *
 * \date 2018-07-22
 * \author consultit
 */

#include "moveList.h"

///MoveList definitions

/**
 *
 */
const Move& MoveList::add_move(const InputState& inInputState, float inTimestamp)
{
	//first move has 0 time. it's okay, it only happens once
	float deltaTime =
			mLastMoveTimestamp >= 0.f ? inTimestamp - mLastMoveTimestamp : 0.f;

	mMoves.emplace_back(inInputState, inTimestamp, deltaTime);

	mLastMoveTimestamp = inTimestamp;

	return mMoves.back();
}

/**
 *
 */
bool MoveList::add_move_if_new(const Move& inMove)
{
	//we might have already received this move in another packet ( since we're sending the same move in multiple packets )
	//so make sure it's new...

	//adjust the deltatime and then place!
	float timeStamp = inMove.get_timestamp();

	if (timeStamp > mLastMoveTimestamp)
	{
		float deltaTime =
				mLastMoveTimestamp >= 0.f ?
						timeStamp - mLastMoveTimestamp : 0.f;

		mLastMoveTimestamp = timeStamp;

		mMoves.emplace_back(inMove.get_input_state(), timeStamp, deltaTime);
		return true;
	}

	return false;
}

/**
 *
 */
void MoveList::removed_processed_moves(float inLastMoveProcessedOnServerTimestamp)
{
	while (!mMoves.empty()
			&& mMoves.front().get_timestamp()
					<= inLastMoveProcessedOnServerTimestamp)
	{
		mMoves.pop_front();
	}
}
