/**
 * \file clientProxy.cxx
 *
 * \date 2018-07-22
 * \author consultit
 */

#include "clientProxy.h"
#include "timing.h"
#ifdef PYTHON_BUILD
#include "py_panda.h"
#endif //PYTHON_BUILD

///ClientProxy definitions

/**
 *
 */
ClientProxy::ClientProxy(const SocketAddress& inSocketAddress,
		const string& inName, int inPlayerId) :
		mSocketAddress(inSocketAddress), mName(inName), mPlayerId(inPlayerId), mDeliveryNotificationManager(
				false, true), mIsLastMoveTimestampDirty(false), mTimeToRespawn(
				0.f)
{
	mRespawnClientObjectsClbk = nullptr;
	update_last_packet_time();
}

/**
 *
 */
ClientProxy::~ClientProxy()
{
#ifdef PYTHON_BUILD
	//Python callback
	Py_XDECREF(mRespawnClientObjectsClbk);
#endif //PYTHON_BUILD
	mRespawnClientObjectsClbk = nullptr;
}

/**
 *
 */
void ClientProxy::respawn_objects_if_necessary()
{
	if (mTimeToRespawn != 0.f
			&& Timing::get_global_ptr()->get_frame_start_time() > mTimeToRespawn)
	{
		if (mRespawnClientObjectsClbk)
		{
#ifdef PYTHON_BUILD
			PyObject *argList, *result;
			// set the arguments
			argList = Py_BuildValue("(i)", mPlayerId);
			if (!argList)
			{
				string errStr =
						"Error: cannot call the 'respawn client object' callback - "
								+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
				PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
				PRINT_ERR(errStr);
#endif
				return;
			}
			// call the callback
			result = PyObject_CallObject(mRespawnClientObjectsClbk, argList);
			Py_DECREF(argList);
			if (!result)
			{
				string errStr =
						string(
								"Error calling 'respawn client object' callback function - ")
								+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
				PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
				PRINT_ERR(errStr);
#endif
				return;
			}
			Py_DECREF(result);
#else
			mRespawnClientObjectsClbk(mPlayerId);
#endif //PYTHON_BUILD
		}
		mTimeToRespawn = 0.f;
	}
}

TYPED_OBJECT_API_DEF(ClientProxy)
