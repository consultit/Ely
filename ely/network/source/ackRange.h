/**
 * \file ackRange.h
 *
 * \date 2018-07-24
 * \author consultit
 */

#ifndef NETWORK_SOURCE_ACKRANGE_H_
#define NETWORK_SOURCE_ACKRANGE_H_

#include "network_includes.h"

#include "memoryBitStream.h"
#include "inFlightPacket.h"

/**
 * AckRange class.
 */
class EXPCL_NETWORK AckRange
{
PUBLISHED:

	INLINE AckRange();
	INLINE AckRange(PacketSequenceNumber inStart);
	//if this is the next in sequence, just extend the range
	INLINE bool extend_if_should(PacketSequenceNumber inSequenceNumber);
	INLINE PacketSequenceNumber get_start() const;
	INLINE uint32_t get_count() const;
	void write(OutputMemoryBitStream& inOutputStream) const;
	void read(InputMemoryBitStream& inInputStream);
	// Python Properties
	MAKE_PROPERTY(start, get_start);
	MAKE_PROPERTY(count, get_count);

#ifndef CPPPARSER
private:
	PacketSequenceNumber mStart;
	uint32_t mCount;
#endif //CPPPARSER
};

///inline
#include "ackRange.I"

#endif /* NETWORK_SOURCE_ACKRANGE_H_ */
