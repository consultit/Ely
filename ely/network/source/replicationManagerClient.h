/**
 * \file replicationManagerClient.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_REPLICATIONMANAGERCLIENT_H_
#define NETWORK_SOURCE_REPLICATIONMANAGERCLIENT_H_

#include "network_includes.h"

#include "memoryBitStream.h"

/**
 * ReplicationManagerClient class.
 */
class EXPCL_NETWORK ReplicationManagerClient
{
PUBLISHED:

	INLINE ReplicationManagerClient();
	void read(InputMemoryBitStream& inInputStream);
	// Python Properties

#ifndef CPPPARSER
private:
	void do_read_and_do_create_action(InputMemoryBitStream& inInputStream,
			int inNetworkId);
	void do_read_and_do_update_action(InputMemoryBitStream& inInputStream,
			int inNetworkId);
	void do_read_and_do_destroy_action(InputMemoryBitStream& inInputStream,
			int inNetworkId);
#endif //CPPPARSER
};

///inline
#include "replicationManagerClient.I"

#endif /* NETWORK_SOURCE_REPLICATIONMANAGERCLIENT_H_ */
