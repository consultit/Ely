/**
 * \file socketUtil.I
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef NETWORK_SOURCE_SOCKETUTIL_I_
#define NETWORK_SOURCE_SOCKETUTIL_I_

///SocketUtil inline definitions

/**
 *
 */
INLINE SocketUtil::SocketUtil()
{
}

/**
 *
 */
inline fd_set* SocketUtil::do_fill_set_from_vector(fd_set& outSet,
		const ValueList_TCPSocketPtr* inSockets, int& ioNaxNfds)
{
	if (inSockets)
	{
		FD_ZERO(&outSet);

		for (int i = 0; i < (*inSockets).get_num_values(); i++)
		{
			const PT(TCPSocket)& socket = (*inSockets)[i];
			FD_SET(socket->mSocket, &outSet);
#if !_WIN32
			ioNaxNfds = max(ioNaxNfds, socket->mSocket);
#endif
		}
		return &outSet;
	}
	else
	{
		return nullptr;
	}
}

/**
 *
 */
inline void SocketUtil::do_fill_vector_from_set(
		ValueList_TCPSocketPtr* outSockets, const ValueList_TCPSocketPtr* inSockets,
		const fd_set& inSet)
{
	if (inSockets && outSockets)
	{
		outSockets->clear();
		for (int i = 0; i < (*inSockets).get_num_values(); i++)
		{
			const PT(TCPSocket)& socket = (*inSockets)[i];
			if (FD_ISSET(socket->mSocket, &inSet))
			{
				outSockets->add_value(socket);
			}
		}
	}
}

#endif /* NETWORK_SOURCE_SOCKETUTIL_I_ */
