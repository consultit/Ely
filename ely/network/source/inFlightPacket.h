/**
 * \file inFlightPacket.h
 *
 * \date 2018-07-24
 * \author consultit
 */

#ifndef NETWORK_SOURCE_INFLIGHTPACKET_H_
#define NETWORK_SOURCE_INFLIGHTPACKET_H_

#include "network_includes.h"

#include "transmissionData.h"
#include "timing.h"
#include <unordered_map>

class DeliveryNotificationManager;

using std::unordered_map;

//in case we decide to change the type of the sequence number to use fewer or more bits
typedef uint16_t PacketSequenceNumber;

/**
 * InFlightPacket class.
 */
class EXPCL_NETWORK InFlightPacket
{
PUBLISHED:

	INLINE explicit InFlightPacket(PacketSequenceNumber inSequenceNumber);
	INLINE PacketSequenceNumber get_sequence_number() const;
	INLINE float get_time_dispatched() const;
	INLINE void set_transmission_data(int inKey, PT(TransmissionData) inTransmissionData);
	INLINE const PT(TransmissionData) get_transmission_data(int inKey) const;
	void handle_delivery_failure(
			DeliveryNotificationManager* inDeliveryNotificationManager) const;
	void handle_delivery_success(
			DeliveryNotificationManager* inDeliveryNotificationManager) const;
	// Python Properties
	MAKE_PROPERTY(sequence_number, get_sequence_number);
	MAKE_PROPERTY(time_dispatched, get_time_dispatched);

public:
	InFlightPacket() = delete;

#ifndef CPPPARSER
private:
	PacketSequenceNumber mSequenceNumber;
	float mTimeDispatched;
	unordered_map<int, PT(TransmissionData)> mTransmissionDataMap;
#endif //CPPPARSER
};

///inline
#include "inFlightPacket.I"

#endif /* NETWORK_SOURCE_INFLIGHTPACKET_H_ */
