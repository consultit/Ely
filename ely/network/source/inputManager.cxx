/**
 * \file inputManager.cxx
 *
 * \date 2018-07-22
 * \author consultit
 */

#include "inputManager.h"

#include "gameNetworkManagerClient.h"
#include "support/StringUtils.h"

///InputManager definitions

/**
 *
 */
const Move& InputManager::do_sample_input_as_move()
{
	return mMoveList.add_move(*get_input_state(), Timing::get_global_ptr()->get_frame_start_time());
}

/**
 *
 */
bool InputManager::do_is_time_to_sample_input()
{
	float time = Timing::get_global_ptr()->get_frame_start_time();
	if (time > mNextTimeToSampleInput)
	{
		mNextTimeToSampleInput = mNextTimeToSampleInput
				+ mTimeBetweenInputSamples;
		return true;
	}

	return false;
}

/**
 *
 */
void InputManager::update()
{
	if (do_is_time_to_sample_input())
	{
		mPendingMove = &do_sample_input_as_move();
	}
}

TYPED_OBJECT_API_DEF(InputManager)

