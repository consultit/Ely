/**
 * \file inputState.I
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_INPUTSTATE_I_
#define NETWORK_SOURCE_INPUTSTATE_I_

///InputState inline definitions

/**
 *
 */
INLINE InputState::InputState()
{
}

/**
 *
 */
INLINE InputState::InputState(const InputState& right):
		mMembers(right.mMembers)
{
}

/**
 *
 */
INLINE InputState::~InputState()
{
}

/**
 *
 */
INLINE bool InputState::write(
		OutputMemoryBitStream& inOutputStream) const
{
	mMembers.write(inOutputStream);
	return true;
}

/**
 *
 */
INLINE bool InputState::read(InputMemoryBitStream& inInputStream)
{
	mMembers.read(inInputStream);
	return true;
}

/**
 *
 */
INLINE DataVariables& InputState::get_members()
{
	return mMembers;
}

/**
 *
 */
INLINE const DataVariables& InputState::get_members() const
{
	return mMembers;
}

///InputState inline definitions

/**
 *
 */
INLINE InputStateFactory::InputStateFactory()
{
}

/**
 *
 */
INLINE InputStateFactory::~InputStateFactory()
{
}

/**
 *
 */
INLINE InputStateFactory* InputStateFactory::get_global_ptr()
{
	return Singleton < InputStateFactory > ::GetSingletonPtr();
}

/**
 *
 */
INLINE InputState* InputStateFactory::get_input_state()
{
	return &mInputState;
}

#endif /* NETWORK_SOURCE_INPUTSTATE_I_ */
