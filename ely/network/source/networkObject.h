/**
 * \file networkObject.h
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef NETWORK_SOURCE_NETWORKOBJECT_H_
#define NETWORK_SOURCE_NETWORKOBJECT_H_

#include "network_includes.h"

#include "memoryBitStream.h"
#include "nodePath.h"
#include "collisionSphere.h"
#include "dataVariables.h"

#ifndef CPPPARSER
#include "support/NetworkMath.h"
#endif //CPPPARSER

#define CLASS_IDENTIFICATION \
	explicit NetworkObject(uint32_t inCode);\
	INLINE uint32_t get_class_id() const { return kClassId; }

/**
 * NetworkObject class.
 *
 *	\note Callbacks:
 *	- void (*UpdateCallback)(PT(NetworkObject));
 *	- uint32_t (*AllStateMaskCallback)(PT(NetworkObject));
 *	- void (*HandleDyingCallback)(PT(NetworkObject));
 *	- void (*PreWriteCallback)(PT(NetworkObject), const BitMask32&);
 *	- void (*PostWriteCallback)(PT(NetworkObject));
 *	- void (*PreReadCallback)(PT(NetworkObject));
 *	- void (*PostReadCallback)(PT(NetworkObject), const BitMask32&);
 */
class EXPCL_NETWORK NetworkObject: public TypedReferenceCount
{
public:
	typedef void (*UpdateCallback)(PT(NetworkObject));
	typedef uint32_t (*AllStateMaskCallback)(PT(NetworkObject));
	typedef void (*HandleDyingCallback)(PT(NetworkObject));
	typedef void (*PreWriteCallback)(PT(NetworkObject), const BitMask32&);
	typedef void (*PostWriteCallback)(PT(NetworkObject));
	typedef void (*PreReadCallback)(PT(NetworkObject));
	typedef void (*PostReadCallback)(PT(NetworkObject), const BitMask32&);

PUBLISHED:

	CLASS_IDENTIFICATION

	~NetworkObject();
	void update();
	uint32_t get_all_state_mask();
	void handle_dying();
	INLINE void set_owner_object(const NodePath& np);
	INLINE NodePath get_owner_object() const;
	INLINE void set_index_in_world(int inIndex);
	INLINE int get_index_in_world() const;
	INLINE void set_does_want_to_die(bool inWants);
	INLINE bool get_does_want_to_die() const;
	INLINE void set_network_id(int inNetworkId);
	INLINE int get_network_id() const;
	INLINE DataVariables& get_members();
	uint32_t write(OutputMemoryBitStream& inOutputStream,
			const BitMask32& inDirtyState);
	void read(InputMemoryBitStream& inInputStream);
    static string get_class_id_str(uint32_t classId);
    static uint32_t get_class_id_int(const string& classIdStr);
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	void set_update_callback(PyObject* clbk);
	void set_all_state_mask_callback(PyObject* clbk);
	void set_handle_dying_callback(PyObject* clbk);
	void set_pre_write_callback(PyObject* clbk);
	void set_post_write_callback(PyObject* clbk);
	void set_pre_read_callback(PyObject* clbk);
	void set_post_read_callback(PyObject* clbk);
#else
	void set_update_callback(UpdateCallback clbk);
	void set_all_state_mask_callback(AllStateMaskCallback clbk);
	void set_handle_dying_callback(HandleDyingCallback clbk);
	void set_pre_write_callback(PreWriteCallback clbk);
	void set_post_write_callback(PostWriteCallback clbk);
	void set_pre_read_callback(PreReadCallback clbk);
	void set_post_read_callback(PostReadCallback clbk);
#endif // PYTHON_BUILD
	// Python Properties
	MAKE_PROPERTY(owner_object, get_owner_object, set_owner_object);
	MAKE_PROPERTY(index_in_world, get_index_in_world, set_index_in_world);
	MAKE_PROPERTY(does_want_to_die, get_does_want_to_die, set_does_want_to_die);
	MAKE_PROPERTY(network_id, get_network_id, set_network_id);
	MAKE_PROPERTY(members, get_members);

public:
	NetworkObject();
	NetworkObject(const NetworkObject& right) = delete;
	NetworkObject(NetworkObject&& right) = delete;
	NetworkObject& operator=(const NetworkObject& right) = delete;
	NetworkObject& operator=(NetworkObject&& right) = delete;

#ifndef CPPPARSER
protected:
	NodePath mNodePath;
	NodePath mRender;
	uint32_t kClassId;
	DataVariables mMembers;
#if defined(PYTHON_BUILD)
	PyObject *mUpdateCallback, *mAllStateMaskClbk, *mHandleDyingClbk,
		*mPreWriteClbk,	*mPostWriteClbk, *mPreReadClbk,	*mPostReadClbk;
	PyObject *mSelf;
#else
	UpdateCallback mUpdateCallback;
	AllStateMaskCallback mAllStateMaskClbk;
	HandleDyingCallback mHandleDyingClbk;
	PreWriteCallback mPreWriteClbk;
	PostWriteCallback mPostWriteClbk;
	PreReadCallback mPreReadClbk;
	PostReadCallback mPostReadClbk;
#endif // PYTHON_BUILD

private:
	int mIndexInWorld;
	bool mDoesWantToDie;
	int mNetworkId;
	inline void do_init();
	inline void do_reset();
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(NetworkObject,TypedReferenceCount)
};

///inline
#include "networkObject.I"

#endif /* NETWORK_SOURCE_NETWORKOBJECT_H_ */
