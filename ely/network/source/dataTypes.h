/**
 * \file dataTypes.h
 *
 * \date 2018-08-29
 * \author consultit
 */

#ifndef NETWORK_SOURCE_DATATYPES_H_
#define NETWORK_SOURCE_DATATYPES_H_

#include "network_includes.h"

template<typename basetype> class DataType
{
PUBLISHED:
	INLINE DataType();
	INLINE DataType(const basetype& var);
	INLINE ~DataType();
	INLINE DataType& operator=(const DataType& var);
	INLINE DataType& operator=(const basetype& var);
	INLINE bool operator==(const DataType& oth) const;
	INLINE bool operator==(const basetype& oth) const;
	INLINE const basetype& get_value() const;
	MAKE_PROPERTY(value, get_value);
public:
	inline operator basetype&();
	inline basetype& get_value();
private:
	basetype mVar;
};

///inline
#include "dataTypes.I"

EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<int8_t>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<int16_t>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<int32_t>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<int64_t>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<uint8_t>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<uint16_t>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<uint32_t>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<uint64_t>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<float>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<double>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<bool>);
EXPORT_TEMPLATE_CLASS(EXPCL_NETWORK, EXPTP_NETWORK, DataType<string>);

typedef DataType<int8_t> Int8;
typedef DataType<int16_t> Int16;
typedef DataType<int32_t> Int32;
typedef DataType<int64_t> Int64;
typedef DataType<uint8_t> UInt8;
typedef DataType<uint16_t> UInt16;
typedef DataType<uint32_t> UInt32;
typedef DataType<uint64_t> UInt64;
typedef DataType<float> Float;
typedef DataType<double> Double;
typedef DataType<bool> Bool;
typedef DataType<string> String;

#endif /* NETWORK_SOURCE_DATATYPES_H_ */
