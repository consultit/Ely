/**
 * \file networkObjectRegistry.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "networkObjectRegistry.h"
#include "networkWorld.h"
#ifdef PYTHON_BUILD
#include "py_panda.h"
extern Dtool_PyTypedObject Dtool_NetworkObject;
#endif //PYTHON_BUILD

///NetworkObjectRegistry definitions

/**
 *
 */
NetworkObjectRegistry::NetworkObjectRegistry()
{
	mNameToNetworkObjectCreationFunctionMap.clear();
}

/**
 *
 */
NetworkObjectRegistry::~NetworkObjectRegistry()
{
#ifdef PYTHON_BUILD
	for (auto item : mNameToNetworkObjectCreationFunctionMap)
	{
		Py_XDECREF(item.second);
	}
#endif //PYTHON_BUILD
	mNameToNetworkObjectCreationFunctionMap.clear();
}

#ifdef PYTHON_BUILD
/**
 *
 */
void NetworkObjectRegistry::register_creation_function(PyObject* name,
		PyObject* func)
{
	uint32_t nameUInt32 = 0;
#if PY_MAJOR_VERSION < 3
	if (PyInt_Check(name))
	{
		nameUInt32 = (uint32_t)PyInt_AsUnsignedLongMask(name);
	}
	else
#endif
	if (PyLong_Check(name))
	{
		nameUInt32 = (uint32_t)PyLong_AsUnsignedLong(name);
	}
	else
	{
		string errStr = string(
				"NetworkObjectRegistry::register_creation_function(): '")
				+ get_py_object_name(name)
				+ string("' must be an (32 bit) integer value - ")
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
		return;
	}
	/* remove any func */
	if (PyCallable_Check(func))
	{
		auto funcIt = mNameToNetworkObjectCreationFunctionMap.find(nameUInt32);
		if (funcIt != mNameToNetworkObjectCreationFunctionMap.end())
		{
			Py_XDECREF(funcIt->second);
			mNameToNetworkObjectCreationFunctionMap.erase(funcIt);
		}
		/* add the callback */
		mNameToNetworkObjectCreationFunctionMap[nameUInt32] = func;
		Py_INCREF(mNameToNetworkObjectCreationFunctionMap[nameUInt32]);
	}
	else
	{
		string errStr = string(
				"NetworkObjectRegistry::register_creation_function(): '")
				+ get_py_object_name(func) + string("' must be callable - ")
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
	}
}
#else
/**
 *
 */
void NetworkObjectRegistry::register_creation_function(uint32_t name,
		NetworkObjectCreationFunc func)
{
	mNameToNetworkObjectCreationFunctionMap[name] = func;
}
#endif // PYTHON_BUILD

/**
 *
 */
PT(NetworkObject) NetworkObjectRegistry::create_network_object(uint32_t inFourCCName)
{
	PT(NetworkObject) networkObject;
#ifdef PYTHON_BUILD
	//no error checking- if the name isn't there, exception!
	PyObject *creationFunc =
			mNameToNetworkObjectCreationFunctionMap[inFourCCName];
	PyObject *resultObj;
	resultObj = PyObject_CallObject(creationFunc, nullptr);
	if (!resultObj)
	{
		string errStr =
				string(
						"NetworkObjectRegistry::create_network_object(): Error calling '")
						+ get_py_object_name(creationFunc)
						+ string("' object creation function - ")
						+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
		return nullptr;
	}
	else
	{
		bool isNetworkObject;
		NetworkObject* networkObjectPtr;
		isNetworkObject = DtoolInstance_GetPointer(resultObj, networkObjectPtr, Dtool_NetworkObject);
		if(isNetworkObject)
		{
			// convert to PT(NetworkObject)
			networkObject = networkObjectPtr;
		}
		else
		{
			string errStr = string(
					"NetworkObjectRegistry::create_network_object(): Error '")
					+ get_py_object_name(creationFunc)
					+ string("' does not create a NetworkObject - ")
					+ PY_GET_EXCEPTION_INFO();
			Py_DECREF(resultObj);
#ifdef ELY_DEBUG
			PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
			PRINT_ERR(errStr);
#endif
			return nullptr;
		}
	}
#else
	//no error checking- if the name isn't there, exception!
	NetworkObjectCreationFunc creationFunc =
			mNameToNetworkObjectCreationFunctionMap[inFourCCName];
	networkObject = creationFunc();
#endif // PYTHON_BUILD
	//should the registry depend on the world? this might be a little weird
	//perhaps you should ask the world to spawn things? for now it will be like this
	NetworkWorld::get_global_ptr()->add_network_object(networkObject);
#ifdef PYTHON_BUILD
	// decref the result
	Py_DECREF(resultObj);
#endif // PYTHON_BUILD
	return networkObject;
}

TYPED_OBJECT_API_DEF(NetworkObjectRegistry)
