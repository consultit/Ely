/**
 * \file replicationManagerTransmissionData.cxx
 *
 * \date 2018-07-24
 * \author consultit
 */

#include "replicationManagerTransmissionData.h"

#include "gameNetworkManagerServer.h"
#include "networkObject.h"

///ReplicationManagerTransmissionData definitions

/**
 *
 */
void ReplicationManagerTransmissionData::add_transmission(int inNetworkId,
		ReplicationAction inAction, uint32_t inState)
{
	/*
	 //it would be silly if we already had a transmission for this network id in here...
	 for( const auto& transmission: mTransmissions )
	 {
	 assert( inNetworkId != transmission.get_network_id() );
	 }
	 */
	mTransmissions.emplace_back(inNetworkId, inAction, inState);
}

/**
 *
 */
void ReplicationManagerTransmissionData::handle_delivery_failure(
		DeliveryNotificationManager* inDeliveryNotificationManager) const
{
	//run through the transmissions
	for (const ReplicationTransmission& rt : mTransmissions)
	{
		//is it a create? then we have to redo the create.
		int networkId = rt.get_network_id();

		switch (rt.get_action())
		{
		case RA_Create:
			do_handle_create_delivery_failure(networkId);
			break;
		case RA_Update:
			do_handle_update_state_delivery_failure(networkId, rt.get_state(),
					inDeliveryNotificationManager);
			break;
		case RA_Destroy:
			do_handle_destroy_delivery_failure(networkId);
			break;
		case RA_RPC:
		case RA_MAX:
		default:
			break;
		}

	}
}

/**
 *
 */
void ReplicationManagerTransmissionData::handle_delivery_success(
		DeliveryNotificationManager* inDeliveryNotificationManager) const
{
	//run through the transmissions, if any are Destroyed then we can remove this network id from the map
	for (const ReplicationTransmission& rt : mTransmissions)
	{
		switch (rt.get_action())
		{
		case RA_Create:
			do_handle_create_delivery_success(rt.get_network_id());
			break;
		case RA_Destroy:
			do_handle_destroy_delivery_success(rt.get_network_id());
			break;
		case RA_Update:
		case RA_RPC:
		case RA_MAX:
		default:
			break;
		}
	}
}

/**
 *
 */
void ReplicationManagerTransmissionData::do_handle_create_delivery_failure(
		int inNetworkId) const
{
	//does the object still exist? it might be dead, in which case we don't resend a create
	PT(NetworkObject) networkObject = GameNetworkManagerServer::get_global_ptr()->get_network_object(
			inNetworkId);
	if (networkObject)
	{
		mReplicationManagerServer->replicate_create(inNetworkId,
				networkObject->get_all_state_mask());
	}
}

/**
 *
 */
void ReplicationManagerTransmissionData::do_handle_destroy_delivery_failure(
		int inNetworkId) const
{
	mReplicationManagerServer->replicate_destroy(inNetworkId);
}

/**
 *
 */
void ReplicationManagerTransmissionData::do_handle_update_state_delivery_failure(
		int inNetworkId, uint32_t inState,
		DeliveryNotificationManager* inDeliveryNotificationManager) const
{
	//does the object still exist? it might be dead, in which case we don't resend an update
	if (GameNetworkManagerServer::get_global_ptr()->get_network_object(inNetworkId))
	{
		//look in all future in flight packets, in all transmissions
		//remove written state from dirty state
		for (const auto& inFlightPacket : inDeliveryNotificationManager->get_in_flight_packets())
		{
			PT(ReplicationManagerTransmissionData) rmtdp =
					DCAST(ReplicationManagerTransmissionData, inFlightPacket.get_transmission_data(
							GameNetworkManager::kReplCC));

			for (const ReplicationTransmission& otherRT : rmtdp->mTransmissions)
			{
				inState &= ~otherRT.get_state();
			}
		}

		//if there's still any dirty state, mark it
		if (inState)
		{
			mReplicationManagerServer->set_state_dirty(inNetworkId, inState);
		}
	}
}

/**
 *
 */
void ReplicationManagerTransmissionData::do_handle_create_delivery_success(
		int inNetworkId) const
{
	//we've received an ack for the create, so we can start sending as only an update
	mReplicationManagerServer->handle_create_ackd(inNetworkId);
}

/**
 *
 */
void ReplicationManagerTransmissionData::do_handle_destroy_delivery_success(
		int inNetworkId) const
{
	mReplicationManagerServer->remove_from_replication(inNetworkId);
}

TYPED_OBJECT_API_DEF(ReplicationManagerTransmissionData)
