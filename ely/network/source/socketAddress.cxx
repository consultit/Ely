/**
 * \file socketAddress.cxx
 *
 * \date 2018-07-22
 * \author consultit
 */

#include "socketAddress.h"
#include "socketUtil.h"

#include "support/StringUtils.h"

///SocketAddress definitions

/**
 *
 */
string SocketAddress::get_to_string() const
{
#if _WIN32
	const sockaddr_in* s = do_get_as_sock_addr_in();
	char destinationBuffer[ 128 ];
	InetNtop( s->sin_family, const_cast< in_addr* >( &s->sin_addr ), destinationBuffer, sizeof( destinationBuffer ) );
	return StringUtils::Sprintf( "%s:%d",
			destinationBuffer,
			ntohs( s->sin_port ) );
#elif __linux__
	const sockaddr_in* s = do_get_as_sock_addr_in();
	char destinationBuffer[INET_ADDRSTRLEN];
	if (!inet_ntop(AF_INET, &(s->sin_addr), destinationBuffer, INET_ADDRSTRLEN))
	{
		string msg("SocketAddress::ToString -> ");
		msg += string(strerror(errno));
		return StringUtils::Sprintf("%s", msg.c_str());
	}
	return StringUtils::Sprintf("%s:%d", destinationBuffer, ntohs(s->sin_port));
#else
	//not implement on mac for now...
	return string( "not implemented on mac for now" );
#endif
}

TYPED_OBJECT_API_DEF(SocketAddress)

///SocketAddressFactory definitions

/**
 *
 */
PT(SocketAddress) SocketAddressFactory::create_ipv4_from_string(
		const string& inString)
{
	auto pos = inString.find_last_of(':');
	string host, service;
	if (pos != string::npos)
	{
		host = inString.substr(0, pos);
		service = inString.substr(pos + 1);
	}
	else
	{
		host = inString;
		//use default port...
		service = "0";
	}
	addrinfo hint;
	memset(&hint, 0, sizeof(hint));
	hint.ai_family = AF_INET;

	addrinfo* result;
	int error = getaddrinfo(host.c_str(), service.c_str(), &hint, &result);
#ifdef _WIN32
	if( error != 0 && result != nullptr )
#else
	if (error != 0)
#endif
	{
		string msg("SocketAddressFactory::CreateIPv4FromString");
#ifndef _WIN32
		msg += string(" -> ") + string(gai_strerror(error));
#endif
		SocketUtil::report_error(msg.c_str());
		return nullptr;
	}

	while (!result->ai_addr && result->ai_next)
	{
		result = result->ai_next;
	}

	if (!result->ai_addr)
	{
		return nullptr;
	}

	auto toRet = new SocketAddress(*result->ai_addr);

	freeaddrinfo(result);

	return toRet;

}
