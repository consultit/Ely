/**
 * \file udpSocket.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "udpSocket.h"
#include "socketUtil.h"

#include "support/StringUtils.h"

///UDPSocket definitions

/**
 *
 */
int UDPSocket::bind_at(const SocketAddress& inBindAddress)
{
	int result = bind(mSocket, &inBindAddress.mSockAddr,
			inBindAddress.get_size());
	if (result == SOCKET_ERROR)
	{
		string msg("UDPSocket::Bind");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}

	return NO_ERROR;
}

/**
 *
 */
int UDPSocket::send_to(const void* inToSend, int inLength,
		const SocketAddress& inToAddress)
{
	int byteSentCount = sendto(mSocket,
#ifdef _WIN32
			static_cast<const char*>(inToSend),
#else
			inToSend,
#endif
			inLength, 0, &inToAddress.mSockAddr, inToAddress.get_size());
	if (byteSentCount <= 0)
	{
		string msg("UDPSocket::SendTo");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	else
	{
		return byteSentCount;
	}
}

/**
 *
 */
int UDPSocket::receive_from(void* inToReceive, int inMaxLength,
		SocketAddress& outFromAddress)
{
	socklen_t fromLength = outFromAddress.get_size();

	int readByteCount = recvfrom(mSocket,
#ifdef _WIN32
			static_cast<char*>(inToReceive),
#else
			inToReceive,
#endif
			inMaxLength, 0, &outFromAddress.mSockAddr, &fromLength);
	if (readByteCount >= 0)
	{
		return readByteCount;
	}
	else
	{
		int error = SocketUtil::get_last_error();

		if (error == WSAEWOULDBLOCK)
		{
			return 0;
		}
		else if (error == WSAECONNRESET)
		{
			//this can happen if a client closed and we haven't DC'd yet.
			//this is the ICMP message being sent back saying the port on that computer is closed
			LOG("Connection reset from %s", outFromAddress.get_to_string().c_str())
			return -WSAECONNRESET;
		}
		else
		{
			string msg("UDPSocket::ReceiveFrom");
#ifndef _WIN32
			msg += string(" -> ") + string(strerror(errno));
#endif
			SocketUtil::report_error(msg.c_str());
			return -error;
		}
	}
}

/**
 *
 */
int UDPSocket::set_non_blocking_mode(bool inShouldBeNonBlocking)
{
#if _WIN32
	u_long arg = inShouldBeNonBlocking ? 1 : 0;
	int result = ioctlsocket( mSocket, FIONBIO, &arg );
#else
	int flags = fcntl(mSocket, F_GETFL, 0);
	flags = inShouldBeNonBlocking ?
			(flags | O_NONBLOCK) : (flags & ~O_NONBLOCK);
	int result = fcntl(mSocket, F_SETFL, flags);
#endif

	if (result == SOCKET_ERROR)
	{
		string msg("UDPSocket::SetNonBlockingMode");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	else
	{
		return NO_ERROR;
	}
}

/**
 *
 */
int UDPSocket::set_socket_opt(int level, int optname, const char *optval,
		int optlen)
{
	int result = setsockopt(mSocket, level, optname,
#ifdef _WIN32
			optval, optlen
#else
			(const void*) optval, (socklen_t) optlen
#endif
			);

	if (result == SOCKET_ERROR)
	{
		string msg("UDPSocket::SetSocketOpt");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	else
	{
		return result;
	}
}

/**
 *
 */
int UDPSocket::get_socket_opt(int level, int optname, char *optval, int *optlen) const
{
	int result = getsockopt(mSocket, level, optname,
#ifdef _WIN32
			optval, optlen
#else
			(void*) optval, (socklen_t*) optlen
#endif
			);

	if (result == SOCKET_ERROR)
	{
		string msg("UDPSocket::GetSocketOpt");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::report_error(msg.c_str());
		return -SocketUtil::get_last_error();
	}
	else
	{
		return result;
	}
}

TYPED_OBJECT_API_DEF(UDPSocket)
