/**
 * \file replicationManagerTransmissionData.h
 *
 * \date 2018-07-24
 * \author consultit
 */

#ifndef NETWORK_SOURCE_REPLICATIONMANAGERTRANSMISSIONDATA_H_
#define NETWORK_SOURCE_REPLICATIONMANAGERTRANSMISSIONDATA_H_

#include "network_includes.h"

#include "transmissionData.h"
#include "replicationCommand.h"

class ReplicationManagerServer;

/**
 * class.
 */
class EXPCL_NETWORK ReplicationManagerTransmissionData: public TransmissionData
{
PUBLISHED:

	INLINE explicit ReplicationManagerTransmissionData(
			ReplicationManagerServer* inReplicationManagerServer);

	class ReplicationTransmission
	{
	public:
		ReplicationTransmission(int inNetworkId, ReplicationAction inAction,
				uint32_t inState) :
				mNetworkId(inNetworkId), mAction(inAction), mState(inState)
		{
		}

		int get_network_id() const
		{
			return mNetworkId;
		}
		ReplicationAction get_action() const
		{
			return mAction;
		}
		uint32_t get_state() const
		{
			return mState;
		}

	private:
		int mNetworkId;
		ReplicationAction mAction;
		uint32_t mState;
	};

	void add_transmission(int inNetworkId, ReplicationAction inAction,
			uint32_t inState);
	virtual void handle_delivery_failure(
			DeliveryNotificationManager* inDeliveryNotificationManager) const
					override;
	virtual void handle_delivery_success(
			DeliveryNotificationManager* inDeliveryNotificationManager) const
					override;
	// Python Properties

public:
	ReplicationManagerTransmissionData() = delete;

#ifndef CPPPARSER
private:

	void do_handle_create_delivery_failure(int inNetworkId) const;
	void do_handle_update_state_delivery_failure(int inNetworkId, uint32_t inState,
			DeliveryNotificationManager* inDeliveryNotificationManager) const;
	void do_handle_destroy_delivery_failure(int inNetworkId) const;
	void do_handle_create_delivery_success(int inNetworkId) const;
	void do_handle_destroy_delivery_success(int inNetworkId) const;
	ReplicationManagerServer* mReplicationManagerServer;
	pvector<ReplicationTransmission> mTransmissions;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(ReplicationManagerTransmissionData,TransmissionData)
};

///inline
#include "replicationManagerTransmissionData.I"

#endif /* NETWORK_SOURCE_REPLICATIONMANAGERTRANSMISSIONDATA_H_ */
