/**
 * \file networkManagerServer.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "gameNetworkManagerServer.h"
#include "networkWorld.h"
#include "replicationManagerTransmissionData.h"

#include "support/StringUtils.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
extern Dtool_PyTypedObject Dtool_OutputMemoryBitStream;
extern Dtool_PyTypedObject Dtool_ClientProxy;
#endif //PYTHON_BUILD

///GameNetworkManagerServer definitions

/**
 *
 */
GameNetworkManagerServer::GameNetworkManagerServer() :
		mNewPlayerId(1), mNewNetworkId(1),
		/*fixme
		 mTimeOfLastStatePacket(0.f),
		 mTimeBetweenStatePackets(0.033f),
		 */
		mClientDisconnectTimeout(5.f), mRespawnClientObjectsDelay(3.f), mDelayBeforeAckTimeout(
				0.5)
{
	mHandlePacketFromNewClientClbk = nullptr;
	mHandleClientDisconnectedClbk = nullptr;
	mSendWelcomePacketClbk = nullptr;
	mSendStatePacketToClientClbk = nullptr;
	mRespawnClientObjectsClbk = nullptr;
}

/**
 *
 */
GameNetworkManagerServer::~GameNetworkManagerServer()
{
#ifdef PYTHON_BUILD
	//Python callback
	Py_XDECREF(mHandlePacketFromNewClientClbk);
	Py_XDECREF(mHandleClientDisconnectedClbk);
	Py_XDECREF(mSendWelcomePacketClbk);
	Py_XDECREF(mSendStatePacketToClientClbk);
	Py_XDECREF(mRespawnClientObjectsClbk);
#endif //PYTHON_BUILD
	mHandlePacketFromNewClientClbk = nullptr;
	mHandleClientDisconnectedClbk = nullptr;
	mSendWelcomePacketClbk = nullptr;
	mSendStatePacketToClientClbk = nullptr;
	mRespawnClientObjectsClbk = nullptr;
}

/**
 *
 */
void GameNetworkManagerServer::handle_connection_reset(
		const SocketAddress& inFromAddress)
{
	//just dc the client right away...
	auto it = mAddressToClientMap.find(inFromAddress);
	if (it != mAddressToClientMap.end())
	{
		do_handle_client_disconnected(it->second);
	}
}

/**
 *
 */
void GameNetworkManagerServer::process_packet(InputMemoryBitStream& inInputStream,
		const SocketAddress& inFromAddress)
{
	//try to get the client proxy for this address
	//pass this to the client proxy to process
	auto it = mAddressToClientMap.find(inFromAddress);
	if (it == mAddressToClientMap.end())
	{
		//didn't find one? it's a new cilent..is the a HELO? if so, create a client proxy...
		do_handle_packet_from_new_client(inInputStream, inFromAddress);
	}
	else
	{
		do_process_packet((*it).second, inInputStream);
	}
}

/**
 *
 */
void GameNetworkManagerServer::do_process_packet(PT(ClientProxy) inClientProxy,
		InputMemoryBitStream& inInputStream)
{
	//remember we got a packet so we know not to disconnect for a bit
	inClientProxy->update_last_packet_time();

	uint32_t packetType;
	inInputStream.read(packetType);
	switch (packetType)
	{
	case kHelloCC:
		//need to resend welcome. to be extra safe we should check the name is the one we expect from this address,
		//otherwise something weird is going on...
		do_send_welcome_packet(inClientProxy);
		break;
	case kInputCC:
		if (inClientProxy->get_delivery_notification_manager().read_and_process_state(
				inInputStream))
		{
			do_handle_input_packet(inClientProxy, inInputStream);
		}
		break;
	default:
		LOG("Unknown packet type received from %s",
				inClientProxy->get_socket_address().get_to_string().c_str())
		break;
	}
}

/**
 *
 */
void GameNetworkManagerServer::do_handle_packet_from_new_client(
		InputMemoryBitStream& inInputStream, const SocketAddress& inFromAddress)
{
	//read the beginning- is it a hello?
	uint32_t packetType;
	inInputStream.read(packetType);
	if (packetType == kHelloCC)
	{
		//read the name
		string name;
		inInputStream.read(name);
		PT(ClientProxy) newClientProxy = new ClientProxy(inFromAddress,
				name, mNewPlayerId++);
		newClientProxy->mDeliveryNotificationManager.set_delay_before_ack_timeout(mDelayBeforeAckTimeout);
		newClientProxy->mRespawnClientObjectsDelay = mRespawnClientObjectsDelay;
#ifdef PYTHON_BUILD
		if (mRespawnClientObjectsClbk)
		{
			// add the newClientProxy's callback
			newClientProxy->mRespawnClientObjectsClbk = mRespawnClientObjectsClbk;
			Py_INCREF(newClientProxy->mRespawnClientObjectsClbk);
		}
#else
		newClientProxy->mRespawnClientObjectsClbk = mRespawnClientObjectsClbk;
#endif //PYTHON_BUILD
		mAddressToClientMap[inFromAddress] = newClientProxy;
		mPlayerIdToClientMap[newClientProxy->get_player_id()] = newClientProxy;

		//tell the server about this client, spawn a cat, etc...
		//if we had a generic message system, this would be a good use for it...
		//instead we'll just tell the server directly
		if (mHandlePacketFromNewClientClbk)
		{
#ifdef PYTHON_BUILD
			PyObject *newClientProxyObj, *result;
			// create the argument object
			newClientProxyObj = DTool_CreatePyInstanceTyped(newClientProxy,
					Dtool_ClientProxy, false, false, get_type_index());
			// call the callback
			result = PyObject_CallFunctionObjArgs(mHandlePacketFromNewClientClbk,
					newClientProxyObj, nullptr);
			Py_DECREF(newClientProxyObj);
			if (!result)
			{
				string errStr =
						string(
								"GameNetworkManagerServer::do_handle_packet_from_new_client(): Error calling '")
								+ get_py_object_name(
										mHandlePacketFromNewClientClbk)
								+ string("' callback function - ")
								+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
				PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
				PRINT_ERR(errStr);
#endif
				return;
			}
			Py_DECREF(result);
#else
			mHandlePacketFromNewClientClbk(newClientProxy);
#endif //PYTHON_BUILD
		}

		//and welcome the client...
		do_send_welcome_packet(newClientProxy);

		//and now init the replication manager with everything we know about!
		for (const auto& pair : mNetworkIdToNetworkObjectMap)
		{
			newClientProxy->get_replication_manager_server().replicate_create(
					pair.first, pair.second->get_all_state_mask());
		}
	}
	else
	{
		//bad incoming packet from unknown client- we're under attack!!
		LOG("Bad incoming packet from unknown client at socket %s",
				inFromAddress.get_to_string().c_str())
	}
}

/**
 *
 */
void GameNetworkManagerServer::do_send_welcome_packet(PT(ClientProxy) inClientProxy)
{
	OutputMemoryBitStream welcomePacket;

	welcomePacket.write(kWelcomeCC);
	welcomePacket.write(inClientProxy->get_player_id());

	LOG("Server Welcoming, new client '%s' as player %d (address: %s)",
			inClientProxy->get_name().c_str(), inClientProxy->get_player_id(),
			inClientProxy->get_socket_address().get_to_string().c_str())

	if (mSendWelcomePacketClbk)
	{
#ifdef PYTHON_BUILD
		PyObject *welcomePacketObj, *result;
		// create the argument object
		welcomePacketObj = DTool_CreatePyInstance(&welcomePacket,
				Dtool_OutputMemoryBitStream, false, false);
		// call the callback
		result = PyObject_CallFunctionObjArgs(mSendWelcomePacketClbk,
				welcomePacketObj, nullptr);
		Py_DECREF(welcomePacketObj);
		if (!result)
		{
			string errStr =
					string(
							"GameNetworkManagerServer::do_send_welcome_packet(): Error calling '")
							+ get_py_object_name(mSendWelcomePacketClbk)
							+ string("' callback function - ")
							+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
			PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
			PRINT_ERR(errStr);
#endif
			return;
		}
		Py_DECREF(result);
#else
		mSendWelcomePacketClbk(welcomePacket);
#endif //PYTHON_BUILD
	}

	send_packet(welcomePacket, inClientProxy->get_socket_address());
}

/**
 *
 */
void GameNetworkManagerServer::respawn_objects_for_clients()
{
	for (auto it = mAddressToClientMap.begin(), end = mAddressToClientMap.end();
			it != end; ++it)
	{
		PT(ClientProxy) clientProxy = it->second;

		clientProxy->respawn_objects_if_necessary();
	}
}

/**
 *
 */
void GameNetworkManagerServer::send_outgoing_packets()
{
	//float time = Timing::get_global_ptr()->get_timef();

	//let's send a client a state packet whenever their move has come in...
	for (auto it = mAddressToClientMap.begin(), end = mAddressToClientMap.end();
			it != end; ++it)
	{
		PT(ClientProxy) clientProxy = it->second;
		//process any timed out packets while we're going through the list
		clientProxy->get_delivery_notification_manager().process_timed_out_packets();

		if (clientProxy->get_last_move_timestamp_dirty())
		{
			do_send_state_packet_to_client(clientProxy);
		}
	}
}

/**
 *
 */
void GameNetworkManagerServer::do_update_all_clients()
{
	for (auto it = mAddressToClientMap.begin(), end = mAddressToClientMap.end();
			it != end; ++it)
	{
		//process any timed out packets while we're going throug hthe list
		it->second->get_delivery_notification_manager().process_timed_out_packets();

		do_send_state_packet_to_client(it->second);
	}
}

/**
 *
 */
void GameNetworkManagerServer::do_send_state_packet_to_client(PT(ClientProxy) inClientProxy)
{
	//build state packet
	OutputMemoryBitStream statePacket;

	//it's state!
	statePacket.write(kStateCC);

	InFlightPacket* ifp =
			inClientProxy->get_delivery_notification_manager().write_state(
					statePacket);

	do_write_last_move_timestamp_if_dirty(statePacket, inClientProxy);

	if (mSendStatePacketToClientClbk)
	{
#ifdef PYTHON_BUILD
		PyObject *statePacketObj,*result;
		// create the argument object
		statePacketObj = DTool_CreatePyInstance(&statePacket,
				Dtool_OutputMemoryBitStream, false, false);
		// call the callback
		result = PyObject_CallFunctionObjArgs(mSendStatePacketToClientClbk,
				statePacketObj, nullptr);
		Py_DECREF(statePacketObj);
		if (!result)
		{
			string errStr =
					string(
							"GameNetworkManagerServer::do_send_state_packet_to_client(): Error calling '")
							+ get_py_object_name(mSendStatePacketToClientClbk)
							+ string("' callback function - ")
							+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
			PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
			PRINT_ERR(errStr);
#endif
			return;
		}
		Py_DECREF(result);
#else
		mSendStatePacketToClientClbk(statePacket);
#endif //PYTHON_BUILD
	}

	ReplicationManagerTransmissionData* rmtd =
			new ReplicationManagerTransmissionData(
					&inClientProxy->get_replication_manager_server());
	inClientProxy->get_replication_manager_server().write(statePacket, rmtd);
	ifp->set_transmission_data(GameNetworkManager::kReplCC, PT(TransmissionData)(rmtd));

	send_packet(statePacket, inClientProxy->get_socket_address());
}

/**
 *
 */
void GameNetworkManagerServer::do_write_last_move_timestamp_if_dirty(
		OutputMemoryBitStream& inOutputStream, PT(ClientProxy) inClientProxy)
{
	//first, dirty?
	bool isTimestampDirty = inClientProxy->get_last_move_timestamp_dirty();
	inOutputStream.write(isTimestampDirty);
	if (isTimestampDirty)
	{
		inOutputStream.write(
				inClientProxy->get_unprocessed_move_list().get_last_move_timestamp());
		inClientProxy->set_last_move_timestamp_dirty(false);
	}
}

/**
 * Should we ask the server for this? or run through the world ourselves?
 */
void GameNetworkManagerServer::do_add_world_state_to_packet(
		OutputMemoryBitStream& inOutputStream)
{
	const pvector<PT(NetworkObject)>& networkObjects =
			NetworkWorld::get_global_ptr()->get_network_objects();

	//now start writing objects- do we need to remember how many there are? we can check first...
	inOutputStream.write(networkObjects.size());

	for (auto networkObject : networkObjects)
	{
		inOutputStream.write(networkObject->get_network_id());
		inOutputStream.write(networkObject->get_class_id());
		networkObject->write(inOutputStream, 0xffffffff);
	}
}

/**
 *
 */
int GameNetworkManagerServer::do_get_new_network_id()
{
	int toRet = mNewNetworkId++;
	if (mNewNetworkId < toRet)
	{
		LOG("Network ID Wrap Around!!! You've been playing way too long...", 0)
	}

	return toRet;

}

/**
 *
 */
void GameNetworkManagerServer::do_handle_input_packet(PT(ClientProxy) inClientProxy,
		InputMemoryBitStream& inInputStream)
{
	uint32_t moveCount = 0;
	Move move;
	inInputStream.read(moveCount, 2);

	for (; moveCount > 0; --moveCount)
	{
		if (move.read(inInputStream))
		{
			if (inClientProxy->get_unprocessed_move_list().add_move_if_new(move))
			{
				inClientProxy->set_last_move_timestamp_dirty(true);
			}
		}
	}
}

/**
 *
 */
PT(ClientProxy) GameNetworkManagerServer::get_client_proxy(int inPlayerId) const
{
	auto it = mPlayerIdToClientMap.find(inPlayerId);
	if (it != mPlayerIdToClientMap.end())
	{
		return it->second;
	}

	return nullptr;
}

/**
 *
 */
ValueList_ClientProxyPtr GameNetworkManagerServer::get_all_client_proxies() const
{
	pvector<PT(ClientProxy)> clients;
	for (auto item: mPlayerIdToClientMap)
	{
		clients.emplace_back(item.second);
	}
	return clients;
}

/**
 *
 */
void GameNetworkManagerServer::clear_all_client_moves()
{
	for (auto item: mPlayerIdToClientMap)
	{
		item.second->get_unprocessed_move_list().clear();
	}
}

/**
 *
 */
void GameNetworkManagerServer::check_for_disconnects()
{
	pvector<PT(ClientProxy)> clientsToDC;

	float minAllowedLastPacketFromClientTime = Timing::get_global_ptr()->get_timef()
			- mClientDisconnectTimeout;
	for (const auto& pair : mAddressToClientMap)
	{
		if (pair.second->get_last_packet_from_client_time()
				< minAllowedLastPacketFromClientTime)
		{
			//can't remove from map while in iterator, so just remember for later...
			clientsToDC.push_back(pair.second);
		}
	}

	for (PT(ClientProxy) client : clientsToDC)
	{
		do_handle_client_disconnected(client);
	}
}

/**
 *
 */
void GameNetworkManagerServer::do_handle_client_disconnected(
		PT(ClientProxy) inClientProxy)
{
	mPlayerIdToClientMap.erase(inClientProxy->get_player_id());
	mAddressToClientMap.erase(inClientProxy->get_socket_address());
	if (mHandleClientDisconnectedClbk)
	{
#ifdef PYTHON_BUILD
		PyObject *inClientProxyObj, *result;
		// create the argument object
		inClientProxyObj = DTool_CreatePyInstanceTyped(inClientProxy,
				Dtool_ClientProxy, false, false, get_type_index());
		// call the callback
		result = PyObject_CallFunctionObjArgs(mHandleClientDisconnectedClbk,
				inClientProxyObj, nullptr);
		Py_DECREF(inClientProxyObj);
		if (!result)
		{
			string errStr =
					string(
							"GameNetworkManagerServer::do_handle_client_disconnected(): Error calling '")
							+ get_py_object_name(mHandleClientDisconnectedClbk)
							+ string("' callback function - ")
							+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
			PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
			PRINT_ERR(errStr);
#endif
			return;
		}
		Py_DECREF(result);
#else
		mHandleClientDisconnectedClbk(inClientProxy);
#endif //PYTHON_BUILD
	}
}

/**
 *
 */
void GameNetworkManagerServer::register_network_object(PT(NetworkObject) inNetworkObject)
{
	//assign network id
	int newNetworkId = do_get_new_network_id();
	inNetworkObject->set_network_id(newNetworkId);

	//add mapping from network id to game object
	mNetworkIdToNetworkObjectMap[newNetworkId] = inNetworkObject;

	//tell all client proxies this is new...
	for (const auto& pair : mAddressToClientMap)
	{
		pair.second->get_replication_manager_server().replicate_create(newNetworkId,
				inNetworkObject->get_all_state_mask());
	}
}

/**
 *
 */
void GameNetworkManagerServer::unregister_network_object(PT(NetworkObject) inNetworkObject)
{
	int networkId = inNetworkObject->get_network_id();
	mNetworkIdToNetworkObjectMap.erase(networkId);

	//tell all client proxies to STOP replicating!
	//tell all client proxies this is new...
	for (const auto& pair : mAddressToClientMap)
	{
		pair.second->get_replication_manager_server().replicate_destroy(networkId);
	}
}

/**
 *
 */
void GameNetworkManagerServer::set_state_dirty(int inNetworkId, uint32_t inDirtyState)
{
	//tell everybody this is dirty
	for (const auto& pair : mAddressToClientMap)
	{
		pair.second->get_replication_manager_server().set_state_dirty(inNetworkId,
				inDirtyState);
	}
}

#ifdef PYTHON_BUILD
/**
 *
 */
void GameNetworkManagerServer::set_send_welcome_packet_callback(PyObject* clbk)
{
	PY_SET_FUNC_BODY(mSendWelcomePacketClbk, clbk,
			"GameNetworkManagerServer.set_send_welcome_packet_callback()");
}

/**
 *
 */
void GameNetworkManagerServer::set_send_state_packet_to_client_callback(PyObject* clbk)
{
	PY_SET_FUNC_BODY(mSendStatePacketToClientClbk, clbk,
			"GameNetworkManagerServer.set_send_state_packet_to_client_callback()");
}

/**
 *
 */
void GameNetworkManagerServer::set_handle_packet_from_new_client_callback(PyObject* clbk)
{
	PY_SET_FUNC_BODY(mHandlePacketFromNewClientClbk, clbk,
			"GameNetworkManagerServer.set_handle_packet_from_new_client_callback()");
}

/**
 *
 */
void GameNetworkManagerServer::set_handle_client_disconnected_callback(PyObject* clbk)
{
	PY_SET_FUNC_BODY(mHandleClientDisconnectedClbk, clbk,
			"GameNetworkManagerServer.set_handle_client_disconnected_callback()");
}

/**
 *
 */
void GameNetworkManagerServer::set_respawn_client_objects_callback(PyObject* clbk)
{
	PY_SET_FUNC_BODY(mRespawnClientObjectsClbk, clbk,
			"GameNetworkManagerServer.set_respawn_client_objects_callback()");
}

#else
/**
 *
 */
void GameNetworkManagerServer::set_send_welcome_packet_callback(
		SendWelcomePacketCallback clbk)
{
	mSendWelcomePacketClbk = clbk;
}

/**
 *
 */
void GameNetworkManagerServer::set_send_state_packet_to_client_callback(
		SendStatePacketToClientCallback clbk)
{
	mSendStatePacketToClientClbk = clbk;
}

/**
 *
 */
void GameNetworkManagerServer::set_handle_packet_from_new_client_callback(
		HandlePacketFromNewClientCallback clbk)
{
	mHandlePacketFromNewClientClbk = clbk;
}

/**
 *
 */
void GameNetworkManagerServer::set_handle_client_disconnected_callback(
		HandleClientDisconnectedCallback clbk)
{
	mHandleClientDisconnectedClbk = clbk;
}

/**
 *
 */
void GameNetworkManagerServer::set_respawn_client_objects_callback(
		RespawnClientObjectsCallback clbk)
{
	mRespawnClientObjectsClbk = clbk;
}
#endif //PYTHON_BUILD

TYPED_OBJECT_API_DEF(GameNetworkManagerServer)
