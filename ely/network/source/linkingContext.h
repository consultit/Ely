/**
 * \file linkingContext.h
 *
 * \date 2018-07-25
 * \author consultit
 */

#ifndef NETWORK_SOURCE_LINKINGCONTEXT_H_
#define NETWORK_SOURCE_LINKINGCONTEXT_H_

#include "network_includes.h"

#include "networkObject.h"
#include <cstdint>
#include <unordered_map>

class NetworkObject;

using std::unordered_map;

namespace std
{
template<> struct hash<CPT(NetworkObject)> : public hash<const NetworkObject*>
{
	size_t operator()(CPT(NetworkObject) cnop) const noexcept
	{
		return hash<const NetworkObject*>::operator()(cnop);
	}
};
}

/**
 * LinkingContext class.
 */
class EXPCL_NETWORK LinkingContext
{
PUBLISHED:

	INLINE LinkingContext();
	INLINE uint32_t get_network_id(PT(NetworkObject) inNetworkObject,
			bool inShouldCreateIfNotFound);
	INLINE PT(NetworkObject) get_network_object(uint32_t inNetworkId) const;
	INLINE void add_network_object(PT(NetworkObject) inNetworkObject,
			uint32_t inNetworkId);
	INLINE void remove_network_object(PT(NetworkObject) inNetworkObject);
	// Python Properties

#ifndef CPPPARSER
private:
	unordered_map<uint32_t, PT(NetworkObject)> mNetworkIdToNetworkObjectMap;
	unordered_map<CPT(NetworkObject), uint32_t, hash<CPT(NetworkObject)>> mNetworkObjectToNetworkIdMap;
///	unordered_map<const NetworkObject*, uint32_t> mNetworkObjectToNetworkIdMap;
	uint32_t mNextNetworkId;
#endif //CPPPARSER
};

///inline
#include "linkingContext.I"

#endif /* NETWORK_SOURCE_LINKINGCONTEXT_H_ */
