/**
 * \file replicationManagerClient.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "replicationManagerClient.h"

#include "networkObjectRegistry.h"
#include "replicationCommand.h"
#include <cassert>

#include "gameNetworkManagerClient.h"

///ReplicationManagerClient definitions

/**
 *
 */
void ReplicationManagerClient::read(InputMemoryBitStream& inInputStream)
{
	while (inInputStream.get_remaining_bit_count() >= 32)
	{
		//read the network id...
		int networkId;
		inInputStream.read(networkId);

		//only need 2 bits for action...
		uint8_t action;
		inInputStream.read(action, 2);

		switch (action)
		{
		case RA_Create:
			do_read_and_do_create_action(inInputStream, networkId);
			break;
		case RA_Update:
			do_read_and_do_update_action(inInputStream, networkId);
			break;
		case RA_Destroy:
			do_read_and_do_destroy_action(inInputStream, networkId);
			break;
		}

	}

}

/**
 *
 */
void ReplicationManagerClient::do_read_and_do_create_action(
		InputMemoryBitStream& inInputStream, int inNetworkId)
{
	//need 4 cc
	uint32_t fourCCName;
	inInputStream.read(fourCCName);

	//we might already have this object- could happen if our ack of the create got dropped so server resends create request 
	//( even though we might have created )
	PT(NetworkObject) networkObject = GameNetworkManagerClient::get_global_ptr()->get_network_object(
			inNetworkId);
	if (!networkObject)
	{
		//create the object and map it...
		networkObject = NetworkObjectRegistry::get_global_ptr()->create_network_object(
				fourCCName);
		networkObject->set_network_id(inNetworkId);
		GameNetworkManagerClient::get_global_ptr()->add_to_network_id_to_network_object_map(
				networkObject);

		//it had really be the rigth type...
		assert(networkObject->get_class_id() == fourCCName);
	}

	//and read state
	networkObject->read(inInputStream);
}

/**
 *
 */
void ReplicationManagerClient::do_read_and_do_update_action(
		InputMemoryBitStream& inInputStream, int inNetworkId)
{
	//need object
	PT(NetworkObject) networkObject = GameNetworkManagerClient::get_global_ptr()->get_network_object(
			inNetworkId);

	//networkObject MUST be found, because create was ack'd if we're getting an update...
	//and read state
	networkObject->read(inInputStream);
}

/**
 *
 */
void ReplicationManagerClient::do_read_and_do_destroy_action(
		InputMemoryBitStream& inInputStream, int inNetworkId)
{
	//if something was destroyed before the create went through, we'll never get it
	//but we might get the destroy request, so be tolerant of being asked to destroy something that wasn't created
	PT(NetworkObject) networkObject = GameNetworkManagerClient::get_global_ptr()->get_network_object(
			inNetworkId);
	if (networkObject)
	{
		networkObject->set_does_want_to_die(true);
		GameNetworkManagerClient::get_global_ptr()->remove_from_network_id_to_network_object_map(
				networkObject);
	}
}
