# version as tuple for simple comparisons
VERSION = (1, 0, 0)
# string created from tuple to avoid inconsistency
__version__ = ".".join([str(x) for x in VERSION])

import panda3d.core, panda3d.direct, panda3d.egg, panda3d.interrogatedb, ely.libtools
