/**
 * \file networkTools.cxx
 *
 * \date 2018-08-14
 * \author consultit
 */

#include "networkTools.h"

///NetBitMask
TypeHandle NetBitMask::_type_handle;

///NETWORK VALUELISTS' (EXPANDED) DEFINITIONS
#define PAIR_EXPANDABLE_PLACEHOLDER
#define VALUELIST_EXPANDABLE_PLACEHOLDER NETWORKVALUELIST_EXPANDABLE

#include "../../libtools/source/support/pairs_valueLists_src.cpp"

#undef PAIR_EXPANDABLE_PLACEHOLDER
#undef VALUELIST_EXPANDABLE_PLACEHOLDER
