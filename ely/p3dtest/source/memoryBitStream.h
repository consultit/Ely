/**
 * \file memoryBitStream.h
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef P3DTEST_SOURCE_MEMORYBITSTREAM_H_
#define P3DTEST_SOURCE_MEMORYBITSTREAM_H_

#include "p3dtest_includes.h"

//#include "dataTypes.h"
#include "lvecBase3.h"
#include "lquaternion.h"
#include "bitMask.h"
#include <string>
#include <type_traits>
#include <algorithm>

using std::string;
using std::is_arithmetic;
using std::is_enum;
using std::max;

#ifndef CPPPARSER
#include "ByteSwap.h"
#endif //CPPPARSER

///IOBuffer

/**
 * IOBuffer class.
 */
class EXPCL_P3DTEST IOBuffer
{
PUBLISHED:
	INLINE IOBuffer();
	INLINE ~IOBuffer();

public:
	inline IOBuffer(char* buffer);
	inline IOBuffer& operator=(char* buffer);
	inline operator char*();
	inline operator const char*() const;

private:
	char* mBuffer;
};

/// IO Streams

/*
#define MEMORYBITSTREAMEXPAND \
EXPAND(Int)\
EXPAND(Int8)\
EXPAND(Int16)\
EXPAND(Int32)\
EXPAND(Int64)\
EXPAND(UInt)\
EXPAND(UInt8)\
EXPAND(UInt16)\
EXPAND(UInt32)\
EXPAND(UInt64)\
EXPAND(Float)\
EXPAND(Double)\
EXPAND(Bool)\
EXPAND(String)
*/

///OutputMemoryBitStream

inline uint32_t ConvertToFixed(float inNumber, float inMin, float inPrecision);

inline float ConvertFromFixed(uint32_t inNumber, float inMin, float inPrecision);

/**
 * OutputMemoryBitStream class.
 */
class EXPCL_P3DTEST OutputMemoryBitStream
{
PUBLISHED:

	INLINE OutputMemoryBitStream();
	INLINE ~OutputMemoryBitStream();
	INLINE IOBuffer get_buffer_ptr() const;
	INLINE uint32_t get_bit_length() const;
	INLINE uint32_t get_byte_length() const;
	// write
/*
#define EXPAND(type) \
	INLINE void write(const type& in##type);\

	MEMORYBITSTREAMEXPAND
#undef EXPAND
*/
	void write(const LVecBase3f& inVector);
	INLINE void write(const LPoint3f& inVector);
	INLINE void write(const LVector3f& inVector);
	void write(const LVecBase4f& inVector);
	void write(const LQuaternionf& inQuat);
	INLINE void write(const BitMask32& inMask);
	// Python Properties
	MAKE_PROPERTY(buffer_ptr, get_buffer_ptr);
	MAKE_PROPERTY(bit_length, get_bit_length);
	MAKE_PROPERTY(byte_length, get_byte_length);

public:
	void write_bits(uint8_t inData, uint32_t inBitCount);
	void write_bits(const void* inData, uint32_t inBitCount);
	inline void write_bytes(const void* inData, uint32_t inByteCount);
	template<typename T> inline void write(T inData, uint32_t inBitCount = sizeof(T) * 8);
	inline void write(const string& inString);
	inline void write(bool inData);

private:
	void do_realloc_buffer(uint32_t inNewBitCapacity);
	char* mBuffer;
	uint32_t mBitHead;
	uint32_t mBitCapacity;
};

///InputMemoryBitStream

/**
 * InputMemoryBitStream class.
 */
class EXPCL_P3DTEST InputMemoryBitStream
{
PUBLISHED:

	INLINE InputMemoryBitStream();
	INLINE InputMemoryBitStream(const IOBuffer& inBuffer, uint32_t inBitCount);
	INLINE InputMemoryBitStream(const InputMemoryBitStream& inOther);
	INLINE ~InputMemoryBitStream();
	INLINE InputMemoryBitStream& operator=(const InputMemoryBitStream& inOther);
	INLINE IOBuffer get_buffer_ptr() const;
	INLINE uint32_t get_remaining_bit_count() const;
	INLINE void reset_to_capacity(uint32_t inByteCapacity);
	// read
/**
#define EXPAND(type) \
	INLINE void read(type& in##type);\

	MEMORYBITSTREAMEXPAND
#undef EXPAND
*/
	void read(LVecBase3f& inVector);
	INLINE void read(LPoint3f& inVector);
	INLINE void read(LVector3f& inVector);
	void read(LVecBase4f& inVector);
	void read(LQuaternionf& outQuat);
	INLINE void read(BitMask32& inMask);
	// Python Properties
	MAKE_PROPERTY(buffer_ptr, get_buffer_ptr);
	MAKE_PROPERTY(remaining_bit_count, get_remaining_bit_count);

public:
	void read_bits(uint8_t& outData, uint32_t inBitCount);
	void read_bits(void* outData, uint32_t inBitCount);
	inline void read_bytes(void* outData, uint32_t inByteCount);
	template<typename T> inline void read(T& inData, uint32_t inBitCount = sizeof(T) * 8);
	inline void read(string& inString);
	inline void read(bool& outData);

private:
	IOBuffer mBuffer;
	uint32_t mBitHead;
	uint32_t mBitCapacity;
	bool mIsBufferOwner;
	inline void do_allocate(uint32_t bitCapacity, uint32_t bitHead, const char* buffer);
};

///inline
#include "memoryBitStream.I"

#endif /* P3DTEST_SOURCE_MEMORYBITSTREAM_H_ */
