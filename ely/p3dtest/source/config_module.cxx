#include "config_module.h"
#include "dconfig.h"

#include "typedClass.h"
#include "networkObject.h"
#include "networkWorld.h"

Configure(config_p3test);
NotifyCategoryDef(p3test, "");

ConfigureFn( config_p3test )
{
	init_libp3test();
}

void init_libp3test()
{
	static bool initialized = false;
	if (initialized)
	{
		return;
	}
	initialized = true;

	// Init your dynamic types here, e.g.:
	TypedClass::init_type();
	NetworkObject::init_type();
	NetworkWorld::init_type();

	return;
}

