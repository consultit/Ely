/**
 * \file networkObject.h
 *
 * \date 2018-07-22
 * \author consultit
 */

#ifndef P3DTEST_SOURCE_NETWORKOBJECT_H_
#define P3DTEST_SOURCE_NETWORKOBJECT_H_

#include "p3dtest_includes.h"
#include "pandabase.h"
#include "typedReferenceCount.h"
#include "../../common/source/commonTools.h"
#include "pairs_valueLists.h"
#include "utilities.h"
#include "nodePath.h"

BEGIN_PUBLISH
EXPCL_P3DTEST string get_class_id_str(uint32_t classId);
EXPCL_P3DTEST uint32_t get_class_id_int(const string& classIdStr);
END_PUBLISH

#define CLASS_IDENTIFICATION \
	INLINE explicit NetworkObject(uint32_t inCode);\
	INLINE uint32_t get_class_id() const { return kClassId; }

constexpr int32_t constInt32(char c3, char c2, char c1, char c0)
{
	return c3 << 24 | c2 << 16 | c1 << 8 | c0;
}

/**
 * NetworkObject class.
 */
class EXPCL_P3DTEST NetworkObject: public TypedReferenceCount
{
PUBLISHED:

	CLASS_IDENTIFICATION
	enum E
	{
		kHelloCC = constInt32('H','E','L','O'),
		kWelcomeCC = constInt32('W','L','C','M'),
		kStateCC = constInt32('S','T','A','T'),
		kInputCC = constInt32('I','N','P','T'),
		kReplCC = constInt32('R','P','L','M'),
	};
//	static const uint32_t kHelloCC = constInt32('H','E','L','O');
//	static const uint32_t kWelcomeCC = constInt32('W','L','C','M');
//	static const uint32_t kStateCC = constInt32('S','T','A','T');
//	static const uint32_t kInputCC = constInt32('I','N','P','T');
//	static const uint32_t kReplCC = constInt32('R','P','L','M');

	INLINE virtual ~NetworkObject();
	INLINE virtual void update();
	INLINE virtual uint32_t get_all_state_mask() const;
	INLINE virtual void handle_dying();
	INLINE void set_owner_object(const NodePath& np);
	INLINE NodePath get_owner_object() const;
	INLINE void set_index_in_world(int inIndex);
	INLINE int get_index_in_world() const;
	INLINE void set_does_want_to_die(bool inWants);
	INLINE bool get_does_want_to_die() const;
	INLINE void set_network_id(int inNetworkId);
	INLINE int get_network_id() const;

public:
	inline NetworkObject();

protected:
	NodePath mNodePath;
	NodePath mRender;

private:
	uint32_t kClassId;
	int mIndexInWorld;
	bool mDoesWantToDie;
	int mNetworkId;
	inline void do_init();

TYPED_OBJECT_API_DECL(NetworkObject,TypedReferenceCount)
};

///inline
#include "networkObject.I"

#endif /* P3DTEST_SOURCE_NETWORKOBJECT_H_ */
