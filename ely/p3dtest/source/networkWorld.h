/**
 * \file networkWorld.h
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef P3DTEST_SOURCE_NETWORKWORLD_H_
#define P3DTEST_SOURCE_NETWORKWORLD_H_

#include "p3dtest_includes.h"
#include "pandabase.h"
#include "typedReferenceCount.h"
#include "networkObject.h"
#include "networkTools.h"

class InputMemoryBitStream;

/**
 * NetworkWorld class.
 *
 * The world tracks all the live game objects. Fairly inefficient for now, but
 * not that much of a problem.
 */
class EXPCL_P3DTEST NetworkWorld: public TypedReferenceCount, public Singleton<NetworkWorld>
{
PUBLISHED:
	typedef void (*HandleWelcomePacketCallback)(InputMemoryBitStream&);
	typedef void (*UpdateCallback)(PT(NetworkWorld));

	NetworkWorld();
	virtual ~NetworkWorld();
	INLINE static NetworkWorld* get_global_ptr();
	void add_network_object(PT(NetworkObject) inNetworkObject);
	void remove_network_object(PT(NetworkObject) inNetworkObject);
	void call_inputstream_callbacks(InputMemoryBitStream& inInputStream);
	void update();
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	void set_handle_welcome_packet_callback(PyObject* clbk);
	void set_update_callback(PyObject* clbk);
#else
	void set_handle_welcome_packet_callback(HandleWelcomePacketCallback clbk);
	void set_update_callback(UpdateCallback clbk);
#endif // PYTHON_BUILD

	INLINE ValueList_NetworkObjectPtr get_network_objects() const;

public:
	NetworkWorld(const NetworkWorld&) = delete;

private:
	pvector<PT(NetworkObject)> mNetworkObjects;
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	PyObject *mHandleWelcomePacketClbk, *mSelf, *mUpdateCallback, *mSelfArg;
#else
	HandleWelcomePacketCallback mHandleWelcomePacketClbk;
	UpdateCallback mUpdateCallback;
#endif // PYTHON_BUILD

TYPED_OBJECT_API_DECL(NetworkWorld,TypedReferenceCount)
};

///inline
#include "networkWorld.I"

#endif /* P3DTEST_SOURCE_NETWORKWORLD_H_ */
