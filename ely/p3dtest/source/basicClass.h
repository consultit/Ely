/**
 * \file basicClass.h
 *
 * \date 2018-08-12
 * \author consultit
 */

#ifndef P3DTEST_SOURCE_BASICCLASS_H_
#define P3DTEST_SOURCE_BASICCLASS_H_

#include "config_module.h"
#include "p3dtest_includes.h"
#include "pandabase.h"
#include "typedClass.h"
#include "pointerTo.h"
#include "../../common/source/commonTools.h"

/**
 * BasicClass class.
 */
class EXPCL_P3DTEST BasicClass
{
PUBLISHED:
	INLINE BasicClass();
	INLINE virtual ~BasicClass();

	INLINE virtual void set_value(int n);
	INLINE int get_value() const;
	MAKE_PROPERTY(value, get_value, set_value);

	virtual void set_value_alt(int n);
	int get_value_alt() const;
	MAKE_PROPERTY(value_alt, get_value_alt, set_value_alt);

	// argument substitution
	void set_typed_class(TypedClass& cl);//1 attempt
	void set_typed_class(TypedClass* cl);//2 attempt
	void set_typed_class(const TypedClass& cl);//3 attempt
	void set_typed_class(PT(TypedClass) cl);//4 attempt

public:
    static inline void* operator new(size_t size);
    static inline void operator delete(void * ptr);

private:
	int _value;
};

#include "basicClass.I"

#endif /* P3DTEST_SOURCE_BASICCLASS_H_ */
