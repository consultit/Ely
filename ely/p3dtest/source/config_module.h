#ifndef P3DTEST_SOURCE_CONFIG_P3DTEST_H_
#define P3DTEST_SOURCE_CONFIG_P3DTEST_H_

#pragma once

#include "p3dtestsymbols.h"
#include "pandabase.h"
#include "notifyCategoryProxy.h"
#include "configVariableDouble.h"
#include "configVariableString.h"
#include "configVariableInt.h"


NotifyCategoryDecl(p3test, EXPCL_P3DTEST, EXPTP_P3DTEST);

extern void init_libp3test();

#endif //P3DTEST_SOURCE_CONFIG_P3DTEST_H_
