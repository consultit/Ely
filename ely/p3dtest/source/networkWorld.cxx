/**
 * \file networkWorld.cxx
 *
 * \date 2018-07-23
 * \author consultit
 */

#include "networkWorld.h"
#include "memoryBitStream.h"
#ifdef PYTHON_BUILD
#include "py_panda.h"
extern Dtool_PyTypedObject Dtool_NetworkWorld;
extern Dtool_PyTypedObject Dtool_InputMemoryBitStream;
#endif //PYTHON_BUILD

///NetworkWorld definitions

/**
 *
 */
NetworkWorld::NetworkWorld()
{
	PRINT_DEBUG("NetworkWorld::NetworkWorld()");
#ifdef PYTHON_BUILD
	mSelf = nullptr;
	mSelfArg = nullptr;
#endif //PYTHON_BUILD
	mHandleWelcomePacketClbk = nullptr;
	mUpdateCallback = nullptr;
	//
}

/**
 *
 */
NetworkWorld::~NetworkWorld()
{
	PRINT_DEBUG("NetworkWorld::~NetworkWorld()");
#ifdef PYTHON_BUILD
	//Python callback
	Py_XDECREF(mHandleWelcomePacketClbk);
	PY_SELF_ARG_ONLY_DECREF(mUpdateCallback)
	mSelf = nullptr;
	mSelfArg = nullptr;
#endif //PYTHON_BUILD
	mHandleWelcomePacketClbk = nullptr;
	mUpdateCallback = nullptr;
}
#ifdef PYTHON_BUILD
/**
 *
 */
void NetworkWorld::set_handle_welcome_packet_callback(PyObject* clbk)
{
	// remove any callback
	Py_XDECREF(mHandleWelcomePacketClbk);
	mHandleWelcomePacketClbk = nullptr;
	if (clbk == Py_None)
	{
		// done
		return;
	}
	else if (PyCallable_Check(clbk))
	{
		// add the callback
		mHandleWelcomePacketClbk = clbk;
		Py_INCREF(mHandleWelcomePacketClbk);
	}
	else
	{
		PyErr_SetString(PyExc_TypeError,
				"Error: the argument must be callable or None");
		return;
	}
}

/**
 *
 */
void NetworkWorld::set_update_callback(PyObject* clbk)
{
	PY_SET_CALLBACK_SELF_ARG_ONLY_BODY(Dtool_NetworkWorld, mUpdateCallback,
			"NetworkWorld.set_update_callback()")
}
#else
/**
 *
 */
void NetworkWorld::set_handle_welcome_packet_callback(
		HandleWelcomePacketCallback clbk)
{
	mHandleWelcomePacketClbk = clbk;
}

/**
 *
 */
void NetworkWorld::set_update_callback(UpdateCallback clbk)
{
	mUpdateCallback = clbk;
}
#endif //PYTHON_BUILD

/**
 *
 */
void NetworkWorld::add_network_object(PT(NetworkObject) inNetworkObject)
{
	mNetworkObjects.push_back(inNetworkObject);
	inNetworkObject->set_index_in_world(mNetworkObjects.size() - 1);
}

/**
 *
 */
void NetworkWorld::remove_network_object(PT(NetworkObject) inNetworkObject)
{
	int index = inNetworkObject->get_index_in_world();

	int lastIndex = mNetworkObjects.size() - 1;
	if (index != lastIndex)
	{
		mNetworkObjects[index] = mNetworkObjects[lastIndex];
		mNetworkObjects[index]->set_index_in_world(index);
	}

	inNetworkObject->set_index_in_world(-1);

	mNetworkObjects.pop_back();
}

void NetworkWorld::call_inputstream_callbacks(InputMemoryBitStream& inInputStream)
{
	if (mHandleWelcomePacketClbk)
	{
#ifdef PYTHON_BUILD
		PyObject *inInputStreamObj, *argList, *result;
		// create the argument object
		inInputStreamObj = DTool_CreatePyInstance(&inInputStream,
				Dtool_InputMemoryBitStream, false, false);
		// set the arguments
		argList = Py_BuildValue("(O)", inInputStreamObj);
		if (argList == nullptr)
		{
			PyErr_SetString(PyExc_TypeError,
					"Error: cannot install the 'handle welcome packet' callback");
			Py_DECREF(inInputStreamObj);
			return;
		}
		// call the callback
		result = PyObject_CallObject(mHandleWelcomePacketClbk, argList);
		Py_DECREF(argList);
		Py_DECREF(inInputStreamObj);
		if (result == nullptr)
		{
			string errStr =
					string("Error calling 'handle welcome packet' callback function");
			PyErr_SetString(PyExc_TypeError, errStr.c_str());
			return;
		}
		Py_DECREF(result);
#else
		mHandleWelcomePacketClbk(inInputStream);
#endif //PYTHON_BUILD
	}
}

/**
 *
 */
void NetworkWorld::update()
{
	//update all game objects- sometimes they want to die, so we need to treat carefully...

	for (int i = 0, c = mNetworkObjects.size(); i < c; ++i)
	{
		PT(NetworkObject) go = mNetworkObjects[i];

		if (!go->get_does_want_to_die())
		{
			go->update();
		}
		//you might suddenly want to die after your update, so check again
		if (go->get_does_want_to_die())
		{
			remove_network_object(go);
			go->handle_dying();
			--i;
			--c;
		}
	}
	//
	if (mUpdateCallback)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_ONLY_CALL(mUpdateCallback, "NetworkWorld::update()")
#else
		// call c++ callback
		mUpdateCallback(this);
#endif //PYTHON_BUILD
	}
}

TYPED_OBJECT_API_DEF(NetworkWorld)
