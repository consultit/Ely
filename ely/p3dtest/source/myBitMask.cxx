/**
 * PANDA 3D SOFTWARE
 * Copyright (c) Carnegie Mellon University.  All rights reserved.
 *
 * All use of this software is subject to the terms of the revised BSD
 * license.  You should have received a copy of this license along
 * with this source code in a file named "LICENSE."
 *
 * @file bitMask.cxx
 * @author drose
 * @date 2000-06-08
 */

#include "myBitMask.h"

template<> void MyBitMask<uint16_t, 16>::invert_in_place(){}

//template class MyBitMask<uint16_t, 16>; XXX
template class MyBitMask<uint32_t, 32>;
template class MyBitMask<uint64_t, 64>;


#if !defined(CPPPARSER) && !defined(__APPLE__)
#include <type_traits>

static_assert(std::is_literal_type<MyBitMask16>::value, "MyBitMask16 is not a literal type");
static_assert(std::is_literal_type<MyBitMask32>::value, "MyBitMask32 is not a literal type");
static_assert(std::is_literal_type<MyBitMask64>::value, "MyBitMask64 is not a literal type");
#endif
