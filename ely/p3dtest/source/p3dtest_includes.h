/**
 * \file p3dtest_includes.h
 *
 * \date 2018-09-17
 * \author consultit
 */

#ifndef P3DTEST_SOURCE_P3DTEST_INCLUDES_H_
#define P3DTEST_SOURCE_P3DTEST_INCLUDES_H_

#include "p3dtestsymbols.h"

#include "../../common/source/commonTools.h"
#include "../../common/source/commonMacros.h"

#include "pairs_valueLists.h"
#include "utilities.h"

#ifdef CPPPARSER
//Panda3d interrogate fake declarations

#endif //CPPPARSER

#endif /* P3DTEST_SOURCE_P3DTEST_INCLUDES_H_ */
