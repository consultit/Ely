/**
 * \file typedClass.cxx
 *
 * \date 2018-08-12
 * \author consultit
 */

#include "typedClass.h"

TypeHandle TypedClass::_type_handle;

/**
 *
 */
void TypedClass::set_value_alt(int n)
{
	_value = n;
}

/**
 *
 */
int TypedClass::get_value_alt() const
{
	return _value;
}
