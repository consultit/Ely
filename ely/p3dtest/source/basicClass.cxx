/**
 * \file basicClass.cxx
 *
 * \date 2018-08-12
 * \author consultit
 */

#include "basicClass.h"
#include <iostream>

/**
 *
 */
void BasicClass::set_value_alt(int n)
{
	_value = n;
}

/**
 *
 */
int BasicClass::get_value_alt() const
{
	return _value;
}

/*
 *
 */
void BasicClass::set_typed_class(TypedClass& cl)
{
	std::cout << "BasicClass::set_typed_class(TypedClass& cl)" << std::endl;
}

/*
 *
 */
void BasicClass::set_typed_class(TypedClass* cl)
{
	std::cout << "BasicClass::set_typed_class(TypedClass* cl)" << std::endl;
}

/*
 *
 */
void BasicClass::set_typed_class(const TypedClass& cl)
{
	std::cout << "BasicClass::set_typed_class(const TypedClass& cl)" << std::endl;
}

/*
 *
 */
void BasicClass::set_typed_class(PT(TypedClass) cl)
{
	std::cout << "BasicClass::set_typed_class(PT(TypedClass) cl)" << std::endl;
}
