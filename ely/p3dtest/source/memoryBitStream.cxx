/**
 * \file memoryBitStream.cxx
 *
 * \date 2018-07-22
 * \author consultit
 */

#include "memoryBitStream.h"

///OutputMemoryBitStream definitions

/**
 *
 */
void OutputMemoryBitStream::write_bits(uint8_t inData, uint32_t inBitCount)
{
	uint32_t nextBitHead = mBitHead + static_cast<uint32_t>(inBitCount);

	if (nextBitHead > mBitCapacity)
	{
		do_realloc_buffer(max(mBitCapacity * 2, nextBitHead));
	}

	//calculate the byteOffset into our buffer
	//by dividing the head by 8
	//and the bitOffset by taking the last 3 bits
	uint32_t byteOffset = mBitHead >> 3;
	uint32_t bitOffset = mBitHead & 0x7;

	uint8_t currentMask = ~(0xff << bitOffset);
	mBuffer[byteOffset] = (mBuffer[byteOffset] & currentMask)
			| (inData << bitOffset);

	//calculate how many bits were not yet used in
	//our target byte in the buffer
	uint32_t bitsFreeThisByte = 8 - bitOffset;

	//if we needed more than that, carry to the next byte
	if (bitsFreeThisByte < inBitCount)
	{
		//we need another byte
		mBuffer[byteOffset + 1] = inData >> bitsFreeThisByte;
	}

	mBitHead = nextBitHead;
}

/**
 *
 */
void OutputMemoryBitStream::write_bits(const void* inData, uint32_t inBitCount)
{
	const char* srcByte = static_cast<const char*>(inData);
	//write all the bytes
	while (inBitCount > 8)
	{
		write_bits(*srcByte, 8);
		++srcByte;
		inBitCount -= 8;
	}
	//write anything left
	if (inBitCount > 0)
	{
		write_bits(*srcByte, inBitCount);
	}
}

/**
 *
 */
void OutputMemoryBitStream::write(const LVecBase3f& inVector)
{
	write(inVector.get_x());
	write(inVector.get_y());
	write(inVector.get_z());
}

/**
 *
 */
void OutputMemoryBitStream::write(const LVecBase4f& inVector)
{
	write(inVector.get_x());
	write(inVector.get_y());
	write(inVector.get_z());
	write(inVector.get_w());
}

/**
 *
 */
void OutputMemoryBitStream::write(const LQuaternionf& inQuat)
{
	float precision = (2.f / 65535.f);
	write(ConvertToFixed(inQuat.get_i(), -1.f, precision), 16);
	write(ConvertToFixed(inQuat.get_j(), -1.f, precision), 16);
	write(ConvertToFixed(inQuat.get_k(), -1.f, precision), 16);
	write(inQuat.get_r() < 0);
}

/**
 *
 */
void OutputMemoryBitStream::do_realloc_buffer(uint32_t inNewBitLength)
{
	uint32_t inNewByteLength = (inNewBitLength + 7) >> 3;
	if (mBuffer == nullptr)
	{
		//just need to memset on first allocation
		mBuffer = static_cast<char*>(malloc(inNewByteLength));
		memset(mBuffer, 0, inNewByteLength);
	}
	else
	{
		//need to memset, then copy the buffer
		char* tempBuffer = static_cast<char*>(malloc(inNewByteLength));
		memset(tempBuffer, 0, inNewByteLength);
		memcpy(tempBuffer, mBuffer, (mBitCapacity + 7) >> 3);
		free(mBuffer);
		mBuffer = tempBuffer;
	}

	//handle realloc failure
	//...
	mBitCapacity = inNewBitLength;
}

///InputMemoryBitStream definitions

/**
 *
 */
void InputMemoryBitStream::read_bits(uint8_t& outData, uint32_t inBitCount)
{
	uint32_t byteOffset = mBitHead >> 3;
	uint32_t bitOffset = mBitHead & 0x7;

	outData = static_cast<uint8_t>(mBuffer[byteOffset]) >> bitOffset;

	uint32_t bitsFreeThisByte = 8 - bitOffset;
	if (bitsFreeThisByte < inBitCount)
	{
		//we need another byte
		outData |= static_cast<uint8_t>(mBuffer[byteOffset + 1])
				<< bitsFreeThisByte;
	}

	//don't forget a mask so that we only read the bit we wanted...
	outData &= (~(0x00ff << inBitCount));

	mBitHead += inBitCount;
}

/**
 *
 */
void InputMemoryBitStream::read_bits(void* outData, uint32_t inBitCount)
{
	uint8_t* destByte = reinterpret_cast<uint8_t*>(outData);
	//write all the bytes
	while (inBitCount > 8)
	{
		read_bits(*destByte, 8);
		++destByte;
		inBitCount -= 8;
	}
	//write anything left
	if (inBitCount > 0)
	{
		read_bits(*destByte, inBitCount);
	}
}

/**
 *
 */
void InputMemoryBitStream::read(LVecBase3f& outVector)
{
	read(outVector[0]);
	read(outVector[1]);
	read(outVector[2]);
}

/**
 *
 */
void InputMemoryBitStream::read(LVecBase4f& outVector)
{
	read(outVector[0]);
	read(outVector[1]);
	read(outVector[2]);
	read(outVector[3]);
}

/**
 *
 */
void InputMemoryBitStream::read(LQuaternionf& outQuat)
{
	float precision = (2.f / 65535.f);

	uint32_t f = 0;

	read(f, 16);
	outQuat.set_i(ConvertFromFixed(f, -1.f, precision));
	read(f, 16);
	outQuat.set_j(ConvertFromFixed(f, -1.f, precision));
	read(f, 16);
	outQuat.set_k(ConvertFromFixed(f, -1.f, precision));

	outQuat.set_r(
			sqrtf(
					1.f
							- (outQuat.get_i() * outQuat.get_i()
									+ outQuat.get_j() * outQuat.get_j()
									+ outQuat.get_k() * outQuat.get_k())));
	bool isNegative;
	read(isNegative);

	if (isNegative)
	{
		outQuat.set_r(outQuat.get_r() * (-1));
	}
}
