/**
 * \file networkWorld.I
 *
 * \date 2018-07-23
 * \author consultit
 */

#ifndef P3DTEST_SOURCE_NETWORKWORLD_I_
#define P3DTEST_SOURCE_NETWORKWORLD_I_

///NetworkWorld inline definitions

/**
 *
 */
INLINE NetworkWorld* NetworkWorld::get_global_ptr()
{
	return Singleton < NetworkWorld > ::GetSingletonPtr();
}

/**
 *
 */
INLINE ValueList_NetworkObjectPtr NetworkWorld::get_network_objects() const
{
	return mNetworkObjects;
}

#endif /* P3DTEST_SOURCE_NETWORKWORLD_I_ */
