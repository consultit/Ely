/**
 * \file opensteer_includes.h
 *
 * \date 2016-09-16
 * \author consultit
 */

#ifndef AI_SOURCE_OPENSTEER_INCLUDES_H_
#define AI_SOURCE_OPENSTEER_INCLUDES_H_

#include "aisymbols.h"

#include "../../common/source/commonTools.h"
#include "../../common/source/commonMacros.h"

#include "pairs_valueLists.h"
#include "utilities.h"

#ifdef CPPPARSER
//Panda3d interrogate fake declarations

namespace OpenSteer
{
	struct AbstractObstacle;
	struct AbstractPlugIn;
	struct ObstacleGroup;
	struct AbstractVehicle;
}
namespace ossup
{
	struct VehicleSettings;
	template<typename T1, typename T2> struct VehicleAddOnMixin;
	template<typename T1> struct CtfBase{enum seekerState;};
}

#endif //CPPPARSER

#endif /* AI_SOURCE_OPENSTEER_INCLUDES_H_ */
