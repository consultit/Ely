/**
 * \file aiTools.h
 *
 * \date 2016-09-17
 * \author consultit
 */

#ifndef AI_SOURCE_AITOOLS_H_
#define AI_SOURCE_AITOOLS_H_

#include "pandabase.h"
#include "lvector3.h"
#include "lpoint3.h"
#include "opensteer_includes.h"
#include "recastnavigation_includes.h"

//
#ifndef CPPPARSER
#include "support_os/common.h"
#include "support_os/DrawMeshDrawer.h"
#include "support_rn/NavMeshType.h"
#endif //CPPPARSER

///Vehicle settings.
class OSSteerVehicle;
/**
 * Class used as getter/setter for these OSSteerVehicle' parameters:
 * - mass : float
 * - radius : float
 * - speed : float
 * - max force : float
 * - max speed : float
 * - forward : LVector3f
 * - side : LVector3f
 * - up : LVector3f
 * - position : LPoint3f
 * - start : LPoint3f
 * - path prediction time : float
 * - obstacle min time collision : float
 * - neighbor min time collision : float
 * - neighbor min separation distance : float
 * - separation max distance : float
 * - separation cos max angle : float
 * - alignment max distance : float
 * - alignment cos max angle : float
 * - cohesion max distance : float
 * - cohesion cos max angle : float
 * - pursuit max prediction time : float
 * - evasion max prediction time : float
 * - target speed : float
 */
struct EXPCL_AI OSVehicleSettings
{
PUBLISHED:
	INLINE void set_mass(float value);
	INLINE float get_mass() const;
	INLINE void set_radius(float value);
	INLINE float get_radius() const;
	INLINE void set_speed(float value);
	INLINE float get_speed() const;
	INLINE void set_max_force(float value);
	INLINE float get_max_force() const;
	INLINE void set_max_speed(float value);
	INLINE float get_max_speed() const;
	INLINE void set_forward(const LVector3f& value);
	INLINE LVector3f get_forward() const;
	INLINE void set_side(const LVector3f& value);
	INLINE LVector3f get_side() const;
	INLINE void set_up(const LVector3f& value);
	INLINE LVector3f get_up() const;
	INLINE void set_position(const LPoint3f& value);
	INLINE LPoint3f get_position() const;
	INLINE void set_start(const LPoint3f& value);
	INLINE LPoint3f get_start() const;
	INLINE void set_path_pred_time(float value);
	INLINE float get_path_pred_time() const;
	INLINE void set_obstacle_min_time_coll(float value);
	INLINE float get_obstacle_min_time_coll() const;
	INLINE void set_neighbor_min_time_coll(float value);
	INLINE float get_neighbor_min_time_coll() const;
	INLINE void set_neighbor_min_sep_dist(float value);
	INLINE float get_neighbor_min_sep_dist() const;
	INLINE void set_separation_max_dist(float value);
	INLINE float get_separation_max_dist() const;
	INLINE void set_separation_cos_max_angle(float value);
	INLINE float get_separation_cos_max_angle() const;
	INLINE void set_alignment_max_dist(float value);
	INLINE float get_alignment_max_dist() const;
	INLINE void set_alignment_cos_max_angle(float value);
	INLINE float get_alignment_cos_max_angle() const;
	INLINE void set_cohesion_max_dist(float value);
	INLINE float get_cohesion_max_dist() const;
	INLINE void set_cohesion_cos_max_angle(float value);
	INLINE float get_cohesion_cos_max_angle() const;
	INLINE void set_pursuit_max_pred_time(float value);
	INLINE float get_pursuit_max_pred_time() const;
	INLINE void set_evasion_max_pred_time(float value);
	INLINE float get_evasion_max_pred_time() const;
	INLINE void set_target_speed(float value);
	INLINE float get_target_speed() const;
	// Python Properties
	MAKE_PROPERTY(mass, get_mass, set_mass);
	MAKE_PROPERTY(radius, get_radius, set_radius);
	MAKE_PROPERTY(speed, get_speed, set_speed);
	MAKE_PROPERTY(max_force, get_max_force, set_max_force);
	MAKE_PROPERTY(max_speed, get_max_speed, set_max_speed);
	MAKE_PROPERTY(forward, get_forward, set_forward);
	MAKE_PROPERTY(side, get_side, set_side);
	MAKE_PROPERTY(up, get_up, set_up);
	MAKE_PROPERTY(position, get_position, set_position);
	MAKE_PROPERTY(start, get_start, set_start);
	MAKE_PROPERTY(path_pred_time, get_path_pred_time, set_path_pred_time);
	MAKE_PROPERTY(obstacle_min_time_coll, get_obstacle_min_time_coll, set_obstacle_min_time_coll);
	MAKE_PROPERTY(neighbor_min_time_coll, get_neighbor_min_time_coll, set_neighbor_min_time_coll);
	MAKE_PROPERTY(neighbor_min_sep_dist, get_neighbor_min_sep_dist, set_neighbor_min_sep_dist);
	MAKE_PROPERTY(separation_max_dist, get_separation_max_dist, set_separation_max_dist);
	MAKE_PROPERTY(separation_cos_max_angle, get_separation_cos_max_angle, set_separation_cos_max_angle);
	MAKE_PROPERTY(alignment_max_dist, get_alignment_max_dist, set_alignment_max_dist);
	MAKE_PROPERTY(alignment_cos_max_angle, get_alignment_cos_max_angle, set_alignment_cos_max_angle);
	MAKE_PROPERTY(cohesion_max_dist, get_cohesion_max_dist, set_cohesion_max_dist);
	MAKE_PROPERTY(cohesion_cos_max_angle, get_cohesion_cos_max_angle, set_cohesion_cos_max_angle);
	MAKE_PROPERTY(pursuit_max_pred_time, get_pursuit_max_pred_time, set_pursuit_max_pred_time);
	MAKE_PROPERTY(evasion_max_pred_time, get_evasion_max_pred_time, set_evasion_max_pred_time);
	MAKE_PROPERTY(target_speed, get_target_speed, set_target_speed);

	void output(ostream &out) const;

public:
	typedef ossup::VehicleAddOnMixin<ossup::SimpleVehicle, OSSteerVehicle> VehicleAddOn;
	OSVehicleSettings(VehicleAddOn& vehicle) :
		_vehicle(vehicle)
	{
	}
#ifndef CPPPARSER
	operator VehicleAddOn&() const
	{
		return _vehicle;
	}

private:
	VehicleAddOn& _vehicle;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const OSVehicleSettings & settings);

///Flock settings.
struct EXPCL_AI OSFlockSettings
{
PUBLISHED:
	OSFlockSettings();
#ifndef CPPPARSER
	OSFlockSettings(float sW, float aW,	float cW);
#endif //CPPPARSER

	INLINE void set_separation_weight(float value);
	INLINE float get_separation_weight() const;
	INLINE void set_alignment_weight(float value);
	INLINE float get_alignment_weight() const;
	INLINE void set_cohesion_weight(float value);
	INLINE float get_cohesion_weight() const;
	// Python Properties
	MAKE_PROPERTY(separation_weight, get_separation_weight, set_separation_weight);
	MAKE_PROPERTY(alignment_weight, get_alignment_weight, set_alignment_weight);
	MAKE_PROPERTY(cohesion_weight, get_cohesion_weight, set_cohesion_weight);

	void output(ostream &out) const;
#ifndef CPPPARSER
	float& separation_weight(){return _separationWeight;}
	float& alignment_weight(){return _alignmentWeight;}
	float& cohesion_weight(){return _cohesionWeight;}

private:
	float _separationWeight;
	float _alignmentWeight;
	float _cohesionWeight;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const OSFlockSettings & settings);

///OSObstacleSettings.
struct EXPCL_AI OSObstacleSettings
{
PUBLISHED:
	OSObstacleSettings();

	INLINE bool operator==(
			const OSObstacleSettings &other) const;
	INLINE void set_type(const string& value);
	INLINE string get_type() const;
	INLINE void set_seen_from_state(const string& value);
	INLINE string get_seen_from_state() const;
	INLINE void set_position(const LPoint3f& value);
	INLINE const LPoint3f& get_position() const;
	INLINE void set_forward(const LVector3f& value);
	INLINE const LVector3f& get_forward() const;
	INLINE void set_up(const LVector3f& value);
	INLINE const LVector3f& get_up() const;
	INLINE void set_side(const LVector3f& value);
	INLINE const LVector3f& get_side() const;
	INLINE void set_width(float value);
	INLINE float get_width() const;
	INLINE void set_height(float value);
	INLINE float get_height() const;
	INLINE void set_depth(float value);
	INLINE float get_depth() const;
	INLINE void set_radius(float value);
	INLINE float get_radius() const;
	INLINE void set_ref(int value);
	INLINE int get_ref() const;
	// Python Properties
	MAKE_PROPERTY(type, get_type, set_type);
	MAKE_PROPERTY(seenFromState, get_seen_from_state, set_seen_from_state);
	MAKE_PROPERTY(position, get_position, set_position);
	MAKE_PROPERTY(forward, get_forward, set_forward);
	MAKE_PROPERTY(up, get_up, set_up);
	MAKE_PROPERTY(side, get_side, set_side);
	MAKE_PROPERTY(width, get_width, set_width);
	MAKE_PROPERTY(height, get_height, set_height);
	MAKE_PROPERTY(depth, get_depth, set_depth);
	MAKE_PROPERTY(radius, get_radius, set_radius);
	MAKE_PROPERTY(ref, get_ref, set_ref);

	void output(ostream &out) const;
public:
	inline OpenSteer::AbstractObstacle* get_obstacle() const;
	inline void set_obstacle(OpenSteer::AbstractObstacle* value);

#ifndef CPPPARSER
private:
	string _type;
	string _seenFromState;
	LPoint3f _position;
	LVector3f _forward;
	LVector3f _up;
	LVector3f _side;
	float _width;
	float _height;
	float _depth;
	float _radius;
	int _ref;
	//not serialized
	OpenSteer::AbstractObstacle* _obstacle;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const OSObstacleSettings & settings);

///NavMesh settings.
struct EXPCL_AI RNNavMeshSettings
{
PUBLISHED:
	RNNavMeshSettings();
#ifndef CPPPARSER
	RNNavMeshSettings(const rnsup::NavMeshSettings& settings) :
			_navMeshSettings(settings)
	{
	}
	operator rnsup::NavMeshSettings() const
	{
		return _navMeshSettings;
	}
#endif
	INLINE void set_cell_size(float value);
	INLINE float get_cell_size() const;
	INLINE void set_cell_height(float value);
	INLINE float get_cell_height() const;
	INLINE void set_agent_height(float value);
	INLINE float get_agent_height() const;
	INLINE void set_agent_radius(float value);
	INLINE float get_agent_radius() const;
	INLINE void set_agent_max_climb(float value);
	INLINE float get_agent_max_climb() const;
	INLINE void set_agent_max_slope(float value);
	INLINE float get_agent_max_slope() const;
	INLINE void set_region_min_size(float value);
	INLINE float get_region_min_size() const;
	INLINE void set_region_merge_size(float value);
	INLINE float get_region_merge_size() const;
	INLINE void set_edge_max_len(float value);
	INLINE float get_edge_max_len() const;
	INLINE void set_edge_max_error(float value);
	INLINE float get_edge_max_error() const;
	INLINE void set_verts_per_poly(float value);
	INLINE float get_verts_per_poly() const;
	INLINE void set_detail_sample_dist(float value);
	INLINE float get_detail_sample_dist() const;
	INLINE void set_detail_sample_max_error(float value);
	INLINE float get_detail_sample_max_error() const;
	INLINE void set_partition_type(int value);
	INLINE int get_partition_type() const;
	// Python Properties
	MAKE_PROPERTY(cell_size, get_cell_size, set_cell_size);
	MAKE_PROPERTY(cell_height, get_cell_height, set_cell_height);
	MAKE_PROPERTY(agent_height, get_agent_height, set_agent_height);
	MAKE_PROPERTY(agent_radius, get_agent_radius, set_agent_radius);
	MAKE_PROPERTY(agent_max_climb, get_agent_max_climb, set_agent_max_climb);
	MAKE_PROPERTY(agent_max_slope, get_agent_max_slope, set_agent_max_slope);
	MAKE_PROPERTY(region_min_size, get_region_min_size, set_region_min_size);
	MAKE_PROPERTY(region_merge_size, get_region_merge_size, set_region_merge_size);
	MAKE_PROPERTY(edge_max_len, get_edge_max_len, set_edge_max_len);
	MAKE_PROPERTY(edge_max_error, get_edge_max_error, set_edge_max_error);
	MAKE_PROPERTY(verts_per_poly, get_verts_per_poly, set_verts_per_poly);
	MAKE_PROPERTY(detail_sample_dist, get_detail_sample_dist, set_detail_sample_dist);
	MAKE_PROPERTY(detail_sample_max_error, get_detail_sample_max_error, set_detail_sample_max_error);
	MAKE_PROPERTY(partition_type, get_partition_type, set_partition_type);

	void output(ostream &out) const;

#ifndef CPPPARSER
private:
	rnsup::NavMeshSettings _navMeshSettings;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const RNNavMeshSettings & settings);

///NavMesh tile settings.
struct EXPCL_AI RNNavMeshTileSettings
{
PUBLISHED:
	RNNavMeshTileSettings();
#ifndef CPPPARSER
	RNNavMeshTileSettings(const rnsup::NavMeshTileSettings& settings) :
			_navMeshTileSettings(settings)
	{
	}
	operator rnsup::NavMeshTileSettings() const
	{
		return _navMeshTileSettings;
	}
#endif //CPPPARSER
	INLINE void set_build_all_tiles(bool value);
	INLINE bool get_build_all_tiles() const;
	INLINE void set_max_tiles(int value);
	INLINE int get_max_tiles() const;
	INLINE void set_max_polys_per_tile(int value);
	INLINE int get_max_polys_per_tile() const;
	INLINE void set_tile_size(float value);
	INLINE float get_tile_size() const;
	// Python Properties
	MAKE_PROPERTY(build_all_tiles, get_build_all_tiles, set_build_all_tiles);
	MAKE_PROPERTY(max_tiles, get_max_tiles, set_max_tiles);
	MAKE_PROPERTY(max_polys_per_tile, get_max_polys_per_tile, set_max_polys_per_tile);
	MAKE_PROPERTY(tile_size, get_tile_size, set_tile_size);

	void output(ostream &out) const;

#ifndef CPPPARSER
private:
	rnsup::NavMeshTileSettings _navMeshTileSettings;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const RNNavMeshTileSettings & settings);

///Convex volume settings.
struct EXPCL_AI RNConvexVolumeSettings
{
PUBLISHED:
	RNConvexVolumeSettings();

	INLINE bool operator== (const RNConvexVolumeSettings &other) const;
	INLINE void set_area(int value);
	INLINE int get_area() const;
	INLINE void set_flags(int value);
	INLINE int get_flags() const;
	INLINE void set_centroid(LPoint3f value);
	INLINE const LPoint3f& get_centroid() const;
	INLINE void set_ref(int value);
	INLINE int get_ref() const;
	// Python Properties
	MAKE_PROPERTY(area, get_area, set_area);
	MAKE_PROPERTY(flags, get_flags, set_flags);
	MAKE_PROPERTY(centroid, get_centroid, set_centroid);
	MAKE_PROPERTY(ref, get_ref, set_ref);

	void output(ostream &out) const;

#ifndef CPPPARSER
private:
	int _area;
	int _flags;
	LPoint3f _centroid;
	int _ref;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const RNConvexVolumeSettings & settings);

///Off mesh connection settings.
struct EXPCL_AI RNOffMeshConnectionSettings
{
PUBLISHED:
	RNOffMeshConnectionSettings();

	INLINE bool operator==(
			const RNOffMeshConnectionSettings &other) const;
	INLINE void set_rad(float value);
	INLINE float get_rad() const;
	INLINE void set_bidir(bool value);
	INLINE bool get_bidir() const;
	INLINE void set_user_id(unsigned int value);
	INLINE unsigned int get_user_id() const;
	INLINE void set_area(int value);
	INLINE int get_area() const;
	INLINE void set_flags(int value);
	INLINE int get_flags() const;
	INLINE void set_ref(int value);
	INLINE int get_ref() const;
	// Python Properties
	MAKE_PROPERTY(rad, get_rad, set_rad);
	MAKE_PROPERTY(bidir, get_bidir, set_bidir);
	MAKE_PROPERTY(user_id, get_user_id, set_user_id);
	MAKE_PROPERTY(area, get_area, set_area);
	MAKE_PROPERTY(flags, get_flags, set_flags);
	MAKE_PROPERTY(ref, get_ref, set_ref);

	void output(ostream &out) const;

#ifndef CPPPARSER
private:
	float _rad;
	bool _bidir;
	unsigned int _userId;
	int _area;
	int _flags;
	int _ref;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const RNOffMeshConnectionSettings & settings);

///Obstacle settings.
struct EXPCL_AI RNObstacleSettings
{
PUBLISHED:
	RNObstacleSettings();

	INLINE bool operator==(
			const RNObstacleSettings &other) const;
	INLINE void set_radius(float value);
	INLINE float get_radius() const;
	INLINE void set_dims(const LVecBase3f& value);
	INLINE const LVecBase3f& get_dims() const;
	INLINE void set_ref(unsigned int value);
	INLINE unsigned int get_ref() const;
	// Python Properties
	MAKE_PROPERTY(radius, get_radius, set_radius);
	MAKE_PROPERTY(dims, get_dims, set_dims);
	MAKE_PROPERTY(ref, get_ref, set_ref);

	void output(ostream &out) const;

#ifndef CPPPARSER
private:
	float _radius;
	LVecBase3f _dims;
	unsigned int _ref;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const RNObstacleSettings & settings);

///CrowdAgentParams
struct EXPCL_AI RNCrowdAgentParams
{
PUBLISHED:
	RNCrowdAgentParams();
#ifndef CPPPARSER
	RNCrowdAgentParams(const dtCrowdAgentParams& params) :
			_dtCrowdAgentParams(params)
	{
	}
	operator dtCrowdAgentParams() const
	{
		return _dtCrowdAgentParams;
	}
#endif //CPPPARSER
	INLINE void set_radius(float value);
	INLINE float get_radius() const;
	INLINE void set_height(float value);
	INLINE float get_height() const;
	INLINE void set_max_acceleration(float value);
	INLINE float get_max_acceleration() const;
	INLINE void set_max_speed(float value);
	INLINE float get_max_speed() const;
	INLINE void set_collision_query_range(float value);
	INLINE float get_collision_query_range() const;
	INLINE void set_path_optimization_range(float value);
	INLINE float get_path_optimization_range() const;
	INLINE void set_separation_weight(float value);
	INLINE float get_separation_weight() const;
	INLINE void set_update_flags(unsigned char value);
	INLINE unsigned char get_update_flags() const;
	INLINE void set_obstacle_avoidance_type(unsigned char value);
	INLINE unsigned char get_obstacle_avoidance_type() const;
	INLINE void set_query_filter_type(unsigned char value);
	INLINE unsigned char get_query_filter_type() const;
	INLINE void set_user_data(void* value);
	INLINE void* get_user_data() const;
	// Python Properties
	MAKE_PROPERTY(radius, get_radius, set_radius);
	MAKE_PROPERTY(height, get_height, set_height);
	MAKE_PROPERTY(max_acceleration, get_max_acceleration, set_max_acceleration);
	MAKE_PROPERTY(max_speed, get_max_speed, set_max_speed);
	MAKE_PROPERTY(collision_query_range, get_collision_query_range, set_collision_query_range);
	MAKE_PROPERTY(path_optimization_range, get_path_optimization_range, set_path_optimization_range);
	MAKE_PROPERTY(separation_weight, get_separation_weight, set_separation_weight);
	MAKE_PROPERTY(update_flags, get_update_flags, set_update_flags);
	MAKE_PROPERTY(obstacle_avoidance_type, get_obstacle_avoidance_type, set_obstacle_avoidance_type);
	MAKE_PROPERTY(query_filter_type, get_query_filter_type, set_query_filter_type);
	MAKE_PROPERTY(user_data, get_user_data, set_user_data);

	void output(ostream &out) const;

#ifndef CPPPARSER
private:
	dtCrowdAgentParams _dtCrowdAgentParams;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const RNCrowdAgentParams & params);

///inline
#include "aiTools.I"

#endif /* AI_SOURCE_AITOOLS_H_ */
