#ifndef AI_SOURCE_CONFIG_AI_H_
#define AI_SOURCE_CONFIG_AI_H_

#pragma once

#include "aisymbols.h"
#include "pandabase.h"
#include "notifyCategoryProxy.h"
#include "configVariableDouble.h"
#include "configVariableString.h"
#include "configVariableInt.h"


NotifyCategoryDecl(p3ai, EXPCL_AI, EXPTP_AI);

extern void init_libp3ai();

#endif //AI_SOURCE_CONFIG_AI_H_
