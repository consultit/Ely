/**
 * \file gameAIManager.cxx
 *
 * \date 2016-09-17
 * \author consultit
 */

#include "gameAIManager.h"

#include "osSteerPlugIn.h"
#include "osSteerVehicle.h"
#include "rnCrowdAgent.h"
#include "rnNavMesh.h"
#include "asyncTaskManager.h"
#include "bamFile.h"

///GameAIManager definitions
/**
 *
 */
GameAIManager::GameAIManager(int taskSort, const NodePath& root,
		const CollideMask& mask):
		mReferenceNP{NodePath("ReferenceNode")},
		mTaskSort{taskSort},
		mUtils{root, mask},
		mReferenceDebugNP{NodePath("ReferenceDebugNode")},
		mReferenceDebug2DNP{NodePath("ReferenceDebugNode2D")},
		mRef{0}
{
	PRINT_DEBUG("GameAIManager::GameAIManager: creating the singleton manager.");

	// steer plug-ins
	mSteerPlugIns.clear();
	mSteerPlugInsParameterTable.clear();
	set_parameters_defaults(STEERPLUGIN);
	// steer vehicles
	mSteerVehicles.clear();
	mSteerVehiclesParameterTable.clear();
	set_parameters_defaults(STEERVEHICLE);
	// obstacles
	mObstacles.first().clear();
	mObstacles.second().clear();
	// nav meshes
	mNavMeshes.clear();
	mNavMeshesParameterTable.clear();
	set_parameters_defaults(NAVMESH);
	// crowd agents
	mCrowdAgents.clear();
	mCrowdAgentsParameterTable.clear();
	set_parameters_defaults(CROWDAGENT);
	//
	mUpdateData.clear();
	mUpdateTask.clear();
#ifdef ELY_DEBUG
	mOSDD = nullptr;
	if (!mUtils.get_collision_root().is_empty())
	{
		//create new DebugDrawer
		mOSDD = new ossup::DebugDrawPanda3d(mUtils.get_collision_root());
	}
	mRNDD = nullptr;
	if (!mUtils.get_collision_root().is_empty())
	{
		//create new DebugDrawer
		mRNDD = new DebugDrawPrimitives(mUtils.get_collision_root());
	}
#endif //ELY_DEBUG
}

/**
 *
 */
GameAIManager::~GameAIManager()
{
	PRINT_DEBUG("GameAIManager::~GameAIManager: destroying the singleton manager.");

	//stop any default update
	stop_default_update();
	{
		//destroy all OSSteerVehicles
		PTA(PT(OSSteerVehicle))::iterator iterC = mSteerVehicles.begin();
		while (iterC != mSteerVehicles.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to OSSteerVehicle to cleanup itself before being destroyed.
			(*iterC)->do_finalize();
			//remove the OSSteerVehicles from the inner list (and from the update task)
			iterC = mSteerVehicles.erase(iterC);
		}

		//destroy all OSSteerPlugIns
		PTA(PT(OSSteerPlugIn))::iterator iterN = mSteerPlugIns.begin();
		while (iterN != mSteerPlugIns.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to OSSteerVehicle to cleanup itself before being destroyed.
			(*iterN)->do_finalize();
			//remove the OSSteerVehicles from the inner list (and from the update task)
			iterN = mSteerPlugIns.erase(iterN);
		}
	}
	{
		//destroy all RNCrowdAgents
		PTA(PT(RNCrowdAgent))::iterator iterC = mCrowdAgents.begin();
		while (iterC != mCrowdAgents.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to RNCrowdAgent to cleanup itself before being destroyed.
			(*iterC)->do_finalize();
			//remove the RNCrowdAgents from the inner list (and from the update task)
			iterC = mCrowdAgents.erase(iterC);
		}

		//destroy all RNNavMeshes
		PTA(PT(RNNavMesh))::iterator iterN = mNavMeshes.begin();
		while (iterN != mNavMeshes.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to RNCrowdAgent to cleanup itself before being destroyed.
			(*iterN)->do_finalize();
			//remove the RNCrowdAgents from the inner list (and from the update task)
			iterN = mNavMeshes.erase(iterN);
		}
	}
	//clear parameters' tables
	mSteerPlugInsParameterTable.clear();
	mSteerVehiclesParameterTable.clear();
	mNavMeshesParameterTable.clear();
	mCrowdAgentsParameterTable.clear();

#ifdef ELY_DEBUG
	if (mOSDD)
	{
		delete mOSDD;
		mOSDD = nullptr;
	}
	if (mRNDD)
	{
		delete mRNDD;
		mRNDD = nullptr;
	}
#endif //ELY_DEBUG
}

/**
 * Creates a OSSteerPlugIn with a given (mandatory and not empty) name.
 * Returns a NodePath to the new OSSteerPlugIn,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GameAIManager::create_steer_plug_in(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(OSSteerPlugIn) newSteerPlugIn = new OSSteerPlugIn(name);
	nassertr_always(newSteerPlugIn, NodePath::fail())

	// set reference nodes
	newSteerPlugIn->mReferenceNP = mReferenceNP;
	newSteerPlugIn->mReferenceDebugNP = mReferenceDebugNP;
	newSteerPlugIn->mReferenceDebug2DNP = mReferenceDebug2DNP;
	// initialize the new SteerPlugIn (could use mReferenceNP)
	newSteerPlugIn->do_initialize();

	// add the new SteerPlugIn to the inner list (and to the update task)
	mSteerPlugIns.push_back(newSteerPlugIn);
	// reparent to reference node
	NodePath np = mReferenceNP.attach_new_node(newSteerPlugIn);
	//
	return np;
}

/**
 * Destroys a OSSteerPlugIn.
 * Returns false on error.
 */
bool GameAIManager::destroy_steer_plug_in(NodePath steerPlugInNP)
{
	CONTINUE_IF_ELSE_R(
			steerPlugInNP.node()->is_of_type(OSSteerPlugIn::get_class_type()),
			false)

	PT(OSSteerPlugIn)steerPlugIn = DCAST(OSSteerPlugIn, steerPlugInNP.node());
	SteerPlugInList::iterator iter = find(mSteerPlugIns.begin(),
			mSteerPlugIns.end(), steerPlugIn);
	CONTINUE_IF_ELSE_R(iter != mSteerPlugIns.end(), false)

	//give a chance to OSSteerPlugIn to cleanup itself before being destroyed.
	steerPlugIn->do_finalize();
	//remove the OSSteerPlugIn from the inner list (and from the update task)
	mSteerPlugIns.erase(iter);
	//
	return true;
}

/**
 * Gets an OSSteerPlugIn by index, or nullptr on error.
 */
PT(OSSteerPlugIn) GameAIManager::get_steer_plug_in(int index) const
{
	nassertr_always((index >= 0) && (index < (int ) mSteerPlugIns.size()),
			nullptr)

	return mSteerPlugIns[index];
}

/**
 * Creates a OSSteerVehicle with a given (mandatory and not empty) name.
 * Returns a NodePath to the new OSSteerVehicle,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GameAIManager::create_steer_vehicle(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(OSSteerVehicle)newSteerVehicle = new OSSteerVehicle(name);
	nassertr_always(newSteerVehicle, NodePath::fail())

	// set reference node
	newSteerVehicle->mReferenceNP = mReferenceNP;
	// reparent to reference node and set "this" NodePath
	NodePath np = mReferenceNP.attach_new_node(newSteerVehicle);
	//initialize the new SteerVehicle (could use mReferenceNP and mThisNP)
	newSteerVehicle->do_initialize();

	//add the new SteerVehicle to the inner list
	mSteerVehicles.push_back(newSteerVehicle);
	//
	return np;
}

/**
 * Destroys a OSSteerVehicle.
 * Returns false on error.
 */
bool GameAIManager::destroy_steer_vehicle(NodePath steerVehicleNP)
{
	CONTINUE_IF_ELSE_R(
			steerVehicleNP.node()->is_of_type(OSSteerVehicle::get_class_type()),
			false)

	PT(OSSteerVehicle)steerVehicle = DCAST(OSSteerVehicle, steerVehicleNP.node());
	SteerVehicleList::iterator iter = find(mSteerVehicles.begin(),
			mSteerVehicles.end(), steerVehicle);
	CONTINUE_IF_ELSE_R(iter != mSteerVehicles.end(), false)

	//give a chance to OSSteerVehicle to cleanup itself before being destroyed.
	steerVehicle->do_finalize();
	//remove the OSSteerVehicle from the inner list (and from the update task)
	mSteerVehicles.erase(iter);
	//
	return true;
}

/**
 * Gets an OSSteerVehicle by index, or nullptr on error.
 */
PT(OSSteerVehicle) GameAIManager::get_steer_vehicle(int index) const
{
	nassertr_always((index >= 0) && (index < (int ) mSteerVehicles.size()),
			nullptr)

	return mSteerVehicles[index];
}

/**
 * Creates a RNNavMesh with a given (mandatory and not empty) name.
 * Returns a NodePath to the new RNNavMesh,or an empty NodePath with the ET_fail
 * error type set on error.
 */
NodePath GameAIManager::create_nav_mesh(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(RNNavMesh) newNavMesh = new RNNavMesh(name);
	nassertr_always(newNavMesh, NodePath::fail())

	// set reference nodes
	newNavMesh->mReferenceNP = mReferenceNP;
	newNavMesh->mReferenceDebugNP = mReferenceDebugNP;
	// initialize the new NavMesh  (could use mReferenceNP)
	newNavMesh->do_initialize();

	// add the new NavMesh to the inner list (and to the update task)
	mNavMeshes.push_back(newNavMesh);
	// reparent to reference node
	NodePath np = mReferenceNP.attach_new_node(newNavMesh);
	//
	return np;
}

/**
 * Destroys a RNNavMesh.
 * Returns false on error.
 */
bool GameAIManager::destroy_nav_mesh(NodePath navMeshNP)
{
	CONTINUE_IF_ELSE_R(
			navMeshNP.node()->is_of_type(RNNavMesh::get_class_type()),
			false)

	PT(RNNavMesh) navMesh = DCAST(RNNavMesh, navMeshNP.node());
	NavMeshList::iterator iter = find(mNavMeshes.begin(), mNavMeshes.end(),
			navMesh);
	CONTINUE_IF_ELSE_R(iter != mNavMeshes.end(), false)

	//give a chance to NavMesh to cleanup itself before being destroyed.
	navMesh->do_finalize();
	//remove the NavMesh from the inner list (and from the update task)
	mNavMeshes.erase(iter);
	//
	return true;
}

/**
 * Gets an RNNavMesh by index, or nullptr on error.
 */
PT(RNNavMesh) GameAIManager::get_nav_mesh(int index) const
{
	nassertr_always((index >= 0) && (index < (int ) mNavMeshes.size()),
			nullptr);

	return mNavMeshes[index];
}

/**
 * Creates a RNCrowdAgent with a given (mandatory and not empty) name.
 * Returns a NodePath to the new RNCrowdAgent,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GameAIManager::create_crowd_agent(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(RNCrowdAgent)newCrowdAgent = new RNCrowdAgent(name);
	nassertr_always(newCrowdAgent, NodePath::fail())

	// set reference node
	newCrowdAgent->mReferenceNP = mReferenceNP;
	// reparent to reference node and set "this" NodePath
	NodePath np = mReferenceNP.attach_new_node(newCrowdAgent);
	// initialize the new CrowdAgent (could use mReferenceNP and mThisNP)
	newCrowdAgent->do_initialize();

	//add the new CrowdAgent to the inner list
	mCrowdAgents.push_back(newCrowdAgent);
	//
	return np;
}

/**
 * Destroys a RNCrowdAgent.
 * Returns false on error.
 */
bool GameAIManager::destroy_crowd_agent(NodePath crowdAgentNP)
{
	CONTINUE_IF_ELSE_R(
			crowdAgentNP.node()->is_of_type(RNCrowdAgent::get_class_type()),
			false)

	PT(RNCrowdAgent)crowdAgent = DCAST(RNCrowdAgent, crowdAgentNP.node());
	CrowdAgentList::iterator iter = find(mCrowdAgents.begin(),
			mCrowdAgents.end(), crowdAgent);
	CONTINUE_IF_ELSE_R(iter != mCrowdAgents.end(), false)

	//give a chance to CrowdAgent to cleanup itself before being destroyed.
	crowdAgent->do_finalize();
	//remove the CrowdAgent from the inner list (and from the update task)
	mCrowdAgents.erase(iter);
	//
	return true;
}

/**
 * Gets an RNCrowdAgent by index, or nullptr on error.
 */
PT(RNCrowdAgent) GameAIManager::get_crowd_agent(int index) const
{
	nassertr_always((index >= 0) && (index < (int ) mCrowdAgents.size()),
			nullptr);

	return mCrowdAgents[index];
}

/**
 * Sets a multi-valued parameter to a multi-value overwriting the existing one(s).
 */
void GameAIManager::set_parameter_values(AIType type, const string& paramName,
		const ValueList_string& paramValues)
{
	switch (type)
	{
	case STEERPLUGIN:
		::set_parameter_values(mSteerPlugInsParameterTable, paramName,
				paramValues);
		break;
	case STEERVEHICLE:
		::set_parameter_values(mSteerVehiclesParameterTable, paramName,
				paramValues);
		break;
	case NAVMESH:
		::set_parameter_values(mNavMeshesParameterTable, paramName, paramValues);
		break;
	case CROWDAGENT:
		::set_parameter_values(mCrowdAgentsParameterTable, paramName,
				paramValues);
		break;
	default:
		break;
	}
}

/**
 * Gets the multiple values of a (actually set) parameter.
 */
ValueList_string GameAIManager::get_parameter_values(AIType type,
		const string& paramName) const
{
	switch (type)
	{
	case STEERPLUGIN:
		return ::get_parameter_values(mSteerPlugInsParameterTable, paramName);
	case STEERVEHICLE:
		return ::get_parameter_values(mSteerVehiclesParameterTable, paramName);
	case NAVMESH:
		return ::get_parameter_values(mNavMeshesParameterTable, paramName);
	case CROWDAGENT:
		return ::get_parameter_values(mCrowdAgentsParameterTable, paramName);
	default:
		break;
	}
	return ValueList_string();
}

/**
 * Sets a multi/single-valued parameter to a single value overwriting the existing one(s).
 */
void GameAIManager::set_parameter_value(AIType type, const string& paramName,
		const string& value)
{
	ValueList_string valueList;
	valueList.add_value(value);
	set_parameter_values(type, paramName, valueList);
}

/**
 * Gets a single value (i.e. the first one) of a parameter.
 */
string GameAIManager::get_parameter_value(AIType type,
		const string& paramName) const
{
	ValueList_string valueList = get_parameter_values(type, paramName);
	return (valueList.size() != 0 ? valueList[0] : string(""));
}

/**
 * Gets a list of the names of the parameters actually set.
 */
ValueList_string GameAIManager::get_parameter_name_list(AIType type) const
{
	switch (type)
	{
	case STEERPLUGIN:
		return ::get_parameter_name_list(mSteerPlugInsParameterTable);
	case STEERVEHICLE:
		return ::get_parameter_name_list(mSteerVehiclesParameterTable);
	case NAVMESH:
		return ::get_parameter_name_list(mNavMeshesParameterTable);
	case CROWDAGENT:
		return ::get_parameter_name_list(mCrowdAgentsParameterTable);
	default:
		break;
	}
	return ValueList_string();
}

/**
 * Sets all parameters to their default values (if any).
 * \note: After reading objects from bam files, the objects' creation parameters
 * which reside in the manager, are reset to their default values.
 */
void GameAIManager::set_parameters_defaults(AIType type)
{
	switch (type)
	{
	case STEERPLUGIN:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"plugin_type", "one_turning"},
			{"pathway", "0.0,0.0,0.0:1E-4,1E-4,1E-4$0.0$false"},
		};
		::set_parameters_defaults(mSteerPlugInsParameterTable, nameValueList);
	}
		break;
	case STEERVEHICLE:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"vehicle_type", "one_turning"},
			{"external_update", "false"},
			{"mov_type", "opensteer"},
			{"up_axis_fixed", "false"},
			{"up_axis_fixed_mode", "light"},
			{"mass", "1.0"},
			{"speed", "0.0"},
			{"max_force", "0.1"},
			{"max_speed", "1.0"},
			{"radius", "1.0"},
			{"path_pred_time", "3.0"},
			{"obstacle_min_time_coll", "4.5"},
			{"neighbor_min_time_coll", "3.0"},
			{"neighbor_min_sep_dist", "1.0"},
			{"separation_max_dist", "5.0"},
			{"separation_cos_max_angle", "-0.707"},
			{"alignment_max_dist", "7.5"},
			{"alignment_cos_max_angle", "0.7"},
			{"cohesion_max_dist", "9.0"},
			{"cohesion_cos_max_angle", "-0.15"},
			{"pursuit_max_pred_time", "20.0"},
			{"evasion_max_pred_time", "20.0"},
			{"target_speed", "1.0"},
		};
		::set_parameters_defaults(mSteerVehiclesParameterTable, nameValueList);
	}
		break;
	case NAVMESH:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"navmesh_type", "solo"},
			{"cell_size", "0.3"},
			{"cell_height", "0.2"},
			{"agent_height", "2.0"},
			{"agent_radius", "0.6"},
			{"agent_max_climb", "0.9"},
			{"agent_max_slope", "45.0"},
			{"region_min_size", "8"},
			{"region_merge_size", "20"},
			{"partition_type", "watershed"},
			{"edge_max_len", "12.0"},
			{"edge_max_error", "1.3"},
			{"verts_per_poly", "6.0"},
			{"detail_sample_dist", "6.0"},
			{"detail_sample_max_error", "1.0"},
			{"build_all_tiles", "false"},
			{"max_tiles", "128"},
			{"max_polys_per_tile", "32768"},
			{"tile_size", "32"},
			{"area_flags_cost", "0@0x01@1.0"},
			{"area_flags_cost", "1@0x02@10.0"},
			{"area_flags_cost", "2@0x01@1.0"},
			{"area_flags_cost", "3@0x01:0x04@1.0"},
			{"area_flags_cost", "4@0x01@2.0"},
			{"area_flags_cost", "5@0x08@1.5"},
			{"crowd_include_flags", "0xffef"},
			{"crowd_exclude_flags", "0x10"},
		};
		::set_parameters_defaults(mNavMeshesParameterTable, nameValueList);
	}
		break;
	case CROWDAGENT:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"add_to_navmesh", ""},
			{"mov_type", "recast"},
			{"move_target", "0.0,0.0,0.0"},
			{"move_velocity", "0.0,0.0,0.0"},
			{"max_acceleration", "8.0"},
			{"max_speed", "3.5"},
			{"collision_query_range", "12.0"},
			{"path_optimization_range", "30.0"},
			{"separation_weight", "2.0"},
			{"update_flags", "0x1b"},
			{"obstacle_avoidance_type", "3"},
			{"ray_mask", "all_on"},
		};
		::set_parameters_defaults(mCrowdAgentsParameterTable, nameValueList);
	}
		break;
	default:
		break;
	}
}

/**
 * Updates ai objects.
 *
 * Will be called automatically in a task.
 */
AsyncTask::DoneStatus GameAIManager::update(GenericAsyncTask* task)
{
	float dt = ClockObject::get_global_clock()->get_dt();

#ifdef TESTING
	dt = 0.016666667; //60 fps
#endif

	// call all OSSteerPlugIns' update functions, passing delta time
	for (auto plugIn: mSteerPlugIns)
	{
		plugIn->update(dt);
	}
	// call all RNNavMeshes' update functions, passing delta time
	for (auto navMesh: mNavMeshes)
	{
		navMesh->update(dt);
	}
	//
	return AsyncTask::DS_cont;
}

/**
 * Adds a task to repeatedly call ai updates.
 */
void GameAIManager::start_default_update()
{
	//create the task for updating AI objects
	mUpdateData = new TaskInterface<GameAIManager>::TaskData(this,
			&GameAIManager::update);
	mUpdateTask = new GenericAsyncTask(string("GameAIManager::update"),
			&TaskInterface<GameAIManager>::taskFunction,
			reinterpret_cast<void*>(mUpdateData.p()));
	mUpdateTask->set_sort(mTaskSort);
	//Adds mUpdateTask to the active queue.
	AsyncTaskManager::get_global_ptr()->add(mUpdateTask);
}

/**
 * Removes a task to repeatedly call ai updates.
 */
void GameAIManager::stop_default_update()
{
	if (mUpdateTask)
	{
		AsyncTaskManager::get_global_ptr()->remove(mUpdateTask);
	}
	//
	mUpdateData.clear();
	mUpdateTask.clear();
}

/**
 * Returns the obstacle's settings with the specified unique reference (>0).
 * Returns OSObstacleSettings::ref == a negative number on error.
 */
OSObstacleSettings GameAIManager::get_obstacle_settings(int ref) const
{
	OSObstacleSettings settings = OSObstacleSettings();
	settings.set_ref(RESULT_ERROR);
	CONTINUE_IF_ELSE_R(ref > 0, settings)

	// find settings by ref
	pvector<ObstacleAttributes>& obstacleAttrs =
			const_cast<GlobalObstacles&>(mObstacles).second();
	for (auto attr: obstacleAttrs)
	{
		if (attr.first().get_ref() == ref)
		{
			settings = attr.first();
			break;
		}
	}
	//
	return settings;
}

/**
 * Returns the NodePath of the obstacle with the specified unique reference (>0).
 * Return an empty NodePath with the ET_fail error type set on error.
 */
NodePath GameAIManager::get_obstacle_by_ref(int ref) const
{
	NodePath obstacleNP = NodePath::fail();
	CONTINUE_IF_ELSE_R(ref > 0, obstacleNP)

	// find settings by ref
	pvector<ObstacleAttributes>& obstacleAttrs =
			const_cast<GlobalObstacles&>(mObstacles).second();
	for (auto attr: obstacleAttrs)
	{
		if (attr.first().get_ref() == ref)
		{
			obstacleNP = attr.second();
			break;
		}
	}
	return obstacleNP;
}

/**
 * Draws the specified primitive, given the points, the color (RGBA) and point's size.
 */
void GameAIManager::debug_draw_primitive(OSDebugDrawPrimitives primitive,
		const ValueList_LPoint3f& points, const LVecBase4f color, float size)
{
#ifdef ELY_DEBUG
	mOSDD->begin((ossup::DrawMeshDrawer::DrawPrimitive)primitive, size);
	// calculate the real point list size
	unsigned int realSize;
	switch (primitive)
	{
	case OS_POINTS:
		realSize = points.size();
		break;
	case OS_LINES:
		realSize = points.size() - (points.size() % 2);
		break;
	case OS_TRIS:
		realSize = points.size() - (points.size() % 3);
		break;
	case OS_QUADS:
		realSize = points.size() - (points.size() % 4);
		break;
	default:
		break;
	}
	for (unsigned int i = 0; i < realSize; ++i)
	{
		mOSDD->vertex(points[i], color);
	}
	mOSDD->end();
#endif //ELY_DEBUG
}

/**
 * Draws the specified primitive, given the points, the color (RGBA) and point's size.
 */
void GameAIManager::debug_draw_primitive(RNDebugDrawPrimitives primitive,
		const ValueList_LPoint3f& points, const LVecBase4f color, float size)
{
#ifdef ELY_DEBUG
	mRNDD->begin((duDebugDrawPrimitives) primitive, size);
	// calculate the real point list size
	unsigned int realSize;
	switch (primitive)
	{
	case RN_POINTS:
		realSize = points.size();
		break;
	case RN_LINES:
		realSize = points.size() - (points.size() % 2);
		break;
	case RN_TRIS:
		realSize = points.size() - (points.size() % 3);
		break;
	case RN_QUADS:
		realSize = points.size() - (points.size() % 4);
		break;
	default:
		break;
	}
	for (unsigned int i = 0; i < realSize; ++i)
	{
		mRNDD->vertex(points[i], color);
	}
	mRNDD->end();
#endif //ELY_DEBUG
}

/**
 * Erases all primitives drawn until now.
 */
void GameAIManager::debug_draw_reset(OSDebugDrawPrimitives)
{
#ifdef ELY_DEBUG
	mOSDD->reset();
#endif //ELY_DEBUG
}

/**
 * Erases all primitives drawn until now.
 */
void GameAIManager::debug_draw_reset(RNDebugDrawPrimitives)
{
#ifdef ELY_DEBUG
	mRNDD->reset();
#endif //ELY_DEBUG
}

/**
 * Writes to a bam file the entire collections of ai objects and related
 * geometries (i.e. models' NodePaths)
 */
bool GameAIManager::write_to_bam_file(const string& fileName)
{
	string errorReport;
	// write to bam file
	BamFile outBamFile;
	if (outBamFile.open_write(Filename(fileName)))
	{
		cout << "Current system Bam version: "
				<< outBamFile.get_current_major_ver() << "."
				<< outBamFile.get_current_minor_ver() << endl;
		// just write the reference node
		if (!outBamFile.write_object(mReferenceNP.node()))
		{
			errorReport += string("Error writing ") + mReferenceNP.get_name()
					+ string(" node in ") + fileName + string("\n");
		}
		// close the file
		outBamFile.close();
	}
	else
	{
		errorReport += string("\nERROR: cannot open ") + fileName;
	}
	//check
	if (errorReport.empty())
	{
		cout
				<< "SUCCESS: all ai object collections were written to "
				<< fileName << endl;
	}
	else
	{
		cerr << errorReport << endl;
	}
	return errorReport.empty();
}

/**
 * Reads from a bam file the entire hierarchy of ai objects and related
 * geometries (i.e. models' NodePaths)
 */
bool GameAIManager::read_from_bam_file(const string& fileName)
{
	string errorReport;
	//read from bamFile
	BamFile inBamFile;
	if (inBamFile.open_read(Filename(fileName)))
	{
		cout << "Current system Bam version: "
				<< inBamFile.get_current_major_ver() << "."
				<< inBamFile.get_current_minor_ver() << endl;
		cout << "Bam file version: " << inBamFile.get_file_major_ver() << "."
				<< inBamFile.get_file_minor_ver() << endl;
		// just read the reference node
		TypedWritable* reference = inBamFile.read_object();
		if (reference)
		{
			//resolve pointers
			if (!inBamFile.resolve())
			{
				errorReport += string("Error resolving pointers in ") + fileName
						+ string("\n");
			}
		}
		else
		{
			errorReport += string("Error reading ") + fileName + string("\n");
		}
		// close the file
		inBamFile.close();
		// post process after restoring
		// restore reference node
		mReferenceNP = NodePath::any_path(DCAST(PandaNode, reference));
		// post processing from bam for all managed objects
		for (auto vehicle: mSteerVehicles)
		{
			vehicle->post_process_from_bam();
		}
		for (auto plugIn: mSteerPlugIns)
		{
			plugIn->post_process_from_bam();
		}
		for (auto agent: mCrowdAgents)
		{
			agent->post_process_from_bam();
		}
		for (auto navMesh: mNavMeshes)
		{
			navMesh->post_process_from_bam();
		}
	}
	else
	{
		errorReport += string("\nERROR: cannot open ") + fileName;
	}
	//check
	if (errorReport.empty())
	{
		cout << "SUCCESS: all ai objects were read from "
				<< fileName << endl;
	}
	else
	{
		cerr << errorReport << endl;
	}
	return errorReport.empty();
}

TYPED_OBJECT_API_DEF(GameAIManager)
