/**
 * \file recastnavigation_composite2.cxx
 *
 * \date 2016-09-16
 * \author consultit
 */

///library
#include "recastnavigation/Detour/Source/DetourNavMesh.cpp"
#include "recastnavigation/Recast/Source/RecastMesh.cpp"

///support
#include "support_rn/InputGeom.cpp"
