/**
 * \file aisymbols.h
 *
 * \date 2018-10-03
 * \author consultit
 */
#ifndef AI_SOURCE_AISYMBOLS_H_
#define AI_SOURCE_AISYMBOLS_H_

#ifdef PYTHON_BUILD_ai
#	define EXPCL_AI EXPORT_CLASS
#	define EXPTP_AI EXPORT_TEMPL
#else
#	define EXPCL_AI IMPORT_CLASS
#	define EXPTP_AI IMPORT_TEMPL
#endif

#endif /* AI_SOURCE_AISYMBOLS_H_ */
