/**
 * \file recastnavigation_includes.h
 *
 * \date 2016-09-16
 * \author consultit
 */

#ifndef AI_SOURCE_RECASTNAVIGATION_INCLUDES_H_
#define AI_SOURCE_RECASTNAVIGATION_INCLUDES_H_

#include "aisymbols.h"

#include "../../common/source/commonTools.h"
#include "../../common/source/commonMacros.h"

#include "pairs_valueLists.h"
#include "utilities.h"

#ifdef CPPPARSER
//Panda3d interrogate fake declarations

namespace rnsup
{
	struct InputGeom;
	struct NavMeshType;
	struct NavMeshSettings;
	struct NavMeshTileSettings;
	struct BuildContext;
	struct NavMeshPolyAreaFlags;
	struct NavMeshPolyAreaCost;
	struct DebugDrawPanda3d;
	struct DebugDrawMeshDrawer;
	struct NavMeshTesterTool;
	struct rcMeshLoaderObj;
}

struct dtNavMesh;
struct dtNavMeshQuery;
struct dtCrowd;
struct dtTileCache;
struct dtCrowdAgentParams;
typedef unsigned int dtObstacleRef;

#endif //CPPPARSER

#endif /* AI_SOURCE_RECASTNAVIGATION_INCLUDES_H_ */
