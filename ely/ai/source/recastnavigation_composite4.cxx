/**
 * \file recastnavigation_composite4.cxx
 *
 * \date 2016-09-16
 * \author consultit
 */

///library
#include "recastnavigation/Recast/Source/RecastContour.cpp"

///support
#include "support_rn/ChunkyTriMesh.cpp"
#include "support_rn/ConvexVolumeTool.cpp"
#include "support_rn/DebugInterfaces.cpp"
#include "support_rn/MeshLoaderObj.cpp"
#include "support_rn/NavMeshTesterTool.cpp"
#include "support_rn/NavMeshType.cpp"
#include "support_rn/NavMeshType_Obstacle.cpp"
#include "support_rn/NavMeshType_Solo.cpp"
#include "support_rn/NavMeshType_Tile.cpp"
#include "support_rn/OffMeshConnectionTool.cpp"
#include "support_rn/PerfTimer.cpp"
#include "support_rn/fastlz.c"
