/**
 * \file opensteer_composite1.cxx
 *
 * \date 2016-09-16
 * \author consultit
 */

///library
#include "opensteer/src/Camera.cpp"
#include "opensteer/src/Clock.cpp"
#include "opensteer/src/Color.cpp"
#include "opensteer/src/lq.c"
#include "opensteer/src/Obstacle.cpp"
#include "opensteer/src/OldPathway.cpp"
#include "opensteer/src/Path.cpp"
#include "opensteer/src/Pathway.cpp"
#include "opensteer/src/PlugIn.cpp"
#include "opensteer/src/PolylineSegmentedPath.cpp"
#include "opensteer/src/PolylineSegmentedPathwaySegmentRadii.cpp"
#include "opensteer/src/PolylineSegmentedPathwaySingleRadius.cpp"
#include "opensteer/src/SegmentedPath.cpp"
#include "opensteer/src/SegmentedPathway.cpp"
#include "opensteer/src/TerrainRayTest.cpp"
#include "opensteer/src/Vec3.cpp"
#include "opensteer/src/Vec3Utilities.cpp"
//#include "opensteer/src/main.cpp"
//#include "opensteer/src/OpenSteerDemo.cpp"
//#include "opensteer/src/Draw.cpp"
//#include "opensteer/src/SimpleVehicle.cpp"
///support
#include "support_os/Draw.cpp"
#include "support_os/DrawMeshDrawer.cpp"
#include "support_os/SimpleVehicle.cpp"
