/**
 * \file aiTools.cxx
 *
 * \date 2016-09-17
 * \author consultit
 */

#include "aiTools.h"

///OSVehicleSettings definitions
/**
 * Writes the Vehicle settings into a datagram.
 */
void OSVehicleSettings::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_mass());
	dg.add_stdfloat(get_radius());
	dg.add_stdfloat(get_speed());
	dg.add_stdfloat(get_max_force());
	dg.add_stdfloat(get_max_speed());
	get_forward().write_datagram(dg);
	get_side().write_datagram(dg);
	get_up().write_datagram(dg);
	get_position().write_datagram(dg);
	get_start().write_datagram(dg);
	dg.add_stdfloat(get_path_pred_time());
	dg.add_stdfloat(get_obstacle_min_time_coll());
	dg.add_stdfloat(get_neighbor_min_time_coll());
	dg.add_stdfloat(get_neighbor_min_sep_dist());
	dg.add_stdfloat(get_separation_max_dist());
	dg.add_stdfloat(get_separation_cos_max_angle());
	dg.add_stdfloat(get_alignment_max_dist());
	dg.add_stdfloat(get_alignment_cos_max_angle());
	dg.add_stdfloat(get_cohesion_max_dist());
	dg.add_stdfloat(get_cohesion_cos_max_angle());
	dg.add_stdfloat(get_pursuit_max_pred_time());
	dg.add_stdfloat(get_evasion_max_pred_time());
	dg.add_stdfloat(get_target_speed());
}

/**
 * Restores the Vehicle settings from the datagram.
 */
void OSVehicleSettings::read_datagram(DatagramIterator &scan)
{
	set_mass(scan.get_stdfloat());
	set_radius(scan.get_stdfloat());
	set_speed(scan.get_stdfloat());
	set_max_force(scan.get_stdfloat());
	set_max_speed(scan.get_stdfloat());
	LVecBase3f value;
	value.read_datagram(scan);
	set_forward(value);
	value.read_datagram(scan);
	set_side(value);
	value.read_datagram(scan);
	set_up(value);
	value.read_datagram(scan);
	set_position(value);
	value.read_datagram(scan);
	set_start(value);
	set_path_pred_time(scan.get_stdfloat());
	set_obstacle_min_time_coll(scan.get_stdfloat());
	set_neighbor_min_time_coll(scan.get_stdfloat());
	set_neighbor_min_sep_dist(scan.get_stdfloat());
	set_separation_max_dist(scan.get_stdfloat());
	set_separation_cos_max_angle(scan.get_stdfloat());
	set_alignment_max_dist(scan.get_stdfloat());
	set_alignment_cos_max_angle(scan.get_stdfloat());
	set_cohesion_max_dist(scan.get_stdfloat());
	set_cohesion_cos_max_angle(scan.get_stdfloat());
	set_pursuit_max_pred_time(scan.get_stdfloat());
	set_evasion_max_pred_time(scan.get_stdfloat());
	set_target_speed(scan.get_stdfloat());
}

/**
 * Writes a sensible description of the OSVehicleSettings to the indicated
 * output stream.
 */
void OSVehicleSettings::output(ostream &out) const
{
	out << "mass: " << get_mass() << endl;
	out << "radius: " << get_radius() << endl;
	out << "speed: " << get_speed() << endl;
	out << "max_force: " << get_max_force() << endl;
	out << "max_speed: " << get_max_speed() << endl;
	out << "forward: " << get_forward() << endl;
	out << "side: " << get_side() << endl;
	out << "up: " << get_up() << endl;
	out << "position: " << get_position() << endl;
	out << "start: " << get_start() << endl;
	out << "path_pred_time: " << get_path_pred_time() << endl;
	out << "obstacle_min_time_coll: " << get_obstacle_min_time_coll() << endl;
	out << "neighbor_min_time_coll: " << get_neighbor_min_time_coll() << endl;
	out << "neighbor_min_sep_dist: " << get_neighbor_min_sep_dist() << endl;
	out << "separation_max_dist: " << get_separation_max_dist() << endl;
	out << "separation_cos_max_angle: " << get_separation_cos_max_angle()
			<< endl;
	out << "alignment_max_dist: " << get_alignment_max_dist() << endl;
	out << "alignment_cos_max_angle: " << get_alignment_cos_max_angle() << endl;
	out << "cohesion_max_dist: " << get_cohesion_max_dist() << endl;
	out << "cohesion_cos_max_angle: " << get_cohesion_cos_max_angle() << endl;
	out << "pursuit_max_pred_time: " << get_pursuit_max_pred_time() << endl;
	out << "evasion_max_pred_time: " << get_evasion_max_pred_time() << endl;
	out << "speed: " << get_speed() << endl;
}

///OSFlockSettings definitions
/**
 *
 */
OSFlockSettings::OSFlockSettings() :
		_separationWeight(0.0), _alignmentWeight(0.0), _cohesionWeight(0.0)
{
}/**
 *
 */
OSFlockSettings::OSFlockSettings(float sW, float aW, float cW) :
		_separationWeight(sW), _alignmentWeight(aW), _cohesionWeight(cW)
{
}
/**
 * Writes the flock settings into a datagram.
 */
void OSFlockSettings::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_separation_weight());
	dg.add_stdfloat(get_alignment_weight());
	dg.add_stdfloat(get_cohesion_weight());
}

/**
 * Restores the flock settings from the datagram.
 */
void OSFlockSettings::read_datagram(DatagramIterator &scan)
{
	set_separation_weight(scan.get_stdfloat());
	set_alignment_weight(scan.get_stdfloat());
	set_cohesion_weight(scan.get_stdfloat());
}

/**
 * Writes a sensible description of the OSFlockSettings to the indicated output
 * stream.
 */
void OSFlockSettings::output(ostream &out) const
{
	out << "separation_weight: " << get_separation_weight() << endl;
	out << "alignment_weight: " << get_alignment_weight() << endl;
	out << "cohesion_weight: " << get_cohesion_weight() << endl;
}

///OSObstacleSettings definitions
/**
 *
 */
OSObstacleSettings::OSObstacleSettings() :
		_width(0), _height(0), _depth(0), _radius(0), _ref(0), _obstacle(0)
{
}
/**
 * Writes the OSObstacleSettings into a datagram.
 */
void OSObstacleSettings::write_datagram(Datagram &dg) const
{
	dg.add_string(_type);
	dg.add_string(_seenFromState);
	_position.write_datagram(dg);
	_forward.write_datagram(dg);
	_up.write_datagram(dg);
	_side.write_datagram(dg);
	dg.add_stdfloat(get_width());
	dg.add_stdfloat(get_height());
	dg.add_stdfloat(get_depth());
	dg.add_stdfloat(get_radius());
	dg.add_int32(get_ref());
}
/**
 * Restores the OSObstacleSettings from the datagram.
 */
void OSObstacleSettings::read_datagram(DatagramIterator &scan)
{
	set_type(scan.get_string());
	set_seen_from_state(scan.get_string());
	_position.read_datagram(scan);
	_forward.read_datagram(scan);
	_up.read_datagram(scan);
	_side.read_datagram(scan);
	set_width(scan.get_stdfloat());
	set_height(scan.get_stdfloat());
	set_depth(scan.get_stdfloat());
	set_radius(scan.get_stdfloat());
	set_ref(scan.get_int32());
}

/**
 * Writes a sensible description of the OSObstacleSettings to the indicated
 * output stream.
 */
void OSObstacleSettings::output(ostream &out) const
{
	out << "type: " << get_type() << endl;
	out << "seen_from_state: " << get_seen_from_state() << endl;
	out << "position: " << get_position() << endl;
	out << "forward: " << get_forward() << endl;
	out << "up: " << get_up() << endl;
	out << "side: " << get_side() << endl;
	out << "width: " << get_width() << endl;
	out << "height: " << get_height() << endl;
	out << "depth: " << get_depth() << endl;
	out << "radius: " << get_radius() << endl;
	out << "ref: " << get_ref() << endl;
}

///RNNavMeshSettings definitions
/**
 *
 */
RNNavMeshSettings::RNNavMeshSettings(): _navMeshSettings()
{
}
/**
 * Writes the NavMeshSettings into a datagram.
 */
void RNNavMeshSettings::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_cell_size());
	dg.add_stdfloat(get_cell_height());
	dg.add_stdfloat(get_agent_height());
	dg.add_stdfloat(get_agent_radius());
	dg.add_stdfloat(get_agent_max_climb());
	dg.add_stdfloat(get_agent_max_slope());
	dg.add_stdfloat(get_region_min_size());
	dg.add_stdfloat(get_region_merge_size());
	dg.add_stdfloat(get_edge_max_len());
	dg.add_stdfloat(get_edge_max_error());
	dg.add_stdfloat(get_verts_per_poly());
	dg.add_stdfloat(get_detail_sample_dist());
	dg.add_stdfloat(get_detail_sample_max_error());
	dg.add_int32(get_partition_type());
}

/**
 * Restores the NavMeshSettings from the datagram.
 */
void RNNavMeshSettings::read_datagram(DatagramIterator &scan)
{
	set_cell_size(scan.get_stdfloat());
	set_cell_height(scan.get_stdfloat());
	set_agent_height(scan.get_stdfloat());
	set_agent_radius(scan.get_stdfloat());
	set_agent_max_climb(scan.get_stdfloat());
	set_agent_max_slope(scan.get_stdfloat());
	set_region_min_size(scan.get_stdfloat());
	set_region_merge_size(scan.get_stdfloat());
	set_edge_max_len(scan.get_stdfloat());
	set_edge_max_error(scan.get_stdfloat());
	set_verts_per_poly(scan.get_stdfloat());
	set_detail_sample_dist(scan.get_stdfloat());
	set_detail_sample_max_error(scan.get_stdfloat());
	set_partition_type(scan.get_int32());
}

/**
 * Writes a sensible description of the RNNavMeshSettings to the indicated
 * output stream.
 */
void RNNavMeshSettings::output(ostream &out) const
{
	out << "cell_size: " << get_cell_size() << endl;
	out << "cell_height: " << get_cell_height() << endl;
	out << "agent_height: " << get_agent_height() << endl;
	out << "agent_radius: " << get_agent_radius() << endl;
	out << "agent_max_climb: " << get_agent_max_climb() << endl;
	out << "agent_max_slope: " << get_agent_max_slope() << endl;
	out << "region_min_size: " << get_region_min_size() << endl;
	out << "region_merge_size: " << get_region_merge_size() << endl;
	out << "edge_max_len: " << get_edge_max_len() << endl;
	out << "edge_max_error: " << get_edge_max_error() << endl;
	out << "verts_per_poly: " << get_verts_per_poly() << endl;
	out << "detail_sample_dist: " << get_detail_sample_dist() << endl;
	out << "detail_sample_max_error: " << get_detail_sample_max_error() << endl;
	out << "partition_ype: " << get_partition_type() << endl;
}

///RNNavMeshTileSettings definitions
/**
 *
 */
RNNavMeshTileSettings::RNNavMeshTileSettings() :
		_navMeshTileSettings()
{
}
/**
 * Writes the NavMeshTileSettings into a datagram.
 */
void RNNavMeshTileSettings::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_build_all_tiles());
	dg.add_int32(get_max_tiles());
	dg.add_int32(get_max_polys_per_tile());
	dg.add_stdfloat(get_tile_size());
}
/**
 * Restores the NavMeshTileSettings from the datagram.
 */
void RNNavMeshTileSettings::read_datagram(DatagramIterator &scan)
{
	set_build_all_tiles(scan.get_stdfloat());
	set_max_tiles(scan.get_int32());
	set_max_polys_per_tile(scan.get_int32());
	set_tile_size(scan.get_stdfloat());
}

/**
 * Writes a sensible description of the RNNavMeshTileSettings to the indicated
 * output stream.
 */
void RNNavMeshTileSettings::output(ostream &out) const
{
	out << "build_all_tiles: " << get_build_all_tiles() << endl;
	out << "max_tiles: " << get_max_tiles() << endl;
	out << "max_polys_per_tile: " << get_max_polys_per_tile() << endl;
	out << "tile_size: " << get_tile_size() << endl;
}

///RNConvexVolumeSettings definitions
/**
 *
 */
RNConvexVolumeSettings::RNConvexVolumeSettings() :
		_area(0), _flags(0), _ref(0)
{
}
/**
 * Writes the RNConvexVolumeSettings into a datagram.
 */
void RNConvexVolumeSettings::write_datagram(Datagram &dg) const
{
	dg.add_int32(get_area());
	dg.add_int32(get_flags());
	_centroid.write_datagram(dg);
	dg.add_int32(get_ref());
}
/**
 * Restores the RNConvexVolumeSettings from the datagram.
 */
void RNConvexVolumeSettings::read_datagram(DatagramIterator &scan)
{
	set_area(scan.get_int32());
	set_flags(scan.get_int32());
	_centroid.read_datagram(scan);
	set_ref(scan.get_int32());
}

/**
 * Writes a sensible description of the RNConvexVolumeSettings to the indicated
 * output stream.
 */
void RNConvexVolumeSettings::output(ostream &out) const
{
	out << "area: " << get_area() << endl;
	out << "flags: " << get_flags() << endl;
	out << "centroid: " << get_centroid() << endl;
	out << "ref: " << get_ref() << endl;
}

///RNOffMeshConnectionSettings definitions
/**
 *
 */
RNOffMeshConnectionSettings::RNOffMeshConnectionSettings() :
		_area(0), _bidir(false), _userId(0), _flags(0), _rad(0.0), _ref(0)
{
}
/**
 * Writes the RNOffMeshConnectionSettings into a datagram.
 */
void RNOffMeshConnectionSettings::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_rad());
	dg.add_bool(get_bidir());
	dg.add_uint32(get_user_id());
	dg.add_int32(get_area());
	dg.add_int32(get_flags());
	dg.add_int32(get_ref());
}
/**
 * Restores the RNOffMeshConnectionSettings from the datagram.
 */
void RNOffMeshConnectionSettings::read_datagram(DatagramIterator &scan)
{
	set_rad(scan.get_stdfloat());
	set_bidir(scan.get_bool());
	set_user_id(scan.get_uint32());
	set_area(scan.get_int32());
	set_flags(scan.get_int32());
	set_ref(scan.get_int32());
}

/**
 * Writes a sensible description of the RNOffMeshConnectionSettings to the
 * indicated output stream.
 */
void RNOffMeshConnectionSettings::output(ostream &out) const
{
	out << "rad: " << get_rad() << endl;
	out << "bidir: " << get_bidir() << endl;
	out << "userId: " << get_user_id() << endl;
	out << "area: " << get_area() << endl;
	out << "flags: " << get_flags() << endl;
	out << "ref: " << get_ref() << endl;
}

///RNObstacleSettings definitions
/**
 *
 */
RNObstacleSettings::RNObstacleSettings() :
		_radius(0.0), _dims(LVecBase3f()), _ref(0)
{
}

/**
 * Writes the RNObstacleSettings into a datagram.
 */
void RNObstacleSettings::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_radius());
	_dims.write_datagram(dg);
	dg.add_uint32(get_ref());
}
/**
 * Restores the RNObstacleSettings from the datagram.
 */
void RNObstacleSettings::read_datagram(DatagramIterator &scan)
{
	set_radius(scan.get_stdfloat());
	_dims.read_datagram(scan);
	set_ref(scan.get_uint32());
}

/**
 * Writes a sensible description of the RNObstacleSettings to the indicated
 * output stream.
 */
void RNObstacleSettings::output(ostream &out) const
{
	out << "radius: " << get_radius() << endl;
	out << "dims: " << get_dims() << endl;
	out << "ref: " << get_ref() << endl;
}

///RNCrowdAgentParams definitions
/**
 *
 */
RNCrowdAgentParams::RNCrowdAgentParams(): _dtCrowdAgentParams()
{
}

/**
 * Writes the CrowdAgentParams into a datagram.
 */
void RNCrowdAgentParams::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_radius());
	dg.add_stdfloat(get_height());
	dg.add_stdfloat(get_max_acceleration());
	dg.add_stdfloat(get_max_speed());
	dg.add_stdfloat(get_collision_query_range());
	dg.add_stdfloat(get_path_optimization_range());
	dg.add_stdfloat(get_separation_weight());
	dg.add_uint8(get_update_flags());
	dg.add_uint8(get_obstacle_avoidance_type());
	dg.add_uint8(get_query_filter_type());
	//Note: void *dtCrowdAgentParams::userData is not used
}
/**
 * Restores the CrowdAgentParams from the datagram.
 */
void RNCrowdAgentParams::read_datagram(DatagramIterator &scan)
{
	set_radius(scan.get_stdfloat());
	set_height(scan.get_stdfloat());
	set_max_acceleration(scan.get_stdfloat());
	set_max_speed(scan.get_stdfloat());
	set_collision_query_range(scan.get_stdfloat());
	set_path_optimization_range(scan.get_stdfloat());
	set_separation_weight(scan.get_stdfloat());
	set_update_flags(scan.get_uint8());
	set_obstacle_avoidance_type(scan.get_uint8());
	set_query_filter_type(scan.get_uint8());
	//Note: void *dtCrowdAgentParams::userData is not used
}

/**
 * Writes a sensible description of the RNCrowdAgentParams to the indicated
 * output stream.
 */
void RNCrowdAgentParams::output(ostream &out) const
{
	out << "radius: " << get_radius() << endl;
	out << "height: " << get_height() << endl;
	out << "max_acceleration: " << get_max_acceleration() << endl;
	out << "max_speed: " << get_max_speed() << endl;
	out << "collision_query_range: " << get_collision_query_range() << endl;
	out << "path_optimization_range: " << get_path_optimization_range() << endl;
	out << "separation_weight: " << get_separation_weight() << endl;
	out << "update_flags: " << static_cast<unsigned int>(get_update_flags())
			<< endl;
	out << "obstacle_avoidance_type: "
			<< static_cast<unsigned int>(get_obstacle_avoidance_type()) << endl;
	out << "query_filter_type: " << get_query_filter_type() << endl;
	out << "user_data: " << get_user_data() << endl;
}
