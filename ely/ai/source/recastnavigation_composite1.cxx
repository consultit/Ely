/**
 * \file recastnavigation_composite1.cxx
 *
 * \date 2016-09-16
 * \author consultit
 */

///library
#include "recastnavigation/DebugUtils/Source/DebugDraw.cpp"
#include "recastnavigation/Detour/Source/DetourAlloc.cpp"
#include "recastnavigation/Detour/Source/DetourAssert.cpp"
#include "recastnavigation/Detour/Source/DetourCommon.cpp"
#include "recastnavigation/DetourCrowd/Source/DetourCrowd.cpp"
#include "recastnavigation/DebugUtils/Source/DetourDebugDraw.cpp"
#include "recastnavigation/DetourCrowd/Source/DetourLocalBoundary.cpp"
#include "recastnavigation/Detour/Source/DetourNavMeshBuilder.cpp"
#include "recastnavigation/Detour/Source/DetourNavMeshQuery.cpp"
#include "recastnavigation/Detour/Source/DetourNode.cpp"
#include "recastnavigation/DetourCrowd/Source/DetourObstacleAvoidance.cpp"
#include "recastnavigation/DetourCrowd/Source/DetourPathCorridor.cpp"
#include "recastnavigation/DetourCrowd/Source/DetourPathQueue.cpp"
#include "recastnavigation/DetourCrowd/Source/DetourProximityGrid.cpp"
#include "recastnavigation/DetourTileCache/Source/DetourTileCache.cpp"
#include "recastnavigation/Recast/Source/RecastAlloc.cpp"
#include "recastnavigation/Recast/Source/RecastAssert.cpp"
#include "recastnavigation/Recast/Source/RecastArea.cpp"
#include "recastnavigation/Recast/Source/Recast.cpp"
#include "recastnavigation/DebugUtils/Source/RecastDebugDraw.cpp"
#include "recastnavigation/DebugUtils/Source/RecastDump.cpp"
#include "recastnavigation/Recast/Source/RecastFilter.cpp"
#include "recastnavigation/Recast/Source/RecastLayers.cpp"
#include "recastnavigation/Recast/Source/RecastMeshDetail.cpp"
#include "recastnavigation/Recast/Source/RecastRasterization.cpp"
#include "recastnavigation/Recast/Source/RecastRegion.cpp"
