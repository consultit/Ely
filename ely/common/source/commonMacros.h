/**
 * \file commonMacros.h
 *
 * \date 2018-09-17
 * \author consultit
 */

#ifndef COMMONMACROS_H_
#define COMMONMACROS_H_

///TYPED OBJECT API
#define TYPED_OBJECT_API_DECL(name, parent1) \
public:\
	static TypeHandle get_class_type()\
	{\
		return _type_handle;\
	}\
	static void init_type()\
	{\
		parent1::init_type();\
		register_type(_type_handle, #name, parent1::get_class_type());\
	}\
	virtual TypeHandle get_type() const override\
	{\
		return get_class_type();\
	}\
	virtual TypeHandle force_init_type() override\
	{\
		init_type();\
		return get_class_type();\
	}\
private:\
	static TypeHandle _type_handle;\

#define TYPED_OBJECT_API_DEF(name) \
TypeHandle name::_type_handle;\

#ifdef PYTHON_BUILD
#include "py_panda.h"
#include "commonTools.h"

///PYTHON GET EXCEPTION INFORMATIONS
inline string PY_GET_EXCEPTION_INFO()
{
	PyObject *type, *value, *traceback;
	PyErr_Fetch(&type, &value, &traceback);
	string typeStr, valueStr;
	if (type)
	{
		typeStr = get_py_object_name(type);
	}
	if (value)
	{
		valueStr = get_py_object_name(value);
	}
	Py_XDECREF(type);
	Py_XDECREF(value);
	Py_XDECREF(traceback);
	return string(" - (") + typeStr + string(": ") + valueStr + string(").");
}

///PYTHON SET FUNCTION WITH GENERIC ARG BODY
/**
 * Sets function as func if this is callable or None, otherwise raises an error.
 */
inline void PY_SET_FUNC_BODY(PyObject*& function, PyObject* func,
		const string& errMsgPref)
{
	if (PyCallable_Check(func))
	{
		if (function)
		{
			/* remove old function*/
			Py_DECREF(function);
		}
		/* add new function*/
		function = func;
		Py_INCREF(function);
	}
	else if (func == Py_None)
	{
		if (function)
		{
			/* remove old function*/
			Py_DECREF(function);
			/* reset function*/
			function = nullptr;
		}
	}
	else
	{
		/* raise an error */
		string errStr = errMsgPref + string(": '")
				+ get_py_object_name(func)
				+ string("' must be callable or None - ")
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
	}
}

///PYTHON SET CALLBACK WITH A SELF ARG (FOR TYPED OBJECT) API
inline void PY_SELF_ARG_DECREF(PyObject* callback, PyObject* mSelf)
{
	if (callback)
	{
		Py_DECREF(callback);
		Py_DECREF(mSelf);
	}
}

/**
 * Sets callback as clbk if this is callable or None, otherwise raises an error.
 * This kind of callback refers to mSelf.
 */
inline void PY_SET_CALLBACK_SELF_ARG_BODY(PyObject*& callback, PyObject* clbk,
		Dtool_PyTypedObject& dtoolType, const string& errMsgPref,
		PyObject*& mSelf, TypedObject* THIS)
{
	if (PyCallable_Check(clbk))
	{
		if (callback)
		{
			/* remove old callback*/
			Py_DECREF(callback);
			/* mSelf is already created and referred */
		}
		else /* !callback */
		{
			if (mSelf)
			{
				/* mSelf is set: it must be incref'ed */
				Py_INCREF(mSelf);
			}
			else /* !mSelf */
			{
				/* mSelf is not set: it must be created (refcount == 1) */
				mSelf = DTool_CreatePyInstanceTyped(THIS, dtoolType, false,
						false, THIS->get_type_index());
			}
		}
		/* add new callback*/
		callback = clbk;
		Py_INCREF(callback);
	}
	else if (clbk == Py_None)
	{
		if (callback)
		{
			/* remove old callback*/
			Py_DECREF(callback);
			/* reset callback*/
			callback = nullptr;

			/* mSelf must be set */
			ASSERT_TRUE(mSelf)
			Py_ssize_t selfRefcnt = mSelf->ob_refcnt;
			Py_DECREF(mSelf);
			if (selfRefcnt == 1)
			{
				/* this callback is the last referent */
				mSelf = nullptr;
			}
		}
	}
	else
	{
		/* raise an error */
		string errStr = errMsgPref + string(": '") + get_py_object_name(clbk)
				+ string("' must be callable or None - ")
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
	}
}

///PYTHON CALLBACK WITH SELF ARG CALL (WITH RETURN VALUE IGNORED) API
inline void PY_CALLBACK_SELF_ARG_CALL(PyObject*& callback,
		PyObject*& mSelf, const string& errMsgPref)
{
	PyObject *result;
	result = PyObject_CallFunctionObjArgs(callback, mSelf, nullptr);
	if (!result)
	{
		/* handle error */
		string errStr = errMsgPref + string(": Error calling '")
				+ get_py_object_name(callback) + string("' callback function - ")
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
	}
	else
	{
		Py_DECREF(result);
	}
}

///PYTHON CALLBACK WITH SELF AND ONE BitMask32 OTHER ARG CALL (WITH RETURN VALUE IGNORED) (FOR NON TYPED OBJECT) API
inline void PY_CALLBACK_SELF_ONE_OTHER_ARGS_CALL(PyObject*& callback,
		BitMask32& mask, Dtool_PyTypedObject& dtoolType, PyObject*& mSelf,
		const string& errMsgPref)
{
	PyObject *maskObj, *result;
	/* create the BitMask32 object */
	maskObj = DTool_CreatePyInstance(&mask, dtoolType, false, false);
	/* call the callback */
	result = PyObject_CallFunctionObjArgs(callback, mSelf, maskObj, nullptr);
	Py_DECREF(maskObj);
	if (!result)
	{
		/* handle error */
		string errStr = errMsgPref + string(": Error calling '")
				+ get_py_object_name(callback) + string("' callback function")
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
	}
	else
	{
		Py_DECREF(result);
	}
}

///PYTHON FUNCTION WITH NO ARGS CALL (WITH RETURN VALUE IGNORED) (FOR NON TYPED OBJECT) API
inline void PY_ZERO_ARGS_CALL(PyObject *&func, const string &errMsgPref,
		const string &errMsgPost)
{
	// call python func
	PyObject *result;
	/* call the callback */
	result = PyObject_CallFunctionObjArgs(func, nullptr);
	if (!result)
	{
		/* handle error */
		string errStr = errMsgPref + get_py_object_name(func) + errMsgPost
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
	}
	else
	{
		Py_DECREF(result);
	}
}

///PYTHON FUNCTION WITH ONE ARGS CALL (WITH RETURN VALUE IGNORED) (FOR NON TYPED OBJECT) API
inline void PY_ONE_ARGS_CALL(PyObject *&func, void *arg,
		Dtool_PyTypedObject &dtoolType, const string &errMsgPref,
		const string &errMsgPost)
{
	// call python func
	PyObject *argObj, *result;
	/* create the arg object */
	argObj = DTool_CreatePyInstance(arg, dtoolType, false, false);
	/* call the callback */
	result = PyObject_CallFunctionObjArgs(func, argObj, nullptr);
	Py_DECREF(argObj);
	if (!result)
	{
		/* handle error */
		string errStr = errMsgPref + get_py_object_name(func) + errMsgPost
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
	}
	else
	{
		Py_DECREF(result);
	}
}

///PYTHON FUNCTION WITH ONE AND STRING ARGS CALL (WITH RETURN VALUE IGNORED) (FOR NON TYPED OBJECT) API
inline void PY_ONE_STR_ARGS_CALL(PyObject *&func, void *arg1,
		const string &arg2, Dtool_PyTypedObject &dtoolType1,
		const string &errMsgPref, const string &errMsgPost)
{
	// call python func
	PyObject *arg1Obj, *arg2Obj, *result;
	/* create the arg objects */
	arg1Obj = DTool_CreatePyInstance(arg1, dtoolType1, false, false);
	arg2Obj = Py_BuildValue("s", arg2.c_str());
	/* call the callback */
	result = PyObject_CallFunctionObjArgs(func, arg1Obj, arg2Obj, nullptr);
	Py_DECREF(arg1Obj);
	Py_DECREF(arg2Obj);
	if (!result)
	{
		/* handle error */
		string errStr = errMsgPref + get_py_object_name(func) + errMsgPost
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
	}
	else
	{
		Py_DECREF(result);
	}
}

/**
 * Adds a function object into a container.
 */
template<typename Container>
inline void PY_ADD_FUNC_IN_CONTAINER(PyObject *func, Container &container,
		const string &errMsgPref, const string &errMsgPost)
{
	if (PyCallable_Check(func))
	{
		/* add new func*/
		container.push_back(func);
		Py_INCREF(func);
	}
	else
	{
		/* handle error */
		string errStr = errMsgPref + get_py_object_name(func) + errMsgPost
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
	}
}

/**
 * Adds a function object into a table indexed by a key.
 */
template<typename Table, typename Key>
inline void PY_ADD_FUNC_IN_TABLE(PyObject *func, Table &table, const Key &key,
		const string &errMsgPref, const string &errMsgPost)
{
	if (PyCallable_Check(func))
	{
		/* add new func*/
		table[key] = func;
		Py_INCREF(func);
	}
	else
	{
		/* handle error */
		string errStr = errMsgPref + get_py_object_name(func) + errMsgPost
				+ PY_GET_EXCEPTION_INFO();
#ifdef ELY_DEBUG
		PyErr_SetString(PyExc_TypeError, errStr.c_str());
#else
		PRINT_ERR(errStr);
#endif
	}
}

#endif //PYTHON_BUILD

#endif /* COMMONMACROS_H_ */
