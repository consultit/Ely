/**
 * \file commonTools.cxx
 *
 * \date 2016-10-03
 * \author consultit
 */

#include "commonTools.h"
#include "lpoint3.h"

///commonTools definitions
pvector<string> parseCompoundString(
		const string& srcCompoundString, char separator)
{
	if (srcCompoundString.empty())
	{
		return pvector<string>{};
	}
	//erase blanks
	string compoundString = srcCompoundString;
	compoundString = eraseCharacter(compoundString, ' ');
	compoundString = eraseCharacter(compoundString, '\t');
	compoundString = eraseCharacter(compoundString, '\n');
	//parse
	pvector<string> substrings;
	int len = compoundString.size() + 1;
	char* dest = new char[len];
	strncpy(dest, compoundString.c_str(), len);
	//find
	char* pch;
	char* start = dest;
	bool stop = false;
	while (! stop)
	{
		string substring("");
		pch = strchr(start, separator);
		if (pch != NULL)
		{
			//insert the substring
			substring.append(start, pch - start);
			start = pch + 1;
			substrings.push_back(substring);
		}
		else
		{
			if (start < &dest[len - 1])
			{
				//insert the last not empty substring
				substring.append(start, &dest[len - 1] - start);
				substrings.push_back(substring);
			}
			else if (start == &dest[len - 1])
			{
				//insert the last empty substring
				substrings.push_back(substring);
			}
			stop = true;
		}
	}
	delete[] dest;
	//
	return substrings;
}

string eraseCharacter(const string& source, int character)
{
	int len = source.size() + 1;
	char* dest = new char[len];
	char* start = dest;
	strncpy(dest, source.c_str(), len);
	//erase
	char* pch;
	pch = strchr(dest, character);
	while (pch != NULL)
	{
		len -= pch - start;
		memmove(pch, pch + 1, len - 1);
		start = pch;
		//continue
		pch = strchr(pch, character);
	}
	string outStr(dest);
	delete[] dest;
	return outStr;
}

string replaceCharacter(const string& source, int character,
		int replacement)
{
	int len = source.size() + 1;
	char* dest = new char[len];
	strncpy(dest, source.c_str(), len);
	//replace hyphens
	char* pch;
	pch = strchr(dest, character);
	while (pch != NULL)
	{
		*pch = replacement;
		pch = strchr(pch + 1, character);
	}
	string outStr(dest);
	delete[] dest;
	return outStr;
}

///ThrowEventData
void ThrowEventData::write_datagram(Datagram &dg) const
{
	dg.add_bool(mEnable);
	dg.add_string(mEventName);
	dg.add_bool(mThrown);
	dg.add_stdfloat(mTimeElapsed);
	dg.add_uint32(mCount);
	dg.add_stdfloat(mFrequency);
	dg.add_stdfloat(mPeriod);
}

void ThrowEventData::read_datagram(DatagramIterator &scan)
{
	mEnable = scan.get_bool();
	mEventName = scan.get_string();
	mThrown = scan.get_bool();
	mTimeElapsed = scan.get_stdfloat();
	mCount = scan.get_uint32();
	mFrequency = scan.get_stdfloat();
	mPeriod = scan.get_stdfloat();
}

/**
 * Definitions for parameters management.
 */
void set_parameter_values(ParameterTable& table, const string& paramName,
		const ValueList<string>& paramValues)
{
	pair<ParameterTableIter, ParameterTableIter> iterRange;
	//find from mParameterTable the paramName's values to be overwritten
	iterRange = table.equal_range(paramName);
	//...&& erase them
	table.erase(iterRange.first, iterRange.second);
	//insert the new values
	for (int idx = 0; idx < paramValues.size(); ++idx)
	{
		table.insert(ParameterNameValue(paramName, paramValues[idx]));
	}
}

ValueList<string> get_parameter_values(const ParameterTable& table,
		const string& paramName)
{
	ValueList<string> strList;
	ParameterTableConstIter iter;
	pair<ParameterTableConstIter, ParameterTableConstIter> iterRange;
	iterRange = table.equal_range(paramName);
	if (iterRange.first != iterRange.second)
	{
		for (iter = iterRange.first; iter != iterRange.second; ++iter)
		{
			strList.add_value(iter->second);
		}
	}
	return strList;
}

ValueList<string> get_parameter_name_list(const ParameterTable& table)
{
	ValueList<string> strList;
	ParameterTableIter iter;
	ParameterTable tempTable;
	tempTable = table;
	for (iter = tempTable.begin(); iter != tempTable.end(); ++iter)
	{
		string name = (*iter).first;
		if (!strList.has_value(name))
		{
			strList.add_value(name);
		}
	}
	return strList;
}

void set_parameters_defaults(ParameterTable& table,
		const ValueList<Pair<string,string>>& nameValueList)
{
	///table must be the first cleared
	table.clear();
	//sets the (mandatory) parameters to their default values:
	for (int i = 0; i < nameValueList.size(); i++)
	{
		table.insert(
				ParameterNameValue(nameValueList[i].first(),
						nameValueList[i].second()));
	}
}

///Pair template specializations
#if defined(PYTHON_BUILD)
//bool,float
template<>
void Pair<bool, float>::do_insert_iterable_items(PyObject *iterator)
{
	// check first 2 elements
	for (int i = 0; i < 2 ; i++)
	{
		PyObject *item = NULL;
		item = PyIter_Next(iterator);
		if (item)
		{
			if (i == 0)
			{
				if (PyBool_Check(item))
				{
					item == Py_False ? mPair.first = false: mPair.first = true;
				}
				else
				{
					cerr << "Pair: item '" << get_py_object_name(item) << "' cannot be inserted" << endl;
					// insert a default value
					mPair.first = false;
				}
			}
			else if (i == 1)
			{
				if (PyFloat_Check(item))
				{
					mPair.second = (float) PyFloat_AsDouble(item);
				}
#if PY_MAJOR_VERSION < 3
				else if (PyInt_Check(item))
				{
					mPair.second = (float)PyInt_AS_LONG(item);
				}
#endif
				else if (PyLong_Check(item))
				{
					mPair.second = (float)PyLong_AsLong(item);
				}
				else
				{
					cerr << "Pair: item '" << get_py_object_name(item) << "' cannot be inserted" << endl;
					// insert a default value
					mPair.second = 0.0;
				}
			}
		}
		Py_XDECREF(item);
	}

	if (PyErr_Occurred())
	{
		mPair = make_pair(false, 0.0);
		cerr << "Pair: an error occurred on insertion" << endl;
	}
}
//LPoint3f,int
template<>
void Pair<LPoint3f, int>::do_insert_iterable_items(PyObject *iterator)
{
	// check first 2 elements
	for (int i = 0; i < 2 ; i++)
	{
		PyObject *item = NULL;
		item = PyIter_Next(iterator);
		if (item)
		{
			if (i == 0)
			{
				LPoint3f *into = NULL;
				if (DtoolInstance_GetPointer(item, into))
				{
					mPair.first = *into;
				}
				else
				{
					cerr << "Pair: item '" << get_py_object_name(item) << "' cannot be inserted" << endl;
					// insert a default value
					mPair.first = LPoint3f();
				}
			}
			else if (i == 1)
			{
#if PY_MAJOR_VERSION < 3
				if (PyInt_Check(item))
				{
					mPair.second = (int)PyInt_AsUnsignedLongMask(item);
				}
				else
#endif
				if (PyLong_Check(item))
				{
					mPair.second = (int)PyLong_AsUnsignedLongMask(item);
				}
				else if (PyFloat_Check(item))
				{
					mPair.second = (int)PyFloat_AsDouble(item);
				}
				else
				{
					cerr << "Pair: item '" << get_py_object_name(item) << "' cannot be inserted" << endl;
					// insert a default value
					mPair.second = 0;
				}
			}
		}
		Py_XDECREF(item);
	}

	if (PyErr_Occurred())
	{
		mPair = make_pair(LPoint3f(), 0);
		cerr << "Pair: an error occurred on insertion" << endl;
	}
}
#endif //PYTHON_BUILD

///ValueList template specializations.
#if defined(PYTHON_BUILD)
//int
template<>
void ValueList<int>::do_insert_iterable_items(PyObject *iterator)
{
	PyObject *item = NULL;
	while (item = PyIter_Next(iterator))
	{
#if PY_MAJOR_VERSION < 3
		if (PyInt_Check(item))
		{
			_values.push_back((int)PyInt_AS_LONG(item));
		}
		else
#endif
		if (PyLong_Check(item))
		{
			_values.push_back((int)PyLong_AsLong(item));
		}
		else if (PyFloat_Check(item))
		{
			_values.push_back((int)PyFloat_AsDouble(item));
		}
		else
		{
			cerr << "ValueList: item '" << get_py_object_name(item) << "' cannot be inserted" << endl;
			// insert a default value
			_values.push_back(0);
		}
		Py_DECREF(item);
	}

	if (PyErr_Occurred())
	{
		_values.clear();
		cerr << "ValueList: an error occurred on insertion" << endl;
	}
}
//float
template<>
void ValueList<float>::do_insert_iterable_items(PyObject *iterator)
{
	PyObject *item = NULL;
	while (item = PyIter_Next(iterator))
	{
		if (PyFloat_Check(item))
		{
			_values.push_back((float)PyFloat_AsDouble(item));
		}
#if PY_MAJOR_VERSION < 3
		else if (PyInt_Check(item))
		{
			_values.push_back((float)PyInt_AS_LONG(item));
		}
#endif
		else if (PyLong_Check(item))
		{
			_values.push_back((float)PyLong_AsLong(item));
		}
		else
		{
			cerr << "ValueList: item '" << get_py_object_name(item) << "' cannot be inserted" << endl;
			// insert a default value
			_values.push_back(0.0);
		}
		Py_DECREF(item);
	}

	if (PyErr_Occurred())
	{
		_values.clear();
		cerr << "ValueList: an error occurred on insertion" << endl;
	}
}
//string
template<>
void ValueList<string>::do_insert_iterable_items(PyObject *iterator)
{
	PyObject *item = NULL;
	while (item = PyIter_Next(iterator))
	{
#if PY_MAJOR_VERSION < 3
		if (PyString_Check(item))
#else
		if (PyUnicode_Check(item))
#endif
		{
			_values.push_back(get_py_object_name(item));
		}
		else
		{
			cerr << "ValueList: item '" << get_py_object_name(item) << "' cannot be inserted" << endl;
			// insert a default value
			_values.push_back(string());
		}
		Py_DECREF(item);
	}

	if (PyErr_Occurred())
	{
		_values.clear();
		cerr << "ValueList: an error occurred on insertion" << endl;
	}
}
#endif //PYTHON_BUILD
