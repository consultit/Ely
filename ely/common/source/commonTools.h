/**
 * \file commonTools.h
 *
 * \date 2016-10-03
 * \author consultit
 */
#ifndef COMMONTOOLS_H_
#define COMMONTOOLS_H_

#include "genericAsyncTask.h"
#include "event.h"
#include "plist.h"
#include "nodePath.h"
#include "collisionTraverser.h"
#include "collisionHandlerQueue.h"
#include "collisionRay.h"

using namespace std;

#ifdef _WIN32
#include <ciso646>
#define STRTOF (float)strtod
#else
#define STRTOF strtof
#endif

#ifdef PYTHON_BUILD
#include "py_panda.h"
#endif //PYTHON_BUILD

///Macros
#	define PREFIX __FILE__ << ":" << __LINE__ << " - "
#ifdef ELY_DEBUG
#	define PRINT_DEBUG(msg) cout << PREFIX << msg << endl
#	define PRINT_ERR_DEBUG(msg) cerr << PREFIX << msg << endl
#   define PRINT(msg)
#   define PRINT_ERR(msg)
#	define ASSERT_TRUE(cond) \
		{\
			if (!(cond))\
			{\
			  cerr << PREFIX << "assertion error : (" << #cond << ": "\
			  << __FILE__ << ", " << __LINE__ << ")" << endl;\
			  abort();\
			}\
		}
#else
#	define PRINT_DEBUG(msg)
#	define PRINT_ERR_DEBUG(msg)
#   define PRINT(msg) cout << PREFIX << msg << endl
#   define PRINT_ERR(msg) cerr << PREFIX << msg << endl
#	define ASSERT_TRUE(cond)
#endif

#define RETURN_ON_COND(_flag_,_return_)\
	{\
		if (_flag_)\
		{\
			return _return_;\
		}\
	}

//continue if condition is true else return a value
#define CONTINUE_IF_ELSE_R(condition, return_value) \
	{\
		if (!(condition))\
		{\
		  return return_value;\
		}\
	}
//continue if condition is true else return (void)
#define CONTINUE_IF_ELSE_V(condition) \
	{\
		if (!(condition))\
		{\
		  return;\
		}\
	}

/**
 * An automatic Singleton Utility.
 *
 * \note This Singleton class is based on the article "An automatic
 * Singleton Utility" by Scott Bilas in "Game Programming Gems 1" book.
 * Non multi-threaded.
 */
template<typename T> class Singleton
{
	static T* ms_Singleton;

public:
	Singleton(void)
	{
		assert(!ms_Singleton);
		unsigned long int offset = (unsigned long int) (T*) 1
				- (unsigned long int) (Singleton<T>*) (T*) 1;
		ms_Singleton = (T*) ((unsigned long int) this + offset);
	}
	~Singleton(void)
	{
		assert(ms_Singleton);
		ms_Singleton = 0;
	}
	static T& GetSingleton(void)
	{
		assert(ms_Singleton);
		return (*ms_Singleton);
	}
	static T* GetSingletonPtr(void)
	{
		return (ms_Singleton);
	}
};

template<typename T> T* Singleton<T>::ms_Singleton = 0;

/**
 * A Pair template.
 */
template<typename T1, typename T2> struct Pair
{
PUBLISHED:
	Pair();
	Pair(const T1& first, const T2& second);
	Pair(const Pair &copy);
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	Pair(PyObject *obj);
	INLINE Pair& operator =(PyObject *obj);
#endif //PYTHON_BUILD
	INLINE Pair& operator =(const Pair &copy);
	INLINE bool operator== (const Pair &other) const;
	INLINE void set_first(const T1& first);
	INLINE T1 get_first() const;
	INLINE void set_second(const T2& second);
	INLINE T2 get_second() const;
	// Python Properties
	MAKE_PROPERTY(first, get_first, set_first);
	MAKE_PROPERTY(second, get_second, set_second);

	void output(ostream &out) const;

public:
	Pair(const pair<T1, T2>& value);
	T1& first()
	{
		return mPair.first;
	}
	T2& second()
	{
		return mPair.second;
	}
private:
	pair<T1, T2> mPair;
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	void do_insert_iterable_items(PyObject *iterator);
#endif //PYTHON_BUILD

public:
	static TypeHandle get_class_type()
	{
		return _type_handle;
	}
	static void init_type(const string& module = string())
	{
		register_type(_type_handle, string("Pair") + module);
	}

private:
	static TypeHandle _type_handle;

};
template<typename T1, typename T2> INLINE ostream &operator << (ostream &out,
		const Pair<T1,T2> & pair);

/**
 * Pair that can be used with PT/CPT
 */
template<typename T1, typename T2> struct PairRC: public Pair<T1, T2>,
		public ReferenceCount
{
PUBLISHED:
	PairRC() :
			Pair<T1, T2>()
	{
	}
	PairRC(const T1& first, const T2& second) :
			Pair<T1, T2>(first, second)
	{
	}

public:
	static TypeHandle get_class_type()
	{
		return _type_handle;
	}
	static void init_type(const string& module = string())
	{
		Pair<T1, T2>::init_type();
		register_type(_type_handle, string("PairRC") + module);
	}

private:
	static TypeHandle _type_handle;
};

/**
 * Template struct for generic Event Callback interface
 *
 * The effective Event Callbacks are composed by a Pair of an object and
 * a method (member function) doing the effective task.
 * To define a event callback:
 * 1) in class A define a (pointer to) TaskData member:
 * \code
 * 	SMARTPTR(EventCallbackInterface<A>::EventCallbackData) myData;
 * \endcode
 * 2) and a method (that will execute the real event callback) with signature:
 * \code
 * 	void myEventCallback(const Event* event);
 * \endcode
 * 3) in code associate to myData a new EventCallbackData referring to this
 * class instance and myEventCallback:
 * \code
 * 	myData = new EventCallbackInterface<A>::
 * 			EventCallbackData(this, &A::myEventCallback);
 * \endcode
 * 4) finally register this event callback to your global event handler
 * with data parameter equal to myData (reinterpreted as void*):
 * \code
 * 	pandaFramework.define_key("myKey", "myEventCallback",
 * 		&EventCallbackInterface<A>::eventCallbackFunction,
 * 			reinterpret_cast<void*>(myData.p()));
 * \endcode
 * or
 * \code
 * 	EventHandler::get_global_event_handler()->add_hook("myKey",
 * 		&EventCallbackInterface<A>::eventCallbackFunction,
 * 			reinterpret_cast<void*>(myData.p()));
 * \endcode
 * From now on myEventCallback will execute the event callback, while being able
 * to refer directly to data members of the A class instance.
 */
#ifndef CPPPARSER
#define CONST const
#else
#define CONST
#endif
template<typename A> struct EventCallbackInterface
{
	typedef void (A::*EventCallbackPtr)(CONST Event* event);
	typedef PairRC<A*, EventCallbackPtr> EventCallbackData;
	static void eventCallbackFunction(CONST Event* event, void* data)
	{
		EventCallbackData* appData = reinterpret_cast<EventCallbackData*>(data);
		((appData->first())->*(appData->second()))(event);
	}
};

/**
 * Template struct for generic Task Function interface
 *
 * The effective Tasks are composed by a Pair of an object and
 * a method (member function) doing the effective task.
 * To register a task:
 * 1) in class A define a (pointer to) TaskData member:
 * \code
 * 	SMARTPTR(TaskInterface<A>::TaskData) myData;
 * \endcode
 * 2) and a method (that will execute the real task) with signature:
 * \code
 * 	AsyncTask::DoneStatus myTask(GenericAsyncTask* task);
 * \endcode
 * 3) in code associate to myData a new TaskData referring to this
 * class instance and myTask, then create a new GenericAsyncTask
 * referring to taskFunction and with data parameter equal to
 * myData (reinterpreted as void*):
 * \code
 * 	myData = new TaskInterface<A>::TaskData(this, &A::myTask);
 * 	AsyncTask* task = new GenericAsyncTask("my task",
 * 							&TaskInterface<A>::taskFunction,
 * 							reinterpret_cast<void*>(myData.p()));
 * \endcode
 * 4) finally register the async-task to your manager:
 * \code
 * 	pandaFramework.get_task_mgr().add(task);
 * 	\endcode
 * From now on myTask will execute the task, while being able
 * to refer directly to data members of the A class instance.
 */
template<typename A> struct TaskInterface
{
	typedef AsyncTask::DoneStatus (A::*TaskPtr)(GenericAsyncTask* taskFunction);
	typedef PairRC<A*, TaskPtr> TaskData;
	static AsyncTask::DoneStatus taskFunction(GenericAsyncTask* task,
			void * data)
	{
		TaskData* appData = reinterpret_cast<TaskData*>(data);
		return ((appData->first())->*(appData->second()))(task);
	}
};

/**
 * Throwing event data.
 *
 * Data related to throwing events by components.
 */
class ThrowEventData
{
	struct Frequency;
	struct Period
	{
		Period() : mPeriod(), mFrequency()
		{
		}
		Period& operator =(const Period& other)
		{
			mPeriod = other.mPeriod;
			mPeriod <= FLT_MIN ?
					mFrequency->mFrequency = FLT_MAX :
					mFrequency->mFrequency = 1.0 / mPeriod;
			return *this;
		}
		Period& operator =(float value)
		{
			mPeriod = abs(value);
			mPeriod <= FLT_MIN ?
					mFrequency->mFrequency = FLT_MAX :
					mFrequency->mFrequency = 1.0 / mPeriod;
			return *this;
		}
		operator float() const
		{
			return mPeriod;
		}
		Frequency* mFrequency;
		float mPeriod;
	};
	struct Frequency
	{
		Frequency() : mFrequency(), mPeriod()
		{
		}
		Frequency& operator =(const Frequency& other)
		{
			mFrequency = other.mFrequency;
			mFrequency <= FLT_MIN ?
					mPeriod->mPeriod = FLT_MAX :
					mPeriod->mPeriod = 1.0 / mFrequency;
			return *this;
		}
		Frequency& operator =(float value)
		{
			mFrequency = abs(value);
			mFrequency <= FLT_MIN ?
					mPeriod->mPeriod = FLT_MAX :
					mPeriod->mPeriod = 1.0 / mFrequency;
			return *this;
		}
		operator float() const
		{
			return mFrequency;
		}
		Period* mPeriod;
		float mFrequency;
	};

public:
	ThrowEventData() :
			mEnable(false), mEventName(string("")), mThrown(false), mTimeElapsed(
					0), mCount(0)
	{
		mFrequency.mPeriod = &mPeriod;
		mPeriod.mFrequency = &mFrequency;
		mFrequency = 30.0;
	}
	ThrowEventData(const string& eventName, float frequency):
		mEnable(false), mEventName(eventName), mThrown(false), mTimeElapsed(
				0), mCount(0)
	{
		mFrequency.mPeriod = &mPeriod;
		mPeriod.mFrequency = &mFrequency;
		mFrequency = frequency;
	}
	bool mEnable;
	string mEventName;
	bool mThrown;
	float mTimeElapsed;
	unsigned int mCount;
	Frequency mFrequency;
	Period mPeriod;

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};

/**
 * Template function for conversion values to string.
 */
template<typename Type> string str(Type value)
{
	return static_cast<ostringstream&>(ostringstream().operator <<(value)).str();
}

/**
 * Parses a string composed by substrings separated by a character
 * separator.
 * \note all blanks are erased before parsing.
 * @param srcCompoundString The source string.
 * @param separator The character separator.
 * @return The substrings vector.
 */
pvector<string> parseCompoundString(
		const string& srcCompoundString, char separator);

/**
 * \brief Into a given string, replaces any occurrence of a character with
 * another character.
 * @param source The source string.
 * @param character Character to be replaced .
 * @param replacement Replaced character.
 * @return The result string.
 */
string replaceCharacter(const string& source, int character,
		int replacement);

/**
 * \brief Into a given string, erases any occurrence of a given character.
 * @param source The source string.
 * @param character To be erased character.
 * @return The result string.
 */
string eraseCharacter(const string& source, int character);

/**
 * ValueList template.
 * \note in Python it behaves (mostly) as an iterable.
 */
template<typename Type>
class ValueList
{
PUBLISHED:
	ValueList(unsigned int size=0);
	ValueList(const ValueList &copy);
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	ValueList(PyObject *obj);
	INLINE ValueList& operator =(PyObject *obj);
#endif //PYTHON_BUILD
	INLINE ~ValueList();

	INLINE ValueList& operator =(const ValueList &copy);
	INLINE bool operator== (const ValueList &other) const;
	INLINE void add_value(const Type& value);
	bool remove_value(const Type& value);
	bool has_value(const Type& value) const;
	void add_values_from(const ValueList &other);
	void remove_values_from(const ValueList &other);
	INLINE void clear();
	INLINE int get_num_values() const;
	INLINE void set_value(int index, const Type& value);
	INLINE Type get_value(int index) const;
	MAKE_SEQ(get_values, get_num_values, get_value);
	INLINE Type operator [](int index) const;
	INLINE Type& operator [](int index);
	INLINE int size() const;
	INLINE void operator +=(const ValueList &other);
	INLINE ValueList operator +(const ValueList &other) const;

	void output(ostream &out) const;

#ifndef CPPPARSER
public:
	ValueList(initializer_list<Type> lst);
	ValueList(const pvector<Type>& lst);
	ValueList(const plist<Type>& lst);
	operator plist<Type>() const;
	operator pvector<Type>() const;
#endif //CPPPARSER
	static Type dummy;

private:
#ifndef CPPPARSER
	pvector<Type> _values;
#endif //CPPPARSER
#if defined(PYTHON_BUILD)
	void do_insert_iterable_items(PyObject *iterator);
#endif //PYTHON_BUILD

public:
	static TypeHandle get_class_type()
	{
		return _type_handle;
	}
	static void init_type(const string& module = string())
	{
		register_type(_type_handle, string("ValueList") + module);
	}

private:
	static TypeHandle _type_handle;

};
template<typename Type> INLINE ostream &operator << (ostream &out,
		const ValueList<Type> & lst);

/**
 * Declarations for parameters management.
 */
typedef multimap<string, string> ParameterTable;
typedef multimap<string, string>::iterator ParameterTableIter;
typedef multimap<string, string>::const_iterator ParameterTableConstIter;
typedef map<string, ParameterTable> ParameterTableMap;
typedef pair<string, string> ParameterNameValue;
void set_parameter_values(ParameterTable& table, const string& paramName,
		const ValueList<string>& paramValues);
ValueList<string> get_parameter_values(const ParameterTable& table,
		const string& paramName);
ValueList<string> get_parameter_name_list(const ParameterTable& table);
void set_parameters_defaults(ParameterTable& table,
		const ValueList<Pair<string,string>>& nameValueList);

///Result values
#define RESULT_SUCCESS 0
#define RESULT_ERROR -1

///Helpers.
#if defined(PYTHON_BUILD)
inline string get_py_object_name(PyObject *obj);
#endif //PYTHON_BUILD

///inline
#include "commonTools.I"

#endif /* COMMONTOOLS_H_ */
