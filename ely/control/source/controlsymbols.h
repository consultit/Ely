/**
 * \file controlsymbols.h
 *
 * \date 2018-10-03
 * \author consultit
 */
#ifndef CONTROL_SOURCE_CONTROLSYMBOLS_H_
#define CONTROL_SOURCE_CONTROLSYMBOLS_H_

#ifdef PYTHON_BUILD_control
#	define EXPCL_CONTROL EXPORT_CLASS
#	define EXPTP_CONTROL EXPORT_TEMPL
#else
#	define EXPCL_CONTROL IMPORT_CLASS
#	define EXPTP_CONTROL IMPORT_TEMPL
#endif

#endif /* CONTROL_SOURCE_CONTROLSYMBOLS_H_ */
