/**
 * \file gameControlManager.cxx
 *
 * \date 2016-09-18
 * \author consultit
 */

#include "gameControlManager.h"

#include "p3Driver.h"
#include "p3Chaser.h"
#include "asyncTaskManager.h"
#include "graphicsWindow.h"
#include "bamFile.h"

///GameControlManager definitions
/**
 *
 */
GameControlManager::GameControlManager(const GameControlManager::Output& win,
		int taskSort, const NodePath& root, const CollideMask& mask):
		mWin{win.mWin},
		mTaskSort{taskSort},
		mReferenceNP{NodePath("ReferenceNode")},
		mUtils{root, mask},
		mRef{0}
{
	PRINT_DEBUG("GameControlManager::GameControlManager: creating the singleton manager.");

	// drivers
	mDrivers.clear();
	mDriversParameterTable.clear();
	set_parameters_defaults(DRIVER);
	// chasers
	mChasers.clear();
	mChasersParameterTable.clear();
	set_parameters_defaults(CHASER);
	//
	mUpdateData.clear();
	mUpdateTask.clear();
}

/**
 *
 */
GameControlManager::~GameControlManager()
{
	PRINT_DEBUG("GameControlManager::~GameControlManager: destroying the singleton manager.");

	//stop any default update
	stop_default_update();
	{
		//destroy all P3Drivers
		PTA(PT(P3Driver))::iterator iterN = mDrivers.begin();
		while (iterN != mDrivers.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to P3Driver to cleanup itself before being destroyed.
			(*iterN)->do_finalize();
			//remove the P3Drivers from the inner list (and from the update task)
			iterN = mDrivers.erase(iterN);
		}

		//destroy all P3Chasers
		PTA(PT(P3Chaser))::iterator iterC = mChasers.begin();
		while (iterC != mChasers.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to P3Chaser to cleanup itself before being destroyed.
			(*iterC)->do_finalize();
			//remove the P3Chasers from the inner list (and from the update task)
			iterC = mChasers.erase(iterC);
		}
	}
	//clear parameters' tables
	mDriversParameterTable.clear();
	mChasersParameterTable.clear();
}

/**
 * Creates a P3Driver with a given (mandatory and not empty) name.
 * Returns a NodePath to the new P3Driver,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GameControlManager::create_driver(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(P3Driver)newDriver = new P3Driver(name);
	nassertr_always(newDriver, NodePath::fail())

	// set reference node
	newDriver->mReferenceNP = mReferenceNP;
	// reparent to reference node and set "this" NodePath
	NodePath np = mReferenceNP.attach_new_node(newDriver);
	// set the reference graphic window.
	newDriver->mWin = mWin;
	// initialize the new Driver (could use mReferenceNP, mThisNP, mWin)
	newDriver->do_initialize();

	// add the new Driver to the inner list (and to the update task)
	mDrivers.push_back(newDriver);
	//
	return np;
}

/**
 * Destroys a P3Driver.
 * Returns false on error.
 */
bool GameControlManager::destroy_driver(NodePath driverNP)
{
	CONTINUE_IF_ELSE_R(
			driverNP.node()->is_of_type(P3Driver::get_class_type()),
			false)

	PT(P3Driver)driver = DCAST(P3Driver, driverNP.node());
	DriverList::iterator iter = find(mDrivers.begin(),
			mDrivers.end(), driver);
	CONTINUE_IF_ELSE_R(iter != mDrivers.end(), false)

	// give a chance to P3Driver to cleanup itself before being destroyed.
	driver->do_finalize();
	// reset the reference graphic window.
	driver->mWin.clear();
	//remove the P3Driver from the inner list (and from the update task)
	mDrivers.erase(iter);
	//
	return true;
}

/**
 * Gets an P3Driver by index, or NULL on error.
 */
PT(P3Driver) GameControlManager::get_driver(int index) const
{
	nassertr_always((index >= 0) && (index < (int ) mDrivers.size()),
			nullptr)

	return mDrivers[index];
}

/**
 * Creates a P3Chaser with a given (mandatory and not empty) name.
 * Returns a NodePath to the new P3Chaser,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GameControlManager::create_chaser(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(P3Chaser) newChaser = new P3Chaser(name);
	nassertr_always(newChaser, NodePath::fail())

	// set reference node
	newChaser->mReferenceNP = mReferenceNP;
	// reparent to reference node and set "this" NodePath
	NodePath np = mReferenceNP.attach_new_node(newChaser);
	// set the reference graphic window.
	newChaser->mWin = mWin;
	// initialize the new Chaser (could use mReferenceNP, mThisNP, mWin)
	newChaser->do_initialize();

	// add the new Chaser to the inner list (and to the update task)
	mChasers.push_back(newChaser);
	//
	return np;
}

/**
 * Destroys a P3Chaser.
 * Returns false on error.
 */
bool GameControlManager::destroy_chaser(NodePath chaserNP)
{
	CONTINUE_IF_ELSE_R(
			chaserNP.node()->is_of_type(P3Chaser::get_class_type()),
			false)

	PT(P3Chaser)chaser = DCAST(P3Chaser, chaserNP.node());
	ChaserList::iterator iter = find(mChasers.begin(),
			mChasers.end(), chaser);
	CONTINUE_IF_ELSE_R(iter != mChasers.end(), false)

	// give a chance to P3Chaser to cleanup itself before being destroyed.
	chaser->do_finalize();
	// reset the reference graphic window.
	chaser->mWin.clear();
	//remove the P3Chaser from the inner list (and from the update task)
	mChasers.erase(iter);
	//
	return true;
}

/**
 * Gets an P3Chaser by index, or NULL on error.
 */
PT(P3Chaser) GameControlManager::get_chaser(int index) const
{
	nassertr_always((index >= 0) && (index < (int ) mChasers.size()),
			nullptr)

	return mChasers[index];
}

/**
 * Sets a multi-valued parameter to a multi-value overwriting the existing one(s).
 */
void GameControlManager::set_parameter_values(ControlType type, const string& paramName,
		const ValueList_string& paramValues)
{
	switch (type)
	{
	case DRIVER:
		::set_parameter_values(mDriversParameterTable, paramName,
				paramValues);
		break;
	case CHASER:
		::set_parameter_values(mChasersParameterTable, paramName,
				paramValues);
		break;
	default:
		break;
	}
}

/**
 * Gets the multiple values of a (actually set) parameter.
 */
ValueList_string GameControlManager::get_parameter_values(ControlType type,
		const string& paramName) const
{
	switch (type)
	{
	case DRIVER:
		return ::get_parameter_values(mDriversParameterTable, paramName);
	case CHASER:
		return ::get_parameter_values(mChasersParameterTable, paramName);
	default:
		break;
	}
	return ValueList_string();
}

/**
 * Sets a multi/single-valued parameter to a single value overwriting the existing one(s).
 */
void GameControlManager::set_parameter_value(ControlType type, const string& paramName,
		const string& value)
{
	ValueList_string valueList;
	valueList.add_value(value);
	set_parameter_values(type, paramName, valueList);
}

/**
 * Gets a single value (i.e. the first one) of a parameter.
 */
string GameControlManager::get_parameter_value(ControlType type,
		const string& paramName) const
{
	ValueList_string valueList = get_parameter_values(type, paramName);
	return (valueList.size() != 0 ? valueList[0] : string(""));
}

/**
 * Gets a list of the names of the parameters actually set.
 */
ValueList_string GameControlManager::get_parameter_name_list(ControlType type) const
{
	switch (type)
	{
	case DRIVER:
		return ::get_parameter_name_list(mDriversParameterTable);
	case CHASER:
		return ::get_parameter_name_list(mChasersParameterTable);
	default:
		break;
	}
	return ValueList_string();
}

/**
 * Sets all parameters to their default values (if any).
 * \note: After reading objects from bam files, the objects' creation parameters
 * which reside in the manager, are reset to their default values.
 */
void GameControlManager::set_parameters_defaults(ControlType type)
{
	switch (type)
	{
	case DRIVER:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"enabled", "true"},
			{"forward", "enabled"},
			{"backward", "enabled"},
			{"head_limit", "false@0.0"},
			{"head_left", "enabled"},
			{"head_right", "enabled"},
			{"pitch_limit", "false@0.0"},
			{"pitch_up", "enabled"},
			{"pitch_down", "enabled"},
			{"strafe_left", "enabled"},
			{"strafe_right", "enabled"},
			{"up", "enabled"},
			{"down", "enabled"},
			{"mouse_move", "disabled"},
			{"mouse_head", "disabled"},
			{"mouse_pitch", "disabled"},
			{"inverted_translation", "false"},
			{"inverted_rotation", "false"},
			{"max_linear_speed", "5.0"},
			{"max_angular_speed", "5.0"},
			{"linear_accel", "5.0"},
			{"angular_accel", "5.0"},
			{"linear_friction", "0.1"},
			{"angular_friction", "0.1"},
			{"stop_threshold", "0.01"},
			{"fast_factor", "5.0"},
			{"sens_x", "0.2"},
			{"sens_y", "0.2"},
		};
		::set_parameters_defaults(mDriversParameterTable, nameValueList);
	}
		break;
	case CHASER:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"enabled", "true"},
			{"backward", "true"},
			{"fixed_relative_position", "true"},
			{"friction", "1.0"},
			{"fixed_look_at", "true"},
			{"mouse_move", "false"},
			{"mouse_head", "false"},
			{"mouse_pitch", "false"},
			{"head_left", "enabled"},
			{"head_right", "enabled"},
			{"pitch_up", "enabled"},
			{"pitch_down", "enabled"},
			{"sens_x", "0.2"},
			{"sens_y", "0.2"},
			{"inverted_rotation", "false"},
		};
		::set_parameters_defaults(mChasersParameterTable, nameValueList);
	}
		break;
	default:
		break;
	}
}

/**
 * Updates control objects.
 *
 * Will be called automatically in a task.
 */
AsyncTask::DoneStatus GameControlManager::update(GenericAsyncTask* task)
{
	float dt = ClockObject::get_global_clock()->get_dt();

#ifdef TESTING
	dt = 0.016666667; //60 fps
#endif

	// call all P3Drivers' update functions, passing delta time
	for (auto driver: mDrivers)
	{
		driver->update(dt);
	}
	// call all P3Chasers' update functions, passing delta time
	for (auto chaser: mChasers)
	{
		chaser->update(dt);
	}
	//
	return AsyncTask::DS_cont;
}


/**
 * Adds a task to repeatedly call control updates.
 */
void GameControlManager::start_default_update()
{
	//create the task for updating AI objects
	mUpdateData = new TaskInterface<GameControlManager>::TaskData(this,
			&GameControlManager::update);
	mUpdateTask = new GenericAsyncTask(string("GameControlManager::update"),
			&TaskInterface<GameControlManager>::taskFunction,
			reinterpret_cast<void*>(mUpdateData.p()));
	mUpdateTask->set_sort(mTaskSort);
	//Adds mUpdateTask to the active queue.
	AsyncTaskManager::get_global_ptr()->add(mUpdateTask);
}

/**
 * Removes a task to repeatedly call control updates.
 */
void GameControlManager::stop_default_update()
{
	if (mUpdateTask)
	{
		AsyncTaskManager::get_global_ptr()->remove(mUpdateTask);
	}
	//
	mUpdateData.clear();
	mUpdateTask.clear();
}


/**
 * Writes to a bam file the entire collections of control objects and related
 * geometries (i.e. models' NodePaths)
 */
bool GameControlManager::write_to_bam_file(const string& fileName)
{
	string errorReport;
	// write to bam file
	BamFile outBamFile;
	if (outBamFile.open_write(Filename(fileName)))
	{
		cout << "Current system Bam version: "
				<< outBamFile.get_current_major_ver() << "."
				<< outBamFile.get_current_minor_ver() << endl;
		// just write the reference node
		if (!outBamFile.write_object(mReferenceNP.node()))
		{
			errorReport += string("Error writing ") + mReferenceNP.get_name()
					+ string(" node in ") + fileName + string("\n");
		}
		// close the file
		outBamFile.close();
	}
	else
	{
		errorReport += string("\nERROR: cannot open ") + fileName;
	}
	//check
	if (errorReport.empty())
	{
		cout
				<< "SUCCESS: all control object collections were written to "
				<< fileName << endl;
	}
	else
	{
		cerr << errorReport << endl;
	}
	return errorReport.empty();
}

/**
 * Reads from a bam file the entire hierarchy of control objects and related
 * geometries (i.e. models' NodePaths)
 */
bool GameControlManager::read_from_bam_file(const string& fileName)
{
	string errorReport;
	//read from bamFile
	BamFile inBamFile;
	if (inBamFile.open_read(Filename(fileName)))
	{
		cout << "Current system Bam version: "
				<< inBamFile.get_current_major_ver() << "."
				<< inBamFile.get_current_minor_ver() << endl;
		cout << "Bam file version: " << inBamFile.get_file_major_ver() << "."
				<< inBamFile.get_file_minor_ver() << endl;
		// just read the reference node
		TypedWritable* reference = inBamFile.read_object();
		if (reference)
		{
			//resolve pointers
			if (!inBamFile.resolve())
			{
				errorReport += string("Error resolving pointers in ") + fileName
						+ string("\n");
			}
		}
		else
		{
			errorReport += string("Error reading ") + fileName + string("\n");
		}
		// close the file
		inBamFile.close();
		// post process after restoring
		// restore reference node
		mReferenceNP = NodePath::any_path(DCAST(PandaNode, reference));
		// post processing from bam for all managed objects
		for (auto chaser: mChasers)
		{
			chaser->post_process_from_bam();
		}
		for (auto driver: mDrivers)
		{
			driver->post_process_from_bam();
		}
	}
	else
	{
		errorReport += string("\nERROR: cannot open ") + fileName;
	}
	//check
	if (errorReport.empty())
	{
		cout << "SUCCESS: all control objects were read from "
				<< fileName << endl;
	}
	else
	{
		cerr << errorReport << endl;
	}
	return errorReport.empty();
}

TYPED_OBJECT_API_DEF(GameControlManager)
