/**
 * \file gameControlManager.h
 *
 * \date 2016-09-18
 * \author consultit
 */

#ifndef CONTROL_SOURCE_GAMECONTROLMANGER_H_
#define CONTROL_SOURCE_GAMECONTROLMANGER_H_

#include "graphicsWindow.h"
#include "control_includes.h"

class P3Driver;
class P3Chaser;

/**
 * GameControlManager Singleton class.
 *
 * Used for handling P3Drivers, P3Chasers.
 */
class EXPCL_CONTROL GameControlManager: public TypedReferenceCount,
		public Singleton<GameControlManager>
{
PUBLISHED:

	/**
	 * The type of object for creation parameters.
	 */
	enum ControlType: unsigned char
	{
		DRIVER = 0,
		CHASER
	};

	struct EXPCL_CONTROL Output
	{
	PUBLISHED:
		Output() :
				mWin{ nullptr }
		{
		}
		Output(PT(GraphicsWindow)win):
			mWin{win}
		{
		}
	public:
		PT(GraphicsWindow) mWin;
	};

	GameControlManager(const GameControlManager::Output& win,
			int taskSort = 10,	const NodePath& root = NodePath(),
			const CollideMask& mask = GeomNode::get_default_collide_mask());
	virtual ~GameControlManager();

	/**
	 * \name REFERENCE NODES
	 */
	///@{
	INLINE NodePath get_reference_node_path() const;
	INLINE void set_reference_node_path(const NodePath& reference);
	// Python Properties
	MAKE_PROPERTY(reference_node_path, get_reference_node_path, set_reference_node_path);
	///@}

	/**
	 * \name P3Driver
	 */
	///@{
	NodePath create_driver(const string& name);
	bool destroy_driver(NodePath driverNP);
	PT(P3Driver) get_driver(int index) const;
	INLINE int get_num_drivers() const;
	MAKE_SEQ(get_drivers, get_num_drivers, get_driver);
	// Python Properties
	MAKE_PROPERTY(num_drivers, get_num_drivers);
	MAKE_SEQ_PROPERTY(drivers, get_num_drivers, get_driver);
	///@}

	/**
	 * \name P3Chaser
	 */
	///@{
	NodePath create_chaser(const string& name);
	bool destroy_chaser(NodePath chaserNP);
	PT(P3Chaser) get_chaser(int index) const;
	INLINE int get_num_chasers() const;
	MAKE_SEQ(get_chasers, get_num_chasers, get_chaser);
	// Python Properties
	MAKE_PROPERTY(num_chasers, get_num_chasers);
	MAKE_SEQ_PROPERTY(chasers, get_num_chasers, get_chaser);
	///@}

	/**
	 * \name TEXTUAL PARAMETERS
	 */
	///@{
	ValueList_string get_parameter_name_list(ControlType type) const;
	void set_parameter_values(ControlType type, const string& paramName, const ValueList_string& paramValues);
	ValueList_string get_parameter_values(ControlType type, const string& paramName) const;
	void set_parameter_value(ControlType type, const string& paramName, const string& value);
	string get_parameter_value(ControlType type, const string& paramName) const;
	void set_parameters_defaults(ControlType type);
	///@}

	/**
	 * \name DEFAULT UPDATE
	 */
	///@{
	INLINE void update();
#ifndef CPPPARSER
	AsyncTask::DoneStatus update(GenericAsyncTask* task);
#endif //CPPPARSER
	void start_default_update();
	void stop_default_update();
	///@}

	/**
	 * \name SINGLETON
	 */
	///@{
	INLINE static GameControlManager* get_global_ptr();
	///@}

	/**
	 * \name UTILITIES
	 */
	///@{
	INLINE const Utilities& get_utilities();
	// Python Properties
	MAKE_PROPERTY(utilities, get_utilities);
	///@}

	/**
	 * \name SERIALIZATION
	 */
	///@{
	bool write_to_bam_file(const string& fileName);
	bool read_from_bam_file(const string& fileName);
	///@}

public:
	///Unique ref producer.
	inline int unique_ref();

#ifndef CPPPARSER
private:
	///The reference graphic window.
	PT(GraphicsWindow) mWin;
	///The update task sort (should be >0).
	int mTaskSort;

	///The reference node path.
	NodePath mReferenceNP;

	///List of P3Drivers handled by this manager.
	typedef pvector<PT(P3Driver)> DriverList;
	DriverList mDrivers;
	///P3Drivers' parameter table.
	ParameterTable mDriversParameterTable;

	///List of P3Chasers handled by this manager.
	typedef pvector<PT(P3Chaser)> ChaserList;
	ChaserList mChasers;
	///P3Chasers' parameter table.
	ParameterTable mChasersParameterTable;

	///@{
	///A task data for step simulation update.
	PT(TaskInterface<GameControlManager>::TaskData) mUpdateData;
	PT(AsyncTask) mUpdateTask;
	///@}

	///Unique ref.
	int mRef;

	///Utilities.
	Utilities mUtils;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(GameControlManager,TypedReferenceCount)
};

///inline
#include "gameControlManager.I"

#endif /* CONTROL_SOURCE_GAMECONTROLMANGER_H_ */
