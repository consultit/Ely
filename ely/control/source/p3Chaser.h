/**
 * \file p3Chaser.h
 *
 * \date 2016-09-27
 * \author consultit
 */

#ifndef CONTROL_SOURCE_P3CHASER_H_
#define CONTROL_SOURCE_P3CHASER_H_

#include "control_includes.h"
#include "gameControlManager.h"

#ifndef CPPPARSER
#include "support/common.h"
#endif //CPPPARSER

/**
 * P3Chaser is a PandaNode class designed to make an object a chaser of another
 * object.
 *
 * P3Chaser can be enabled/disabled as a whole (enabled by default).\n
 * P3Chaser can handle basic rotations movement (head_left, head_right,
 * pitch_left, pitch_right) only if it is enabled to do so by calling the
 * corresponding "enabler". In turn, an enabled basic rotation movement can be
 * activated/deactivated through the corresponding "activator".\n
 * Rotation is updated relative to:
 * - z local axis, ie head (yaw)
 * - x local axis, ie pitch
 * Rotation through y local axis (roll) is not considered.\n
 * Chasing can be fixed or dampened, from behind or from the front.\n
 * With dampened chasing various movement's parameters (like friction, min/max
 * distance, min/max height etc...) can be set.\n
 * A task could update the position/orientation of the attached PandaNode object
 * based on the current position/orientation of the chased object, and the
 * currently enabled basic rotation movements, by calling the "update()"
 * method.\n
 * Usually movements are activated/deactivated through callback associated to
 * events (keyboard, mouse etc...).\n
 * Since in Panda3d by default, "mouse-move" events are not defined, mouse
 * movements can be enabled/disabled as "implicit activators" of head/pitch
 * basic rotations by calling "enable_mouse_head()"/"enable_mouse_pitch()"
 * methods. In this way head/pitch basic rotations can be activated
 * independently through mouse movements and/or normal activator methods.\n
 * On the other hand, "mouse-move" event could be defined by using
 * \code
 * ButtonThrower::set_move_event()
 * \endcode
 * (see http://www.panda3d.org/forums/viewtopic.php?t=9326 and
 * http://www.panda3d.org/forums/viewtopic.php?t=6049), and in this case
 * if an application wishes to handle directly these events, it has to disable
 * the previously described mouse movements handling by calling:
 * enable_mouse_move(true))
 *
 * The up axis is the "z" axis.\n
 * Rotation movements can be inverted (default: not inverted).
 *
 * > **P3Chaser text parameters**:
 * param | type | default | note
 * ------|------|---------|-----
 * | *enabled*  				|single| *true* | -
 * | *backward*					|single| *true* | -
 * | *fixed_relative_position*	|single| *true* | -
 * | *max_distance*				|single| - | -
 * | *min_distance*				|single| - | -
 * | *max_height*				|single| - | -
 * | *min_height*				|single| - | -
 * | *friction*					|single| 1.0 | -
 * | *fixed_look_at*			|single| *true* | -
 * | *look_at_distance*			|single| - | -
 * | *look_at_height*			|single| - | -
 * | *mouse_move*  				|single| *disabled* | -
 * | *mouse_head*  				|single| *disabled* | -
 * | *mouse_pitch*  			|single| *disabled* | -
 * | *head_left*  				|single| *enabled* | -
 * | *head_right*  				|single| *enabled* | -
 * | *pitch_up*  				|single| *enabled* | -
 * | *pitch_down*  				|single| *enabled* | -
 * | *sens_x*  					|single| 0.2 | -
 * | *sens_y*  					|single| 0.2 | -
 * | *inverted_rotation*		|single| *false* | -
 *
 * \note parts inside [] are optional.\n
 */
class EXPCL_CONTROL P3Chaser: public PandaNode
{
PUBLISHED:

	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	virtual ~P3Chaser();
#endif //CPPPARSER

	/**
	 * \name CHASER
	 */
	///@{
	INLINE void set_chased_object(const NodePath& object);
	INLINE NodePath get_chased_object() const;
	bool enable();
	bool disable();
	INLINE bool get_enabled() const;
	void update(float dt);
	// Python Properties
	MAKE_PROPERTY(chased_object, get_chased_object, set_chased_object);
	MAKE_PROPERTY(is_enabled, get_enabled);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS: MOVEMENT ENABLERS
	 */
	///@{
	INLINE void set_enable_head_left(bool enable);
	INLINE bool get_enable_head_left() const;
	INLINE void set_enable_head_right(bool enable);
	INLINE bool get_enable_head_right() const;
	INLINE void set_enable_pitch_up(bool enable);
	INLINE bool get_enable_pitch_up() const;
	INLINE void set_enable_pitch_down(bool enable);
	INLINE bool get_enable_pitch_down() const;
	INLINE void set_enable_mouse_head(bool enable);
	INLINE bool get_enable_mouse_head() const;
	INLINE void set_enable_mouse_pitch(bool enable);
	INLINE bool get_enable_mouse_pitch() const;
	INLINE void set_enable_mouse_move(bool enable);
	INLINE bool get_enable_mouse_move() const;
	// Python Properties
	MAKE_PROPERTY(enable_head_left, get_enable_head_left, set_enable_head_left);
	MAKE_PROPERTY(enable_head_right, get_enable_head_right, set_enable_head_right);
	MAKE_PROPERTY(enable_pitch_up, get_enable_pitch_up, set_enable_pitch_up);
	MAKE_PROPERTY(enable_pitch_down, get_enable_pitch_down, set_enable_pitch_down);
	MAKE_PROPERTY(enable_mouse_head, get_enable_mouse_head, set_enable_mouse_head);
	MAKE_PROPERTY(enable_mouse_pitch, get_enable_mouse_pitch, set_enable_mouse_pitch);
	MAKE_PROPERTY(enable_mouse_move, get_enable_mouse_move, set_enable_mouse_move);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS: MOVEMENT ACTIVATORS
	 */
	///@{
	INLINE void set_rotate_head_left(bool activate);
	INLINE bool get_rotate_head_left() const;
	INLINE void set_rotate_head_right(bool activate);
	INLINE bool get_rotate_head_right() const;
	INLINE void set_rotate_pitch_up(bool activate);
	INLINE bool get_rotate_pitch_up() const;
	INLINE void set_rotate_pitch_down(bool activate);
	INLINE bool get_rotate_pitch_down() const;
	// Python Properties
	MAKE_PROPERTY(rotate_head_left, get_rotate_head_left, set_rotate_head_left);
	MAKE_PROPERTY(rotate_head_right, get_rotate_head_right, set_rotate_head_right);
	MAKE_PROPERTY(rotate_pitch_up, get_rotate_pitch_up, set_rotate_pitch_up);
	MAKE_PROPERTY(rotate_pitch_down, get_rotate_pitch_down, set_rotate_pitch_down);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE void set_hold_look_at(bool activate);
	INLINE bool get_hold_look_at() const;
	INLINE void set_backward(bool activate);
	INLINE bool get_backward() const;
	INLINE void set_fixed_relative_position(bool activate);
	INLINE bool get_fixed_relative_position() const;
	INLINE void set_inverted_rotation(bool activate);
	INLINE bool get_inverted_rotation() const;
	INLINE void set_max_distance(float absMaxDistance);
	INLINE float get_max_distance() const;
	INLINE void set_min_distance(float absMinDistance);
	INLINE float get_min_distance() const;
	INLINE void set_max_height(float absMaxHeight);
	INLINE float get_max_height() const;
	INLINE void set_min_height(float absMinHeight);
	INLINE float get_min_height() const;
	INLINE void set_look_at_distance(float absLookAtDistance);
	INLINE float get_look_at_distance() const;
	INLINE void set_look_at_height(float absLookAtHeight);
	INLINE float get_look_at_height() const;
	INLINE void set_friction(float friction);
	INLINE float get_friction() const;
	INLINE void set_sens(const ValueList_float& sens);
	INLINE ValueList_float get_sens() const;
	// Python Properties
	MAKE_PROPERTY(hold_look_at, get_hold_look_at, set_hold_look_at);
	MAKE_PROPERTY(backward, get_backward, set_backward);
	MAKE_PROPERTY(fixed_relative_position, get_fixed_relative_position, set_fixed_relative_position);
	MAKE_PROPERTY(inverted_rotation, get_inverted_rotation, set_inverted_rotation);
	MAKE_PROPERTY(max_distance, get_max_distance, set_max_distance);
	MAKE_PROPERTY(min_distance, get_min_distance, set_min_distance);
	MAKE_PROPERTY(max_height, get_max_height, set_max_height);
	MAKE_PROPERTY(min_height, get_min_height, set_min_height);
	MAKE_PROPERTY(look_at_distance, get_look_at_distance, set_look_at_distance);
	MAKE_PROPERTY(look_at_height, get_look_at_height, set_look_at_height);
	MAKE_PROPERTY(friction, get_friction, set_friction);
	MAKE_PROPERTY(sens, get_sens, set_sens);
	///@}

	/**
	 * \name OUTPUT
	 */
	///@{
	void output(ostream &out) const;
	///@}

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	/**
	 * \name PYTHON UPDATE CALLBACK
	 */
	///@{
	void set_update_callback(PyObject *clbk);
	///@}
#else
	/**
	 * \name C++ UPDATE CALLBACK
	 */
	///@{
	typedef void (*UPDATECALLBACKFUNC)(PT(P3Chaser));
	void set_update_callback(UPDATECALLBACKFUNC value);
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name TypedWritable API
	 */
	///@{
	void post_process_from_bam();
	///@}

public:
	/**
	 * \name C++ ONLY
	 * Library & support low level related methods.
	 */
	///@{
	///@}

	// Explicitly deleted copy constructor and copy assignment operator.
	P3Chaser(const P3Chaser&) = delete;
	P3Chaser& operator=(const P3Chaser&) = delete;

#ifndef CPPPARSER
protected:
	friend void unref_delete<P3Chaser>(P3Chaser*);
	friend class GameControlManager;

	P3Chaser(const string& name);
	virtual ~P3Chaser();

private:
	///The chased object's node path.
	NodePath mChasedNP;
	///The reference node path.
	NodePath mReferenceNP;
	///The reference graphic window.
	PT(GraphicsWindow) mWin;
	///Auxiliary node path to track the fixed look at.
	NodePath mFixedLookAtNP;
	///Flags.
	bool mEnabled, mFixedRelativePosition, mBackward, mFixedLookAt, mHoldLookAt;
	///Kinematic parameters.
	float mAbsLookAtDistance, mAbsLookAtHeight, mAbsMaxDistance, mAbsMinDistance,
	mAbsMinHeight, mAbsMaxHeight, mFriction;
	///Positions.
	LPoint3f mChaserPosition, mLookAtPosition;
	///@{
	///Key controls and effective keys.
	bool mHeadLeft, mHeadRight, mPitchUp, mPitchDown;
	bool mHeadLeftKey, mHeadRightKey, mPitchUpKey, mPitchDownKey, mMouseMoveKey;
	///@}
	///@{
	///Key control values.
	bool mMouseEnabledH, mMouseEnabledP, mMouseHandled;
	char mSignOfMouse;
	///@}
	///@{
	/// Sensitivity settings.
	float mSensX, mSensY, mHeadSensX, mHeadSensY;
	int mCentX, mCentY;
	///@}

	inline void do_reset();
	void do_initialize();
	void do_finalize();

	/**
	 * \name Helpers variables/functions.
	 */
	///@{
	void do_enable();
	void do_disable();
	void do_handle_mouse();
	LPoint3f do_get_chaser_pos(LPoint3f desiredChaserPos,
			LPoint3f currentChaserPos, float deltaTime);
	void do_correct_chaser_height(LPoint3f& newPos, float baseHeight);
	///@}

#if defined(PYTHON_BUILD)
	/**
	 * \name Python callback.
	 */
	///@{
	PyObject *mSelf, *mUpdateCallback;
	///@}
#else
	/**
	 * \name C++ callback.
	 */
	///@{
	UPDATECALLBACKFUNC mUpdateCallback;
	///@}
#endif //PYTHON_BUILD
#endif //CPPPARSER

public:
	/**
	 * \name TypedWritable API
	 */
	///@{
	static void register_with_read_factory();
	virtual void write_datagram (BamWriter *manager, Datagram &dg) override;
	virtual int complete_pointers(TypedWritable **p_list, BamReader *manager) override;
	///@}

#ifndef CPPPARSER
protected:
	static TypedWritable *make_from_bam(const FactoryParams &params);
	virtual void fillin(DatagramIterator &scan, BamReader *manager) override;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(P3Chaser,PandaNode)
};

INLINE ostream &operator << (ostream &out, const P3Chaser & chaser);

///inline
#include "p3Chaser.I"

#endif /* CONTROL_SOURCE_P3CHASER_H_ */
