/**
 * \file p3Driver.I
 *
 * \date 2016-09-18
 * \author consultit
 */

#ifndef CONTROL_SOURCE_P3DRIVER_I_
#define CONTROL_SOURCE_P3DRIVER_I_


///P3Driver inline definitions

/**
 * Returns if the P3Driver is enabled (default: enabled).
 */
INLINE bool P3Driver::get_enabled() const
{
	return mEnabled;
}

/**
 * Enables/disables forward movement (default: enabled).
 */
INLINE void P3Driver::set_enable_forward(bool enable)
{
	mForwardKey = enable;
	if (!enable)
	{
		mForward = false;
	}
}

/**
 * Returns if forward movement is enabled/disabled (default: enabled).
 */
INLINE bool P3Driver::get_enable_forward() const
{
	return mForwardKey;
}

/**
 * Enables/disables backward movement (default: enabled).
 */
INLINE void P3Driver::set_enable_backward(bool enable)
{
	mBackwardKey = enable;
	if (!enable)
	{
		mBackward = false;
	}
}

/**
 * Returns if backward movement is enabled/disabled (default: enabled).
 */
INLINE bool P3Driver::get_enable_backward() const
{
	return mBackwardKey;
}

/**
 * Enables/disables strafe left movement (default: enabled).
 */
INLINE void P3Driver::set_enable_strafe_left(bool enable)
{
	mStrafeLeftKey = enable;
	if (!enable)
	{
		mStrafeLeft = false;
	}
}

/**
 * Returns if strafe left movement is enabled/disabled (default: enabled).
 */
INLINE bool P3Driver::get_enable_strafe_left() const
{
	return mStrafeLeftKey;
}

/**
 * Enables/disables strafe right movement (default: enabled).
 */
INLINE void P3Driver::set_enable_strafe_right(bool enable)
{
	mStrafeRightKey = enable;
	if (!enable)
	{
		mStrafeRight = false;
	}
}

/**
 * Returns if strafe right movement is enabled/disabled (default: enabled).
 */
INLINE bool P3Driver::get_enable_strafe_right() const
{
	return mStrafeRightKey;
}

/**
 * Enables/disables up movement (default: enabled).
 */
INLINE void P3Driver::set_enable_up(bool enable)
{
	mUpKey = enable;
	if (!enable)
	{
		mUp = false;
	}
}

/**
 * Returns if up movement is enabled/disabled (default: enabled).
 */
INLINE bool P3Driver::get_enable_up() const
{
	return mUpKey;
}

/**
 * Enables/disables down movement (default: enabled).
 */
INLINE void P3Driver::set_enable_down(bool enable)
{
	mDownKey = enable;
	if (!enable)
	{
		mDown = false;
	}
}

/**
 * Returns if down movement is enabled/disabled (default: enabled).
 */
INLINE bool P3Driver::get_enable_down() const
{
	return mDownKey;
}

/**
 * Enables/disables head left rotation (default: enabled).
 */
INLINE void P3Driver::set_enable_head_left(bool enable)
{
	mHeadLeftKey = enable;
	if (!enable)
	{
		mHeadLeft = false;
	}
}

/**
 * Returns if head left rotation is enabled/disabled (default: enabled).
 */
INLINE bool P3Driver::get_enable_head_left() const
{
	return mHeadLeftKey;
}

/**
 * Enables/disables head right rotation (default: enabled).
 */
INLINE void P3Driver::set_enable_head_right(bool enable)
{
	mHeadRightKey = enable;
	if (!enable)
	{
		mHeadRight = false;
	}
}

/**
 * Returns if head right rotation is enabled/disabled (default: enabled).
 */
INLINE bool P3Driver::get_enable_head_right() const
{
	return mHeadRightKey;
}

/**
 * Enables/disables pitch up rotation (default: enabled).
 */
INLINE void P3Driver::set_enable_pitch_up(bool enable)
{
	mPitchUpKey = enable;
	if (!enable)
	{
		mPitchUp = false;
	}
}

/**
 * Returns if pitch up rotation is enabled/disabled (default: enabled).
 */
INLINE bool P3Driver::get_enable_pitch_up() const
{
	return mPitchUpKey;
}

/**
 * Enables/disables pitch down rotation (default: enabled).
 */
INLINE void P3Driver::set_enable_pitch_down(bool enable)
{
	mPitchDownKey = enable;
	if (!enable)
	{
		mPitchDown = false;
	}
}

/**
 * Returns if pitch down rotation is enabled/disabled (default: enabled).
 */
INLINE bool P3Driver::get_enable_pitch_down() const
{
	return mPitchDownKey;
}

/**
 * Enables/disables mouse head rotation (default: disabled).
 */
INLINE void P3Driver::set_enable_mouse_head(bool enable)
{
	RETURN_ON_COND(!mWin && (mMouseEnabledH == enable),)

	mMouseEnabledH = enable;
	// handle mouse if possible
	do_handle_mouse();
}

/**
 * Returns if mouse head rotation is enabled/disabled (default: disabled).
 */
INLINE bool P3Driver::get_enable_mouse_head() const
{
	return mMouseEnabledH;
}

/**
 * Enables/disables mouse pitch rotation (default: disabled).
 */
INLINE void P3Driver::set_enable_mouse_pitch(bool enable)
{
	RETURN_ON_COND(!mWin && (mMouseEnabledP == enable),)

	mMouseEnabledP = enable;
	// handle mouse if possible
	do_handle_mouse();
}

/**
 * Returns if mouse pitch rotation is enabled/disabled (default: disabled).
 */
INLINE bool P3Driver::get_enable_mouse_pitch() const
{
	return mMouseEnabledP;
}

/**
 * Enables/disables mouse movement externally handled (default: disabled).
 */
INLINE void P3Driver::set_enable_mouse_move(bool enable)
{
	RETURN_ON_COND(!mWin && (mMouseMoveKey == enable),)

	mMouseMoveKey = enable;
	// handle mouse if possible
	do_handle_mouse();
}

/**
 * Returns if mouse movement externally handled is enabled/disabled (default:
 * disabled).
 */
INLINE bool P3Driver::get_enable_mouse_move() const
{
	return mMouseMoveKey;
}

/**
 * Activates/deactivates forward movement.
 */
INLINE void P3Driver::set_move_forward(bool activate)
{
	if (mForwardKey)
	{
		mForward = activate;
	}
}

/**
 * Returns if the forward movement is activated/deactivated.
 */
INLINE bool P3Driver::get_move_forward() const
{
	return mForward;
}

/**
 * Activates/deactivates backward movement.
 */
INLINE void P3Driver::set_move_backward(bool activate)
{
	if (mBackwardKey)
	{
		mBackward = activate;
	}
}

/**
 * Returns if backward movement is activated/deactivated.
 */
INLINE bool P3Driver::get_move_backward() const
{
	return mBackward;
}

/**
 * Activates/deactivates strafe left movement.
 */
INLINE void P3Driver::set_move_strafe_left(bool activate)
{
	if (mStrafeLeftKey)
	{
		mStrafeLeft = activate;
	}
}

/**
 * Returns if strafe left movement is activated/deactivated.
 */
INLINE bool P3Driver::get_move_strafe_left() const
{

	return mStrafeLeft;
}

/**
 * Activates/deactivates strafe right movement.
 */
INLINE void P3Driver::set_move_strafe_right(bool activate)
{
	if (mStrafeRightKey)
	{
		mStrafeRight = activate;
	}
}

/**
 * Returns if strafe right movement is activated/deactivated.
 */
INLINE bool P3Driver::get_move_strafe_right() const
{
	return mStrafeRight;
}

/**
 * Activates/deactivates up movement.
 */
INLINE void P3Driver::set_move_up(bool activate)
{
	if (mUpKey)
	{
		mUp = activate;
	}
}

/**
 * Returns if up movement is activated/deactivated.
 */
INLINE bool P3Driver::get_move_up() const
{
	return mUp;
}

/**
 * Activates/deactivates down movement.
 */
INLINE void P3Driver::set_move_down(bool activate)
{
	if (mDownKey)
	{
		mDown = activate;
	}
}

/**
 * Returns if down movement is activated/deactivated.
 */
INLINE bool P3Driver::get_move_down() const
{
	return mDown;
}

/**
 * Activates/deactivates head left rotation.
 */
INLINE void P3Driver::set_rotate_head_left(bool activate)
{
	if (mHeadLeftKey)
	{
		mHeadLeft = activate;
	}
}

/**
 * Returns if head left rotation is activated/deactivated.
 */
INLINE bool P3Driver::get_rotate_head_left() const
{
	return mHeadLeft;
}

/**
 * Activates/deactivates head right rotation.
 */
INLINE void P3Driver::set_rotate_head_right(bool activate)
{
	if (mHeadRightKey)
	{
		mHeadRight = activate;
	}
}

/**
 * Returns if head right rotation is activated/deactivated.
 */
INLINE bool P3Driver::get_rotate_head_right() const
{
	return mHeadRight;
}

/**
 * Activates/deactivates pitch up rotation.
 */
INLINE void P3Driver::set_rotate_pitch_up(bool activate)
{
	if (mPitchUpKey)
	{
		mPitchUp = activate;
	}
}

/**
 * Returns if pitch up rotation is activated/deactivated.
 */
INLINE bool P3Driver::get_rotate_pitch_up() const
{
	return mPitchUp;
}

/**
 * Activates/deactivates pitch down rotation.
 */
INLINE void P3Driver::set_rotate_pitch_down(bool activate)
{
	if (mPitchDownKey)
	{
		mPitchDown = activate;
	}
}

/**
 * Returns if pitch down rotation is activated/deactivated.
 */
INLINE bool P3Driver::get_rotate_pitch_down() const
{
	return mPitchDown;
}

/**
 * Activates/deactivates inverted translation (default: deactivated).
 */
INLINE void P3Driver::set_inverted_translation(bool activate)
{
	activate ? mSignOfTranslation = -1: mSignOfTranslation = 1;
}

/**
 * Returns if inverted translation is activated/deactivated (default:
 * deactivated).
 */
INLINE bool P3Driver::get_inverted_translation() const
{
	return (mSignOfTranslation == -1) ? true : false;
}

/**
 * Activates/deactivates inverted rotation (default: deactivated).
 */
INLINE void P3Driver::set_inverted_rotation(bool activate)
{
	activate ? mSignOfMouse = -1: mSignOfMouse = 1;
}

/**
 * Returns if inverted rotation is activated/deactivated (default: deactivated).
 */
INLINE bool P3Driver::get_inverted_rotation() const
{
	return (mSignOfMouse == -1) ? true : false;
}

/**
 * Sets head rotation limit (>=0.0).
 */
INLINE void P3Driver::set_head_limit(bool enabled, float hLimit)
{
	mHeadLimitEnabled = enabled;
	hLimit >= 0.0 ? mHLimit = hLimit : mHLimit = -hLimit;
}

/**
 * Returns head rotation limit (>=0.0).
 */
INLINE Pair_bool_float P3Driver::get_head_limit() const
{
	return Pair_bool_float(mHeadLimitEnabled, mHLimit);
}

/**
 * Sets pitch rotation limit (>=0.0).
 */
INLINE void P3Driver::set_pitch_limit(bool enabled, float pLimit)
{
	mPitchLimitEnabled = enabled;
	pLimit >= 0.0 ? mPLimit = pLimit : mPLimit = -pLimit;
}

/**
 * Returns pitch rotation limit (>=0.0).
 */
INLINE Pair_bool_float P3Driver::get_pitch_limit() const
{
	return Pair_bool_float(mPitchLimitEnabled, mPLimit);
}

/**
 * Sets max linear speed along local x,y,z axes independently (>=0.0).
 */
INLINE void P3Driver::set_max_linear_speed(const LVector3f& maxLinearSpeed)
{
	mMaxSpeedXYZ = LVector3f(
			abs(maxLinearSpeed.get_x()),
			abs(maxLinearSpeed.get_y()),
			abs(maxLinearSpeed.get_z()));
	mMaxSpeedSquaredXYZ = LVector3f(
			maxLinearSpeed.get_x() * maxLinearSpeed.get_x(),
			maxLinearSpeed.get_y() * maxLinearSpeed.get_y(),
			maxLinearSpeed.get_z() * maxLinearSpeed.get_z());
}

/**
 * Returns max linear speed along local x,y,z axes independently (>=0.0).
 */
INLINE const LVector3f& P3Driver::get_max_linear_speed() const
{
	return mMaxSpeedXYZ;
}

/**
 * Sets max angular speed for head/pitch rotations (>=0.0).
 */
INLINE void P3Driver::set_max_angular_speed(float maxAngularSpeed)
{
	mMaxSpeedHP = abs(maxAngularSpeed);
	mMaxSpeedSquaredHP = maxAngularSpeed * maxAngularSpeed;
}

/**
 * Returns max angular speed for head/pitch rotations (>=0.0).
 */
INLINE float P3Driver::get_max_angular_speed() const
{
	return mMaxSpeedHP;
}

/**
 * Sets linear accelerations along local x,y,z axes independently (>=0.0).
 * \note The value 0 means that the acceleration is not used and the linear
 * speed is set to the maximum value.
 */
INLINE void P3Driver::set_linear_accel(const LVector3f& linearAccel)
{
	mAccelXYZ = LVector3f(
			abs(linearAccel.get_x()),
			abs(linearAccel.get_y()),
			abs(linearAccel.get_z()));
}

/**
 * Returns linear accelerations along local x,y,z axes independently (>=0.0).
 * \note The value 0 means that the acceleration is not used and the linear
 * speed is set to the maximum value.
 */
INLINE const LVector3f& P3Driver::get_linear_accel() const
{
	return mAccelXYZ;
}

/**
 * Sets max angular acceleration for head/pitch rotations (>=0.0).
 * \note The value 0 means that the acceleration is not used and the angular
 * speed is set to the maximum value.
 */
INLINE void P3Driver::set_angular_accel(float angularAccel)
{
	mAccelHP = abs(angularAccel);
}

/**
 * Returns max angular acceleration for head/pitch rotations (>=0.0).
 * \note The value 0 means that the acceleration is not used and the angular
 * speed is set to the maximum value.
 */
INLINE float P3Driver::get_angular_accel() const
{
	return mAccelHP;
}

/**
 * Sets linear friction along local x,y,z axes (>=0.0).
 */
INLINE void P3Driver::set_linear_friction(float linearFriction)
{
	mFrictionXYZ = abs(linearFriction);
}

/**
 * Returns linear friction along local x,y,z axes (>=0.0).
 */
INLINE float P3Driver::get_linear_friction() const
{
	return mFrictionXYZ;
}

/**
 * Sets angular friction for head/pitch rotations (>=0.0).
 */
INLINE void P3Driver::set_angular_friction(float angularFriction)
{
	mFrictionHP = abs(angularFriction);
}

/**
 * Returns angular friction for head/pitch rotations (>=0.0).
 */
INLINE float P3Driver::get_angular_friction() const
{
	return mFrictionHP;
}

/**
 * Sets movement stop threshold ([0.0, 1.0]).
 */
INLINE void P3Driver::set_stop_threshold(float threshold)
{
	mStopThreshold = (
			threshold >= 0.0 ?
					threshold - floor(threshold) : ceil(threshold) - threshold);
}

/**
 * Returns movement stop threshold ([0.0, 1.0]).
 */
INLINE float P3Driver::get_stop_threshold() const
{
	return mStopThreshold;
}

/**
 * Sets mouse movement sensitivities along screen x,y axes (>=0.0).
 */
INLINE void P3Driver::set_sens(const ValueList_float& sens)
{
	RETURN_ON_COND(sens.size() < 2,)

	mSensX = abs(sens[0]);
	mSensY = abs(sens[1]);
}

/**
 * Returns mouse movement sensitivities along screen x,y axes (>=0.0).
 */
INLINE ValueList_float P3Driver::get_sens() const
{
	ValueList_float sens;
	sens.add_value(mSensX);
	sens.add_value(mSensY);
	return sens;
}

/**
 * Sets movement fast factor (>=0.0).
 */
INLINE void P3Driver::set_fast_factor(float factor)
{
	mFastFactor = abs(factor);
}

/**
 * Returns movement fast factor (>=0.0).
 */
INLINE float P3Driver::get_fast_factor() const
{
	return mFastFactor;
}

/**
 * Sets current linear "signed" speeds along local x,y,z axes.
 */
INLINE void P3Driver::set_linear_speed(const LVector3f& speed)
{
	mActualSpeedXYZ = speed;
}

/**
 * Returns current linear "signed" speeds along local x,y,z axes.
 */
INLINE const LVector3f& P3Driver::get_linear_speed() const
{
	return mActualSpeedXYZ;
}

/**
 * Sets current angular "signed" speeds for head/pitch rotations.
 */
INLINE void P3Driver::set_angular_speeds(const ValueList_float& speeds)
{
	mActualSpeedH = speeds[0];
	mActualSpeedP = speeds[1];
}

/**
 * Returns current angular "signed" speeds for head/pitch rotations.
 */
INLINE ValueList_float P3Driver::get_angular_speeds() const
{
	ValueList_float angularSpeeds;
	angularSpeeds.add_value(mActualSpeedH);
	angularSpeeds.add_value(mActualSpeedP);
	return angularSpeeds;
}

/**
 * Resets the P3Driver.
 * \note Internal use only.
 */
inline void P3Driver::do_reset()
{
	//
	mEnabled = false;
	mForward = mBackward = mStrafeLeft = mStrafeRight = mUp = mDown =
			mHeadLeft = mHeadRight = mPitchUp = mPitchDown = false;
	mForwardKey = mBackwardKey = mStrafeLeftKey = mStrafeRightKey = mUpKey =
			mDownKey = mHeadLeftKey = mHeadRightKey = mPitchUpKey =
					mPitchDownKey = mMouseMoveKey = false;
	mMouseEnabledH = mMouseEnabledP = mMouseHandled = mHeadLimitEnabled =
			mPitchLimitEnabled = false;
	mHLimit = mPLimit = 0.0;
	mSignOfTranslation = mSignOfMouse = 1;
	mFastFactor = 0.0;
	mActualSpeedXYZ = mMaxSpeedXYZ = mMaxSpeedSquaredXYZ = LVecBase3f::zero();
	mActualSpeedH = mActualSpeedP = mMaxSpeedHP = mMaxSpeedSquaredHP = 0.0;
	mAccelXYZ = LVecBase3f::zero();
	mAccelHP = 0.0;
	mFrictionXYZ = mFrictionHP = 0.0;
	mStopThreshold = 0.0;
	mSensX = mSensY = 0.0;
	mCentX = mCentY = 0.0;
	mReferenceNP.clear();
#ifdef PYTHON_BUILD
	mSelf = nullptr;
#endif //PYTHON_BUILD
	mUpdateCallback = nullptr;
}

INLINE ostream &operator <<(ostream &out, const P3Driver& driver)
{
	driver.output(out);
	return out;
}

#endif /* CONTROL_SOURCE_P3DRIVER_I_ */
