/**
 * \file control_includes.h
 *
 * \date 2016-09-18
 * \author consultit
 */

#ifndef CONTROL_SOURCE_CONTROL_INCLUDES_H_
#define CONTROL_SOURCE_CONTROL_INCLUDES_H_

#include "controlsymbols.h"

#include "../../common/source/commonTools.h"
#include "../../common/source/commonMacros.h"

#include "pairs_valueLists.h"
#include "utilities.h"

#ifdef CPPPARSER
//Panda3d interrogate fake declarations

#endif //CPPPARSER

#endif /* CONTROL_SOURCE_CONTROL_INCLUDES_H_ */
