/**
 * \file p3Driver.h
 *
 * \date 2016-09-18
 * \author consultit
 */

#ifndef CONTROL_SOURCE_P3DRIVER_H_
#define CONTROL_SOURCE_P3DRIVER_H_

#include "control_includes.h"
#include "gameControlManager.h"

#ifndef CPPPARSER
#include "support/common.h"
#endif //CPPPARSER

/**
 * P3Driver is a PandaNode class designed for the control of
 * translation/rotation movements. To be driven, a PandaNode object should be
 * attached to this P3Driver.\n
 *
 * P3Driver can be enabled/disabled as a whole (enabled by default).\n
 * P3Driver can handle a given basic movement (forward, backward, head_left,
 * head_right etc...) only if it is enabled to do so by calling the
 * corresponding "enabler". In turn, an enabled basic movement can be
 * activated/deactivated through the corresponding "activator".\n
 * Movement is, by default, based on acceleration (i.e. "dynamic"):
 * accelerations and max speeds can be independently set for any local direction
 * (translation) and local axis (rotation).\n
 * Translation is updated relative to:
 * - x local axis, ie left-right side direction
 * - y local axis, ie forward-backward direction
 * - z local axis, ie up-down direction
 * Rotation is updated relative to:
 * - z local axis, ie head (yaw)
 * - x local axis, ie pitch
 * Rotation through y local axis (roll) is not considered.\n
 * To obtain a fixed movement/rotation (i.e. "kinematic") in any direction, the
 * related acceleration and friction value should be set to zero and a very high
 * value respectively.\n
 * A task could update the position/orientation of the attached PandaNode object
 * based on the currently enabled basic movements, by calling the "update()"
 * method.\n
 * Usually movements are activated/deactivated through callback associated to
 * events (keyboard, mouse etc...).\n
 * Since in Panda3d by default, "mouse-move" events are not defined, mouse
 * movements can be enabled/disabled as "implicit activators" of head/pitch
 * basic rotations by calling "enable_mouse_head()"/"enable_mouse_pitch()"
 * methods. In this way head/pitch basic rotations can be activated
 * independently through mouse movements and/or normal activator methods.\n
 * On the other hand, "mouse-move" event could be defined by using
 * \code
 * ButtonThrower::set_move_event()
 * \endcode
 * (see http://www.panda3d.org/forums/viewtopic.php?t=9326 and
 * http://www.panda3d.org/forums/viewtopic.php?t=6049), and in this case
 * if an application wishes to handle directly these events, it has to disable
 * the previously described mouse movements handling by calling:
 * enable_mouse_move(true))
 *
 * All movements (but up and down) can be inverted (default: not inverted).\n
 * All movements are computed wrt reference NodePath.
 *
 * > **P3Driver text parameters**:
 * param | type | default | note
 * ------|------|---------|-----
 * | *enabled*  				|single| *true* | -
 * | *forward*  				|single| *enabled* | -
 * | *backward*  				|single| *enabled* | -
 * | *head_limit*  				|single| *false@0.0* | specified as "enabled@[limit] with enabled = true,false, with limit >= 0.0
 * | *head_left*  				|single| *enabled* | -
 * | *head_right*  				|single| *enabled* | -
 * | *pitch_limit*  			|single| *false@0.0* | specified as "enabled@[limit] with enabled = true,false, with limit >= 0.0
 * | *pitch_up*  				|single| *enabled* | -
 * | *pitch_down*  				|single| *enabled* | -
 * | *strafe_left*  			|single| *enabled* | -
 * | *strafe_right*  			|single| *enabled* | -
 * | *up*  						|single| *enabled* | -
 * | *down*  					|single| *enabled* | -
 * | *mouse_move*  				|single| *disabled* | -
 * | *mouse_head*  				|single| *disabled* | -
 * | *mouse_pitch*  			|single| *disabled* | -
 * | *inverted_translation*  	|single| *false* | -
 * | *inverted_rotation*		|single| *false* | -
 * | *max_linear_speed*  		|single| 5.0 | -
 * | *max_angular_speed*  		|single| 5.0 | -
 * | *linear_accel*  			|single| 5.0 | -
 * | *angular_accel*  			|single| 5.0 | -
 * | *linear_friction*  		|single| 0.1 | -
 * | *angular_friction*  		|single| 0.1 | -
 * | *stop_threshold*	  		|single| 0.01 | -
 * | *fast_factor*  			|single| 5.0 | -
 * | *sens_x*  					|single| 0.2 | -
 * | *sens_y*  					|single| 0.2 | -
 *
 * \note parts inside [] are optional.\n
 */
class EXPCL_CONTROL P3Driver: public PandaNode
{
PUBLISHED:

	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	virtual ~P3Driver();
#endif //CPPPARSER

	/**
	 * \name DRIVER
	 */
	///@{
	bool enable();
	bool disable();
	INLINE bool get_enabled() const;
	void update(float dt);
	// Python Properties
	MAKE_PROPERTY(is_enabled, get_enabled);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS: MOVEMENT ENABLERS
	 */
	///@{
	INLINE void set_enable_forward(bool enable);
	INLINE bool get_enable_forward() const;
	INLINE void set_enable_backward(bool enable);
	INLINE bool get_enable_backward() const;
	INLINE void set_enable_strafe_left(bool enable);
	INLINE bool get_enable_strafe_left() const;
	INLINE void set_enable_strafe_right(bool enable);
	INLINE bool get_enable_strafe_right() const;
	INLINE void set_enable_up(bool enable);
	INLINE bool get_enable_up() const;
	INLINE void set_enable_down(bool enable);
	INLINE bool get_enable_down() const;
	INLINE void set_enable_head_left(bool enable);
	INLINE bool get_enable_head_left() const;
	INLINE void set_enable_head_right(bool enable);
	INLINE bool get_enable_head_right() const;
	INLINE void set_enable_pitch_up(bool enable);
	INLINE bool get_enable_pitch_up() const;
	INLINE void set_enable_pitch_down(bool enable);
	INLINE bool get_enable_pitch_down() const;
	INLINE void set_enable_mouse_head(bool enable);
	INLINE bool get_enable_mouse_head() const;
	INLINE void set_enable_mouse_pitch(bool enable);
	INLINE bool get_enable_mouse_pitch() const;
	INLINE void set_enable_mouse_move(bool enable);
	INLINE bool get_enable_mouse_move() const;
	// Python Properties
	MAKE_PROPERTY(enable_forward, get_enable_forward, set_enable_forward);
	MAKE_PROPERTY(enable_backward, get_enable_backward, set_enable_backward);
	MAKE_PROPERTY(enable_strafe_left, get_enable_strafe_left, set_enable_strafe_left);
	MAKE_PROPERTY(enable_strafe_right, get_enable_strafe_right, set_enable_strafe_right);
	MAKE_PROPERTY(enable_up, get_enable_up, set_enable_up);
	MAKE_PROPERTY(enable_down, get_enable_down, set_enable_down);
	MAKE_PROPERTY(enable_head_left, get_enable_head_left, set_enable_head_left);
	MAKE_PROPERTY(enable_head_right, get_enable_head_right, set_enable_head_right);
	MAKE_PROPERTY(enable_pitch_up, get_enable_pitch_up, set_enable_pitch_up);
	MAKE_PROPERTY(enable_pitch_down, get_enable_pitch_down, set_enable_pitch_down);
	MAKE_PROPERTY(enable_mouse_head, get_enable_mouse_head, set_enable_mouse_head);
	MAKE_PROPERTY(enable_mouse_pitch, get_enable_mouse_pitch, set_enable_mouse_pitch);
	MAKE_PROPERTY(enable_mouse_move, get_enable_mouse_move, set_enable_mouse_move);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS: MOVEMENT ACTIVATORS
	 */
	///@{
	INLINE void set_move_forward(bool activate);
	INLINE bool get_move_forward() const;
	INLINE void set_move_backward(bool activate);
	INLINE bool get_move_backward() const;
	INLINE void set_move_strafe_left(bool activate);
	INLINE bool get_move_strafe_left() const;
	INLINE void set_move_strafe_right(bool activate);
	INLINE bool get_move_strafe_right() const;
	INLINE void set_move_up(bool activate);
	INLINE bool get_move_up() const;
	INLINE void set_move_down(bool activate);
	INLINE bool get_move_down() const;
	INLINE void set_rotate_head_left(bool activate);
	INLINE bool get_rotate_head_left() const;
	INLINE void set_rotate_head_right(bool activate);
	INLINE bool get_rotate_head_right() const;
	INLINE void set_rotate_pitch_up(bool activate);
	INLINE bool get_rotate_pitch_up() const;
	INLINE void set_rotate_pitch_down(bool activate);
	INLINE bool get_rotate_pitch_down() const;
	// Python Properties
	MAKE_PROPERTY(move_forward, get_move_forward, set_move_forward);
	MAKE_PROPERTY(move_backward, get_move_backward, set_move_backward);
	MAKE_PROPERTY(move_strafe_left, get_move_strafe_left, set_move_strafe_left);
	MAKE_PROPERTY(move_strafe_right, get_move_strafe_right, set_move_strafe_right);
	MAKE_PROPERTY(move_up, get_move_up, set_move_up);
	MAKE_PROPERTY(move_down, get_move_down, set_move_down);
	MAKE_PROPERTY(rotate_head_left, get_rotate_head_left, set_rotate_head_left);
	MAKE_PROPERTY(rotate_head_right, get_rotate_head_right, set_rotate_head_right);
	MAKE_PROPERTY(rotate_pitch_up, get_rotate_pitch_up, set_rotate_pitch_up);
	MAKE_PROPERTY(rotate_pitch_down, get_rotate_pitch_down, set_rotate_pitch_down);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE void set_inverted_translation(bool activate);
	INLINE bool get_inverted_translation() const;
	INLINE void set_inverted_rotation(bool activate);
	INLINE bool get_inverted_rotation() const;
	INLINE void set_head_limit(bool enabled, float hLimit);
	INLINE Pair_bool_float get_head_limit() const;
	INLINE void set_pitch_limit(bool enabled, float pLimit);
	INLINE Pair_bool_float get_pitch_limit() const;
	INLINE void set_max_linear_speed(const LVector3f& linearSpeed);
	INLINE const LVector3f& get_max_linear_speed() const;
	INLINE void set_max_angular_speed(float angularSpeed);
	INLINE float get_max_angular_speed() const;
	INLINE void set_linear_accel(const LVector3f& linearAccel);
	INLINE const LVector3f& get_linear_accel() const;
	INLINE void set_angular_accel(float angularAccel);
	INLINE float get_angular_accel() const;
	INLINE void set_linear_friction(float linearFriction);
	INLINE float get_linear_friction() const;
	INLINE void set_angular_friction(float angularFriction);
	INLINE float get_angular_friction() const;
	INLINE void set_stop_threshold(float threshold);
	INLINE float get_stop_threshold() const;
	INLINE void set_sens(const ValueList_float& sens);
	INLINE ValueList_float get_sens() const;
	INLINE void set_fast_factor(float factor);
	INLINE float get_fast_factor() const;
	INLINE void set_linear_speed(const LVector3f& speed);
	INLINE const LVector3f& get_linear_speed() const;
	INLINE void set_angular_speeds(const ValueList_float& speeds);
	INLINE ValueList_float get_angular_speeds() const;
	// Python Properties
	MAKE_PROPERTY(inverted_translation, get_inverted_translation, set_inverted_translation);
	MAKE_PROPERTY(inverted_rotation, get_inverted_rotation, set_inverted_rotation);
	MAKE_PROPERTY(max_linear_speed, get_max_linear_speed, set_max_linear_speed);
	MAKE_PROPERTY(max_angular_speed, get_max_angular_speed, set_max_angular_speed);
	MAKE_PROPERTY(linear_accel, get_linear_accel, set_linear_accel);
	MAKE_PROPERTY(angular_accel, get_angular_accel, set_angular_accel);
	MAKE_PROPERTY(linear_friction, get_linear_friction, set_linear_friction);
	MAKE_PROPERTY(angular_friction, get_angular_friction, set_angular_friction);
	MAKE_PROPERTY(stop_threshold, get_stop_threshold, set_stop_threshold);
	MAKE_PROPERTY(sens, get_sens, set_sens);
	MAKE_PROPERTY(fast_factor, get_fast_factor, set_fast_factor);
	MAKE_PROPERTY(linear_speed, get_linear_speed, set_linear_speed);
	MAKE_PROPERTY(angular_speeds, get_angular_speeds, set_angular_speeds);
	///@}

	/**
	 * \name OUTPUT
	 */
	///@{
	void output(ostream &out) const;
	///@}

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	/**
	 * \name PYTHON UPDATE CALLBACK
	 */
	///@{
	void set_update_callback(PyObject *clbk);
	///@}
#else
	/**
	 * \name C++ UPDATE CALLBACK
	 */
	///@{
	typedef void (*UPDATECALLBACKFUNC)(PT(P3Driver));
	void set_update_callback(UPDATECALLBACKFUNC value);
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name TypedWritable API
	 */
	///@{
	void post_process_from_bam();
	///@}

public:
	/**
	 * \name C++ ONLY
	 * Library & support low level related methods.
	 */
	///@{
	///@}

	// Explicitly deleted copy constructor and copy assignment operator.
	P3Driver(const P3Driver&) = delete;
	P3Driver& operator=(const P3Driver&) = delete;

#ifndef CPPPARSER
protected:
	friend void unref_delete<P3Driver>(P3Driver*);
	friend class GameControlManager;

	P3Driver(const string& name);
	virtual ~P3Driver();

private:
	///The reference node path.
	NodePath mReferenceNP;
	///The reference graphic window.
	PT(GraphicsWindow) mWin;
	///Enable/disable flag.
	bool mEnabled;
	///Movement commands' switches: activated/deactivated.
	///@{
	bool mForward, mBackward, mStrafeLeft, mStrafeRight, mUp, mDown, mHeadLeft,
			mHeadRight, mPitchUp, mPitchDown;
	///@}
	///Movement enablers' keys: enabled/disabled.
	///@{
	bool mForwardKey, mBackwardKey, mStrafeLeftKey, mStrafeRightKey, mUpKey,
			mDownKey, mHeadLeftKey, mHeadRightKey, mPitchUpKey, mPitchDownKey,
			mMouseMoveKey;
	///@}
	///Key control values.
	///@{
	bool mMouseEnabledH, mMouseEnabledP, mMouseHandled;
	bool mHeadLimitEnabled, mPitchLimitEnabled;
	float mHLimit, mPLimit;
	char mSignOfTranslation, mSignOfMouse;
	///@}
	/// Sensitivity settings.
	///@{
	float mFastFactor;
	LVector3f mActualSpeedXYZ, mMaxSpeedXYZ, mMaxSpeedSquaredXYZ;
	float mActualSpeedH, mActualSpeedP, mMaxSpeedHP, mMaxSpeedSquaredHP;
	LVector3f mAccelXYZ;
	float mAccelHP;
	float mFrictionXYZ;
	float mFrictionHP;
	float mStopThreshold;
	float mSensX, mSensY;
	int mCentX, mCentY;
	///@}

	inline void do_reset();
	void do_initialize();
	void do_finalize();

	/**
	 * \name Helpers variables/functions.
	 */
	///@{
	void do_enable();
	void do_disable();
	void do_handle_mouse();
	///@}

#if defined(PYTHON_BUILD)
	/**
	 * \name Python callback.
	 */
	///@{
	PyObject *mSelf, *mUpdateCallback;
	///@}
#else
	/**
	 * \name C++ callback.
	 */
	///@{
	UPDATECALLBACKFUNC mUpdateCallback;
	///@}
#endif //PYTHON_BUILD
#endif //CPPPARSER

public:
	/**
	 * \name TypedWritable API
	 */
	///@{
	static void register_with_read_factory();
	virtual void write_datagram (BamWriter *manager, Datagram &dg) override;
	virtual int complete_pointers(TypedWritable **p_list, BamReader *manager) override;
	///@}

#ifndef CPPPARSER
protected:
	static TypedWritable *make_from_bam(const FactoryParams &params);
	virtual void fillin(DatagramIterator &scan, BamReader *manager) override;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(P3Driver,PandaNode)
};

INLINE ostream &operator << (ostream &out, const P3Driver & driver);

///inline
#include "p3Driver.I"

#endif /* CONTROL_SOURCE_P3DRIVER_H_ */
