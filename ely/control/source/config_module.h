#ifndef CONTROL_SOURCE_CONFIG_CONTROL_H_
#define CONTROL_SOURCE_CONFIG_CONTROL_H_

#pragma once

#include "controlsymbols.h"
#include "pandabase.h"
#include "notifyCategoryProxy.h"
#include "configVariableDouble.h"
#include "configVariableString.h"
#include "configVariableInt.h"


NotifyCategoryDecl(p3control, EXPCL_CONTROL, EXPTP_CONTROL);

extern void init_libp3control();

#endif //CONTROL_SOURCE_CONFIG_CONTROL_H_
