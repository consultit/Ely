/**
 * \file gameControlManager.I
 *
 * \date 2016-09-18
 * \author consultit
 */

#ifndef CONTROL_SOURCE_GAMECONTROLMANGER_I_
#define CONTROL_SOURCE_GAMECONTROLMANGER_I_

///GameControlManager inline definitions

/**
 * Returns the reference NodePath.
 */
INLINE NodePath GameControlManager::get_reference_node_path() const
{
	return mReferenceNP;
}

/**
 * Sets the reference NodePath.
 * This is usually called after restoring from a bam file.
 */
INLINE void GameControlManager::set_reference_node_path(const NodePath& reference)
{
	mReferenceNP = reference;
}

/**
 * This overload is available for the manual update of the manager.
 */
INLINE void GameControlManager::update()
{
	update(static_cast<GenericAsyncTask*>(nullptr));
}

/**
 * Returns the number of P3Drivers.
 */
INLINE int GameControlManager::get_num_drivers() const
{
	return (int) mDrivers.size();
}

/**
 * Returns the number of P3Chasers.
 */
INLINE int GameControlManager::get_num_chasers() const
{
	return (int) mChasers.size();
}

/**
 * Returns the singleton pointer.
 */
INLINE GameControlManager* GameControlManager::get_global_ptr()
{
	return Singleton<GameControlManager>::GetSingletonPtr();
}

/**
 * Returns the utilities object.
 */
INLINE const Utilities& GameControlManager::get_utilities()
{
	return mUtils;
}

/**
 *
 */
inline int GameControlManager::unique_ref()
{
	return ++mRef;
}

#endif /* CONTROL_SOURCE_GAMECONTROLMANGER_I_ */
