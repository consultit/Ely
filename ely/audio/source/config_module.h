#ifndef AUDIO_SOURCE_CONFIG_AUDIO_H_
#define AUDIO_SOURCE_CONFIG_AUDIO_H_

#pragma once

#include "audiosymbols.h"
#include "pandabase.h"
#include "notifyCategoryProxy.h"
#include "configVariableDouble.h"
#include "configVariableString.h"
#include "configVariableInt.h"


NotifyCategoryDecl(p3audio, EXPCL_AUDIO, EXPTP_AUDIO);

extern void init_libp3audio();

#endif //AUDIO_SOURCE_CONFIG_AUDIO_H_
