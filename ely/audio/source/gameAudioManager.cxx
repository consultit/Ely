/**
 * \file gameAudioManager.cxx
 *
 * \date 2016-09-30
 * \author consultit
 */

#include "gameAudioManager.h"

#include "p3Sound3d.h"
#include "p3Listener.h"
#include "asyncTaskManager.h"
#include "bamFile.h"

///GameAudioManager definitions
/**
 *
 */
GameAudioManager::GameAudioManager(int taskSort, const NodePath& root,
		const CollideMask& mask):
		mTaskSort{taskSort},
		mReferenceNP{NodePath("ReferenceNode")},
		mUtils{root, mask},
		mRef{0}
{
	PRINT_DEBUG("GameAudioManager::GameAudioManager: creating the singleton manager.");

	mSound3ds.clear();
	mSound3dsParameterTable.clear();
	mListeners.clear();
	mListenersParameterTable.clear();
	set_parameters_defaults(SOUND3D);
	set_parameters_defaults(LISTENER);
	//
	mUpdateData.clear();
	mUpdateTask.clear();
	//
	mAudioMgr = AudioManager::create_AudioManager();
}

/**
 *
 */
GameAudioManager::~GameAudioManager()
{
	PRINT_DEBUG("GameAudioManager::~GameAudioManager: destroying the singleton manager.");

	//stop any default update
	stop_default_update();
	{
		//destroy all P3Sound3ds
		PTA(PT(P3Sound3d))::iterator iterN = mSound3ds.begin();
		while (iterN != mSound3ds.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to P3Sound3d to cleanup itself before being destroyed.
			(*iterN)->do_finalize();
			//remove the P3Sound3ds from the inner list (and from the update task)
			iterN = mSound3ds.erase(iterN);
		}

		//destroy all P3Listeners
		PTA(PT(P3Listener))::iterator iterC = mListeners.begin();
		while (iterC != mListeners.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to P3Listener to cleanup itself before being destroyed.
			(*iterC)->do_finalize();
			//remove the P3Listeners from the inner list (and from the update task)
			iterC = mListeners.erase(iterC);
		}
	}
	//clear parameters' tables
	mSound3dsParameterTable.clear();
	mListenersParameterTable.clear();
}

/**
 * Creates a P3Sound3d with a given (mandatory and not empty) name.
 * Returns a NodePath to the new P3Sound3d,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GameAudioManager::create_sound3d(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(P3Sound3d)newSound3d = new P3Sound3d(name);
	nassertr_always(newSound3d, NodePath::fail())

	// set reference node
	newSound3d->mReferenceNP = mReferenceNP;
	// reparent to reference node and set "this" NodePath
	NodePath np = mReferenceNP.attach_new_node(newSound3d);
	// initialize the new P3Sound3d (could use mReferenceNP, mThisNP)
	newSound3d->do_initialize();

	// add the new P3Sound3d to the inner list (and to the update task)
	mSound3ds.push_back(newSound3d);
	//
	return np;
}

/**
 * Destroys a P3Sound3d.
 * Returns false on error.
 */
bool GameAudioManager::destroy_sound3d(NodePath sound3dNP)
{
	CONTINUE_IF_ELSE_R(
			sound3dNP.node()->is_of_type(P3Sound3d::get_class_type()),
			false)

	PT(P3Sound3d)sound3d = DCAST(P3Sound3d, sound3dNP.node());
	Sound3dList::iterator iter = find(mSound3ds.begin(),
			mSound3ds.end(), sound3d);
	CONTINUE_IF_ELSE_R(iter != mSound3ds.end(), false)

	// give a chance to P3Sound3d to cleanup itself before being destroyed.
	sound3d->do_finalize();
	//remove the P3Sound3d from the inner list (and from the update task)
	mSound3ds.erase(iter);
	//
	return true;
}

/**
 * Gets an P3Sound3d by index, or nullptr on error.
 */
PT(P3Sound3d) GameAudioManager::get_sound3d(int index) const
{
	nassertr_always((index >= 0) && (index < (int ) mSound3ds.size()),
			nullptr)

	return mSound3ds[index];
}

/**
 * Creates a P3Listener with a given (mandatory and not empty) name.
 * Returns a NodePath to the new P3Listener,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GameAudioManager::create_listener(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(P3Listener)newListener = new P3Listener(name);
	nassertr_always(newListener, NodePath::fail())

	// set reference node
	newListener->mReferenceNP = mReferenceNP;
	// reparent to reference node and set "this" NodePath
	NodePath np = mReferenceNP.attach_new_node(newListener);
	// initialize the new P3Listener (could use mReferenceNP, mThisNP)
	newListener->do_initialize();

	// add the new P3Listener to the inner list (and to the update task)
	mListeners.push_back(newListener);
	//
	return np;
}

/**
 * Destroys a P3Listener.
 * Returns false on error.
 */
bool GameAudioManager::destroy_listener(NodePath listenerNP)
{
	CONTINUE_IF_ELSE_R(
			listenerNP.node()->is_of_type(P3Listener::get_class_type()),
			false)

	PT(P3Listener)listener = DCAST(P3Listener, listenerNP.node());
	ListenerList::iterator iter = find(mListeners.begin(),
			mListeners.end(), listener);
	CONTINUE_IF_ELSE_R(iter != mListeners.end(), false)

	// give a chance to P3Listener to cleanup itself before being destroyed.
	listener->do_finalize();
	//remove the P3Listener from the inner list (and from the update task)
	mListeners.erase(iter);
	//
	return true;
}

/**
 * Gets an P3Listener by index, or nullptr on error.
 */
PT(P3Listener) GameAudioManager::get_listener(int index) const
{
	nassertr_always((index >= 0) && (index < (int ) mListeners.size()),
			nullptr)

	return mListeners[index];
}

/**
 * Sets a multi-valued parameter to a multi-value overwriting the existing one(s).
 */
void GameAudioManager::set_parameter_values(AudioType type, const string& paramName,
		const ValueList_string& paramValues)
{
	switch (type)
	{
	case SOUND3D:
		::set_parameter_values(mSound3dsParameterTable, paramName,
				paramValues);
		break;
	case LISTENER:
		::set_parameter_values(mListenersParameterTable, paramName,
				paramValues);
		break;
	default:
		break;
	}
}

/**
 * Gets the multiple values of a (actually set) parameter.
 */
ValueList_string GameAudioManager::get_parameter_values(AudioType type,
		const string& paramName) const
{
	switch (type)
	{
	case SOUND3D:
		return ::get_parameter_values(mSound3dsParameterTable, paramName);
	case LISTENER:
		return ::get_parameter_values(mListenersParameterTable, paramName);
	default:
		break;
	}
	return ValueList_string();
}

/**
 * Sets a multi/single-valued parameter to a single value overwriting the existing one(s).
 */
void GameAudioManager::set_parameter_value(AudioType type, const string& paramName,
		const string& value)
{
	ValueList_string valueList;
	valueList.add_value(value);
	set_parameter_values(type, paramName, valueList);
}

/**
 * Gets a single value (i.e. the first one) of a parameter.
 */
string GameAudioManager::get_parameter_value(AudioType type,
		const string& paramName) const
{
	ValueList_string valueList = get_parameter_values(type, paramName);
	return (valueList.size() != 0 ? valueList[0] : string(""));
}

/**
 * Gets a list of the names of the parameters actually set.
 */
ValueList_string GameAudioManager::get_parameter_name_list(AudioType type) const
{
	switch (type)
	{
	case SOUND3D:
		return ::get_parameter_name_list(mSound3dsParameterTable);
	case LISTENER:
		return ::get_parameter_name_list(mListenersParameterTable);
	default:
		break;
	}
	return ValueList_string();
}

/**
 * Sets all parameters to their default values (if any).
 * \note: After reading objects from bam files, the objects' creation parameters
 * which reside in the manager, are reset to their default values.
 */
void GameAudioManager::set_parameters_defaults(AudioType type)
{
	switch (type)
	{
	case SOUND3D:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"static", "false"},
			{"min_distance", "1.0"},
			{"max_distance", "1000000000.0"},
		};
		::set_parameters_defaults(mSound3dsParameterTable, nameValueList);
	}
		break;
	case LISTENER:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"static", "false"},
		};
		::set_parameters_defaults(mListenersParameterTable, nameValueList);
	}
		break;
	default:
		break;
	}
}

/**
 * Updates audio objects.
 *
 * Will be called automatically in a task.
 */
AsyncTask::DoneStatus GameAudioManager::update(GenericAsyncTask* task)
{
	float dt = ClockObject::get_global_clock()->get_dt();

#ifdef TESTING
	dt = 0.016666667; //60 fps
#endif

	// call all P3Sound3ds' update functions, passing delta time
	for (auto sound3d: mSound3ds)
	{
		sound3d->update(dt);
	}
	// call all P3Listeners' update functions, passing delta time
	for (auto listener: mListeners)
	{
		listener->update(dt);
	}
	//Update audio manager
	mAudioMgr->update();
	//
	return AsyncTask::DS_cont;
}


/**
 * Adds a task to repeatedly call audio updates.
 */
void GameAudioManager::start_default_update()
{
	//create the task for updating AI objects
	mUpdateData = new TaskInterface<GameAudioManager>::TaskData(this,
			&GameAudioManager::update);
	mUpdateTask = new GenericAsyncTask(string("GameAudioManager::update"),
			&TaskInterface<GameAudioManager>::taskFunction,
			reinterpret_cast<void*>(mUpdateData.p()));
	mUpdateTask->set_sort(mTaskSort);
	//Adds mUpdateTask to the active queue.
	AsyncTaskManager::get_global_ptr()->add(mUpdateTask);
}

/**
 * Removes a task to repeatedly call audio updates.
 */
void GameAudioManager::stop_default_update()
{
	if (mUpdateTask)
	{
		AsyncTaskManager::get_global_ptr()->remove(mUpdateTask);
	}
	//
	mUpdateData.clear();
	mUpdateTask.clear();
}

/**
 * Writes to a bam file the entire collections of audio objects and related
 * geometries (i.e. models' NodePaths)
 */
bool GameAudioManager::write_to_bam_file(const string& fileName)
{
	string errorReport;
	// write to bam file
	BamFile outBamFile;
	if (outBamFile.open_write(Filename(fileName)))
	{
		cout << "Current system Bam version: "
				<< outBamFile.get_current_major_ver() << "."
				<< outBamFile.get_current_minor_ver() << endl;
		// just write the reference node
		if (!outBamFile.write_object(mReferenceNP.node()))
		{
			errorReport += string("Error writing ") + mReferenceNP.get_name()
					+ string(" node in ") + fileName + string("\n");
		}
		// close the file
		outBamFile.close();
	}
	else
	{
		errorReport += string("\nERROR: cannot open ") + fileName;
	}
	//check
	if (errorReport.empty())
	{
		cout
				<< "SUCCESS: all audio object collections were written to "
				<< fileName << endl;
	}
	else
	{
		cerr << errorReport << endl;
	}
	return errorReport.empty();
}

/**
 * Reads from a bam file the entire hierarchy of audio objects and related
 * geometries (i.e. models' NodePaths)
 */
bool GameAudioManager::read_from_bam_file(const string& fileName)
{
	string errorReport;
	//read from bamFile
	BamFile inBamFile;
	if (inBamFile.open_read(Filename(fileName)))
	{
		cout << "Current system Bam version: "
				<< inBamFile.get_current_major_ver() << "."
				<< inBamFile.get_current_minor_ver() << endl;
		cout << "Bam file version: " << inBamFile.get_file_major_ver() << "."
				<< inBamFile.get_file_minor_ver() << endl;
		// just read the reference node
		TypedWritable* reference = inBamFile.read_object();
		if (reference)
		{
			//resolve pointers
			if (!inBamFile.resolve())
			{
				errorReport += string("Error resolving pointers in ") + fileName
						+ string("\n");
			}
		}
		else
		{
			errorReport += string("Error reading ") + fileName + string("\n");
		}
		// close the file
		inBamFile.close();
		// post process after restoring
		// restore reference node
		mReferenceNP = NodePath::any_path(DCAST(PandaNode, reference));
		// post processing from bam for all managed objects
		for (auto listener: mListeners)
		{
			listener->post_process_from_bam();
		}
		for (auto sound3d: mSound3ds)
		{
			sound3d->post_process_from_bam();
		}
	}
	else
	{
		errorReport += string("\nERROR: cannot open ") + fileName;
	}
	//check
	if (errorReport.empty())
	{
		cout << "SUCCESS: all audio objects were read from "
				<< fileName << endl;
	}
	else
	{
		cerr << errorReport << endl;
	}
	return errorReport.empty();
}

TYPED_OBJECT_API_DEF(GameAudioManager)
