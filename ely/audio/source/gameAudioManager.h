/**
 * \file gameAudioManager.h
 *
 * \date 2016-09-30
 * \author consultit
 */

#ifndef AUDIO_SOURCE_GAMEAUDIOMANGER_H_
#define AUDIO_SOURCE_GAMEAUDIOMANGER_H_

#include "audioManager.h"
#include "audio_includes.h"

class P3Sound3d;
class P3Listener;

/**
 * GameAudioManager Singleton class.
 *
 * Used for handling P3Sound3ds, P3Listeners.
 */
class EXPCL_AUDIO GameAudioManager: public TypedReferenceCount,
		public Singleton<GameAudioManager>
{
PUBLISHED:

	/**
	 * The type of object for creation parameters.
	 */
	enum AudioType: unsigned char
	{
		SOUND3D = 0,
		LISTENER
	};

	GameAudioManager(int taskSort = 0, const NodePath& root = NodePath(),
			const CollideMask& mask = GeomNode::get_default_collide_mask());
	virtual ~GameAudioManager();

	/**
	 * \name REFERENCE NODES
	 */
	///@{
	INLINE NodePath get_reference_node_path() const;
	INLINE void set_reference_node_path(const NodePath& reference);
	// Python Properties
	MAKE_PROPERTY(reference_node_path, get_reference_node_path, set_reference_node_path);
	///@}

	/**
	 * \name P3Sound3d
	 */
	///@{
	NodePath create_sound3d(const string& name);
	bool destroy_sound3d(NodePath sound3dNP);
	PT(P3Sound3d) get_sound3d(int index) const;
	INLINE int get_num_sound3ds() const;
	MAKE_SEQ(get_sound3ds, get_num_sound3ds, get_sound3d);
	// Python Properties
	MAKE_PROPERTY(num_sound3ds, get_num_sound3ds);
	MAKE_SEQ_PROPERTY(sound3ds, get_num_sound3ds, get_sound3d);
	///@}

	/**
	 * \name P3Listener
	 */
	///@{
	NodePath create_listener(const string& name);
	bool destroy_listener(NodePath listenerNP);
	PT(P3Listener) get_listener(int index) const;
	INLINE int get_num_listeners() const;
	MAKE_SEQ(get_listeners, get_num_listeners, get_listener);
	// Python Properties
	MAKE_PROPERTY(num_listeners, get_num_listeners);
	MAKE_SEQ_PROPERTY(listeners, get_num_listeners, get_listener);
	///@}

	/**
	 * \name TEXTUAL PARAMETERS
	 */
	///@{
	ValueList_string get_parameter_name_list(AudioType type) const;
	void set_parameter_values(AudioType type, const string& paramName, const ValueList_string& paramValues);
	ValueList_string get_parameter_values(AudioType type, const string& paramName) const;
	void set_parameter_value(AudioType type, const string& paramName, const string& value);
	string get_parameter_value(AudioType type, const string& paramName) const;
	void set_parameters_defaults(AudioType type);
	///@}

	/**
	 * \name DEFAULT UPDATE
	 */
	///@{
	INLINE void update();
#ifndef CPPPARSER
	AsyncTask::DoneStatus update(GenericAsyncTask* task);
#endif //CPPPARSER
	void start_default_update();
	void stop_default_update();
	///@}

	/**
	 * \name SINGLETON
	 */
	///@{
	INLINE static GameAudioManager* get_global_ptr();
	///@}

	/**
	 * \name AUDIO MANAGER
	 */
	///@{
	INLINE PT(AudioManager) get_audio_manager() const;
	///@}

	/**
	 * \name UTILITIES
	 */
	///@{
	INLINE const Utilities& get_utilities();
	// Python Properties
	MAKE_PROPERTY(utilities, get_utilities);
	///@}

	/**
	 * \name SERIALIZATION
	 */
	///@{
	bool write_to_bam_file(const string& fileName);
	bool read_from_bam_file(const string& fileName);
	///@}

public:
	///Unique ref producer.
	inline int unique_ref();

#ifndef CPPPARSER
private:
	/// Audio manager.
	PT(AudioManager) mAudioMgr;
	///The update task sort (should be >=0).
	int mTaskSort;

	///The reference node path.
	NodePath mReferenceNP;

	///List of P3Sound3ds handled by this manager.
	typedef pvector<PT(P3Sound3d)> Sound3dList;
	Sound3dList mSound3ds;
	///P3Sound3ds' parameter table.
	ParameterTable mSound3dsParameterTable;

	///List of P3Listeners handled by this manager.
	typedef pvector<PT(P3Listener)> ListenerList;
	ListenerList mListeners;
	///P3Listeners' parameter table.
	ParameterTable mListenersParameterTable;

	///@{
	///A task data for step simulation update.
	PT(TaskInterface<GameAudioManager>::TaskData) mUpdateData;
	PT(AsyncTask) mUpdateTask;
	///@}

	///Unique ref.
	int mRef;

	///Utilities.
	Utilities mUtils;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(GameAudioManager,TypedReferenceCount)
};

///inline
#include "gameAudioManager.I"

#endif /* AUDIO_SOURCE_GAMEAUDIOMANGER_H_ */
