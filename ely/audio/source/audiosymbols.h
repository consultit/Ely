/**
 * \file audiosymbols.h
 *
 * \date 2018-10-03
 * \author consultit
 */
#ifndef AUDIO_SOURCE_AUDIOSYMBOLS_H_
#define AUDIO_SOURCE_AUDIOSYMBOLS_H_

#ifdef PYTHON_BUILD_audio
#	define EXPCL_AUDIO EXPORT_CLASS
#	define EXPTP_AUDIO EXPORT_TEMPL
#else
#	define EXPCL_AUDIO IMPORT_CLASS
#	define EXPTP_AUDIO IMPORT_TEMPL
#endif

#endif /* AUDIO_SOURCE_AUDIOSYMBOLS_H_ */
