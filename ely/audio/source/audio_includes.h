/**
 * \file audio_includes.h
 *
 * \date 2016-09-30
 * \author consultit
 */

#ifndef AUDIO_SOURCE_AUDIO_INCLUDES_H_
#define AUDIO_SOURCE_AUDIO_INCLUDES_H_

#include "audiosymbols.h"

#include "../../common/source/commonTools.h"
#include "../../common/source/commonMacros.h"

#include "pairs_valueLists.h"
#include "utilities.h"

#ifdef CPPPARSER
//Panda3d interrogate fake declarations

#endif //CPPPARSER

#endif /* AUDIO_SOURCE_AUDIO_INCLUDES_H_ */
