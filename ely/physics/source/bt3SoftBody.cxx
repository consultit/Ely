/**
 * \file bt3SoftBody.cxx
 *
 * \date 2017-03-17
 * \author consultit
 */

#if defined(_WIN32)
#include "support_os/pstdint.h"
#endif

#include "bt3SoftBody.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
extern struct Dtool_PyTypedObject Dtool_BT3SoftBody;
extern struct Dtool_PyTypedObject Dtool_CollisionObjectAttrs;
#endif //PYTHON_BUILD

///BT3SoftBody definitions
/**
 *
 */
BT3SoftBody::BT3SoftBody(const string& name) :
		PandaNode(name)
{
	do_reset();
}

/**
 *
 */
BT3SoftBody::~BT3SoftBody()
{
}

/**
 * Initializes the BT3SoftBody with starting settings.
 * \note Internal use only.
 */
void BT3SoftBody::do_initialize()
{
	GamePhysicsManager* mTmpl = GamePhysicsManager::get_global_ptr();
	// body_type
	string bodyType = mTmpl->get_parameter_value(
			GamePhysicsManager::SOFTBODY, string("body_type"));
	mSoftBodyData.type = do_get_soft_body_type(bodyType);
	// group mask
	string groupMask = mTmpl->get_parameter_value(
			GamePhysicsManager::SOFTBODY, string("group_mask"));
	mGroupMask = do_get_mask(groupMask);
	// collide mask
	string collideMask = mTmpl->get_parameter_value(
			GamePhysicsManager::SOFTBODY, string("collide_mask"));
	mCollideMask = do_get_mask(collideMask);
	// total mass (>=0.0)
	float value = STRTOF(mTmpl->get_parameter_value(
					GamePhysicsManager::SOFTBODY, string("total_mass")).c_str(), nullptr);
	mSoftBodyData.totalMass = abs(value);
	// from faces
	mSoftBodyData.fromFaces = (mTmpl->get_parameter_value(
					GamePhysicsManager::SOFTBODY,
					string("from_faces")) == string("true") ? true : false);
	/// body type specific parameters
	if(mSoftBodyData.type == bt3::SoftBodyData::SoftBodyType::PATCH)
	{
		// patch points
		ValueList_LPoint3f pointsOut = {
			LPoint3f(-1.0, -1.0, 0.0), LPoint3f(1.0, -1.0, 0.0),
			LPoint3f(-1.0, 1.0, 0.0), LPoint3f(1.0, 1.0, 0.0)
		};
		do_get_points(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY, string("patch_points")),
				pointsOut);
		mSoftBodyData.patchData.corner[0][0] = pointsOut[0];
		mSoftBodyData.patchData.corner[1][0] = pointsOut[1];
		mSoftBodyData.patchData.corner[0][1] = pointsOut[2];
		mSoftBodyData.patchData.corner[1][1] = pointsOut[3];
		// patch fixeds (>0)
		int valueI = strtol(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("patch_fixeds")).c_str(), nullptr, 0);
		mSoftBodyData.patchData.fixeds = abs(valueI);
		// patch resxy (>0)
		pvector<string> resxyStr =
				parseCompoundString(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("patch_resxy")), ':');
		int resTmp[2] = {1,1};
		for (unsigned int i = 0; (i < resxyStr.size()) && (i < 2); i++)
		{
			resTmp[i] = abs(strtol(resxyStr[i].c_str(), nullptr, 0));
		}
		mSoftBodyData.patchData.resx = resTmp[0];
		mSoftBodyData.patchData.resy = resTmp[1];
		// patch gendiags
		mSoftBodyData.patchData.gendiags = (mTmpl->get_parameter_value(
						GamePhysicsManager::SOFTBODY,
						string("patch_gendiags")) == string("true") ? true : false);
	}
	else if(mSoftBodyData.type == bt3::SoftBodyData::SoftBodyType::ELLIPSOID)
	{
		// ellipsoid center
		pvector<string> valuesStr = parseCompoundString(
				mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("ellipsoid_center")), ',');
		LPoint3f centerTmp(0.0, 0.0, 0.0);
		for (unsigned int i = 0; (i < valuesStr.size()) && (i < 3); i++)
		{
			centerTmp[i] = STRTOF(valuesStr[i].c_str(), nullptr);
		}
		mSoftBodyData.ellipsoidData.center = centerTmp;
		// ellipsoid radius (rx,ry,rz>=0.0)
		valuesStr = parseCompoundString(
				mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("ellipsoid_radius")), ',');
		LVecBase3f radiusTmp(1.0, 1.0, 1.0);
		for (unsigned int i = 0; (i < valuesStr.size()) && (i < 3); i++)
		{
			radiusTmp[i] = abs(STRTOF(valuesStr[i].c_str(), nullptr));
		}
		mSoftBodyData.ellipsoidData.radius = radiusTmp;
		// ellipsoid res (>0)
		int valueI = strtol(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("ellipsoid_res")).c_str(), nullptr, 0);
		mSoftBodyData.ellipsoidData.res = abs(valueI);

	}
	else if((mSoftBodyData.type == bt3::SoftBodyData::SoftBodyType::TRIANGLE_MESH)
			&& (mSoftBodyData.type == bt3::SoftBodyData::SoftBodyType::CONVEX_HULL))
	{
		// triangle mesh rand constraints
		mSoftBodyData.triangleMeshData.randomizeConstraints =
				(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("triangle_mesh_rand_constraints")) ==
								string("true") ? true : false);
	}
	else if(mSoftBodyData.type == bt3::SoftBodyData::SoftBodyType::TETGEN_MESH)
	{
		// tetgen ele face node
		pvector<string> fileNames = parseCompoundString(
						mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
								string("tetgen_ele_face_node")), ':');
		// all files should be specified
		int specified = 0;
		string fileContents[3];
		while ((specified < (int)fileNames.size()) && (specified < 3))
		{
			ifstream inFile(fileNames[specified]);
			if (inFile.fail())
			{
				break;
			}
			fileContents[specified] = move(string((istreambuf_iterator<char>(inFile)),
						istreambuf_iterator<char>()));
			specified++;
		}
		// check for errors
		if (specified != 3)
		{
			fileContents[0] = TetraBunny::getElements();
			fileContents[1] = "";
			fileContents[2] = TetraBunny::getNodes();
		}
		mSoftBodyData.tetgenMeshData.ele = move(fileContents[0]);
		mSoftBodyData.tetgenMeshData.face = move(fileContents[1]);
		mSoftBodyData.tetgenMeshData.node = move(fileContents[2]);
		// tetgen_facelinks
		mSoftBodyData.tetgenMeshData.bfacelinks =
				(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("tetgen_facelinks")) ==
								string("true") ? true : false);
		// tetgen_tetralinks
		mSoftBodyData.tetgenMeshData.btetralinks =
				(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("tetgen_tetralinks")) ==
								string("true") ? true : false);
		// tetgen_facesfromtetras
		mSoftBodyData.tetgenMeshData.bfacesfromtetras =
				(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("tetgen_facesfromtetras")) ==
								string("true") ? true : false);
	}
	else //(mSoftBodyData.type == bt3::SoftBodyData::SoftBodyType::ROPE)
	{
		// rope points
		ValueList_LPoint3f pointsOut =
		{
			LPoint3f::zero(), LPoint3f(1.0, 1.0, 1.0)
		};
		do_get_points(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY, string("rope_points")),
				pointsOut);
		mSoftBodyData.ropeData.from = pointsOut[0];
		mSoftBodyData.ropeData.to = pointsOut[1];
		// rope fixeds (>0)
		int valueI = strtol(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("rope_fixeds")).c_str(), nullptr, 0);
		mSoftBodyData.ropeData.fixeds = abs(valueI);
		// rope res (>0)
		valueI = strtol(mTmpl->get_parameter_value(GamePhysicsManager::SOFTBODY,
						string("rope_res")).c_str(), nullptr, 0);
		mSoftBodyData.ropeData.res = abs(valueI);
	}
	// owner object
	string object = mTmpl->get_parameter_value(
			GamePhysicsManager::SOFTBODY, string("object"));
	if(!object.empty())
	{
		// search owner object under reference node
		NodePath objectNP = mReferenceNP.find(string("**/") + object);
		if (!objectNP.is_empty())
		{
			mObjectNP = objectNP;
			setup();
		}
	}
}

/**
 * Sets the BT3SoftBody up for the given object (if not empty), given the
 * shape's type and size (among others).
 */
int BT3SoftBody::setup()
{
	// continue if soft body has not been setup yet
	CONTINUE_IF_ELSE_R(!mSetup, RESULT_SUCCESS)

	// continue if object not empty
	CONTINUE_IF_ELSE_R(!mObjectNP.is_empty(), RESULT_ERROR)

	// get this node path
	NodePath thisNP = NodePath::any_path(this);
	bt3::Physics& physics =
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics();
	// when not de-serializing: correct the transforms and reparent
	if (!mBuildFromBam)
	{
		// move the transform of the object to this node path
		thisNP.set_transform(mObjectNP.node()->get_transform());
		mObjectNP.set_transform(TransformState::make_identity());
		// reparent the object node path to this
		mObjectNP.reparent_to(thisNP);
	}
	// add a soft body
	bt3::addSoftBodyToNode(thisNP, physics, mSoftBodyData);
	// check for errors
	mSoftBody = btSoftBody::upcast(
					bt3::getPhysicsObjectOfNode(thisNP,
							*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics()));
	RETURN_ON_COND(!mSoftBody, RESULT_ERROR)

	// create collision object's attrs
	mCollisionObjectAttrs = do_create_collision_object_attrs();
	mCollisionObjectAttrs->set_collision_object(mSoftBody);
#ifdef PYTHON_BUILD
	mCollisionObjectAttrsPy = do_create_collision_object_attrs_py();
#endif //PYTHON_BUILD
	// set the flag
	mSetup = true;
	//
	return RESULT_SUCCESS;
}

/**
 * On destruction cleanup.
 * Gives an BT3SoftBody the ability to do any cleaning is necessary when
 * destroyed.
 * \note Internal use only.
 */
void BT3SoftBody::do_finalize()
{
	//cleanup (if needed)
	cleanup();

	//remove this NodePath
	NodePath::any_path(this).remove_node();
	//
#ifdef PYTHON_BUILD
	//Python callback
	PY_SELF_ARG_DECREF(mUpdateCallback, mSelf);
#endif //PYTHON_BUILD
	do_reset();
	//
	return;
}

/**
 * Cleans the BT3SoftBody up from detaching any child objects (if not empty).
 */
int BT3SoftBody::cleanup()
{
	// continue if soft body has been already setup
	CONTINUE_IF_ELSE_R(mSetup, RESULT_SUCCESS)

	int result = RESULT_ERROR;
	// get this node path
	NodePath thisNP = NodePath::any_path(this);
	// remove the soft body
	if (removeSoftBodyFromNode(thisNP,
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics()))
	{
		// move the transform of this node path to the object
		mObjectNP.set_transform(thisNP.node()->get_transform());
		thisNP.set_transform(TransformState::make_identity());
		// detach object
		mObjectNP.detach_node();
#ifdef PYTHON_BUILD
		Py_XDECREF(mCollisionObjectAttrsPy);
		mCollisionObjectAttrsPy = nullptr;
#endif //PYTHON_BUILD
		// destroy collision object's attrs
		delete mCollisionObjectAttrs;
		mCollisionObjectAttrs = nullptr;
		// reset bullet soft body reference
		mSoftBody = nullptr;
		// set the flag
		mSetup = false;
		result = RESULT_SUCCESS;
	}
	//
	return result;
}

/**
 * Sets the ele, face, node files (areFiles == true) or strings (areFiles ==
 * false).
 * \note this will also set the body type to TETGEN_MESH.
 */
void BT3SoftBody::set_tetgen_ele_face_node(const ValueList_string& eleFaceNode,
		bool areFiles)
{
	RETURN_ON_COND(mSetup,)

	RETURN_ON_COND(eleFaceNode.size() < 3,)

	set_body_type(TETGEN_MESH);
	string fileContents[3];
	if (areFiles)
	{
		int specified = 0;
		while (specified < 3)
		{
			ifstream inFile(eleFaceNode[specified]);
			if (inFile.fail())
			{
				break;
			}
			fileContents[specified] = move(
					string((istreambuf_iterator<char>(inFile)),
							istreambuf_iterator<char>()));
			specified++;
		}
		// check for errors
		if (specified != 3)
		{
			fileContents[0] = TetraBunny::getElements();
			fileContents[1] = "";
			fileContents[2] = TetraBunny::getNodes();
		}
	}
	else
	{
		fileContents[0] = move(eleFaceNode[0]);
		fileContents[1] = move(eleFaceNode[1]);
		fileContents[2] = move(eleFaceNode[2]);
	}
	mSoftBodyData.tetgenMeshData.ele = move(fileContents[0]);
	mSoftBodyData.tetgenMeshData.face = move(fileContents[1]);
	mSoftBodyData.tetgenMeshData.node = move(fileContents[2]);
}

/**
 * Gets the closest node index to the given point.
 */
int BT3SoftBody::get_closest_node_index(LPoint3f point) const
{
	RETURN_ON_COND(!mSetup, RESULT_ERROR)

	float maxDistSqr = FLT_MAX;
	int nodeIdx = 0;
	for (int i = 0; i < get_num_nodes(); ++i)
	{
		float distSqr = (bt3::btVector3_to_LVecBase3(mSoftBody->m_nodes[i].m_x)
				- point).length_squared();

		if (distSqr < maxDistSqr)
		{
			maxDistSqr = distSqr;
			nodeIdx = i;
		}
	}
	return nodeIdx;
}

/**
 * Appends a linear joint with the given rigid/soft body with the given linear
 * joint specifications.
 */
void BT3SoftBody::append_linear_joint(const BT3SoftBodyLJointSpecs& specs,
		const NodePath& object)
{
	RETURN_ON_COND(!mSetup,)

	btCollisionObject *colObj = bt3::getPhysicsObjectOfNode(object,
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics());
	RETURN_ON_COND(!colObj,)

	btRigidBody* rb = btRigidBody::upcast(colObj);
	btSoftBody* sb = btSoftBody::upcast(colObj);
	if (rb)
	{
		mSoftBody->appendLinearJoint(specs, rb);
	}
	else if (sb)
	{
		mSoftBody->appendLinearJoint(specs, sb);
	}
	else
	{
		mSoftBody->appendLinearJoint(specs, colObj);
	}
	// store references for serialization
	mJointToPhysicObjectdMap[mSoftBody->m_joints.size() - 1] = object.node();
}

/**
 * Appends an angular joint with the given rigid/soft body with the given
 * angular joint specifications.
 */
void BT3SoftBody::append_angular_joint(const BT3SoftBodyAJointSpecs& specs,
		const NodePath& object)
{
	RETURN_ON_COND(!mSetup,)

	btCollisionObject *colObj = bt3::getPhysicsObjectOfNode(object,
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics());
	RETURN_ON_COND(!colObj,)

	btRigidBody* rb = btRigidBody::upcast(colObj);
	btSoftBody* sb = btSoftBody::upcast(colObj);
	if (rb)
	{
		mSoftBody->appendAngularJoint(specs, rb);
	}
	else if (sb)
	{
		mSoftBody->appendAngularJoint(specs, sb);
	}
	else
	{
		mSoftBody->appendAngularJoint(specs, colObj);
	}
	// store references for serialization
	mJointToPhysicObjectdMap[mSoftBody->m_joints.size() - 1] = object.node();
}

/**
 * Performs a collision test between this soft body and the segment (not
 * infinite ray/direction) "rayTo - rayFrom".
 * Returns the index of the node nearest to the point of impact, otherwise a
 * negative value when there is no impact or in case of error.
 */
int BT3SoftBody::ray_test(const LVector3f& rayFrom, const LVector3f& rayTo)
{
	RETURN_ON_COND(!mSetup, RESULT_ERROR)

	int index = RESULT_ERROR;
	btSoftBody::sRayCast results;
	btVector3 from = bt3::LVecBase3_to_btVector3(rayFrom);
	btVector3 to = bt3::LVecBase3_to_btVector3(rayTo);
	if (mSoftBody->rayTest(from, to, results))
	{
		// impact: get the closest node
		index = get_closest_node_index(
				bt3::btVector3_to_LVecBase3((from - to) * results.fraction));
	}
	return index;
}

/**
 * Returns the soft body shape type given the name.
 * \note Internal use only.
 */
bt3::SoftBodyData::SoftBodyType BT3SoftBody::do_get_soft_body_type(
		const string& name) const
{
	bt3::SoftBodyData::SoftBodyType bodyType;
	if (name == string("rope"))
	{
		bodyType = bt3::SoftBodyData::SoftBodyType::ROPE;
	}
	else if (name == string("patch"))
	{
		bodyType = bt3::SoftBodyData::SoftBodyType::PATCH;
	}
	else if (name == string("ellipsoid"))
	{
		bodyType = bt3::SoftBodyData::SoftBodyType::ELLIPSOID;
	}
	else if (name == string("triangle_mesh"))
	{
		bodyType = bt3::SoftBodyData::SoftBodyType::TRIANGLE_MESH;
	}
	else if (name == string("convex_hull"))
	{
		bodyType = bt3::SoftBodyData::SoftBodyType::CONVEX_HULL;
	}
	else if (name == string("tetgen_mesh"))
	{
		bodyType = bt3::SoftBodyData::SoftBodyType::TETGEN_MESH;
	}
	return bodyType;
}

/**
 * Sets pointsOut with the specified points.
 * \note Internal use only.
 */
void BT3SoftBody::do_get_points(const string& pointSpecsStr, ValueList_LPoint3f& pointsOut)
{
	pvector<string> pointsStr = parseCompoundString(pointSpecsStr, ':');
	for(int i = 0; (i < (int)pointsStr.size()) && (i < pointsOut.size()); i++)
	{
		pvector<string> valuesStr = parseCompoundString(pointsStr[i], ',');
		LPoint3f point(LPoint3f::zero());
		for (int j = 0; (j < (int)valuesStr.size()) && (j < 3); j++)
		{
			point[j] = STRTOF(valuesStr[j].c_str(), nullptr);
		}
		pointsOut.set_value(i, point);
	}
}

/**
 * Returns the mask given the name.
 * \note Internal use only.
 */
BitMask32 BT3SoftBody::do_get_mask(const string& name) const
{
	BitMask32 mask;
	if (name == string("all_on"))
	{
		mask = BitMask32::all_on();
	}
	else if (name == string("all_off"))
	{
		mask = BitMask32::all_off();
	}
	else
	{
		mask.set_word((uint32_t) strtol(name.c_str(), nullptr, 0));
	}
#ifdef ELY_DEBUG
		mask.write(cout, 0);
#endif //ELY_DEBUG
	return mask;
}

/**
 * Creates collision object attrs class.
 * \note Internal use only.
 */
CollisionObjectAttrs* BT3SoftBody::do_create_collision_object_attrs()
{
	CollisionObjectAttrs* attrs = nullptr;
	attrs = new CollisionObjectAttrs();
	return attrs;
}
#if defined(PYTHON_BUILD)
PyObject *BT3SoftBody::do_create_collision_object_attrs_py()
{
	PyObject *attrs = nullptr;
	attrs = DTool_CreatePyInstance((void *)mCollisionObjectAttrs,
			Dtool_CollisionObjectAttrs, false, false);
	return attrs;
}
#endif //PYTHON_BUILD

/**
 * Updates the BT3SoftBody state.
 */
void BT3SoftBody::update(float dt)
{
	RETURN_ON_COND(!mSetup,)

#ifdef TESTING
	dt = 0.016666667; //60 fps
#endif
	//
	if (mUpdateCallback)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_CALL(mUpdateCallback, mSelf,
				get_name() + " BT3SoftBody::update()");
#else
		// call c++ callback
		mUpdateCallback(this);
#endif //PYTHON_BUILD
	}
}

/**
 * Writes a sensible description of the BT3SoftBody to the indicated output
 * stream.
 */
void BT3SoftBody::output(ostream &out) const
{
	out << get_type() << " " << get_name();
}

#ifdef PYTHON_BUILD
/**
 * Sets the update callback as a python function taking this BT3SoftBody as
 * an argument, or None. On error raises an python exception.
 * \note Python only.
 */
void BT3SoftBody::set_update_callback(PyObject *clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mUpdateCallback, clbk, Dtool_BT3SoftBody,
			get_name() + " BT3SoftBody.set_update_callback()", mSelf, this);
}
#else
/**
 * Sets the update callback as a c++ function taking this BT3SoftBody as
 * an argument, or nullptr.
 * \note C++ only.
 */
void BT3SoftBody::set_update_callback(UPDATECALLBACKFUNC value)
{
	mUpdateCallback = value;
}
#endif //PYTHON_BUILD

//TypedWritable API
/**
 * Tells the BamReader how to create objects of type BT3SoftBody.
 */
void BT3SoftBody::register_with_read_factory()
{
	BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

/**
 * Writes the contents of this object to the datagram for shipping out to a
 * Bam file.
 */
void BT3SoftBody::write_datagram(BamWriter *manager, Datagram &dg)
{
	PandaNode::write_datagram(manager, dg);

	///The setup flag.
	dg.add_bool(mSetup);

	///@{
	///Physical parameters.
	dg.add_uint32(mGroupMask.get_word());
	dg.add_uint32(mCollideMask.get_word());
	///mSoftBodyData
	do_write_soft_body_data(dg);
	///@}

	/// Dynamic stuff (parameters + state) and mCollisionObjectAttrs: save only if setup
	if (mSetup)
	{
		// service variables
		unordered_map<unsigned int, void*> idToObjectMap;
		unordered_map<void*, unsigned int> objectToIdMap;
		//Dynamic stuff
		// materials (parameters)
		dg.add_int32(get_num_materials());
		for (int i = 0; i < get_num_materials(); i++)
		{
			BT3SoftBodyMaterial material = get_material(i);
			// save material
			material.write_datagram(dg);
			// save uid of material
			unsigned int uid = do_get_uid(
					(void*) (&static_cast<btSoftBody::Material&>(material)),
					true, idToObjectMap, objectToIdMap);
			dg.add_uint32(uid);
		}
		// nodes (parameters + state)
		dg.add_int32(get_num_nodes());
		for (int i = 0; i < get_num_nodes(); i++)
		{
			BT3SoftBodyNode node = get_node(i);
			// get uid of referenced material
			unsigned int uid = do_get_uid(
					(void*) (static_cast<btSoftBody::Node&>(node).m_material),
					true, idToObjectMap, objectToIdMap);
			node.set_material_uid(uid);
			// save node
			node.write_datagram(dg);
		}
		// links (parameters + state)
		dg.add_int32(mSoftBody->m_links.size());
		for (int i = 0; i < mSoftBody->m_links.size(); i++)
		{
			btSoftBody::Link& link = mSoftBody->m_links[i];
			// get uid of referenced material
			unsigned int materialUid = do_get_uid((void*) (link.m_material),
					true, idToObjectMap, objectToIdMap);
			dg.add_uint32(materialUid);
			for (int i = 0; i < 2; i++)
			{
				dg.add_int32(
						link.m_n[i] ?
								link.m_n[i] - &mSoftBody->m_nodes[0] : -1);
			}
			dg.add_stdfloat(link.m_rl);
			dg.add_bool((bool) link.m_bbending);
		}
		// faces (parameters + state)
		dg.add_int32(mSoftBody->m_faces.size());
		for (int i = 0; i < mSoftBody->m_faces.size(); i++)
		{
			btSoftBody::Face& face = mSoftBody->m_faces[i];
			// get uid of referenced material
			unsigned int materialUid = do_get_uid((void*) (face.m_material),
					true, idToObjectMap, objectToIdMap);
			dg.add_uint32(materialUid);
			for (int i = 0; i < 3; i++)
			{
				dg.add_int32(
						face.m_n[i] ?
								face.m_n[i] - &mSoftBody->m_nodes[0] : -1);
			}
			bt3::btVector3_to_LVecBase3(face.m_normal).write_datagram(dg);
			dg.add_stdfloat(face.m_ra);
		}
		// tetras (parameters + state)
		dg.add_int32(mSoftBody->m_tetras.size());
		for (int i = 0; i < mSoftBody->m_tetras.size(); i++)
		{
			btSoftBody::Tetra& tetra = mSoftBody->m_tetras[i];
			// get uid of referenced material
			unsigned int materialUid = do_get_uid((void*) (tetra.m_material),
					true, idToObjectMap, objectToIdMap);
			dg.add_uint32(materialUid);
			for (int i = 0; i < 4; i++)
			{
				dg.add_int32(
						tetra.m_n[i] ?
								tetra.m_n[i] - &mSoftBody->m_nodes[0] : -1);
			}
			for (int i = 0; i < 4; i++)
			{
				bt3::btVector3_to_LVecBase3(tetra.m_c0[i]).write_datagram(dg);
			}
			dg.add_stdfloat(tetra.m_rv);
			dg.add_stdfloat(tetra.m_c1);
			dg.add_stdfloat(tetra.m_c2);
		}
		// config
		get_config().write_datagram(dg);
		// pose (parameters + state)
		dg.add_bool(mSoftBody->m_pose.m_bvolume);
		dg.add_bool(mSoftBody->m_pose.m_bframe);
		do_write_btMatrix3x3(mSoftBody->m_pose.m_rot, dg);
		do_write_btMatrix3x3(mSoftBody->m_pose.m_scl, dg);
		// rest length scale
		dg.add_stdfloat(get_rest_length_scale());
		// wind velocity
		get_wind_velocity().write_datagram(dg);
		// clusters (parameters + state)
		dg.add_int32(mSoftBody->m_clusters.size());
		// store clusters' nodes first
		for (int i = 0; i < mSoftBody->m_clusters.size(); i++)
		{
			btSoftBody::Cluster& cluster = *mSoftBody->m_clusters[i];
			dg.add_int32(cluster.m_nodes.size());
			for (int i = 0; i < cluster.m_nodes.size(); i++)
			{
				dg.add_int32(
						cluster.m_nodes[i] ?
								cluster.m_nodes[i] - &mSoftBody->m_nodes[0] :
								-1);
			}
		}
		// store clusters' other parameters
		for (int i = 0; i < mSoftBody->m_clusters.size(); i++)
		{
			btSoftBody::Cluster& cluster = *mSoftBody->m_clusters[i];
			dg.add_int32(cluster.m_masses.size());
			for (int i = 0; i < cluster.m_masses.size(); i++)
			{
				dg.add_stdfloat(cluster.m_masses[i]);
			}
			dg.add_int32(cluster.m_framerefs.size());
			for (int i = 0; i < cluster.m_framerefs.size(); i++)
			{
				bt3::btVector3_to_LVecBase3(cluster.m_framerefs[i]).write_datagram(
						dg);
			}
			bt3::btVector3_to_LVecBase3(cluster.m_framexform.getOrigin()).write_datagram(
					dg);
			bt3::btQuat_to_LQuaternion(cluster.m_framexform.getRotation()).write_datagram(
					dg);
			dg.add_stdfloat(cluster.m_idmass);
			dg.add_stdfloat(cluster.m_imass);
			do_write_btMatrix3x3(cluster.m_locii, dg);
			do_write_btMatrix3x3(cluster.m_invwi, dg);
			bt3::btVector3_to_LVecBase3(cluster.m_com).write_datagram(dg);
			for (int i = 0; i < 2; i++)
			{
				bt3::btVector3_to_LVecBase3(cluster.m_vimpulses[i]).write_datagram(
						dg);
			}
			for (int i = 0; i < 2; i++)
			{
				bt3::btVector3_to_LVecBase3(cluster.m_dimpulses[i]).write_datagram(
						dg);
			}
			dg.add_int32(cluster.m_nvimpulses);
			dg.add_int32(cluster.m_ndimpulses);
			bt3::btVector3_to_LVecBase3(cluster.m_lv).write_datagram(dg);
			bt3::btVector3_to_LVecBase3(cluster.m_av).write_datagram(dg);
			dg.add_stdfloat(cluster.m_ndamping);
			dg.add_stdfloat(cluster.m_ldamping);
			dg.add_stdfloat(cluster.m_adamping);
			dg.add_stdfloat(cluster.m_matching);
			dg.add_stdfloat(cluster.m_maxSelfCollisionImpulse);
			dg.add_stdfloat(cluster.m_selfCollisionImpulseFactor);
			dg.add_bool(cluster.m_containsAnchor);
			dg.add_bool(cluster.m_collide);
			dg.add_int32(cluster.m_clusterIndex);
		}
		// collision object attrs
		mCollisionObjectAttrs->write_datagram(dg);

		/// items with external references must be saved last for correct restoring
		// anchors (parameters + state)
		dg.add_int32(mSoftBody->m_anchors.size());
		for (int i = 0; i < mSoftBody->m_anchors.size(); i++)
		{
			btSoftBody::Anchor& anchor = mSoftBody->m_anchors[i];
			dg.add_int32(
					anchor.m_node ?
							anchor.m_node - &mSoftBody->m_nodes[0] : -1);
			do_write_btMatrix3x3(anchor.m_c0, dg);
			bt3::btVector3_to_LVecBase3(anchor.m_c1).write_datagram(dg);
			bt3::btVector3_to_LVecBase3(anchor.m_local).write_datagram(dg);
			dg.add_stdfloat(anchor.m_influence);
			dg.add_stdfloat(anchor.m_c2);
		}
		// joints (parameters + state): have to be the last saved for correct restoring
		dg.add_int32(mSoftBody->m_joints.size());
		for (int i = 0; i < mSoftBody->m_joints.size(); i++)
		{
			btSoftBody::Joint* joint = mSoftBody->m_joints[i];
			dg.add_uint8((uint8_t) joint->Type());
			for (int i = 0; i < 2 ; i++)
			{
				bt3::btVector3_to_LVecBase3(joint->m_refs[i]).write_datagram(dg);
			}
			dg.add_stdfloat(joint->m_cfm);
			dg.add_stdfloat(joint->m_erp);
			dg.add_stdfloat(joint->m_split);
			dg.add_bool(joint->m_delete);
		}
	}

	///Pointers
	///The object this BT3SoftBody is attached to.
	manager->write_pointer(dg, mObjectNP.node());
	///The reference node path.
	manager->write_pointer(dg, mReferenceNP.node());
	//
	if (mSetup)
	{
		// anchors' references
		for (int i = 0; i < mSoftBody->m_anchors.size(); i++)
		{
			// the body this i-th anchor is attached to.
			manager->write_pointer(dg, mAnchorToRigidBodydMap[i]);
		}
		// joints' references
		for (int i = 0; i < mSoftBody->m_joints.size(); i++)
		{
			// the object this i-th joint is attached to.
			manager->write_pointer(dg, mJointToPhysicObjectdMap[i]);
		}
	}
}

/**
 * Receives an array of pointers, one for each time manager->read_pointer()
 * was called in fillin(). Returns the number of pointers processed.
 */
int BT3SoftBody::complete_pointers(TypedWritable **p_list, BamReader *manager)
{
	int pi = PandaNode::complete_pointers(p_list, manager);

	/// Pointers
	///The object this BT3SoftBody is attached to.
	PT(PandaNode)objectNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mObjectNP = NodePath::any_path(objectNPPandaNode);
	///The reference node path.
	PT(PandaNode)referenceNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mReferenceNP = NodePath::any_path(referenceNPPandaNode);
	//
	if (mSetup)
	{
		// anchors' references
		for (unsigned int i = 0; i < mAnchorToRigidBodydMap.size(); i++)
		{
			// the body this i-th anchor is attached to.
			PT(PandaNode)rigidBody = DCAST(PandaNode, p_list[pi++]);
			mAnchorToRigidBodydMap[i] = rigidBody;
		}
		// joints' references
		for (unsigned int i = 0; i < mJointToPhysicObjectdMap.size(); i++)
		{
			// the object this i-th joint is attached to.
			PT(PandaNode)object = DCAST(PandaNode, p_list[pi++]);
			mJointToPhysicObjectdMap[i] = object;
		}
	}

	return pi;
}

/**
 * Should be after the complete restoring from the bam file.
 * \note for a complete restoring it should be called twice.
 */
void BT3SoftBody::post_process_from_bam()
{
	/// setup this BT3SoftBody if needed
	if (mSetup)
	{
		// first step
		if (mBuildFromBam)
		{
			mSetup = !mSetup;
			setup();
			mBuildFromBam = false;
			/// restore Dynamic stuff
			mScan.assign(mOutDatagram);
			// service variables
			unordered_map<unsigned int, void*> idToObjectMap;
			unordered_map<void*, unsigned int> objectToIdMap;
			int materialsNum, nodesNum, linksNum, facesNum, tetrasNum, clusterNums;
			LVecBase3f triVal;
			LQuaternionf quat;
			// temporary maps to be used for reference resolution later
			unordered_map<int, unsigned int> nodeToMaterialIdMap;
			unordered_map<int, unsigned int> linkToMaterialIdMap;
			unordered_map<int, unsigned int> faceToMaterialIdMap;
			unordered_map<int, unsigned int> tetraToMaterialIdMap;
			// materials (parameters)
			materialsNum = mScan.get_int32();
			for (int i = 0; i < materialsNum; i++)
			{
				// default material (i.e. i == 0) was added at construction time
				BT3SoftBodyMaterial material = (
						i == 0 ? get_material(0) : append_material());
				// restore material
				material.read_datagram(mScan);
				// restore uid of material
				int uid = mScan.get_uint32();
				do_set_uid(uid, &static_cast<btSoftBody::Material&>(material),
						idToObjectMap, objectToIdMap);
			}
			// nodes (parameters + state)
			nodesNum = mScan.get_int32();
			ASSERT_TRUE(nodesNum == get_num_nodes())

			for (int i = 0; i < get_num_nodes(); i++)
			{
				BT3SoftBodyNode node = get_node(i);
				// restore node
				node.read_datagram(mScan);
				// store uid of referenced material
				nodeToMaterialIdMap[i] = node.get_material_uid();
			}
			// links (parameters + state)
			linksNum = mScan.get_int32();
			// first remove all the links (created at construction time)
			mSoftBody->m_links.clear();
			for (int i = 0; i < linksNum; i++)
			{
				// store uid of referenced material
				linkToMaterialIdMap[i] = mScan.get_uint32();
				// get nodes' indexes
				int nodeIdxs[2];
				for (int i = 0; i < 2; i++)
				{
					int nodeIdx = mScan.get_int32();
					ASSERT_TRUE(nodeIdx >= 0)
					nodeIdxs[i] = nodeIdx;
				}
				// re-create (append) the link with default material
				mSoftBody->appendLink(nodeIdxs[0], nodeIdxs[1], nullptr);
				int linkIdx = mSoftBody->m_links.size() - 1;
				// restore the link's other stuff
				mSoftBody->m_links[linkIdx].m_rl = mScan.get_stdfloat();
				mSoftBody->m_links[linkIdx].m_bbending = (int) mScan.get_bool();
			}
			// faces (parameters + state)
			facesNum = mScan.get_int32();
			// first remove all the faces (created at construction time)
			mSoftBody->m_faces.clear();
			for (int i = 0; i < facesNum; i++)
			{
				// store uid of referenced material
				faceToMaterialIdMap[i] = mScan.get_uint32();
				// get faces' indexes
				int faceIdxs[3];
				for (int i = 0; i < 3; i++)
				{
					int nodeIdx = mScan.get_int32();
					ASSERT_TRUE(nodeIdx >= 0)
					faceIdxs[i] = nodeIdx;
				}
				// re-create (append) the face with default material
				mSoftBody->appendFace(faceIdxs[0], faceIdxs[1], faceIdxs[2],
						nullptr);
				int faceIdx = mSoftBody->m_faces.size() - 1;
				// restore the face's other stuff
				triVal.read_datagram(mScan);
				mSoftBody->m_faces[faceIdx].m_normal =
						bt3::LVecBase3_to_btVector3(triVal);
				mSoftBody->m_faces[faceIdx].m_ra = mScan.get_stdfloat();
			}
			// tetras (parameters + state)
			tetrasNum = mScan.get_int32();
			// first remove all the tetras (created at construction time)
			mSoftBody->m_tetras.clear();
			for (int i = 0; i < tetrasNum; i++)
			{
				// store uid of referenced material
				tetraToMaterialIdMap[i] = mScan.get_uint32();
				// get tetras' indexes
				int tetraIdxs[4];
				for (int i = 0; i < 4; i++)
				{
					int nodeIdx = mScan.get_int32();
					ASSERT_TRUE(nodeIdx >= 0)
					tetraIdxs[i] = nodeIdx;
				}
				// re-create (append) the tetra with default material
				mSoftBody->appendTetra(tetraIdxs[0], tetraIdxs[1], tetraIdxs[2],
						tetraIdxs[3], nullptr);
				int tetraIdx = mSoftBody->m_tetras.size() - 1;
				// restore the tetra's other stuff
				for (int i = 0; i < 4; i++)
				{
					triVal.read_datagram(mScan);
					mSoftBody->m_tetras[tetraIdx].m_c0[i] =
							bt3::LVecBase3_to_btVector3(triVal);
				}
				mSoftBody->m_tetras[tetraIdx].m_rv = mScan.get_stdfloat();
				mSoftBody->m_tetras[tetraIdx].m_c1 = mScan.get_stdfloat();
				mSoftBody->m_tetras[tetraIdx].m_c2 = mScan.get_stdfloat();
			}
			// config
			get_config().read_datagram(mScan);
			// pose (parameters + state)
			bool bvolume = mScan.get_bool();
			bool bframe = mScan.get_bool();
			set_pose(bvolume, bframe);
			do_read_btMatrix3x3(mSoftBody->m_pose.m_rot, mScan);
			do_read_btMatrix3x3(mSoftBody->m_pose.m_scl, mScan);
			// rest length scale
			set_rest_length_scale(mScan.get_stdfloat());
			// wind velocity
			triVal.read_datagram(mScan);
			set_wind_velocity(triVal);
			// clusters (parameters + state)
			clusterNums = mScan.get_int32();
			// there are no cluster at construction time: re-create all of them
			// and restore clusters' nodes first
			mSoftBody->m_clusters.resize(clusterNums);
			for (int i = 0; i < mSoftBody->m_clusters.size()/*clusterNums*/;
					i++)
			{
				int intVal;
				mSoftBody->m_clusters[i] =
						new (btAlignedAlloc(sizeof(btSoftBody::Cluster), 16)) btSoftBody::Cluster();
				btSoftBody::Cluster& cluster = *mSoftBody->m_clusters[i];
				intVal = mScan.get_int32();
				cluster.m_nodes.resize(intVal);
				for (int i = 0; i < cluster.m_nodes.size()/*intVal*/; i++)
				{
					int nodeIdx = mScan.get_int32();
					ASSERT_TRUE(nodeIdx >= 0)
					cluster.m_nodes[i] = &mSoftBody->m_nodes[nodeIdx];
				}
			}
			// re-initialize clusters
			if (mSoftBody->m_clusters.size())
			{
				mSoftBody->initializeClusters();
				mSoftBody->updateClusters();

				mSoftBody->m_clusterConnectivity.clear();
				//for self-collision
				mSoftBody->m_clusterConnectivity.resize(
						mSoftBody->m_clusters.size()
								* mSoftBody->m_clusters.size());
				{
					for (int c0 = 0; c0 < mSoftBody->m_clusters.size(); c0++)
					{
						mSoftBody->m_clusters[c0]->m_clusterIndex = c0;
						for (int c1 = 0; c1 < mSoftBody->m_clusters.size();
								c1++)
						{

							bool connected = false;
							btSoftBody::Cluster* cla = mSoftBody->m_clusters[c0];
							btSoftBody::Cluster* clb = mSoftBody->m_clusters[c1];
							for (int i = 0;
									!connected && i < cla->m_nodes.size(); i++)
							{
								for (int j = 0; j < clb->m_nodes.size(); j++)
								{
									if (cla->m_nodes[i] == clb->m_nodes[j])
									{
										connected = true;
										break;
									}
								}
							}
							mSoftBody->m_clusterConnectivity[c0
									+ c1 * mSoftBody->m_clusters.size()] =
									connected;
						}
					}
				}
			}
			// restore clusters' other parameters
			for (int i = 0; i < mSoftBody->m_clusters.size()/*clusterNums*/;
					i++)
			{
				int intVal;
				btSoftBody::Cluster& cluster = *mSoftBody->m_clusters[i];
				intVal = mScan.get_int32();
				cluster.m_masses.resize(intVal);
				for (int i = 0; i < cluster.m_masses.size()/*intVal*/; i++)
				{
					cluster.m_masses[i] = mScan.get_stdfloat();
				}
				intVal = mScan.get_int32();
				cluster.m_framerefs.resize(intVal);
				for (int i = 0; i < cluster.m_framerefs.size()/*intVal*/; i++)
				{
					triVal.read_datagram(mScan);
					cluster.m_framerefs[i] = bt3::LVecBase3_to_btVector3(
							triVal);
				}
				cluster.m_framexform.setIdentity();
				triVal.read_datagram(mScan);
				cluster.m_framexform.setOrigin(
						bt3::LVecBase3_to_btVector3(triVal));
				quat.read_datagram(mScan);
				cluster.m_framexform.setRotation(
						bt3::LQuaternion_to_btQuat(quat));
				cluster.m_idmass = mScan.get_stdfloat();
				cluster.m_imass = mScan.get_stdfloat();
				do_read_btMatrix3x3(cluster.m_locii, mScan);
				do_read_btMatrix3x3(cluster.m_invwi, mScan);
				triVal.read_datagram(mScan);
				cluster.m_com = bt3::LVecBase3_to_btVector3(triVal);
				for (int i = 0; i < 2; i++)
				{
					triVal.read_datagram(mScan);
					cluster.m_vimpulses[i] = bt3::LVecBase3_to_btVector3(
							triVal);
				}
				for (int i = 0; i < 2; i++)
				{
					triVal.read_datagram(mScan);
					cluster.m_dimpulses[i] = bt3::LVecBase3_to_btVector3(
							triVal);
				}
				cluster.m_nvimpulses = mScan.get_int32();
				cluster.m_ndimpulses = mScan.get_int32();
				triVal.read_datagram(mScan);
				cluster.m_lv = bt3::LVecBase3_to_btVector3(triVal);
				triVal.read_datagram(mScan);
				cluster.m_av = bt3::LVecBase3_to_btVector3(triVal);
				cluster.m_ndamping = mScan.get_stdfloat();
				cluster.m_ldamping = mScan.get_stdfloat();
				cluster.m_adamping = mScan.get_stdfloat();
				cluster.m_matching = mScan.get_stdfloat();
				cluster.m_maxSelfCollisionImpulse = mScan.get_stdfloat();
				cluster.m_selfCollisionImpulseFactor = mScan.get_stdfloat();
				cluster.m_containsAnchor = mScan.get_bool();
				cluster.m_collide = mScan.get_bool();
				cluster.m_clusterIndex = mScan.get_int32();
			}
			// collision object attrs
			mCollisionObjectAttrs->read_datagram(mScan, nullptr);

			/// LAST STEP: restore all unfinished references
			// nodes (parameters + state)
			for (int i = 0; i < get_num_nodes(); i++)
			{
				btSoftBody::Node& node = get_node(i);
				// restore node's material reference
				btSoftBody::Material* material =
						static_cast<btSoftBody::Material*>(idToObjectMap[nodeToMaterialIdMap[i]]);
				node.m_material = material;
			}
			// links (parameters + state)
			for (int i = 0; i < linksNum; i++)
			{
				// restore link's material reference
				btSoftBody::Material* material =
						static_cast<btSoftBody::Material*>(idToObjectMap[linkToMaterialIdMap[i]]);
				mSoftBody->m_links[i].m_material = material;
			}
			// faces (parameters + state)
			for (int i = 0; i < facesNum; i++)
			{
				// restore face's material reference
				btSoftBody::Material* material =
						static_cast<btSoftBody::Material*>(idToObjectMap[faceToMaterialIdMap[i]]);
				mSoftBody->m_faces[i].m_material = material;
			}
			// tetras (parameters + state)
			for (int i = 0; i < tetrasNum; i++)
			{
				// restore tetra's material reference
				btSoftBody::Material* material =
						static_cast<btSoftBody::Material*>(idToObjectMap[tetraToMaterialIdMap[i]]);
				mSoftBody->m_tetras[i].m_material = material;
			}
		}
		// second step
		else if (mOutDatagram.get_length() != 0)
		{
			/// items with external references must be saved last for correct restoring
			int anchorsNum, jointsNum;
			LVecBase3f triVal;
			// anchors (parameters + state)
			anchorsNum = mScan.get_int32();
			// there are no anchors at construction time: re-add all of them
			for (int i = 0; i < anchorsNum; i++)
			{
				// append a (quite invalid) anchor
				int nodeIdx = mScan.get_int32();
				ASSERT_TRUE(nodeIdx >= 0)
				mSoftBody->appendAnchor(nodeIdx, nullptr,
						btVector3(0.0, 0.0, 0.0), false, 0.0);
				//
				btSoftBody::Anchor& anchor =
						mSoftBody->m_anchors[mSoftBody->m_anchors.size() - 1];
				do_read_btMatrix3x3(anchor.m_c0, mScan);
				triVal.read_datagram(mScan);
				anchor.m_c1 = bt3::LVecBase3_to_btVector3(triVal);
				triVal.read_datagram(mScan);
				anchor.m_local = bt3::LVecBase3_to_btVector3(triVal);
				anchor.m_influence = mScan.get_stdfloat();
				anchor.m_c2 = mScan.get_stdfloat();
			}
			// joints (parameters + state)
			jointsNum = mScan.get_int32();
			// there are no joint at construction time: re-add all of them
			// save a copy of mJointToPhysicObjectdMap and clear it
			unordered_map<int, PT(PandaNode)> mJTPOMCopy = move(mJointToPhysicObjectdMap);
			mJointToPhysicObjectdMap.clear();
			for (int i = 0; i < jointsNum; i++)
			{
				// append a new joint with the given type and object
				btSoftBody::Joint::eType::_ type =
						(btSoftBody::Joint::eType::_) mScan.get_uint8();
				btVector3 refs[2];
				for (int i = 0; i < 2; i++)
				{
					triVal.read_datagram(mScan);
					refs[i] = bt3::LVecBase3_to_btVector3(triVal);
				}
				float cfm = mScan.get_stdfloat();
				float erp = mScan.get_stdfloat();
				float split = mScan.get_stdfloat();
				bool deleteJ = mScan.get_bool();
				NodePath object = NodePath::any_path(mJTPOMCopy[i]);
				// while appending a new linear/angular joint use a default specs,
				// because joint's values are restored after its creation
				switch (type)
				{
				case btSoftBody::Joint::eType::Linear:
				{
					BT3SoftBodyLJointSpecs specs;
					append_linear_joint(specs, object);
				}
					break;
				case btSoftBody::Joint::eType::Angular:
				{
					BT3SoftBodyAJointSpecs specs;
					append_angular_joint(specs, object);
				}
					break;
				case btSoftBody::Joint::eType::Contact:
				default:
					break;
				}
				// restore joint's saved values
				btSoftBody::Joint* joint =
						mSoftBody->m_joints[mSoftBody->m_joints.size() - 1];
				for (int i = 0; i < 2; i++)
				{
					joint->m_refs[i] = refs[i];
				}
				joint->m_cfm = cfm;
				joint->m_erp = erp;
				joint->m_split = split;
				joint->m_delete = deleteJ;
			}

			/// LAST STEP: restore all external unfinished references
			// anchors (parameters + state)
			for (int i = 0; i < anchorsNum; i++)
			{
				// restore rigid body's reference
				PT(BT3RigidBody)rbNode = DCAST(BT3RigidBody, mAnchorToRigidBodydMap[i]);
				mSoftBody->m_anchors[i].m_body = &rbNode->get_rigid_body();
			}
			// restoring completed.
			mOutDatagram.clear();
		}
	}
}

/**
 * This function is called by the BamReader's factory when a new object of
 * type BT3SoftBody is encountered in the Bam file.  It should create the
 * BT3SoftBody and extract its information from the file.
 */
TypedWritable *BT3SoftBody::make_from_bam(const FactoryParams &params)
{
	// continue only if GamePhysicsManager exists
	CONTINUE_IF_ELSE_R(GamePhysicsManager::get_global_ptr(), nullptr)

	// create a BT3SoftBody with default parameters' values: they'll be restored later
	GamePhysicsManager::get_global_ptr()->set_parameters_defaults(
			GamePhysicsManager::SOFTBODY);
	BT3SoftBody *node = DCAST(BT3SoftBody,
			GamePhysicsManager::get_global_ptr()->create_soft_body(
					"BT3SoftBody").node());

	DatagramIterator scan;
	BamReader *manager;

	parse_params(params, scan, manager);
	node->fillin(scan, manager);

	return node;
}

/**
 * This internal function is called by make_from_bam to read in all of the
 * relevant data from the BamFile for the new BT3SoftBody.
 */
void BT3SoftBody::fillin(DatagramIterator &scan, BamReader *manager)
{
	PandaNode::fillin(scan, manager);

	///The setup flag.
	mSetup = scan.get_bool();

	///@{
	///Physical parameters.
	mGroupMask.set_word(scan.get_uint32());
	mCollideMask.set_word(scan.get_uint32());
	///mSoftBodyData
	do_read_soft_body_data(scan);
	///@}

	/// Dynamic stuff: read only if setup and defer restoring to post_process_from_bam()
	int anchorsNum = 0, jointsNum = 0;
	if (mSetup)
	{
		// common service variables
		btMatrix3x3 matrixVal;
		int intVal;
		BT3SoftBodyMaterial material;
		BT3SoftBodyNode node;
		LVecBase3 triVal;
		LQuaternionf quat;
		//Dynamic stuff
		// materials (parameters)
		intVal = scan.get_int32();
		mOutDatagram.add_int32(intVal);
		for (int i = 0; i < intVal; i++)
		{
			// save material
			material.read_datagram(scan);
			material.write_datagram(mOutDatagram);
			// save uid of material
			mOutDatagram.add_uint32(scan.get_uint32());
		}
		// nodes (parameters + state)
		intVal = scan.get_int32();
		mOutDatagram.add_int32(intVal);
		for (int i = 0; i < intVal; i++)
		{
			// save node
			node.read_datagram(scan);
			node.write_datagram(mOutDatagram);
		}
		// links (parameters + state)
		intVal = scan.get_int32();
		mOutDatagram.add_int32(intVal);
		for (int i = 0; i < intVal; i++)
		{
			// get uid of referenced material
			mOutDatagram.add_uint32(scan.get_uint32());
			for (int i = 0; i < 2; i++)
			{
				mOutDatagram.add_int32(scan.get_int32());
			}
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_bool(scan.get_bool());
		}
		// faces (parameters + state)
		intVal = scan.get_int32();
		mOutDatagram.add_int32(intVal);
		for (int i = 0; i < intVal; i++)
		{
			// get uid of referenced material
			mOutDatagram.add_uint32(scan.get_uint32());
			for (int i = 0; i < 3; i++)
			{
				mOutDatagram.add_int32(scan.get_int32());
			}
			triVal.read_datagram(scan);
			triVal.write_datagram(mOutDatagram);
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
		}
		// tetras (parameters + state)
		intVal = scan.get_int32();
		mOutDatagram.add_int32(intVal);
		for (int i = 0; i < intVal; i++)
		{
			// get uid of referenced material
			mOutDatagram.add_uint32(scan.get_uint32());
			for (int i = 0; i < 4; i++)
			{
				mOutDatagram.add_int32(scan.get_int32());
			}
			for (int i = 0; i < 4; i++)
			{
				triVal.read_datagram(scan);
				triVal.write_datagram(mOutDatagram);
			}
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
		}
		// config: use a temporary btSoftBody::Config
		btSoftBody::Config btConfig;
		BT3SoftBodyConfig config(btConfig);
		config.read_datagram(scan);
		config.write_datagram(mOutDatagram);
		// pose (parameters + state)
		mOutDatagram.add_bool(scan.get_bool());
		mOutDatagram.add_bool(scan.get_bool());
		do_read_btMatrix3x3(matrixVal, scan);
		do_write_btMatrix3x3(matrixVal, mOutDatagram);
		do_read_btMatrix3x3(matrixVal, scan);
		do_write_btMatrix3x3(matrixVal, mOutDatagram);
		// rest length scale
		mOutDatagram.add_stdfloat(scan.get_stdfloat());
		// wind velocity
		triVal.read_datagram(scan);
		triVal.write_datagram(mOutDatagram);
		// clusters (parameters + state)
		intVal = scan.get_int32();
		mOutDatagram.add_int32(intVal);
		for (int i = 0; i < intVal; i++)
		{
			int intVal;
			intVal = scan.get_int32();
			mOutDatagram.add_int32(intVal);
			for (int i = 0; i < intVal; i++)
			{
				mOutDatagram.add_int32(scan.get_int32());
			}
		}
		for (int i = 0; i < intVal; i++)
		{
			int intVal;
			intVal = scan.get_int32();
			mOutDatagram.add_int32(intVal);
			for (int i = 0; i < intVal; i++)
			{
				mOutDatagram.add_stdfloat(scan.get_stdfloat());
			}
			intVal = scan.get_int32();
			mOutDatagram.add_int32(intVal);
			for (int i = 0; i < intVal; i++)
			{
				triVal.read_datagram(scan);
				triVal.write_datagram(mOutDatagram);
			}
			triVal.read_datagram(scan);
			triVal.write_datagram(mOutDatagram);
			quat.read_datagram(scan);
			quat.write_datagram(mOutDatagram);
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			do_read_btMatrix3x3(matrixVal, scan);
			do_write_btMatrix3x3(matrixVal, mOutDatagram);
			do_read_btMatrix3x3(matrixVal, scan);
			do_write_btMatrix3x3(matrixVal, mOutDatagram);
			triVal.read_datagram(scan);
			triVal.write_datagram(mOutDatagram);
			for (int i = 0; i < 2; i++)
			{
				triVal.read_datagram(scan);
				triVal.write_datagram(mOutDatagram);
			}
			for (int i = 0; i < 2; i++)
			{
				triVal.read_datagram(scan);
				triVal.write_datagram(mOutDatagram);
			}
			mOutDatagram.add_int32(scan.get_int32());
			mOutDatagram.add_int32(scan.get_int32());
			triVal.read_datagram(scan);
			triVal.write_datagram(mOutDatagram);
			triVal.read_datagram(scan);
			triVal.write_datagram(mOutDatagram);
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_bool(scan.get_bool());
			mOutDatagram.add_bool(scan.get_bool());
			mOutDatagram.add_int32(scan.get_int32());
		}
		// collision object attrs: use a temporary *Attrs of the right type
		CollisionObjectAttrs* attrsTmp = do_create_collision_object_attrs();
		attrsTmp->read_datagram(scan, &mOutDatagram);
		delete attrsTmp;

		/// items with external references must be saved last for correct restoring
		// anchors (parameters + state)
		anchorsNum = scan.get_int32();
		mOutDatagram.add_int32(anchorsNum);
		for (int i = 0; i < anchorsNum; i++)
		{
			mOutDatagram.add_int32(scan.get_int32());
			do_read_btMatrix3x3(matrixVal, scan);
			do_write_btMatrix3x3(matrixVal, mOutDatagram);
			triVal.read_datagram(scan);
			triVal.write_datagram(mOutDatagram);
			triVal.read_datagram(scan);
			triVal.write_datagram(mOutDatagram);
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
		}
		// joints (parameters + state)
		jointsNum = scan.get_int32();
		mOutDatagram.add_int32(jointsNum);
		for (int i = 0; i < jointsNum; i++)
		{
			mOutDatagram.add_uint8(scan.get_uint8());
			for (int i = 0; i < 2 ; i++)
			{
				triVal.read_datagram(scan);
				triVal.write_datagram(mOutDatagram);
			}
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_stdfloat(scan.get_stdfloat());
			mOutDatagram.add_bool(scan.get_bool());
		}
	}

	///Pointers
	///The object this BT3SoftBody is attached to.
	manager->read_pointer(scan);
	///The reference node path.
	manager->read_pointer(scan);
	//
	if (mSetup)
	{
		// anchors' references
		for (int i = 0; i < anchorsNum; i++)
		{
			// the body this i-th anchor is attached to.
			manager->read_pointer(scan);
			mAnchorToRigidBodydMap[i] = nullptr;
		}
		// joints' references
		for (int i = 0; i < jointsNum; i++)
		{
			// the object this i-th joint is attached to.
			manager->read_pointer(scan);
			mJointToPhysicObjectdMap[i] = nullptr;
		}
	}
	// for correct restoring
	mBuildFromBam = true;
}

/**
 * Writes mSoftBodyData into a Datagram.
 * \note Internal use only.
 */
void BT3SoftBody::do_write_soft_body_data(Datagram &dg)
{
	dg.add_uint8((uint8_t) mSoftBodyData.type);
	dg.add_stdfloat(mSoftBodyData.totalMass);
	dg.add_bool(mSoftBodyData.fromFaces);
	switch (mSoftBodyData.type)
	{
	case bt3::SoftBodyData::SoftBodyType::ROPE:
		mSoftBodyData.ropeData.from.write_datagram(dg);
		mSoftBodyData.ropeData.to.write_datagram(dg);
		dg.add_int32(mSoftBodyData.ropeData.res);
		dg.add_int8(mSoftBodyData.ropeData.fixeds);
		break;
	case bt3::SoftBodyData::SoftBodyType::PATCH:
		mSoftBodyData.patchData.corner[0][0].write_datagram(dg);
		mSoftBodyData.patchData.corner[0][1].write_datagram(dg);
		mSoftBodyData.patchData.corner[1][0].write_datagram(dg);
		mSoftBodyData.patchData.corner[1][1].write_datagram(dg);
		dg.add_int32(mSoftBodyData.patchData.resx);
		dg.add_int32(mSoftBodyData.patchData.resy);
		dg.add_int8(mSoftBodyData.patchData.fixeds);
		dg.add_bool(mSoftBodyData.patchData.gendiags);
		break;
	case bt3::SoftBodyData::SoftBodyType::ELLIPSOID:
		mSoftBodyData.ellipsoidData.center.write_datagram(dg);
		mSoftBodyData.ellipsoidData.radius.write_datagram(dg);
		dg.add_int32(mSoftBodyData.ellipsoidData.res);
		break;
	case bt3::SoftBodyData::SoftBodyType::TRIANGLE_MESH:
		dg.add_bool(mSoftBodyData.triangleMeshData.randomizeConstraints);
		break;
	case bt3::SoftBodyData::SoftBodyType::TETGEN_MESH:
		dg.add_string(mSoftBodyData.tetgenMeshData.ele);
		dg.add_string(mSoftBodyData.tetgenMeshData.face);
		dg.add_string(mSoftBodyData.tetgenMeshData.node);
		dg.add_bool(mSoftBodyData.tetgenMeshData.bfacelinks);
		dg.add_bool(mSoftBodyData.tetgenMeshData.btetralinks);
		dg.add_bool(mSoftBodyData.tetgenMeshData.bfacesfromtetras);
		break;
	case bt3::SoftBodyData::SoftBodyType::CONVEX_HULL:
	default:
		break;
	}
}

/**
 * Reads mSoftBodyData from a DatagramIterator.
 * \note Internal use only.
 */
void BT3SoftBody::do_read_soft_body_data(DatagramIterator &scan)
{
	mSoftBodyData.type = (bt3::SoftBodyData::SoftBodyType)scan.get_uint8();
	mSoftBodyData.totalMass = scan.get_stdfloat();
	mSoftBodyData.fromFaces = scan.get_bool();
	switch (mSoftBodyData.type)
	{
	case bt3::SoftBodyData::SoftBodyType::ROPE:
		mSoftBodyData.ropeData.from.read_datagram(scan);
		mSoftBodyData.ropeData.to.read_datagram(scan);
		mSoftBodyData.ropeData.res = scan.get_int32();
		mSoftBodyData.ropeData.fixeds = scan.get_int8();
		break;
	case bt3::SoftBodyData::SoftBodyType::PATCH:
		mSoftBodyData.patchData.corner[0][0].read_datagram(scan);
		mSoftBodyData.patchData.corner[0][1].read_datagram(scan);
		mSoftBodyData.patchData.corner[1][0].read_datagram(scan);
		mSoftBodyData.patchData.corner[1][1].read_datagram(scan);
		mSoftBodyData.patchData.resx = scan.get_int32();
		mSoftBodyData.patchData.resy = scan.get_int32();
		mSoftBodyData.patchData.fixeds = scan.get_int8();
		mSoftBodyData.patchData.gendiags = scan.get_bool();
		break;
	case bt3::SoftBodyData::SoftBodyType::ELLIPSOID:
		mSoftBodyData.ellipsoidData.center.read_datagram(scan);
		mSoftBodyData.ellipsoidData.radius.read_datagram(scan);
		mSoftBodyData.ellipsoidData.res = scan.get_int32();
		break;
	case bt3::SoftBodyData::SoftBodyType::TRIANGLE_MESH:
		mSoftBodyData.triangleMeshData.randomizeConstraints = scan.get_bool();
		break;
	case bt3::SoftBodyData::SoftBodyType::TETGEN_MESH:
		mSoftBodyData.tetgenMeshData.ele = scan.get_string();
		mSoftBodyData.tetgenMeshData.face = scan.get_string();
		mSoftBodyData.tetgenMeshData.node = scan.get_string();
		mSoftBodyData.tetgenMeshData.bfacelinks = scan.get_bool();
		mSoftBodyData.tetgenMeshData.btetralinks = scan.get_bool();
		mSoftBodyData.tetgenMeshData.bfacesfromtetras = scan.get_bool();
		break;
	case bt3::SoftBodyData::SoftBodyType::CONVEX_HULL:
	default:
		break;
	}
}

/**
 * Writes a btMatrix3x3 into a Datagram.
 * \note Internal use only.
 */
void BT3SoftBody::do_write_btMatrix3x3(const btMatrix3x3& matrix, Datagram &dg)
{
	bt3::btVector3_to_LVecBase3(matrix[0]).write_datagram(dg);
	bt3::btVector3_to_LVecBase3(matrix[1]).write_datagram(dg);
	bt3::btVector3_to_LVecBase3(matrix[2]).write_datagram(dg);
}

/**
 * Reads a btMatrix3x3 from a DatagramIterator.
 * \note Internal use only.
 */
void BT3SoftBody::do_read_btMatrix3x3(btMatrix3x3& matrix, DatagramIterator &scan)
{
	bt3::btVector3_to_LVecBase3(matrix[0]).read_datagram(scan);
	bt3::btVector3_to_LVecBase3(matrix[1]).read_datagram(scan);
	bt3::btVector3_to_LVecBase3(matrix[2]).read_datagram(scan);
}

/**
 * Returns an unique id for object.
 * \note Internal use only.
 */
unsigned int BT3SoftBody::do_get_uid(void* object, bool createIfNotFound,
		unordered_map<unsigned int, void*>& idToObjectMap,
		unordered_map<void*, unsigned int>& objectToIdMap)
{
	auto it = objectToIdMap.find(object);
	if (it != objectToIdMap.end())
	{
		return it->second;
	}
	else if (createIfNotFound)
	{
		unsigned int newId = mNextId++;
		do_set_uid(newId, object, idToObjectMap, objectToIdMap);
		return newId;
	}
	else
	{
		return 0;
	}
}

/**
 * Sets an unique id for object.
 * \note Internal use only.
 */
void BT3SoftBody::do_set_uid(unsigned int uid, void* object,
		unordered_map<unsigned int, void*>& idToObjectMap,
		unordered_map<void*, unsigned int>& objectToIdMap)
{
	idToObjectMap[uid] = object;
	objectToIdMap[object] = uid;
}

TYPED_OBJECT_API_DEF(BT3SoftBody)
