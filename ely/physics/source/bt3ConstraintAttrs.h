/**
 * \file bt3ConstraintAttrs.h
 *
 * \date 2017-04-02
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_BT3CONSTRAINTATTRS_H_
#define PHYSICS_SOURCE_BT3CONSTRAINTATTRS_H_

#include "physics_includes.h"
#include "physicsTools.h"
#include "bt3Constraint.h"

#ifndef CPPPARSER
#include "support/physics.h"
#include "support/constraint.h"
#include "bullet3/src/BulletDynamics/ConstraintSolver/btSliderConstraint.h"
#include "bullet3/src/BulletDynamics/ConstraintSolver/btPoint2PointConstraint.h"
#include "bullet3/src/BulletDynamics/ConstraintSolver/btHingeConstraint.h"
#include "bullet3/src/BulletDynamics/ConstraintSolver/btConeTwistConstraint.h"
#include "bullet3/src/BulletDynamics/ConstraintSolver/btGearConstraint.h"
#include "bullet3/src/BulletDynamics/ConstraintSolver/btHinge2Constraint.h"
#include "bullet3/src/BulletDynamics/ConstraintSolver/btGeneric6DofSpringConstraint.h"
#include "bullet3/src/BulletDynamics/ConstraintSolver/btUniversalConstraint.h"
#endif //CPPPARSER

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
#define GETATTRDECL(CLSATTR) \
		CLSATTR();\
	virtual ~CLSATTR();\
	PyObject *__getattr__(PyObject *attr) const;
#else
#define GETATTRDECL(CLSATTR) \
		CLSATTR();\
	virtual ~CLSATTR();
#endif //PYTHON_BUILD

#define SERIALIZATIONDECL \
		virtual void write_datagram(Datagram &dg) const;\
	virtual void read_datagram(DatagramIterator &scan, Datagram* outDatagram);

/**
 * TypedConstraintAttrs class.
 *
 * This is the base class defining methods to tweak the dynamic attributes
 * common to all the constraints types. Each derived class defines the
 * methods to tweak the dynamic attributes specific to the corresponding
 * constraint type.
 *
 * \note All physical quantities are expressed in the same units of measurement
 * used by Bullet.
 *
 */
class EXPCL_PHYSICS TypedConstraintAttrs
{
PUBLISHED:
	TypedConstraintAttrs();
	virtual ~TypedConstraintAttrs();
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	PyObject *check_getattr(PyObject *attr) const;
	PyObject *__dict__;
	string msg;
#endif //PYTHON_BUILD
	INLINE void set_enable(bool value);
	INLINE bool get_enable() const;
	INLINE void set_override_num_solver_iterations(int overideNumIterations);
	INLINE int	get_override_num_solver_iterations() const;
	INLINE void set_breaking_impulse_threshold(float value);
	INLINE float get_breaking_impulse_threshold() const;
	INLINE void set_enable_feedback(bool needsFeedback);
	INLINE bool get_enable_feedback() const;
	INLINE void set_param(int num, float value, int axis = -1);
	INLINE float get_param(int num, int axis = -1) const;
	INLINE float get_applied_impulse() const;
	INLINE void set_debug_draw_size(float value);
	INLINE float get_debug_draw_size() const;
	// Python Properties
	MAKE_PROPERTY(enable, get_enable, set_enable);
	MAKE_PROPERTY(override_num_solver_iterations, get_override_num_solver_iterations, set_override_num_solver_iterations);
	MAKE_PROPERTY(breaking_impulse_threshold, get_breaking_impulse_threshold, set_breaking_impulse_threshold);
	MAKE_PROPERTY(enable_feedback, get_enable_feedback, set_enable_feedback);
	MAKE_PROPERTY(applied_impulse, get_applied_impulse);
	MAKE_PROPERTY(debug_draw_size, get_debug_draw_size, set_debug_draw_size);

#ifndef CPPPARSER
protected:
	btTypedConstraint* mConstraint;
#endif //CPPPARSER

public:
	inline void set_constraint(btTypedConstraint* constraint);
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const TypedConstraintAttrs & attrs);

/**
 * PointToPointAttrs class.
 */
class EXPCL_PHYSICS PointToPointAttrs: public TypedConstraintAttrs
{
PUBLISHED:
	GETATTRDECL(PointToPointAttrs)
	INLINE int get_flags() const;
	// Python Properties
	MAKE_PROPERTY(flags, get_flags);

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const PointToPointAttrs & attrs);

/**
 * HingeAttrs class.
 */
class EXPCL_PHYSICS HingeAttrs: public TypedConstraintAttrs
{
PUBLISHED:
	GETATTRDECL(HingeAttrs)
	INLINE void set_angular_only(bool value);
	INLINE bool get_angular_only() const;
	INLINE void set_enable_angular_motor(bool value);
	INLINE bool get_enable_angular_motor() const;
	INLINE void set_max_motor_impulse(float value);
	INLINE float get_max_motor_impulse() const;
	INLINE void set_motor_target_velocity(float value);
	INLINE void set_motor_target_velocity(float targetAngle, float dt);
	INLINE void set_motor_target_velocity(const LQuaternionf& qAinB, float dt);
	INLINE float get_motor_target_velocity() const;
	INLINE void set_limit(float low, float high, float _softness = 0.9f,
			float _biasFactor = 0.3f, float _relaxationFactor = 1.0f);
	INLINE bool get_has_limit() const;
	INLINE float get_limit_softness() const;
	INLINE float get_limit_bias_factor() const;
	INLINE float get_limit_relaxation_factor() const;
	INLINE float get_lower_limit() const;
	INLINE float get_upper_limit() const;
	INLINE int get_solve_limit() const;
	INLINE float get_limit_sign() const;
	INLINE float get_hinge_angle() const;
	INLINE int get_flags() const;
	// Python Properties
	MAKE_PROPERTY(angular_only, get_angular_only, set_angular_only);
	MAKE_PROPERTY(enable_angular_motor, get_enable_angular_motor, set_enable_angular_motor);
	MAKE_PROPERTY(max_motor_impulse, get_max_motor_impulse, set_max_motor_impulse);
	MAKE_PROPERTY(motor_target_velocity, get_motor_target_velocity, set_motor_target_velocity);
	MAKE_PROPERTY(has_limit, get_has_limit);
	MAKE_PROPERTY(limit_softness, get_limit_softness);
	MAKE_PROPERTY(limit_bias_factor, get_limit_bias_factor);
	MAKE_PROPERTY(limit_relaxation_factor, get_limit_relaxation_factor);
	MAKE_PROPERTY(lower_limit, get_lower_limit);
	MAKE_PROPERTY(upper_limit, get_upper_limit);
	MAKE_PROPERTY(solve_limit, get_solve_limit);
	MAKE_PROPERTY(limit_sign, get_limit_sign);
	MAKE_PROPERTY(hinge_angle, get_hinge_angle);
	MAKE_PROPERTY(flags, get_flags);

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const HingeAttrs & attrs);

/**
 * HingeAccumulatedAttrs class.
 */
class EXPCL_PHYSICS HingeAccumulatedAttrs: public HingeAttrs
{
PUBLISHED:
	GETATTRDECL(HingeAccumulatedAttrs)
	INLINE void set_accumulated_hinge_angle(float value);
	INLINE float get_accumulated_hinge_angle() const;
	// Python Properties
	MAKE_PROPERTY(accumulated_hinge_angle, get_accumulated_hinge_angle, set_accumulated_hinge_angle);

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const HingeAccumulatedAttrs & attrs);

/**
 * ConeTwistAttrs class.
 */
class EXPCL_PHYSICS ConeTwistAttrs: public TypedConstraintAttrs
{
PUBLISHED:
	GETATTRDECL(ConeTwistAttrs)
	INLINE void set_angular_only(bool value);
	INLINE bool get_angular_only() const;
	INLINE void set_limit(float _swingSpan1, float _swingSpan2, float _twistSpan,
			float _softness = 1.f, float _biasFactor = 0.3f,
			float _relaxationFactor = 1.0f);
	INLINE float get_swing_span1() const;
	INLINE float get_swing_span2() const;
	INLINE float get_twist_span() const;
	INLINE float get_limit_softness() const;
	INLINE float get_bias_factor() const;
	INLINE float get_relaxation_factor() const;
	INLINE void set_damping(float damping);
	INLINE float get_damping() const;
	INLINE void set_enable_motor(bool b);
	INLINE bool get_enable_motor() const;
	INLINE void set_max_motor_impulse(float maxMotorImpulse);
	INLINE float get_max_motor_impulse() const;
	INLINE void set_max_motor_impulse_normalized(bool maxMotorImpulseNormalized);
	INLINE bool get_max_motor_impulse_normalized() const;
	INLINE void set_motor_target(const LQuaternionf &q);
	INLINE void set_motor_target_in_constraint_space(const LQuaternionf &q);
	INLINE LQuaternionf get_motor_target() const;
	INLINE void set_fix_thresh(float fixThresh);
	INLINE float get_fix_thresh() const;
	INLINE int get_solve_twist_limit() const;
	INLINE int get_solve_swing_limit() const;
	INLINE float get_twist_limit_sign() const;
	INLINE float get_twist_angle() const;
	INLINE bool get_past_swing_limit() const;
	INLINE int get_flags() const;
	// Python Properties
	MAKE_PROPERTY(angular_only, get_angular_only, set_angular_only);
	MAKE_PROPERTY(swing_span1, get_swing_span1);
	MAKE_PROPERTY(swing_span2, get_swing_span2);
	MAKE_PROPERTY(twist_span, get_twist_span);
	MAKE_PROPERTY(limit_softness, get_limit_softness);
	MAKE_PROPERTY(bias_factor, get_bias_factor);
	MAKE_PROPERTY(relaxation_factor, get_relaxation_factor);
	MAKE_PROPERTY(damping, get_damping, set_damping);
	MAKE_PROPERTY(enable_motor, get_enable_motor, set_enable_motor);
	MAKE_PROPERTY(max_motor_impulse, get_max_motor_impulse, set_max_motor_impulse);
	MAKE_PROPERTY(max_motor_impulse_normalized, get_max_motor_impulse_normalized, set_max_motor_impulse_normalized);
	MAKE_PROPERTY(motor_target, get_motor_target);
	MAKE_PROPERTY(fix_thresh, get_fix_thresh, set_fix_thresh);
	MAKE_PROPERTY(solve_twist_limit, get_solve_twist_limit);
	MAKE_PROPERTY(solve_swing_limit, get_solve_swing_limit);
	MAKE_PROPERTY(twist_limit_sign, get_twist_limit_sign);
	MAKE_PROPERTY(twist_angle, get_twist_angle);
	MAKE_PROPERTY(past_swing_limit, get_past_swing_limit);
	MAKE_PROPERTY(flags, get_flags);

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const ConeTwistAttrs & attrs);

/**
 * GearAttrs class.
 */
class EXPCL_PHYSICS GearAttrs: public TypedConstraintAttrs
{
PUBLISHED:
	GETATTRDECL(GearAttrs)
	INLINE void set_ratio(float ratio);
	INLINE float get_ratio() const;
	// Python Properties
	MAKE_PROPERTY(ratio, get_ratio, set_ratio);

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const GearAttrs & attrs);

/**
 * SliderAttrs class.
 */
class EXPCL_PHYSICS SliderAttrs: public TypedConstraintAttrs
{
PUBLISHED:
	GETATTRDECL(SliderAttrs)
	INLINE void set_lower_lin_limit(float lowerLimit);
	INLINE float get_lower_lin_limit() const;
	INLINE void set_upper_lin_limit(float upperLimit);
	INLINE float get_upper_lin_limit() const;
	INLINE void set_lower_ang_limit(float lowerLimit);
	INLINE float get_lower_ang_limit() const;
	INLINE void set_upper_ang_limit(float upperLimit);
	INLINE float get_upper_ang_limit() const;
	INLINE void set_softness_dir_lin(float softnessDirLin);
	INLINE float get_softness_dir_lin() const;
	INLINE void set_restitution_dir_lin(float restitutionDirLin);
	INLINE float get_restitution_dir_lin() const;
	INLINE void set_softness_lim_ang(float softnessLimAng);
	INLINE float get_softness_lim_ang() const;
	INLINE void set_restitution_lim_ang(float restitutionLimAng);
	INLINE float get_restitution_lim_ang() const;
	INLINE void set_damping_lim_ang(float dampingLimAng);
	INLINE float get_damping_lim_ang() const;
	INLINE void set_softness_ortho_lin(float softnessOrthoLin);
	INLINE float get_softness_ortho_lin() const;
	INLINE void set_restitution_ortho_lin(float restitutionOrthoLin);
	INLINE float get_restitution_ortho_lin() const;
	INLINE void set_damping_ortho_lin(float dampingOrthoLin);
	INLINE float get_damping_ortho_lin() const;
	INLINE void set_softness_ortho_ang(float softnessOrthoAng);
	INLINE float get_softness_ortho_ang() const;
	INLINE void set_restitution_ortho_ang(float restitutionOrthoAng);
	INLINE float get_restitution_ortho_ang() const;
	INLINE void set_damping_ortho_ang(float dampingOrthoAng);
	INLINE float get_damping_ortho_ang() const;
	INLINE void set_damping_dir_lin(float dampingDirLin);
	INLINE float get_damping_dir_lin() const;
	INLINE void set_softness_dir_ang(float softnessDirAng);
	INLINE float get_softness_dir_ang() const;
	INLINE void set_restitution_dir_ang(float restitutionDirAng);
	INLINE float get_restitution_dir_ang() const;
	INLINE void set_damping_dir_ang(float dampingDirAng);
	INLINE float get_damping_dir_ang() const;
	INLINE void set_softness_lim_lin(float softnessLimLin);
	INLINE float get_softness_lim_lin() const;
	INLINE void set_restitution_lim_lin(float restitutionLimLin);
	INLINE float get_restitution_lim_lin() const;
	INLINE void set_damping_lim_lin(float dampingLimLin);
	INLINE float get_damping_lim_lin() const;
	INLINE void set_powered_lin_motor(bool onOff);
	INLINE bool get_powered_lin_motor() const;
	INLINE void set_target_lin_motor_velocity(float targetLinMotorVelocity);
	INLINE float get_target_lin_motor_velocity() const;
	INLINE void set_max_lin_motor_force(float maxLinMotorForce);
	INLINE float get_max_lin_motor_force() const;
	INLINE void set_powered_ang_motor(bool onOff);
	INLINE bool get_powered_ang_motor() const;
	INLINE void set_target_ang_motor_velocity(float targetAngMotorVelocity);
	INLINE float get_target_ang_motor_velocity() const;
	INLINE void set_max_ang_motor_force(float maxAngMotorForce);
	INLINE float get_max_ang_motor_force() const;
	INLINE void set_use_frame_offset(bool frameOffsetOnOff);
	INLINE bool get_use_frame_offset() const;
	INLINE float get_linear_pos() const;
	INLINE float get_angular_pos() const;
	INLINE int get_flags() const;
	// Python Properties
	MAKE_PROPERTY(lower_lin_limit, get_lower_lin_limit, set_lower_lin_limit);
	MAKE_PROPERTY(upper_lin_limit, get_upper_lin_limit, set_upper_lin_limit);
	MAKE_PROPERTY(lower_ang_limit, get_lower_ang_limit, set_lower_ang_limit);
	MAKE_PROPERTY(upper_ang_limit, get_upper_ang_limit, set_upper_ang_limit);
	MAKE_PROPERTY(softness_dir_lin, get_softness_dir_lin, set_softness_dir_lin);
	MAKE_PROPERTY(restitution_dir_lin, get_restitution_dir_lin, set_restitution_dir_lin);
	MAKE_PROPERTY(softness_lim_ang, get_softness_lim_ang, set_softness_lim_ang);
	MAKE_PROPERTY(restitution_lim_ang, get_restitution_lim_ang, set_restitution_lim_ang);
	MAKE_PROPERTY(damping_lim_ang, get_damping_lim_ang, set_damping_lim_ang);
	MAKE_PROPERTY(softness_ortho_lin, get_softness_ortho_lin, set_softness_ortho_lin);
	MAKE_PROPERTY(restitution_ortho_lin, get_restitution_ortho_lin, set_restitution_ortho_lin);
	MAKE_PROPERTY(damping_ortho_lin, get_damping_ortho_lin, set_damping_ortho_lin);
	MAKE_PROPERTY(softness_ortho_ang, get_softness_ortho_ang, set_softness_ortho_ang);
	MAKE_PROPERTY(restitution_ortho_ang, get_restitution_ortho_ang, set_restitution_ortho_ang);
	MAKE_PROPERTY(damping_ortho_ang, get_damping_ortho_ang, set_damping_ortho_ang);
	MAKE_PROPERTY(damping_dir_lin, get_damping_dir_lin, set_damping_dir_lin);
	MAKE_PROPERTY(softness_dir_ang, get_softness_dir_ang, set_softness_dir_ang);
	MAKE_PROPERTY(restitution_dir_ang, get_restitution_dir_ang, set_restitution_dir_ang);
	MAKE_PROPERTY(damping_dir_ang, get_damping_dir_ang, set_damping_dir_ang);
	MAKE_PROPERTY(softness_lim_lin, get_softness_lim_lin, set_softness_lim_lin);
	MAKE_PROPERTY(restitution_lim_lin, get_restitution_lim_lin, set_restitution_lim_lin);
	MAKE_PROPERTY(damping_lim_lin, get_damping_lim_lin, set_damping_lim_lin);
	MAKE_PROPERTY(powered_lin_motor, get_powered_lin_motor, set_powered_lin_motor);
	MAKE_PROPERTY(target_lin_motor_velocity, get_target_lin_motor_velocity, set_target_lin_motor_velocity);
	MAKE_PROPERTY(max_lin_motor_force, get_max_lin_motor_force, set_max_lin_motor_force);
	MAKE_PROPERTY(powered_ang_motor, get_powered_ang_motor, set_powered_ang_motor);
	MAKE_PROPERTY(target_ang_motor_velocity, get_target_ang_motor_velocity, set_target_ang_motor_velocity);
	MAKE_PROPERTY(max_ang_motor_force, get_max_ang_motor_force, set_max_ang_motor_force);
	MAKE_PROPERTY(linear_pos, get_linear_pos);
	MAKE_PROPERTY(angular_pos, get_angular_pos);
	MAKE_PROPERTY(flags, get_flags);

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const SliderAttrs & attrs);

/**
 * Dof6Attrs class.
 */
class EXPCL_PHYSICS Dof6Attrs: public TypedConstraintAttrs
{
PUBLISHED:
	GETATTRDECL(Dof6Attrs)
	INLINE BT3RotationalLimitMotor get_rotational_limit_motor(int index) const;
	INLINE BT3TranslationalLimitMotor get_translational_limit_motor() const;
	INLINE void set_linear_lower_limit(const LVector3f& linearLower);
	INLINE LVector3f get_linear_lower_limit() const;
	INLINE void set_linear_upper_limit(const LVector3f& linearUpper);
	INLINE LVector3f get_linear_upper_limit() const;
	INLINE void set_angular_lower_limit(const LVector3f& angularLower);
	INLINE LVector3f get_angular_lower_limit() const;
	INLINE void set_angular_upper_limit(const LVector3f& angularUpper);
	INLINE LVector3f get_angular_upper_limit() const;
	INLINE void set_limit(int axis, float lo, float hi);
	INLINE bool get_limited(int limitIndex) const;
	INLINE LVector3f get_axis(int axis_index) const;
	INLINE float get_angle(int axis_index) const;
	INLINE float get_relative_pivot_position(int axis_index) const;
	INLINE int get_flags() const;
	// Python Properties
	MAKE_PROPERTY(linear_lower_limit, get_linear_lower_limit, set_linear_lower_limit);
	MAKE_PROPERTY(linear_upper_limit, get_linear_upper_limit, set_linear_upper_limit);
	MAKE_PROPERTY(angular_lower_limit, get_angular_lower_limit, set_angular_lower_limit);
	MAKE_PROPERTY(angular_upper_limit, get_angular_upper_limit, set_angular_upper_limit);
	MAKE_PROPERTY(flags, get_flags);

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const Dof6Attrs & attrs);

/**
 * Dof6SpringAttrs class.
 */
class EXPCL_PHYSICS Dof6SpringAttrs: public Dof6Attrs
{
PUBLISHED:
	GETATTRDECL(Dof6SpringAttrs)
	INLINE void set_enable_spring(int index, bool onOff);
	INLINE bool get_enable_spring(int index) const;
	INLINE void set_stiffness(int index, float stiffness);
	INLINE float get_stiffness(int index) const;
	INLINE void set_damping(int index, float damping);
	INLINE float get_damping(int index) const;
	INLINE void set_equilibrium_point();
	INLINE void set_equilibrium_point(int index);
	INLINE void set_equilibrium_point(int index, float val);
	INLINE float get_equilibrium_point(int index) const;
	// Python Properties

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const Dof6SpringAttrs & attrs);

/**
 * UniversalAttrs class.
 */
class EXPCL_PHYSICS UniversalAttrs: public Dof6Attrs
{
PUBLISHED:
	GETATTRDECL(UniversalAttrs)
	INLINE void set_upper_limit(float ang1max, float ang2max);
	INLINE void set_lower_limit(float ang1min, float ang2min);
	INLINE float get_angle_1() const;
	INLINE float get_angle_2() const;
	INLINE LPoint3f get_anchor_1() const;
	INLINE LPoint3f get_anchor_2() const;
	INLINE LVector3f get_axis_1() const;
	INLINE LVector3f get_axis_2() const;
	// Python Properties
	MAKE_PROPERTY(angle_1, get_angle_1);
	MAKE_PROPERTY(angle_2, get_angle_2);
	MAKE_PROPERTY(anchor_1, get_anchor_1);
	MAKE_PROPERTY(anchor_2, get_anchor_2);
	MAKE_PROPERTY(axis_1, get_axis_1);
	MAKE_PROPERTY(axis_2, get_axis_2);

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const UniversalAttrs & attrs);

/**
 * Dof6Spring2Attrs class.
 */
class EXPCL_PHYSICS Dof6Spring2Attrs: public TypedConstraintAttrs
{
PUBLISHED:
	GETATTRDECL(Dof6Spring2Attrs)
	INLINE BT3RotationalLimitMotor2 get_rotational_limit_motor(int index) const;
	INLINE BT3TranslationalLimitMotor2 get_translational_limit_motor() const;
	INLINE void set_linear_lower_limit(const LVector3f& linearLower);
	INLINE LVector3f get_linear_lower_limit() const;
	INLINE void set_linear_upper_limit(const LVector3f& linearUpper);
	INLINE LVector3f get_linear_upper_limit() const;
	INLINE void set_angular_lower_limit(const LVector3f& angularLower);
	INLINE LVector3f get_angular_lower_limit() const;
	INLINE void set_angular_lower_limit_reversed(const LVector3f& angularLower);
	INLINE LVector3f get_angular_lower_limit_reversed() const;
	INLINE void set_angular_upper_limit(const LVector3f& angularUpper);
	INLINE LVector3f get_angular_upper_limit() const;
	INLINE void set_angular_upper_limit_reversed(const LVector3f& angularUpper);
	INLINE LVector3f get_angular_upper_limit_reversed() const;
	INLINE void set_limit(int axis, float lo, float hi);
	INLINE void set_limit_reversed(int axis, float lo, float hi);
	INLINE bool get_limited(int limitIndex) const;
	INLINE void set_rotation_order(BT3Constraint::BT3RotateOrder order);
	INLINE BT3Constraint::BT3RotateOrder get_rotation_order() const;
	INLINE void set_bounce(int index, float bounce);
	INLINE void set_enable_motor(int index, bool onOff);
	INLINE void set_servo(int index, bool onOff);
	INLINE void set_target_velocity(int index, float velocity);
	INLINE void set_servo_target(int index, float target);
	INLINE void set_max_motor_force(int index, float force);
	INLINE void set_enable_spring(int index, bool onOff);
	INLINE void set_stiffness(int index, float stiffness, bool limitIfNeeded = true);
	INLINE void set_damping(int index, float damping, bool limitIfNeeded = true);
	INLINE void set_equilibrium_point();
	INLINE void set_equilibrium_point(int index);
	INLINE void set_equilibrium_point(int index, float val);
	INLINE LVector3f get_axis(int axis_index) const;
	INLINE float get_angle(int axis_index) const;
	INLINE float get_relative_pivot_position(int axis_index) const;
	// Python Properties
	MAKE_PROPERTY(linear_lower_limit, get_linear_lower_limit, set_linear_lower_limit);
	MAKE_PROPERTY(linear_upper_limit, get_linear_upper_limit, set_linear_upper_limit);
	MAKE_PROPERTY(angular_lower_limit, get_angular_lower_limit, set_angular_lower_limit);
	MAKE_PROPERTY(angular_lower_limit_reversed, get_angular_lower_limit_reversed, set_angular_lower_limit_reversed);
	MAKE_PROPERTY(angular_upper_limit, get_angular_upper_limit, set_angular_upper_limit);
	MAKE_PROPERTY(angular_upper_limit_reversed, get_angular_upper_limit_reversed, set_angular_upper_limit_reversed);
	MAKE_PROPERTY(rotation_order, get_rotation_order, set_rotation_order);

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const Dof6Spring2Attrs & attrs);

/**
 * Hinge2Attrs class.
 */
class EXPCL_PHYSICS Hinge2Attrs: public Dof6Spring2Attrs
{
PUBLISHED:
	GETATTRDECL(Hinge2Attrs)
	INLINE void set_upper_limit(float ang1max);
	INLINE void set_lower_limit(float ang1min);
	INLINE float get_angle_1() const;
	INLINE float get_angle_2() const;
	INLINE LPoint3f get_anchor_1() const;
	INLINE LPoint3f get_anchor_2() const;
	INLINE LVector3f get_axis_1() const;
	INLINE LVector3f get_axis_2() const;
	// Python Properties
	MAKE_PROPERTY(angle_1, get_angle_1);
	MAKE_PROPERTY(angle_2, get_angle_2);
	MAKE_PROPERTY(anchor_1, get_anchor_1);
	MAKE_PROPERTY(anchor_2, get_anchor_2);
	MAKE_PROPERTY(axis_1, get_axis_1);
	MAKE_PROPERTY(axis_2, get_axis_2);

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const Hinge2Attrs & attrs);

/**
 * FixedAttrs class.
 */
class EXPCL_PHYSICS FixedAttrs: public Dof6Spring2Attrs
{
PUBLISHED:
	GETATTRDECL(FixedAttrs)
	// Python Properties

public:
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const FixedAttrs & attrs);

/**
 * ContactAttrs class. //todo (?)
 */

///inline
#include "bt3ConstraintAttrs.I"

#endif /* PHYSICS_SOURCE_BT3CONSTRAINTATTRS_H_ */
