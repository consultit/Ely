/**
 * \file bt3RigidBody.cxx
 *
 * \date 2017-03-07
 * \author consultit
 */

#if defined(_WIN32)
#include "support_os/pstdint.h"
#endif

#include "bt3RigidBody.h"
#include "transformState.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
extern struct Dtool_PyTypedObject Dtool_BT3RigidBody;
extern struct Dtool_PyTypedObject Dtool_CollisionObjectAttrs;
#endif //PYTHON_BUILD

///BT3RigidBody definitions
/**
 *
 */
BT3RigidBody::BT3RigidBody(const string& name) :
		PandaNode(name)
{
	do_reset();
}

/**
 *
 */
BT3RigidBody::~BT3RigidBody()
{
}

/**
 * Initializes the BT3RigidBody with starting settings.
 * \note Internal use only.
 */
void BT3RigidBody::do_initialize()
{
	GamePhysicsManager* mTmpl = GamePhysicsManager::get_global_ptr();
	// shape type
	string shapeType = mTmpl->get_parameter_value(
			GamePhysicsManager::RIGIDBODY, string("shape_type"));
	mShapeType = do_get_shape_type(shapeType);
	// child shape type
	string childShapeType = mTmpl->get_parameter_value(
			GamePhysicsManager::RIGIDBODY, string("child_shape_type"));
	mChildShapeType = do_get_shape_type(childShapeType);
	// use shape of (another object)
	mUseShapeOfId = mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
			string("use_shape_of"));
	// up axis
	string upAxis = mTmpl->get_parameter_value(
			GamePhysicsManager::RIGIDBODY, string("up_axis"));
	if (upAxis == string("x"))
	{
		mUpAxis = GamePhysicsManager::X_up;
	}
	else if (upAxis == string("y"))
	{
		mUpAxis = GamePhysicsManager::Y_up;
	}
	else if (upAxis == string("z"))
	{
		mUpAxis = GamePhysicsManager::Z_up;
	}
	else if (upAxis == string("no_up"))
	{
		mUpAxis = GamePhysicsManager::NO_up;
	}
	// group mask
	string groupMask = mTmpl->get_parameter_value(
			GamePhysicsManager::RIGIDBODY, string("group_mask"));
	mGroupMask = do_get_mask(groupMask);
	// collide mask
	string collideMask = mTmpl->get_parameter_value(
			GamePhysicsManager::RIGIDBODY, string("collide_mask"));
	mCollideMask = do_get_mask(collideMask);
	// body mass (>=0.0)
	float value = STRTOF(mTmpl->get_parameter_value(
					GamePhysicsManager::RIGIDBODY, string("mass")).c_str(), nullptr);
	mMass = abs(value);
	// RigidBodyData parameters
	mRigidBodyData.linearDamping =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("linear_damping")).c_str(),nullptr);
	mRigidBodyData.angularDamping =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("angular_damping")).c_str(),nullptr);
	mRigidBodyData.friction =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("friction")).c_str(),nullptr);
	mRigidBodyData.rollingFriction =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("rolling_friction")).c_str(),nullptr);
	mRigidBodyData.spinningFriction =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("spinning_friction")).c_str(),nullptr);
	mRigidBodyData.restitution =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("restitution")).c_str(),nullptr);
	mRigidBodyData.linearSleepingThreshold =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("linear_sleeping_threshold")).c_str(),nullptr);
	mRigidBodyData.angularSleepingThreshold =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("angular_sleeping_threshold")).c_str(),nullptr);
	mRigidBodyData.additionalDamping = (mTmpl->get_parameter_value(
					GamePhysicsManager::CONSTRAINT,
					string("disable_objects_collisions")) == string("true") ? true : false);
	mRigidBodyData.additionalDampingFactor =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("additional_damping_factor")).c_str(),nullptr);
	mRigidBodyData.additionalLinearDampingThresholdSqr =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("additional_linear_damping_threshold_sqr")).c_str(),nullptr);
	mRigidBodyData.additionalAngularDampingThresholdSqr =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("additional_angular_damping_threshold_sqr")).c_str(),nullptr);
	mRigidBodyData.additionalAngularDampingFactor =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::RIGIDBODY,
					string("additional_angular_damping_factor")).c_str(),nullptr);
	// owner object
	string object = mTmpl->get_parameter_value(
			GamePhysicsManager::RIGIDBODY, string("object"));
	if(!object.empty())
	{
		// search owner object under reference node
		NodePath objectNP = mReferenceNP.find(string("**/") + object);
		if (!objectNP.is_empty())
		{
			mObjectNP = objectNP;
			//shape data fixme
			if (mShapeType == GamePhysicsManager::BT3ShapeType::TERRAIN)
			{
				PRINT_DEBUG("Currently BT3RigidBody with a "
						"GamePhysicsManager::BT3ShapeType::TERRAIN shape,"
						"cannot be built automatically");
				mShapeType = GamePhysicsManager::BT3ShapeType::PLANE;
			}
			setup();
		}
	}
}

/**
 * Sets the BT3RigidBody up for the given object (if not empty), given the
 * shape's type and size (among others).
 */
int BT3RigidBody::setup()
{
	// continue if rigid body has not been setup yet
	CONTINUE_IF_ELSE_R(!mSetup, RESULT_SUCCESS)

	// continue if object not empty
	CONTINUE_IF_ELSE_R(!mObjectNP.is_empty(), RESULT_ERROR)

	// get this node path
	NodePath thisNP = NodePath::any_path(this);
	bt3::Physics& physics =
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics();
	// when not de-serializing: correct the transforms and reparent
	if (!mBuildFromBam)
	{
		// move the transform of the object to this node path
		thisNP.set_transform(mObjectNP.node()->get_transform());
		mObjectNP.set_transform(TransformState::make_identity());
		// reparent the object node path to this
		mObjectNP.reparent_to(thisNP);
	}
	// shared shape
	btCollisionShape* sharedShape = nullptr;
	if (!mUseShapeOfId.empty())
	{
		NodePath sharedShapeNP = mReferenceNP.find(
				string("**/") + mUseShapeOfId);
		if (!sharedShapeNP.is_empty())
		{
			btRigidBody* body = btRigidBody::upcast(
					getPhysicsObjectOfNode(sharedShapeNP, physics));
			if (body != nullptr)
			{
				sharedShape = body->getCollisionShape();
			}
		}
	}
	// add a rigid body
	bt3::addRigidBodyToNode(thisNP, physics, mMass, mRigidBodyData, sharedShape,
			(BroadphaseNativeTypes) mShapeType, (bt3::Panda3dUpAxis) mUpAxis,
			mGroupMask, mCollideMask, mUserData,
			(BroadphaseNativeTypes) mChildShapeType);
	// check for errors
	mRigidBody = btRigidBody::upcast(
					bt3::getPhysicsObjectOfNode(thisNP,
							*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics()));
	RETURN_ON_COND(!mRigidBody, RESULT_ERROR)

	// create collision object's attrs
	mCollisionObjectAttrs = do_create_collision_object_attrs();
	mCollisionObjectAttrs->set_collision_object(mRigidBody);
#ifdef PYTHON_BUILD
	mCollisionObjectAttrsPy = do_create_collision_object_attrs_py();
#endif //PYTHON_BUILD
	// set the flag
	mSetup = true;
	//
	return RESULT_SUCCESS;
}

/**
 * On destruction cleanup.
 * Gives an BT3RigidBody the ability to do any cleaning is necessary when
 * destroyed.
 * \note Internal use only.
 */
void BT3RigidBody::do_finalize()
{
	//cleanup (if needed)
	cleanup();

	//remove this NodePath
	NodePath::any_path(this).remove_node();
	//
#ifdef PYTHON_BUILD
	//Python callback
	PY_SELF_ARG_DECREF(mUpdateCallback, mSelf);
#endif //PYTHON_BUILD
	do_reset();
	//
	return;
}

/**
 * Cleans the BT3RigidBody up, detaching any child objects (if not empty).
 */
int BT3RigidBody::cleanup()
{
	// continue if rigid body has been already setup
	CONTINUE_IF_ELSE_R(mSetup, RESULT_SUCCESS)

	int result = RESULT_ERROR;
	// get this node path
	NodePath thisNP = NodePath::any_path(this);
	// remove the rigid body
	if (bt3::removeRigidBodyFromNode(thisNP,
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics()))
	{
		// move the transform of this node path to the object
		mObjectNP.set_transform(thisNP.node()->get_transform());
		thisNP.set_transform(TransformState::make_identity());
//		// TRIANGLE_MESH preserved its transform fixme
//		if (mShapeType != GamePhysicsManager::TRIANGLE_MESH)
//		{
//			// restore child's transform to the
//			// current rigid body's one
//			childNP.set_transform(get_transform());
//		}
		// detach object
		mObjectNP.detach_node();
		// reset user data pointer
		if (mUserData && (mShapeType == GamePhysicsManager::BT3ShapeType::TERRAIN))
		{
			delete static_cast<PNMImage*>(mUserData);
		}
		mUserData = nullptr;
#ifdef PYTHON_BUILD
		Py_XDECREF(mCollisionObjectAttrsPy);
		mCollisionObjectAttrsPy = nullptr;
#endif //PYTHON_BUILD
		// destroy collision object's attrs
		delete mCollisionObjectAttrs;
		mCollisionObjectAttrs = nullptr;
		// reset bullet rigid body reference
		mRigidBody = nullptr;
		// set the flag
		mSetup = false;
		result = RESULT_SUCCESS;
	}
	//
	return result;
}

/**
 * Switches the current body type of this BT3RigidBody.
 * For the DYNAMIC type the mass must be prior set > 0.0.
 * For the STATIC and KINEMATIC types the mass must be prior set == 0.0.
 */
void BT3RigidBody::switch_body_type(BT3BodyType bodyType)
{
	RETURN_ON_COND(!mSetup,)

	// get this node path
	NodePath thisNP = NodePath::any_path(this);
	// switch type
	bt3::switchRigidBodyTypeOfNode(thisNP,
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics(),
			(bt3::RigidBodyType) bodyType, mMass);
}

/**
 * Returns the current body type of this BT3RigidBody.
 */
BT3RigidBody::BT3BodyType BT3RigidBody::get_body_type() const
{
	BT3BodyType type = INVALID_TYPE;
	RETURN_ON_COND(!mSetup, type)

	type = mMass > 0.0 ?
			DYNAMIC :
			(!(mRigidBody->getCollisionFlags()
					& btCollisionObject::CF_KINEMATIC_OBJECT) ?
					STATIC : KINEMATIC);

	nassertr_always((type == STATIC) ? mRigidBody->isStaticObject():true,
			INVALID_TYPE)
	nassertr_always((type == KINEMATIC) ? mRigidBody->isKinematicObject():true,
			INVALID_TYPE)
	nassertr_always(
			(type == DYNAMIC) ? !mRigidBody->isKinematicObject()
					&& !mRigidBody->isStaticObject():true, INVALID_TYPE)

	return type;
}

/**
 * Returns the shape type given the name.
 * \note Internal use only.
 */
GamePhysicsManager::BT3ShapeType BT3RigidBody::do_get_shape_type(
		const string& name) const
{
	GamePhysicsManager::BT3ShapeType shapeType = GamePhysicsManager::INVALID;
	if (name == string("plane"))
	{
		shapeType = GamePhysicsManager::PLANE;
	}
	else if (name == string("box"))
	{
		shapeType = GamePhysicsManager::BOX;
	}
	else if (name == string("sphere"))
	{
		shapeType = GamePhysicsManager::SPHERE;
	}
	else if (name == string("capsule"))
	{
		shapeType = GamePhysicsManager::CAPSULE;
	}
	else if (name == string("cylinder"))
	{
		shapeType = GamePhysicsManager::CYLINDER;
	}
	else if (name == string("cone"))
	{
		shapeType = GamePhysicsManager::CONE;
	}
	else if (name == string("multi_sphere"))
	{
		shapeType = GamePhysicsManager::MULTI_SPHERE;
	}
	else if (name == string("convex_hull"))
	{
		shapeType = GamePhysicsManager::CONVEX_HULL;
	}
	else if (name == string("gimpact"))
	{
		shapeType = GamePhysicsManager::GIMPACT;
	}
	else if (name == string("triangle_mesh"))
	{
		shapeType = GamePhysicsManager::TRIANGLE_MESH;
	}
	else if (name == string("terrain"))
	{
		shapeType = GamePhysicsManager::TERRAIN;
	}
	else if (name == string("compound"))
	{
		shapeType = GamePhysicsManager::COMPOUND;
	}
	return shapeType;
}

/**
 * Returns the mask given the name.
 * \note Internal use only.
 */
BitMask32 BT3RigidBody::do_get_mask(const string& name) const
{
	BitMask32 mask;
	if (name == string("all_on"))
	{
		mask = BitMask32::all_on();
	}
	else if (name == string("all_off"))
	{
		mask = BitMask32::all_off();
	}
	else
	{
		mask.set_word((uint32_t) strtol(name.c_str(), nullptr, 0));
	}
#ifdef ELY_DEBUG
		mask.write(cout, 0);
#endif //ELY_DEBUG
	return mask;
}

/**
 * Creates collision object attrs class.
 * \note Internal use only.
 */
CollisionObjectAttrs* BT3RigidBody::do_create_collision_object_attrs()
{
	CollisionObjectAttrs* attrs = nullptr;
	attrs = new CollisionObjectAttrs();
	return attrs;
}
#if defined(PYTHON_BUILD)
PyObject *BT3RigidBody::do_create_collision_object_attrs_py()
{
	PyObject *attrs = nullptr;
	attrs = DTool_CreatePyInstance((void *)mCollisionObjectAttrs,
			Dtool_CollisionObjectAttrs, false, false);
	return attrs;
}
#endif //PYTHON_BUILD

/**
 * Updates the BT3RigidBody state.
 */
void BT3RigidBody::update(float dt)
{
	RETURN_ON_COND(!mSetup,)

#ifdef TESTING
	dt = 0.016666667; //60 fps
#endif
	//
	if (mUpdateCallback)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_CALL(mUpdateCallback, mSelf,
				get_name() + " BT3RigidBody::update()");
#else
		// call c++ callback
		mUpdateCallback(this);
#endif //PYTHON_BUILD
	}
}

/**
 * Writes a sensible description of the BT3RigidBody to the indicated output
 * stream.
 */
void BT3RigidBody::output(ostream &out) const
{
	out << get_type() << " " << get_name();
}

#ifdef PYTHON_BUILD
/**
 * Sets the update callback as a python function taking this BT3RigidBody as
 * an argument, or None. On error raises an python exception.
 * \note Python only.
 */
void BT3RigidBody::set_update_callback(PyObject *clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mUpdateCallback, clbk, Dtool_BT3RigidBody,
			get_name() + " BT3RigidBody.set_update_callback()", mSelf, this);
}
#else
/**
 * Sets the update callback as a c++ function taking this BT3RigidBody as
 * an argument, or nullptr.
 * \note C++ only.
 */
void BT3RigidBody::set_update_callback(UPDATECALLBACKFUNC value)
{
	mUpdateCallback = value;
}
#endif //PYTHON_BUILD

//TypedWritable API
/**
 * Tells the BamReader how to create objects of type BT3RigidBody.
 */
void BT3RigidBody::register_with_read_factory()
{
	BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

/**
 * Writes the contents of this object to the datagram for shipping out to a
 * Bam file.
 */
void BT3RigidBody::write_datagram(BamWriter *manager, Datagram &dg)
{
	PandaNode::write_datagram(manager, dg);

	///The setup flag.
	dg.add_bool(mSetup);

	///@{
	///Physical parameters.
	dg.add_stdfloat(mMass);
	dg.add_uint8((uint8_t) mShapeType);
	dg.add_uint8((uint8_t) mChildShapeType);
	dg.add_uint32(mGroupMask.get_word());
	dg.add_uint32(mCollideMask.get_word());
	dg.add_string(mUseShapeOfId);
	dg.add_uint8((uint8_t) mUpAxis);
	if (mSetup && mUserData
			&& (mShapeType == GamePhysicsManager::BT3ShapeType::TERRAIN))
	{
		// serialize heightfield through string
		ostringstream imgOSS;
		if (reinterpret_cast<PNMImage*>(mUserData)->write(imgOSS, "png"))
		{
			dg.add_bool(true);
			dg.add_string32(imgOSS.str());
		}
		else
		{
			PRINT_DEBUG("Error: Write PNMImage");
			dg.add_bool(false);
		}
	}
	///mRigidBodyData
	do_write_rigid_body_data(dg);
	///@}

	/// Dynamic stuff (parameters + state) and mCollisionObjectAttrs: save only if setup
	if (mSetup)
	{
		// linear/angular velocities
		bt3::btVector3_to_LVecBase3(mRigidBody->getLinearVelocity()).write_datagram(
				dg);
		bt3::btVector3_to_LVecBase3(mRigidBody->getAngularVelocity()).write_datagram(
						dg);
		// total force/torque
		bt3::btVector3_to_LVecBase3(mRigidBody->getTotalForce()).write_datagram(
				dg);
		bt3::btVector3_to_LVecBase3(mRigidBody->getTotalTorque()).write_datagram(
				dg);
		mCollisionObjectAttrs->write_datagram(dg);
	}

	///The object this BT3RigidBody is attached to.
	manager->write_pointer(dg, mObjectNP.node());
	///The reference node path.
	manager->write_pointer(dg, mReferenceNP.node());
}

/**
 * Receives an array of pointers, one for each time manager->read_pointer()
 * was called in fillin(). Returns the number of pointers processed.
 */
int BT3RigidBody::complete_pointers(TypedWritable **p_list, BamReader *manager)
{
	int pi = PandaNode::complete_pointers(p_list, manager);

	/// Pointers
	///The object this BT3RigidBody is attached to.
	PT(PandaNode)objectNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mObjectNP = NodePath::any_path(objectNPPandaNode);
	///The reference node path.
	PT(PandaNode)referenceNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mReferenceNP = NodePath::any_path(referenceNPPandaNode);

	return pi;
}

/**
 * Should be after the complete restoring from the bam file.
 * \note for a complete restoring it should be called twice.
 */
void BT3RigidBody::post_process_from_bam()
{
	/// setup this BT3RigidBody if needed
	if (mSetup)
	{
		// first step
		if (mBuildFromBam)
		{
			mSetup = !mSetup;
			setup();
			mBuildFromBam = false;
			/// restore Dynamic stuff
			// service variables
			DatagramIterator scan(mOutDatagram);
			LVecBase3f triVal;
			// linear/angular velocities
			triVal.read_datagram(scan);
			mRigidBody->setLinearVelocity(bt3::LVecBase3_to_btVector3(triVal));
			triVal.read_datagram(scan);
			mRigidBody->setAngularVelocity(bt3::LVecBase3_to_btVector3(triVal));
			// total force/torque
			triVal.read_datagram(scan);
			mRigidBody->applyCentralForce(bt3::LVecBase3_to_btVector3(triVal));
			triVal.read_datagram(scan);
			mRigidBody->applyTorque(bt3::LVecBase3_to_btVector3(triVal));
			// restore collision attrs
			mCollisionObjectAttrs->read_datagram(scan, nullptr);
			mOutDatagram.clear();
		}
		// second step
	}
}

/**
 * This function is called by the BamReader's factory when a new object of
 * type BT3RigidBody is encountered in the Bam file.  It should create the
 * BT3RigidBody and extract its information from the file.
 */
TypedWritable *BT3RigidBody::make_from_bam(const FactoryParams &params)
{
	// continue only if GamePhysicsManager exists
	CONTINUE_IF_ELSE_R(GamePhysicsManager::get_global_ptr(), nullptr)

	// create a BT3RigidBody with default parameters' values: they'll be restored later
	GamePhysicsManager::get_global_ptr()->set_parameters_defaults(
			GamePhysicsManager::RIGIDBODY);
	BT3RigidBody *node = DCAST(BT3RigidBody,
			GamePhysicsManager::get_global_ptr()->create_rigid_body(
					"BT3RigidBody").node());

	DatagramIterator scan;
	BamReader *manager;

	parse_params(params, scan, manager);
	node->fillin(scan, manager);

	return node;
}

/**
 * This internal function is called by make_from_bam to read in all of the
 * relevant data from the BamFile for the new BT3RigidBody.
 */
void BT3RigidBody::fillin(DatagramIterator &scan, BamReader *manager)
{
	PandaNode::fillin(scan, manager);

	///The setup flag.
	mSetup = scan.get_bool();

	///@{
	///Physical parameters.
	mMass = scan.get_stdfloat();
	mShapeType = (GamePhysicsManager::BT3ShapeType) scan.get_uint8();
	mChildShapeType = (GamePhysicsManager::BT3ShapeType) scan.get_uint8();
	mGroupMask.set_word(scan.get_uint32());
	mCollideMask.set_word(scan.get_uint32());
	mUseShapeOfId = scan.get_string();
	mUpAxis = (GamePhysicsManager::BT3UpAxis) scan.get_uint8();
	if (mSetup && (mShapeType == GamePhysicsManager::BT3ShapeType::TERRAIN))
	{
		// de-serialize through string
		if (scan.get_bool())
		{
			istringstream imgISS(scan.get_string32());
			mUserData = new PNMImage();
			if (!reinterpret_cast<PNMImage*>(mUserData)->read(imgISS, "png"))
			{
				PRINT_DEBUG("Error: Read PNMImage");
			}
		}
	}
	///mRigidBodyData
	do_read_rigid_body_data(scan);
	///@}

	/// Dynamic stuff: read only if setup and defer restoring to post_process_from_bam()
	if (mSetup)
	{
		// service variables
		LVecBase3 triVal;
		// linear/angular velocities
		triVal.read_datagram(scan);
		triVal.write_datagram(mOutDatagram);
		triVal.read_datagram(scan);
		triVal.write_datagram(mOutDatagram);
		// total force/torque
		triVal.read_datagram(scan);
		triVal.write_datagram(mOutDatagram);
		triVal.read_datagram(scan);
		triVal.write_datagram(mOutDatagram);
		// use a temporary *Attrs of the right type
		CollisionObjectAttrs* attrsTmp = do_create_collision_object_attrs();
		attrsTmp->read_datagram(scan, &mOutDatagram);
		delete attrsTmp;
	}

	///The object this BT3RigidBody is attached to.
	manager->read_pointer(scan);
	///The reference node path.
	manager->read_pointer(scan);
	// for correct restoring
	mBuildFromBam = true;
}

/**
 * Writes mRigidBodyData into a Datagram.
 * \note Internal use only.
 */
void BT3RigidBody::do_write_rigid_body_data(Datagram &dg)
{
	dg.add_stdfloat(mRigidBodyData.linearDamping);
	dg.add_stdfloat(mRigidBodyData.angularDamping);
	dg.add_stdfloat(mRigidBodyData.friction);
	dg.add_stdfloat(mRigidBodyData.rollingFriction);
	dg.add_stdfloat(mRigidBodyData.spinningFriction);
	dg.add_stdfloat(mRigidBodyData.restitution);
	dg.add_stdfloat(mRigidBodyData.linearSleepingThreshold);
	dg.add_stdfloat(mRigidBodyData.angularSleepingThreshold);
	dg.add_bool(mRigidBodyData.additionalDamping);
	dg.add_stdfloat(mRigidBodyData.additionalDampingFactor);
	dg.add_stdfloat(mRigidBodyData.additionalLinearDampingThresholdSqr);
	dg.add_stdfloat(mRigidBodyData.additionalAngularDampingThresholdSqr);
	dg.add_stdfloat(mRigidBodyData.additionalAngularDampingFactor);
}

/**
 * Reads mRigidBodyData from a DatagramIterator.
 * \note Internal use only.
 */
void BT3RigidBody::do_read_rigid_body_data(DatagramIterator &scan)
{
	mRigidBodyData.linearDamping = scan.get_stdfloat();
	mRigidBodyData.angularDamping = scan.get_stdfloat();
	mRigidBodyData.friction = scan.get_stdfloat();
	mRigidBodyData.rollingFriction = scan.get_stdfloat();
	mRigidBodyData.spinningFriction = scan.get_stdfloat();
	mRigidBodyData.restitution = scan.get_stdfloat();
	mRigidBodyData.linearSleepingThreshold = scan.get_stdfloat();
	mRigidBodyData.angularSleepingThreshold = scan.get_stdfloat();
	mRigidBodyData.additionalDamping = scan.get_bool();
	mRigidBodyData.additionalDampingFactor = scan.get_stdfloat();
	mRigidBodyData.additionalLinearDampingThresholdSqr = scan.get_stdfloat();
	mRigidBodyData.additionalAngularDampingThresholdSqr = scan.get_stdfloat();
	mRigidBodyData.additionalAngularDampingFactor = scan.get_stdfloat();
}

TYPED_OBJECT_API_DEF(BT3RigidBody)
