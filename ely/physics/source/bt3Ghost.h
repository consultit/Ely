/**
 * \file bt3Ghost.h
 *
 * \date 2017-03-17
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_BT3GHOST_H_
#define PHYSICS_SOURCE_BT3GHOST_H_

#include "gamePhysicsManager.h"
#include "physics_includes.h"
#include "geoMipTerrain.h"
#include "bt3CollisionObjectAttrs.h"

#ifndef CPPPARSER
#include "support/ghost.h"
#endif //CPPPARSER

/**
 * BT3Ghost is a PandaNode representing a "ghost" of the Bullet library.
 *
 * It constructs a ghost with the single specified collision shape type
 * along with relevant parameters.\n
 * When enabled, BT3Ghost can throw these events:
 * - when it overlaps an object (default: <objectName>_<ghostName>_Overlap).
 * This Event is thrown continuously until the object keeps overlapping, at a
 * frequency which is the minimum between the fps and the frequency specified
 * (which defaults to 30 times per seconds).
 * - when it stops overlapping an object (default:
 * <objectName>_<ghostName>_OverlapOff). This event is thrown only once.
 *
 * Both these Events have as arguments the object and this BT3Ghost involved in
 * the overlapping/stop overlapping.
 *
 * \note All physical quantities are expressed in the same units of measurement
 * used by Bullet.
 *
 * > **BT3Ghost text parameters**:
 * param | type | default | note
 * ------|------|---------|-----
 * | *thrown_events* 			|single| *overlap@@30.0* | specified as "event1@[event_name1]@[frequency1][:...[:eventN@[event_nameN]@[frequencyN]]]" with eventX = overlap,
 * | *object*  					|single| - | -
 * | *visible*					|single| *false* | -
 * | *shape_type*  				|single| *sphere* | values: box,plane,sphere,capsule,cylinder,cone,multi_sphere,convex_hull,gimpact,triangle_mesh,terrain,compound
 * | *child_shape_type*  		|single| *convex_hull* | values: box,plane,sphere,capsule,cylinder,cone,multi_sphere,convex_hull,gimpact,triangle_mesh,terrain,compound
 * | *use_shape_of*				|single| - | -
 * | *up_axis*  				|single| *z* | values: x,y,z,no_up
 * | *group_mask*  				|single| *all_on* | -
 * | *collide_mask*  			|single| *all_on* | -
 * | *kinematic*				|single| *true* | true if the inner Bullet ghost is updated by this node, false if the opposite
 *
 * \note parts inside [] are optional.\n
 */
class EXPCL_PHYSICS BT3Ghost: public PandaNode
{
PUBLISHED:

	/**
	 * Ghost thrown events.
	 */
	enum BT3EventThrown: unsigned char
	{
		OVERLAP
	};

	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	virtual ~BT3Ghost();
#endif //CPPPARSER

	/**
	 * \name OWNER OBJECT
	 */
	///@{
	INLINE void set_owner_object(const NodePath& object);
	INLINE NodePath get_owner_object() const;
	// Python Properties
	MAKE_PROPERTY(owner_object, get_owner_object, set_owner_object);
	///@}

	/**
	 * \name GHOST
	 */
	///@{
	int setup();
	int cleanup();
	void update(float dt);
	///@}

	/**
	 * \name CONSTRUCTION PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE void set_shape_type(GamePhysicsManager::BT3ShapeType value);
	INLINE GamePhysicsManager::BT3ShapeType get_shape_type() const;
	INLINE void set_child_shape_type(GamePhysicsManager::BT3ShapeType value);
	INLINE GamePhysicsManager::BT3ShapeType get_child_shape_type() const;
	INLINE void set_group_mask(const CollideMask& value);
	INLINE const CollideMask& get_group_mask() const;
	INLINE void set_collide_mask(const CollideMask& value);
	INLINE const CollideMask& get_collide_mask() const;
	INLINE void set_use_shape_of(const string& value);
	INLINE const string& get_use_shape_of() const;
	INLINE void set_up_axis(GamePhysicsManager::BT3UpAxis value);
	INLINE GamePhysicsManager::BT3UpAxis get_up_axis() const;
	INLINE void set_user_data(TypedObject* userData);
	// Python Properties
	MAKE_PROPERTY(shape_type, get_shape_type, set_shape_type);
	MAKE_PROPERTY(child_shape_type, get_child_shape_type, set_child_shape_type);
	MAKE_PROPERTY(group_mask, get_group_mask, set_group_mask);
	MAKE_PROPERTY(collide_mask, get_collide_mask, set_collide_mask);
	MAKE_PROPERTY(use_shape_of, get_use_shape_of, set_use_shape_of);
	MAKE_PROPERTY(up_axis, get_up_axis, set_up_axis);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE void set_visible(bool value);
	INLINE bool get_visible() const;
	INLINE void set_kinematic(bool value);
	INLINE bool get_kinematic() const;
	INLINE int get_num_overlapping_objects() const;
	INLINE NodePath get_overlapping_object(int index) const;
	MAKE_SEQ(get_overlapping_objects, get_num_overlapping_objects, get_overlapping_object);
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	INLINE PyObject* get_collision_object_attrs();
	// Python Properties
	MAKE_PROPERTY(collision_object_attrs, get_collision_object_attrs);
#else
	inline CollisionObjectAttrs* get_collision_object_attrs();
#endif //PYTHON_BUILD
	// Python Properties
	MAKE_PROPERTY(visible, get_visible, set_visible);
	MAKE_PROPERTY(kinematic, get_kinematic, set_kinematic);
	MAKE_SEQ_PROPERTY(overlapping_objects, get_num_overlapping_objects, get_overlapping_object);
	///@}

	/**
	 * \name EVENTS
	 */
	///@{
	void enable_throw_event(BT3EventThrown event, bool enable,
			float frequency = 30.0, const string& eventName = "");
	///@}

	/**
	 * \name OUTPUT
	 */
	///@{
	void output(ostream &out) const;
	///@}

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	/**
	 * \name PYTHON UPDATE CALLBACK
	 */
	///@{
	void set_update_callback(PyObject *clbk);
	///@}
#else
	/**
	 * \name C++ UPDATE CALLBACK
	 */
	///@{
	typedef void (*UPDATECALLBACKFUNC)(PT(BT3Ghost));
	void set_update_callback(UPDATECALLBACKFUNC value);
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name TypedWritable API
	 */
	///@{
	void post_process_from_bam();
	///@}

public:
	/**
	 * \name C++ ONLY
	 * Library & support low level related methods.
	 */
	///@{
	inline btGhostObject& get_ghost_object();
	inline const btGhostObject& get_ghost_object() const;
	///@}

	// Explicitly deleted copy constructor and copy assignment operator.
	BT3Ghost(const BT3Ghost&) = delete;
	BT3Ghost& operator=(const BT3Ghost&) = delete;

protected:
	friend void unref_delete<BT3Ghost>(BT3Ghost*);
	friend class GamePhysicsManager;

	BT3Ghost(const string& name);
	virtual ~BT3Ghost();

#ifndef CPPPARSER
private:
	///The owner object this BT3Ghost is attached to.
	NodePath mObjectNP;
	///The reference node path.
	NodePath mReferenceNP;
	///The setup flag.
	bool mSetup;
	///Bullet ghost object reference.
	btGhostObject* mGhost;
	///The motion state of this BT3Ghost.
	bt3::DynamicMotionState* mMotionState;
	///@{
	///Physical parameters.
	//construction.
	GamePhysicsManager::BT3ShapeType mShapeType;
	GamePhysicsManager::BT3ShapeType mChildShapeType;
	BitMask32 mGroupMask, mCollideMask;
	string mUseShapeOfId;
	GamePhysicsManager::BT3UpAxis mUpAxis;
	void* mUserData;
	//dynamic
	bool mKinematic;
#if defined(PYTHON_BUILD)
	PyObject *mCollisionObjectAttrsPy;
#endif //PYTHON_BUILD
	CollisionObjectAttrs* mCollisionObjectAttrs;
	///@}

	inline void do_reset();
	void do_initialize();
	void do_finalize();

	///Helpers.
	GamePhysicsManager::BT3ShapeType do_get_shape_type(const string& name) const;
	BitMask32 do_get_mask(const string& name) const;
	CollisionObjectAttrs* do_create_collision_object_attrs();
#if defined(PYTHON_BUILD)
	PyObject *do_create_collision_object_attrs_py();
#endif //PYTHON_BUILD

	/**
	 * \name Ghost events.
	 */
	///@{
	/// Overlap notification through event.
	struct OverlappingNode
	{
		struct OverlappingNodeData
		{
			PandaNode* mPnode;
			string mEventName;
			int mCount;
		};
		//main constructors
		OverlappingNode(PandaNode* _pnode) :
				mOverlappingNodeData(new OverlappingNodeData), mRefCount(new int)
		{
			mOverlappingNodeData->mPnode = _pnode;
			*mRefCount = 1;
		}
		OverlappingNode(PandaNode* _pnode, const string& _name, int _count) :
				mOverlappingNodeData(new OverlappingNodeData), mRefCount(new int)
		{
			mOverlappingNodeData->mPnode = _pnode;
			mOverlappingNodeData->mEventName = _name;
			mOverlappingNodeData->mCount = _count;
			*mRefCount = 1;
		}
		//copy constructor
		OverlappingNode(const OverlappingNode& on) :
				mOverlappingNodeData(on.mOverlappingNodeData), mRefCount(on.mRefCount)
		{
			++(*mRefCount);
		}
		//destructor
		~OverlappingNode()
		{
			if (--(*mRefCount) == 0)
			{
				delete mOverlappingNodeData;
				delete mRefCount;
			}
		}
		//assignment operator
		OverlappingNode& operator=(const OverlappingNode& on)
		{
			mOverlappingNodeData = on.mOverlappingNodeData;
			++(*mRefCount);
			return *this;
		}
		//comparison operator
		bool operator<(const OverlappingNode& on) const
		{
			return mOverlappingNodeData->mPnode < on.mOverlappingNodeData->mPnode;
		}
		//owned resource
		OverlappingNodeData* mOverlappingNodeData;
	private:
		int* mRefCount;
	};
	ThrowEventData mOverlap;
	//
	pset<OverlappingNode> mOverlappingNodes;
	///@}

#if defined(PYTHON_BUILD)
	/**
	 * \name Python callback.
	 */
	///@{
	PyObject *mSelf, *mUpdateCallback;
	///@}
#else
	/**
	 * \name C++ callback.
	 */
	///@{
	UPDATECALLBACKFUNC mUpdateCallback;
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name SERIALIZATION ONLY SETTINGS.
	 */
	///@{
	// persistent storage for serialized data
	bool mBuildFromBam;
	Datagram mOutDatagram;
	///@}
#endif //CPPPARSER

public:
	/**
	 * \name TypedWritable API
	 */
	///@{
	static void register_with_read_factory();
	virtual void write_datagram (BamWriter *manager, Datagram &dg) override;
	virtual int complete_pointers(TypedWritable **p_list, BamReader *manager) override;
	///@}

#ifndef CPPPARSER
protected:
	static TypedWritable *make_from_bam(const FactoryParams &params);
	virtual void fillin(DatagramIterator &scan, BamReader *manager) override;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(BT3Ghost,PandaNode)
};

INLINE ostream &operator << (ostream &out, const BT3Ghost & ghost);

///inline
#include "bt3Ghost.I"

#endif /* PHYSICS_SOURCE_BT3GHOST_H_ */
