/**
 * \file physics_includes.h
 *
 * \date 2016-10-09
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_PHYSICS_INCLUDES_H_
#define PHYSICS_SOURCE_PHYSICS_INCLUDES_H_

#include "physicssymbols.h"

#include "../../common/source/commonTools.h"
#include "../../common/source/commonMacros.h"

#include "pairs_valueLists.h"
#include "utilities.h"

#ifdef CPPPARSER
//Panda3d interrogate fake declarations

namespace bt3
{
	struct Physics;
	struct PhysicsInfo;
	struct Picker;
	struct DynamicMotionState;
	struct ConstraintData;
	struct RigidBodyData;
	struct SoftBodyData
	{
		enum SoftBodyType;
	};
	struct VehicleData;
	struct CharacterControllerData;
}
struct btRigidBody;
struct btSoftBody
{
	struct Material;
	struct Config;
	struct Node;
	struct Joint
	{
		struct Specs;
	};
	struct LJoint
	{
		struct Specs;
	};
	struct AJoint
	{
		struct Specs;
	};
};
struct btGhostObject;
struct btTypedConstraint;
struct btRotationalLimitMotor;
struct btTranslationalLimitMotor;
struct btRotationalLimitMotor2;
struct btTranslationalLimitMotor2;
struct btRaycastVehicle
{
	struct btVehicleTuning;
};
struct btWheelInfo;
struct btKinematicCharacterController;
struct btMatrix3x3;
struct btCollisionWorld
{
	struct RayResultCallback;
	struct ClosestRayResultCallback;
	struct AllHitsRayResultCallback;
	struct ConvexResultCallback;
	struct ClosestConvexResultCallback;
	struct ContactResultCallback;
};
struct lwContactPoint;
struct bt2SingleContactResultCallback;
struct bt2PairContactResultCallback;

#endif //CPPPARSER

#endif /* PHYSICS_SOURCE_PHYSICS_INCLUDES_H_ */
