/**
 * \file bt3ConstraintAttrs.cxx
 *
 * \date 2017-04-02
 * \author consultit
 */

#include "bt3ConstraintAttrs.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
#define GETATTRDEF(CLSATTR) \
	CLSATTR::CLSATTR()\
	{\
		msg = #CLSATTR;\
		__dict__ = PyDict_New();\
	}\
	CLSATTR::~CLSATTR()\
	{\
		Py_DECREF(__dict__);\
	}\
	PyObject *CLSATTR::__getattr__(PyObject *attr) const\
	{\
		return TypedConstraintAttrs::check_getattr(attr);\
	}
#else
#define GETATTRDEF(CLSATTR) \
	CLSATTR::CLSATTR()\
	{\
	}\
	CLSATTR::~CLSATTR()\
	{\
	}
#endif //PYTHON_BUILD

///TypedConstraintAttrs definitions
TypedConstraintAttrs::TypedConstraintAttrs():mConstraint{nullptr}
{
}
TypedConstraintAttrs::~TypedConstraintAttrs()
{
}
#if defined(PYTHON_BUILD)
PyObject *TypedConstraintAttrs::check_getattr(PyObject *attr) const
{
	PyObject *item = PyDict_GetItem(__dict__, attr);
	if (item == nullptr)
	{
#if PY_MAJOR_VERSION < 3
		string attrName = PyString_AS_STRING(attr);
#else
		string attrName = PyUnicode_AsUTF8(attr);
#endif
		cerr << msg << ": not existent attribute: " << attrName << endl;
		Py_RETURN_NONE;
	}
	// PyDict_GetItem returns a borrowed reference.
	Py_INCREF(item);
	return item;
}
#endif //PYTHON_BUILD
void TypedConstraintAttrs::output(ostream &out) const
{
	out
	<< "enabled " << get_enable() << endl
	<< "override_num_solver_iterations " << get_override_num_solver_iterations() << endl
	<< "breaking_impulse_threshold " << get_breaking_impulse_threshold() << endl
	<< "enable_feedback " << get_enable_feedback() << endl
	<< "applied_impulse " << get_applied_impulse() << endl
	<< "debug_draw_size " << get_debug_draw_size() << endl;
}
void TypedConstraintAttrs::write_datagram(Datagram &dg) const
{
	dg.add_bool(get_enable());
	dg.add_int32(get_override_num_solver_iterations());
	dg.add_stdfloat(get_breaking_impulse_threshold());
	dg.add_bool(get_enable_feedback());
	///m_appliedImpulse is private: cannot de-serialize fixme
	dg.add_stdfloat(get_debug_draw_size());
}
void TypedConstraintAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	if (outDatagram)
	{
		outDatagram->add_bool(scan.get_bool());
		outDatagram->add_int32(scan.get_int32());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_bool(scan.get_bool());
		///m_appliedImpulse is private: cannot de-serialize fixme
		outDatagram->add_stdfloat(scan.get_stdfloat());
	}
	else
	{
		set_enable(scan.get_bool());
		set_override_num_solver_iterations(scan.get_int32());
		set_breaking_impulse_threshold(scan.get_stdfloat());
		set_enable_feedback(scan.get_bool());
		///m_appliedImpulse is private: cannot de-serialize fixme
		set_debug_draw_size(scan.get_stdfloat());
	}
}
///PointToPointAttrs definitions
GETATTRDEF(PointToPointAttrs)
void PointToPointAttrs::output(ostream &out) const
{
	TypedConstraintAttrs::output(out);
}
void PointToPointAttrs::write_datagram(Datagram &dg) const
{
	TypedConstraintAttrs::write_datagram(dg);
}
void PointToPointAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	TypedConstraintAttrs::read_datagram(scan, outDatagram);
}
///HingeAttrs definitions
GETATTRDEF(HingeAttrs)
void HingeAttrs::output(ostream &out) const
{
	TypedConstraintAttrs::output(out);
	out
	<< "angular_only " << get_angular_only() << endl
	<< "enable_angular_motor " << get_enable_angular_motor() << endl
	<< "max_motor_impulse " << get_max_motor_impulse() << endl
	<< "motor_target_velocity " << get_motor_target_velocity() << endl
	<< "has_limit " << get_has_limit() << endl
	<< "limit_softness " << get_limit_softness() << endl
	<< "limit_bias_factor " << get_limit_bias_factor() << endl
	<< "limit_relaxation_factor " << get_limit_relaxation_factor() << endl
	<< "lower_limit " << get_lower_limit() << endl
	<< "upper_limit " << get_upper_limit() << endl
	<< "solve_limit " << get_solve_limit() << endl
	<< "limit_sign " << get_limit_sign() << endl
	<< "hinge_angle " << get_hinge_angle() << endl
	<< "flags " << get_flags() << endl;
}
void HingeAttrs::write_datagram(Datagram &dg) const
{
	TypedConstraintAttrs::write_datagram(dg);
	dg.add_bool(get_angular_only());
	dg.add_bool(get_enable_angular_motor());
	dg.add_stdfloat(get_motor_target_velocity());
	dg.add_stdfloat(get_max_motor_impulse());
	dg.add_stdfloat(get_lower_limit());
	dg.add_stdfloat(get_upper_limit());
	dg.add_stdfloat(get_limit_softness());
	dg.add_stdfloat(get_limit_bias_factor());
	dg.add_stdfloat(get_limit_relaxation_factor());
}
void HingeAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	TypedConstraintAttrs::read_datagram(scan, outDatagram);
	if (outDatagram)
	{
		outDatagram->add_bool(scan.get_bool());
		outDatagram->add_bool(scan.get_bool());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
	}
	else
	{
		set_angular_only(scan.get_bool());
		set_enable_angular_motor(scan.get_bool());
		set_motor_target_velocity(scan.get_stdfloat());
		set_max_motor_impulse(scan.get_stdfloat());
		//note: must serialize value restoring
		float low = scan.get_stdfloat();
		float high = scan.get_stdfloat();
		float _softness = scan.get_stdfloat();
		float _biasFactor = scan.get_stdfloat();
		float _relaxationFactor = scan.get_stdfloat();
		set_limit(low, high, _softness,	_biasFactor, _relaxationFactor);
	}
}
///HingeAccumulatedAttrs definitions
GETATTRDEF(HingeAccumulatedAttrs)
void HingeAccumulatedAttrs::output(ostream &out) const
{
	HingeAttrs::output(out);
	out
	<< "accumulated_hinge_angle " << get_accumulated_hinge_angle() << endl;
}
void HingeAccumulatedAttrs::write_datagram(Datagram &dg) const
{
	HingeAttrs::write_datagram(dg);
	dg.add_stdfloat(get_accumulated_hinge_angle());
}
void HingeAccumulatedAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	HingeAttrs::read_datagram(scan, outDatagram);
	if (outDatagram)
	{
		outDatagram->add_stdfloat(scan.get_stdfloat());
	}
	else
	{
		set_accumulated_hinge_angle(scan.get_stdfloat());
	}
}
///ConeTwistAttrs definitions
GETATTRDEF(ConeTwistAttrs)
void ConeTwistAttrs::output(ostream &out) const
{
	TypedConstraintAttrs::output(out);
	out
	<< "angular_only " << get_angular_only() << endl
	<< "swing_span1 " << get_swing_span1() << endl
	<< "swing_span2 " << get_swing_span2() << endl
	<< "twist_span " << get_twist_span() << endl
	<< "limit_softness " << get_limit_softness() << endl
	<< "bias_factor " << get_bias_factor() << endl
	<< "relaxation_factor " << get_relaxation_factor() << endl
	<< "damping " << get_damping() << endl
	<< "is_motor_enabled " << get_enable_motor() << endl
	<< "max_motor_impulse " << get_max_motor_impulse() << endl
	<< "is_max_motor_impulse_normalized " << get_max_motor_impulse_normalized() << endl
	<< "motor_target " << get_motor_target() << endl
	<< "fix_thresh " << get_fix_thresh() << endl
	<< "solve_twist_limit " << get_solve_twist_limit() << endl
	<< "solve_swing_limit " << get_solve_swing_limit() << endl
	<< "twist_limit_sign " << get_twist_limit_sign() << endl
	<< "twist_angle " << get_twist_angle() << endl
	<< "is_past_swing_limit " << get_past_swing_limit() << endl
	<< "flags " << get_flags() << endl;
}
void ConeTwistAttrs::write_datagram(Datagram &dg) const
{
	TypedConstraintAttrs::write_datagram(dg);
	dg.add_bool(get_angular_only());
	dg.add_stdfloat(get_swing_span1());
	dg.add_stdfloat(get_swing_span2());
	dg.add_stdfloat(get_twist_span());
	dg.add_stdfloat(get_limit_softness());
	dg.add_stdfloat(get_bias_factor());
	dg.add_stdfloat(get_relaxation_factor());
	dg.add_stdfloat(get_damping());
}
void ConeTwistAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	TypedConstraintAttrs::read_datagram(scan, outDatagram);
	if (outDatagram)
	{
		outDatagram->add_bool(scan.get_bool());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
	}
	else
	{
		set_angular_only(scan.get_bool());
		//note: must serialize value restoring
		float swing_span1 = scan.get_stdfloat();
		float swing_span2 = scan.get_stdfloat();
		float twist_span = scan.get_stdfloat();
		float limit_softness = scan.get_stdfloat();
		float bias_factor = scan.get_stdfloat();
		float relaxation_factor = scan.get_stdfloat();
		set_limit(swing_span1, swing_span2, twist_span, limit_softness,
				bias_factor, relaxation_factor);
		set_damping(scan.get_stdfloat());
	}
}
///GearAttrs definitions
GETATTRDEF(GearAttrs)
void GearAttrs::output(ostream &out) const
{
	TypedConstraintAttrs::output(out);
	out
	<< "ratio " << get_ratio() << endl;
}
void GearAttrs::write_datagram(Datagram &dg) const
{
	TypedConstraintAttrs::write_datagram(dg);
	dg.add_stdfloat(get_ratio());
}
void GearAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	TypedConstraintAttrs::read_datagram(scan, outDatagram);
	if (outDatagram)
	{
		outDatagram->add_stdfloat(scan.get_stdfloat());
	}
	else
	{
		set_ratio(scan.get_stdfloat());
	}
}
///SliderAttrs definitions
GETATTRDEF(SliderAttrs)
void SliderAttrs::output(ostream &out) const
{
	TypedConstraintAttrs::output(out);
	out
	<< "lower_lin_limit " << get_lower_lin_limit() << endl
	<< "upper_lin_limit " << get_upper_lin_limit() << endl
	<< "lower_ang_limit " << get_lower_ang_limit() << endl
	<< "upper_ang_limit " << get_upper_ang_limit() << endl
	<< "softness_dir_lin " << get_softness_dir_lin() << endl
	<< "restitution_dir_lin " << get_restitution_dir_lin() << endl
	<< "softness_lim_ang " << get_softness_lim_ang() << endl
	<< "restitution_lim_ang " << get_restitution_lim_ang() << endl
	<< "damping_lim_ang " << get_damping_lim_ang() << endl
	<< "softness_ortho_lin " << get_softness_ortho_lin() << endl
	<< "restitution_ortho_lin " << get_restitution_ortho_lin() << endl
	<< "damping_ortho_lin " << get_damping_ortho_lin() << endl
	<< "softness_ortho_ang " << get_softness_ortho_ang() << endl
	<< "restitution_ortho_ang " << get_restitution_ortho_ang() << endl
	<< "damping_ortho_ang " << get_damping_ortho_ang() << endl
	<< "damping_dir_lin " << get_damping_dir_lin() << endl
	<< "softness_dir_ang " << get_softness_dir_ang() << endl
	<< "restitution_dir_ang " << get_restitution_dir_ang() << endl
	<< "damping_dir_ang " << get_damping_dir_ang() << endl
	<< "softness_lim_lin " << get_softness_lim_lin() << endl
	<< "restitution_lim_lin " << get_restitution_lim_lin() << endl
	<< "damping_lim_lin " << get_damping_lim_lin() << endl
	<< "powered_lin_motor " << get_powered_lin_motor() << endl
	<< "target_lin_motor_velocity " << get_target_lin_motor_velocity() << endl
	<< "max_lin_motor_force " << get_max_lin_motor_force() << endl
	<< "powered_ang_motor " << get_powered_ang_motor() << endl
	<< "target_ang_motor_velocity " << get_target_ang_motor_velocity() << endl
	<< "max_ang_motor_force " << get_max_ang_motor_force() << endl
	<< "use_frame_offset " << get_use_frame_offset() << endl
	<< "linear_pos " << get_linear_pos() << endl
	<< "angular_pos " << get_angular_pos() << endl
	<< "flags " << get_flags() << endl;
}
void SliderAttrs::write_datagram(Datagram &dg) const
{
	TypedConstraintAttrs::write_datagram(dg);
	dg.add_stdfloat(get_upper_lin_limit());
	dg.add_stdfloat(get_lower_lin_limit());
	dg.add_stdfloat(get_upper_ang_limit());
	dg.add_stdfloat(get_lower_ang_limit());
	dg.add_bool(get_use_frame_offset());
}
void SliderAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	TypedConstraintAttrs::read_datagram(scan, outDatagram);
	if (outDatagram)
	{
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_bool(scan.get_bool());
	}
	else
	{
		set_upper_lin_limit(scan.get_stdfloat());
		set_lower_lin_limit(scan.get_stdfloat());
		set_upper_ang_limit(scan.get_stdfloat());
		set_lower_ang_limit(scan.get_stdfloat());
		set_use_frame_offset(scan.get_bool());
	}
}
///Dof6Attrs definitions
GETATTRDEF(Dof6Attrs)
void Dof6Attrs::output(ostream &out) const
{
	TypedConstraintAttrs::output(out);
	for (int i = 0; i < 3; i++)
	{
		out << "rotational_limit_motor " << i << "-"<< endl;
		get_rotational_limit_motor(i).output(out);
	}
	out << "translational_limit_motor -"<< endl;
	get_translational_limit_motor().output(out);
	out
	<< "limited ";
	for (int i = 0; i < 6; i++)
	{
		out << "i " << get_limited(i) << ", ";
	}
	out << endl
	<< "axes ";
	for (int i = 0; i < 6; i++)
	{
		out << "i " << get_axis(i) << ", ";
	}
	out << endl
	<< "angles ";
	for (int i = 0; i < 3; i++)
	{
		out << "i " << get_angle(i) << ", ";
	}
	out << endl
	<< "relative_pivot_positions: ";
	for (int i = 0; i < 3; i++)
	{
		out << "i " << get_relative_pivot_position(i) << ", ";
	}
	out << endl
	<< "flags " << get_flags() << endl;
}
void Dof6Attrs::write_datagram(Datagram &dg) const
{
	TypedConstraintAttrs::write_datagram(dg);
	for (int i = 0; i < 3; i++)
	{
		get_rotational_limit_motor(i).write_datagram(dg);
	}
	get_translational_limit_motor().write_datagram(dg);
}
void Dof6Attrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	TypedConstraintAttrs::read_datagram(scan, outDatagram);
	if (outDatagram)
	{
		for (int i = 0; i < 3; i++)
		{
			get_rotational_limit_motor(i).read_datagram(scan);
			get_rotational_limit_motor(i).write_datagram(*outDatagram);
		}
		get_translational_limit_motor().read_datagram(scan);
		get_translational_limit_motor().write_datagram(*outDatagram);
	}
	else
	{
		for (int i = 0; i < 3; i++)
		{
			get_rotational_limit_motor(i).read_datagram(scan);
		}
		get_translational_limit_motor().read_datagram(scan);
	}
}
///Dof6SpringAttrs definitions
GETATTRDEF(Dof6SpringAttrs)
void Dof6SpringAttrs::output(ostream &out) const
{
	Dof6Attrs::output(out);
	out
	<< "enable_spring ";
	for (int i = 0; i < 6; i++)
	{
		out << "i " << get_enable_spring(i) << ", ";
	}
	out << endl
	<< "stiffness ";
	for (int i = 0; i < 6; i++)
	{
		out << "i " << get_stiffness(i) << ", ";
	}
	out << endl
	<< "damping ";
	for (int i = 0; i < 6; i++)
	{
		out << "i " << get_damping(i) << ", ";
	}
	out << endl
	<< "equilibrium_point ";
	for (int i = 0; i < 6; i++)
	{
		out << "i " << get_equilibrium_point(i) << ", ";
	}
	out << endl;
}
void Dof6SpringAttrs::write_datagram(Datagram &dg) const
{
	Dof6Attrs::write_datagram(dg);
	for (int i = 0; i < 6; i++)
	{
		dg.add_bool(get_enable_spring(i));
	}
	for (int i = 0; i < 6; i++)
	{
		dg.add_stdfloat(get_equilibrium_point(i));
	}
	for (int i = 0; i < 6; i++)
	{
		dg.add_stdfloat(get_stiffness(i));
	}
	for (int i = 0; i < 6; i++)
	{
		dg.add_stdfloat(get_damping(i));
	}
}
void Dof6SpringAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	Dof6Attrs::read_datagram(scan, outDatagram);
	if (outDatagram)
	{
		for (int i = 0; i < 6; i++)
		{
			outDatagram->add_bool(scan.get_bool());
		}
		for (int i = 0; i < 6; i++)
		{
			outDatagram->add_stdfloat(scan.get_stdfloat());
		}
		for (int i = 0; i < 6; i++)
		{
			outDatagram->add_stdfloat(scan.get_stdfloat());
		}
		for (int i = 0; i < 6; i++)
		{
			outDatagram->add_stdfloat(scan.get_stdfloat());
		}
	}
	else
	{
		for (int i = 0; i < 6; i++)
		{
			set_enable_spring(i, scan.get_bool());
		}
		for (int i = 0; i < 6; i++)
		{
			set_equilibrium_point(i, scan.get_stdfloat());
		}
		for (int i = 0; i < 6; i++)
		{
			set_stiffness(i, scan.get_stdfloat());
		}
		for (int i = 0; i < 6; i++)
		{
			set_damping(i, scan.get_stdfloat());
		}
	}
}
///UniversalAttrs definitions
GETATTRDEF(UniversalAttrs)
void UniversalAttrs::output(ostream &out) const
{
	Dof6Attrs::output(out);
	out
	<< "angle_1 " << get_angle_1() << endl
	<< "angle_2 " << get_angle_2() << endl
	<< "anchor_1 " << get_anchor_1() << endl
	<< "anchor_2 " << get_anchor_2() << endl
	<< "axis_1 " << get_axis_1() << endl
	<< "axis_2 " << get_axis_2() << endl;
}
void UniversalAttrs::write_datagram(Datagram &dg) const
{
	Dof6Attrs::write_datagram(dg);
}
void UniversalAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	Dof6Attrs::read_datagram(scan, outDatagram);
}
///Dof6Spring2Attrs definitions
GETATTRDEF(Dof6Spring2Attrs)
void Dof6Spring2Attrs::output(ostream &out) const
{
	TypedConstraintAttrs::output(out);
	for (int i = 0; i < 3; i++)
	{
		out << "rotational_limit_motor " << i << "-"<< endl;
		get_rotational_limit_motor(i).output(out);
	}
	out << "translational_limit_motor -"<< endl;
	get_translational_limit_motor().output(out);
	out
	<< "limited ";
	for (int i = 0; i < 6; i++)
	{
		out << "i " << get_limited(i) << ", ";
	}
	out << endl
	<< "rotation_order " << get_rotation_order() << endl
	<< "axis ";
	for (int i = 0; i < 6; i++)
	{
		out << "i " << get_axis(i) << ", ";
	}
	out << endl
	<< "angle ";
	for (int i = 0; i < 6; i++)
	{
		out << "i " << get_angle(i) << ", ";
	}
	out << endl
	<< "relative_pivot_position ";
	for (int i = 0; i < 6; i++)
	{
		out << "i " << get_relative_pivot_position(i) << ", ";
	}
	out << endl;
}
void Dof6Spring2Attrs::write_datagram(Datagram &dg) const
{
	TypedConstraintAttrs::write_datagram(dg);
	for (int i = 0; i < 3; i++)
	{
		get_rotational_limit_motor(i).write_datagram(dg);
	}
	get_translational_limit_motor().write_datagram(dg);
	dg.add_uint8((uint8_t)get_rotation_order());
}
void Dof6Spring2Attrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	TypedConstraintAttrs::read_datagram(scan, outDatagram);
	if (outDatagram)
	{
		for (int i = 0; i < 3; i++)
		{
			get_rotational_limit_motor(i).read_datagram(scan);
			get_rotational_limit_motor(i).write_datagram(*outDatagram);
		}
		get_translational_limit_motor().read_datagram(scan);
		get_translational_limit_motor().write_datagram(*outDatagram);
		outDatagram->add_uint8(scan.get_uint8());
	}
	else
	{
		for (int i = 0; i < 3; i++)
		{
			get_rotational_limit_motor(i).read_datagram(scan);
		}
		get_translational_limit_motor().read_datagram(scan);
		set_rotation_order((BT3Constraint::BT3RotateOrder)scan.get_uint8());
	}
}
///Hinge2Attrs definitions
GETATTRDEF(Hinge2Attrs)
void Hinge2Attrs::output(ostream &out) const
{
	Dof6Spring2Attrs::output(out);
	out
	<< "angle_1 " << get_angle_1() << endl
	<< "angle_2 " << get_angle_2() << endl
	<< "anchor_1 " << get_anchor_1() << endl
	<< "anchor_2 " << get_anchor_2() << endl
	<< "axis_1 " << get_axis_1() << endl
	<< "axis_2 " << get_axis_2() << endl;
}
void Hinge2Attrs::write_datagram(Datagram &dg) const
{
	Dof6Spring2Attrs::write_datagram(dg);
}
void Hinge2Attrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	Dof6Spring2Attrs::read_datagram(scan, outDatagram);
}
///FixedAttrs definitions
GETATTRDEF(FixedAttrs)
void FixedAttrs::output(ostream &out) const
{
	Dof6Spring2Attrs::output(out);
}
void FixedAttrs::write_datagram(Datagram &dg) const
{
	Dof6Spring2Attrs::write_datagram(dg);
}
void FixedAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	Dof6Spring2Attrs::read_datagram(scan, outDatagram);
}

