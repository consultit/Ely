/**
 * \file bt3CharacterController.cxx
 *
 * \date 2017-08-09
 * \author consultit
 */

#if defined(_WIN32)
#include "support_os/pstdint.h"
#endif

#include "bt3CharacterController.h"
#include "throw_event.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
extern struct Dtool_PyTypedObject Dtool_BT3CharacterController;
#endif //PYTHON_BUILD

///BT3CharacterController definitions
/**
 *
 */
BT3CharacterController::BT3CharacterController(const string& name) :
		PandaNode(name)
{
	do_reset();
}

/**
 *
 */
BT3CharacterController::~BT3CharacterController()
{
}

/**
 * Initializes the BT3CharacterController with starting settings.
 * \note Internal use only.
 */
void BT3CharacterController::do_initialize()
{
	GamePhysicsManager* mTmpl = GamePhysicsManager::get_global_ptr();
	// set thrown events
	string thrownEvents = mTmpl->get_parameter_value(
			GamePhysicsManager::CHARACTERCONTROLLER, string("thrown_events"));
	unsigned int idx1, valueNum1;
	pvector<string> paramValuesStr1, paramValuesStr2;
	//events specified
	//event1@[event_name1]@[frequency1][:...[:eventN@[event_nameN]@[frequencyN]]]
	paramValuesStr1 = parseCompoundString(thrownEvents, ':');
	valueNum1 = paramValuesStr1.size();
	for (idx1 = 0; idx1 < valueNum1; ++idx1)
	{
		//eventX@[event_nameX]@[frequencyX]
		paramValuesStr2 = parseCompoundString(paramValuesStr1[idx1], '@');
		if (paramValuesStr2.size() >= 3)
		{
			// get event
			BT3EventThrown event;
			// get event name
			string name;
			// get frequency
			float frequency = abs(STRTOF(paramValuesStr2[2].c_str(), nullptr));
			//get event
			if (paramValuesStr2[0] == "on_ground")
			{
				event = ONGROUND;
				name = paramValuesStr2[1].empty() ?
						get_name() + "_OnGround":
						paramValuesStr2[1];
			}
			else if (paramValuesStr2[0] == "in_air")
			{
				event = INAIR;
				name = paramValuesStr2[1].empty() ?
						get_name() + "_InAir":
						paramValuesStr2[1];
			}
			else
			{
				//paramValuesStr2[0] is not a suitable event:
				//continue with the next event
				continue;
			}
			//enable the event
			enable_throw_event(event, true, frequency, name);
		}
	}
	// up vector
	pvector<string> valuesStr = parseCompoundString(
			mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("up_vector")), ',');
	LVecBase3f triVal(0.0, 0.0, 1.0);
	for (unsigned int i = 0; (i < valuesStr.size()) && (i < 3); i++)
	{
		triVal[i] = STRTOF(valuesStr[i].c_str(), nullptr);
	}
	mCharacterControllerData.up = triVal;
	// gravity
	mCharacterControllerData.gravity =
			abs(STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
							string("gravity")).c_str(),nullptr));
	// step height
	mCharacterControllerData.stepHeight =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("step_height")).c_str(),nullptr);
	//
	float value, absValue;
	//max linear speed (>=0)
	value = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
			string("max_linear_speed")).c_str(), nullptr);
	absValue = abs(value);
	mMaxLinearSpeed = LVecBase3f(absValue, absValue, absValue);
	mMaxLinearSpeedSquared = LVector3f(mMaxLinearSpeed.get_x() * mMaxLinearSpeed.get_x(),
			mMaxLinearSpeed.get_y() * mMaxLinearSpeed.get_y(),
			mMaxLinearSpeed.get_z() * mMaxLinearSpeed.get_z());
	//max angular speed (>=0)
	value = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
			string("max_angular_speed")).c_str(), nullptr);
	mMaxAngularSpeed = abs(value);
	mMaxAngularSpeedSquared = mMaxAngularSpeed * mMaxAngularSpeed;
	//linear accel (>=0)
	value = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
			string("linear_accel")).c_str(), nullptr);
	absValue = abs(value);
	mLinearAccel = LVecBase3f(absValue, absValue, absValue);
	//angular accel (>=0)
	value = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
			string("angular_accel")).c_str(), nullptr);
	mAngularAccel = abs(value);
	//reset actual speeds
	mLinearSpeed = LVector3f::zero();
	mAngularSpeed = 0.0;
	//linear friction (>=0)
	value = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
			string("linear_friction")).c_str(), nullptr);
	mLinearFriction = abs(value);
	//angular friction (>=0)
	value = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
			string("angular_friction")).c_str(), nullptr);
	mAngularFriction = abs(value);
	//stop threshold ([0.0, 1.0])
	value = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
			string("stop_threshold")).c_str(), nullptr);
	mStopThreshold = (value >= 0.0 ? value - floor(value) : ceil(value) - value);
	// local linear movement
	mLocalLinearMovement = (mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("local_linear_movement")) == string("false") ? false : true);
	// linear damping
	mCharacterControllerData.linearDamping =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("linear_damping")).c_str(),nullptr);
	// angular damping
	mCharacterControllerData.angularDamping =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("angular_damping")).c_str(),nullptr);
	// fall speed
	mCharacterControllerData.fallSpeed =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("fall_speed")).c_str(),nullptr);
	// jump speed
	mCharacterControllerData.jumpSpeed =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("jump_speed")).c_str(),nullptr);
	// max slope
	mCharacterControllerData.maxSlope =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("max_slope")).c_str(),nullptr);
	// max penetration depth
	mCharacterControllerData.maxPenetrationDepth =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("max_penetration_depth")).c_str(),nullptr);
	// max jump height
	mCharacterControllerData.maxJumpHeight =
			STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("max_jump_height")).c_str(),nullptr);
	// forward
	mForwardKey = (mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("forward")) == string("disabled") ? false : true);
	// backward
	mBackwardKey = (mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("backward")) == string("disabled") ? false : true);
	// up
	mUpKey = (mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("up")) == string("disabled") ? false : true);
	// down
	mDownKey = (mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("down")) == string("disabled") ? false : true);
	// head left
	mHeadLeftKey = (mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("head_left")) == string("disabled") ? false : true);
	// head right
	mHeadRightKey = (mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("head_right")) == string("disabled") ? false : true);
	// strafe left
	mStrafeLeftKey = (mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("strafe_left")) == string("disabled") ? false : true);
	// strafe right
	mStrafeRightKey = (mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("strafe_right")) == string("disabled") ? false : true);
	// jump
	mJumpKey = (mTmpl->get_parameter_value(GamePhysicsManager::CHARACTERCONTROLLER,
					string("jump")) == string("disabled") ? false : true);
	// object
	string object = mTmpl->get_parameter_value(
			GamePhysicsManager::CHARACTERCONTROLLER, string("object"));
	if(!object.empty())
	{
		// search object under reference node
		NodePath objectNP = mReferenceNP.find(string("**/") + object);
		if (!objectNP.is_empty())
		{
			mObjectNP = objectNP;
			setup();
		}
	}
}

/**
 * Sets the BT3CharacterController up for the given object (if not empty), given the
 * shape's type and size (among others).
 */
int BT3CharacterController::setup()
{
	// continue if characterController has not been setup yet
	CONTINUE_IF_ELSE_R(!mSetup, RESULT_SUCCESS)

	// continue if object not empty
	CONTINUE_IF_ELSE_R(!mObjectNP.is_empty(), RESULT_ERROR)

	// owner object's node must be a BT3Ghost to be used as character
	CONTINUE_IF_ELSE_R(mObjectNP.node()->is_of_type(BT3Ghost::get_class_type()),
			RESULT_ERROR)

	// add a btKinematicCharacterController
	mCharacterController = bt3::addCharacterController(mObjectNP,
			mCharacterControllerData);
	// check for errors
	RETURN_ON_COND(!mCharacterController, RESULT_ERROR)

	// post creation setup
	PT(BT3Ghost)ghost = DCAST(BT3Ghost, mObjectNP.node());
	// BT3Ghost will be driven by its inner Bullet ghost object.
	ghost->set_kinematic(false);
	// make BT3Ghost' owner node geometry visible
	ghost->set_visible(true);

	// set the flag
	mSetup = true;
	//
	return RESULT_SUCCESS;
}

/**
 * On destruction cleanup.
 * Gives an BT3CharacterController the ability to do any cleaning is necessary when
 * destroyed.
 * \note Internal use only.
 */
void BT3CharacterController::do_finalize()
{
	//cleanup (if needed)
	cleanup();

	//remove this NodePath
	NodePath::any_path(this).remove_node();
	//
#ifdef PYTHON_BUILD
	//Python callback
	PY_SELF_ARG_DECREF(mUpdateCallback, mSelf);
#endif //PYTHON_BUILD
	do_reset();
	//
	return;
}

/**
 * Cleans the BT3CharacterController up, detaching any child objects (if not empty).
 */
int BT3CharacterController::cleanup()
{
	// continue if characterController has been already setup
	CONTINUE_IF_ELSE_R(mSetup, RESULT_SUCCESS)

	int result = RESULT_ERROR;

	// remove the characterController
	if (bt3::removeCharacterController(mCharacterController))
	{
		// reset bullet characterController reference
		mCharacterController = nullptr;
		// set the flag
		mSetup = false;
		result = RESULT_SUCCESS;
	}
	//
	return result;
}

/**
 * Enables/disables characterController notifications through events.
 */
void BT3CharacterController::enable_throw_event(BT3EventThrown event, bool enable,
		float frequency, const string& eventName)
{
	ThrowEventData eventData(eventName,	abs(frequency));
	eventData.mEnable = enable;
	switch (event)
	{
	case ONGROUND:
		mOnGround = eventData;
		break;
	case INAIR:
		mInAir = eventData;
		break;
	default:
		break;
	}
}

/**
 * Throws events.
 * \note Internal use only.
 */
void BT3CharacterController::do_throw_event(ThrowEventData& eventData)
{
	if (eventData.mThrown)
	{
		eventData.mTimeElapsed += ClockObject::get_global_clock()->get_dt();
		if (eventData.mTimeElapsed >= eventData.mPeriod)
		{
			//enough time is passed: throw the event
			throw_event(eventData.mEventName, EventParameter(this));
			//update elapsed time
			eventData.mTimeElapsed -= eventData.mPeriod;
		}
	}
	else
	{
		//throw the event
		throw_event(eventData.mEventName, EventParameter(this));
		eventData.mThrown = true;
	}
}

/**
 * Updates the BT3CharacterController state.
 */
void BT3CharacterController::update(float dt)
{
	RETURN_ON_COND(!mSetup,)

#ifdef TESTING
	dt = 0.016666667; //60 fps
#endif

	//update speeds
	//y axis
	if (mForward && (! mBackward))
	{
		if (mLinearAccel.get_y() != 0.0)
		{
			if (mLinearSpeed.get_y() > -mMaxLinearSpeed.get_y())
			{
				//accelerate
				mLinearSpeed.set_y(
						max(-mMaxLinearSpeed.get_y(),
								mLinearSpeed.get_y()
										- mLinearAccel.get_y() * dt));
			}
			else if(mLinearSpeed.get_y() < -mMaxLinearSpeed.get_y())
			{
				//decelerate to max speed
				mLinearSpeed.set_y(
						-mMaxLinearSpeed.get_y()
								+ (mLinearSpeed.get_y()
										- (-mMaxLinearSpeed.get_y()))
										* (1.0 - min(mLinearFriction * dt, 1.0f)));
			}
		}
		else
		{
			//kinematic
			mLinearSpeed.set_y(-mMaxLinearSpeed.get_y());
		}
	}
	else if (mBackward && (! mForward))
	{
		if (mLinearAccel.get_y() != 0.0)
		{
			if (mLinearSpeed.get_y() < mMaxLinearSpeed.get_y())
			{
				//accelerate
				mLinearSpeed.set_y(
						min(mMaxLinearSpeed.get_y(),
								mLinearSpeed.get_y()
										+ mLinearAccel.get_y() * dt));
			}
			else if(mLinearSpeed.get_y() > mMaxLinearSpeed.get_y())
			{
				//decelerate to max speed
				mLinearSpeed.set_y(
						mMaxLinearSpeed.get_y()
								+ (mLinearSpeed.get_y()
										- (mMaxLinearSpeed.get_y()))
										* (1.0 - min(mLinearFriction * dt, 1.0f)));
			}
		}
		else
		{
			//kinematic
			mLinearSpeed.set_y(mMaxLinearSpeed.get_y());
		}
	}
	else if (mLinearSpeed.get_y() != 0.0)
	{
		if (mLinearSpeed.get_y() * mLinearSpeed.get_y()
				< mMaxLinearSpeedSquared.get_y() * mStopThreshold)
		{
			//stop
			mLinearSpeed.set_y(0.0);
		}
		else
		{
			//decelerate
			mLinearSpeed.set_y(
					mLinearSpeed.get_y() * (1.0 - min(mLinearFriction * dt, 1.0f)));
		}
	}
	//x axis
	if (mStrafeLeft && (! mStrafeRight))
	{
		if (mLinearAccel.get_x() != 0.0)
		{
			if (mLinearSpeed.get_x() < mMaxLinearSpeed.get_x())
			{
				//accelerate
				mLinearSpeed.set_x(
						min(mMaxLinearSpeed.get_x(),
								mLinearSpeed.get_x()
										+ mLinearAccel.get_x() * dt));
			}
			else if(mLinearSpeed.get_x() > mMaxLinearSpeed.get_x())
			{
				//decelerate to max speed
				mLinearSpeed.set_x(
						mMaxLinearSpeed.get_x()
								+ (mLinearSpeed.get_x()
										- (mMaxLinearSpeed.get_x()))
										* (1.0 - min(mLinearFriction * dt, 1.0f)));
			}
		}
		else
		{
			//kinematic
			mLinearSpeed.set_x(mMaxLinearSpeed.get_x());
		}
	}
	else if (mStrafeRight && (! mStrafeLeft))
	{
		if (mLinearAccel.get_x() != 0.0)
		{
			if (mLinearSpeed.get_x() > -mMaxLinearSpeed.get_x())
			{
				//accelerate
				mLinearSpeed.set_x(
						max(-mMaxLinearSpeed.get_x(),
								mLinearSpeed.get_x()
										- mLinearAccel.get_x() * dt));
			}
			else if(mLinearSpeed.get_x() < -mMaxLinearSpeed.get_x())
			{
				//decelerate to max speed
				mLinearSpeed.set_x(
						-mMaxLinearSpeed.get_x()
								+ (mLinearSpeed.get_x()
										- (-mMaxLinearSpeed.get_x()))
										* (1.0 - min(mLinearFriction * dt, 1.0f)));
			}
		}
		else
		{
			//kinematic
			mLinearSpeed.set_x(-mMaxLinearSpeed.get_y());
		}
	}
	else if (mLinearSpeed.get_x() != 0.0)
	{
		if (mLinearSpeed.get_x() * mLinearSpeed.get_x()
				< mMaxLinearSpeedSquared.get_x() * mStopThreshold)
		{
			//stop
			mLinearSpeed.set_x(0.0);
		}
		else
		{
			//decelerate
			mLinearSpeed.set_x(
					mLinearSpeed.get_x() * (1.0 - min(mLinearFriction * dt, 1.0f)));
		}
	}
	//z axis
	if (mUp && (! mDown))
	{
		if (mLinearAccel.get_z() != 0.0)
		{
			if (mLinearSpeed.get_z() < mMaxLinearSpeed.get_z())
			{
				//accelerate
				mLinearSpeed.set_z(
						min(mMaxLinearSpeed.get_z(),
								mLinearSpeed.get_z()
										+ mLinearAccel.get_z() * dt));
			}
			else if(mLinearSpeed.get_z() > mMaxLinearSpeed.get_z())
			{
				//decelerate to max speed
				mLinearSpeed.set_z(
						mMaxLinearSpeed.get_z()
								+ (mLinearSpeed.get_z()
										- (mMaxLinearSpeed.get_z()))
										* (1.0 - min(mLinearFriction * dt, 1.0f)));
			}
		}
		else
		{
			//kinematic
			mLinearSpeed.set_z(mMaxLinearSpeed.get_z());
		}
	}
	else if (mDown && (! mUp))
	{
		if (mLinearAccel.get_z() != 0.0)
		{
			if (mLinearSpeed.get_z() > -mMaxLinearSpeed.get_z())
			{
				//accelerate
				mLinearSpeed.set_z(
						max(-mMaxLinearSpeed.get_z(),
								mLinearSpeed.get_z()
										- mLinearAccel.get_z() * dt));
			}
			else if(mLinearSpeed.get_z() < -mMaxLinearSpeed.get_z())
			{
				//decelerate to max speed
				mLinearSpeed.set_z(
						-mMaxLinearSpeed.get_z()
								+ (mLinearSpeed.get_z()
										- (-mMaxLinearSpeed.get_z()))
										* (1.0 - min(mLinearFriction * dt, 1.0f)));
			}
		}
		else
		{
			//kinematic
			mLinearSpeed.set_z(-mMaxLinearSpeed.get_z());
		}
	}
	else if (mLinearSpeed.get_z() != 0.0)
	{
		if (mLinearSpeed.get_z() * mLinearSpeed.get_z()
				< mMaxLinearSpeedSquared.get_z() * mStopThreshold)
		{
			//stop
			mLinearSpeed.set_z(0.0);
		}
		else
		{
			//decelerate
			mLinearSpeed.set_z(
					mLinearSpeed.get_z() * (1.0 - min(mLinearFriction * dt, 1.0f)));
		}
	}
	//rotation h
	if (mHeadLeft && (! mHeadRight))
	{
		if (mAngularAccel != 0.0)
		{
			if (mAngularSpeed < mMaxAngularSpeed)
			{
				//accelerate
				mAngularSpeed = min(mMaxAngularSpeed,
						mAngularSpeed + mAngularAccel * dt);
			}
			else if(mAngularSpeed > mMaxAngularSpeed)
			{
				//decelerate to max speed
				mAngularSpeed = mMaxAngularSpeed
						+ (mAngularSpeed - (mMaxAngularSpeed))
								* (1.0 - min(mAngularFriction * dt, 1.0f));
			}
		}
		else
		{
			//kinematic
			mAngularSpeed = mMaxAngularSpeed;
		}
	}
	else if (mHeadRight && (! mHeadLeft))
	{
		if (mAngularAccel != 0.0)
		{
			if (mAngularSpeed > -mMaxAngularSpeed)
			{
				//accelerate
				mAngularSpeed = max(-mMaxAngularSpeed,
						mAngularSpeed - mAngularAccel * dt);
			}
			else if(mAngularSpeed < -mMaxAngularSpeed)
			{
				//decelerate to max speed
				mAngularSpeed = -mMaxAngularSpeed
						+ (mAngularSpeed - (-mMaxAngularSpeed))
								* (1.0 - min(mAngularFriction * dt, 1.0f));
			}
		}
		else
		{
			//kinematic
			mAngularSpeed = -mMaxAngularSpeed;
		}
	}
	else if (mAngularSpeed != 0.0)
	{
		if (mAngularSpeed * mAngularSpeed < mMaxAngularSpeedSquared * mStopThreshold)
		{
			//stop
			mAngularSpeed = 0.0;
		}
		else
		{
			//decelerate
            mAngularSpeed = mAngularSpeed * (1.0 - min(mAngularFriction * dt, 1.0f));
		}
	}

	//	compute mCharacterController's movement
	{
		btGhostObject& ghost = *mCharacterController->getGhostObject();
		// Angular rotation
		btScalar angle = dt * mAngularSpeed;
		btMatrix3x3 m = ghost.getWorldTransform().getBasis();
		m *= btMatrix3x3(btQuaternion(mCharacterController->getUp(), angle));
		ghost.getWorldTransform().setBasis(m);
		// Linear movement
		btVector3 v;
		if (mLocalLinearMovement)
		{
			btTransform xform = ghost.getWorldTransform();
			xform.setOrigin(btVector3(0.0f, 0.0f, 0.0f));
			v = xform(bt3::LVecBase3_to_btVector3(mLinearSpeed));
		}
		else
		{
			v = bt3::LVecBase3_to_btVector3(mLinearSpeed);
		}
		// mCharacterController->setVelocityForTimeInterval(v, dt);
		mCharacterController->setWalkDirection(v * dt);
	}

	//handle CharacterController ground-air events
	if (mCharacterController->onGround())
	{
		//handle events
		//throw OnGround event (if enabled)
		if (mOnGround.mEnable)
		{
			do_throw_event(mOnGround);
		}
		//reset InAir event (if enabled and if thrown)
		if (mInAir.mEnable and mInAir.mThrown)
		{
			mInAir.mThrown = false;
			mInAir.mTimeElapsed = 0.0;
		}

		//take other actions
		//jump if requested
		if (mJump)
		{
			mCharacterController->jump(btVector3(0.0, 0.0, 0.0));
		}
	}
	else
	{
		//handle events
		//reset OnGround event (if enabled and if thrown)
		if (mOnGround.mEnable and mOnGround.mThrown)
		{
			mOnGround.mThrown = false;
			mOnGround.mTimeElapsed = 0.0;
		}
		//throw InAir event (if enabled)
		if (mInAir.mEnable)
		{
			do_throw_event(mInAir);
		}
	}
	//
	if (mUpdateCallback)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_CALL(mUpdateCallback, mSelf,
				get_name() + " BT3CharacterController::update()");
#else
		// call c++ callback
		mUpdateCallback(this);
#endif //PYTHON_BUILD
	}
}

/**
 * Writes a sensible description of the BT3CharacterController to the indicated output
 * stream.
 */
void BT3CharacterController::output(ostream &out) const
{
	out << get_type() << " " << get_name();
}

#ifdef PYTHON_BUILD
/**
 * Sets the update callback as a python function taking this BT3CharacterController as
 * an argument, or None. On error raises an python exception.
 * \note Python only.
 */
void BT3CharacterController::set_update_callback(PyObject *clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mUpdateCallback, clbk, Dtool_BT3CharacterController,
			get_name() + " BT3CharacterController.set_update_callback()", mSelf,
			this);
}
#else
/**
 * Sets the update callback as a c++ function taking this BT3CharacterController as
 * an argument, or nullptr.
 * \note C++ only.
 */
void BT3CharacterController::set_update_callback(UPDATECALLBACKFUNC value)
{
	mUpdateCallback = value;
}
#endif //PYTHON_BUILD

//TypedWritable API
/**
 * Tells the BamReader how to create objects of type BT3CharacterController.
 */
void BT3CharacterController::register_with_read_factory()
{
	BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

/**
 * Writes the contents of this object to the datagram for shipping out to a
 * Bam file.
 */
void BT3CharacterController::write_datagram(BamWriter *manager, Datagram &dg)
{
	PandaNode::write_datagram(manager, dg);

	///The setup flag.
	dg.add_bool(mSetup);

	///@{
	///Physical parameters.
	///mCharacterControllerData
	do_write_character_controller_data(dg);
	mLinearSpeed.write_datagram(dg);
	mMaxLinearSpeed.write_datagram(dg);
	mMaxLinearSpeedSquared.write_datagram(dg);
	dg.add_stdfloat(mAngularSpeed);
	dg.add_stdfloat(mMaxAngularSpeed);
	dg.add_stdfloat(mMaxAngularSpeedSquared);
	mLinearAccel.write_datagram(dg);
	dg.add_stdfloat(mAngularAccel);
	dg.add_stdfloat(mLinearFriction);
	dg.add_stdfloat(mAngularFriction);
	dg.add_stdfloat(mStopThreshold);
	dg.add_bool(mLocalLinearMovement);
	dg.add_bool(mForward);
	dg.add_bool(mBackward);
	dg.add_bool(mHeadLeft);
	dg.add_bool(mHeadRight);
	dg.add_bool(mUp);
	dg.add_bool(mDown);
	dg.add_bool(mStrafeLeft);
	dg.add_bool(mStrafeRight);
	dg.add_bool(mJump);
	///Key controls and effective keys.
	dg.add_bool(mForwardKey);
	dg.add_bool(mBackwardKey);
	dg.add_bool(mHeadLeftKey);
	dg.add_bool(mHeadRightKey);
	dg.add_bool(mUpKey);
	dg.add_bool(mDownKey);
	dg.add_bool(mStrafeLeftKey);
	dg.add_bool(mStrafeRightKey);
	dg.add_bool(mJumpKey);
	///@{

	/// Move/steady notification through event.
	mOnGround.write_datagram(dg);
	mInAir.write_datagram(dg);
	///@}

	/// Dynamic stuff: read only if setup and defer restoring to post_process_from_bam()
	if (mSetup)
	{
		/// HACK: dummy saving to make restoring correct
		dg.add_bool(true);
	}

	///The owner object this BT3CharacterController is attached to.
	manager->write_pointer(dg, mObjectNP.node());
	///The reference node path.
	manager->write_pointer(dg, mReferenceNP.node());
}

/**
 * Receives an array of pointers, one for each time manager->read_pointer()
 * was called in fillin(). Returns the number of pointers processed.
 */
int BT3CharacterController::complete_pointers(TypedWritable **p_list, BamReader *manager)
{
	int pi = PandaNode::complete_pointers(p_list, manager);

	/// Pointers
	///The object this BT3CharacterController is attached to.
	PT(PandaNode)objectNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mObjectNP = NodePath::any_path(objectNPPandaNode);
	///The reference node path.
	PT(PandaNode)referenceNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mReferenceNP = NodePath::any_path(referenceNPPandaNode);

	return pi;
}

/**
 * Should be after the complete restoring from the bam file.
 * \note for a complete restoring it should be called twice.
 */
void BT3CharacterController::post_process_from_bam()
{
	/// setup this BT3CharacterController if needed
	if (mSetup)
	{
		// first step
		if (mBuildFromBam)
		{
			mBuildFromBam = false;
		}
		// second step
		else if (mOutDatagram.get_length() != 0)
		{
			// all external referenced objects should be set up
			mSetup = !mSetup;
			setup();
			// restore attributes
			DatagramIterator scan(mOutDatagram);
			/// HACK: dummy saving to make restoring correct
			scan.get_bool();
			mOutDatagram.clear();
		}
	}
}

/**
 * This function is called by the BamReader's factory when a new object of
 * type BT3CharacterController is encountered in the Bam file.  It should create the
 * BT3CharacterController and extract its information from the file.
 */
TypedWritable *BT3CharacterController::make_from_bam(const FactoryParams &params)
{
	// continue only if GamePhysicsManager exists
	CONTINUE_IF_ELSE_R(GamePhysicsManager::get_global_ptr(), nullptr)

	// create a BT3CharacterController with default parameters' values: they'll be restored later
	GamePhysicsManager::get_global_ptr()->set_parameters_defaults(
			GamePhysicsManager::CHARACTERCONTROLLER);
	BT3CharacterController *node = DCAST(BT3CharacterController,
			GamePhysicsManager::get_global_ptr()->create_character_controller(
					"BT3CharacterController").node());

	DatagramIterator scan;
	BamReader *manager;

	parse_params(params, scan, manager);
	node->fillin(scan, manager);

	return node;
}

/**
 * This internal function is called by make_from_bam to read in all of the
 * relevant data from the BamFile for the new BT3CharacterController.
 */
void BT3CharacterController::fillin(DatagramIterator &scan, BamReader *manager)
{
	PandaNode::fillin(scan, manager);

	///The setup flag.
	mSetup = scan.get_bool();

	///@{
	///Physical parameters.
	///mCharacterControllerData
	do_read_character_controller_data(scan);
	mLinearSpeed.read_datagram(scan);
	mMaxLinearSpeed.read_datagram(scan);
	mMaxLinearSpeedSquared.read_datagram(scan);
	mAngularSpeed = scan.get_stdfloat();
	mMaxAngularSpeed = scan.get_stdfloat();
	mMaxAngularSpeedSquared = scan.get_stdfloat();
	mLinearAccel.read_datagram(scan);
	mAngularAccel = scan.get_stdfloat();
	mLinearFriction = scan.get_stdfloat();
	mAngularFriction = scan.get_stdfloat();
	mStopThreshold = scan.get_stdfloat();
	mLocalLinearMovement = scan.get_bool();
	mForward = scan.get_bool();
	mBackward = scan.get_bool();
	mHeadLeft = scan.get_bool();
	mHeadRight = scan.get_bool();
	mUp = scan.get_bool();
	mDown = scan.get_bool();
	mStrafeLeft = scan.get_bool();
	mStrafeRight = scan.get_bool();
	mJump = scan.get_bool();
	///Key controls and effective keys.
	mForwardKey = scan.get_bool();
	mBackwardKey = scan.get_bool();
	mHeadLeftKey = scan.get_bool();
	mHeadRightKey = scan.get_bool();
	mUpKey = scan.get_bool();
	mDownKey = scan.get_bool();
	mStrafeLeftKey = scan.get_bool();
	mStrafeRightKey = scan.get_bool();
	mJumpKey = scan.get_bool();
	///@{

	///@{
	/// Move/steady notification through event.
	mOnGround.read_datagram(scan);
	mInAir.read_datagram(scan);
	// reset counters
	enable_throw_event(ONGROUND, mOnGround.mEnable, mOnGround.mFrequency,
			mOnGround.mEventName);
	enable_throw_event(INAIR, mInAir.mEnable, mInAir.mFrequency,
			mInAir.mEventName);
	///@}

	/// Dynamic stuff: read only if setup and defer restoring to post_process_from_bam()
	if (mSetup)
	{
		/// HACK: dummy saving to make restoring correct
		mOutDatagram.add_bool(scan.get_bool());
	}

	///The owner object this BT3CharacterController is attached to.
	manager->read_pointer(scan);
	///The reference node path.
	manager->read_pointer(scan);
	// for correct restoring
	mBuildFromBam = true;
}

/**
 * Writes mCharacterControllerData into a Datagram.
 * \note Internal use only.
 */
void BT3CharacterController::do_write_character_controller_data(Datagram &dg)
{
	mCharacterControllerData.up.write_datagram(dg);
	dg.add_stdfloat(mCharacterControllerData.gravity);
	dg.add_stdfloat(mCharacterControllerData.stepHeight);
	dg.add_stdfloat(mCharacterControllerData.linearDamping);
	dg.add_stdfloat(mCharacterControllerData.angularDamping);
	dg.add_stdfloat(mCharacterControllerData.fallSpeed);
	dg.add_stdfloat(mCharacterControllerData.jumpSpeed);
	dg.add_stdfloat(mCharacterControllerData.maxSlope);
	dg.add_stdfloat(mCharacterControllerData.maxPenetrationDepth);
	dg.add_stdfloat(mCharacterControllerData.maxJumpHeight);
}

/**
 * Reads mCharacterControllerData from a DatagramIterator.
 * \note Internal use only.
 */
void BT3CharacterController::do_read_character_controller_data(DatagramIterator &scan)
{
	mCharacterControllerData.up.read_datagram(scan);
	mCharacterControllerData.gravity = scan.get_stdfloat();
	mCharacterControllerData.stepHeight = scan.get_stdfloat();
	mCharacterControllerData.linearDamping = scan.get_stdfloat();
	mCharacterControllerData.angularDamping = scan.get_stdfloat();
	mCharacterControllerData.fallSpeed = scan.get_stdfloat();
	mCharacterControllerData.jumpSpeed = scan.get_stdfloat();
	mCharacterControllerData.maxSlope = scan.get_stdfloat();
	mCharacterControllerData.maxPenetrationDepth = scan.get_stdfloat();
	mCharacterControllerData.maxJumpHeight = scan.get_stdfloat();
}

TYPED_OBJECT_API_DEF(BT3CharacterController)
