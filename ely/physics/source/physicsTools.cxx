/**
 * \file physicsTools.cxx
 *
 * \date 2017-04-29
 * \author consultit
 */

#include "physicsTools.h"

///BT3PhysicsInfo definitions
/**
 *
 */
BT3PhysicsInfo::BT3PhysicsInfo(): mInfo()
{
}

/**
 *
 */
BT3PhysicsInfo::BT3PhysicsInfo(
			BT3WorldType worldType,
			BT3BroadphaseType broadPhaseType,
			BT3ConstraintSolverType solverType,
			BT3MLCPSolverType mlcpSolverType,
			BT3SolverTypes softBodySolverType,
			int numPoolMtSolvers,
			const LVecBase3f& worldDims,
			unsigned short int maxHandles,
			float airDensity,
			float waterDensity,
			float waterOffset,
			float maxDisplacement,
			const LVector3f& waterNormal,
			///Multi-Threading
			int persistentManifoldPoolSize,
			int collisionAlgorithmPoolSize,
			int grainSize)
{
	mInfo.worldType = static_cast<bt3::PhysicsInfo::WorldType>(worldType);
	mInfo.broadPhaseType = static_cast<bt3::PhysicsInfo::BroadphaseType>(broadPhaseType);
	mInfo.solverType = static_cast<bt3::PhysicsInfo::ConstraintSolverType>(solverType);
	mInfo.mlcpSolverType = static_cast<bt3::PhysicsInfo::MLCPSolverType>(mlcpSolverType);
	mInfo.softBodySolverType = static_cast<btSoftBodySolver::SolverTypes>(softBodySolverType);
	mInfo.numPoolMtSolvers = numPoolMtSolvers;
	mInfo.worldDims = bt3::LVecBase3_to_btVector3_abs(worldDims);
	mInfo.maxHandles = maxHandles;
	mInfo.airDensity = airDensity;
	mInfo.waterDensity = waterDensity;
	mInfo.waterOffset = waterOffset;
	mInfo.maxDisplacement = maxDisplacement;
	mInfo.waterNormal = bt3::LVecBase3_to_btVector3(waterNormal);
	///Multi-Threading
	mInfo.maxPersistentManifoldPoolSize = persistentManifoldPoolSize;
	mInfo.maxCollisionAlgorithmPoolSize = collisionAlgorithmPoolSize;
	mInfo.grainSize = grainSize;
}

///BT3SoftBodyMaterial definitions
/**
 * Writes the BT3SoftBodyMaterial into a datagram.
 */
void BT3SoftBodyMaterial::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_linear_stiffness_coefficient());
	dg.add_stdfloat(get_angular_stiffness_coefficient());
	dg.add_stdfloat(get_volume_stiffness_coefficient());
	dg.add_int32(get_flags());
}

/**
 * Restores the BT3SoftBodyMaterial from the datagram.
 */
void BT3SoftBodyMaterial::read_datagram(DatagramIterator &scan)
{
	set_linear_stiffness_coefficient(scan.get_stdfloat());
	set_angular_stiffness_coefficient(scan.get_stdfloat());
	set_volume_stiffness_coefficient(scan.get_stdfloat());
	set_flags(scan.get_int32());
}

/**
 * Writes a sensible description of the BT3SoftBodyMaterial to the indicated
 * output stream.
 */
void BT3SoftBodyMaterial::output(ostream &out) const
{
	out
	<< "linear_stiffness " << get_linear_stiffness_coefficient() << endl
	<< "angular_stiffness " << get_angular_stiffness_coefficient() << endl
	<< "volume_stiffness " << get_volume_stiffness_coefficient() << endl
	<< "flags " << get_flags() << endl;
}

btSoftBody::Material BT3SoftBodyMaterial::_dummy{};

///BT3SoftBodyConfig definitions
/**
 * Writes the BT3SoftBodyConfig into a datagram.
 */
void BT3SoftBodyConfig::write_datagram(Datagram &dg) const
{
	dg.add_uint8((uint8_t)get_aerodynamic_model());
	dg.add_stdfloat(get_velocities_correction_factor());
	dg.add_stdfloat(get_damping_coefficient());
	dg.add_stdfloat(get_drag_coefficient());
	dg.add_stdfloat(get_lift_coefficient());
	dg.add_stdfloat(get_pressure_coefficient());
	dg.add_stdfloat(get_volume_conversation_coefficient());
	dg.add_stdfloat(get_dynamic_friction_coefficient());
	dg.add_stdfloat(get_pose_matching_coefficient());
	dg.add_stdfloat(get_rigid_contacts_hardness());
	dg.add_stdfloat(get_kinetic_contacts_hardness());
	dg.add_stdfloat(get_soft_contacts_hardness());
	dg.add_stdfloat(get_anchors_hardness());
	dg.add_stdfloat(get_soft_rigid_hardness());
	dg.add_stdfloat(get_soft_kinetic_hardness());
	dg.add_stdfloat(get_soft_soft_hardness());
	dg.add_stdfloat(get_soft_rigid_impulse_split());
	dg.add_stdfloat(get_soft_kinetic_impulse_split());
	dg.add_stdfloat(get_soft_soft_impulse_split());
	dg.add_stdfloat(get_maximum_volume_ratio_pose());
	dg.add_stdfloat(get_time_scale());
	dg.add_int32(get_velocities_solver_iterations());
	dg.add_int32(get_positions_solver_iterations());
	dg.add_int32(get_drift_solver_iterations());
	dg.add_int32(get_cluster_solver_iterations());
	dg.add_int32(get_collisions_flags());
}

/**
 * Restores the BT3SoftBodyConfig from the datagram.
 */
void BT3SoftBodyConfig::read_datagram(DatagramIterator &scan)
{
	set_aerodynamic_model((BT3AeroModel)scan.get_uint8());
	set_velocities_correction_factor(scan.get_stdfloat());
	set_damping_coefficient(scan.get_stdfloat());
	set_drag_coefficient(scan.get_stdfloat());
	set_lift_coefficient(scan.get_stdfloat());
	set_pressure_coefficient(scan.get_stdfloat());
	set_volume_conversation_coefficient(scan.get_stdfloat());
	set_dynamic_friction_coefficient(scan.get_stdfloat());
	set_pose_matching_coefficient(scan.get_stdfloat());
	set_rigid_contacts_hardness(scan.get_stdfloat());
	set_kinetic_contacts_hardness(scan.get_stdfloat());
	set_soft_contacts_hardness(scan.get_stdfloat());
	set_anchors_hardness(scan.get_stdfloat());
	set_soft_rigid_hardness(scan.get_stdfloat());
	set_soft_kinetic_hardness(scan.get_stdfloat());
	set_soft_soft_hardness(scan.get_stdfloat());
	set_soft_rigid_impulse_split(scan.get_stdfloat());
	set_soft_kinetic_impulse_split(scan.get_stdfloat());
	set_soft_soft_impulse_split(scan.get_stdfloat());
	set_maximum_volume_ratio_pose(scan.get_stdfloat());
	set_time_scale(scan.get_stdfloat());
	set_velocities_solver_iterations(scan.get_int32());
	set_positions_solver_iterations(scan.get_int32());
	set_drift_solver_iterations(scan.get_int32());
	set_cluster_solver_iterations(scan.get_int32());
	set_collisions_flags(scan.get_int32());
}

/**
 * Writes a sensible description of the BT3SoftBodyConfig to the indicated
 * output stream.
 */
void BT3SoftBodyConfig::output(ostream &out) const
{
	out
	<< "aerodynamic_model " << get_aerodynamic_model() << endl
	<< "velocities_correction_factor " << get_velocities_correction_factor() << endl
	<< "damping_coefficient " << get_damping_coefficient() << endl
	<< "drag_coefficient " << get_drag_coefficient() << endl
	<< "lift_coefficient " << get_lift_coefficient() << endl
	<< "pressure_coefficient " << get_pressure_coefficient() << endl
	<< "volume_conversation_coefficient " << get_volume_conversation_coefficient() << endl
	<< "dynamic_friction_coefficient " << get_dynamic_friction_coefficient() << endl
	<< "pose_matching_coefficient " << get_pose_matching_coefficient() << endl
	<< "rigid_contacts_hardness " << get_rigid_contacts_hardness() << endl
	<< "kinetic_contacts_hardness " << get_kinetic_contacts_hardness() << endl
	<< "soft_contacts_hardness " << get_soft_contacts_hardness() << endl
	<< "anchors_hardness " << get_anchors_hardness() << endl
	<< "soft_rigid_hardness " << get_soft_rigid_hardness() << endl
	<< "soft_kinetic_hardness " << get_soft_kinetic_hardness() << endl
	<< "soft_soft_hardness " << get_soft_soft_hardness() << endl
	<< "soft_rigid_impulse_split " << get_soft_rigid_impulse_split() << endl
	<< "soft_kinetic_impulse_split " << get_soft_kinetic_impulse_split() << endl
	<< "soft_soft_impulse_split " << get_soft_soft_impulse_split() << endl
	<< "maximum_volume_ratio_pose " << get_maximum_volume_ratio_pose() << endl
	<< "time_scale " << get_time_scale() << endl
	<< "velocities_solver_iterations " << get_velocities_solver_iterations() << endl
	<< "positions_solver_iterations " << get_positions_solver_iterations() << endl
	<< "drift_solver_iterations " << get_drift_solver_iterations() << endl
	<< "cluster_solver_iterations " << get_cluster_solver_iterations() << endl
	<< "collisions_flags " << get_collisions_flags() << endl;
}

btSoftBody::Config BT3SoftBodyConfig::_dummy{};

///BT3SoftBodyNode definitions
/**
 * Writes the BT3SoftBodyNode into a datagram.
 */
void BT3SoftBodyNode::write_datagram(Datagram &dg) const
{
	get_position().write_datagram(dg);
	get_velocity().write_datagram(dg);
	bt3::btVector3_to_LVecBase3(_node.m_f).write_datagram(dg);
	get_normal().write_datagram(dg);
	dg.add_stdfloat(get_inv_mass());
	bt3::btVector3_to_LVecBase3(_node.m_q).write_datagram(dg);
	dg.add_stdfloat(get_area());
	dg.add_bool(get_attached());
	dg.add_uint32(_materialUid);
}

/**
 * Restores the BT3SoftBodyNode from the datagram.
 */
void BT3SoftBodyNode::read_datagram(DatagramIterator &scan)
{
	LVecBase3f value;
	value.read_datagram(scan);
	_node.m_x = bt3::LVecBase3_to_btVector3(value);
	value.read_datagram(scan);
	_node.m_v = bt3::LVecBase3_to_btVector3(value);
	value.read_datagram(scan);
	_node.m_f = bt3::LVecBase3_to_btVector3(value);
	value.read_datagram(scan);
	_node.m_n = bt3::LVecBase3_to_btVector3(value);
	_node.m_im = scan.get_stdfloat();
	value.read_datagram(scan);
	_node.m_q = bt3::LVecBase3_to_btVector3(value);
	_node.m_area = scan.get_stdfloat();
	_node.m_battach = (int) scan.get_bool();
	_materialUid = scan.get_uint32();
}

/**
 * Writes a sensible description of the BT3SoftBodyNode to the indicated
 * output stream.
 */
void BT3SoftBodyNode::output(ostream &out) const
{
	out
	<< "position " << get_position() << endl
	<< "velocity " << get_velocity() << endl
	<< "normal " << get_normal() << endl
	<< "inv_mass " << get_inv_mass() << endl
	<< "area " << get_area() << endl
	<< "attached " << get_attached() << endl;
}

btSoftBody::Node BT3SoftBodyNode::_dummy{};

///BT3SoftBodyAJointControl definitions
/**
 *
 */
void BT3SoftBodyAJointControl::Prepare(btSoftBody::AJoint* joint)
{
	switch (_type)
	{
	case STEER:
		if (btFabs(_sign) > 0.0)
		{
			joint->m_refs[0][0] = btCos(_angle * _sign);
			joint->m_refs[0][2] = btSin(_angle * _sign);
		}
		break;
	case MOTOR:
	default:
		btSoftBody::AJoint::IControl::Prepare(joint);
		break;
	}
}

/**
 *
 */
float BT3SoftBodyAJointControl::Speed(btSoftBody::AJoint *joint, float current)
{
	float speed;
	switch (_type)
	{
	case STEER:
	case MOTOR:
		speed = (current
				+ btMin(_maxtorque, btMax(-_maxtorque, _goal - current)));
		break;
	default:
		speed = btSoftBody::AJoint::IControl::Speed(joint, current);
		break;
	}
	return speed;
}

/**
 * Writes the BT3SoftBodyAJointControl into a datagram.
 */
void BT3SoftBodyAJointControl::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(_goal);
	dg.add_stdfloat(_maxtorque);
	dg.add_stdfloat(_angle);
	dg.add_stdfloat(_sign);
}

/**
 * Restores the BT3SoftBodyAJointControl from the datagram.
 */
void BT3SoftBodyAJointControl::read_datagram(DatagramIterator &scan)
{
	_goal = scan.get_stdfloat();
	_maxtorque = scan.get_stdfloat();
	_angle = scan.get_stdfloat();
	_sign = scan.get_stdfloat();
}

/**
 * Writes a sensible description of the BT3SoftBodyAJointControl to the indicated
 * output stream.
 */
void BT3SoftBodyAJointControl::output(ostream &out) const
{
	out
	<< "goal " << _goal << endl
	<< "maxtorque " << _maxtorque << endl
	<< "angle " << _angle << endl
	<< "sign " << _sign << endl;
}

///BT3RotationalLimitMotor definitions
/**
 * Writes the BT3RotationalLimitMotor into a datagram.
 */
void BT3RotationalLimitMotor::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_lower_limit());
	dg.add_stdfloat(get_upper_limit());
	dg.add_stdfloat(get_target_velocity());
	dg.add_stdfloat(get_max_motor_force());
	dg.add_stdfloat(get_max_limit_force());
	dg.add_stdfloat(get_damping());
	dg.add_stdfloat(get_limit_softness());
	dg.add_stdfloat(get_normal_cfm());
	dg.add_stdfloat(get_stop_erp());
	dg.add_stdfloat(get_stop_cfm());
	dg.add_stdfloat(get_bounce());
	dg.add_bool(get_enable_motor());
	dg.add_stdfloat(get_current_limit_error());
	dg.add_stdfloat(get_current_position());
	dg.add_int32(get_current_limit());
	dg.add_stdfloat(get_accumulated_impulse());
}

/**
 * Restores the BT3RotationalLimitMotor from the datagram.
 */
void BT3RotationalLimitMotor::read_datagram(DatagramIterator &scan)
{
	set_lower_limit(scan.get_stdfloat());
	set_upper_limit(scan.get_stdfloat());
	set_target_velocity(scan.get_stdfloat());
	set_max_motor_force(scan.get_stdfloat());
	set_max_limit_force(scan.get_stdfloat());
	set_damping(scan.get_stdfloat());
	set_limit_softness(scan.get_stdfloat());
	set_normal_cfm(scan.get_stdfloat());
	set_stop_erp(scan.get_stdfloat());
	set_stop_cfm(scan.get_stdfloat());
	set_bounce(scan.get_stdfloat());
	set_enable_motor(scan.get_bool());
	_motor.m_currentLimitError = scan.get_stdfloat();
	_motor.m_currentPosition = scan.get_stdfloat();
	_motor.m_currentLimit = scan.get_int32();
	_motor.m_accumulatedImpulse = scan.get_stdfloat();
}

/**
 * Writes a sensible description of the BT3RotationalLimitMotor to the indicated
 * output stream.
 */
void BT3RotationalLimitMotor::output(ostream &out) const
{
	out
	<< "low_limit " << get_lower_limit() << endl
	<< "high_limit " << get_upper_limit() << endl
	<< "target_velocity " << get_target_velocity() << endl
	<< "max_motor_force " << get_max_motor_force() << endl
	<< "max_limit_force " << get_max_limit_force() << endl
	<< "damping " << get_damping() << endl
	<< "limit_softness " << get_limit_softness() << endl
	<< "normal_cfm " << get_normal_cfm() << endl
	<< "stop_erp " << get_stop_erp() << endl
	<< "stop_cfm " << get_stop_cfm() << endl
	<< "bounce " << get_bounce() << endl
	<< "enable_motor " << get_enable_motor() << endl
	<< "current_limit_error " << get_current_limit_error() << endl
	<< "current_position " << get_current_position() << endl
	<< "current_limit " << get_current_limit() << endl
	<< "accumulated_impulse " << get_accumulated_impulse() << endl;
}

btRotationalLimitMotor BT3RotationalLimitMotor::_dummy{};

///BT3TranslationalLimitMotor definitions
/**
 * Writes the BT3TranslationalLimitMotor into a datagram.
 */
void BT3TranslationalLimitMotor::write_datagram(Datagram &dg) const
{
	get_lower_limit().write_datagram(dg);
	get_upper_limit().write_datagram(dg);
	dg.add_stdfloat(get_limit_softness());
	dg.add_stdfloat(get_damping());
	dg.add_stdfloat(get_restitution());
	get_normal_cfm().write_datagram(dg);
	get_stop_erp().write_datagram(dg);
	get_stop_cfm().write_datagram(dg);
	for (auto i = 0; i < 3; i++)
	{
		dg.add_bool(get_enable_motor(i));
	}
	get_target_velocity().write_datagram(dg);
	get_max_motor_force().write_datagram(dg);
	get_accumulated_impulse().write_datagram(dg);
	get_current_limit_error().write_datagram(dg);
	get_current_linear_diff().write_datagram(dg);
	for (auto i = 0; i < 3; i++)
	{
		dg.add_int8(get_current_limit(i));
	}
}

/**
 * Restores the BT3TranslationalLimitMotor from the datagram.
 */
void BT3TranslationalLimitMotor::read_datagram(DatagramIterator &scan)
{
	LVecBase3f triVal;
	triVal.read_datagram(scan);
	set_lower_limit(triVal);
	triVal.read_datagram(scan);
	set_upper_limit(triVal);
	set_limit_softness(scan.get_stdfloat());
	set_damping(scan.get_stdfloat());
	set_restitution(scan.get_stdfloat());
	triVal.read_datagram(scan);
	set_normal_cfm(triVal);
	triVal.read_datagram(scan);
	set_stop_erp(triVal);
	triVal.read_datagram(scan);
	set_stop_cfm(triVal);
	for (auto i = 0; i < 3; i++)
	{
		set_enable_motor(i, scan.get_bool());
	}
	triVal.read_datagram(scan);
	set_target_velocity(triVal);
	triVal.read_datagram(scan);
	set_max_motor_force(triVal);
	triVal.read_datagram(scan);
	_motor.m_accumulatedImpulse = bt3::LVecBase3_to_btVector3(triVal);
	triVal.read_datagram(scan);
	_motor.m_currentLimitError = bt3::LVecBase3_to_btVector3(triVal);
	triVal.read_datagram(scan);
	_motor.m_currentLinearDiff = bt3::LVecBase3_to_btVector3(triVal);
	for (auto i = 0; i < 3; i++)
	{
		_motor.m_currentLimit[bt3::p3dBt3AxisSwitch(i)] = scan.get_int8();
	}
}

/**
 * Writes a sensible description of the BT3TranslationalLimitMotor to the indicated
 * output stream.
 */
void BT3TranslationalLimitMotor::output(ostream &out) const
{
	out
	<< "lower_limit " << get_lower_limit() << endl
	<< "upper_limit " << get_upper_limit() << endl
	<< "limit_softness " << get_limit_softness() << endl
	<< "damping " << get_damping() << endl
	<< "restitution " << get_restitution() << endl
	<< "normal_cfm " << get_normal_cfm() << endl
	<< "stop_erp " << get_stop_erp() << endl
	<< "stop_cfm " << get_stop_cfm() << endl
	<< "enable_motor ";
	for (auto i = 0; i < 3; i++)
	{
		out << get_enable_motor(i) << ", ";
	}
	out << endl
	<< "target_velocity " << get_target_velocity() << endl
	<< "max_motor_force " << get_max_motor_force() << endl
	<< "accumulated_impulse " << get_accumulated_impulse() << endl
	<< "current_limit_error " << get_current_limit_error() << endl
	<< "current_linear_diff " << get_current_linear_diff() << endl
	<< "current_limit ";
	for (auto i = 0; i < 3; i++)
	{
		out << get_current_limit(i) << ", ";
	}
	out << endl;
}

btTranslationalLimitMotor BT3TranslationalLimitMotor::_dummy{};

///BT3RotationalLimitMotor2 definitions
/**
 * Writes the BT3RotationalLimitMotor2 into a datagram.
 */
void BT3RotationalLimitMotor2::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_lower_limit());
	dg.add_stdfloat(get_upper_limit());
	dg.add_stdfloat(get_target_velocity());
	dg.add_stdfloat(get_max_motor_force());
	dg.add_bool(get_servo_motor());
	dg.add_stdfloat(get_servo_target());
	dg.add_stdfloat(get_motor_erp());
	dg.add_stdfloat(get_motor_cfm());
	dg.add_stdfloat(get_stop_erp());
	dg.add_stdfloat(get_stop_cfm());
	dg.add_stdfloat(get_bounce());
	dg.add_bool(get_enable_motor());
	dg.add_bool(get_enable_spring());
	dg.add_stdfloat(get_spring_stiffness());
	dg.add_bool(get_spring_stiffness_limited());
	dg.add_stdfloat(get_spring_damping());
	dg.add_bool(get_spring_damping_limited());
	dg.add_bool(get_equilibrium_point());
	dg.add_stdfloat(get_current_limit_error());
	dg.add_stdfloat(get_current_position());
	dg.add_int32(get_current_limit());
	dg.add_stdfloat(get_current_limit_error_upper());
}

/**
 * Restores the BT3RotationalLimitMotor2 from the datagram.
 */
void BT3RotationalLimitMotor2::read_datagram(DatagramIterator &scan)
{
	set_lower_limit(scan.get_stdfloat());
	set_upper_limit(scan.get_stdfloat());
	set_target_velocity(scan.get_stdfloat());
	set_max_motor_force(scan.get_stdfloat());
	set_servo_motor(scan.get_bool());
	set_servo_target(scan.get_stdfloat());
	set_motor_erp(scan.get_stdfloat());
	set_motor_cfm(scan.get_stdfloat());
	set_stop_erp(scan.get_stdfloat());
	set_stop_cfm(scan.get_stdfloat());
	set_bounce(scan.get_stdfloat());
	set_enable_motor(scan.get_bool());
	set_enable_spring(scan.get_bool());
	set_spring_stiffness(scan.get_stdfloat());
	set_spring_stiffness_limited(scan.get_bool());
	set_spring_damping(scan.get_stdfloat());
	set_spring_damping_limited(scan.get_bool());
	set_equilibrium_point(scan.get_bool());
	_motor.m_currentLimitError = scan.get_stdfloat();
	_motor.m_currentPosition = scan.get_stdfloat();
	_motor.m_currentLimit = scan.get_int32();
	_motor.m_currentLimitErrorHi = scan.get_stdfloat();
}

/**
 * Writes a sensible description of the BT3RotationalLimitMotor2 to the indicated
 * output stream.
 */
void BT3RotationalLimitMotor2::output(ostream &out) const
{
	out
	<< "lower_limit " << get_lower_limit() << endl
	<< "upper_limit " << get_upper_limit() << endl
	<< "target_velocity " << get_target_velocity() << endl
	<< "max_motor_force " << get_max_motor_force() << endl
	<< "servo_motor " << get_servo_motor() << endl
	<< "servo_target " << get_servo_target() << endl
	<< "motor_erp " << get_motor_erp() << endl
	<< "motor_cfm " << get_motor_cfm() << endl
	<< "stop_erp " << get_stop_erp() << endl
	<< "stop_cfm " << get_stop_cfm() << endl
	<< "bounce " << get_bounce() << endl
	<< "enable_motor " << get_enable_motor() << endl
	<< "enable_spring " << get_enable_spring() << endl
	<< "spring_stiffness " << get_spring_stiffness() << endl
	<< "spring_stiffness_limited " << get_spring_stiffness_limited() << endl
	<< "spring_damping " << get_spring_damping() << endl
	<< "spring_damping_limited " << get_spring_damping_limited() << endl
	<< "equilibrium_point " << get_equilibrium_point() << endl
	<< "current_limit_error " << get_current_limit_error() << endl
	<< "current_position " << get_current_position() << endl
	<< "current_limit " << get_current_limit() << endl
	<< "current_limit_error_upper " << get_current_limit_error_upper() << endl;
}

btRotationalLimitMotor2 BT3RotationalLimitMotor2::_dummy{};

///BT3TranslationalLimitMotor2 definitions
/**
 * Writes the BT3TranslationalLimitMotor2 into a datagram.
 */
void BT3TranslationalLimitMotor2::write_datagram(Datagram &dg) const
{
	get_lower_limit().write_datagram(dg);
	get_upper_limit().write_datagram(dg);
	get_bounce().write_datagram(dg);
	get_equilibrium_point().write_datagram(dg);
	get_motor_erp().write_datagram(dg);
	get_motor_cfm().write_datagram(dg);
	get_stop_erp().write_datagram(dg);
	get_stop_cfm().write_datagram(dg);
	for (auto i = 0; i < 3; i++)
	{
		dg.add_bool(get_enable_motor(i));
	}
	get_target_velocity().write_datagram(dg);
	get_max_motor_force().write_datagram(dg);
	for (auto i = 0; i < 3; i++)
	{
		dg.add_bool(get_servo_motor(i));
	}
	get_servo_target().write_datagram(dg);
	for (auto i = 0; i < 3; i++)
	{
		dg.add_bool(get_enable_spring(i));
	}
	get_spring_stiffness().write_datagram(dg);
	for (auto i = 0; i < 3; i++)
	{
		dg.add_bool(get_spring_stiffness_limited(i));
	}
	get_spring_damping().write_datagram(dg);
	for (auto i = 0; i < 3; i++)
	{
		dg.add_bool(get_spring_damping_limited(i));
	}
	get_current_limit_error_upper().write_datagram(dg);
	get_current_limit_error().write_datagram(dg);
	get_current_linear_diff().write_datagram(dg);
	for (auto i = 0; i < 3; i++)
	{
		dg.add_int8(get_current_limit(i));
	}
}

/**
 * Restores the BT3TranslationalLimitMotor2 from the datagram.
 */
void BT3TranslationalLimitMotor2::read_datagram(DatagramIterator &scan)
{
	LVecBase3f triVal;
	triVal.read_datagram(scan);
	set_lower_limit(triVal);
	triVal.read_datagram(scan);
	set_upper_limit(triVal);
	triVal.read_datagram(scan);
	set_bounce(triVal);
	triVal.read_datagram(scan);
	set_equilibrium_point(triVal);
	triVal.read_datagram(scan);
	set_motor_erp(triVal);
	triVal.read_datagram(scan);
	set_motor_cfm(triVal);
	triVal.read_datagram(scan);
	set_stop_erp(triVal);
	triVal.read_datagram(scan);
	set_stop_cfm(triVal);
	for (auto i = 0; i < 3; i++)
	{
		set_enable_motor(i, scan.get_bool());
	}
	triVal.read_datagram(scan);
	set_target_velocity(triVal);
	triVal.read_datagram(scan);
	set_max_motor_force(triVal);
	for (auto i = 0; i < 3; i++)
	{
		set_servo_motor(i, scan.get_bool());
	}
	triVal.read_datagram(scan);
	set_servo_target(triVal);
	for (auto i = 0; i < 3; i++)
	{
		set_enable_spring(i, scan.get_bool());
	}
	triVal.read_datagram(scan);
	set_spring_stiffness(triVal);
	for (auto i = 0; i < 3; i++)
	{
		set_spring_stiffness_limited(i, scan.get_bool());
	}
	triVal.read_datagram(scan);
	set_spring_damping(triVal);
	for (auto i = 0; i < 3; i++)
	{
		set_spring_damping_limited(i, scan.get_bool());
	}
	triVal.read_datagram(scan);
	_motor.m_currentLimitErrorHi = bt3::LVecBase3_to_btVector3(triVal);
	triVal.read_datagram(scan);
	_motor.m_currentLimitError = bt3::LVecBase3_to_btVector3(triVal);
	triVal.read_datagram(scan);
	_motor.m_currentLinearDiff = bt3::LVecBase3_to_btVector3(triVal);
	for (auto i = 0; i < 3; i++)
	{
		_motor.m_currentLimit[bt3::p3dBt3AxisSwitch(i)] = scan.get_int8();
	}
}

/**
 * Writes a sensible description of the BT3TranslationalLimitMotor2 to the indicated
 * output stream.
 */
void BT3TranslationalLimitMotor2::output(ostream &out) const
{
	out
	<< "lower_limit " << get_lower_limit() << endl
	<< "upper_limit " << get_upper_limit() << endl
	<< "bounce " << get_bounce() << endl
	<< "equilibrium_point " << get_equilibrium_point() << endl
	<< "motor_erp " << get_motor_erp() << endl
	<< "motor_cfm " << get_motor_cfm() << endl
	<< "stop_erp " << get_stop_erp() << endl
	<< "stop_cfm " << get_stop_cfm() << endl
	<< "enable_motor " ;
	for (auto i = 0; i < 3; i++)
	{
		out << get_enable_motor(i) << ", ";
	}
	out << endl
	<< "target_velocity " << get_target_velocity() << endl
	<< "max_motor_force " << get_max_motor_force() << endl
	<< "servo_motor " ;
	for (auto i = 0; i < 3; i++)
	{
		out << get_servo_motor(i) << ", ";
	}
	out << endl
	<< "servo_target " << get_servo_target() << endl
	<< "enable_spring " ;
	for (auto i = 0; i < 3; i++)
	{
		out << get_enable_spring(i) << ", ";
	}
	out << endl
	<< "spring_stiffness " << get_spring_stiffness() << endl
	<< "spring_stiffness_limited " ;
	for (auto i = 0; i < 3; i++)
	{
		out << get_spring_stiffness_limited(i) << ", ";
	}
	out << endl
	<< "spring_damping " << get_spring_damping() << endl
	<< "spring_damping_limited " ;
	for (auto i = 0; i < 3; i++)
	{
		out << get_spring_damping_limited(i) << ", ";
	}
	out << endl
	<< "current_limit_error_upper " << get_current_limit_error_upper() << endl
	<< "current_limit_error " << get_current_limit_error() << endl
	<< "current_linear_diff " << get_current_linear_diff() << endl
	<< "current_limit " ;
	for (auto i = 0; i < 3; i++)
	{
		out << get_current_limit(i) << ", ";
	}
	out << endl;
}

btTranslationalLimitMotor2 BT3TranslationalLimitMotor2::_dummy{};

///BT3SoftBodyJointSpecs definitions
/**
 * Writes the BT3SoftBodyJointSpecs into a datagram.
 */
void BT3SoftBodyJointSpecs::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_erp());
	dg.add_stdfloat(get_cfm());
	dg.add_stdfloat(get_split());
}

/**
 * Restores the BT3SoftBodyJointSpecs from the datagram.
 */
void BT3SoftBodyJointSpecs::read_datagram(DatagramIterator &scan)
{
	set_erp(scan.get_stdfloat());
	set_cfm(scan.get_stdfloat());
	set_split(scan.get_stdfloat());
}

/**
 * Writes a sensible description of the BT3SoftBodyJointSpecs to the indicated
 * output stream.
 */
void BT3SoftBodyJointSpecs::output(ostream &out) const
{
	out
	<< "erp " << get_erp() << endl
	<< "cfm " << get_cfm() << endl
	<< "split " << get_split() << endl;
}

btSoftBody::Joint::Specs BT3SoftBodyJointSpecs::_dummy{};

///BT3SoftBodyLJointSpecs definitions
/**
 * Writes the BT3SoftBodyLJointSpecs into a datagram.
 */
void BT3SoftBodyLJointSpecs::write_datagram(Datagram &dg) const
{
	BT3SoftBodyJointSpecs::write_datagram(dg);
	get_position().write_datagram(dg);
}

/**
 * Restores the BT3SoftBodyLJointSpecs from the datagram.
 */
void BT3SoftBodyLJointSpecs::read_datagram(DatagramIterator &scan)
{
	BT3SoftBodyJointSpecs::read_datagram(scan);
	LVecBase3f triVal;
	triVal.read_datagram(scan);
	set_position(triVal);
}

/**
 * Writes a sensible description of the BT3SoftBodyLJointSpecs to the indicated
 * output stream.
 */
void BT3SoftBodyLJointSpecs::output(ostream &out) const
{
	BT3SoftBodyJointSpecs::output(out);
	out
	<< "position " << get_position() << endl;
}

///BT3SoftBodyAJointSpecs definitions
/**
 * Writes the BT3SoftBodyAJointSpecs into a datagram.
 */
void BT3SoftBodyAJointSpecs::write_datagram(Datagram &dg) const
{
	BT3SoftBodyJointSpecs::write_datagram(dg);
	get_axis().write_datagram(dg);
//	get_icontrol().write_datagram(dg);
}

/**
 * Restores the BT3SoftBodyAJointSpecs from the datagram.
 */
void BT3SoftBodyAJointSpecs::read_datagram(DatagramIterator &scan)
{
	BT3SoftBodyJointSpecs::read_datagram(scan);
	LVecBase3f triVal;
	triVal.read_datagram(scan);
	set_axis(triVal);
//	BT3SoftBodyAJointControl control;
//	control.read_datagram(scan);
//	set_icontrol(control);
}

/**
 * Writes a sensible description of the BT3SoftBodyAJointSpecs to the indicated
 * output stream.
 */
void BT3SoftBodyAJointSpecs::output(ostream &out) const
{
	BT3SoftBodyJointSpecs::output(out);
	out
	<< "axis " << get_axis() << endl
	<< "icontrol " << *get_icontrol() << endl;
}

BT3SoftBodyAJointControl BT3SoftBodyAJointSpecs::_dummy{};

///BT3VehicleTuning definitions
/**
 * Writes the BT3VehicleTuning into a datagram.
 */
void BT3VehicleTuning::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_suspension_stiffness());
	dg.add_stdfloat(get_suspension_compression());
	dg.add_stdfloat(get_suspension_damping());
	dg.add_stdfloat(get_max_suspension_travel_cm());
	dg.add_stdfloat(get_friction_slip());
	dg.add_stdfloat(get_max_suspension_force());
}

/**
 * Restores the BT3VehicleTuning from the datagram.
 */
void BT3VehicleTuning::read_datagram(DatagramIterator &scan)
{
	set_suspension_stiffness(scan.get_stdfloat());
	set_suspension_compression(scan.get_stdfloat());
	set_suspension_damping(scan.get_stdfloat());
	set_max_suspension_travel_cm(scan.get_stdfloat());
	set_friction_slip(scan.get_stdfloat());
	set_max_suspension_force(scan.get_stdfloat());
}

/**
 * Writes a sensible description of the BT3VehicleTuning to the indicated
 * output stream.
 */
void BT3VehicleTuning::output(ostream &out) const
{
	out
	<< "suspension_stiffness " << get_suspension_stiffness() << endl
	<< "suspension_compression " << get_suspension_compression() << endl
	<< "suspension_damping " << get_suspension_damping() << endl
	<< "max_suspension_travel_cm " << get_max_suspension_travel_cm() << endl
	<< "friction_slip " << get_friction_slip() << endl
	<< "max_suspension_force " << get_max_suspension_force() << endl;
}

///BT3WheelInfo definitions
/**
 * Writes the BT3WheelInfo into a datagram.
 */
void BT3WheelInfo::write_datagram(Datagram &dg) const
{
	dg.add_stdfloat(get_max_suspension_travel_cm());
	dg.add_stdfloat(get_suspension_stiffness());
	dg.add_stdfloat(get_wheels_damping_compression());
	dg.add_stdfloat(get_wheels_damping_relaxation());
	dg.add_stdfloat(get_friction_slip());
	dg.add_stdfloat(get_roll_influence());
	dg.add_stdfloat(get_max_suspension_force());
	get_chassis_connection_point_cs().write_datagram(dg);
	get_wheel_direction_cs().write_datagram(dg);
	get_wheel_axle_cs().write_datagram(dg);
	dg.add_stdfloat(get_suspension_rest_length());
	dg.add_stdfloat(get_wheels_radius());
	dg.add_bool(get_is_front_wheel());
	dg.add_stdfloat(get_steering());
	dg.add_stdfloat(get_rotation());
	dg.add_stdfloat(get_delta_rotation());
	dg.add_stdfloat(get_engine_force());
	dg.add_stdfloat(get_brake());
	//no raycast infos' serialization
}

/**
 * Restores the BT3WheelInfo from the datagram.
 */
void BT3WheelInfo::read_datagram(DatagramIterator &scan)
{
	set_max_suspension_travel_cm(scan.get_stdfloat());
	set_suspension_stiffness(scan.get_stdfloat());
	set_wheels_damping_compression(scan.get_stdfloat());
	set_wheels_damping_relaxation(scan.get_stdfloat());
	set_friction_slip(scan.get_stdfloat());
	set_roll_influence(scan.get_stdfloat());
	set_max_suspension_force(scan.get_stdfloat());
	LVecBase3f triVal;
	triVal.read_datagram(scan);
	_info.m_chassisConnectionPointCS = bt3::LVecBase3_to_btVector3(triVal);
	triVal.read_datagram(scan);
	_info.m_wheelDirectionCS = bt3::LVecBase3_to_btVector3(triVal);
	triVal.read_datagram(scan);
	_info.m_wheelAxleCS = bt3::LVecBase3_to_btVector3(triVal);
	_info.m_suspensionRestLength1 = scan.get_stdfloat();
	_info.m_wheelsRadius = scan.get_stdfloat();
	_info.m_bIsFrontWheel = scan.get_bool();
	_info.m_steering = scan.get_stdfloat();
	_info.m_rotation = scan.get_stdfloat();
	_info.m_deltaRotation = scan.get_stdfloat();
	_info.m_engineForce = scan.get_stdfloat();
	_info.m_brake = scan.get_stdfloat();
	//no raycast infos' serialization
}

/**
 * Writes a sensible description of the BT3WheelInfo to the indicated
 * output stream.
 */
void BT3WheelInfo::output(ostream &out) const
{
	out
	<< "max_suspension_travel_cm " << get_max_suspension_travel_cm() << endl
	<< "suspension_stiffness " << get_suspension_stiffness() << endl
	<< "wheels_damping_compression " << get_wheels_damping_compression() << endl
	<< "wheels_damping_relaxation " << get_wheels_damping_relaxation() << endl
	<< "friction_slip " << get_friction_slip() << endl
	<< "roll_influence " << get_roll_influence() << endl
	<< "max_suspension_force " << get_max_suspension_force() << endl
	<< "chassis_connection_point_cs " << get_chassis_connection_point_cs() << endl
	<< "wheel_direction_cs " << get_wheel_direction_cs() << endl
	<< "wheel_axle_cs " << get_wheel_axle_cs() << endl
	<< "suspension_rest_length " << get_suspension_rest_length() << endl
	<< "wheels_radius " << get_wheels_radius() << endl
	<< "is_front_wheel " << get_is_front_wheel() << endl
	<< "steering " << get_steering() << endl
	<< "rotation " << get_rotation() << endl
	<< "delta_rotation " << get_delta_rotation() << endl
	<< "engine_force " << get_engine_force() << endl
	<< "brake " << get_brake() << endl
	//raycast infos
	<< "raycast_contact_normal_ws " << get_raycast_contact_normal_ws() << endl
	<< "raycast_contact_point_ws " << get_raycast_contact_point_ws() << endl
	<< "raycast_hard_point_ws " << get_raycast_hard_point_ws() << endl
	<< "raycast_wheel_direction_ws " << get_raycast_wheel_direction_ws() << endl
	<< "raycast_wheel_axle_ws " << get_raycast_wheel_axle_ws() << endl
	<< "raycast_suspension_length " << get_raycast_suspension_length() << endl
	<< "raycast_is_in_contact " << get_raycast_is_in_contact() << endl
	<< "raycast_ground_object " << get_raycast_ground_object() << endl;
}

btWheelInfo BT3WheelInfo::_dummy{};

///BT3RayTestResult definitions
NodePath BT3RayTestResult::get_hit_object(int i) const
{
	RETURN_ON_COND((_type == NONE) || !get_has_hit(), NodePath())

	if (_type == BT3HitType::CLOSEST)
	{
		ClosestResultType& result = static_cast<ClosestResultType&>(*_result);
		RETURN_ON_COND(i != 0, NodePath())

		return bt3::getNodeFromPhysicsObject(result.m_collisionObject);
	}
	else if (_type == BT3HitType::ALL)
	{
		AllResultsType& result = static_cast<AllResultsType&>(*_result);
		RETURN_ON_COND((i < 0) || (i >= result.m_collisionObjects.size()),
				NodePath())

		return bt3::getNodeFromPhysicsObject(result.m_collisionObjects[i]);
	}
	return NodePath();
}
LPoint3f BT3RayTestResult::get_hit_point(int i) const
{
	RETURN_ON_COND((_type == NONE) || !get_has_hit(), LPoint3f())

	if (_type == BT3HitType::CLOSEST)
	{
		ClosestResultType& result = static_cast<ClosestResultType&>(*_result);
		RETURN_ON_COND(i != 0, LPoint3f())

		return bt3::btVector3_to_LVecBase3(result.m_hitPointWorld);
	}
	else if (_type == BT3HitType::ALL)
	{
		AllResultsType& result = static_cast<AllResultsType&>(*_result);
		RETURN_ON_COND((i < 0) || (i >= result.m_hitPointWorld.size()),
				LPoint3f())

		return bt3::btVector3_to_LVecBase3(result.m_hitPointWorld[i]);
	}
	return LPoint3f();
}
LVector3f BT3RayTestResult::get_hit_normal(int i) const
{
	RETURN_ON_COND((_type == NONE) || !get_has_hit(), LVector3f())

	if (_type == BT3HitType::CLOSEST)
	{
		ClosestResultType& result = static_cast<ClosestResultType&>(*_result);
		RETURN_ON_COND(i != 0, LVector3f())

		return bt3::btVector3_to_LVecBase3(result.m_hitNormalWorld);
	}
	else if (_type == BT3HitType::ALL)
	{
		AllResultsType& result = static_cast<AllResultsType&>(*_result);
		RETURN_ON_COND((i < 0) || (i >= result.m_hitNormalWorld.size()),
				LVector3f())

		return bt3::btVector3_to_LVecBase3(result.m_hitNormalWorld[i]);
	}
	return LVector3f();
}
float BT3RayTestResult::get_hit_fraction(int i) const
{
	RETURN_ON_COND((_type == NONE) || !get_has_hit(), RESULT_ERROR)

	if (_type == BT3HitType::CLOSEST)
	{
		ClosestResultType& result = static_cast<ClosestResultType&>(*_result);
		RETURN_ON_COND(i != 0, RESULT_ERROR)

		return result.m_closestHitFraction;
	}
	else if (_type == BT3HitType::ALL)
	{
		AllResultsType& result = static_cast<AllResultsType&>(*_result);
		RETURN_ON_COND((i < 0) || (i >= result.m_hitFractions.size()),
				RESULT_ERROR)

		return result.m_hitFractions[i];
	}
	return RESULT_ERROR;
}

///BT3ConvexTestResult definitions
NodePath BT3ConvexTestResult::get_hit_object(int i) const
{
	RETURN_ON_COND((_type == NONE) || !get_has_hit(), NodePath())

	if (_type == BT3HitType::CLOSEST)
	{
		ClosestResultType& result = static_cast<ClosestResultType&>(*_result);
		RETURN_ON_COND(i != 0, NodePath())

		return bt3::getNodeFromPhysicsObject(result.m_hitCollisionObject);
	}
	return NodePath();
}
LPoint3f BT3ConvexTestResult::get_hit_point(int i) const
{
	RETURN_ON_COND((_type == NONE) || !get_has_hit(), LPoint3f())

	if (_type == BT3HitType::CLOSEST)
	{
		ClosestResultType& result = static_cast<ClosestResultType&>(*_result);
		RETURN_ON_COND(i != 0, LPoint3f())

		return bt3::btVector3_to_LVecBase3(result.m_hitPointWorld);
	}
	return LPoint3f();
}
LVector3f BT3ConvexTestResult::get_hit_normal(int i) const
{
	RETURN_ON_COND((_type == NONE) || !get_has_hit(), LVector3f())

	if (_type == BT3HitType::CLOSEST)
	{
		ClosestResultType& result = static_cast<ClosestResultType&>(*_result);
		RETURN_ON_COND(i != 0, LVector3f())

		return bt3::btVector3_to_LVecBase3(result.m_hitNormalWorld);
	}
	return LVector3f();
}
float BT3ConvexTestResult::get_hit_fraction(int i) const
{
	RETURN_ON_COND((_type == NONE) || !get_has_hit(), RESULT_ERROR)

	if (_type == BT3HitType::CLOSEST)
	{
		ClosestResultType& result = static_cast<ClosestResultType&>(*_result);
		RETURN_ON_COND(i != 0, RESULT_ERROR)

		return result.m_closestHitFraction;
	}
	return RESULT_ERROR;
}

///BT3ContactTestResult && BT3ContactPoint definitions
lwContactPoint BT3ContactPoint::_dummy{};
///
BT3ContactPoint BT3ContactTestResult::get_contact_point(int i) const
{
	RETURN_ON_COND(_type == NONE, BT3ContactPoint::_dummy)

	if (_type == BT3ContactType::SINGLE)
	{
		SingleResultType& result = static_cast<SingleResultType&>(*_result);
		RETURN_ON_COND((i < 0) || (i >= result.m_numContacts),
				BT3ContactPoint::_dummy)

		return result.m_pointsOut[i];
	}
	else if (_type == BT3ContactType::PAIR)
	{
		PairResultType& result = static_cast<PairResultType&>(*_result);
		RETURN_ON_COND((i < 0) || (i >= result.m_numContacts),
				BT3ContactPoint::_dummy)

		return result.m_pointsOut[i];
	}
	return BT3ContactPoint::_dummy;
}
ValueList_NodePath BT3ContactTestResult::get_contact_objects(int i) const
{
	ValueList_NodePath nodeList;
	RETURN_ON_COND(_type == NONE, nodeList)

	if (_type == BT3ContactType::SINGLE)
	{
		SingleResultType& result = static_cast<SingleResultType&>(*_result);
		RETURN_ON_COND((i < 0) || (i >= result.m_numContacts), nodeList)

		NodePath nodeA = bt3::getNodeFromPhysicsObject(
				result.m_objectsPerPointIdx[i].first);
		NodePath nodeB = bt3::getNodeFromPhysicsObject(
				result.m_objectsPerPointIdx[i].second);
		nodeList.add_value(nodeA);
		nodeList.add_value(nodeB);
	}
	else if (_type == BT3ContactType::PAIR)
	{
		///Note: contact objects are input parameters
	}
	return nodeList;
}

