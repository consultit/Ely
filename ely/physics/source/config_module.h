#ifndef PHYSICS_SOURCE_CONFIG_PHYSICS_H_
#define PHYSICS_SOURCE_CONFIG_PHYSICS_H_

#pragma once

#include "physicssymbols.h"
#include "pandabase.h"
#include "notifyCategoryProxy.h"
#include "configVariableDouble.h"
#include "configVariableString.h"
#include "configVariableInt.h"


NotifyCategoryDecl(p3physics, EXPCL_PHYSICS, EXPTP_PHYSICS);

extern void init_libp3physics();

#endif //PHYSICS_SOURCE_CONFIG_PHYSICS_H_
