/**
 * \file bullet3_composite4.cxx
 *
 * \date 2017-03-06
 * \author consultit
 */

///library

#ifdef OPENCL_FOUND
#include "bullet3/src/Bullet3OpenCL/RigidBody/b3GpuRigidBodyPipeline.cpp"
#endif // OPENCL_FOUND

// Extras
#include "bullet3/Extras/GIMPACTUtils/btGImpactConvexDecompositionShape.cpp"
#include "bullet3/Extras/HACD/hacdGraph.cpp"
#include "bullet3/Extras/HACD/hacdHACD.cpp"
#include "bullet3/Extras/HACD/hacdICHull.cpp"
#include "bullet3/Extras/HACD/hacdManifoldMesh.cpp"
#include "bullet3/Extras/InverseDynamics/RandomTreeCreator.cpp"
#include "bullet3/Extras/InverseDynamics/btMultiBodyTreeCreator.cpp"
#include "bullet3/Extras/InverseDynamics/DillCreator.cpp"
#include "bullet3/Extras/InverseDynamics/MultiBodyTreeDebugGraph.cpp"
#include "bullet3/Extras/InverseDynamics/MultiBodyTreeCreator.cpp"
#include "bullet3/Extras/InverseDynamics/CoilCreator.cpp"
#include "bullet3/Extras/InverseDynamics/MultiBodyNameMap.cpp"
#include "bullet3/Extras/InverseDynamics/User2InternalIndex.cpp"
#include "bullet3/Extras/InverseDynamics/SimpleTreeCreator.cpp"
#include "bullet3/Extras/InverseDynamics/IDRandomUtil.cpp"
#include "bullet3/Extras/InverseDynamics/CloneTreeCreator.cpp"
///#include "bullet3/Extras/obj2sdf/obj2sdf.cpp"
///#include "bullet3/Extras/Serialize/BulletFileLoader/bChunk.cpp"
///#include "bullet3/Extras/Serialize/BulletFileLoader/bDNA.cpp"
///#include "bullet3/Extras/Serialize/BulletFileLoader/bFile.cpp"
///#include "bullet3/Extras/Serialize/BulletFileLoader/btBulletFile.cpp"
///#include "bullet3/Extras/Serialize/BulletWorldImporter/btBulletWorldImporter.cpp"
///#include "bullet3/Extras/Serialize/BulletWorldImporter/btMultiBodyWorldImporter.cpp"
///#include "bullet3/Extras/Serialize/BulletWorldImporter/btWorldImporter.cpp"
///#include "bullet3/Extras/Serialize/BulletXmlWorldImporter/btBulletXmlWorldImporter.cpp"
///#include "bullet3/Extras/Serialize/BlenderSerialize/bBlenderFile.cpp"
///#include "bullet3/Extras/Serialize/BlenderSerialize/bMain.cpp"
///#include "bullet3/Extras/Serialize/BlenderSerialize/dna249-64bit.cpp"
///#include "bullet3/Extras/Serialize/BlenderSerialize/dna249.cpp"
///#include "bullet3/Extras/Serialize/BulletXmlWorldImporter/string_split.cpp"
///#include "bullet3/Extras/Serialize/HeaderGenerator/apiGen.cpp"
///#include "bullet3/Extras/Serialize/makesdna/makesdna.cpp"
///#include "bullet3/Extras/Serialize/ReadBulletSample/BulletDataExtractor.cpp"
///#include "bullet3/Extras/Serialize/ReadBulletSample/main.cpp"
///#include "bullet3/Extras/VHACD/src/btAlignedAllocator.cpp"
///#include "bullet3/Extras/VHACD/src/btConvexHullComputer.cpp"
///#include "bullet3/Extras/VHACD/src/VHACD.cpp"
///#include "bullet3/Extras/VHACD/src/vhacdICHull.cpp"
///#include "bullet3/Extras/VHACD/src/vhacdManifoldMesh.cpp"
///#include "bullet3/Extras/VHACD/src/vhacdMesh.cpp"
///#include "bullet3/Extras/VHACD/src/vhacdVolume.cpp"
///#include "bullet3/Extras/VHACD/test/src/main.cpp"
///#include "bullet3/Extras/VHACD/test/src/oclHelper.cpp"

// BulletDynamics
#include "bullet3/src/BulletDynamics/Featherstone/btMultiBodySliderConstraint.cpp"

// BulletCollision
#include "bullet3/src/BulletCollision/NarrowPhaseCollision/btPolyhedralContactClipping.cpp"
#include "bullet3/src/BulletCollision/NarrowPhaseCollision/btContinuousConvexCollision.cpp"
