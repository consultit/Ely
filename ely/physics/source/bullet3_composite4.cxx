/**
 * \file bullet3_composite4.cxx
 *
 * \date 2017-03-06
 * \author consultit
 */

///library

#ifdef OPENCL_FOUND
#include "bullet3/src/Bullet3OpenCL/Initialize/b3OpenCLUtils.cpp"
#endif //OPENCL_FOUND

// BulletDynamics
#include "bullet3/src/BulletDynamics/Dynamics/btSimulationIslandManagerMt.cpp"

// BulletSoftBody
#include "bullet3/src/BulletSoftBody/btSoftMultiBodyDynamicsWorld.cpp"
#include "bullet3/src/BulletSoftBody/btDeformableContactProjection.cpp"
#include "bullet3/src/BulletSoftBody/btDeformableMultiBodyConstraintSolver.cpp"
#include "bullet3/src/BulletSoftBody/btDeformableMultiBodyDynamicsWorld.cpp"
#include "bullet3/src/BulletSoftBody/btDeformableBackwardEulerObjective.cpp"
#include "bullet3/src/BulletSoftBody/btDeformableBodySolver.cpp"

// Extras
#include "bullet3/Extras/ConvexDecomposition/ConvexDecomposition.cpp"
#include "bullet3/Extras/ConvexDecomposition/vlookup.cpp"
#include "bullet3/Extras/ConvexDecomposition/fitsphere.cpp"
#include "bullet3/Extras/ConvexDecomposition/cd_wavefront.cpp"
#include "bullet3/Extras/ConvexDecomposition/cd_hull.cpp"
#include "bullet3/Extras/ConvexDecomposition/bestfit.cpp"
#include "bullet3/Extras/ConvexDecomposition/ConvexBuilder.cpp"
#include "bullet3/Extras/ConvexDecomposition/planetri.cpp"
#include "bullet3/Extras/ConvexDecomposition/raytri.cpp"
#include "bullet3/Extras/ConvexDecomposition/bestfitobb.cpp"
#include "bullet3/Extras/ConvexDecomposition/meshvolume.cpp"
#include "bullet3/Extras/ConvexDecomposition/splitplane.cpp"
#include "bullet3/Extras/ConvexDecomposition/concavity.cpp"
#include "bullet3/Extras/ConvexDecomposition/float_math.cpp"

// v-hacd
#include "v-hacd/src/VHACD_Lib/src/vhacdVolume.cpp"
#include "v-hacd/src/VHACD_Lib/src/VHACD-ASYNC.cpp"
