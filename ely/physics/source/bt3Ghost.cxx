/**
 * \file bt3Ghost.cxx
 *
 * \date 2017-03-17
 * \author consultit
 */

#if defined(_WIN32)
#include "support_os/pstdint.h"
#endif

#include "bt3Ghost.h"
#include "transformState.h"
#include "throw_event.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
extern struct Dtool_PyTypedObject Dtool_BT3Ghost;
extern struct Dtool_PyTypedObject Dtool_CollisionObjectAttrs;
#endif //PYTHON_BUILD

///BT3Ghost definitions
/**
 *
 */
BT3Ghost::BT3Ghost(const string& name) :
		PandaNode(name)
{
	do_reset();
}

/**
 *
 */
BT3Ghost::~BT3Ghost()
{
}

/**
 * Initializes the BT3Ghost with starting settings.
 * \note Internal use only.
 */
void BT3Ghost::do_initialize()
{
	GamePhysicsManager* mTmpl = GamePhysicsManager::get_global_ptr();
	// visible
	string visible = mTmpl->get_parameter_value(
			GamePhysicsManager::GHOST, string("visible"));
	set_visible(visible == string("true") ? true : false);
	// shape type
	string shapeType = mTmpl->get_parameter_value(
			GamePhysicsManager::GHOST, string("shape_type"));
	mShapeType = do_get_shape_type(shapeType);
	// child shape type
	string childShapeType = mTmpl->get_parameter_value(
			GamePhysicsManager::GHOST, string("child_shape_type"));
	mChildShapeType = do_get_shape_type(childShapeType);
	// use shape of (another object)
	mUseShapeOfId = mTmpl->get_parameter_value(GamePhysicsManager::GHOST,
			string("use_shape_of"));
	// up axis
	string upAxis = mTmpl->get_parameter_value(
			GamePhysicsManager::GHOST, string("up_axis"));
	if (upAxis == string("x"))
	{
		mUpAxis = GamePhysicsManager::X_up;
	}
	else if (upAxis == string("y"))
	{
		mUpAxis = GamePhysicsManager::Y_up;
	}
	else if (upAxis == string("z"))
	{
		mUpAxis = GamePhysicsManager::Z_up;
	}
	else if (upAxis == string("no_up"))
	{
		mUpAxis = GamePhysicsManager::NO_up;
	}
	//group mask
	string groupMask = mTmpl->get_parameter_value(
			GamePhysicsManager::GHOST, string("group_mask"));
	mGroupMask = do_get_mask(groupMask);
	//collide mask
	string collideMask = mTmpl->get_parameter_value(
			GamePhysicsManager::GHOST, string("collide_mask"));
	mCollideMask = do_get_mask(collideMask);
	// set thrown events
	string thrownEvents =  mTmpl->get_parameter_value(
			GamePhysicsManager::GHOST, string("thrown_events"));
	unsigned int idx1, valueNum1;
	pvector<string> paramValuesStr1, paramValuesStr2;
	//events specified
	//event1@[event_name1]@[frequency1][:...[:eventN@[event_nameN]@[frequencyN]]]
	paramValuesStr1 = parseCompoundString(thrownEvents, ':');
	valueNum1 = paramValuesStr1.size();
	for (idx1 = 0; idx1 < valueNum1; ++idx1)
	{
		//eventX@[event_nameX]@[frequencyX]
		paramValuesStr2 = parseCompoundString(paramValuesStr1[idx1], '@');
		if (paramValuesStr2.size() >= 3)
		{
			// get event
			BT3EventThrown event;
			// get event name
			string name = paramValuesStr2[1];
			// get frequency
			float frequency = abs(STRTOF(paramValuesStr2[2].c_str(), nullptr));
			//get event
			if (paramValuesStr2[0] == "overlap")
			{
				event = OVERLAP;
			}
			else
			{
				//paramValuesStr2[0] is not a suitable event:
				//continue with the next event
				continue;
			}
			//enable the event
			enable_throw_event(event, true, frequency, name);
		}
	}
	// kinematic
	mKinematic = (mTmpl->get_parameter_value(GamePhysicsManager::GHOST,
					string("kinematic")) == string("false") ? false : true);
	// object
	string object = mTmpl->get_parameter_value(
			GamePhysicsManager::GHOST, string("object"));
	if(!object.empty())
	{
		// search object under reference node
		NodePath objectNP = mReferenceNP.find(string("**/") + object);
		if (!objectNP.is_empty())
		{
			mObjectNP = objectNP;
			//shape data fixme
			if (mShapeType == GamePhysicsManager::BT3ShapeType::TERRAIN)
			{
				PRINT_DEBUG("Currently BT3Ghost with a "
						"GamePhysicsManager::BT3ShapeType::TERRAIN shape,"
						"cannot be built automatically");
				mShapeType = GamePhysicsManager::BT3ShapeType::PLANE;
			}
			setup();
		}
	}
}

/**
 * Sets the BT3Ghost up for the given object (if not empty), given the
 * shape's type and size (among others).
 */
int BT3Ghost::setup()
{
	// continue if ghost has not been setup yet
	CONTINUE_IF_ELSE_R(!mSetup, RESULT_SUCCESS)

	// continue if object not empty
	CONTINUE_IF_ELSE_R(!mObjectNP.is_empty(), RESULT_ERROR)

	// get this node path
	NodePath thisNP = NodePath::any_path(this);
	bt3::Physics& physics =
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics();
	// when not de-serializing: correct the transforms and reparent
	if (!mBuildFromBam)
	{
		// move the transform of the object to this node path
		thisNP.set_transform(mObjectNP.node()->get_transform());
		mObjectNP.set_transform(TransformState::make_identity());
		// reparent the object node path to this
		mObjectNP.reparent_to(thisNP);
	}
	// shared shape
	btCollisionShape* sharedShape = nullptr;
	if (!mUseShapeOfId.empty())
	{
		NodePath sharedShapeNP = mReferenceNP.find(
				string("**/") + mUseShapeOfId);
		if (!sharedShapeNP.is_empty())
		{
			btGhostObject* ghost = btGhostObject::upcast(
					getPhysicsObjectOfNode(sharedShapeNP, physics));
			if (ghost != nullptr)
			{
				sharedShape = ghost->getCollisionShape();
			}
			else
			{
				btRigidBody* body = btRigidBody::upcast(
						getPhysicsObjectOfNode(sharedShapeNP, physics));
				if (body != nullptr)
				{
					sharedShape = body->getCollisionShape();
				}
			}
		}
	}
	// add a ghost
	bt3::addGhostToNode(thisNP, physics, get_visible(), sharedShape,
			(BroadphaseNativeTypes) mShapeType, (bt3::Panda3dUpAxis) mUpAxis,
			mGroupMask, mCollideMask, mUserData,
			(BroadphaseNativeTypes) mChildShapeType);
	// check for errors
	mGhost = btGhostObject::upcast(
					bt3::getPhysicsObjectOfNode(thisNP,
							*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics()));
	RETURN_ON_COND(!mGhost, RESULT_ERROR)

	// add a motion state
	mMotionState = bt3::makeMotionState(thisNP);

	// create collision object's attrs
	mCollisionObjectAttrs = do_create_collision_object_attrs();
	mCollisionObjectAttrs->set_collision_object(mGhost);
#ifdef PYTHON_BUILD
	mCollisionObjectAttrsPy = do_create_collision_object_attrs_py();
#endif //PYTHON_BUILD
	// set the flag
	mSetup = true;
	//
	return RESULT_SUCCESS;
}

/**
 * On destruction cleanup.
 * Gives an BT3Ghost the ability to do any cleaning is necessary when
 * destroyed.
 * \note Internal use only.
 */
void BT3Ghost::do_finalize()
{
	//cleanup (if needed)
	cleanup();

	//remove this NodePath
	NodePath::any_path(this).remove_node();
	//
#ifdef PYTHON_BUILD
	//Python callback
	PY_SELF_ARG_DECREF(mUpdateCallback, mSelf);
#endif //PYTHON_BUILD
	do_reset();
	//
	return;
}

/**
 * Cleans the BT3Ghost up, detaching any child objects (if not empty).
 */
int BT3Ghost::cleanup()
{
	// continue if ghost has been already setup
	CONTINUE_IF_ELSE_R(mSetup, RESULT_SUCCESS)

	int result = RESULT_ERROR;
	// get this node path
	NodePath thisNP = NodePath::any_path(this);
	// remove the ghost
	if (bt3::removeGhostFromNode(thisNP,
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics()))
	{
		// move the transform of this node path to the object
		mObjectNP.set_transform(thisNP.node()->get_transform());
		thisNP.set_transform(TransformState::make_identity());
//		// TRIANGLE_MESH preserved its transform fixme
//		if (mShapeType != GamePhysicsManager::TRIANGLE_MESH)
//		{
//			// restore child's transform to the
//			// current ghost's one
//			childNP.set_transform(get_transform());
//		}
		// detach object
		mObjectNP.detach_node();
		// reset user data pointer
		if (mUserData && (mShapeType == GamePhysicsManager::BT3ShapeType::TERRAIN))
		{
			delete static_cast<PNMImage*>(mUserData);
		}
		mUserData = nullptr;
#ifdef PYTHON_BUILD
		Py_XDECREF(mCollisionObjectAttrsPy);
		mCollisionObjectAttrsPy = nullptr;
#endif //PYTHON_BUILD
		// destroy collision object's attrs
		delete mCollisionObjectAttrs;
		mCollisionObjectAttrs = nullptr;
		// reset bullet ghost reference
		mGhost = nullptr;
		// remove the motion state
		delete mMotionState;
		// set the flag
		mSetup = false;
		result = RESULT_SUCCESS;
	}
	//
	return result;
}

/**
 * Returns the shape type given the name.
 * \note Internal use only.
 */
GamePhysicsManager::BT3ShapeType BT3Ghost::do_get_shape_type(
		const string& name) const
{
	GamePhysicsManager::BT3ShapeType shapeType;
	if (name == string("plane"))
	{
		shapeType = GamePhysicsManager::PLANE;
	}
	else if (name == string("box"))
	{
		shapeType = GamePhysicsManager::BOX;
	}
	else if (name == string("sphere"))
	{
		shapeType = GamePhysicsManager::SPHERE;
	}
	else if (name == string("capsule"))
	{
		shapeType = GamePhysicsManager::CAPSULE;
	}
	else if (name == string("cylinder"))
	{
		shapeType = GamePhysicsManager::CYLINDER;
	}
	else if (name == string("cone"))
	{
		shapeType = GamePhysicsManager::CONE;
	}
	else if (name == string("multi_sphere"))
	{
		shapeType = GamePhysicsManager::MULTI_SPHERE;
	}
	else if (name == string("convex_hull"))
	{
		shapeType = GamePhysicsManager::CONVEX_HULL;
	}
	else if (name == string("gimpact"))
	{
		shapeType = GamePhysicsManager::GIMPACT;
	}
	else if (name == string("triangle_mesh"))
	{
		shapeType = GamePhysicsManager::TRIANGLE_MESH;
	}
	else if (name == string("terrain"))
	{
		shapeType = GamePhysicsManager::TERRAIN;
	}
	else if (name == string("compound"))
	{
		shapeType = GamePhysicsManager::COMPOUND;
	}
	return shapeType;
}

/**
 * Returns the mask given the name.
 * \note Internal use only.
 */
BitMask32 BT3Ghost::do_get_mask(const string& name) const
{
	BitMask32 mask;
	if (name == string("all_on"))
	{
		mask = BitMask32::all_on();
	}
	else if (name == string("all_off"))
	{
		mask = BitMask32::all_off();
	}
	else
	{
		mask.set_word((uint32_t) strtol(name.c_str(), nullptr, 0));
	}
#ifdef ELY_DEBUG
		mask.write(cout, 0);
#endif //ELY_DEBUG
	return mask;
}

/**
 * Creates collision object attrs class.
 * \note Internal use only.
 */
CollisionObjectAttrs* BT3Ghost::do_create_collision_object_attrs()
{
	CollisionObjectAttrs* attrs = nullptr;
	attrs = new CollisionObjectAttrs();
	return attrs;
}
#if defined(PYTHON_BUILD)
PyObject *BT3Ghost::do_create_collision_object_attrs_py()
{
	PyObject *attrs = nullptr;
	attrs = DTool_CreatePyInstance((void *)mCollisionObjectAttrs,
			Dtool_CollisionObjectAttrs, false, false);
	return attrs;
}
#endif //PYTHON_BUILD

/**
 * Enables/disables ghost notifications through events.
 * - *Overlap notification*:
 * for each physic PandaNode that are overlapping the
 * "<nodeName>_<ghostName>_Overlap" (or "eventName" if eventName is not empty)
 * event is emitted at the given frequency; for each physic PandaNodes that stop
 * overlapping the "<nodeName>_<ghostName1>_OverlapOff" (or "eventNameOff" if
 * eventName is not empty) event is emitted once; both events have as arguments
 * the PandaNode and the BT3Ghost.
 */
void BT3Ghost::enable_throw_event(BT3EventThrown event, bool enable,
		float frequency, const string& eventName)
{
	ThrowEventData eventData(eventName,	abs(frequency));
	eventData.mEnable = enable;
	switch (event)
	{
	case OVERLAP:
		mOverlap = eventData;
		break;
	default:
		break;
	}
}

/**
 * Updates the BT3Ghost state.
 */
void BT3Ghost::update(float dt)
{
	RETURN_ON_COND(!mSetup,)

#ifdef TESTING
	dt = 0.016666667; //60 fps
#endif

	if (mKinematic)
	{
		// updating the inner btGhostObject's transformation from this BT3Ghost
		mMotionState->getWorldTransform(mGhost->getWorldTransform());
	}

	/// Handle events
	// Overlap notifications
	if (mOverlap.mEnable)
	{
		//update general count:
		//only actual overlapping objects have their count updated,
		//while just gone out objects will be erased from the set
		++mOverlap.mCount;

		//elaborate current overlapping object list
		if (mGhost->getNumOverlappingObjects() > 0)
		{
			//update elapsed time
			mOverlap.mTimeElapsed += ClockObject::get_global_clock()->get_dt();
			for (int i = 0; i < mGhost->getNumOverlappingObjects(); ++i)
			{
				PandaNode* node =
						reinterpret_cast<PandaNode*>(mGhost->getOverlappingObject(i)->getUserPointer());
				//insert a default: check of equality is done only on OverlapNodeData::mPnode member
				pair<pset<OverlappingNode>::iterator, bool> res =
				mOverlappingNodes.insert(OverlappingNode(node));
				if (res.second)
				{
					// this is a "new" overlapping object
					// set event name: <nodeName>_<ghostName>_Overlap or mOverlap.mEventName
					(res.first)->mOverlappingNodeData->mEventName =
						(mOverlap.mEventName.empty()?
						node->get_name() + "_" + get_name() + "_Overlap"
						:
						mOverlap.mEventName);
					// throw the event
					throw_event((res.first)->mOverlappingNodeData->mEventName,
							EventParameter(node), EventParameter(this));
				}
				else
				{
					// this is an "old" overlapping object
					if (mOverlap.mTimeElapsed >= mOverlap.mPeriod)
					{
						// throw the event
						throw_event((res.first)->mOverlappingNodeData->mEventName,
								EventParameter(node), EventParameter(this));
					}
				}
				//update count flag
				(res.first)->mOverlappingNodeData->mCount = mOverlap.mCount;
			}
			//update elapsed time
			if (mOverlap.mTimeElapsed >= mOverlap.mPeriod)
			{
				mOverlap.mTimeElapsed -= mOverlap.mPeriod;
			}
		}
		else
		{
			mOverlap.mTimeElapsed = 0.0;
		}

		//erase gone "out" objects (which have not the count flag updated)
		for (set<OverlappingNode>::iterator iter = mOverlappingNodes.begin(); iter != mOverlappingNodes.end();)
		{
			//check if it has a previous count
			if (iter->mOverlappingNodeData->mCount != (int)mOverlap.mCount)
			{
				//throw the "off" event
				//event name: <nodeName>_<ghostName>_OverlapOff
				throw_event(iter->mOverlappingNodeData->mEventName + "Off",
						EventParameter(iter->mOverlappingNodeData->mPnode), EventParameter(this));
				//erase the object
				mOverlappingNodes.erase(iter++);
			}
			else
			{
				++iter;
			}
		}
	}
	//
	if (mUpdateCallback)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_CALL(mUpdateCallback, mSelf,
				get_name() + " BT3Ghost::update()");
#else
		// call c++ callback
		mUpdateCallback(this);
#endif //PYTHON_BUILD
	}
}

/**
 * Writes a sensible description of the BT3Ghost to the indicated output
 * stream.
 */
void BT3Ghost::output(ostream &out) const
{
	out << get_type() << " " << get_name();
}

#ifdef PYTHON_BUILD
/**
 * Sets the update callback as a python function taking this BT3Ghost as
 * an argument, or None. On error raises an python exception.
 * \note Python only.
 */
void BT3Ghost::set_update_callback(PyObject *clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mUpdateCallback, clbk, Dtool_BT3Ghost,
			get_name() + " BT3Ghost.set_update_callback()", mSelf, this);
}
#else
/**
 * Sets the update callback as a c++ function taking this BT3Ghost as
 * an argument, or nullptr.
 * \note C++ only.
 */
void BT3Ghost::set_update_callback(UPDATECALLBACKFUNC value)
{
	mUpdateCallback = value;
}
#endif //PYTHON_BUILD

//TypedWritable API
/**
 * Tells the BamReader how to create objects of type BT3Ghost.
 */
void BT3Ghost::register_with_read_factory()
{
	BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

/**
 * Writes the contents of this object to the datagram for shipping out to a
 * Bam file.
 */
void BT3Ghost::write_datagram(BamWriter *manager, Datagram &dg)
{
	PandaNode::write_datagram(manager, dg);

	///The setup flag.
	dg.add_bool(mSetup);

	///visible/hidden
	dg.add_bool(get_visible());

	///@{
	///Physical parameters.
	dg.add_uint8((uint8_t) mShapeType);
	dg.add_uint8((uint8_t) mChildShapeType);
	dg.add_uint32(mGroupMask.get_word());
	dg.add_uint32(mCollideMask.get_word());
	dg.add_string(mUseShapeOfId);
	dg.add_uint8((uint8_t) mUpAxis);
	if (mSetup && mUserData
			&& (mShapeType == GamePhysicsManager::BT3ShapeType::TERRAIN))
	{
		// serialize heightfield through string
		ostringstream imgOSS;
		if (reinterpret_cast<PNMImage*>(mUserData)->write(imgOSS, "png"))
		{
			dg.add_bool(true);
			dg.add_string32(imgOSS.str());
		}
		else
		{
			PRINT_DEBUG("Error: Write PNMImage");
			dg.add_bool(false);
		}
	}
	dg.add_bool(mKinematic);
	///@}

	/// mCollisionObjectAttrs: save only if setup
	if (mSetup)
	{
		mCollisionObjectAttrs->write_datagram(dg);
	}

	///@{
	/// Overlap notification through event.
	mOverlap.write_datagram(dg);
	///@}

	///The object this BT3Ghost is attached to.
	manager->write_pointer(dg, mObjectNP.node());
	///The reference node path.
	manager->write_pointer(dg, mReferenceNP.node());

	///SERIALIZATION ONLY DATA
	// most common dynamic physical parameters
}

/**
 * Receives an array of pointers, one for each time manager->read_pointer()
 * was called in fillin(). Returns the number of pointers processed.
 */
int BT3Ghost::complete_pointers(TypedWritable **p_list, BamReader *manager)
{
	int pi = PandaNode::complete_pointers(p_list, manager);

	/// Pointers
	///The object this BT3Ghost is attached to.
	PT(PandaNode)objectNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mObjectNP = NodePath::any_path(objectNPPandaNode);
	///The reference node path.
	PT(PandaNode)referenceNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mReferenceNP = NodePath::any_path(referenceNPPandaNode);

	return pi;
}

/**
 * Should be after the complete restoring from the bam file.
 * \note for a complete restoring it should be called twice.
 */
void BT3Ghost::post_process_from_bam()
{
	/// setup this BT3Ghost if needed
	if (mSetup)
	{
		// first step
		if (mBuildFromBam)
		{
			mSetup = !mSetup;
			setup();
			mBuildFromBam = false;
			// restore attributes
			DatagramIterator scan(mOutDatagram);
			mCollisionObjectAttrs->read_datagram(scan, nullptr);
			mOutDatagram.clear();
		}
		// second step
	}
}

/**
 * This function is called by the BamReader's factory when a new object of
 * type BT3Ghost is encountered in the Bam file.  It should create the
 * BT3Ghost and extract its information from the file.
 */
TypedWritable *BT3Ghost::make_from_bam(const FactoryParams &params)
{
	// continue only if GamePhysicsManager exists
	CONTINUE_IF_ELSE_R(GamePhysicsManager::get_global_ptr(), nullptr)

	// create a BT3Ghost with default parameters' values: they'll be restored later
	GamePhysicsManager::get_global_ptr()->set_parameters_defaults(
			GamePhysicsManager::GHOST);
	BT3Ghost *node = DCAST(BT3Ghost,
			GamePhysicsManager::get_global_ptr()->create_ghost(
					"BT3Ghost").node());

	DatagramIterator scan;
	BamReader *manager;

	parse_params(params, scan, manager);
	node->fillin(scan, manager);

	return node;
}

/**
 * This internal function is called by make_from_bam to read in all of the
 * relevant data from the BamFile for the new BT3Ghost.
 */
void BT3Ghost::fillin(DatagramIterator &scan, BamReader *manager)
{
	PandaNode::fillin(scan, manager);

	///The setup flag.
	mSetup = scan.get_bool();

	///visible/hidden
	set_visible(scan.get_bool());

	///@{
	///Physical parameters.
	mShapeType = (GamePhysicsManager::BT3ShapeType) scan.get_uint8();
	mChildShapeType = (GamePhysicsManager::BT3ShapeType) scan.get_uint8();
	mGroupMask.set_word(scan.get_uint32());
	mCollideMask.set_word(scan.get_uint32());
	mUseShapeOfId = scan.get_string();
	mUpAxis = (GamePhysicsManager::BT3UpAxis) scan.get_uint8();
	if (mSetup && (mShapeType == GamePhysicsManager::BT3ShapeType::TERRAIN))
	{
		// de-serialize through string
		if (scan.get_bool())
		{
			istringstream imgISS(scan.get_string32());
			mUserData = new PNMImage();
			if (!reinterpret_cast<PNMImage*>(mUserData)->read(imgISS, "png"))
			{
				PRINT_DEBUG("Error: Read PNMImage");
			}
		}
	}
	mKinematic = scan.get_bool();
	///@}

	/// Dynamic stuff: read only if setup and defer restoring to post_process_from_bam()
	if (mSetup)
	{
		// use a temporary *Attrs of the right type
		CollisionObjectAttrs* attrsTmp = do_create_collision_object_attrs();
		attrsTmp->read_datagram(scan, &mOutDatagram);
		delete attrsTmp;
	}

	///@{
	/// Overlap notification through event.
	mOverlap.read_datagram(scan);
	// reset counters
	enable_throw_event(OVERLAP, mOverlap.mEnable, mOverlap.mFrequency,
			mOverlap.mEventName);
	///@}

	///The object this BT3Ghost is attached to.
	manager->read_pointer(scan);
	///The reference node path.
	manager->read_pointer(scan);
	// for correct restoring
	mBuildFromBam = true;
}

TYPED_OBJECT_API_DEF(BT3Ghost)
