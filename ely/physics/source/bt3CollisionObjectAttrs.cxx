/**
 * \file bt3CollisionObjectAttrs.cxx
 *
 * \date 2017-04-22
 * \author consultit
 */

#include "bt3CollisionObjectAttrs.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
#endif //PYTHON_BUILD

///CollisionObjectAttrs definitions
#ifdef PYTHON_BUILD
CollisionObjectAttrs::CollisionObjectAttrs() :
		mCollisionObject {nullptr}, mAnisotropicFrictionMode{
				ANISOTROPIC_FRICTION_DISABLED}
{
	msg = "CollisionObjectAttrs";
	__dict__ = PyDict_New();
}
CollisionObjectAttrs::~CollisionObjectAttrs()
{
	Py_DECREF(__dict__);
}
PyObject *CollisionObjectAttrs::__getattr__(PyObject *attr) const
{
	return check_getattr(attr);
}
PyObject *CollisionObjectAttrs::check_getattr(PyObject *attr) const
{
	PyObject *item = PyDict_GetItem(__dict__, attr);
	if (item == nullptr)
	{
#if PY_MAJOR_VERSION < 3
		string attrName = PyString_AS_STRING(attr);
#else
		string attrName = PyUnicode_AsUTF8(attr);
#endif
		cerr << msg << ": not existent attribute: " << attrName << endl;
		Py_RETURN_NONE;
	}
	// PyDict_GetItem returns a borrowed reference.
	Py_INCREF(item);
	return item;
}
#else
CollisionObjectAttrs::CollisionObjectAttrs() :
		mCollisionObject{nullptr}, mAnisotropicFrictionMode{
				ANISOTROPIC_FRICTION_DISABLED}
{
}
CollisionObjectAttrs::~CollisionObjectAttrs()
{
}
#endif //PYTHON_BUILD
void CollisionObjectAttrs::output(ostream &out) const
{
	out << "anisotropic_friction_mode " << mAnisotropicFrictionMode << "|"
	<< "anisotropic_friction " << get_anisotropic_friction() << "|"
	<< "contact_processing_threshold " << get_contact_processing_threshold() << "|"
	<< "friction " << get_friction() << "|"
	<< "rolling_friction " << get_rolling_friction() << "|"
	<< "contact_damping " << get_contact_damping() << "|"
	<< "contact_stiffness " << get_contact_stiffness() << "|"
	<< "restitution " << get_restitution() << "|"
	<< "ccd_swept_sphere_radius " << get_ccd_swept_sphere_radius() << "|"
	<< "ccd_motion_threshold " << get_ccd_motion_threshold() << "|"
	<< "activation_state " << get_activation_state() << "|"
	<< "deactivation_time " << get_deactivation_time();
}
void CollisionObjectAttrs::write_datagram(Datagram &dg) const
{
	dg.add_uint8((uint8_t)mAnisotropicFrictionMode);
	get_anisotropic_friction().write_datagram(dg);
	dg.add_stdfloat(get_contact_processing_threshold());
	dg.add_stdfloat(get_deactivation_time());
	dg.add_stdfloat(get_friction());
	dg.add_stdfloat(get_rolling_friction());
	dg.add_stdfloat(get_contact_damping());
	dg.add_stdfloat(get_contact_stiffness());
	dg.add_stdfloat(get_restitution());
	dg.add_stdfloat(mCollisionObject->getHitFraction());
	dg.add_stdfloat(get_ccd_swept_sphere_radius());
	dg.add_stdfloat(get_ccd_motion_threshold());
	dg.add_int32(mCollisionObject->getCollisionFlags());
	dg.add_int32(mCollisionObject->getIslandTag());
	dg.add_int32(mCollisionObject->getCompanionId());
	dg.add_uint8((uint8_t)get_activation_state());
}
void CollisionObjectAttrs::read_datagram(DatagramIterator &scan, Datagram* outDatagram)
{
	if (outDatagram)
	{
		outDatagram->add_uint8(scan.get_uint8());
		LVecBase3f frict;
		frict.read_datagram(scan);
		frict.write_datagram(*outDatagram);
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_stdfloat(scan.get_stdfloat());
		outDatagram->add_int32(scan.get_int32());
		outDatagram->add_int32(scan.get_int32());
		outDatagram->add_int32(scan.get_int32());
		outDatagram->add_uint8(scan.get_uint8());
	}
	else
	{
		mAnisotropicFrictionMode = (BT3AnisotropicFrictionMode)scan.get_uint8();
		LVecBase3f frict;
		frict.read_datagram(scan);
		set_anisotropic_friction(frict);
		set_contact_processing_threshold(scan.get_stdfloat());
		set_deactivation_time(scan.get_stdfloat());
		set_friction(scan.get_stdfloat());
		set_rolling_friction(scan.get_stdfloat());
		set_contact_damping(scan.get_stdfloat());
		set_contact_stiffness(scan.get_stdfloat());
		set_restitution(scan.get_stdfloat());
		mCollisionObject->setHitFraction(scan.get_stdfloat());
		set_ccd_swept_sphere_radius(scan.get_stdfloat());
		set_ccd_motion_threshold(scan.get_stdfloat());
		mCollisionObject->setCollisionFlags(scan.get_int32());
		mCollisionObject->setIslandTag(scan.get_int32());
		mCollisionObject->setCompanionId(scan.get_int32());
		set_activation_state((BT3ActivationState)scan.get_uint8());
	}
}
