
#include "config_module.h"
#include "dconfig.h"

#include "bt3RigidBody.h"
#include "bt3SoftBody.h"
#include "bt3Ghost.h"
#include "bt3Constraint.h"
#include "bt3Vehicle.h"
#include "bt3CharacterController.h"
#include "gamePhysicsManager.h"

Configure( config_p3physics );
NotifyCategoryDef( p3physics , "");

ConfigureFn( config_p3physics ) {
  init_libp3physics();
}

void
init_libp3physics() {
  static bool initialized = false;
  if (initialized) {
    return;
  }
  initialized = true;

  // Init your dynamic types here, e.g.:
  // MyDynamicClass::init_type();
  BT3RigidBody::init_type();
  BT3SoftBody::init_type();
  BT3Ghost::init_type();
  BT3Constraint::init_type();
  BT3Vehicle::init_type();
  BT3CharacterController::init_type();
  BT3RigidBody::register_with_read_factory();
  BT3SoftBody::register_with_read_factory();
  BT3Ghost::register_with_read_factory();
  BT3Constraint::register_with_read_factory();
  BT3Vehicle::register_with_read_factory();
  BT3CharacterController::register_with_read_factory();
  GamePhysicsManager::init_type();

  return;
}

