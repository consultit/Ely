/**
 * \file bt3CollisionObjectAttrs.h
 *
 * \date 2017-04-22
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_BT3COLLISIONOBJECTATTRS_H_
#define PHYSICS_SOURCE_BT3COLLISIONOBJECTATTRS_H_

#include "physics_includes.h"

#ifndef CPPPARSER
#include "support/physics.h"
#include "support/constraint.h"
#endif //CPPPARSER

#define SERIALIZATIONDECL \
		virtual void write_datagram(Datagram &dg) const;\
	virtual void read_datagram(DatagramIterator &scan, Datagram* outDatagram);

/**
 * CollisionObjectAttrs class.
 *
 * \note All physical quantities are expressed in the same units of measurement
 * used by Bullet.
 *
 */
class EXPCL_PHYSICS CollisionObjectAttrs
{
PUBLISHED:

	/**
	 * Equivalent to btCollisionObject's island management (m_activationState1).
	 */
	enum BT3ActivationState: unsigned char
	{
#ifndef CPPPARSER
		ACTIVE_STATE = ACTIVE_TAG,
		ISLAND_SLEEPING_STATE = ISLAND_SLEEPING,
		WANTS_DEACTIVATION_STATE = WANTS_DEACTIVATION,
		DISABLE_DEACTIVATION_STATE = DISABLE_DEACTIVATION,
		DISABLE_SIMULATION_STATE = DISABLE_SIMULATION,
#else
		ACTIVE_STATE,ISLAND_SLEEPING_STATE,WANTS_DEACTIVATION_STATE,
		DISABLE_DEACTIVATION_STATE,DISABLE_SIMULATION_STATE
#endif //CPPPARSER
	};

	/**
	 * Equivalent to btCollisionObject::AnisotropicFrictionFlags.
	 */
	enum BT3AnisotropicFrictionMode: unsigned char
	{
#ifndef CPPPARSER
		ANISOTROPIC_FRICTION_DISABLED =
				btCollisionObject::CF_ANISOTROPIC_FRICTION_DISABLED,
		ANISOTROPIC_FRICTION = btCollisionObject::CF_ANISOTROPIC_FRICTION,
		ANISOTROPIC_ROLLING_FRICTION =
				btCollisionObject::CF_ANISOTROPIC_ROLLING_FRICTION,
#else
		ANISOTROPIC_FRICTION_DISABLED,ANISOTROPIC_FRICTION,
		ANISOTROPIC_ROLLING_FRICTION,
#endif //CPPPARSER
	};

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	CollisionObjectAttrs();
	virtual ~CollisionObjectAttrs();
	PyObject *__getattr__(PyObject *attr) const;
	PyObject *check_getattr(PyObject *attr) const;
	PyObject *__dict__;
	string msg;
#else
	CollisionObjectAttrs();
	virtual ~CollisionObjectAttrs();
#endif //PYTHON_BUILD
	INLINE void set_activation_state(BT3ActivationState newState);
	INLINE void set_force_activation_state(BT3ActivationState newState);
	INLINE BT3ActivationState get_activation_state() const;
	INLINE void set_deactivation_time(float time);
	INLINE float get_deactivation_time() const;
	INLINE void set_activate(bool forceActivation = false);
	INLINE bool get_is_active() const;
	//
	INLINE void set_anisotropic_friction_mode(
		BT3AnisotropicFrictionMode frictionMode = ANISOTROPIC_FRICTION);
	INLINE BT3AnisotropicFrictionMode get_anisotropic_friction_mode() const;
	INLINE void set_anisotropic_friction(const LVecBase3f& anisotropicFriction);
	INLINE LVecBase3f get_anisotropic_friction() const;
	INLINE void set_contact_processing_threshold(float contactProcessingThreshold);
	INLINE float get_contact_processing_threshold() const;
	INLINE void set_restitution(float rest);
	INLINE float get_restitution() const;
	INLINE void set_friction(float frict);
	INLINE float get_friction() const;
	INLINE void set_rolling_friction(float frict);
	INLINE float get_rolling_friction() const;
	INLINE void set_spinning_friction(float frict);
	INLINE float get_spinning_friction() const;
	INLINE void set_contact_stiffness(float stiffness);
	INLINE float get_contact_stiffness() const;
	INLINE void set_contact_damping(float damping);
	INLINE float get_contact_damping() const;
	INLINE void set_ccd_swept_sphere_radius(float radius);
	INLINE float get_ccd_swept_sphere_radius() const;
	INLINE void set_ccd_motion_threshold(float ccdMotionThreshold);
	INLINE float get_ccd_motion_threshold() const;
	//
	INLINE void set_world_transform(CPT(TransformState) trans);
	INLINE CPT(TransformState) get_world_transform() const;
	//collision shape margin
	INLINE void set_shape_margin(float margin);
	INLINE float get_shape_margin() const;
	// Python Properties
	MAKE_PROPERTY(anisotropic_friction_mode, get_anisotropic_friction_mode, set_anisotropic_friction_mode);
	MAKE_PROPERTY(anisotropic_friction, get_anisotropic_friction, set_anisotropic_friction);
	MAKE_PROPERTY(contact_processing_threshold, get_contact_processing_threshold, set_contact_processing_threshold);
	MAKE_PROPERTY(restitution, get_restitution, set_restitution);
	MAKE_PROPERTY(friction, get_friction, set_friction);
	MAKE_PROPERTY(rolling_friction, get_rolling_friction, set_rolling_friction);
	MAKE_PROPERTY(spinning_friction, get_spinning_friction, set_spinning_friction);
	MAKE_PROPERTY(contact_stiffness, get_contact_stiffness, set_contact_stiffness);
	MAKE_PROPERTY(contact_damping, get_contact_damping, set_contact_damping);
	MAKE_PROPERTY(ccd_swept_sphere_radius, get_ccd_swept_sphere_radius, set_ccd_swept_sphere_radius);
	MAKE_PROPERTY(ccd_motion_threshold, get_ccd_motion_threshold, set_ccd_motion_threshold);
	MAKE_PROPERTY(shape_margin, get_shape_margin, set_shape_margin);

#ifndef CPPPARSER
protected:
	btCollisionObject* mCollisionObject;
	BT3AnisotropicFrictionMode mAnisotropicFrictionMode;
#endif //CPPPARSER

public:
	inline void set_collision_object(btCollisionObject* collisionObject);
	void output(ostream &out) const;
	SERIALIZATIONDECL
};
INLINE ostream &operator << (ostream &out, const CollisionObjectAttrs & attrs);

///inline
#include "bt3CollisionObjectAttrs.I"

#endif /* PHYSICS_SOURCE_BT3COLLISIONOBJECTATTRS_H_ */
