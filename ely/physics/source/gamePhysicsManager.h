/**
 * \file gamePhysicsManager.h
 *
 * \date 2016-10-09
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_GAMEPHYSICSMANGER_H_
#define PHYSICS_SOURCE_GAMEPHYSICSMANGER_H_

#include "nodePath.h"
#include "mouseWatcher.h"
#include "physics_includes.h"
#include "physicsTools.h"

#ifndef CPPPARSER
#include "support/softBody.h"
#include "support/vehicle.h"
#include "support/characterController.h"
#include "support/picker.h"
#include "support/collisionShape.h"
#include "bullet3/src/BulletSoftBody/btSoftBodyHelpers.h"
#endif //CPPPARSER

class BT3RigidBody;
class BT3SoftBody;
class BT3Ghost;
class BT3Constraint;
class BT3Vehicle;
class BT3CharacterController;

/**
 * GamePhysicsManager Singleton class.
 *
 * Used for handling BT3RigidBodys, BT3SoftBodys, BT3Ghosts, BT3Constraints,
 * BT3Vehicles, BT3CharacterControllers.
 *
 * GamePhysicsManager could throw events when objects collide. By default this
 * feature is disabled.\n
 * When enabled, GamePhysicsManager can throw these events:
 * - when two objects collide (default: <obect1Name>_<object2Name>_Collision).
 * This Event is thrown continuously until the two objects keep colliding, at a
 * frequency which is the minimum between the fps and the frequency specified as
 * parameter (which defaults to 30 times per seconds);
 * - when the two objects stop collide (default:
 * <obect1Name>_<object2Name>_CollisionOff). This Event is thrown only once.
 *
 * Both these Events have as arguments the two objects involved in the
 * collision/stop collision.
 *
 * \note All physical quantities are expressed in the same units of measurement
 * used by Bullet.
 *
 */
class EXPCL_PHYSICS GamePhysicsManager: public TypedReferenceCount,
		public Singleton<GamePhysicsManager>
{
PUBLISHED:

	/**
	 * Equivalent to bt3::Panda3dUpAxis.
	 */
	enum BT3UpAxis: unsigned char
	{
#ifndef CPPPARSER
		X_up = static_cast<unsigned char>(bt3::Panda3dUpAxis::X_up),
		Y_up = static_cast<unsigned char>(bt3::Panda3dUpAxis::Y_up),
		Z_up = static_cast<unsigned char>(bt3::Panda3dUpAxis::Z_up),
		NO_up = static_cast<unsigned char>(bt3::Panda3dUpAxis::NO_up)
#else
		X_up,Y_up,Z_up,NO_up
#endif //CPPPARSER
	};

	/**
	 * The type of object for creation parameters.
	 */
	enum BT3PhysicType: unsigned char
	{
		RIGIDBODY = 0,
		SOFTBODY,
		GHOST,
		CONSTRAINT,
		VEHICLE,
		CHARACTERCONTROLLER,
	};

	/**
	 * Equivalent to BroadphaseNativeTypes.
	 */
	enum BT3ShapeType: unsigned char
	{
#ifndef CPPPARSER
		PLANE = BroadphaseNativeTypes::STATIC_PLANE_PROXYTYPE, //!< PLANE (norm_x, norm_y, norm_z, d)
		BOX = BroadphaseNativeTypes::BOX_SHAPE_PROXYTYPE, //!< BOX (half_x, half_y, half_z)
		SPHERE = BroadphaseNativeTypes::SPHERE_SHAPE_PROXYTYPE, //!< SPHERE (radius)
		CAPSULE = BroadphaseNativeTypes::CAPSULE_SHAPE_PROXYTYPE, //!< CAPSULE (radius, height, up)
		CYLINDER = BroadphaseNativeTypes::CYLINDER_SHAPE_PROXYTYPE, //!< CYLINDER (radius, height, up)
		CONE = BroadphaseNativeTypes::CONE_SHAPE_PROXYTYPE, //!< CONE (radius, height, up)
		MULTI_SPHERE = BroadphaseNativeTypes::MULTI_SPHERE_SHAPE_PROXYTYPE,
		CONVEX_HULL = BroadphaseNativeTypes::CONVEX_HULL_SHAPE_PROXYTYPE,
		GIMPACT = BroadphaseNativeTypes::GIMPACT_SHAPE_PROXYTYPE,
		TRIANGLE_MESH = BroadphaseNativeTypes::TRIANGLE_MESH_SHAPE_PROXYTYPE, //!< TRIANGLE_MESH (dynamic)
		TERRAIN = BroadphaseNativeTypes::TERRAIN_SHAPE_PROXYTYPE, //!< TERRAIN (image, height, up, scale_w, scale_d)
		COMPOUND = BroadphaseNativeTypes::COMPOUND_SHAPE_PROXYTYPE,
		INVALID = BroadphaseNativeTypes::INVALID_SHAPE_PROXYTYPE,
#else
		PLANE,BOX,SPHERE,CAPSULE,CYLINDER,CONE,MULTI_SPHERE,
		CONVEX_HULL,GIMPACT,TRIANGLE_MESH,TERRAIN,COMPOUND,INVALID
#endif //CPPPARSER
	};

	/**
	 * Physics thrown events.
	 */
	enum BT3EventThrown: unsigned char
	{
		COLLISIONNOTIFY
	};

	/**
	 * Picking types.
	 */
	enum BT3PickingType: unsigned char
	{
#ifndef CPPPARSER
		POINT2POINT = btTypedConstraintType::POINT2POINT_CONSTRAINT_TYPE,
		DOF6 = btTypedConstraintType::D6_CONSTRAINT_TYPE,
#else
		POINT2POINT,DOF6
#endif //CPPPARSER
	};

	/**
	 * Equivalent to btIDebugDraw::DebugDrawModes.
	 */
	enum BT3DebugMode: int
	{
#ifndef CPPPARSER
		NoDebug = btIDebugDraw::DBG_NoDebug,
		DrawWireframe  = btIDebugDraw::DBG_DrawWireframe ,
		DrawAabb  = btIDebugDraw::DBG_DrawAabb ,
		DrawFeaturesText  = btIDebugDraw::DBG_DrawFeaturesText ,
		DrawContactPoints  = btIDebugDraw::DBG_DrawContactPoints ,
		NoDeactivation  = btIDebugDraw::DBG_NoDeactivation ,
		NoHelpText  = btIDebugDraw::DBG_NoHelpText ,
		DrawText  = btIDebugDraw::DBG_DrawText ,
		ProfileTimings  = btIDebugDraw::DBG_ProfileTimings ,
		EnableSatComparison  = btIDebugDraw::DBG_EnableSatComparison ,
		DisableBulletLCP  = btIDebugDraw::DBG_DisableBulletLCP ,
		EnableCCD  = btIDebugDraw::DBG_EnableCCD ,
		DrawConstraints  = btIDebugDraw::DBG_DrawConstraints ,
		DrawConstraintLimits  = btIDebugDraw::DBG_DrawConstraintLimits ,
		FastWireframe  = btIDebugDraw::DBG_FastWireframe ,
		DrawNormals  = btIDebugDraw::DBG_DrawNormals ,
		DrawFrames = btIDebugDraw::DBG_DrawFrames,
#else
		NoDebug,DrawWireframe,DrawAabb,DrawFeaturesText,DrawContactPoints,
		NoDeactivation,NoHelpText,DrawText,ProfileTimings,EnableSatComparison,
		DisableBulletLCP,EnableCCD,DrawConstraints,DrawConstraintLimits,
		FastWireframe,DrawNormals,DrawFrames
#endif //CPPPARSER
	};

	/**
	 * Equivalent to fDrawFlags (soft body only).
	 */
	enum BT3DrawSoftFlags: int
	{
#ifndef CPPPARSER
		Nodes = fDrawFlags::Nodes,
		Links = fDrawFlags::Links,
		Faces = fDrawFlags::Faces,
		Tetras = fDrawFlags::Tetras,
		Normals = fDrawFlags::Normals,
		Contacts = fDrawFlags::Contacts,
		Anchors = fDrawFlags::Anchors,
		Notes = fDrawFlags::Notes,
		Clusters = fDrawFlags::Clusters,
		NodeTree = fDrawFlags::NodeTree,
		FaceTree = fDrawFlags::FaceTree,
		ClusterTree = fDrawFlags::ClusterTree,
		Joints = fDrawFlags::Joints,
		Std = fDrawFlags::Std,
		StdTetra = fDrawFlags::StdTetra,
#else
		Nodes,Links,Faces,Tetras,Normals,Contacts,Anchors,Notes,Clusters,
		NodeTree,FaceTree,ClusterTree,Joints,Std,StdTetra,
#endif //CPPPARSER
	};

	GamePhysicsManager(int taskSort = 10, const NodePath& root = NodePath(),
			const CollideMask& groupMask = GeomNode::get_default_collide_mask(),
			const CollideMask& collideMask = GeomNode::get_default_collide_mask(),
			const BT3PhysicsInfo& info = BT3PhysicsInfo(),
			int debugMode = DrawWireframe|DrawConstraints|DrawConstraintLimits,
			int drawSoftFlags = Std);
	virtual ~GamePhysicsManager();

	/**
	 * \name REFERENCE NODES
	 */
	///@{
	INLINE NodePath get_reference_node_path() const;
	INLINE void set_reference_node_path(const NodePath& reference);
	INLINE NodePath get_reference_node_path_debug() const;
	// Python Properties
	MAKE_PROPERTY(reference_node_path, get_reference_node_path, set_reference_node_path);
	MAKE_PROPERTY(reference_node_path_debug, get_reference_node_path_debug);
	///@}

	/**
	 * \name BT3RigidBody
	 */
	///@{
	NodePath create_rigid_body(const string& name);
	bool destroy_rigid_body(NodePath rigidBodyNP);
	PT(BT3RigidBody) get_rigid_body(int index) const;
	INLINE int get_num_rigid_bodies() const;
	MAKE_SEQ(get_rigid_bodies, get_num_rigid_bodies, get_rigid_body);
	// Python Properties
	MAKE_PROPERTY(num_rigid_bodies, get_num_rigid_bodies);
	MAKE_SEQ_PROPERTY(rigid_bodies, get_num_rigid_bodies, get_rigid_body);
	///@}

	/**
	 * \name BT3SoftBody
	 */
	///@{
	NodePath create_soft_body(const string& name);
	bool destroy_soft_body(NodePath softBodyNP);
	PT(BT3SoftBody) get_soft_body(int index) const;
	INLINE int get_num_soft_bodies() const;
	MAKE_SEQ(get_soft_bodies, get_num_soft_bodies, get_soft_body);
	// Python Properties
	MAKE_PROPERTY(num_soft_bodies, get_num_soft_bodies);
	MAKE_SEQ_PROPERTY(soft_bodies, get_num_soft_bodies, get_soft_body);
	///@}

	/**
	 * \name BT3SoftBody's world settings.
	 */
	///@{
	INLINE void set_air_density(float value);
	INLINE float get_air_density();
	INLINE void set_water_density(float value);
	INLINE float get_water_density();
	INLINE void set_water_offset(float value);
	INLINE float get_water_offset();
	INLINE void set_max_displacement(float value);
	INLINE float get_max_displacement();
	INLINE void set_water_normal(const LVector3f& value);
	INLINE LVector3f get_water_normal();
	// Python Properties
	MAKE_PROPERTY(air_density, get_air_density, set_air_density);
	MAKE_PROPERTY(water_density, get_water_density, set_water_density);
	MAKE_PROPERTY(water_offset, get_water_offset, set_water_offset);
	MAKE_PROPERTY(max_displacement, get_max_displacement, set_max_displacement);
	MAKE_PROPERTY(water_normal, get_water_normal, set_water_normal);
	///@}

	/**
	 * \name BT3Ghost
	 */
	///@{
	NodePath create_ghost(const string& name);
	bool destroy_ghost(NodePath ghostNP);
	PT(BT3Ghost) get_ghost(int index) const;
	INLINE int get_num_ghosts() const;
	MAKE_SEQ(get_ghosts, get_num_ghosts, get_ghost);
	// Python Properties
	MAKE_PROPERTY(num_ghosts, get_num_ghosts);
	MAKE_SEQ_PROPERTY(ghosts, get_num_ghosts, get_ghost);
	///@}

	/**
	 * \name BT3Constraint
	 */
	///@{
	NodePath create_constraint(const string& name);
	bool destroy_constraint(NodePath constraintNP);
	PT(BT3Constraint) get_constraint(int index) const;
	INLINE int get_num_constraints() const;
	MAKE_SEQ(get_constraints, get_num_constraints, get_constraint);
	// Python Properties
	MAKE_PROPERTY(num_constraints, get_num_constraints);
	MAKE_SEQ_PROPERTY(constraints, get_num_constraints, get_constraint);
	///@}

	/**
	 * \name BT3Vehicle
	 */
	///@{
	NodePath create_vehicle(const string& name);
	bool destroy_vehicle(NodePath vehicleNP);
	PT(BT3Vehicle) get_vehicle(int index) const;
	INLINE int get_num_vehicles() const;
	MAKE_SEQ(get_vehicles, get_num_vehicles, get_vehicle);
	// Python Properties
	MAKE_PROPERTY(num_vehicles, get_num_vehicles);
	MAKE_SEQ_PROPERTY(vehicles, get_num_vehicles, get_vehicle);
	///@}

	/**
	 * \name BT3CharacterController
	 */
	///@{
	NodePath create_character_controller(const string& name);
	bool destroy_character_controller(NodePath characterControllerNP);
	PT(BT3CharacterController) get_character_controller(int index) const;
	INLINE int get_num_character_controllers() const;
	MAKE_SEQ(get_character_controllers, get_num_character_controllers, get_character_controller);
	// Python Properties
	MAKE_PROPERTY(num_character_controllers, get_num_character_controllers);
	MAKE_SEQ_PROPERTY(character_controllers, get_num_character_controllers, get_character_controller);
	///@}

	/**
	 * \name RAY/CONTACT TESTS
	 */
	///@{
	BT3RayTestResult ray_test(const LPoint3f& rayFromWorld,
		const LPoint3f& rayToWorld, BT3RayTestResult::BT3HitType hitType,
		const CollideMask& groupMask = CollideMask::all_on(),
		const CollideMask& collideMask = CollideMask::all_on()) const;
	BT3ConvexTestResult convex_sweep_test(const NodePath& convexObject,
		CPT(TransformState) convexFromWorld, CPT(TransformState) convexToWorld,
		BT3ConvexTestResult::BT3HitType hitType, float allowedCcdPenetration = 0.0,
		const CollideMask& groupMask = CollideMask::all_on(),
		const CollideMask& collideMask = CollideMask::all_on()) const;
	BT3ContactTestResult contact_test(const NodePath& colObjA,
		const NodePath& colObjB = NodePath(),
		BT3ContactTestResult::BT3ContactType contactType=BT3ContactTestResult::SINGLE,
		int maxNumContact = 16, const CollideMask& groupMask = CollideMask::all_on(),
		const CollideMask& collideMask = CollideMask::all_on()) const;
	///@}

	/**
	 * \name TEXTUAL PARAMETERS
	 */
	///@{
	ValueList_string get_parameter_name_list(BT3PhysicType type) const;
	void set_parameter_values(BT3PhysicType type, const string& paramName, const ValueList_string& paramValues);
	ValueList_string get_parameter_values(BT3PhysicType type, const string& paramName) const;
	void set_parameter_value(BT3PhysicType type, const string& paramName, const string& value);
	string get_parameter_value(BT3PhysicType type, const string& paramName) const;
	void set_parameters_defaults(BT3PhysicType type);
	///@}

	/**
	 * \name DEFAULT UPDATE
	 */
	///@{
	INLINE void update();
#ifndef CPPPARSER
	AsyncTask::DoneStatus update(GenericAsyncTask* task);
#endif //CPPPARSER
	void start_default_update();
	void stop_default_update();
	///@}

	/**
	 * \name SINGLETON
	 */
	///@{
	INLINE static GamePhysicsManager* get_global_ptr();
	///@}

	/**
	 * \name UTILITIES (bullet3 based)
	 */
	///@{
	float get_bounding_dimensions(NodePath modelNP, LVecBase3f& modelDims,
			LVector3f& modelDeltaCenter, BT3UpAxis upAxis = Z_up) const;
	Pair_bool_float get_collision_height(const LPoint3f& fromPos);
	INLINE const CollideMask& get_group_mask() const;
	INLINE const CollideMask& get_collide_mask() const;
	// Python Properties
	MAKE_PROPERTY(group_mask, get_group_mask);
	MAKE_PROPERTY(collide_mask, get_collide_mask);
	///@}

	/**
	 * \name UTILITIES (panda3d based)
	 */
	///@{
	INLINE const Utilities& get_utilities();
	// Python Properties
	MAKE_PROPERTY(utilities, get_utilities);
	///@}

	/**
	 * \name EVENTS
	 */
	///@{
	void enable_throw_event(BT3EventThrown event, bool enable,
			float eventFreq = 30.0, const string& eventName = "");
	///@}

	/**
	 * \name PICKING
	 */
	///@{
	INLINE void enable_picking(const NodePath& render, const NodePath& camera,
			PT(MouseWatcher) watcher, const string& pickKeyOn,
			 const string& pickKeyOff);
	INLINE void disable_picking();
	INLINE bool get_picking_enabled();
	INLINE PT(PandaNode) picking_ray_cast() const;
	INLINE LPoint3f get_hit_ray_cast_pos() const;
	INLINE LPoint3f get_hit_ray_cast_from_pos() const;
	INLINE LPoint3f get_hit_ray_cast_to_pos() const;
	INLINE LVector3f get_hit_ray_cast_normal() const;
	INLINE float get_hit_ray_cast_fraction() const;
	INLINE void set_picking_type(BT3PickingType type);
	INLINE BT3PickingType get_picking_type() const;
	INLINE void set_picking_cfm(float value);
	INLINE float get_picking_cfm() const;
	INLINE void set_picking_erp(float value);
	INLINE float get_picking_erp() const;
	// Python Properties
	MAKE_PROPERTY(is_picking_enabled, get_picking_enabled);
	MAKE_PROPERTY(hit_ray_cast_pos, get_hit_ray_cast_pos);
	MAKE_PROPERTY(hit_ray_cast_from_pos, get_hit_ray_cast_from_pos);
	MAKE_PROPERTY(hit_ray_cast_to_pos, get_hit_ray_cast_to_pos);
	MAKE_PROPERTY(hit_ray_cast_normal, get_hit_ray_cast_normal);
	MAKE_PROPERTY(hit_ray_cast_fraction, get_hit_ray_cast_fraction);
	MAKE_PROPERTY(picking_type, get_picking_type, set_picking_type);
	MAKE_PROPERTY(picking_cfm, get_picking_cfm, set_picking_cfm);
	MAKE_PROPERTY(picking_erp, get_picking_erp, set_picking_erp);
	///@}

	/**
	 * \name DEBUG
	 */
	///@{
	void debug(bool enable);
	///@}

	/**
	 * \name SERIALIZATION
	 */
	///@{
	bool write_to_bam_file(const string& fileName);
	bool read_from_bam_file(const string& fileName);
	///@}

public:
	///Bullet physics.
	inline bt3::Physics* get_bullet_physics();

	///Unique ref producer.
	inline int unique_ref();

#ifndef CPPPARSER
private:
	/// Bullet physics.
	bt3::Physics mPhysics;

	///The update task sort (should be >0).
	int mTaskSort;

	///The reference node path.
	NodePath mReferenceNP;

	///List of BT3RigidBodys handled by this manager.
	typedef pvector<PT(BT3RigidBody)> RigidBodyList;
	RigidBodyList mRigidBodies;
	///BT3RigidBodys' parameter table.
	ParameterTable mRigidBodiesParameterTable;

	///List of BT3SoftBodys handled by this manager.
	typedef pvector<PT(BT3SoftBody)> SoftBodyList;
	SoftBodyList mSoftBodies;
	PT(bt3::SoftBodyManager) mSoftBodyManager;
	///BT3SoftBodys' parameter table.
	ParameterTable mSoftBodiesParameterTable;

	///List of BT3Ghosts handled by this manager.
	typedef pvector<PT(BT3Ghost)> GhostList;
	GhostList mGhosts;
	///BT3Ghosts' parameter table.
	ParameterTable mGhostsParameterTable;

	///List of BT3Constraints handled by this manager.
	typedef pvector<PT(BT3Constraint)> ConstraintList;
	ConstraintList mConstraints;
	///BT3Constraints' parameter table.
	ParameterTable mConstraintsParameterTable;

	///List of BT3Vehicles handled by this manager.
	typedef pvector<PT(BT3Vehicle)> VehicleList;
	VehicleList mVehicles;
	PT(bt3::VehicleManager) mVehicleManager;
	///BT3Vehicles' parameter table.
	ParameterTable mVehiclesParameterTable;

	///List of BT3CharacterControllers handled by this manager.
	typedef pvector<PT(BT3CharacterController)> CharacterControllerList;
	CharacterControllerList mCharacterControllers;
	PT(bt3::CharacterControllerManager) mCharacterControllerManager;
	///BT3CharacterControllers' parameter table.
	ParameterTable mCharacterControllersParameterTable;

	///@{
	///A task data for step simulation update.
	PT(TaskInterface<GamePhysicsManager>::TaskData) mUpdateData;
	PT(AsyncTask) mUpdateTask;
	///@}

	///Unique ref.
	int mRef;

	///Utilities (bullet3 based).
	CollideMask mGroupMask; //a.k.a. BitMask32
	CollideMask mCollideMask; //a.k.a. BitMask32

	///Utilities.
	Utilities mUtils;

	///Picker.
	bt3::Picker* mPicker;

	/**
	 * \name Physics events
	 */
	///@{
	/// Collision notification through event.
	struct CollidingNodePair
	{
		struct CollidingNodePairData
		{
			pair<PandaNode*, PandaNode*> mPnode;
			string mEventName;
			EventParameter mEventParameters[2];
			int mCount;
		};
		//main constructors
		CollidingNodePair(PandaNode *_pnode0, PandaNode *_pnode1) :
				mCollidingNodePairData(new CollidingNodePairData), mRefCount(new int)
		{
			// always create the pair in a predictable order
			// (use the pointer value..)
			if (_pnode0 > _pnode1)
			{
				mCollidingNodePairData->mPnode = make_pair(_pnode1, _pnode0);
			}
			else
			{
				mCollidingNodePairData->mPnode = make_pair(_pnode0, _pnode1);
			}
			*mRefCount = 1;
		}
		CollidingNodePair(PandaNode *_pnode0, PandaNode *_pnode1, const string& _name, int _count) :
				mCollidingNodePairData(new CollidingNodePairData), mRefCount(new int)
		{
			// always create the pair in a predictable order
			// (use the pointer value..)
			if (_pnode0 > _pnode1)
			{
				mCollidingNodePairData->mPnode = make_pair(_pnode1, _pnode0);
			}
			else
			{
				mCollidingNodePairData->mPnode = make_pair(_pnode0, _pnode1);
			}
			mCollidingNodePairData->mEventName = _name;
			mCollidingNodePairData->mCount = _count;
			*mRefCount = 1;
		}
		//copy constructor
		CollidingNodePair(const CollidingNodePair& on) :
				mCollidingNodePairData(on.mCollidingNodePairData), mRefCount(on.mRefCount)
		{
			++(*mRefCount);
		}
		//destructor
		~CollidingNodePair()
		{
			if (--(*mRefCount) == 0)
			{
				delete mCollidingNodePairData;
				delete mRefCount;
			}
		}
		//assignment operator
		CollidingNodePair& operator=(const CollidingNodePair& on)
		{
			mCollidingNodePairData = on.mCollidingNodePairData;
			++(*mRefCount);
			return *this;
		}
		//comparison operator
		bool operator<(const CollidingNodePair& on) const
		{
			return mCollidingNodePairData->mPnode < on.mCollidingNodePairData->mPnode;
		}
		//owned resource
		CollidingNodePairData* mCollidingNodePairData;
	private:
		int* mRefCount;
	};
	ThrowEventData mCollision;
	pset<CollidingNodePair> mCollidingNodePairs;
	///@}

	///The reference node path for debug drawing.
	NodePath mReferenceDebugNP;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(GamePhysicsManager,TypedReferenceCount)
};

///inline
#include "gamePhysicsManager.I"

#endif /* PHYSICS_SOURCE_GAMEPHYSICSMANGER_H_ */
