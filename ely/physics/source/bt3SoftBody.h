/**
 * \file bt3SoftBody.h
 *
 * \date 2017-03-17
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_BT3SOFTBODY_H_
#define PHYSICS_SOURCE_BT3SOFTBODY_H_

#include "gamePhysicsManager.h"
#include "physics_includes.h"
#include "bt3CollisionObjectAttrs.h"
#include "bt3RigidBody.h"

#ifndef CPPPARSER
#include "support/softBody.h"
#endif //CPPPARSER

/**
 * BT3SoftBody is a PandaNode representing a "soft body" of the Bullet
 * library.
 *
 * It constructs a soft body with the single specified collision shape type
 * along with relevant parameters.\n
 *
 * \note All physical quantities are expressed in the same units of measurement
 * used by Bullet.
 *
 * > **BT3SoftBody text parameters**:
 * param | type | default | note
 * ------|------|---------|-----
 * | *object*  							|single| - | -
 * | *body_type*						|single| *rope* | values: rope,patch,ellipsoid,triangle_mesh,convex_hull,tetgen_mesh
 * | *group_mask*  						|single| *all_on* | -
 * | *collide_mask*  					|single| *all_on* | -
 * | *total_mass*  						|single| 1.0 | -
 * | *from_faces*  						|single| *false* | -
 * | *rope_points*  					|single| *0.0,0.0,0.0:1.0,1.0,1.0* | specified as "fromX,fromY,fromZ[:toX,toY,toZ]"
 * | *rope_fixeds*  					|single| 0 | -
 * | *rope_res*  						|single| 1 | -
 * | *patch_points*  					|single| *-1.0,-1.0,0.0:1.0,-1.0,0.0:-1.0,1.0,0.0:1.0,1.0,0.0* | specified as "p1X,p1Y,p1Z[:p2X,p2Y,p2Z[:p3X,p3Y,p3Z[:p4X,p4Y,p4Z]]]"
 * | *patch_fixeds*  					|single| 0 | -
 * | *patch_resxy*  					|single| *1:1* | specified as "resx[:resy]"
 * | *patch_gendiags*  					|single| *false* | -
 * | *ellipsoid_center*  				|single| *0.0,0.0,0.0* | -
 * | *ellipsoid_radius*  				|single| *1.0,1.0,1.0* | -
 * | *ellipsoid_res*  					|single| 1 | -
 * | *triangle_mesh_rand_constraints*	|single| *false* | -
 * | *tetgen_ele_face_node*  			|single| - | specified as "ele_file:face_file:node_file"
 * | *tetgen_facelinks*  				|single| *false* | -
 * | *tetgen_tetralinks*  				|single| *false* | -
 * | *tetgen_facesfromtetras* 			|single| *false* | -
 *
 * \note parts inside [] are optional.\n
 */
class EXPCL_PHYSICS BT3SoftBody: public PandaNode
{
PUBLISHED:

	/**
	 * Equivalent to bt3::SoftBodyData::SoftBodyType.
	 */
	enum BT3BodyType: unsigned char
	{
#ifndef CPPPARSER
		ROPE = static_cast<unsigned char>(bt3::SoftBodyData::SoftBodyType::ROPE), //!< rope shaped body specified by points,fixeds,res
		PATCH = static_cast<unsigned char>(bt3::SoftBodyData::SoftBodyType::PATCH), //!< patch shaped body specified by points,fixeds,res
		ELLIPSOID = static_cast<unsigned char>(bt3::SoftBodyData::SoftBodyType::ELLIPSOID), //!< ellipsoid shaped body specified by center,radius,res
		TRIANGLE_MESH = static_cast<unsigned char>(bt3::SoftBodyData::SoftBodyType::TRIANGLE_MESH), //!< body built from a triangle mesh; works with models having one GeomNode holding one or more Geoms
		CONVEX_HULL = static_cast<unsigned char>(bt3::SoftBodyData::SoftBodyType::CONVEX_HULL), //!< body built from the convex hull of a point set
		TETGEN_MESH = static_cast<unsigned char>(bt3::SoftBodyData::SoftBodyType::TETGEN_MESH), //!< body built from TetGen specification: ele,face,node
		INVALID_TYPE = static_cast<unsigned char>(bt3::SoftBodyData::SoftBodyType::INVALID_TYPE) //!< invalid body type
#else
		ROPE,PATCH,ELLIPSOID,TRIANGLE_MESH,CONVEX_HULL,TETGEN_MESH,INVALID_TYPE
#endif //CPPPARSER
	};

	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	virtual ~BT3SoftBody();
#endif //CPPPARSER

	/**
	 * \name OWNER OBJECT
	 */
	///@{
	INLINE void set_owner_object(const NodePath& object);
	INLINE NodePath get_owner_object() const;
	// Python Properties
	MAKE_PROPERTY(owner_object, get_owner_object, set_owner_object);
	///@}

	/**
	 * \name SOFTBODY
	 */
	///@{
	int setup();
	int cleanup();
	void update(float dt);
	///@}

	/**
	 * \name CONSTRUCTION PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE void set_body_type(BT3BodyType type);
	INLINE BT3BodyType get_body_type() const;
	INLINE void set_group_mask(const CollideMask& value);
	INLINE const CollideMask& get_group_mask() const;
	INLINE void set_collide_mask(const CollideMask& value);
	INLINE const CollideMask& get_collide_mask() const;
	INLINE void set_total_mass(float mass, bool fromfaces = false);
	INLINE float get_total_mass() const;
	INLINE void set_from_faces(bool fromFaces);
	INLINE bool get_from_faces() const;
	INLINE void set_rope_points(const ValueList_LPoint3f& ropePoints);
	INLINE ValueList_LPoint3f get_rope_points() const;
	INLINE void set_rope_fixeds(int ropeFixeds);
	INLINE int get_rope_fixeds() const;
	INLINE void set_rope_res(int ropeRes);
	INLINE int get_rope_res() const;
	INLINE void set_patch_points(const ValueList_LPoint3f& patchPoints);
	INLINE ValueList_LPoint3f get_patch_points() const;
	INLINE void set_patch_fixeds(int patchFixeds);
	INLINE int get_patch_fixeds() const;
	INLINE void set_patch_resxy(const ValueList_int& patchResxy);
	INLINE ValueList_int get_patch_resxy() const;
	INLINE void set_patch_gendiags(bool patchGendiags);
	INLINE bool get_patch_gendiags() const;
	INLINE void set_ellipsoid_center(const LPoint3f& ellipsoidCenter);
	INLINE const LPoint3f& get_ellipsoid_center() const;
	INLINE void set_ellipsoid_radius(const LVecBase3f& ellipsoidRadius);
	INLINE const LVecBase3f& get_ellipsoid_radius() const;
	INLINE void set_ellipsoid_res(int ellipsoidRes);
	INLINE int get_ellipsoid_res() const;
	INLINE void set_triangle_mesh_rand_constraints(bool triangleMeshRandConstraints);
	INLINE bool get_triangle_mesh_rand_constraints() const;
	void set_tetgen_ele_face_node(const ValueList_string& eleFaceNode, bool areFiles = true);
	INLINE ValueList_string get_tetgen_ele_face_node() const;
	INLINE void set_tetgen_facelinks(bool tetgenFacelinks);
	INLINE bool get_tetgen_facelinks() const;
	INLINE void set_tetgen_tetralinks(bool tetgenTetralinks);
	INLINE bool get_tetgen_tetralinks() const;
	INLINE void set_tetgen_facesfromtetras(bool tetgenFacesfromtetras);
	INLINE bool get_tetgen_facesfromtetras() const;
	// Python Properties
	MAKE_PROPERTY(body_type, get_body_type, set_body_type);
	MAKE_PROPERTY(group_mask, get_group_mask, set_group_mask);
	MAKE_PROPERTY(collide_mask, get_collide_mask, set_collide_mask);
	MAKE_PROPERTY(total_mass, get_total_mass);
	MAKE_PROPERTY(from_faces, get_from_faces, set_from_faces);
	MAKE_PROPERTY(rope_points, get_rope_points, set_rope_points);
	MAKE_PROPERTY(rope_fixeds, get_rope_fixeds, set_rope_fixeds);
	MAKE_PROPERTY(rope_res, get_rope_res, set_rope_res);
	MAKE_PROPERTY(patch_points, get_patch_points, set_patch_points);
	MAKE_PROPERTY(patch_fixeds, get_patch_fixeds, set_patch_fixeds);
	MAKE_PROPERTY(patch_resxy, get_patch_resxy, set_patch_resxy);
	MAKE_PROPERTY(patch_gendiags, get_patch_gendiags, set_patch_gendiags);
	MAKE_PROPERTY(ellipsoid_center, get_ellipsoid_center, set_ellipsoid_center);
	MAKE_PROPERTY(ellipsoid_radius, get_ellipsoid_radius, set_ellipsoid_radius);
	MAKE_PROPERTY(ellipsoid_res, get_ellipsoid_res, set_ellipsoid_res);
	MAKE_PROPERTY(triangle_mesh_rand_constraints, get_triangle_mesh_rand_constraints, set_triangle_mesh_rand_constraints);
	MAKE_PROPERTY(tetgen_ele_face_node, get_tetgen_ele_face_node);
	MAKE_PROPERTY(tetgen_facelinks, get_tetgen_facelinks, set_tetgen_facelinks);
	MAKE_PROPERTY(tetgen_tetralinks, get_tetgen_tetralinks, set_tetgen_tetralinks);
	MAKE_PROPERTY(tetgen_facesfromtetras, get_tetgen_facesfromtetras, set_tetgen_facesfromtetras);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	//Construction
	INLINE float get_air_density() const;
	INLINE float get_water_density() const;
	INLINE float get_water_offset() const;
	INLINE float get_max_displacement() const;
	INLINE LVector3f get_water_normal() const;
	//Configuration
	INLINE BT3SoftBodyConfig get_config() const;
	INLINE void set_mass(int node, float mass);
	INLINE float get_mass(int node) const;
	INLINE void set_total_density(float density);
	INLINE void set_volume_mass(float mass);
	INLINE void set_volume_density(float density);
	INLINE void set_rest_length_scale(float restLength);
	INLINE float get_rest_length_scale() const;
	INLINE void reset_link_rest_lengths();
	INLINE void set_wind_velocity(const LVector3f &velocity);
	INLINE LVector3f get_wind_velocity() const;
	INLINE float get_volume() const;
	INLINE ValueList_LPoint3f get_aabb() const;
	INLINE void set_pose(bool bvolume, bool bframe);
	INLINE int generate_bending_constraints(int distance,
			BT3SoftBodyMaterial mat =
					BT3SoftBodyMaterial(BT3SoftBodyMaterial::_dummy));
	INLINE void randomize_constraints();
	//Nodes
	INLINE BT3SoftBodyNode get_node(int idx) const;
	INLINE int get_num_nodes() const;
	MAKE_SEQ(get_nodes, get_num_nodes, get_node);
	int get_closest_node_index(LPoint3f point) const;
	//Materials
	INLINE BT3SoftBodyMaterial get_material(int idx) const;
	INLINE int get_num_materials() const;
	MAKE_SEQ(get_materials, get_num_materials, get_material);
	INLINE BT3SoftBodyMaterial append_material();
	//Clusters
	INLINE int get_num_clusters() const;
	INLINE LPoint3f get_cluster_com(int cluster) const;
	INLINE int generate_clusters(int k, int maxiterations = 8192);
	INLINE void release_cluster(int cluster);
	INLINE void release_all_clusters();
	INLINE void set_cluster_node_damping(int cluster, float value);
	INLINE float get_cluster_node_damping(int cluster) const;
	INLINE void set_cluster_linear_damping(int cluster, float value);
	INLINE float get_cluster_linear_damping(int cluster) const;
	INLINE void set_cluster_angular_damping(int cluster, float value);
	INLINE float get_cluster_angular_damping(int cluster) const;
	INLINE void set_cluster_matching(int cluster, float value);
	INLINE float get_cluster_matching(int cluster) const;
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	INLINE PyObject* get_collision_object_attrs();
	// Python Properties
	MAKE_PROPERTY(collision_object_attrs, get_collision_object_attrs);
#else
	inline CollisionObjectAttrs* get_collision_object_attrs();
#endif //PYTHON_BUILD
	// Python Properties
	MAKE_PROPERTY(air_density, get_air_density);
	MAKE_PROPERTY(water_density, get_water_density);
	MAKE_PROPERTY(water_offset, get_water_offset);
	MAKE_PROPERTY(max_displacement, get_max_displacement);
	MAKE_PROPERTY(water_normal, get_water_normal);
	MAKE_PROPERTY(config, get_config);
	MAKE_PROPERTY(rest_length_scale, get_rest_length_scale, set_rest_length_scale);
	MAKE_PROPERTY(wind_velocity, get_wind_velocity, set_wind_velocity);
	MAKE_PROPERTY(volume, get_volume);
	MAKE_PROPERTY(aabb, get_aabb);
	MAKE_PROPERTY(num_nodes, get_num_nodes);
	MAKE_PROPERTY(num_materials, get_num_materials);
	MAKE_PROPERTY(num_clusters, get_num_clusters);
	MAKE_SEQ_PROPERTY(nodes, get_num_nodes, get_node);
	MAKE_SEQ_PROPERTY(materials, get_num_materials, get_material);
	///@}

	/**
	 * \name PHYSICAL ACTIONS
	 */
	///@{
	//Structure
	INLINE void append_anchor(int node, PT(BT3RigidBody) body,
			bool disableCollisionBetweenLinkedBodies = false, float influence = 1);
	INLINE void append_anchor(int node, PT(BT3RigidBody) body,
			const LVector3f& localPivot, bool disableCollisionBetweenLinkedBodies = false,
			float influence = 1);
	void append_linear_joint(const BT3SoftBodyLJointSpecs& specs, const NodePath& object);
	void append_angular_joint(const BT3SoftBodyAJointSpecs& specs, const NodePath& object);
	INLINE void add_force(const LVector3f& force);
	INLINE void add_force(const LVector3f& force, int node);
	INLINE void add_aero_force_to_node(const LVector3f& windVelocity, int nodeIndex);
	INLINE void set_velocity(const LVector3f& velocity);
	INLINE void add_velocity(const LVector3f& velocity);
	INLINE void add_velocity(const LVector3f& velocity, int node);
	INLINE void transform(CPT(TransformState) trs);
	INLINE void translate(const LPoint3f& trs);
	INLINE void rotate(const LQuaternionf& rot);
	INLINE void scale(const LVecBase3f& scl);
	int ray_test(const LVector3f& rayFrom, const LVector3f& rayTo);
	INLINE void reoptimize_link_order();
	// Python Properties
	///@}

	/**
	 * \name OUTPUT
	 */
	///@{
	void output(ostream &out) const;
	///@}

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	/**
	 * \name PYTHON UPDATE CALLBACK
	 */
	///@{
	void set_update_callback(PyObject *clbk);
	///@}
#else
	/**
	 * \name C++ UPDATE CALLBACK
	 */
	///@{
	typedef void (*UPDATECALLBACKFUNC)(PT(BT3SoftBody));
	void set_update_callback(UPDATECALLBACKFUNC value);
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name TypedWritable API
	 */
	///@{
	void post_process_from_bam();
	///@}

public:
	/**
	 * \name C++ ONLY
	 * Library & support low level related methods.
	 */
	///@{
	inline btSoftBody& get_soft_body();
	inline const btSoftBody& get_soft_body() const;
	///@}

	// Explicitly deleted copy constructor and copy assignment operator.
	BT3SoftBody(const BT3SoftBody&) = delete;
	BT3SoftBody& operator=(const BT3SoftBody&) = delete;

#ifndef CPPPARSER
protected:
	friend void unref_delete<BT3SoftBody>(BT3SoftBody*);
	friend class GamePhysicsManager;

	BT3SoftBody(const string& name);
	virtual ~BT3SoftBody();

private:
	///The owner object this BT3SoftBody is attached to.
	NodePath mObjectNP;
	///The reference node path.
	NodePath mReferenceNP;
	///The setup flag.
	bool mSetup;
	///Bullet soft body reference.
	btSoftBody* mSoftBody;
	///@{
	///Physical parameters.
	//construction.
	BitMask32 mGroupMask, mCollideMask;
	bt3::SoftBodyData mSoftBodyData;
	//dynamic
#if defined(PYTHON_BUILD)
	PyObject *mCollisionObjectAttrsPy;
#endif //PYTHON_BUILD
	CollisionObjectAttrs* mCollisionObjectAttrs;
	///@}

	inline void do_reset();
	void do_initialize();
	void do_finalize();

	///Helpers.
	bt3::SoftBodyData::SoftBodyType do_get_soft_body_type(const string& name) const;
	void do_get_points(const string& pointSpecsStr, ValueList_LPoint3f& pointsOut);
	BitMask32 do_get_mask(const string& name) const;
	struct TetraBunny
	{
#include "bullet3/examples/SoftDemo/bunny.inl"
	};
	CollisionObjectAttrs* do_create_collision_object_attrs();
#if defined(PYTHON_BUILD)
	PyObject *do_create_collision_object_attrs_py();
#endif //PYTHON_BUILD

#if defined(PYTHON_BUILD)
	/**
	 * \name Python callback.
	 */
	///@{
	PyObject *mSelf, *mUpdateCallback;
	///@}
#else
	/**
	 * \name C++ callback.
	 */
	///@{
	UPDATECALLBACKFUNC mUpdateCallback;
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name SERIALIZATION ONLY SETTINGS.
	 */
	///@{

	// persistent storage for serialized data
	bool mBuildFromBam;
	Datagram mOutDatagram;
	DatagramIterator mScan;
	unordered_map<int, PT(PandaNode)> mAnchorToRigidBodydMap;
	unordered_map<int, PT(PandaNode)> mJointToPhysicObjectdMap;
	unsigned int mNextId;
	///@}
#endif //CPPPARSER

public:
	/**
	 * \name TypedWritable API
	 */
	///@{
	static void register_with_read_factory();
	virtual void write_datagram (BamWriter *manager, Datagram &dg) override;
	virtual int complete_pointers(TypedWritable **p_list, BamReader *manager) override;
	///@}

#ifndef CPPPARSER
protected:
	static TypedWritable *make_from_bam(const FactoryParams &params);
	virtual void fillin(DatagramIterator &scan, BamReader *manager) override;
	void do_write_soft_body_data(Datagram &dg);
	void do_read_soft_body_data(DatagramIterator &scan);
	void do_write_btMatrix3x3(const btMatrix3x3& matrix, Datagram &dg);
	void do_read_btMatrix3x3(btMatrix3x3& matrix, DatagramIterator &scan);
	unsigned int do_get_uid(void* object, bool createIfNotFound,
			unordered_map<unsigned int, void*>& idToObjectMap,
			unordered_map<void*, unsigned int>& objectToIdMap);
	void do_set_uid(unsigned int uid, void* object,
			unordered_map<unsigned int, void*>& idToObjectMap,
			unordered_map<void*, unsigned int>& objectToIdMap);
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(BT3SoftBody,PandaNode)
};

INLINE ostream &operator << (ostream &out, const BT3SoftBody & soft_body);

///inline
#include "bt3SoftBody.I"

#endif /* PHYSICS_SOURCE_BT3SOFTBODY_H_ */
