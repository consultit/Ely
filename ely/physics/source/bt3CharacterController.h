/**
 * \file bt3CharacterController.h
 *
 * \date 2017-08-09
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_BT3CHARACTERCONTROLLER_H_
#define PHYSICS_SOURCE_BT3CHARACTERCONTROLLER_H_

#include "gamePhysicsManager.h"
#include "physics_includes.h"
#include "bt3Ghost.h"

#ifndef CPPPARSER
#include "support/characterController.h"
#endif //CPPPARSER

/**
 * BT3CharacterController is a PandaNode representing a "characterController
 * controller" of the Bullet library.
 *
 * The control is accomplished through physics.\n
 * It constructs a character controller with a "character" represented by a
 * BT3Ghost.\n
 * The up vector defaults to the Z axis.\n
 * When enabled, BT3CharacterController can throw these events:
 * - when it stands on ground (default: <objectName>_OnGround).
 * - when it soars in air (default: <objectName>_InAir).
 * These Event are thrown continuously until the object keeps moving/being
 * steady, at a frequency which is the minimum between the fps and the frequency
 * specified (which defaults to 30 times per seconds). Both these Events have as
 * argument this BT3CharacterController involved in the standing/soaring.
 *
 * \note All physical quantities are expressed in the same units of measurement
 * used by Bullet.
 *
 * > **BT3CharacterController text parameters**:
 * param | type | default | note
 * ------|------|---------|-----
 * | *thrown_events*			|single| - | specified as "event1@[event_name1]@[frequency1][:...[:eventN@[event_nameN]@[frequencyN]]]" with eventX = on_ground,in_air
 * | *object*  					|single| - | the character's object (ghost)
 * | *up_vector*				|single| 0.0,0.0,1.0 | -
 * | *gravity*  				|single| 29.4 | == 3G
 * | *step_height*  			|single| 1.0 | -
 * | *max_linear_speed*  		|single| 60.0 | -
 * | *max_angular_speed*  		|single| 1.6 | -
 * | *linear_accel*  			|single| 30.0 | -
 * | *angular_accel*  			|single| 0.8 | -
 * | *linear_friction*  		|single| 2.0 | -
 * | *angular_friction*  		|single| 2.0 | -
 * | *stop_threshold*	  		|single| 0.01 | -
 * | *local_linear_movement*	|single| *true* | -
 * | *linear_damping*  			|single| 0.0 | -
 * | *angular_damping*  		|single| 0.0 | -
 * | *fall_speed*  				|single| 55.0 | -
 * | *jump_speed*  				|single| 10.0 | -
 * | *max_slope*  				|single| 0.79 | -
 * | *max_penetration_depth* 	|single| 0.2 | -
 * | *max_jump_height*  		|single| 1.0 | -
 * | *forward*  				|single| *enabled* | -
 * | *backward*  				|single| *enabled* | -
 * | *up*  						|single| *enabled* | -
 * | *down*  					|single| *enabled* | -
 * | *head_left*  				|single| *enabled* | -
 * | *head_right*  				|single| *enabled* | -
 * | *strafe_left*  			|single| *enabled* | -
 * | *strafe_right*  			|single| *enabled* | -
 * | *jump*  					|single| *enabled* | -
 *
 * \note parts inside [] are optional.\n
 */

class EXPCL_PHYSICS BT3CharacterController: public PandaNode //fixme should be derived from TypedWritableReferenceCount and Namable only ?
{
PUBLISHED:

	/**
	 * CharacterController thrown events.
	 */
	enum BT3EventThrown: unsigned char
	{
		ONGROUND,
		INAIR,
	};

	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	virtual ~BT3CharacterController();
#endif //CPPPARSER

	/**
	 * \name OWNER OBJECT
	 */
	///@{
	INLINE void set_owner_object(const NodePath& object);
	INLINE NodePath get_owner_object() const;
	// Python Properties
	MAKE_PROPERTY(owner_object, get_owner_object, set_owner_object);
	///@}

	/**
	 * \name CHARACTERCONTROLLER
	 */
	///@{
	int setup();
	int cleanup();
	void update(float dt);
	///@}

	/**
	 * \name CONSTRUCTION PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	// Python Properties
	///@}

	/**
	 * \name DYNAMIC PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE PT(BT3Ghost) get_character() const;
	INLINE void set_max_linear_speed(const LVector3f& linearSpeed);
	INLINE const LVector3f& get_max_linear_speed() const;
	INLINE void set_max_angular_speed(float angularSpeed);
	INLINE float get_max_angular_speed() const;
	INLINE void set_linear_accel(const LVector3f& linearAccel);
	INLINE const LVector3f& get_linear_accel() const;
	INLINE void set_angular_accel(float angularAccel);
	INLINE float get_angular_accel() const;
	INLINE void set_linear_friction(float linearFriction);
	INLINE float get_linear_friction() const;
	INLINE void set_angular_friction(float angularFriction);
	INLINE float get_angular_friction() const;
	INLINE void set_stop_threshold(float threshold);
	INLINE float get_stop_threshold() const;
	INLINE void set_local_linear_movement(bool value);
	INLINE bool get_local_linear_movement() const;
	INLINE void set_up_vector(const LVector3f& up);
	INLINE LVector3f get_up_vector() const;
	INLINE void set_gravity(float gravity);
	INLINE float get_gravity() const;
	INLINE void set_linear_damping(float d);
	INLINE float get_linear_damping() const;
	INLINE void set_angular_damping(float d);
	INLINE float get_angular_damping() const;
	INLINE void set_step_height(float h);
	INLINE float get_step_height() const;
	INLINE void set_fall_speed (float fallSpeed);
	INLINE float get_fall_speed() const;
	INLINE void set_jump_speed (float jumpSpeed);
	INLINE float get_jump_speed() const;
	INLINE void set_max_slope(float slopeRadians);
	INLINE float get_max_slope() const;
	INLINE void set_max_penetration_depth(float d);
	INLINE float get_max_penetration_depth() const;
	INLINE void set_max_jump_height(float maxJumpHeight);//fixme unused as of 2017-08-11
	INLINE void set_use_ghost_sweep_test(bool useGhostObjectSweepTest);
	INLINE void set_up_interpolate (bool value);
	INLINE const LVector3f& get_linear_speed() const;
	INLINE float get_angular_speed() const;
	//movement enablers
	INLINE void set_enable_forward(bool value);
	INLINE bool get_enable_forward() const;
	INLINE void set_enable_backward(bool value);
	INLINE bool get_enable_backward() const;
	INLINE void set_enable_up(bool value);
	INLINE bool get_enable_up() const;
	INLINE void set_enable_down(bool value);
	INLINE bool get_enable_down() const;
	INLINE void set_enable_head_left(bool value);
	INLINE bool get_enable_head_left() const;
	INLINE void set_enable_head_right(bool value);
	INLINE bool get_enable_head_right() const;
	INLINE void set_enable_strafe_left(bool value);
	INLINE bool get_enable_strafe_left() const;
	INLINE void set_enable_strafe_right(bool value);
	INLINE bool get_enable_strafe_right() const;
	INLINE void set_enable_jump(bool value);
	INLINE bool get_enable_jump() const;
	//movement activators
	INLINE void set_move_forward(bool value);
	INLINE bool get_move_forward() const;
	INLINE void set_move_backward(bool value);
	INLINE bool get_move_backward() const;
	INLINE void set_move_up(bool value);
	INLINE bool get_move_up() const;
	INLINE void set_move_down(bool value);
	INLINE bool get_move_down() const;
	INLINE void set_rotate_head_left(bool value);
	INLINE bool get_rotate_head_left() const;
	INLINE void set_rotate_head_right(bool value);
	INLINE bool get_rotate_head_right() const;
	INLINE void set_move_strafe_left(bool value);
	INLINE bool get_move_strafe_left() const;
	INLINE void set_move_strafe_right(bool value);
	INLINE bool get_move_strafe_right() const;
	INLINE void set_jump(bool value);
	INLINE bool get_jump() const;
	// Python Properties
	MAKE_PROPERTY(character, get_character);
	MAKE_PROPERTY(max_linear_speed, get_max_linear_speed, set_max_linear_speed);
	MAKE_PROPERTY(max_angular_speed, get_max_angular_speed, set_max_angular_speed);
	MAKE_PROPERTY(linear_accel, get_linear_accel, set_linear_accel);
	MAKE_PROPERTY(angular_accel, get_angular_accel, set_angular_accel);
	MAKE_PROPERTY(linear_friction, get_linear_friction, set_linear_friction);
	MAKE_PROPERTY(angular_friction, get_angular_friction, set_angular_friction);
	MAKE_PROPERTY(stop_threshold, get_stop_threshold, set_stop_threshold);
	MAKE_PROPERTY(local_linear_movement, get_local_linear_movement, set_local_linear_movement);
	MAKE_PROPERTY(up_vector, get_up_vector, set_up_vector);
	MAKE_PROPERTY(gravity, get_gravity, set_gravity);
	MAKE_PROPERTY(linear_damping, get_linear_damping, set_linear_damping);
	MAKE_PROPERTY(angular_damping, get_angular_damping, set_angular_damping);
	MAKE_PROPERTY(step_height, get_step_height, set_step_height);
	MAKE_PROPERTY(fall_speed, get_fall_speed, set_fall_speed);
	MAKE_PROPERTY(jump_speed, get_jump_speed, set_jump_speed);
	MAKE_PROPERTY(max_slope, get_max_slope, set_max_slope);
	MAKE_PROPERTY(max_penetration_depth, get_max_penetration_depth, set_max_penetration_depth);
	MAKE_PROPERTY(linear_speed, get_linear_speed);
	MAKE_PROPERTY(angular_speed, get_angular_speed);
	MAKE_PROPERTY(enable_forward, get_enable_forward, set_enable_forward);
	MAKE_PROPERTY(enable_backward, get_enable_backward, set_enable_backward);
	MAKE_PROPERTY(enable_up, get_enable_up, set_enable_up);
	MAKE_PROPERTY(enable_down, get_enable_down, set_enable_down);
	MAKE_PROPERTY(enable_head_left, get_enable_head_left, set_enable_head_left);
	MAKE_PROPERTY(enable_head_right, get_enable_head_right, set_enable_head_right);
	MAKE_PROPERTY(enable_strafe_left, get_enable_strafe_left, set_enable_strafe_left);
	MAKE_PROPERTY(enable_strafe_right, get_enable_strafe_right, set_enable_strafe_right);
	MAKE_PROPERTY(enable_jump, get_enable_jump, set_enable_jump);
	MAKE_PROPERTY(move_forward, get_move_forward, set_move_forward);
	MAKE_PROPERTY(move_backward, get_move_backward, set_move_backward);
	MAKE_PROPERTY(move_up, get_move_up, set_move_up);
	MAKE_PROPERTY(move_down, get_move_down, set_move_down);
	MAKE_PROPERTY(rotate_head_left, get_rotate_head_left, set_rotate_head_left);
	MAKE_PROPERTY(rotate_head_right, get_rotate_head_right, set_rotate_head_right);
	MAKE_PROPERTY(move_strafe_left, get_move_strafe_left, set_move_strafe_left);
	MAKE_PROPERTY(move_strafe_right, get_move_strafe_right, set_move_strafe_right);
	MAKE_PROPERTY(jump, get_jump, set_jump);
	///@}

	/**
	 * \name PHYSICAL ACTIONS
	 */
	///@{
	INLINE void apply_jump(const LVector3f& v = LVector3f::zero());
	INLINE void apply_warp(const LVector3f& origin);
	INLINE void reset();
	INLINE bool get_on_ground() const;
	// Python Properties
	MAKE_PROPERTY(on_ground, get_on_ground);
	///@}

	/**
	 * \name EVENTS
	 */
	///@{
	void enable_throw_event(BT3EventThrown event, bool enable,
			float frequency = 30.0, const string& eventName = "");
	///@}

	/**
	 * \name OUTPUT
	 */
	///@{
	void output(ostream &out) const;
	///@}

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	/**
	 * \name PYTHON UPDATE CALLBACK
	 */
	///@{
	void set_update_callback(PyObject *clbk);
	///@}
#else
	/**
	 * \name C++ UPDATE CALLBACK
	 */
	///@{
	typedef void (*UPDATECALLBACKFUNC)(PT(BT3CharacterController));
	void set_update_callback(UPDATECALLBACKFUNC value);
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name TypedWritable API
	 */
	///@{
	void post_process_from_bam();
	///@}

public:
	/**
	 * \name C++ ONLY
	 * Library & support low level related methods.
	 */
	///@{
	inline btKinematicCharacterController& get_character_object();
	inline const btKinematicCharacterController& get_character_object() const;
	///@}

	// Explicitly deleted copy constructor and copy assignment operator.
	BT3CharacterController(const BT3CharacterController&) = delete;
	BT3CharacterController& operator=(const BT3CharacterController&) = delete;

#ifndef CPPPARSER
protected:
	friend void unref_delete<BT3CharacterController>(BT3CharacterController*);
	friend class GamePhysicsManager;

	BT3CharacterController(const string& name);
	virtual ~BT3CharacterController();

private:
	///The owner object this BT3CharacterController is attached to.
	NodePath mObjectNP;
	///The reference node path.
	NodePath mReferenceNP;
	///The setup flag.
	bool mSetup;
	///Bullet characterController object reference.
	btKinematicCharacterController* mCharacterController;
	///@{
	///Physical parameters.
	bt3::CharacterControllerData mCharacterControllerData;
	LVector3f mLinearAccel;
	float mAngularAccel;
	LVector3f mLinearSpeed, mMaxLinearSpeed, mMaxLinearSpeedSquared;
	float mAngularSpeed, mMaxAngularSpeed, mMaxAngularSpeedSquared;
	float mLinearFriction;
	float mAngularFriction;
	float mStopThreshold;
	bool mLocalLinearMovement;
	///Key controls and effective keys.
	bool mForward, mBackward, mHeadLeft, mHeadRight, mUp, mDown, mStrafeLeft,
	mStrafeRight, mJump;
	bool mForwardKey, mBackwardKey, mHeadLeftKey, mHeadRightKey, mUpKey,
	mDownKey, mStrafeLeftKey, mStrafeRightKey, mJumpKey;
	///@}

	inline void do_reset();
	void do_initialize();
	void do_finalize();

	/**
	 * \name CharacterController events.
	 */
	///@{
	ThrowEventData mOnGround, mInAir;
	void do_throw_event(ThrowEventData& eventData);
	///@}

#if defined(PYTHON_BUILD)
	/**
	 * \name Python callback.
	 */
	///@{
	PyObject *mSelf, *mUpdateCallback;
	///@}
#else
	/**
	 * \name C++ callback.
	 */
	///@{
	UPDATECALLBACKFUNC mUpdateCallback;
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name SERIALIZATION ONLY SETTINGS.
	 */
	///@{
	// persistent storage for serialized data
	bool mBuildFromBam;
	Datagram mOutDatagram;
	///@}
#endif //CPPPARSER

public:
	/**
	 * \name TypedWritable API
	 */
	///@{
	static void register_with_read_factory();
	virtual void write_datagram (BamWriter *manager, Datagram &dg) override;
	virtual int complete_pointers(TypedWritable **p_list, BamReader *manager) override;
	///@}

#ifndef CPPPARSER
protected:
	static TypedWritable *make_from_bam(const FactoryParams &params);
	virtual void fillin(DatagramIterator &scan, BamReader *manager) override;
	void do_write_character_controller_data(Datagram &dg);
	void do_read_character_controller_data(DatagramIterator &scan);
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(BT3CharacterController,PandaNode)
};

INLINE ostream &operator << (ostream &out, const BT3CharacterController & characterController);

///inline
#include "bt3CharacterController.I"

#endif /* PHYSICS_SOURCE_BT3CHARACTERCONTROLLER_H_ */
