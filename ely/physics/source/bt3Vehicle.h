/**
 * \file bt3Vehicle.h
 *
 * \date 2017-07-26
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_BT3VEHICLE_H_
#define PHYSICS_SOURCE_BT3VEHICLE_H_

#include "gamePhysicsManager.h"
#include "physics_includes.h"
#include "bt3RigidBody.h"
#include <unordered_map>

#ifndef CPPPARSER
#include "support/vehicle.h"
#endif //CPPPARSER

/**
 * BT3Vehicle is a PandaNode representing a "raycast vehicle" of the Bullet
 * library.
 *
 * The control is accomplished through physics.\n
 * It constructs a vehicle with a "chassis" represented by a BT3RigidBody.\n
 * The "wheels" are represented by a series of graphic objects that are passed
 * as parameters. Wheels are constructed on vehicle construction and destroyed
 * during vehicle destruction.\n
 * The default up axis is the Z axis.\n
 * When enabled, BT3Vehicle can throw these events:
 * - when it moves (default: <objectName>_Move).
 * - when it is steady (default: <objectName>_Steady).
 * These Event are thrown continuously until the object keeps moving/being
 * steady, at a frequency which is the minimum between the fps and the frequency
 * specified (which defaults to 30 times per seconds). Both these Events have as
 * argument this BT3Vehicle involved in the moving/being steady.
 *
 * \note All physical quantities are expressed in the same units of measurement
 * used by Bullet.
 *
 * > **BT3Vehicle text parameters**:
 * param | type | default | note
 * ------|------|---------|-----
 * | *thrown_events*				|single| - | specified as "event1@[event_name1]@[frequency1][:...[:eventN@[event_nameN]@[frequencyN]]]" with eventX = move,steady
 * | *up_axis*						|single| *z* | values: x,y,z
 * | *object*  						|single| - | the chassis's object (rigid body)
 * | *wheels_number*  				|single| 4 | 2 or 4
 * | *wheel_model*  				|single| - | specified as "modelF[:modelR]"
 * | *wheel_apply_steering*  		|single| *true:false* | specified as "steeringF[:steeringR]"
 * | *wheel_apply_engine_force*  	|single| *false:true* | specified as "engineForceF[:engineForceR]"
 * | *wheel_apply_brake*  			|single| *false:true* | specified as "brakeF[:brakeR]"
 * | *connection_height_factor* 	|single| 1.5:1.5 | specified as "valueF[:valueR]"
 * | *connection_width_factor*	 	|single| 0.3:0.3 | specified as "valueF[:valueR]"
 * | *connection_depth_factor* 		|single| 1.5:1.5 | specified as "valueF[:valueR]"
 * | *suspension_rest_lenght_factor*|single| 1.2:1.2 | specified as "valueF[:valueR]"
 * | *max_engine_force*				|single| 3.0 | -
 * | *max_brake*					|single| 1.5 | -
 * | *steering_clamp*				|single| 0.79 | -
 * | *steering_increment*			|single| 2.09 | -
 * | *steering_decrement*			|single| 1.05 | -
 * | *forward*  					|single| *enabled* | -
 * | *backward*  					|single| *enabled* | -
 * | *brake*  						|single| *enabled* | -
 * | *head_left*					|single| *enabled* | -
 * | *head_right*  					|single| *enabled* | -
 * | *suspension_stiffness*  		|single| 5.88 | -
 * | *suspension_compression*  		|single| 0.83 | -
 * | *suspension_damping*	  		|single| 0.88 | -
 * | *max_suspension_travel_cm*		|single| 500.0 | -
 * | *friction_slip*  				|single| 10.5 | -
 * | *max_suspension_force*			|single| 6000.0 | -
 *
 * \note parts inside [] are optional.\n
 */

class EXPCL_PHYSICS BT3Vehicle: public PandaNode //fixme should be derived from TypedWritableReferenceCount and Namable only ?
{
PUBLISHED:

	/**
	 * Vehicle thrown events.
	 */
	enum BT3EventThrown: unsigned char
	{
		MOVE,
		STEADY,
	};

	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	virtual ~BT3Vehicle();
#endif //CPPPARSER

	/**
	 * \name OWNER OBJECT
	 */
	///@{
	INLINE void set_owner_object(const NodePath& object);
	INLINE NodePath get_owner_object() const;
	// Python Properties
	MAKE_PROPERTY(owner_object, get_owner_object, set_owner_object);
	///@}

	/**
	 * \name VEHICLE
	 */
	///@{
	int setup();
	int cleanup();
	void update(float dt);
	///@}

	/**
	 * \name CONSTRUCTION PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE void set_up_axis(GamePhysicsManager::BT3UpAxis value);
	INLINE GamePhysicsManager::BT3UpAxis get_up_axis() const;
	INLINE void set_vehicle_tuning(const BT3VehicleTuning& tuning);
	INLINE BT3VehicleTuning get_vehicle_tuning() const;
	INLINE void set_wheels_number(int wheelNum);
	INLINE int get_wheels_number() const;
	INLINE void set_wheel_model(const NodePath& model, bool isFront);
	INLINE NodePath get_wheel_model(bool isFront) const;
	INLINE void set_connection_height_factor(float value, bool isFront);
	INLINE float get_connection_height_factor(bool isFront) const;
	INLINE void set_connection_width_factor(float value, bool isFront);
	INLINE float get_connection_width_factor(bool isFront) const;
	INLINE void set_connection_depth_factor(float value, bool isFront);
	INLINE float get_connection_depth_factor(bool isFront) const;
	INLINE void set_suspension_rest_lenght_factor(float value, bool isFront);
	INLINE float get_suspension_rest_lenght_factor(bool isFront) const;
	// Python Properties
	MAKE_PROPERTY(up_axis, get_up_axis, set_up_axis);
	MAKE_PROPERTY(vehicle_tuning, get_vehicle_tuning, set_vehicle_tuning);
	MAKE_PROPERTY(wheels_number, get_wheels_number, set_wheels_number);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE PT(BT3RigidBody) get_chassis() const;
	INLINE BT3WheelInfo get_wheel_info(int wheel) const;
	INLINE void set_max_engine_force(float value);
	INLINE float get_max_engine_force() const;
	INLINE void set_max_brake(float value);
	INLINE float get_max_brake() const;
	INLINE void set_steering_clamp(float value);
	INLINE float get_steering_clamp() const;
	INLINE void set_steering_increment(float value);
	INLINE float get_steering_increment() const;
	INLINE void set_steering_decrement(float value);
	INLINE float get_steering_decrement() const;
	//movement enablers
	INLINE void set_enable_forward(bool value);
	INLINE bool get_enable_forward() const;
	INLINE void set_enable_backward(bool value);
	INLINE bool get_enable_backward() const;
	INLINE void set_enable_brake(bool value);
	INLINE bool get_enable_brake() const;
	INLINE void set_enable_head_left(bool value);
	INLINE bool get_enable_head_left() const;
	INLINE void set_enable_head_right(bool value);
	INLINE bool get_enable_head_right() const;
	//movement activators
	INLINE void set_move_forward(bool value);
	INLINE bool get_move_forward() const;
	INLINE void set_move_backward(bool value);
	INLINE bool get_move_backward() const;
	INLINE void set_brake(bool value);
	INLINE bool get_brake() const;
	INLINE void set_rotate_head_left(bool value);
	INLINE bool get_rotate_head_left() const;
	INLINE void set_rotate_head_right(bool value);
	INLINE bool get_rotate_head_right() const;
	// Python Properties
	MAKE_PROPERTY(chassis, get_chassis);
	MAKE_PROPERTY(max_engine_force, get_max_engine_force, set_max_engine_force);
	MAKE_PROPERTY(max_brake, get_max_brake, set_max_brake);
	MAKE_PROPERTY(steering_clamp, get_steering_clamp, set_steering_clamp);
	MAKE_PROPERTY(steering_increment, get_steering_increment, set_steering_increment);
	MAKE_PROPERTY(steering_decrement, get_steering_decrement, set_steering_decrement);
	MAKE_PROPERTY(enable_forward, get_enable_forward, set_enable_forward);
	MAKE_PROPERTY(enable_backward, get_enable_backward, set_enable_backward);
	MAKE_PROPERTY(enable_brake, get_enable_brake, set_enable_brake);
	MAKE_PROPERTY(enable_head_left, get_enable_head_left, set_enable_head_left);
	MAKE_PROPERTY(enable_head_right, get_enable_head_right, set_enable_head_right);
	MAKE_PROPERTY(move_forward, get_move_forward, set_move_forward);
	MAKE_PROPERTY(move_backward, get_move_backward, set_move_backward);
	MAKE_PROPERTY(brake, get_brake, set_brake);
	MAKE_PROPERTY(rotate_head_left, get_rotate_head_left, set_rotate_head_left);
	MAKE_PROPERTY(rotate_head_right, get_rotate_head_right, set_rotate_head_right);
	///@}

	/**
	 * \name PHYSICAL ACTIONS
	 */
	///@{
	INLINE void set_wheel_apply_steering(bool enable, bool isFront);
	INLINE bool get_wheel_apply_steering(bool isFront) const;
	INLINE void set_wheel_apply_engine_force(bool enable, bool isFront);
	INLINE bool get_wheel_apply_engine_force(bool isFront) const;
	INLINE void set_wheel_apply_brake(bool enable, bool isFront);
	INLINE bool get_wheel_apply_brake(bool isFront) const;
	INLINE void apply_steering_value(float steering, int wheel);
	INLINE void apply_engine_force(float force, int wheel);
	INLINE void apply_brake(float brake, int wheel);
	INLINE void set_pitch_control(float pitch);
	INLINE void reset_suspension();
	INLINE LVector3f get_forward_vector() const;
	INLINE float get_current_speed_km_hour() const;
	// Python Properties
	MAKE_PROPERTY(forward_vector, get_forward_vector);
	MAKE_PROPERTY(current_speed_km_hour, get_current_speed_km_hour);
	///@}

	/**
	 * \name EVENTS
	 */
	///@{
	void enable_throw_event(BT3EventThrown event, bool enable,
			float frequency = 30.0, const string& eventName = "");
	///@}

	/**
	 * \name OUTPUT
	 */
	///@{
	void output(ostream &out) const;
	///@}

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	/**
	 * \name PYTHON UPDATE CALLBACK
	 */
	///@{
	void set_update_callback(PyObject *clbk);
	///@}
#else
	/**
	 * \name C++ UPDATE CALLBACK
	 */
	///@{
	typedef void (*UPDATECALLBACKFUNC)(PT(BT3Vehicle));
	void set_update_callback(UPDATECALLBACKFUNC value);
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name TypedWritable API
	 */
	///@{
	void post_process_from_bam();
	///@}

public:
	/**
	 * \name C++ ONLY
	 * Library & support low level related methods.
	 */
	///@{
	inline btRaycastVehicle& get_vehicle_object();
	inline const btRaycastVehicle& get_vehicle_object() const;
	///@}

	// Explicitly deleted copy constructor and copy assignment operator.
	BT3Vehicle(const BT3Vehicle&) = delete;
	BT3Vehicle& operator=(const BT3Vehicle&) = delete;

#ifndef CPPPARSER
protected:
	friend void unref_delete<BT3Vehicle>(BT3Vehicle*);
	friend class GamePhysicsManager;

	BT3Vehicle(const string& name);
	virtual ~BT3Vehicle();

private:
	///The owner object this BT3Vehicle is attached to.
	NodePath mObjectNP;
	///The reference node path.
	NodePath mReferenceNP;
	///The setup flag.
	bool mSetup;
	///Bullet vehicle object reference.
	btRaycastVehicle* mVehicle;
	///@{
	///Physical parameters.
	bt3::VehicleData mVehicleData;
	NodePath mWheelModel[2];
	bool mWheelApplySteering[2], mWheelApplyEngineForce[2],mWheelApplyBrake[2];
	float mMaxEngineForce, mMaxBrake;
	// steering related values are in radians
	float mSteering, mSteeringClamp, mSteeringIncrement, mSteeringDecrement;
	///Key controls and effective keys.
	bool mForward, mBackward, mBrake, mHeadLeft, mHeadRight;
	bool mForwardKey, mBackwardKey, mBrakeKey, mHeadLeftKey, mHeadRightKey;
	///@}

	inline void do_reset();
	void do_initialize();
	void do_finalize();

	/**
	 * \name Vehicle events.
	 */
	///@{
	ThrowEventData mMove, mSteady;
	void do_throw_event(ThrowEventData& eventData);
	///@}

#if defined(PYTHON_BUILD)
	/**
	 * \name Python callback.
	 */
	///@{
	PyObject *mSelf, *mUpdateCallback;
	///@}
#else
	/**
	 * \name C++ callback.
	 */
	///@{
	UPDATECALLBACKFUNC mUpdateCallback;
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name SERIALIZATION ONLY SETTINGS.
	 */
	///@{
	// persistent storage for serialized data
	bool mBuildFromBam;
	Datagram mOutDatagram;
	///@}
#endif //CPPPARSER

public:
	/**
	 * \name TypedWritable API
	 */
	///@{
	static void register_with_read_factory();
	virtual void write_datagram (BamWriter *manager, Datagram &dg) override;
	virtual int complete_pointers(TypedWritable **p_list, BamReader *manager) override;
	///@}

#ifndef CPPPARSER
protected:
	static TypedWritable *make_from_bam(const FactoryParams &params);
	virtual void fillin(DatagramIterator &scan, BamReader *manager) override;
	void do_write_vehicle_data(Datagram &dg);
	void do_read_vehicle_data(DatagramIterator &scan);
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(BT3Vehicle,PandaNode)
};

INLINE ostream &operator << (ostream &out, const BT3Vehicle & vehicle);

///inline
#include "bt3Vehicle.I"

#endif /* PHYSICS_SOURCE_BT3VEHICLE_H_ */
