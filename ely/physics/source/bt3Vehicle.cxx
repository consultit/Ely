/**
 * \file bt3Vehicle.cxx
 *
 * \date 2017-07-26
 * \author consultit
 */

#if defined(_WIN32)
#include "support_os/pstdint.h"
#endif

#include "bt3Vehicle.h"
#include "throw_event.h"
#include "nodePathCollection.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
extern struct Dtool_PyTypedObject Dtool_BT3Vehicle;
#endif //PYTHON_BUILD

///BT3Vehicle definitions
/**
 *
 */
BT3Vehicle::BT3Vehicle(const string& name) :
		PandaNode(name)
{
	do_reset();
}

/**
 *
 */
BT3Vehicle::~BT3Vehicle()
{
}

/**
 * Initializes the BT3Vehicle with starting settings.
 * \note Internal use only.
 */
void BT3Vehicle::do_initialize()
{
	GamePhysicsManager* mTmpl = GamePhysicsManager::get_global_ptr();
	// set thrown events
	string thrownEvents = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("thrown_events"));
	unsigned int idx1, valueNum1;
	pvector<string> paramValuesStr1, paramValuesStr2;
	//events specified
	//event1@[event_name1]@[frequency1][:...[:eventN@[event_nameN]@[frequencyN]]]
	paramValuesStr1 = parseCompoundString(thrownEvents, ':');
	valueNum1 = paramValuesStr1.size();
	for (idx1 = 0; idx1 < valueNum1; ++idx1)
	{
		//eventX@[event_nameX]@[frequencyX]
		paramValuesStr2 = parseCompoundString(paramValuesStr1[idx1], '@');
		if (paramValuesStr2.size() >= 3)
		{
			// get event
			BT3EventThrown event;
			// get event name
			string name;
			// get frequency
			float frequency = abs(STRTOF(paramValuesStr2[2].c_str(), nullptr));
			//get event
			if (paramValuesStr2[0] == "move")
			{
				event = MOVE;
				name = paramValuesStr2[1].empty() ?
						get_name() + "_Move":
						paramValuesStr2[1];
			}
			else if (paramValuesStr2[0] == "steady")
			{
				event = STEADY;
				name = paramValuesStr2[1].empty() ?
						get_name() + "_Steady":
						paramValuesStr2[1];
			}
			else
			{
				//paramValuesStr2[0] is not a suitable event:
				//continue with the next event
				continue;
			}
			//enable the event
			enable_throw_event(event, true, frequency, name);
		}
	}
	// up axis
	string upAxis = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("up_axis"));
	if (upAxis == string("x"))
	{
		set_up_axis(GamePhysicsManager::X_up);
	}
	else if (upAxis == string("y"))
	{
		set_up_axis(GamePhysicsManager::Y_up);
	}
	else if (upAxis == string("z"))
	{
		set_up_axis(GamePhysicsManager::Z_up);
	}
	else if (upAxis == string("no_up"))
	{
		set_up_axis(GamePhysicsManager::NO_up);
	}
	// wheels number
	int valueI = strtol(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("wheels_number")).c_str(), nullptr, 0);
	set_wheels_number(valueI);
	//common tmp variables
	string param;
	pvector<string> paramStr;
	// wheel model (default = modelF)
	param = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("wheel_model"));
	paramStr = parseCompoundString(param, ':');
	if (paramStr.size() > 0)
	{
		for(int i = 0; i < 2; i++)
		{
			if (i < (int)paramStr.size())
			{
				// search models under reference node
				NodePath model = mReferenceNP.find(string("**/") + paramStr[i]);
				if (!model.is_empty())
				{
					mWheelModel[i] = model;
				}
			}
			else
			{
				mWheelModel[i] = mWheelModel[0];
			}
		}
	}
	// wheel apply steering (default = true:false)
	param = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("wheel_apply_steering"));
	paramStr = parseCompoundString(param, ':');
	if (paramStr.size() > 0)
	{
		// front
		mWheelApplySteering[0] = (paramStr[0] == string("false") ? false : true);
		// rear
		if (paramStr.size() > 1)
		{
			mWheelApplySteering[1] = (paramStr[1] == string("true") ? true : false);
		}
	}
	// wheel apply engine force (default = false:true)
	param = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("wheel_apply_engine_force"));
	paramStr = parseCompoundString(param, ':');
	if (paramStr.size() > 0)
	{
		// front
		mWheelApplyEngineForce[0] = (paramStr[0] == string("true") ? true : false);
		// rear
		if (paramStr.size() > 1)
		{
			mWheelApplyEngineForce[1] = (paramStr[1] == string("false") ? false : true);
		}
	}
	// wheel apply brake (default = false:true)
	param = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("wheel_apply_brake"));
	paramStr = parseCompoundString(param, ':');
	if (paramStr.size() > 0)
	{
		// front
		mWheelApplyBrake[0] = (paramStr[0] == string("true") ? true : false);
		// rear
		if (paramStr.size() > 1)
		{
			mWheelApplyBrake[1] = (paramStr[1] == string("false") ? false : true);
		}
	}
	// connection height factor
	param = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("connection_height_factor"));
	paramStr = parseCompoundString(param, ':');
	if (paramStr.size() > 0)
	{
		// front
		mVehicleData.connHeightF[0] = STRTOF(paramStr[0].c_str(),nullptr);
		// rear
		if (paramStr.size() > 1)
		{
			mVehicleData.connHeightF[1] = STRTOF(paramStr[1].c_str(),nullptr);
		}
	}
	// connection width factor
	param = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("connection_width_factor"));
	paramStr = parseCompoundString(param, ':');
	if (paramStr.size() > 0)
	{
		// front
		mVehicleData.connWidthF[0] = STRTOF(paramStr[0].c_str(),nullptr);
		// rear
		if (paramStr.size() > 1)
		{
			mVehicleData.connWidthF[1] = STRTOF(paramStr[1].c_str(),nullptr);
		}
	}
	// connection depth factor
	param = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("connection_depth_factor"));
	paramStr = parseCompoundString(param, ':');
	if (paramStr.size() > 0)
	{
		// front
		mVehicleData.connDepthF[0] = STRTOF(paramStr[0].c_str(),nullptr);
		// rear
		if (paramStr.size() > 1)
		{
			mVehicleData.connDepthF[1] = STRTOF(paramStr[1].c_str(),nullptr);
		}
	}
	// suspension rest length factor
	param = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("suspension_rest_lenght_factor"));
	paramStr = parseCompoundString(param, ':');
	if (paramStr.size() > 0)
	{
		// front
		mVehicleData.suspRestLengthF[0] = abs(STRTOF(paramStr[0].c_str(),nullptr));
		// rear
		if (paramStr.size() > 1)
		{
			mVehicleData.suspRestLengthF[1] = abs(STRTOF(paramStr[1].c_str(),nullptr));
		}
	}
	// max engine force
	mMaxEngineForce = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("max_engine_force")).c_str(),nullptr);
	// max brake
	mMaxBrake = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("max_brake")).c_str(),nullptr);
	// steering clamp
	mSteeringClamp = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("steering_clamp")).c_str(),nullptr);
	// steering increment
	mSteeringIncrement = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("steering_increment")).c_str(),nullptr);
	// steering decrement
	mSteeringDecrement = STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("steering_decrement")).c_str(),nullptr);
	// forward
	mForwardKey = (mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("forward")) == string("disabled") ? false : true);
	// backward
	mBackwardKey = (mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("backward")) == string("disabled") ? false : true);
	// brake
	mBrakeKey = (mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("brake")) == string("disabled") ? false : true);
	// head left
	mHeadLeftKey = (mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("head_left")) == string("disabled") ? false : true);
	// head right
	mHeadRightKey = (mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
					string("head_right")) == string("disabled") ? false : true);
	// suspension stiffness
	mVehicleData.tuning.m_suspensionStiffness =
			abs(STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
							string("suspension_stiffness")).c_str(),nullptr));
	// suspension compression
	mVehicleData.tuning.m_suspensionCompression =
			abs(STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
							string("suspension_compression")).c_str(),nullptr));
	// suspension damping
	mVehicleData.tuning.m_suspensionDamping =
			abs(STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
							string("suspension_damping")).c_str(),nullptr));
	// max suspension travel cm
	mVehicleData.tuning.m_maxSuspensionTravelCm =
			abs(STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
							string("max_suspension_travel_cm")).c_str(),nullptr));
	// friction slip
	mVehicleData.tuning.m_frictionSlip =
			abs(STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
							string("friction_slip")).c_str(),nullptr));
	// max suspension force
	mVehicleData.tuning.m_maxSuspensionForce =
			abs(STRTOF(mTmpl->get_parameter_value(GamePhysicsManager::VEHICLE,
							string("max_suspension_force")).c_str(),nullptr));
	// object
	string object = mTmpl->get_parameter_value(
			GamePhysicsManager::VEHICLE, string("object"));
	if(!object.empty())
	{
		// search object under reference node
		NodePath objectNP = mReferenceNP.find(string("**/") + object);
		if (!objectNP.is_empty())
		{
			mObjectNP = objectNP;
			setup();
		}
	}
}

/**
 * Sets the BT3Vehicle up for the given object (if not empty), given the
 * shape's type and size (among others).
 */
int BT3Vehicle::setup()
{
	// continue if vehicle has not been setup yet
	CONTINUE_IF_ELSE_R(!mSetup, RESULT_SUCCESS)

	// continue if object not empty
	CONTINUE_IF_ELSE_R(!mObjectNP.is_empty(), RESULT_ERROR)

	// owner object's node must be a BT3RigidBody to be used as chassis
	CONTINUE_IF_ELSE_R(
			mObjectNP.node()->is_of_type(BT3RigidBody::get_class_type()),
			RESULT_ERROR)

	// when de-serializing: remove all existing wheel models
	if (mBuildFromBam)
	{
		NodePathCollection wheelNPs = mObjectNP.get_parent().find_all_matches(
				"**/*_wheel_*");
		for (int i = 0; i < wheelNPs.size(); i++)
		{
			wheelNPs[i].remove_node();
		}
	}

	///Note: wheels will be set to instances of mWheelModel(s) and re-parented
	/// to the chassisNP's parent.
	// add a vehicle
	mVehicle = bt3::addVehicle(mObjectNP, mWheelModel, mVehicleData);
	// check for errors
	RETURN_ON_COND(!mVehicle, RESULT_ERROR)

	// set the flag
	mSetup = true;
	//
	return RESULT_SUCCESS;
}

/**
 * On destruction cleanup.
 * Gives an BT3Vehicle the ability to do any cleaning is necessary when
 * destroyed.
 * \note Internal use only.
 */
void BT3Vehicle::do_finalize()
{
	//cleanup (if needed)
	cleanup();

	//remove this NodePath
	NodePath::any_path(this).remove_node();
	//
#ifdef PYTHON_BUILD
	//Python callback
	PY_SELF_ARG_DECREF(mUpdateCallback, mSelf);
#endif //PYTHON_BUILD
	do_reset();
	//
	return;
}

/**
 * Cleans the BT3Vehicle up, detaching any child objects (if not empty).
 */
int BT3Vehicle::cleanup()
{
	// continue if vehicle has been already setup
	CONTINUE_IF_ELSE_R(mSetup, RESULT_SUCCESS)

	int result = RESULT_ERROR;
	// remove the vehicle
	if (bt3::removeVehicle(mVehicle))
	{
		// reset bullet vehicle reference
		mVehicle = nullptr;
		// set the flag
		mSetup = false;
		result = RESULT_SUCCESS;
	}
	//
	return result;
}

/**
 * Enables/disables vehicle notifications through events.
 */
void BT3Vehicle::enable_throw_event(BT3EventThrown event, bool enable,
		float frequency, const string& eventName)
{
	ThrowEventData eventData(eventName,	abs(frequency));
	eventData.mEnable = enable;
	switch (event)
	{
	case MOVE:
		mMove = eventData;
		break;
	case STEADY:
		mSteady = eventData;
		break;
	default:
		break;
	}
}

/**
 * Throws events.
 * \note Internal use only.
 */
void BT3Vehicle::do_throw_event(ThrowEventData& eventData)
{
	if (eventData.mThrown)
	{
		eventData.mTimeElapsed += ClockObject::get_global_clock()->get_dt();
		if (eventData.mTimeElapsed >= eventData.mPeriod)
		{
			//enough time is passed: throw the event
			throw_event(eventData.mEventName, EventParameter(this));
			//update elapsed time
			eventData.mTimeElapsed -= eventData.mPeriod;
		}
	}
	else
	{
		//throw the event
		throw_event(eventData.mEventName, EventParameter(this));
		eventData.mThrown = true;
	}
}

/**
 * Updates the BT3Vehicle state.
 */
void BT3Vehicle::update(float dt)
{
	RETURN_ON_COND(!mSetup,)

#ifdef TESTING
	dt = 0.016666667; //60 fps
#endif

	//update vehicle
	//process input
	float engineForce = 0.0;
	float brake = 0.0;
	//handle keys:
	if (mForward)
	{
		engineForce = -mMaxEngineForce;
	}
	else if (mBackward)
	{
		engineForce = mMaxEngineForce;
	}
	if (mBrake)
	{
		brake = mMaxBrake;
	}
	if (mHeadLeft)
	{
		mSteering += dt * mSteeringIncrement;
		mSteering = min(mSteering, mSteeringClamp);
	}
	else if (mHeadRight)
	{
		mSteering -= dt * mSteeringIncrement;
		mSteering = max(mSteering, -mSteeringClamp);
	}
	else
	{
		if (mSteering < 0.0)
		{
			mSteering = min(mSteering + mSteeringDecrement * dt, float(0.0));
		}
		else if (mSteering > 0.0)
		{
			mSteering = max(mSteering - mSteeringDecrement * dt, float(0.0));
		}
	}
	//Apply steering, engine and brake forces to wheels
	for (int idx = 0; idx < mVehicle->getNumWheels(); ++idx)
	{
		if (mVehicle->getWheelInfo(idx).m_bIsFrontWheel)
		{
			// front
			if (mWheelApplySteering[0])
			{
				mVehicle->setSteeringValue(mSteering, idx);
			}
			if (mWheelApplyEngineForce[0])
			{
				mVehicle->applyEngineForce(engineForce, idx);
			}
			if (mWheelApplyBrake[0])
			{
				mVehicle->setBrake(brake, idx);
			}
		}
		else
		{
			// rear
			if (mWheelApplySteering[1])
			{
				mVehicle->setSteeringValue(mSteering, idx);
			}
			if (mWheelApplyEngineForce[1])
			{
				mVehicle->applyEngineForce(engineForce, idx);
			}
			if (mWheelApplyBrake[1])
			{
				mVehicle->setBrake(brake, idx);
			}
		}
	}

	//handle events
	float speedKMH = mVehicle->getCurrentSpeedKmHour();
	if (speedKMH * speedKMH > 0.001296)
	{
		//throw Move event (if enabled) (> 1cm/sec)
		if (mMove.mEnable)
		{
			do_throw_event(mMove);
		}
		//reset Steady event (if enabled and if thrown)
		if (mSteady.mEnable and mSteady.mThrown)
		{
			mSteady.mThrown = false;
			mSteady.mTimeElapsed = 0.0;
		}
	}
	else
	{
		//reset Move event (if enabled and if thrown)
		if (mMove.mEnable and mMove.mThrown)
		{
			mMove.mThrown = false;
			mMove.mTimeElapsed = 0.0;
		}
		//throw Steady event (if enabled)(<= 1cm/sec)
		if (mSteady.mEnable)
		{
			do_throw_event(mSteady);
		}
	}
	//
	if (mUpdateCallback)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_CALL(mUpdateCallback, mSelf,
				get_name() + " BT3Vehicle::update()");
#else
		// call c++ callback
		mUpdateCallback(this);
#endif //PYTHON_BUILD
	}
}

/**
 * Writes a sensible description of the BT3Vehicle to the indicated output
 * stream.
 */
void BT3Vehicle::output(ostream &out) const
{
	out << get_type() << " " << get_name();
}

#ifdef PYTHON_BUILD
/**
 * Sets the update callback as a python function taking this BT3Vehicle as
 * an argument, or None. On error raises an python exception.
 * \note Python only.
 */
void BT3Vehicle::set_update_callback(PyObject *clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mUpdateCallback, clbk, Dtool_BT3Vehicle,
			get_name() + " BT3Vehicle.set_update_callback()", mSelf, this);
}
#else
/**
 * Sets the update callback as a c++ function taking this BT3Vehicle as
 * an argument, or nullptr.
 * \note C++ only.
 */
void BT3Vehicle::set_update_callback(UPDATECALLBACKFUNC value)
{
	mUpdateCallback = value;
}
#endif //PYTHON_BUILD

//TypedWritable API
/**
 * Tells the BamReader how to create objects of type BT3Vehicle.
 */
void BT3Vehicle::register_with_read_factory()
{
	BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

/**
 * Writes the contents of this object to the datagram for shipping out to a
 * Bam file.
 */
void BT3Vehicle::write_datagram(BamWriter *manager, Datagram &dg)
{
	PandaNode::write_datagram(manager, dg);

	///The setup flag.
	dg.add_bool(mSetup);

	///@{
	///Physical parameters.
	///mVehicleData
	do_write_vehicle_data(dg);
	dg.add_bool(mWheelApplySteering[0]);
	dg.add_bool(mWheelApplySteering[1]);
	dg.add_bool(mWheelApplyEngineForce[0]);
	dg.add_bool(mWheelApplyEngineForce[1]);
	dg.add_bool(mWheelApplyBrake[0]);
	dg.add_bool(mWheelApplyBrake[1]);
	dg.add_stdfloat(mMaxEngineForce);
	dg.add_stdfloat(mMaxBrake);
	///steering related values are in radians
	dg.add_stdfloat(mSteering);
	dg.add_stdfloat(mSteeringClamp);
	dg.add_stdfloat(mSteeringIncrement);
	dg.add_stdfloat(mSteeringDecrement);
	///Key controls and effective keys.
	dg.add_bool(mForward);
	dg.add_bool(mBackward);
	dg.add_bool(mBrake);
	dg.add_bool(mHeadLeft);
	dg.add_bool(mHeadRight);
	dg.add_bool(mForwardKey);
	dg.add_bool(mBackwardKey);
	dg.add_bool(mBrakeKey);
	dg.add_bool(mHeadLeftKey);
	dg.add_bool(mHeadRightKey);
	///@}

	///@{
	/// Move/steady notification through event.
	mMove.write_datagram(dg);
	mSteady.write_datagram(dg);
	///@}

	/// Dynamic stuff: read only if setup and defer restoring to post_process_from_bam()
	if (mSetup)
	{
		/// HACK: dummy saving to make restoring correct
		dg.add_bool(true);
	}

	///The owner object this BT3Vehicle is attached to.
	manager->write_pointer(dg, mObjectNP.node());
	///The reference node path.
	manager->write_pointer(dg, mReferenceNP.node());
	///mWheelModel
	manager->write_pointer(dg, mWheelModel[0].node());
	manager->write_pointer(dg, mWheelModel[1].node());
}

/**
 * Receives an array of pointers, one for each time manager->read_pointer()
 * was called in fillin(). Returns the number of pointers processed.
 */
int BT3Vehicle::complete_pointers(TypedWritable **p_list, BamReader *manager)
{
	int pi = PandaNode::complete_pointers(p_list, manager);

	/// Pointers
	///The owner object this BT3Vehicle is attached to.
	PT(PandaNode)objectNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mObjectNP = NodePath::any_path(objectNPPandaNode);
	///The reference node path.
	PT(PandaNode)referenceNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mReferenceNP = NodePath::any_path(referenceNPPandaNode);

	///mWheelModel
	PT(PandaNode)wheelModel1NPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mWheelModel[0] = NodePath::any_path(wheelModel1NPPandaNode);
	PT(PandaNode)wheelModel2NPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mWheelModel[1] = NodePath::any_path(wheelModel2NPPandaNode);

	return pi;
}

/**
 * Should be after the complete restoring from the bam file.
 * \note for a complete restoring it should be called twice.
 */
void BT3Vehicle::post_process_from_bam()
{
	/// setup this BT3Vehicle if needed
	if (mSetup)
	{
		// first step
		if (mBuildFromBam)
		{
			mBuildFromBam = false;
		}
		// second step
		else if (mOutDatagram.get_length() != 0)
		{
			// all external referenced objects should be set up
			mSetup = !mSetup;
			mBuildFromBam = true;
			setup();
			mBuildFromBam = false;
			// restore attributes
			DatagramIterator scan(mOutDatagram);
			/// HACK: dummy saving to make restoring correct
			scan.get_bool();
			mOutDatagram.clear();
		}
	}
}

/**
 * This function is called by the BamReader's factory when a new object of
 * type BT3Vehicle is encountered in the Bam file.  It should create the
 * BT3Vehicle and extract its information from the file.
 */
TypedWritable *BT3Vehicle::make_from_bam(const FactoryParams &params)
{
	// continue only if GamePhysicsManager exists
	CONTINUE_IF_ELSE_R(GamePhysicsManager::get_global_ptr(), nullptr)

	// create a BT3Vehicle with default parameters' values: they'll be restored later
	GamePhysicsManager::get_global_ptr()->set_parameters_defaults(
			GamePhysicsManager::VEHICLE);
	BT3Vehicle *node = DCAST(BT3Vehicle,
			GamePhysicsManager::get_global_ptr()->create_vehicle(
					"BT3Vehicle").node());

	DatagramIterator scan;
	BamReader *manager;

	parse_params(params, scan, manager);
	node->fillin(scan, manager);

	return node;
}

/**
 * This internal function is called by make_from_bam to read in all of the
 * relevant data from the BamFile for the new BT3Vehicle.
 */
void BT3Vehicle::fillin(DatagramIterator &scan, BamReader *manager)
{
	PandaNode::fillin(scan, manager);

	///The setup flag.
	mSetup = scan.get_bool();

	///@{
	///Physical parameters.
	///mVehicleData
	do_read_vehicle_data(scan);
	mWheelApplySteering[0] = scan.get_bool();
	mWheelApplySteering[1] = scan.get_bool();
	mWheelApplyEngineForce[0] = scan.get_bool();
	mWheelApplyEngineForce[1] = scan.get_bool();
	mWheelApplyBrake[0] = scan.get_bool();
	mWheelApplyBrake[1] = scan.get_bool();
	mMaxEngineForce = scan.get_stdfloat();
	mMaxBrake = scan.get_stdfloat();
	///steering related values are in radians
	mSteering = scan.get_stdfloat();
	mSteeringClamp = scan.get_stdfloat();
	mSteeringIncrement = scan.get_stdfloat();
	mSteeringDecrement = scan.get_stdfloat();
	///Key controls and effective keys.
	mForward = scan.get_bool();
	mBackward = scan.get_bool();
	mBrake = scan.get_bool();
	mHeadLeft = scan.get_bool();
	mHeadRight = scan.get_bool();
	mForwardKey = scan.get_bool();
	mBackwardKey = scan.get_bool();
	mBrakeKey = scan.get_bool();
	mHeadLeftKey = scan.get_bool();
	mHeadRightKey = scan.get_bool();
	///@}

	///@{
	/// Move/steady notification through event.
	mMove.read_datagram(scan);
	mSteady.read_datagram(scan);
	// reset counters
	enable_throw_event(MOVE, mMove.mEnable, mMove.mFrequency,
			mMove.mEventName);
	enable_throw_event(STEADY, mSteady.mEnable, mSteady.mFrequency,
			mSteady.mEventName);
	///@}

	/// Dynamic stuff: read only if setup and defer restoring to post_process_from_bam()
	if (mSetup)
	{
		/// HACK: dummy saving to make restoring correct
		mOutDatagram.add_bool(scan.get_bool());
	}

	///The owner object this BT3Vehicle is attached to.
	manager->read_pointer(scan);
	///The reference node path.
	manager->read_pointer(scan);
	///mWheelModel
	manager->read_pointer(scan);
	manager->read_pointer(scan);
	// for correct restoring
	mBuildFromBam = true;
}

/**
 * Writes mVehicleData into a Datagram.
 * \note Internal use only.
 */
void BT3Vehicle::do_write_vehicle_data(Datagram &dg)
{
	dg.add_uint8((uint8_t)mVehicleData.cs);
	dg.add_stdfloat(mVehicleData.tuning.m_suspensionStiffness);
	dg.add_stdfloat(mVehicleData.tuning.m_suspensionCompression);
	dg.add_stdfloat(mVehicleData.tuning.m_suspensionDamping);
	dg.add_stdfloat(mVehicleData.tuning.m_maxSuspensionTravelCm);
	dg.add_stdfloat(mVehicleData.tuning.m_frictionSlip);
	dg.add_stdfloat(mVehicleData.tuning.m_maxSuspensionForce);
	dg.add_int32(mVehicleData.numWheels);
	dg.add_stdfloat(mVehicleData.connHeightF[0]);
	dg.add_stdfloat(mVehicleData.connHeightF[1]);
	dg.add_stdfloat(mVehicleData.connWidthF[0]);
	dg.add_stdfloat(mVehicleData.connWidthF[1]);
	dg.add_stdfloat(mVehicleData.connDepthF[0]);
	dg.add_stdfloat(mVehicleData.connDepthF[1]);
	dg.add_stdfloat(mVehicleData.suspRestLengthF[0]);
	dg.add_stdfloat(mVehicleData.suspRestLengthF[1]);
}

/**
 * Reads mVehicleData from a DatagramIterator.
 * \note Internal use only.
 */
void BT3Vehicle::do_read_vehicle_data(DatagramIterator &scan)
{
	mVehicleData.cs = (bt3::Panda3dUpAxis)scan.get_uint8();
	mVehicleData.tuning.m_suspensionStiffness = scan.get_stdfloat();
	mVehicleData.tuning.m_suspensionCompression = scan.get_stdfloat();
	mVehicleData.tuning.m_suspensionDamping = scan.get_stdfloat();
	mVehicleData.tuning.m_maxSuspensionTravelCm = scan.get_stdfloat();
	mVehicleData.tuning.m_frictionSlip = scan.get_stdfloat();
	mVehicleData.tuning.m_maxSuspensionForce = scan.get_stdfloat();
	mVehicleData.numWheels = scan.get_int32();
	mVehicleData.connHeightF[0] = scan.get_stdfloat();
	mVehicleData.connHeightF[1] = scan.get_stdfloat();
	mVehicleData.connWidthF[0] = scan.get_stdfloat();
	mVehicleData.connWidthF[1] = scan.get_stdfloat();
	mVehicleData.connDepthF[0] = scan.get_stdfloat();
	mVehicleData.connDepthF[1] = scan.get_stdfloat();
	mVehicleData.suspRestLengthF[0] = scan.get_stdfloat();
	mVehicleData.suspRestLengthF[1] = scan.get_stdfloat();
}

TYPED_OBJECT_API_DEF(BT3Vehicle)
