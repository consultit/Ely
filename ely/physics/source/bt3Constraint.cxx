/**
 * \file bt3Constraint.cxx
 *
 * \date 2017-03-18
 * \author consultit
 */

#if defined(_WIN32)
#include "support_os/pstdint.h"
#endif

#include "bt3Constraint.h"
#include "bt3ConstraintAttrs.h"
#include "transformState.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
extern struct Dtool_PyTypedObject Dtool_BT3Constraint;
extern struct Dtool_PyTypedObject Dtool_PointToPointAttrs;
extern struct Dtool_PyTypedObject Dtool_HingeAttrs;
extern struct Dtool_PyTypedObject Dtool_HingeAccumulatedAttrs;
extern struct Dtool_PyTypedObject Dtool_Hinge2Attrs;
extern struct Dtool_PyTypedObject Dtool_ConeTwistAttrs;
extern struct Dtool_PyTypedObject Dtool_Dof6Attrs;
extern struct Dtool_PyTypedObject Dtool_Dof6SpringAttrs;
extern struct Dtool_PyTypedObject Dtool_Dof6Spring2Attrs;
extern struct Dtool_PyTypedObject Dtool_FixedAttrs;
extern struct Dtool_PyTypedObject Dtool_UniversalAttrs;
extern struct Dtool_PyTypedObject Dtool_SliderAttrs;
extern struct Dtool_PyTypedObject Dtool_GearAttrs;
#endif //PYTHON_BUILD

///BT3Constraint definitions
/**
 *
 */
BT3Constraint::BT3Constraint(const string& name) :
		PandaNode(name)
{
	do_reset();
}

/**
 *
 */
BT3Constraint::~BT3Constraint()
{
}

/**
 * Initializes the BT3Constraint with starting settings.
 * \note Internal use only.
 */
void BT3Constraint::do_initialize()
{
	GamePhysicsManager* mTmpl = GamePhysicsManager::get_global_ptr();
	// contraint type
	string contraintType = mTmpl->get_parameter_value(
			GamePhysicsManager::CONSTRAINT, string("contraint_type"));
	if (!contraintType.empty())
	{
		mConstraintData.type =
				static_cast<bt3::ConstraintType>(do_get_constraint_type(contraintType));
	}
	// use world reference
	mConstraintData.local = (mTmpl->get_parameter_value(
					GamePhysicsManager::CONSTRAINT,
					string("use_world_reference")) == string("true") ? false : true);
	// pivot references
	string pivotReferences = mTmpl->get_parameter_value(
			GamePhysicsManager::CONSTRAINT, string("pivot_references"));
	do_get_vec_base_values(pivotReferences, mConstraintData.pivots.AorGlobal,
			mConstraintData.pivots.B);
	// axis references
	string axisReferences = mTmpl->get_parameter_value(
			GamePhysicsManager::CONSTRAINT, string("axis_references"));
	do_get_vec_base_values(axisReferences, mConstraintData.axes.AorGlobal,
			mConstraintData.axes.B);
	// ratio (>=0.0)
	mConstraintData.ratio = abs(STRTOF(mTmpl->get_parameter_value(
							GamePhysicsManager::CONSTRAINT, string("ratio")).c_str(), nullptr));
	// rotation references
	string rotationReferences = mTmpl->get_parameter_value(
			GamePhysicsManager::CONSTRAINT, string("rotation_references"));
	do_get_vec_base_values(rotationReferences, mConstraintData.rotations.AorGlobal,
			mConstraintData.rotations.B);
	// rotation order
	string rotationOrder = mTmpl->get_parameter_value(
					GamePhysicsManager::CONSTRAINT, string("rotation_order"));
	if (!rotationOrder.empty())
	{
		mConstraintData.rotOrder =
				static_cast<RotateOrder>(do_get_rotation_order(rotationOrder));
	}
	// world anchor
	string worldAnchor = mTmpl->get_parameter_value(
			GamePhysicsManager::CONSTRAINT, string("world_anchor"));
	pvector<string> valuesStr = parseCompoundString(worldAnchor, ',');
	for (unsigned int i = 0; (i < valuesStr.size()) && (i < 3); i++)
	{
		mConstraintData.anchor[i] = STRTOF(valuesStr[i].c_str(), nullptr);
	}
	// world axes
	string worldAxes = mTmpl->get_parameter_value(
			GamePhysicsManager::CONSTRAINT, string("world_axes"));
	do_get_vec_base_values(worldAxes, mConstraintData.axis1,
			mConstraintData.axis2);
	// use reference frame a
	mConstraintData.useReferenceFrameA = (mTmpl->get_parameter_value(
					GamePhysicsManager::CONSTRAINT,
					string("use_reference_frame_a")) == string("true") ? true : false);
	// disable objects collisions
	mConstraintData.disableCollisionsBetweenLinkedBodies = (mTmpl->get_parameter_value(
					GamePhysicsManager::CONSTRAINT,
					string("disable_objects_collisions")) == string("true") ? true : false);
	// objectA
	string objectA = mTmpl->get_parameter_value(
			GamePhysicsManager::CONSTRAINT, string("objectA"));
	if(!objectA.empty())
	{
		// search object under reference node
		NodePath objectANP = mReferenceNP.find(string("**/") + objectA);
		if (!objectANP.is_empty())
		{
			mObjectANP = objectANP;
		}
	}
	// objectB
	string objectB = mTmpl->get_parameter_value(
			GamePhysicsManager::CONSTRAINT, string("objectB"));
	if(!objectB.empty())
	{
		// search object under reference node
		NodePath objectBNP = mReferenceNP.find(string("**/") + objectB);
		if (!objectBNP.is_empty())
		{
			mObjectBNP = objectBNP;
		}
	}
	// setup
	setup();
}

/**
 * Sets the BT3Constraint up for the given object (if not empty), given the
 * shape's type and size (among others).
 */
int BT3Constraint::setup()
{
	// continue if constraint has not been setup yet
	CONTINUE_IF_ELSE_R(!mSetup, RESULT_SUCCESS)

	// continue if at least mObjectANP not empty
	CONTINUE_IF_ELSE_R(!mObjectANP.is_empty(), RESULT_ERROR)

	// convert all global references to local to ease de-serialization
	if (!mConstraintData.local)
	{
		do_convert_references_from_global_to_local();
		// set use of local references on
		mConstraintData.local = true;
	}

	// add a constraint
	bt3::Physics& physics =
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics();
	mConstraint = bt3::addConstraintToNodes(mObjectANP, mObjectBNP, physics,
			mConstraintData);
	// check for errors
	RETURN_ON_COND(!mConstraint, RESULT_ERROR)

	// create constraint's attrs
	mConstraintAttrs = do_create_constraint_attrs();
	mConstraintAttrs->set_constraint(mConstraint);
#ifdef PYTHON_BUILD
	mConstraintAttrsPy = do_create_constraint_attrs_py();
#endif //PYTHON_BUILD
	// set the flag
	mSetup = true;
	//
	return RESULT_SUCCESS;
}

/**
 * On destruction cleanup.
 * Gives an BT3Constraint the ability to do any cleaning is necessary when
 * destroyed.
 * \note Internal use only.
 */
void BT3Constraint::do_finalize()
{
	//cleanup (if needed)
	cleanup();

	//remove this NodePath
	NodePath::any_path(this).remove_node();
	//
#ifdef PYTHON_BUILD
	//Python callback
	PY_SELF_ARG_DECREF(mUpdateCallback, mSelf);
#endif //PYTHON_BUILD
	do_reset();
	//
	return;
}

/**
 * Cleans the BT3Constraint up.
 */
int BT3Constraint::cleanup()
{
	// continue if constraint has been already setup
	CONTINUE_IF_ELSE_R(mSetup, RESULT_SUCCESS)

	int result = RESULT_ERROR;
	// remove the constraint
	if (bt3::removeConstraintFromNodes(mConstraint,
			*GamePhysicsManager::GetSingletonPtr()->get_bullet_physics()))
	{
#ifdef PYTHON_BUILD
		Py_XDECREF(mConstraintAttrsPy);
		mConstraintAttrsPy = nullptr;
#endif //PYTHON_BUILD
		// destroy constraint's attrs
		delete mConstraintAttrs;
		mConstraintAttrs = nullptr;
		// reset bullet constraint reference
		mConstraint = nullptr;
		// set the flag
		mSetup = false;
		result = RESULT_SUCCESS;
	}
	//
	return result;
}

/**
 * Returns the constraint type given the name.
 * \note Internal use only.
 */
BT3Constraint::BT3ConstraintType BT3Constraint::do_get_constraint_type(
		const string& name) const
{
	BT3ConstraintType constraintType;
	if (name == string("point2point"))
	{
		constraintType = POINT2POINT;
	}
	else if (name == string("hinge"))
	{
		constraintType = HINGE;
	}
	else if (name == string("hinge_accumulated"))
	{
		constraintType = HINGE_ACCUMULATED;
	}
	else if (name == string("hinge2"))
	{
		constraintType = HINGE_2;
	}
	else if (name == string("cone_twist"))
	{
		constraintType = CONETWIST;
	}
	else if (name == string("dof6"))
	{
		constraintType = DOF6;
	}
	else if (name == string("dof6_spring"))
	{
		constraintType = DOF6_SPRING;
	}
	else if (name == string("dof6_spring2"))
	{
		constraintType = DOF6_SPRING_2;
	}
	else if (name == string("fixed"))
	{
		constraintType = FIXED;
	}
	else if (name == string("universal"))
	{
		constraintType = UNIVERSAL;
	}
	else if (name == string("slider"))
	{
		constraintType = SLIDER;
	}
	else if (name == string("contact"))
	{
		constraintType = CONTACT;
	}
	else if (name == string("gear"))
	{
		constraintType = GEAR;
	}
	return constraintType;
}

/**
 * Returns the rotation order given the name.
 * \note Internal use only.
 */
BT3Constraint::BT3RotateOrder BT3Constraint::do_get_rotation_order(
		const string& name) const
{
	BT3RotateOrder rotationOrder;
	if (name == string("xyz"))
	{
		rotationOrder = RO_XYZ;
	}
	else if (name == string("xzy"))
	{
		rotationOrder = RO_XZY;
	}
	else if (name == string("yxz"))
	{
		rotationOrder = RO_YXZ;
	}
	else if (name == string("yzx"))
	{
		rotationOrder = RO_YZX;
	}
	else if (name == string("zxy"))
	{
		rotationOrder = RO_ZXY;
	}
	else if (name == string("zyx"))
	{
		rotationOrder = RO_ZYX;
	}
	return rotationOrder;
}

/**
 * Compute the given references.
 * \note Internal use only.
 */
void BT3Constraint::do_get_vec_base_values(const string& references, LVecBase3f& ref1,
		LVecBase3f& ref2)
{
	pvector<string> referencesStr = parseCompoundString(references, ':');
	for (unsigned int i = 0; (i < referencesStr.size()) && (i < 2); i++)
	{
		pvector<string> valuesStr = parseCompoundString(referencesStr[i], ',');
		for (unsigned int v = 0; (v < valuesStr.size()) && (v < 3); v++)
		{
			// i has value 0 or 1
			i == 0 ?
				ref1[v] = STRTOF(valuesStr[v].c_str(), nullptr) :
				ref2[v] = STRTOF(valuesStr[v].c_str(), nullptr);
		}
	}
}

/**
 * Convert all global references to local to ease de-serialization.
 * \note Internal use only.
 */
void BT3Constraint::do_convert_references_from_global_to_local()
{
	// store global reference values
	LPoint3f globalPivot = mConstraintData.pivots.AorGlobal;
	LVector3f globalAxis = mConstraintData.axes.AorGlobal;
	LVecBase3f globalHpr = mConstraintData.rotations.AorGlobal;
	// compute the global constraint's transform
	CPT(TransformState) Mc = TransformState::make_pos_hpr(globalPivot, globalHpr);
	//
	// convert references wrt object A (parented to mReferenceNP)
	{
		mConstraintData.pivots.AorGlobal = mObjectANP.get_relative_point(
				mReferenceNP, globalPivot);
		mConstraintData.axes.AorGlobal = mObjectANP.get_relative_vector(
				mReferenceNP, globalAxis);
		// rotations are more complicated
		{
			// see support/constraint.cpp
			CPT(TransformState)MoINV = mObjectANP.get_transform()->get_inverse();
			CPT(TransformState) McMoINV = MoINV->compose(Mc);
			mConstraintData.rotations.AorGlobal = McMoINV->get_hpr();
		}
	}
	// convert references wrt object B if any (parented to mReferenceNP)
	if (!mObjectBNP.is_empty())
	{
		mConstraintData.pivots.B = mObjectBNP.get_relative_point(mReferenceNP,
				globalPivot);
		mConstraintData.axes.B = mObjectBNP.get_relative_vector(mReferenceNP,
				globalAxis);
		// rotations are more complicated
		{
			// see support/constraint.cpp
			CPT(TransformState)MoINV = mObjectBNP.get_transform()->get_inverse();
			CPT(TransformState) McMoINV = MoINV->compose(Mc);
			mConstraintData.rotations.B = McMoINV->get_hpr();
		}
	}
}

/**
 * Creates constraint attrs class.
 * \note Internal use only.
 */
TypedConstraintAttrs* BT3Constraint::do_create_constraint_attrs()
{
	TypedConstraintAttrs* attrs = nullptr;
	BT3ConstraintType constraintType = get_constraint_type();
	switch (constraintType)
	{
		case POINT2POINT:
			attrs = new PointToPointAttrs();
			break;
		case HINGE:
			attrs = new HingeAttrs();
			break;
		case HINGE_ACCUMULATED:
			attrs = new HingeAccumulatedAttrs();
		break;
		case HINGE_2:
			attrs = new Hinge2Attrs();
			break;
		case CONETWIST:
			attrs = new ConeTwistAttrs();
			break;
		case DOF6:
			attrs = new Dof6Attrs();
		break;
		case DOF6_SPRING:
			attrs = new Dof6SpringAttrs();
			break;
		case DOF6_SPRING_2:
			attrs = new Dof6Spring2Attrs();
			break;
		case FIXED:
			attrs = new FixedAttrs();
			break;
		case UNIVERSAL:
			attrs = new UniversalAttrs();
			break;
		case SLIDER:
			attrs = new SliderAttrs();
			break;
		case CONTACT: //todo (?)
			break;
		case GEAR:
			attrs = new GearAttrs();
			break;
		default:
			break;
	}
	return attrs;
}
#if defined(PYTHON_BUILD)
PyObject *BT3Constraint::do_create_constraint_attrs_py()
{
	PyObject *attrs = nullptr;
	BT3ConstraintType constraintType = get_constraint_type();
	switch (constraintType)
	{
		case POINT2POINT:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_PointToPointAttrs, false, false);
			break;
		case HINGE:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_HingeAttrs, false, false);
			break;
		case HINGE_ACCUMULATED:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_HingeAccumulatedAttrs, false, false);
		break;
		case HINGE_2:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_Hinge2Attrs, false, false);
			break;
		case CONETWIST:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_ConeTwistAttrs, false, false);
			break;
		case DOF6:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_Dof6Attrs, false, false);
		break;
		case DOF6_SPRING:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_Dof6SpringAttrs, false, false);
			break;
		case DOF6_SPRING_2:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_Dof6Spring2Attrs, false, false);
			break;
		case FIXED:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_FixedAttrs, false, false);
			break;
		case UNIVERSAL:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_UniversalAttrs, false, false);
			break;
		case SLIDER:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_SliderAttrs, false, false);
			break;
		case CONTACT: //todo (?)
			break;
		case GEAR:
			attrs = DTool_CreatePyInstance((void *)mConstraintAttrs,
					Dtool_GearAttrs, false, false);
			break;
		default:
			break;
	}
	return attrs;
}
#endif //PYTHON_BUILD

/**
 * Updates the BT3Constraint state.
 */
void BT3Constraint::update(float dt)
{
	RETURN_ON_COND(!mSetup,)

#ifdef TESTING
	dt = 0.016666667; //60 fps
#endif
	//
	if (mUpdateCallback)
	{
#ifdef PYTHON_BUILD
		// call python callback
		PY_CALLBACK_SELF_ARG_CALL(mUpdateCallback, mSelf,
				get_name() + " BT3Constraint::update()");
#else
		// call c++ callback
		mUpdateCallback(this);
#endif //PYTHON_BUILD
	}
}

/**
 * Writes a sensible description of the BT3Constraint to the indicated output
 * stream.
 */
void BT3Constraint::output(ostream &out) const
{
	out << get_type() << " " << get_name();
}

#ifdef PYTHON_BUILD
/**
 * Sets the update callback as a python function taking this BT3Constraint as
 * an argument, or None. On error raises an python exception.
 * \note Python only.
 */
void BT3Constraint::set_update_callback(PyObject *clbk)
{
	PY_SET_CALLBACK_SELF_ARG_BODY(mUpdateCallback, clbk, Dtool_BT3Constraint,
			get_name() + " BT3Constraint.set_update_callback()", mSelf, this);
}
#else
/**
 * Sets the update callback as a c++ function taking this BT3Constraint as
 * an argument, or nullptr.
 * \note C++ only.
 */
void BT3Constraint::set_update_callback(UPDATECALLBACKFUNC value)
{
	mUpdateCallback = value;
}
#endif //PYTHON_BUILD

//TypedWritable API
/**
 * Tells the BamReader how to create objects of type BT3Constraint.
 */
void BT3Constraint::register_with_read_factory()
{
	BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

/**
 * Writes the contents of this object to the datagram for shipping out to a
 * Bam file.
 */
void BT3Constraint::write_datagram(BamWriter *manager, Datagram &dg)
{
	PandaNode::write_datagram(manager, dg);

	///The setup flag.
	dg.add_bool(mSetup);

	///@{
	///Physical parameters.
	dg.add_uint8((uint8_t) mConstraintData.type);
	dg.add_bool(mConstraintData.disableCollisionsBetweenLinkedBodies);
	dg.add_bool(mConstraintData.local);
	mConstraintData.pivots.AorGlobal.write_datagram(dg);
	mConstraintData.pivots.B.write_datagram(dg);
	mConstraintData.axes.AorGlobal.write_datagram(dg);
	mConstraintData.axes.B.write_datagram(dg);
	mConstraintData.rotations.AorGlobal.write_datagram(dg);
	mConstraintData.rotations.B.write_datagram(dg);
	dg.add_bool(mConstraintData.useReferenceFrameA);
	dg.add_uint8((uint8_t) mConstraintData.rotOrder);
	mConstraintData.anchor.write_datagram(dg);
	mConstraintData.axis1.write_datagram(dg);
	mConstraintData.axis2.write_datagram(dg);
	dg.add_stdfloat(mConstraintData.ratio);
	///@}

	/// Dynamic stuff (parameters)  and mConstraintAttrs: save only if setup
	if (mSetup)
	{
		dg.add_bool(get_disable_objects_collisions());
		mConstraintAttrs->write_datagram(dg);
	}

	///The objects this BT3Constraint is attached to.
	manager->write_pointer(dg, mObjectANP.node());
	manager->write_pointer(dg, mObjectBNP.node());
	///The reference node path.
	manager->write_pointer(dg, mReferenceNP.node());
}

/**
 * Receives an array of pointers, one for each time manager->read_pointer()
 * was called in fillin(). Returns the number of pointers processed.
 */
int BT3Constraint::complete_pointers(TypedWritable **p_list, BamReader *manager)
{
	int pi = PandaNode::complete_pointers(p_list, manager);

	/// Pointers
	///The objects this BT3Constraint is attached to.
	PT(PandaNode)objectANPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mObjectANP = NodePath::any_path(objectANPPandaNode);
	PT(PandaNode)objectBNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mObjectBNP = NodePath::any_path(objectBNPPandaNode);
	///The reference node path.
	PT(PandaNode)referenceNPPandaNode = DCAST(PandaNode, p_list[pi++]);
	mReferenceNP = NodePath::any_path(referenceNPPandaNode);

	return pi;
}

/**
 * Should be after the complete restoring from the bam file.
 * \note for a complete restoring it should be called twice.
 */
void BT3Constraint::post_process_from_bam()
{
	/// setup this BT3Constraint if needed
	if (mSetup)
	{
		// first step
		if (mBuildFromBam)
		{
			mBuildFromBam = false;
		}
		// second step
		else if (mOutDatagram.get_length() != 0)
		{
			// all external referenced objects should be set up
			mSetup = !mSetup;
			setup();
			// restore attributes
			DatagramIterator scan(mOutDatagram);
			set_disable_objects_collisions(scan.get_bool());
			mConstraintAttrs->read_datagram(scan, nullptr);
			mOutDatagram.clear();
		}
	}
}

/**
 * This function is called by the BamReader's factory when a new object of
 * type BT3Constraint is encountered in the Bam file.  It should create the
 * BT3Constraint and extract its information from the file.
 */
TypedWritable *BT3Constraint::make_from_bam(const FactoryParams &params)
{
	// continue only if GamePhysicsManager exists
	CONTINUE_IF_ELSE_R(GamePhysicsManager::get_global_ptr(), nullptr)

	// create a BT3Constraint with default parameters' values: they'll be restored later
	GamePhysicsManager::get_global_ptr()->set_parameters_defaults(
			GamePhysicsManager::CONSTRAINT);
	BT3Constraint *node = DCAST(BT3Constraint,
			GamePhysicsManager::get_global_ptr()->create_constraint(
					"BT3Constraint").node());

	DatagramIterator scan;
	BamReader *manager;

	parse_params(params, scan, manager);
	node->fillin(scan, manager);

	return node;
}

/**
 * This internal function is called by make_from_bam to read in all of the
 * relevant data from the BamFile for the new BT3Constraint.
 */
void BT3Constraint::fillin(DatagramIterator &scan, BamReader *manager)
{
	PandaNode::fillin(scan, manager);

	///The setup flag.
	mSetup = scan.get_bool();

	///@{
	///Physical parameters.
	mConstraintData.type = (bt3::ConstraintType) scan.get_uint8();
	mConstraintData.disableCollisionsBetweenLinkedBodies = scan.get_bool();
	mConstraintData.local = scan.get_bool();
	mConstraintData.pivots.AorGlobal.read_datagram(scan);
	mConstraintData.pivots.B.read_datagram(scan);
	mConstraintData.axes.AorGlobal.read_datagram(scan);
	mConstraintData.axes.B.read_datagram(scan);
	mConstraintData.rotations.AorGlobal.read_datagram(scan);
	mConstraintData.rotations.B.read_datagram(scan);
	mConstraintData.useReferenceFrameA = scan.get_bool();
	mConstraintData.rotOrder = (RotateOrder) scan.get_uint8();
	mConstraintData.anchor.read_datagram(scan);
	mConstraintData.axis1.read_datagram(scan);
	mConstraintData.axis2.read_datagram(scan);
	mConstraintData.ratio = scan.get_stdfloat();
	///@}

	/// Dynamic stuff: read only if setup and defer restoring to post_process_from_bam()
	if (mSetup)
	{
		mOutDatagram.add_bool(scan.get_bool());
		// use a temporary *Attrs of the right type
		TypedConstraintAttrs* attrsTmp = do_create_constraint_attrs();
		attrsTmp->read_datagram(scan, &mOutDatagram);
		delete attrsTmp;
	}

	///The objects this BT3Constraint is attached to.
	manager->read_pointer(scan);
	manager->read_pointer(scan);
	///The reference node path.
	manager->read_pointer(scan);
	// for correct restoring
	mBuildFromBam = true;
}

TYPED_OBJECT_API_DEF(BT3Constraint)
