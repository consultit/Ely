/**
 * \file bullet3_composite3.cxx
 *
 * \date 2017-03-06
 * \author consultit
 */

///library
// Bullet3Geometry
#include "bullet3/src/Bullet3Geometry/b3GeometryUtil.cpp"
#include "bullet3/src/Bullet3Geometry/b3ConvexHullComputer.cpp"

// Bullet3Common
#include "bullet3/src/Bullet3Common/b3Logging.cpp"
#include "bullet3/src/Bullet3Common/b3Vector3.cpp"
#include "bullet3/src/Bullet3Common/b3AlignedAllocator.cpp"

// Bullet3Collision
#include "bullet3/src/Bullet3Collision/NarrowPhaseCollision/b3ConvexUtility.cpp"
#include "bullet3/src/Bullet3Collision/NarrowPhaseCollision/b3CpuNarrowPhase.cpp"
#include "bullet3/src/Bullet3Collision/BroadPhaseCollision/b3DynamicBvhBroadphase.cpp"
#include "bullet3/src/Bullet3Collision/BroadPhaseCollision/b3DynamicBvh.cpp"
#include "bullet3/src/Bullet3Collision/BroadPhaseCollision/b3OverlappingPairCache.cpp"

// Bullet3Serialize
#include "bullet3/src/Bullet3Serialize/Bullet2FileLoader/b3DNA.cpp"
#include "bullet3/src/Bullet3Serialize/Bullet2FileLoader/b3Chunk.cpp"
#include "bullet3/src/Bullet3Serialize/Bullet2FileLoader/b3Serializer.cpp"
#include "bullet3/src/Bullet3Serialize/Bullet2FileLoader/b3File.cpp"
#include "bullet3/src/Bullet3Serialize/Bullet2FileLoader/b3BulletFile.cpp"

// Bullet3Dynamics
#include "bullet3/src/Bullet3Dynamics/b3CpuRigidBodyPipeline.cpp"
#include "bullet3/src/Bullet3Dynamics/ConstraintSolver/b3FixedConstraint.cpp"
#include "bullet3/src/Bullet3Dynamics/ConstraintSolver/b3TypedConstraint.cpp"
#include "bullet3/src/Bullet3Dynamics/ConstraintSolver/b3Point2PointConstraint.cpp"
#include "bullet3/src/Bullet3Dynamics/ConstraintSolver/b3Generic6DofConstraint.cpp"
#include "bullet3/src/Bullet3Dynamics/ConstraintSolver/b3PgsJacobiSolver.cpp"

// BulletDynamics
#include "bullet3/src/BulletDynamics/Featherstone/btMultiBodyMLCPConstraintSolver.cpp"

#ifdef OPENCL_FOUND
#include "bullet3/src/Bullet3OpenCL/Raycast/b3GpuRaycast.cpp"
#include "bullet3/src/Bullet3OpenCL/ParallelPrimitives/b3RadixSort32CL.cpp"
#include "bullet3/src/Bullet3OpenCL/ParallelPrimitives/b3PrefixScanCL.cpp"
#include "bullet3/src/Bullet3OpenCL/ParallelPrimitives/b3LauncherCL.cpp"
#include "bullet3/src/Bullet3OpenCL/ParallelPrimitives/b3BoundSearchCL.cpp"
#include "bullet3/src/Bullet3OpenCL/ParallelPrimitives/b3FillCL.cpp"
#include "bullet3/src/Bullet3OpenCL/BroadphaseCollision/b3GpuGridBroadphase.cpp"
#include "bullet3/src/Bullet3OpenCL/BroadphaseCollision/b3GpuParallelLinearBvhBroadphase.cpp"
#include "bullet3/src/Bullet3OpenCL/BroadphaseCollision/b3GpuParallelLinearBvh.cpp"
#include "bullet3/src/Bullet3OpenCL/RigidBody/b3GpuGenericConstraint.cpp"
#include "bullet3/src/Bullet3OpenCL/RigidBody/b3GpuNarrowPhase.cpp"
#include "bullet3/src/Bullet3OpenCL/RigidBody/b3GpuJacobiContactSolver.cpp"
#include "bullet3/src/Bullet3OpenCL/RigidBody/b3GpuPgsContactSolver.cpp"
#include "bullet3/src/Bullet3OpenCL/NarrowphaseCollision/b3QuantizedBvh.cpp"
#include "bullet3/src/Bullet3OpenCL/NarrowphaseCollision/b3VoronoiSimplexSolver.cpp"
#include "bullet3/src/Bullet3OpenCL/NarrowphaseCollision/b3ContactCache.cpp"
#include "bullet3/src/Bullet3OpenCL/NarrowphaseCollision/b3TriangleIndexVertexArray.cpp"
#include "bullet3/src/Bullet3OpenCL/NarrowphaseCollision/b3OptimizedBvh.cpp"
#include "bullet3/src/Bullet3OpenCL/NarrowphaseCollision/b3StridingMeshInterface.cpp"
#include "bullet3/src/Bullet3OpenCL/NarrowphaseCollision/b3TriangleCallback.cpp"
#include "bullet3/src/Bullet3OpenCL/NarrowphaseCollision/b3GjkEpa.cpp"
#endif //OPENCL_FOUND
