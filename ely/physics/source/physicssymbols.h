/**
 * \file physicssymbols.h
 *
 * \date 2018-10-03
 * \author consultit
 */
#ifndef PHYSICS_SOURCE_PHYSICSSYMBOLS_H_
#define PHYSICS_SOURCE_PHYSICSSYMBOLS_H_

#ifdef PYTHON_BUILD_physics
#	define EXPCL_PHYSICS EXPORT_CLASS
#	define EXPTP_PHYSICS EXPORT_TEMPL
#else
#	define EXPCL_PHYSICS IMPORT_CLASS
#	define EXPTP_PHYSICS IMPORT_TEMPL
#endif

#endif /* PHYSICS_SOURCE_PHYSICSSYMBOLS_H_ */
