/**
 * \file bullet3_composite2.cxx
 *
 * \date 2017-03-06
 * \author consultit
 */

///library
// BulletCollision
#include "bullet3/src/BulletCollision/Gimpact/gim_tri_collision.cpp"
#include "bullet3/src/BulletCollision/Gimpact/gim_box_set.cpp"
#include "bullet3/src/BulletCollision/Gimpact/gim_contact.cpp"

#ifdef OPENCL_FOUND
#include "bullet3/src/Bullet3OpenCL/ParallelPrimitives/b3PrefixScanFloat4CL.cpp"
#include "bullet3/src/Bullet3OpenCL/BroadphaseCollision/b3GpuSapBroadphase.cpp"
#include "bullet3/src/Bullet3OpenCL/NarrowphaseCollision/b3ConvexHullContact.cpp"
#include "clew/src/clew.c"
#endif //OPENCL_FOUND

// v-hacd
#include "v-hacd/src/VHACD_Lib/src/vhacdManifoldMesh.cpp"
#include "v-hacd/src/VHACD_Lib/src/vhacdMesh.cpp"
#include "v-hacd/src/test/src/oclHelper.cpp"
#include "v-hacd/src/VHACD_Lib/src/VHACD.cpp"
#include "v-hacd/src/VHACD_Lib/src/vhacdICHull.cpp"
#include "v-hacd/src/VHACD_Lib/src/vhacdRaycastMesh.cpp"
#include "v-hacd/src/VHACD_Lib/src/FloatMath.cpp"
