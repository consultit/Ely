/**
 * \file constraint.h
 *
 * \date 2017-03-08
 * \author consultit
 */
#ifndef CONSTRAINT_H_
#define CONSTRAINT_H_

#include "common.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btGeneric6DofSpring2Constraint.h"

namespace bt3
{

//constraints
enum class ConstraintType: unsigned char
{
	POINT2POINT,//0 - POINT2POINT_CONSTRAINT_TYPE
	HINGE,//1 - HINGE_CONSTRAINT_TYPE
	HINGE_ACCUMULATED,//2
	HINGE_2,//3
	CONETWIST,//4 - CONETWIST_CONSTRAINT_TYPE
	DOF6,//5 - D6_CONSTRAINT_TYPE
	DOF6_SPRING,//6 - D6_SPRING_CONSTRAINT_TYPE
	DOF6_SPRING_2,//7 - D6_SPRING_2_CONSTRAINT_TYPE
	FIXED,//8 - FIXED_CONSTRAINT_TYPE
	UNIVERSAL,//9
	SLIDER,//10 - SLIDER_CONSTRAINT_TYPE
	CONTACT,//TODO (?) - CONTACT_CONSTRAINT_TYPE
	GEAR,//12 - GEAR_CONSTRAINT_TYPE
	INVALID_TYPE
};

struct ConstraintData
{
	struct Pivots
	{
		LPoint3f AorGlobal, B;
	};
	struct Axes
	{
		LVector3f AorGlobal, B;
	};
	struct Rotations
	{
		LVecBase3f AorGlobal, B;
	};

	//constraints
	ConstraintData() :
			type(ConstraintType::POINT2POINT),
			local(true),
			pivots({ LPoint3f::zero(), LPoint3f::zero() }),
			axes({ LVector3f::zero(), LVector3f::zero() }),
			rotations({ LVecBase3f::zero(), LVecBase3f::zero() }),
			useReferenceFrameA(false),
			rotOrder(RO_XYZ),
			anchor(LVector3f::zero()),
			axis1(LVector3f::zero()),
			axis2(LVector3f::zero()),
			ratio(0.0),
			disableCollisionsBetweenLinkedBodies(false)
	{
	}
	//data
	ConstraintType type;
	bool local;
	Pivots pivots;
	Axes axes;
	Rotations rotations;
	bool useReferenceFrameA;
	RotateOrder rotOrder;
	LPoint3f anchor;
	LVector3f axis1, axis2;
	float ratio;
	bool disableCollisionsBetweenLinkedBodies;
};
btTypedConstraint* addConstraintToNodes(const NodePath& targetA, const NodePath& targetB,
		const Physics& physics,	const ConstraintData& constraintData);
bool removeConstraintFromNodes(btTypedConstraint* constraint, const Physics& physics);
bool removeConstraintsFromNode(const NodePath& target, const Physics& physics);

}

#endif /* CONSTRAINT_H_ */
