/**
 * \file softBody.cpp
 *
 * \date 2017-03-08
 * \author consultit
 */

#include "softBody.h"
#include "physics.h"
#include "../bullet3/src/BulletSoftBody/btSoftBodyHelpers.h"
#include "../bullet3/src/BulletSoftBody/btSoftMultiBodyDynamicsWorld.h"
#include <ropeNode.h>
#include <geomVertexRewriter.h>
#include <geomTriangles.h>
#include <geomLines.h>

namespace
{

void moveNodeTransformToBody(const NodePath& target, btSoftBody* body)
{
	CPT(TransformState) nodeTS = target.node()->get_transform();
	//the order of transformations is important: R -> T -> S
	body->rotate(bt3::LQuaternion_to_btQuat(nodeTS->get_quat()));
	body->translate(bt3::LVecBase3_to_btVector3(nodeTS->get_pos()));
	body->scale(bt3::LVecBase3_to_btVector3_abs(nodeTS->get_scale()));
	target.node()->set_transform(TransformState::make_identity());
}

btSoftBody* createSoftBodyForNode(const NodePath& target, bt3::SoftBodyData& sfData,
		btSoftBodyWorldInfo& sfWorldInfo)
{
	ASSERT_TRUE(sfData.type != bt3::SoftBodyData::SoftBodyType::INVALID_TYPE)
	//
	bt3::SoftBodyManager& manager = bt3::SoftBodyManager::GetSingleton();
	// fixme: the target's Geoms/curves vertices/normals are updated in absolute
	// coordinates, so the target's transform has no influence in that update,
	// and it is reset to the identity.
	btSoftBody* body = nullptr;
	// soft body manager data
	bt3::SoftBodyManager::UpdateData updateData;
	switch (sfData.type)
	{
	case bt3::SoftBodyData::SoftBodyType::ROPE:
	{
		//check for RopeNode
		PT(RopeNode)ropeNode = nullptr;
		NodePath ropeNP = target.find("**/+RopeNode");
		if (!ropeNP.is_empty())
		{
			ropeNode = DCAST(RopeNode, ropeNP.node());
		}
		else if (target.node()->is_of_type(RopeNode::get_class_type()))
		{
			ropeNode = DCAST(RopeNode, target.node());
		}
		//check for GeomNode
		PT(GeomNode)geomNode = nullptr;
		NodePath geomNP = target.find("**/+GeomNode");
		if (!geomNP.is_empty())
		{
			geomNode = DCAST(GeomNode, geomNP.node());
		}
		else if (target.node()->is_of_type(GeomNode::get_class_type()))
		{
			geomNode = DCAST(GeomNode, target.node());
		}
		// create the soft body and visualization data
		PT(NurbsCurveEvaluator)curve = nullptr;
		PT(Geom) geom = nullptr;
		if (ropeNode)
		{
			curve = ropeNode->get_curve();
			if (curve.is_null())
			{
				break;
			}
			// deduce res from curve's vertices, provided it turns out to be > 0
			if (curve->get_num_vertices() > 2)
			{
				sfData.ropeData.res = curve->get_num_vertices() - 2;
			}
			else
			{
				// set curve's vertices
				curve->reset(sfData.ropeData.res + 2);
				//note: other (NURBS) curve's parameters are not set, so they
				//take their default values
			}
			body = btSoftBodyHelpers::CreateRope(sfWorldInfo,
					bt3::LVecBase3_to_btVector3(sfData.ropeData.from),
					bt3::LVecBase3_to_btVector3(sfData.ropeData.to), sfData.ropeData.res,
					sfData.ropeData.fixeds);
			updateData.node = ropeNode;
			updateData.curve = curve;
		}
		else if (geomNode)
		{
			body = btSoftBodyHelpers::CreateRope(sfWorldInfo,
					bt3::LVecBase3_to_btVector3(sfData.ropeData.from),
					bt3::LVecBase3_to_btVector3(sfData.ropeData.to), sfData.ropeData.res,
					sfData.ropeData.fixeds);
			// make a new geom for rope
			geom = manager.make_geom(body, updateData.indexMap, false);
			// set the first geom of geom node
			if (geomNode->get_num_geoms() > 0)
			{
				geomNode->set_geom(0, geom);
			}
			else
			{
				geomNode->add_geom(geom);
			}
			updateData.node = geomNode;
			// update only the first geom
			updateData.geoms.push_back(geomNode->modify_geom(0));
			updateData.geomsStartIndexes.push_back(0);
		}
		else
		{
			break;
		}
		// move target transform to body
		moveNodeTransformToBody(target, body);
		// set total mass
		body->setTotalMass(sfData.totalMass, sfData.fromFaces);
		//register with soft body manager
		updateData.body = body;
		manager.getUpdateData().push_back(updateData);
	}
		break;
	case bt3::SoftBodyData::SoftBodyType::PATCH:
	case bt3::SoftBodyData::SoftBodyType::ELLIPSOID:
	case bt3::SoftBodyData::SoftBodyType::TRIANGLE_MESH:
	case bt3::SoftBodyData::SoftBodyType::CONVEX_HULL:
	case bt3::SoftBodyData::SoftBodyType::TETGEN_MESH:
	{
		//check for GeomNode
		PT(GeomNode)geomNode = nullptr;
		NodePath geomNP = target.find("**/+GeomNode");
		if (!geomNP.is_empty())
		{
			geomNode = DCAST(GeomNode, geomNP.node());
		}
		else if (target.node()->is_of_type(GeomNode::get_class_type()))
		{
			geomNode = DCAST(GeomNode, target.node());
		}
		else
		{
			break;
		}
		// create the soft body and visualization data
		PT(Geom) geom = nullptr;
		switch (sfData.type)
		{
			case bt3::SoftBodyData::SoftBodyType::PATCH:
			{
				body = btSoftBodyHelpers::CreatePatch(sfWorldInfo,
						bt3::LVecBase3_to_btVector3(sfData.patchData.corner[0][0]),
						bt3::LVecBase3_to_btVector3(sfData.patchData.corner[1][0]),
						bt3::LVecBase3_to_btVector3(sfData.patchData.corner[0][1]),
						bt3::LVecBase3_to_btVector3(sfData.patchData.corner[1][1]),
						sfData.patchData.resx, sfData.patchData.resy,
						sfData.patchData.fixeds, sfData.patchData.gendiags);
				// move target transform to body
				moveNodeTransformToBody(target, body);
				// make a new geom for patch
				geom = manager.make_geom(body, updateData.indexMap, true);
				// make texcoords for patch
				manager.make_texcoords_for_patch(geom, sfData.patchData.resx,
						sfData.patchData.resy);
				// set the first geom of geom node
				if (geomNode->get_num_geoms() > 0)
				{
					geomNode->set_geom(0, geom);
				}
				else
				{
					geomNode->add_geom(geom);
				}
				// update only the first geom
				updateData.geoms.push_back(geomNode->modify_geom(0));
				updateData.geomsStartIndexes.push_back(0);
			}
				break;
			case bt3::SoftBodyData::SoftBodyType::ELLIPSOID:
			{
				body = btSoftBodyHelpers::CreateEllipsoid(sfWorldInfo,
						bt3::LVecBase3_to_btVector3(sfData.ellipsoidData.center),
						bt3::LVecBase3_to_btVector3_abs(sfData.ellipsoidData.radius),
						sfData.ellipsoidData.res);
				// move target transform to body
				moveNodeTransformToBody(target, body);
				// make a new geom for ellipsoid
				geom = manager.make_geom(body, updateData.indexMap, true);
				// set the first geom of geom node
				if (geomNode->get_num_geoms() > 0)
				{
					geomNode->set_geom(0, geom);
				}
				else
				{
					geomNode->add_geom(geom);
				}
				// update only the first geom
				updateData.geoms.push_back(geomNode->modify_geom(0));
				updateData.geomsStartIndexes.push_back(0);
			}
				break;
			case bt3::SoftBodyData::SoftBodyType::TRIANGLE_MESH:
			{
				// get a geometry processor
				bt3::GeometryProcessor* processor = new bt3::GeometryProcessor();
				// process the Geoms of the unique GeomNode with the target transform applied
				processor->load(target, NodePath());
				// remove duplicate vertices and get an equivalent triangle mesh for soft body
				int outVertCount;
				const float* outVerts;
				const int* outTris;
				int outTriCount = processor->getTriCount();
				manager.make_triangle_mesh_no_duplicate(processor->getVerts(),
						processor->getVertCount(), processor->getTris(), processor->getTriCount(),
						&outVerts, &outVertCount, &outTris, updateData.indexMap, 1e-5f);
				// save geoms' start/end indexes before deleting processor
				updateData.geomsStartIndexes = move(processor->getGeomsStartIndexes());
				// free processor storage
				delete processor;
				// vertices are in panda3d space: convert them in bullet space
				float* outVertsBT = new float[outVertCount * 3];
				for(int i = 0, c = 0; i < outVertCount; i++, c+= 3)
				{
					btVector3&& v = bt3::LVecBase3_to_btVector3(
							LPoint3f(outVerts[c],outVerts[c + 1],outVerts[c + 2]));
					outVertsBT[c] = v.getX();
					outVertsBT[c + 1] = v.getY();
					outVertsBT[c + 2] = v.getZ();
				}
				// free outVerts storage
				delete[] outVerts;
				// create the body
				body = btSoftBodyHelpers::CreateFromTriMesh(sfWorldInfo,
						outVertsBT, outTris, outTriCount,
						sfData.triangleMeshData.randomizeConstraints);
				// free remaining storage
				delete[] outVertsBT;
				delete[] outTris;
				// geom node could have several geoms
				for (int i = 0; i < geomNode->get_num_geoms(); i++)
				{
					updateData.geoms.push_back(geomNode->modify_geom(i));
				}
				// reset target's transform
				target.node()->set_transform(TransformState::make_identity());
			}
				break;
			case bt3::SoftBodyData::SoftBodyType::CONVEX_HULL:
			{
				// get a geometry processor to analyze the model
				bt3::GeometryProcessor* processor = new bt3::GeometryProcessor();
				// process the Geoms of the unique GeomNode with the target transform applied
				processor->load(target, NodePath());
				//
				int outVertCount = processor->getVertCount();
				// vertices are in panda3d space: convert them in bullet space and packe them in btVector3s
				btVector3* outVertices = new btVector3[outVertCount];
				for(int i = 0, c = 0; i < outVertCount; i++, c+= 3)
				{
					outVertices[i] = bt3::LVecBase3_to_btVector3(
							LPoint3f(processor->getVerts()[c], processor->getVerts()[c + 1],
									processor->getVerts()[c + 2]));
				}
				// free processor storage
				delete processor;
				// create the body
				body = btSoftBodyHelpers::CreateFromConvexHull(sfWorldInfo, outVertices,
						outVertCount, sfData.triangleMeshData.randomizeConstraints);
				// free outVertices storage
				delete[] outVertices;
				// create a new GeomNode parented to the target's parent
				PT(GeomNode)nodeCH = new GeomNode(target.get_name() + "_convexHull");
				target.get_parent().attach_new_node(nodeCH);
				NodePath nodeCHNP = NodePath::any_path(nodeCH);
				/// nodeCH inherits some features from target:
				// -render state
				nodeCHNP.set_state(target.get_state());
				// -bounding volume
				nodeCHNP.node()->set_bounds(target.get_bounds());
				nodeCHNP.node()->set_final(true);
				///
				// make a new geom for convex hull
				geom = manager.make_geom(body, updateData.indexMap, true);
				// set the first geom of geom node
				nodeCH->add_geom(geom);
				// update only the first geom
				updateData.geoms.push_back(nodeCH->modify_geom(0));
				updateData.geomsStartIndexes.push_back(0);
			}
				break;
			case bt3::SoftBodyData::SoftBodyType::TETGEN_MESH:
			{
				body = btSoftBodyHelpers::CreateFromTetGenData(sfWorldInfo,
						sfData.tetgenMeshData.ele.c_str(),
						sfData.tetgenMeshData.face.c_str(),
						sfData.tetgenMeshData.node.c_str(),
						sfData.tetgenMeshData.bfacelinks,
						sfData.tetgenMeshData.btetralinks,
						sfData.tetgenMeshData.bfacesfromtetras);
				// move target transform to body
				moveNodeTransformToBody(target, body);
				// make a new geom for tetgen mesh
				geom = manager.make_geom(body, updateData.indexMap, false);
				// set the first geom of geom node
				if (geomNode->get_num_geoms() > 0)
				{
					geomNode->set_geom(0, geom);
				}
				else
				{
					geomNode->add_geom(geom);
				}
				// update only the first geom
				updateData.geoms.push_back(geomNode->modify_geom(0));
				updateData.geomsStartIndexes.push_back(0);
			}
				break;
			default:
				break;
		}
		body->setTotalMass(sfData.totalMass, sfData.fromFaces);
		//register with soft body manager
		updateData.body = body;
		updateData.node = geomNode;
		manager.getUpdateData().push_back(updateData);
	}
		break;
	default:
		break;
	}
	// set target's PandaNode as soft body's user pointer
	body->setUserPointer((void*) target.node());
	// return the just created soft body
	return body;
}

} //namespace anonymous

namespace bt3
{

//SoftBodyManager
void SoftBodyManager::update(float dt)
{
	for (auto uData : mUpdatedData)
	{
		if (!uData.curve.is_null())
		{
			btSoftBody::tNodeArray &nodes(uData.body->m_nodes);
			for (int i = 0; i < nodes.size(); i++)
			{
				btVector3 pos = nodes[i].m_x;
				uData.curve->set_vertex(i, btVector3_to_LVecBase3(pos));
			}
			uData.node->mark_internal_bounds_stale();
		}
		if (!uData.geoms.empty())
		{
			// main vertex index
			int geomIdx = 0;
			for (auto geom: uData.geoms)
			{
				//
				PT(GeomVertexData)vdata = geom->modify_vertex_data();
				GeomVertexRewriter vertices(vdata, InternalName::get_vertex());
				GeomVertexRewriter normals(vdata, InternalName::get_normal());
				/* Each soft body contain an array of vertices (nodes/particles_mass)   */
				btSoftBody::tNodeArray& nodes(uData.body->m_nodes);
				//
				int startInIdx = uData.geomsStartIndexes[geomIdx];
				int j = 0;
				while (!vertices.is_at_end())
				{
					btVector3 &v = nodes[uData.indexMap[startInIdx + j]].m_x;
					btVector3 &n = nodes[uData.indexMap[startInIdx + j]].m_n;

					vertices.set_data3(btVector3_to_LVecBase3(v));
					normals.set_data3(btVector3_to_LVecBase3(n));

					j++;
				}
				geomIdx++;
			}
			uData.node->mark_internal_bounds_stale();
		}
	}
}

PT(Geom)SoftBodyManager::make_geom(btSoftBody *body, vector<int>& indexMap, bool use_faces)
{
	//see http://bulletphysics.org/mediawiki-1.5.8/index.php/Soft_Body_Rendering
	CPT(GeomVertexFormat) fmt = GeomVertexFormat::get_v3n3t2();

	PT(GeomVertexData) vdata = new GeomVertexData("", fmt, Geom::UH_stream);
	GeomVertexWriter vwriter(vdata, InternalName::get_vertex());
	GeomVertexWriter nwriter(vdata, InternalName::get_normal());

	/* Each soft body contain an array of vertices (nodes/particles_mass)   */
	btSoftBody::tNodeArray& nodes(body->m_nodes);

	/* And edges (links/distances constraints)                        */
	btSoftBody::tLinkArray& links(body->m_links);

	/* And finally, faces (triangles)                                 */
	btSoftBody::tFaceArray& faces(body->m_faces);

	/* Then, you can draw vertices...      */
	/* Node::m_x => position            */
	/* Node::m_n => normal (if meaningful)   */
	indexMap.resize(nodes.size());
	for(int j=0;j<nodes.size();++j)
	{
		btVector3 &v = nodes[j].m_x;
		btVector3 &n = nodes[j].m_n;

		vwriter.add_data3(btVector3_to_LVecBase3(v));
		nwriter.add_data3(btVector3_to_LVecBase3(n));

		//make a bijective vertex index map
		indexMap[j] = j;
	}

	// GeomPrimitive
	PT(GeomPrimitive) prim = nullptr;
	if (use_faces)
	{
		prim = new GeomTriangles(Geom::UH_stream);
		prim->set_shade_model(Geom::SM_uniform);

		/* And even faces                  */
		/* Face::m_n[3] -> pointers to nodes   */
		for(int j=0;j<faces.size();++j)
		{
			btSoftBody::Node* node_0=faces[j].m_n[0];
			btSoftBody::Node* node_1=faces[j].m_n[1];
			btSoftBody::Node* node_2=faces[j].m_n[2];

			/* Or if you need indices...      */
			const int indices[]=
			{
				int(node_0-&nodes[0]),
				int(node_1-&nodes[0]),
				int(node_2-&nodes[0])
			};

			prim->add_vertices(indices[0], indices[1], indices[2]);
			prim->close_primitive();
		}
	}
	else
	{
		prim = new GeomLines(Geom::UH_stream);
		prim->set_shade_model(Geom::SM_uniform);

		/* Or edges (for ropes)               */
		/* Link::m_n[2] => pointers to nodes   */
		for(int j=0;j<links.size();++j)
		{
			btSoftBody::Node* node_0=links[j].m_n[0];
			btSoftBody::Node* node_1=links[j].m_n[1];

			/* Or if you need indices...      */
			const int indices[]=
			{
				int(node_0-&nodes[0]),
				int(node_1-&nodes[0])
			};
			prim->add_vertices(indices[0], indices[1]);
			prim->close_primitive();
		}
	}

	// Geom
	PT(Geom) geom = new Geom(vdata);
	geom->add_primitive(prim);

	return geom;
}

void SoftBodyManager::make_texcoords_for_patch(Geom *geom, int resx, int resy)
{
	PT(GeomVertexData)vdata = geom->modify_vertex_data();

	nassertv(vdata->has_column(InternalName::get_texcoord()));

	GeomVertexRewriter texcoords(vdata, InternalName::get_texcoord());

	int n = resx * resy;
	int i = 0;
	int ix;
	int iy;
	float u;
	float v;

	while (!texcoords.is_at_end())
	{
		ix = i / resx;
		iy = i % resy;

		if (i > n) ix -= 1;

		u = (float)ix/(float)(resx - 1);
		v = (float)iy/(float)(resy - 1);

		texcoords.set_data2f(u, v);
		i++;
	}
}

/**
 * Computes from a given input triangle mesh, an equivalent output mesh with
 * duplicate (ie nearly equal) vertices welded together and with same number
 * of triangles which are equivalent (ie nearly equal) to the original ones.
 * Sets a vector (indexMap) which maps input vertex indexes to output ones:
 * 		vector[inIdx] == outIdx
 * that is the inIdx-th vertex of the input triangle mesh has been welded (together with
 * possibly other vertices) into the outIdx-th vertex of the output triangle mesh.
 * This method returns both output vertices and triangles as output parameters by
 * allocating storage for them; it is responsibility of the caller to free such storage.
 */
void SoftBodyManager::make_triangle_mesh_no_duplicate(const float* inVerts, int inVertCount,
		const int* inTris, int inTriCount, const float** outVerts, int* outVertCount,
		const int** outTris, vector<int>& indexMap, float delta)
{
#ifdef ELY_DEBUG
	cout << "input vertices: " << endl;
	for(int i = 0, c=0; i < inVertCount; i++, c += 3)
	{
		cout << i << ": " <<
				LPoint3f(inVerts[c], inVerts[c + 1], inVerts[c + 2]) << endl;
	}
	cout << "equivalence classes' procedure:" << endl;
#endif //ELY_DEBUG
	// find the array of equivalence-classes of input vertices:
	// based on a distance criterion. The size of the array is
	// the number of soft-body's vertices. O(n^2)
	vector<set<int>> eqClasses;
	indexMap.resize(inVertCount);
	vector<bool> vertInClass;
	vertInClass.resize(inVertCount, false);
	// i=index, c=component
	for(int i = 0, ci = 0; i < inVertCount; i++, ci += 3)
	{
		if (!vertInClass[i])
		{
			// vertex_i create a new class
			eqClasses.push_back(set<int>());
			unsigned int curI = eqClasses.size() - 1;
			eqClasses[curI].insert(i);
			indexMap[i] = curI;
			vertInClass[i] = true;
			LPoint3f vertex_i(inVerts[ci], inVerts[ci + 1],	inVerts[ci + 2]);
			PRINT_DEBUG(vertex_i);
			for(int j = i+1, cj = ci + 3; j < inVertCount; j++, cj += 3)
			{
				if (!vertInClass[j])
				{
					LPoint3f vertex_j(inVerts[cj], inVerts[cj + 1], inVerts[cj + 2]);
					// check equivalence
					if ((vertex_i - vertex_j).length() < delta)
					{
						// vj equivalent to vi
						eqClasses[curI].insert(j);
						indexMap[j] = curI;
						vertInClass[j] = true;
						PRINT_DEBUG("\t" << vertex_j);
					}
				}
			}
		}
	}
	// set output vertex count
	int oVertexCount = eqClasses.size();
#ifdef ELY_DEBUG
	cout << "input vertex indexes TO output vertex indexes:" << endl;
	for (int i = 0; i < inVertCount; i++)
	{
		cout << "'" << i << "' -> '"
		<< indexMap[i] << "'" << endl;
	}
	cout << "equivalence classes:" << endl;
	for (auto s: eqClasses)
	{
		for (auto i: s)
		{
			cout << i << ",";
		}
		int idx = *(s.begin());
		cout << ": " << LPoint3f(inVerts[3 * idx], inVerts[3 * idx + 1], inVerts[3 * idx + 2]);
		cout << endl;
	}
#endif //ELY_DEBUG
	// write the output vertices and triangles:
	// - one equivalence class corresponds to each vertex
	// - three equivalence classes correspond to each triangle
	float *oVertices = new float[oVertexCount * 3];
	for (int i = 0, c = 0; i < oVertexCount; i++, c+=3)
	{
		// take an index in the equivalence class, for example the first in set
		int idx = *(eqClasses[i].begin());
		// set output vertex components with the corresponding input vertex ones
		oVertices[c] = inVerts[3 * idx];
		oVertices[c + 1] = inVerts[3 * idx + 1];
		oVertices[c + 2] = inVerts[3 * idx + 2];
	}
	// the output triangles are the same with indexes changed
	int* oTriangles = new int[inTriCount * 3];
	for (int i = 0, c = 0; i < inTriCount; i++, c+=3)
	{
		// input indexes
		int i0 = inTris[c];
		int i1 = inTris[c + 1];
		int i2 = inTris[c + 2];
		// output indexes
		oTriangles[c] = indexMap[i0];
		oTriangles[c + 1] = indexMap[i1];
		oTriangles[c + 2] = indexMap[i2];
	}
#ifdef ELY_DEBUG
	cout << "output vertices:" << endl;
	for (unsigned int i = 0; i < eqClasses.size(); i++)
	{
		cout << i << ": " << LPoint3f(oVertices[3 * i],
				oVertices[3 * i + 1], oVertices[3 * i + 2]) << endl;
	}
	cout << "input triangles VS output triangles: " << endl;
	for(int i = 0, c=0; i < inTriCount; i++, c += 3)
	{
		//geom
		int i0Tg = indexMap[inTris[c]];
		int i1Tg = indexMap[inTris[c + 1]];
		int i2Tg = indexMap[inTris[c + 2]];
		int i0g = *(eqClasses[i0Tg].begin());
		int i1g = *(eqClasses[i1Tg].begin());
		int i2g = *(eqClasses[i2Tg].begin());
		LPoint3f v0g(inVerts[3 * i0g], inVerts[3 * i0g + 1], inVerts[3 * i0g + 2]);
		LPoint3f v1g(inVerts[3 * i1g], inVerts[3 * i1g + 1], inVerts[3 * i1g + 2]);
		LPoint3f v2g(inVerts[3 * i2g], inVerts[3 * i2g + 1], inVerts[3 * i2g + 2]);
		cout << "input: " << i << ": " << LVecBase3i(inTris[c], inTris[c + 1],
				inTris[c + 2]) << endl;
		//soft-body
		int i0s = oTriangles[c];
		int i1s = oTriangles[c + 1];
		int i2s = oTriangles[c + 2];
		LPoint3f v0s(oVertices[3 * i0s], oVertices[3 * i0s + 1], oVertices[3 * i0s + 2]);
		LPoint3f v1s(oVertices[3 * i1s], oVertices[3 * i1s + 1], oVertices[3 * i1s + 2]);
		LPoint3f v2s(oVertices[3 * i2s], oVertices[3 * i2s + 1], oVertices[3 * i2s + 2]);
		cout << "output: " << i << ": " << LVecBase3i(i0s, i1s, i2s) << endl;
		//
		cout << "\tinput  vertex: [(" << v0g << "),(" << v1g << "),(" << v2g << ")]" << endl;
		cout << "\toutput vertex: [(" << v0s << "),(" << v1s << "),(" << v2s << ")]" << endl;
		cout << "\t\tdelta lenghts: [" << (v0g - v0s).length() <<
				"," << (v1g - v1s).length() << ","
				<< (v2g - v2s).length() << "]" << endl;
	}
#endif //ELY_DEBUG
	// set output parameters
	*outVertCount = oVertexCount;
	*outVerts = oVertices;
	*outTris = oTriangles;
}

void addSoftBodyToNode(NodePath& target, Physics& physics, SoftBodyData& sfData,
		BitMask32 groupMask, BitMask32 collideMask)
{
	if (target.is_empty())
	{
		return;
	}
	// check soft world
	btSoftRigidDynamicsWorld* softRigiWorld = nullptr;
	btSoftMultiBodyDynamicsWorld* softMultiBodyWorld = nullptr;
	switch (physics.physicsInfo.worldType)
	{
	case PhysicsInfo::WorldType::SoftRigid:
		softRigiWorld =
				static_cast<btSoftRigidDynamicsWorld*>(physics.dynamicsWorld);
		break;
	case PhysicsInfo::WorldType::SoftMultiBody:
		softMultiBodyWorld =
				static_cast<btSoftMultiBodyDynamicsWorld*>(physics.dynamicsWorld);
		break;
	default:
		break;
	}
	if (!softRigiWorld && !softMultiBodyWorld)
	{
		return;
	}
	// add a soft body to node
	btSoftBody* body = createSoftBodyForNode(target, sfData,
			physics.softBodyWorldInfo);
	// add the soft body to the dynamics world
	if (softMultiBodyWorld)
	{
		softMultiBodyWorld->addSoftBody(body, groupMask.get_word(),
				collideMask.get_word());
	}
	else
	{
		softRigiWorld->addSoftBody(body, groupMask.get_word(),
				collideMask.get_word());
	}
	//add the "physics" tag to target node
	if (!target.has_tag("physics"))
	{
		target.set_tag("physics", "yes");
	}
	PRINT_DEBUG("addSoftBodyToNode(): " << target.get_name());
}

bool removeSoftBodyFromNode(NodePath& target, const Physics& physics)
{
	bool result = false;
	if (target.is_empty())
	{
		return result;
	}

	//delete the soft body from the dynamics world and delete it
	for (int i = 0; i < physics.dynamicsWorld->getNumCollisionObjects(); i++)
	{
		btCollisionObject* obj =
				physics.dynamicsWorld->getCollisionObjectArray()[i];
		if (reinterpret_cast<PandaNode*>(obj->getUserPointer())
				== target.node())
		{
			//check if it is a soft body
			btSoftBody* body = btSoftBody::upcast(obj);
			if (body)
			{
				// unregister from soft body manager
				SoftBodyManager* manager = SoftBodyManager::GetSingletonPtr();
				if (manager)
				{
					for (auto iter = manager->getUpdateData().begin();
							iter!=manager->getUpdateData().end(); ++iter )
					{
						if (iter->body == body)
						{
							manager->getUpdateData().erase(iter);
							break;
						}
					}
				}
				physics.dynamicsWorld->removeCollisionObject(body);
				delete body;
				//remove the "physics" tag from target node
				if (target.has_tag("physics"))
				{
					target.clear_tag("physics");
				}
				//removed: exit loop
				result = true;
				break;
			}
		}
	}
	PRINT_DEBUG(
			(result ? "" : "not ") << "removeSoftBodyFromNode(): " << target.get_name());
	//
	return result;
}

} //namespace bt3
