/**
 * \file common.cpp
 *
 * \date 2016-12-22
 * \author consultit
 */

#include "common.h"
#include <nodePathCollection.h>
#include <geomVertexReader.h>
#include <geomNode.h>
#include "physics.h"

namespace bt3
{
// get (tight) bounding dimensions
float getBoundingDimensions(NodePath modelNP, LVecBase3f& modelDims,
		LVector3f& modelDeltaCenter, Panda3dUpAxis upAxis)
{
	// save pos and hpr (not scale which contribute to dimension)
	LPoint3f pos = modelNP.get_pos();
	LVecBase3f hpr = modelNP.get_hpr();
	//reset pos and hpr
	modelNP.set_pos(LPoint3f::zero());
	modelNP.set_hpr(LVecBase3f::zero());
	//get "tight" dimensions of model
	LPoint3f minP, maxP;
	modelNP.calc_tight_bounds(minP, maxP);
	//
	LVecBase3 delta = maxP - minP;
	LVector3f deltaCenter = -(minP + delta / 2.0);
	//
	modelDims.set(abs(delta.get_x()), abs(delta.get_y()), abs(delta.get_z()));
	modelDeltaCenter.set(deltaCenter.get_x(), deltaCenter.get_y(),
			deltaCenter.get_z());
	float modelRadius;
	if (upAxis == Panda3dUpAxis::X_up)
	{
		modelRadius = max(modelDims.get_y(), modelDims.get_z()) / 2.0;
	}
	else if (upAxis == Panda3dUpAxis::Y_up)
	{
		modelRadius = max(modelDims.get_x(), modelDims.get_z()) / 2.0;
	}
	else if (upAxis == Panda3dUpAxis::Z_up)
	{
		modelRadius = max(modelDims.get_x(), modelDims.get_y()) / 2.0;
	}
	else
	{
		modelRadius = max(max(modelDims.get_x(), modelDims.get_y()),
				modelDims.get_z()) / 2.0;
	}
	//restore pos and hpr
	modelNP.set_pos(pos);
	modelNP.set_hpr(hpr);
	return modelRadius;
}

} //namespace bt3

//Geometry
namespace
{
void reportGeomNode(CPT(GeomNode)geomnode);
void reportGeom(CPT(Geom)geom);
void reportVertexData(CPT(GeomVertexData)vdata);
void reportPrimitive(CPT(GeomPrimitive)prim, CPT(GeomVertexData)vdata);
} //namespace anonymous

namespace bt3
{

void reportGeometry(const NodePath& model)
{
NodePathCollection geomNodeCollection = model.find_all_matches("**/+GeomNode");
for (int i = 0; i < geomNodeCollection.get_num_paths(); i++)
{
	PT(GeomNode)geomNode = DCAST(GeomNode,geomNodeCollection.get_path(i).node());
	geomNode->write(nout, 1);
	reportGeomNode (geomNode);
}
}

} //namespace bt3

namespace
{
void reportGeomNode(CPT(GeomNode)geomnode)
{
	for (int j = 0; j < geomnode->get_num_geoms(); j++)
	{
		CPT(Geom)geom = geomnode->get_geom(j);
		geom->write(nout, 2);	//outputs basic info on the geom
		nout << *(geomnode->get_geom_state(j)) << endl;//basic renderstate info
		reportGeom(geom);
	}
}

void reportGeom(CPT(Geom)geom)
{
	CPT(GeomVertexData)vdata = geom->get_vertex_data();
	vdata->write(nout, 3);
	reportVertexData(vdata);
	for(size_t i=0;i<geom->get_num_primitives();i++)
	{
		CPT(GeomPrimitive) prim = geom->get_primitive(i);
		prim->write(nout,3);
		reportPrimitive(prim, vdata);
	}
}

void reportVertexData(CPT(GeomVertexData)vdata)
{
	nout << "\tFormat = " << *(vdata->get_format()) << endl;
	// report only values with common names, formats and types
	GeomVertexReader vertex = GeomVertexReader(vdata, "vertex");
	if (!vertex.has_column())
	{
		return;
	}
	GeomVertexReader normal = GeomVertexReader(vdata, "normal");
	bool N = (normal.has_column() && (normal.get_column()->get_num_components() == 3) &&
			(normal.get_column()->get_numeric_type() == Geom::NT_float32));
	GeomVertexReader color = GeomVertexReader(vdata, "color");
	bool C = color.has_column() && (color.get_column()->get_num_components() == 3) &&
			(color.get_column()->get_numeric_type() == Geom::NT_float32);
	GeomVertexReader texcoord = GeomVertexReader(vdata, "texcoord");
	bool T = texcoord.has_column() && (texcoord.get_column()->get_num_components() == 2) &&
			(texcoord.get_column()->get_numeric_type() == Geom::NT_float32);
	while (!vertex.is_at_end())
	{
		LPoint3f v = vertex.get_data3f();
		ostringstream nOS, cOS, tOS;
		if(N)
		{
			nOS << "| N = " << LVector3f(normal.get_data3f());
		}
		if(C)
		{
			cOS << "| C = " << LVecBase3f(color.get_data3f());
		}
		if(T)
		{
			tOS << "| T = " << LVecBase2f(texcoord.get_data2f());
		}
		nout << "\t\tV = " << v << nOS.str() << cOS.str() << tOS.str() << endl;
	}
}

void reportPrimitive(CPT(GeomPrimitive)prim, CPT(GeomVertexData)vdata)
{
	GeomVertexReader vertex = GeomVertexReader(vdata, "vertex");
	//Note: There should be prim = prim->decompose(); here,it wouldnt work
	//for me but i use the cvs panda and that could have been broken at this time.
	prim = prim->decompose();
	for (int k = 0; k < prim->get_num_primitives(); k++)
	{
		int s = prim->get_primitive_start(k);
		int e = prim->get_primitive_end(k);
		for (int i = s; i < e; i++)
		{
			int vi = prim->get_vertex(i);
			vertex.set_row(vi);
			LVector3f v = vertex.get_data3f();
			nout << "\t\tprim " << k << " has vertex " << vi << ": " << v << endl;
		}
	}
}
} //namespace anonymous

namespace bt3
{

GeometryProcessor::GeometryProcessor() :
		m_verts(0), m_tris(0), m_normals(0), m_vertCount(0), m_triCount(0), m_currentMaxIndex(
				0), vcap(0), tcap(0)
{
	m_geoms.clear();
	m_vertexData.clear();
	m_startIndices.clear();
}

GeometryProcessor::~GeometryProcessor()
{
	delete[] m_verts;
	m_verts = nullptr;
	delete[] m_normals;
	m_normals = nullptr;
	delete[] m_tris;
	m_tris = nullptr;
}

void GeometryProcessor::reset()
{
	delete[] m_verts;
	m_verts = nullptr;
	delete[] m_normals;
	m_normals = nullptr;
	delete[] m_tris;
	m_tris = nullptr;
	m_vertCount = 0;
	m_triCount = 0;
	m_currentMaxIndex = 0;
	vcap = 0;
	tcap = 0;
	m_geoms.clear();
	m_vertexData.clear();
	m_startIndices.clear();
}

void GeometryProcessor::addVertex(float x, float y, float z, int& cap)
{
	if (m_vertCount + 1 > cap)
	{
		cap = !cap ? 8 : cap * 2;
		float* nv = new float[cap * 3];
		if (m_vertCount)
			memcpy(nv, m_verts, m_vertCount * 3 * sizeof(float));
		delete[] m_verts;
		m_verts = nv;
	}
	float* dst = &m_verts[m_vertCount * 3];
	*dst++ = x;
	*dst++ = y;
	*dst++ = z;
	m_vertCount++;
}

void GeometryProcessor::addTriangle(int a, int b, int c, int& cap)
{
	if (m_triCount + 1 > cap)
	{
		cap = !cap ? 8 : cap * 2;
		int* nv = new int[cap * 3];
		if (m_triCount)
			memcpy(nv, m_tris, m_triCount * 3 * sizeof(int));
		delete[] m_tris;
		m_tris = nv;
	}
	int* dst = &m_tris[m_triCount * 3];
	*dst++ = a;
	*dst++ = b;
	*dst++ = c;
	m_triCount++;
}

bool GeometryProcessor::load(NodePath model, NodePath referenceNP)
{
	//reset max index
	m_currentMaxIndex = 0;
	//all transform are applied wrt reference node
	//get current model transform
	m_currentTranformMat = model.get_transform(referenceNP)->get_mat();
	///Elaborate
	//Walk through all the model's GeomNodes
	NodePathCollection geomNodeCollection;
	// check if model is GeomNode itself (i.e. when procedurally generated)
	if (model.node()->is_of_type(GeomNode::get_class_type()))
	{
		geomNodeCollection.add_path(model);
	}
	else
	{
		//get all GeomNodes for the hierarchy below model
		geomNodeCollection = model.find_all_matches("**/+GeomNode");
	}
	//
	int numPaths = geomNodeCollection.get_num_paths();
	PRINT_DEBUG("\tGeomNodes number: " << numPaths);
	for (int i = 0; i < numPaths; i++)
	{
		PT(GeomNode)geomNode =
				DCAST(GeomNode,geomNodeCollection.get_path(i).node());
		processGeomNode(geomNode);
	}

	// Calculate normals.
	m_normals = new float[m_triCount * 3];
	for (int i = 0; i < m_triCount * 3; i += 3)
	{
		const float* v0 = &m_verts[m_tris[i] * 3];
		const float* v1 = &m_verts[m_tris[i + 1] * 3];
		const float* v2 = &m_verts[m_tris[i + 2] * 3];
		float e0[3], e1[3];
		for (int j = 0; j < 3; ++j)
		{
			e0[j] = v1[j] - v0[j];
			e1[j] = v2[j] - v0[j];
		}
		float* n = &m_normals[i];
		n[0] = e0[1] * e1[2] - e0[2] * e1[1];
		n[1] = e0[2] * e1[0] - e0[0] * e1[2];
		n[2] = e0[0] * e1[1] - e0[1] * e1[0];
		float d = sqrtf(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
		if (d > 0)
		{
			d = 1.0f / d;
			n[0] *= d;
			n[1] *= d;
			n[2] *= d;
		}
	}

	return true;
}

void GeometryProcessor::processGeomNode(PT(GeomNode)geomNode)
{
	//Walk through the list of GeomNode's Geoms
	int numGeoms = geomNode->get_num_geoms();
	PRINT_DEBUG("\t Geoms number: " << numGeoms);
	for (int j = 0; j < numGeoms; j++)
	{
		CPT(Geom)geom = geomNode->get_geom(j);
		PRINT_DEBUG("\t   Geom '" << j << "'");
		processGeom(geom);
	}
}

void GeometryProcessor::processGeom(CPT(Geom)geom)
{
	//check if there are triangles
	GeomEnums::PrimitiveType primType = geom->get_primitive_type();
	if ((primType != GeomEnums::PT_polygons) && (primType != GeomEnums::PT_none))
	{
		return;
	}
	if (primType == GeomEnums::PT_none)
	{
		PRINT_DEBUG("\t     Warning: this Geom doesn't specify the fundamental primitive type,");
		PRINT_DEBUG("\t     \tGeometryProcessor::load() could crash or give unexpected results.");
	}

	//insert geom
	m_geoms.push_back(geom);
	unsigned int geomIndex = m_geoms.size() - 1;
	CPT(GeomVertexData)vertexData = geom->get_vertex_data();
	//Process vertices
	processVertexData(vertexData);
	int numPrimitives = geom->get_num_primitives();
	PRINT_DEBUG("\t     Primitives number: " << numPrimitives);
	//Walk through the list of Geom's GeomPrimitives
	for(int i=0;i<numPrimitives;i++)
	{
		CPT(GeomPrimitive) primitive = geom->get_primitive(i);
		PRINT_DEBUG("\t       Primitive '" << i << "'");
		processPrimitive(primitive, geomIndex);
	}
}

void GeometryProcessor::processVertexData(CPT(GeomVertexData)vertexData)
{
	//check if vertexData already present
	unsigned int vertexDataIndex = m_vertexData.size();
	unsigned int index = 0;
	while(index < vertexDataIndex)
	{
		if(m_vertexData[index] == vertexData)
		{
			break;
		}
		++index;
	}
	if(index == vertexDataIndex)
	{
		//vertexData is not present
		GeomVertexReader vertexReader = GeomVertexReader(vertexData, "vertex");
		while (!vertexReader.is_at_end())
		{
			LVector3f vertex = vertexReader.get_data3f();
			m_currentTranformMat.xform_point_in_place(vertex);
			//add vertex
			addVertex(vertex[0], vertex[1], vertex[2], vcap);
		}
		m_startIndices.push_back(m_currentMaxIndex);
		//increment the max index
		int vertexNum = vertexData->get_num_rows();
		m_currentMaxIndex += vertexNum;
	}
	else
	{
		m_startIndices.push_back(m_startIndices[index]);
	}
	//insert vertexData any way
	m_vertexData.push_back(vertexData);
	PRINT_DEBUG("\t     Vertices number: " << vertexData->get_num_rows() <<
	" - Start index: " << m_startIndices.back());
}

void GeometryProcessor::processPrimitive(CPT(GeomPrimitive)primitive, unsigned int geomIndex)
{
	PRINT_DEBUG("\t         Primitive type: " <<
	primitive->get_type().get_name() <<
	" - number: " << primitive->get_num_primitives());
	//decompose to triangles
	CPT(GeomPrimitive)primitiveDec = primitive->decompose();
	int numPrimitives = primitiveDec->get_num_primitives();
	PRINT_DEBUG("\t         Decomposed Primitive type: " <<
	primitiveDec->get_type().get_name() <<
	" - number: " << numPrimitives);
	for (int k = 0; k < numPrimitives; k++)
	{
		int s = primitiveDec->get_primitive_start(k);
		int e = primitiveDec->get_primitive_end(k);
		//add vertex indices
		int vi[3];
		for (int j = s; j < e; ++j)
		{
			vi[j-s] = primitiveDec->get_vertex(j) +
			m_startIndices[geomIndex];
		}
		addTriangle(vi[0], vi[1], vi[2], tcap);
	}
}

GeometryProcessor& GeometryProcessor::operator=(const GeometryProcessor& copy)
{
	//vertices
	m_vertCount = copy.m_vertCount;
	m_verts = new float[m_vertCount * 3];
	for (int v = 0; v < m_vertCount * 3; ++v)
	{
		m_verts[v] = copy.m_verts[v];
	}
	//triangles
	m_triCount = copy.m_triCount;
	m_tris = new int[m_triCount * 3];
	for (int t = 0; t < m_triCount * 3; ++t)
	{
		m_tris[t] = copy.m_tris[t];
	}
	//normals
	m_normals = new float[m_triCount * 3];
	for (int n = 0; n < m_triCount * 3; ++n)
	{
		m_normals[n] = copy.m_normals[n];
	}
	//Model stuff
	m_geoms = copy.m_geoms;
	m_vertexData = copy.m_vertexData;
	m_startIndices = copy.m_startIndices;
	m_currentTranformMat = copy.m_currentTranformMat;
	m_currentMaxIndex = copy.m_currentMaxIndex;
	vcap = copy.vcap;
	tcap = copy.tcap;
	//
	return *this;
}

void GeometryProcessor::write_datagram(Datagram &dg) const
{
	//vertices
	dg.add_int32(m_vertCount);
	for (int v = 0; v < m_vertCount * 3; ++v)
	{
		dg.add_stdfloat(m_verts[v]);
	}
	//triangles
	dg.add_int32(m_triCount);
	for (int t = 0; t < m_triCount * 3; ++t)
	{
		dg.add_int32(m_tris[t]);
	}
	//normals
	for (int n = 0; n < m_triCount * 3; ++n)
	{
		dg.add_stdfloat(m_normals[n]);
	}

	///Model stuff not written: not needed for rebuilding
}

void GeometryProcessor::read_datagram(DatagramIterator &scan)
{
	//vertices
	m_vertCount = scan.get_int32();
	m_verts = new float[m_vertCount * 3];
	for (int v = 0; v < m_vertCount * 3; ++v)
	{
		m_verts[v] = scan.get_stdfloat();
	}
	//triangles
	m_triCount = scan.get_int32();
	m_tris = new int[m_triCount * 3];
	for (int t = 0; t < m_triCount * 3; ++t)
	{
		m_tris[t] = scan.get_int32();
	}
	//normals
	m_normals = new float[m_triCount * 3];
	for (int n = 0; n < m_triCount * 3; ++n)
	{
		m_normals[n] = scan.get_stdfloat();
	}

	///Model stuff not read: not needed for rebuilding
}

} //namespace bt3

namespace bt3
{

btCollisionObject* getPhysicsObjectOfNode(const NodePath& target, const Physics& physics)
{
	btCollisionObject* object = nullptr;
	if (target.is_empty())
	{
		return object;
	}

	//search the collision object into dynamics world
	for (int i = 0; i < physics.dynamicsWorld->getNumCollisionObjects(); i++)
	{
		btCollisionObject* obj =
				physics.dynamicsWorld->getCollisionObjectArray()[i];
		if (reinterpret_cast<PandaNode*>(obj->getUserPointer())
				== target.node())
		{

			object = obj;
			//found: exit loop
			break;
		}
	}
	PRINT_DEBUG(
			(object ? "" : "not ") << "getPhysicObjectOfNode(): " << target.get_name());
	//
	return object;
}

// make a (Dynamic)MotionState
DynamicMotionState* makeMotionState(const NodePath& target)
{
	LVecBase3f modelDims;
	LVector3f modelDeltaCenter;
	bt3::getBoundingDimensions(target, modelDims, modelDeltaCenter,
			bt3::Panda3dUpAxis::NO_up);
	//compute corrective transform for target
	CPT(TransformState)posTS = TransformState::make_pos(modelDeltaCenter);
	CPT(TransformState)scaleTS = TransformState::make_scale(target.get_scale());
	CPT(TransformState)deltaTS = posTS->compose(scaleTS);
	//create a rigid body
	bt3::DynamicMotionState* motionState = new bt3::DynamicMotionState(target, deltaTS,
			deltaTS->get_inverse());
	return motionState;
}

} //namespace bt3
