/**
 * \file common.h
 *
 * \date 2016-10-09
 * \author consultit
 */

#ifndef PHYSICSCOMMON_H_
#define PHYSICSCOMMON_H_

#include "../physics_includes.h"
#include <nodePath.h>
#include "../bullet3/src/LinearMath/btVector3.h"
#include "../bullet3/src/LinearMath/btQuaternion.h"
#include "../bullet3/src/BulletCollision/CollisionDispatch/btCollisionObject.h"

namespace bt3
{

/// Panda3d UpAxis
enum class Panda3dUpAxis: unsigned char
{
	X_up = 0, Z_up = 1, Y_up = 2, NO_up = 3
};

///Panda3d<-->Bullet3 axis index switch. Returns -1 on error.
inline int p3dBt3AxisSwitch(int index)
{
	int idx = -1;
	switch (index)
	{
	case 0:
		idx = 0;
		break;
	case 1:
		idx = 2;
		break;
	case 2:
		idx = 1;
		break;
	default:
		break;
	}
	return idx;
}

float getBoundingDimensions(NodePath modelNP, LVecBase3f& modelDims,
		LVector3f& modelDeltaCenter,
		Panda3dUpAxis upAxis = Panda3dUpAxis::Z_up);

/// Geometry
void reportGeometry(const NodePath& model);
class GeometryProcessor
{
public:
	GeometryProcessor();
	~GeometryProcessor();

	void reset();
	//Model stuff
	bool load(NodePath model, NodePath referenceNP);

	const float* getVerts() const
	{
		return m_verts;
	}
	const float* getNormals() const
	{
		return m_normals;
	}
	const int* getTris() const
	{
		return m_tris;
	}
	int getVertCount() const
	{
		return m_vertCount;
	}
	int getTriCount() const
	{
		return m_triCount;
	}
	vector<int> getGeomsStartIndexes() const
	{
#ifdef ELY_DEBUG
		cout << "start indexes:" << endl;
		int i = 0;
		for (auto idx: m_startIndices)
		{
			cout << i++ << ": " << idx << endl;
		}
#endif
		return m_startIndices;
	}

	GeometryProcessor& operator=(const GeometryProcessor&);
private:
	// Explicitly disabled copy constructor and copy assignment operator.
	GeometryProcessor(const GeometryProcessor&);

	void addVertex(float x, float y, float z, int& cap);
	void addTriangle(int a, int b, int c, int& cap);
	//Model stuff
	void processGeomNode(PT(GeomNode)geomNode);
	void processGeom(CPT(Geom)geom);
	void processVertexData(CPT(GeomVertexData)vertexData);
	void processPrimitive(CPT(GeomPrimitive)primitive, unsigned int geomIndex);

	float* m_verts;
	int* m_tris;
	float* m_normals;
	int m_vertCount;
	int m_triCount;
	//Model stuff
	vector<CPT(Geom)> m_geoms;
	vector<CPT(GeomVertexData)> m_vertexData;
	vector<int> m_startIndices;
	LMatrix4f m_currentTranformMat;
	int m_currentMaxIndex;
	int vcap, tcap;
public:
	//TypeWritable API
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};

///Panda3d<-->Bullet frame conversion
///      y ^                      z ^
///        |  ^                     |  ^
///        | / z  (bullet)          | / y  (panda3d)
///        |/                       |/
/// x <----|                        |----> x
///
// Conversion from Panda3D to Bullet
inline btVector3 LVecBase3_to_btVector3(const LVecBase3f &v)
{
	return btVector3(-(btScalar) v.get_x(), (btScalar) v.get_z(),
			(btScalar) v.get_y());
}

inline btVector3 LVecBase3_to_btVector3_verbatim(const LVecBase3f &v)
{
	return btVector3((btScalar) v.get_x(), (btScalar) v.get_z(),
			(btScalar) v.get_y());
}

inline btVector3 LVecBase3_to_btVector3_abs(const LVecBase3f &v)
{
	return btVector3((btScalar) abs(v.get_x()), (btScalar) abs(v.get_z()),
			(btScalar) abs(v.get_y()));
}

inline btQuaternion LQuaternion_to_btQuat(const LQuaternionf &q)
{
	return btQuaternion(-(btScalar) q.get_i(), (btScalar) q.get_k(),
			(btScalar) q.get_j(), (btScalar) q.get_r());
}

// Conversion from Bullet to Panda3D
inline LVecBase3f btVector3_to_LVecBase3(const btVector3 &v)
{
	return LVecBase3f(-(PN_stdfloat) v.getX(), (PN_stdfloat) v.getZ(),
			(PN_stdfloat) v.getY());
}

inline LVecBase3f btVector3_to_LVecBase3_verbatim(const btVector3 &v)
{
	return LVecBase3f((PN_stdfloat) v.getX(), (PN_stdfloat) v.getZ(),
			(PN_stdfloat) v.getY());
}

inline LVecBase3f btVector3_to_LVecBase3_abs(const btVector3 &v)
{
	return LVecBase3f((PN_stdfloat) abs(v.getX()), (PN_stdfloat) abs(v.getZ()),
			(PN_stdfloat) abs(v.getY()));
}

inline LQuaternionf btQuat_to_LQuaternion(const btQuaternion &q)
{
	return LQuaternionf((PN_stdfloat) q.getW(), -(PN_stdfloat) q.getX(),
			(PN_stdfloat) q.getZ(), (PN_stdfloat) q.getY());
}

//general
struct Physics;
btCollisionObject* getPhysicsObjectOfNode(const NodePath& target, const Physics& physics);
inline NodePath getNodeFromPhysicsObject(const btCollisionObject* body)
{
	NodePath target = NodePath::any_path(reinterpret_cast<PandaNode*>(body->getUserPointer()));
	PRINT_DEBUG((!target.is_empty() ? "" : "not ") << "getNodeFromPhysicObject(): " << target.get_name());
	return target;
}

class DynamicMotionState: public btMotionState
{
public:
	DynamicMotionState(const NodePath& object, CPT(TransformState)deltaTS,
			CPT(TransformState) deltaTSInv) : mGraphicsObject(object),
				mDeltaTS(deltaTS), mDeltaTSInv(deltaTSInv)
	{
	}

	virtual ~DynamicMotionState()
	{
	}

	virtual void getWorldTransform(btTransform &worldTrans) const
	{
		CPT(TransformState)ts = mGraphicsObject.get_transform()->compose(mDeltaTSInv);
		btVector3 p = LVecBase3_to_btVector3(ts->get_pos());
		btQuaternion q = LQuaternion_to_btQuat(ts->get_quat());
		worldTrans.setOrigin(p);
		worldTrans.setRotation(q);
	}

	virtual void setWorldTransform(const btTransform &worldTrans)
	{
		LPoint3 p = btVector3_to_LVecBase3(worldTrans.getOrigin());
		LQuaternion q = btQuat_to_LQuaternion(worldTrans.getRotation());
		CPT(TransformState)ts = TransformState::make_pos_quat_scale(p, q, 1.0);
		mGraphicsObject.set_transform(ts->compose(mDeltaTS));
	}

	NodePath getGraphicsObject()
	{
		return mGraphicsObject;
	}
	CPT(TransformState) getDeltaTS()
	{
		return mDeltaTS;
	}
	CPT(TransformState) getDeltaTSInv()
	{
		return mDeltaTSInv;
	}

protected:
	NodePath mGraphicsObject;
	CPT(TransformState) mDeltaTS;
	CPT(TransformState) mDeltaTSInv;
};

// make a (Dynamic)MotionState
DynamicMotionState* makeMotionState(const NodePath& target);

} // namespace bt3

#endif /* PHYSICSCOMMON_H_ */
