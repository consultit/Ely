/**
 * \file ghost.h
 *
 * \date 2017-03-08
 * \author consultit
 */
#ifndef GHOST_H_
#define GHOST_H_

#include "common.h"
#include "../bullet3/src/BulletCollision/BroadphaseCollision/btBroadphaseProxy.h"

namespace bt3
{
struct physics;
//ghost TODO: add a "motion state" and an "event handling" (see Ely)
void addGhostToNode(NodePath& target, Physics& physics, bool visible = true,
		btCollisionShape* sharedShape = nullptr,
		BroadphaseNativeTypes shapeType = INVALID_SHAPE_PROXYTYPE,
		Panda3dUpAxis upAxis = Panda3dUpAxis::Z_up, BitMask32 groupMask =
				BitMask32::all_on(), BitMask32 collideMask =
				BitMask32::all_on(), void* shapeData = nullptr,
		BroadphaseNativeTypes childShapeType = INVALID_SHAPE_PROXYTYPE);
bool removeGhostFromNode(NodePath& target, Physics& physics);

} //namespace bt3

#endif /* GHOST_H_ */
