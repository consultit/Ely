/**
 * \file convexDecomposition.cpp
 *
 * \date 2017-03-07
 * \author consultit
 */

#ifdef OPENCL_FOUND
#include "../clew/include/clew.h"
#endif
#include "convexDecomposition.h"

#include "physics.h"
#include "../bullet3/src/BulletCollision/Gimpact/btGImpactShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btCompoundShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btTriangleMesh.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btBvhTriangleMeshShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btCylinderShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btCapsuleShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btSphereShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btConvexHullShape.h"
#include "GTEngine/Include/Mathematics/GteMinimumVolumeBox3.h"
#include "GTEngine/Include/Mathematics/GteMinimumVolumeSphere3.h"
#include "GTEngine/Include/Mathematics/GteUIntegerAP32.h"
#include "GTEngine/Include/Mathematics/GteBSRational.h"
#include "GTEngine/Include/LowLevel/GteLogger.h"

namespace bt3
{

#ifdef ELY_DEBUG
class gteListener: public gte::Logger::Listener
{
public:
	gteListener():gte::Logger::Listener(gte::Logger::Listener::LISTEN_FOR_ALL)
	{
	}
	void Report(std::string const& message)
	{
		std::cout << message << std::endl;
	}
};
#endif //ELY_DEBUG

///convex decomposition
btCollisionShape* convexDecompose(NodePath& target, Physics& physics,
		BroadphaseNativeTypes childShapeType, const ConvexDecompositionParameters& parameters)
{
	//create the result shape
	btCompoundShape* compoundShape = new btCompoundShape();

	// set parameters
	ConvexDecompositionParameters params = parameters;
	ConvexDecompositionCallback myCallback;
	ConvexDecompositionLogger myLogger(params.m_fileNameLog);
	params.m_paramsVHACD.m_logger = &myLogger;
	params.m_paramsVHACD.m_callback = &myCallback;

	ostringstream msg;
#ifdef CL_VERSION_1_1
	msg << "+ OpenCL (ON)" << endl;
	OCLHelper oclHelper;
	if (params.m_paramsVHACD.m_oclAcceleration)
	{
		bool res = InitOCL(params.m_oclPlatformID, params.m_oclDeviceID,
				oclHelper, msg);
		if (!res)
		{
			params.m_paramsVHACD.m_oclAcceleration = false;
			msg << "+ InitOCL: Cannot initialize OpenCL! " << endl;
			myLogger.Log(msg.str().c_str());
		}
	}
#else //CL_VERSION_1_1
	msg << "+ OpenCL (OFF)" << endl;
#endif //CL_VERSION_1_1

#ifdef _OPENMP
	msg << "+ OpenMP (ON)" << endl;
#else
	msg << "+ OpenMP (OFF)" << endl;
#endif

	//debug
#ifdef ELY_DEBUG
	msg << "+ Parameters" << endl;
	msg << "\t resolution                                  "
			<< params.m_paramsVHACD.m_resolution << endl;
	msg << "\t max. concavity                              "
			<< params.m_paramsVHACD.m_concavity << endl;
	msg << "\t plane down-sampling                         "
			<< params.m_paramsVHACD.m_planeDownsampling << endl;
	msg << "\t convex-hull down-sampling                   "
			<< params.m_paramsVHACD.m_convexhullDownsampling << endl;
	msg << "\t alpha                                       "
			<< params.m_paramsVHACD.m_alpha << endl;
	msg << "\t beta                                        "
			<< params.m_paramsVHACD.m_beta << endl;
	msg << "\t maxhulls                                       "
			<< params.m_paramsVHACD.m_maxConvexHulls << endl;
	msg << "\t pca                                         "
			<< params.m_paramsVHACD.m_pca << endl;
	msg << "\t mode                                        "
			<< params.m_paramsVHACD.m_mode << endl;
	msg << "\t max. vertices per convex-hull               "
			<< params.m_paramsVHACD.m_maxNumVerticesPerCH << endl;
	msg << "\t min. volume to add vertices to convex-hulls "
			<< params.m_paramsVHACD.m_minVolumePerCH << endl;
	msg << "\t convex-hull approximation                   "
			<< params.m_paramsVHACD.m_convexhullApproximation << endl;
	msg << "\t OpenCL acceleration                         "
			<< params.m_paramsVHACD.m_oclAcceleration << endl;
	msg << "\t OpenCL platform ID                          "
			<< params.m_oclPlatformID << endl;
	msg << "\t OpenCL device ID                            "
			<< params.m_oclDeviceID << endl;
	msg << "\t log                                         "
			<< params.m_fileNameLog << endl;
	msg << "+ Load mesh" << endl;

	myLogger.Log(msg.str().c_str());
#endif
	PRINT_DEBUG(msg.str());

	// load mesh
	LVecBase3f modelDims;
	LVector3f modelDeltaCenter;
	getBoundingDimensions(target, modelDims, modelDeltaCenter,
			Panda3dUpAxis::NO_up);
	//save and reset target's pos and hpr (not scale)
	LPoint3f pos = target.get_pos();
	LVecBase3f hpr = target.get_hpr();
	target.set_pos_hpr(modelDeltaCenter, LVecBase3f::zero());
	//get geometry
	GeometryProcessor proc;
	proc.load(target, NodePath());
	const float* points = proc.getVerts();
	const int* triangles = proc.getTris();
	//restore target's pos and hpr (not scale)
	target.set_pos_hpr(pos, hpr);

	// run V-HACD
	VHACD::IVHACD* interfaceVHACD = VHACD::CreateVHACD();

#ifdef CL_VERSION_1_1
	if (params.m_paramsVHACD.m_oclAcceleration)
	{
		bool res = interfaceVHACD->OCLInit(oclHelper.GetDevice(), &myLogger);
		if (!res)
		{
			params.m_paramsVHACD.m_oclAcceleration = false;
		}
	}
#endif //CL_VERSION_1_1

	//convex decompose!
	bool res = interfaceVHACD->Compute(points,
			(unsigned int) proc.getVertCount(), (const uint32_t *)triangles,
			(unsigned int) proc.getTriCount(), params.m_paramsVHACD);

	if (res)
	{
		// save output
		unsigned int nConvexHulls = interfaceVHACD->GetNConvexHulls();

#ifdef ELY_DEBUG
		msg.str("");
		msg << "+ Generate output: " << nConvexHulls << " convex-hulls "
				<< endl;
		myLogger.Log(msg.str().c_str());
#endif

		VHACD::IVHACD::ConvexHull ch;
		btCollisionShape* childShape;

		switch (childShapeType)
		{
		case GIMPACT_SHAPE_PROXYTYPE:
		case TRIANGLE_MESH_SHAPE_PROXYTYPE:
			for (unsigned int p = 0; p < nConvexHulls; ++p)
			{
				interfaceVHACD->GetConvexHull(p, ch);
				//create the underlying tri mesh from convex hull
				btTriangleMesh* mesh = new btTriangleMesh(true, true);
				for (unsigned int i = 0; i < ch.m_nTriangles; i++)
				{
					const uint32_t * tris = ch.m_triangles;
					const double* verts = ch.m_points;
					//v0
					int v0I = tris[i * 3];
					LPoint3f v0((float) verts[v0I * 3],
							(float) verts[v0I * 3 + 1],
							(float) verts[v0I * 3 + 2]);
					//v1
					int v1I = tris[i * 3 + 1];
					LPoint3f v1((float) verts[v1I * 3],
							(float) verts[v1I * 3 + 1],
							(float) verts[v1I * 3 + 2]);
					//v2
					int v2I = tris[i * 3 + 2];
					LPoint3f v2((float) verts[v2I * 3],
							(float) verts[v2I * 3 + 1],
							(float) verts[v2I * 3 + 2]);
					//add triangle to mesh
					mesh->addTriangle(LVecBase3_to_btVector3(v0),
							LVecBase3_to_btVector3(v1),
							LVecBase3_to_btVector3(v2), true);
				}
				// create the child shape
				if (childShapeType == GIMPACT_SHAPE_PROXYTYPE)
				{
					childShape = new btGImpactMeshShape(mesh);
					static_cast<btGImpactMeshShape*>(childShape)->updateBound();
				}
				else if (childShapeType == TRIANGLE_MESH_SHAPE_PROXYTYPE)
				{
					childShape = new btBvhTriangleMeshShape(mesh, true, true);
				}
				//store the underlying mesh
				physics.indexVertexArrays[childShape] = mesh;
				//add to compound
				ASSERT_TRUE(childShape && (childShape->getShapeType() != INVALID_SHAPE_PROXYTYPE))

				compoundShape->addChildShape(btTransform::getIdentity(),
						childShape);
				//
				physics.collisionShapes[childShape] = 1;
			}
			break;
		case BOX_SHAPE_PROXYTYPE:
		case CYLINDER_SHAPE_PROXYTYPE:
		case CAPSULE_SHAPE_PROXYTYPE:
		case SPHERE_SHAPE_PROXYTYPE:
			for (unsigned int p = 0; p < nConvexHulls; ++p)
			{
				interfaceVHACD->GetConvexHull(p, ch);
				// use GTE computational geometry tools
#ifdef ELY_DEBUG
				gteListener listener;
				gte::Logger::Subscribe(&listener);
#endif //ELY_DEBUG
				// get functor's parameters
				int numPoints = (int) ch.m_nPoints;
				gte::Vector3<float>* points = new gte::Vector3<float>[numPoints];
				for (int i = 0; i < numPoints; i++)
				{
					points[i][0] = (float) ch.m_points[3 * i];
					points[i][1] = (float) ch.m_points[3 * i + 1];
					points[i][2] = (float) ch.m_points[3 * i + 2];
				}
				// compute
				btTransform childTrans;
				childTrans.setIdentity();
				if (childShapeType == BOX_SHAPE_PROXYTYPE ||
						childShapeType == CYLINDER_SHAPE_PROXYTYPE ||
						childShapeType == CAPSULE_SHAPE_PROXYTYPE)
				{
					// create gte::MinimumVolumeBox3 object
					gte::MinimumVolumeBox3<float,
							gte::BSRational<gte::UIntegerAP32>> minVolBox;
					///gte::MinimumVolumeBox3<float, float> minVolBox;
					// Compute a minimum-volume oriented box containing the specified points.
					gte::OrientedBox3<float> box = minVolBox(numPoints, points);
					// get parameters of the box
					LPoint3f center(box.center[0], box.center[1],
							box.center[2]);
					LVecBase3f extent(box.extent[0], box.extent[1],
							box.extent[2]);
					LVector3f axis[3] =
					{ LVector3f(box.axis[0][0], box.axis[0][1], box.axis[0][2]),
							LVector3f(box.axis[1][0], box.axis[1][1],
									box.axis[1][2]), LVector3f(box.axis[2][0],
									box.axis[2][1], box.axis[2][2]) };
					// create the child shape
					if (childShapeType == BOX_SHAPE_PROXYTYPE)
					{
						childShape = new btBoxShape(
								bt3::LVecBase3_to_btVector3_abs(extent));
					}
					else if (childShapeType == CYLINDER_SHAPE_PROXYTYPE)
					{
						// get max dim axis (0==X,1==Y,2==Z)
						int maxIdx = 0;
						for (int i = 0; i < 2; i++)
						{
							if (extent[maxIdx] < extent[i + 1])
							{
								maxIdx = i + 1;
							}
						}
						// create cylinder along max dim axis
						switch (maxIdx)
						{
						case 0:
							childShape = new btCylinderShapeX(
									bt3::LVecBase3_to_btVector3_abs(extent));
							break;
						case 1:
							childShape = new btCylinderShapeZ(
									bt3::LVecBase3_to_btVector3_abs(extent));
							break;
						case 2:
							childShape = new btCylinderShape(
									bt3::LVecBase3_to_btVector3_abs(extent));
							break;
						default:
							break;
						}
					}
					else if (childShapeType == CAPSULE_SHAPE_PROXYTYPE)
					{
						// get max dim axis (0==X,1==Y,2==Z)
						int maxIdx = 0;
						for (int i = 0; i < 2; i++)
						{
							if (extent[maxIdx] < extent[i + 1])
							{
								maxIdx = i + 1;
							}
						}
						// create capsule along max dim axis
						switch (maxIdx)
						{
						case 0:
						{
							float radius = sqrt(
									pow(extent.get_y(), 2)
											+ pow(extent.get_z(), 2));
							float height = 2.0 * (extent.get_x() - radius);
							childShape = new btCapsuleShapeX(radius, height);
						}
							break;
						case 1:
						{
							float radius = sqrt(
									pow(extent.get_x(), 2)
											+ pow(extent.get_z(), 2));
							float height = 2.0 * (extent.get_y() - radius);
							childShape = new btCapsuleShapeZ(radius, height);
						}
							break;
						case 2:
						{
							float radius = sqrt(
									pow(extent.get_x(), 2)
											+ pow(extent.get_y(), 2));
							float height = 2.0 * (extent.get_z() - radius);
							childShape = new btCapsuleShape(radius, height);
						}
							break;
						default:
							break;
						}
					}
					// compute the child shape's transform in Bullet space
					// get rotation matrix and the relative quaternion
					LMatrix3f rotMat;
					rotMat.set_row(0, axis[0]);
					rotMat.set_row(1, axis[1]);
					rotMat.set_row(2, axis[2]);
					LRotationf rot(rotMat);
					childTrans.setOrigin(bt3::LVecBase3_to_btVector3(center));
					childTrans.setRotation(bt3::LQuaternion_to_btQuat(rot));
				}
				else if (childShapeType == SPHERE_SHAPE_PROXYTYPE)
				{
					// create gte::MinimumVolumeSphere3 object
					gte::MinimumVolumeSphere3<float,
							gte::BSRational<gte::UIntegerAP32>> minVolSphere;
					///gte::MinimumVolumeSphere3<float, float> minVolSphere;
					// Compute the minimum volume sphere containing the input set of points.
					gte::Sphere3<float> sphere;
					minVolSphere(numPoints, points, sphere);
					// get parameters of the sphere
					LPoint3f center(sphere.center[0], sphere.center[1], sphere.center[2]);
					float radius = sphere.radius;
					// create the child shape
					childShape = new btSphereShape(radius);
					// compute the child shape's transform in Bullet space
					childTrans.setOrigin(bt3::LVecBase3_to_btVector3(center));
				}
				// cleanup allocated storage
				delete[] points;
#ifdef ELY_DEBUG
				gte::Logger::Unsubscribe(&listener);
#endif //ELY_DEBUG

				//add to compound
				ASSERT_TRUE(childShape && (childShape->getShapeType() != INVALID_SHAPE_PROXYTYPE))

				// and then add it
				compoundShape->addChildShape(childTrans, childShape);
				//
				physics.collisionShapes[childShape] = 1;
			}
			break;
		case CONVEX_HULL_SHAPE_PROXYTYPE:
		default:
			for (unsigned int p = 0; p < nConvexHulls; ++p)
			{
				interfaceVHACD->GetConvexHull(p, ch);
				// create the child shape
				childShape = new btConvexHullShape();
				//add convex hull points
				for (unsigned int i = 0; i < ch.m_nPoints; i++)
				{
					float x = (float) ch.m_points[i * 3];
					float y = (float) ch.m_points[i * 3 + 1];
					float z = (float) ch.m_points[i * 3 + 2];
					static_cast<btConvexHullShape*>(childShape)->addPoint(
							LVecBase3_to_btVector3(LPoint3f(x, y, z)));
				}
				//add to compound
				ASSERT_TRUE(childShape && (childShape->getShapeType() != INVALID_SHAPE_PROXYTYPE))

				compoundShape->addChildShape(btTransform::getIdentity(),
						childShape);
				//
				physics.collisionShapes[childShape] = 1;
			}
			break;
		}
	}
#ifdef ELY_DEBUG
	else
	{
		myLogger.Log("Decomposition cancelled by user!\n");
	}
#endif

#ifdef CL_VERSION_1_1
	if (params.m_paramsVHACD.m_oclAcceleration)
	{
		bool res = interfaceVHACD->OCLRelease(&myLogger);
		if (!res)
		{
			assert(-1);
		}
	}
#endif //CL_VERSION_1_1

	interfaceVHACD->Clean();
	interfaceVHACD->Release();
#ifdef _CRTDBG_MAP_ALLOC
	_CrtDumpMemoryLeaks();
#endif // _CRTDBG_MAP_ALLOC
	//
	return compoundShape;
}

#ifdef CL_VERSION_1_1
bool InitOCL(const unsigned int oclPlatformID, const unsigned int oclDeviceID, OCLHelper& oclHelper, ostringstream& msg)
{
	bool clpresent = 0 == clewInit();
	if( !clpresent ) {
	    return false;
	}

	bool res = true;
	vector<string> info;
	res = oclHelper.GetPlatformsInfo(info, "\t\t");
	if (!res)
	{
		msg << "+ InitOCL: Cannot retrieve OpenCL platform info(s)! " << endl;
		return res;
	}

	const size_t numPlatforms = info.size();
	msg << "\t Number of OpenCL platforms: " << numPlatforms << endl;
	for (size_t i = 0; i < numPlatforms; ++i)
	{
		msg << "\t OpenCL platform [" << i << "]" << endl;
		msg << info[i];
	}
	msg << "\t Using OpenCL platform [" << oclPlatformID << "]" << endl;
	res = oclHelper.InitPlatform(oclPlatformID);
	if (!res)
	{
		msg << "+ InitOCL: Cannot initialize OpenCL platform! " << endl;
		return res;
	}

	info.clear();
	res = oclHelper.GetDevicesInfo(info, "\t\t");
	if (!res)
		return res;

	const size_t numDevices = info.size();
	msg << "\t Number of OpenCL devices: " << numDevices << endl;
	for (size_t i = 0; i < numDevices; ++i)
	{
		msg << "\t OpenCL device [" << i << "]" << endl;
		msg << info[i];
	}
	msg << "\t Using OpenCL device [" << oclDeviceID << "]" << endl;
	res = oclHelper.InitDevice(oclDeviceID);
	return res;
}
#endif // CL_VERSION_1_1

} //namespace bt3
