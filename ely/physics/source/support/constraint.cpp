/**
 * \file constraint.cpp
 *
 * \date 2017-03-08
 * \author consultit
 */

#include "constraint.h"
#include "physics.h"
#include "rigidBody.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btPoint2PointConstraint.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btHingeConstraint.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btHinge2Constraint.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btGearConstraint.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btConeTwistConstraint.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btGeneric6DofConstraint.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btGeneric6DofSpringConstraint.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btFixedConstraint.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btSliderConstraint.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btUniversalConstraint.h"

namespace
{

CPT(TransformState) getLocalTransform(const NodePath& target,
		const LPoint3f& pivot, const LVecBase3f& rotation,
		CPT(TransformState)deltaTS)
{
	//we've 3 frames: Fw,Fo,Fc. Fo and Fc are given wrt Fw; so:
	//Fo = Mo * Fw  and Fc = Mc * Fw
	//therefore, because Fw = MoINV * Fo and Fw = McINV * Fc, we have:
	//1) Fc = Mc * MoINV * Fo
	//2) Fo = Mo * McINV *Fc
	//we need Fc wrt Fo, ie the 1) formula
	CPT(TransformState) MoINV = target.get_transform()->get_inverse();
	CPT(TransformState) Mc =
			TransformState::make_pos_hpr(pivot, rotation);
	CPT(TransformState) McMoINV = MoINV->compose(Mc);
	///correct transform for rbA
	CPT(TransformState) transform = deltaTS->compose(McMoINV);
	return transform;
}

void activateBodies(btRigidBody* rbA, btRigidBody* rbB = nullptr)
{
	if(!rbA->isActive())
	{
		rbA->activate();
	}
	if (rbB)
	{
		if(!rbB->isActive())
		{
			rbB->activate();
		}
	}
}

} //namespace anonymous

namespace bt3
{

btTypedConstraint* addConstraintToNodes(const NodePath& targetA, const NodePath& targetB,
		const Physics& physics, const ConstraintData& constraintData)
{
	btTypedConstraint* constraint = nullptr;

	// check physic object A
	btCollisionObject *rbACObj = getPhysicsObjectOfNode(targetA, physics);
	btRigidBody *rbA = nullptr;
	if (rbACObj)
	{
		rbA = btRigidBody::upcast(rbACObj);
	}
	// rigid body A is mandatory for every constraint
	if (targetA.is_empty() || !rbA)
	{
		return constraint;
	}

	// check physic object B
	btCollisionObject *rbBCObj = getPhysicsObjectOfNode(targetB, physics);
	btRigidBody *rbB = nullptr;
	if (rbBCObj)
	{
		rbB = btRigidBody::upcast(rbBCObj);
	}

	// switch constraint type
	switch (constraintData.type)
	{
	case ConstraintType::POINT2POINT:
	{
		/// correct transform for rbA
		CPT(TransformState)deltaTS =
		static_cast<DynamicMotionState*>(rbA->getMotionState())->getDeltaTS();
		LPoint3f pivotA;
		if (constraintData.local)
		{
			pivotA = constraintData.pivots.AorGlobal * deltaTS->get_mat();
		}
		else
		{
			// global
			CPT(TransformState) transformRel =
					targetA.get_parent().get_transform(targetA);
			// targetA.get_relative_point(targetA.get_parent(),constraintData.pivot)
			pivotA = constraintData.pivots.AorGlobal *
					transformRel->compose(deltaTS)->get_mat();
		}
		//
		btVector3 pivotInA = LVecBase3_to_btVector3(pivotA);
		if (!targetB.is_empty() && rbB)
		{
			/// correct transform for rbB
			CPT(TransformState) deltaTS =
			static_cast<DynamicMotionState*>(rbB->getMotionState())->getDeltaTS();
			LPoint3f pivotB;
			if (constraintData.local)
			{
				pivotB = constraintData.pivots.B * deltaTS->get_mat();
			}
			else
			{
				// global
				CPT(TransformState) transformRel =
						targetB.get_parent().get_transform(targetB);
				// targetB.get_relative_point(targetB.get_parent(),constraintData.pivot)
				pivotB = constraintData.pivots.AorGlobal *
						transformRel->compose(deltaTS)->get_mat();
			}
			btVector3 pivotInB = LVecBase3_to_btVector3(pivotB);
			//create constraint A <--> B
			constraint = new btPoint2PointConstraint(*rbA, *rbB, pivotInA,
					pivotInB);
			PRINT_DEBUG("POINT2POINT addConstraintToNodes(): "
					<< targetA.get_name() << " - " << targetB.get_name());
			activateBodies(rbA, rbB);
		}
		else
		{
			//create constraint A <--> WORLD
			constraint = new btPoint2PointConstraint(*rbA, pivotInA);
			PRINT_DEBUG("POINT2POINT addConstraintToNodes(): "
					<< targetA.get_name() << " - WORLD");
			activateBodies(rbA);
		}
	}
	break;
	case ConstraintType::HINGE:
	case ConstraintType::HINGE_ACCUMULATED:
	case ConstraintType::GEAR:
	{
		/// correct transform for rbA
		CPT(TransformState)deltaTS =
		static_cast<DynamicMotionState*>(rbA->getMotionState())->getDeltaTS();
		LPoint3f pivotA;
		LVector3f axisA;
		if (constraintData.local)
		{
			pivotA = constraintData.pivots.AorGlobal * deltaTS->get_mat();
			axisA = constraintData.axes.AorGlobal * deltaTS->get_mat();
		}
		else
		{
			// global
			CPT(TransformState) transformRel =
					targetA.get_parent().get_transform(targetA);
			// targetA.get_relative_point(targetA.get_parent(),constraintData.pivot)
			pivotA = constraintData.pivots.AorGlobal *
					transformRel->compose(deltaTS)->get_mat();
			// targetA.get_relative_vector(targetA.get_parent(),constraintData.pivot)
			axisA = constraintData.axes.AorGlobal *
					transformRel->compose(deltaTS)->get_mat();
		}
		//
		btVector3 pivotInA = LVecBase3_to_btVector3(pivotA);
		btVector3 axisInA = LVecBase3_to_btVector3(axisA);
		axisInA.normalize();
		if (!targetB.is_empty() && rbB)
		{
			/// correct transform for rbB
			CPT(TransformState) deltaTS =
			static_cast<DynamicMotionState*>(rbB->getMotionState())->getDeltaTS();
			LPoint3f pivotB;
			LVector3f axisB;
			if (constraintData.local)
			{
				pivotB = constraintData.pivots.B * deltaTS->get_mat();
				axisB = constraintData.axes.B * deltaTS->get_mat();
			}
			else
			{
				// global
				CPT(TransformState) transformRel =
						targetB.get_parent().get_transform(targetB);
				// targetB.get_relative_point(targetB.get_parent(),constraintData.pivot)
				pivotB = constraintData.pivots.AorGlobal *
						transformRel->compose(deltaTS)->get_mat();
				// targetB.get_relative_vector(targetB.get_parent(),constraintData.pivot)
				axisB = constraintData.axes.AorGlobal *
						transformRel->compose(deltaTS)->get_mat();
			}
			//
			btVector3 pivotInB = LVecBase3_to_btVector3(pivotB);
			btVector3 axisInB = LVecBase3_to_btVector3(axisB);
			axisInB.normalize();
			//create constraint A <--> B
			switch (constraintData.type)
			{
				case ConstraintType::HINGE:
				{
					constraint = new btHingeConstraint(*rbA, *rbB, pivotInA, pivotInB,
							axisInA, axisInB, constraintData.useReferenceFrameA);
					PRINT_DEBUG("HINGE addConstraintToNodes(): " << targetA.get_name()
							<< " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				break;
				case ConstraintType::HINGE_ACCUMULATED:
				{
					constraint = new btHingeAccumulatedAngleConstraint(*rbA, *rbB,
							pivotInA, pivotInB, axisInA, axisInB,
							constraintData.useReferenceFrameA);
					PRINT_DEBUG("HINGE_ACCUMULATED addConstraintToNodes(): " <<
							targetA.get_name() << " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				case ConstraintType::GEAR:
				{
					constraint = new btGearConstraint(*rbA, *rbB, axisInA, axisInB,
							constraintData.ratio);
					PRINT_DEBUG("GEAR addConstraintToNodes(): " <<
							targetA.get_name() << " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				break;
				default:
				break;
			}
		}
		else
		{
			//create constraint A <--> WORLD
			switch (constraintData.type)
			{
				case ConstraintType::HINGE:
				{
					constraint = new btHingeConstraint(*rbA, pivotInA, axisInA,
							constraintData.useReferenceFrameA);
					PRINT_DEBUG("HINGE addConstraintToNodes(): " << targetA.get_name()
							<< " - WORLD");
					activateBodies(rbA);				}
				break;
				case ConstraintType::HINGE_ACCUMULATED:
				{
					constraint = new btHingeAccumulatedAngleConstraint(*rbA, pivotInA, axisInA,
							constraintData.useReferenceFrameA);
					PRINT_DEBUG("HINGE_ACCUMULATED addConstraintToNodes(): " <<
							targetA.get_name() << " - WORLD");
					activateBodies(rbA);
				}
				break;
				case ConstraintType::GEAR:
				{
					PRINT_DEBUG("ERROR - GEAR addConstraintToNodes(): " << targetA.get_name()
							<< " - WORLD");
				}
				break;
				default:
				break;
			}
		}
	}
	break;
	case ConstraintType::CONETWIST:
	case ConstraintType::DOF6:
	case ConstraintType::DOF6_SPRING:
	case ConstraintType::DOF6_SPRING_2:
	case ConstraintType::FIXED:
	case ConstraintType::SLIDER:
	{
		/// correct transform for rbA
		CPT(TransformState)deltaTS =
		static_cast<DynamicMotionState*>(rbA->getMotionState())->getDeltaTS();
		btTransform frameInA = btTransform::getIdentity();
		if (constraintData.local)
		{
			CPT(TransformState) localTransform =
			TransformState::make_pos_hpr(constraintData.pivots.AorGlobal,
					constraintData.rotations.AorGlobal);
			CPT(TransformState) transform = deltaTS->compose(localTransform);
			frameInA.setOrigin(LVecBase3_to_btVector3(transform->get_pos()));
			frameInA.setRotation(LQuaternion_to_btQuat(transform->get_quat()));
		}
		else
		{
			// global
			CPT(TransformState) transform = getLocalTransform(targetA,
					constraintData.pivots.AorGlobal, constraintData.rotations.AorGlobal,
					deltaTS);
			frameInA.setOrigin(LVecBase3_to_btVector3(transform->get_pos()));
			frameInA.setRotation(LQuaternion_to_btQuat(transform->get_quat()));
		}
		if (!targetB.is_empty() && rbB)
		{
			/// correct transform for rbB
			CPT(TransformState) deltaTS =
			static_cast<DynamicMotionState*>(rbB->getMotionState())->getDeltaTS();
			btTransform frameInB = btTransform::getIdentity();
			if (constraintData.local)
			{
				CPT(TransformState) localTransform =
				TransformState::make_pos_hpr(constraintData.pivots.B,
						constraintData.rotations.B);
				CPT(TransformState) transform = deltaTS->compose(localTransform);
				frameInB.setOrigin(LVecBase3_to_btVector3(transform->get_pos()));
				frameInB.setRotation(LQuaternion_to_btQuat(transform->get_quat()));
			}
			else
			{
				//global
				CPT(TransformState) transform = getLocalTransform(targetB,
						constraintData.pivots.AorGlobal, constraintData.rotations.AorGlobal,
						deltaTS);
				frameInB.setOrigin(LVecBase3_to_btVector3(transform->get_pos()));
				frameInB.setRotation(LQuaternion_to_btQuat(transform->get_quat()));
			}
			//create constraint A <--> B
			switch (constraintData.type)
			{
				case ConstraintType::CONETWIST:
				{
					constraint = new btConeTwistConstraint(*rbA, *rbB, frameInA, frameInB);
					PRINT_DEBUG("CONETWIST addConstraintToNodes(): " << targetA.get_name()
							<< " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				break;
				case ConstraintType::DOF6:
				{
					constraint = new btGeneric6DofConstraint(*rbA, *rbB, frameInA, frameInB,
							constraintData.useReferenceFrameA);
					PRINT_DEBUG("DOF6 addConstraintToNodes(): " << targetA.get_name()
							<< " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				break;
				case ConstraintType::DOF6_SPRING:
				{
					constraint = new btGeneric6DofSpringConstraint(*rbA, *rbB, frameInA, frameInB,
							constraintData.useReferenceFrameA);
					PRINT_DEBUG("DOF6_SPRING addConstraintToNodes(): " << targetA.get_name()
							<< " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				break;
				case ConstraintType::DOF6_SPRING_2:
				{
					constraint = new btGeneric6DofSpring2Constraint(*rbA, *rbB, frameInA, frameInB,
							constraintData.rotOrder);
					PRINT_DEBUG("DOF6_SPRING_2 addConstraintToNodes(): " << targetA.get_name()
							<< " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				break;
				case ConstraintType::FIXED:
				{
					constraint = new btFixedConstraint(*rbA, *rbB, frameInA, frameInB);
					PRINT_DEBUG("FIXED addConstraintToNodes(): " << targetA.get_name()
							<< " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				break;
				case ConstraintType::SLIDER:
				{
					constraint = new btSliderConstraint(*rbA, *rbB, frameInA, frameInB,
							constraintData.useReferenceFrameA);
					PRINT_DEBUG("SLIDER addConstraintToNodes(): " << targetA.get_name()
							<< " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				break;
				default:
				break;
			}
		}
		else
		{
			//create constraint A <--> WORLD
			switch (constraintData.type)
			{
				case ConstraintType::CONETWIST:
				{
					constraint = new btConeTwistConstraint(*rbA, frameInA);
					PRINT_DEBUG("CONETWIST addConstraintToNodes(): " << targetA.get_name()
							<< " - WORLD");
					activateBodies(rbA);				}
				break;
				case ConstraintType::DOF6:
				{
					constraint = new btGeneric6DofConstraint(*rbA, frameInA,
							constraintData.useReferenceFrameA);
					PRINT_DEBUG("DOF6 addConstraintToNodes(): " << targetA.get_name()
							<< " - WORLD");
					activateBodies(rbA);				}
				break;
				case ConstraintType::DOF6_SPRING:
				{
					constraint = new btGeneric6DofSpringConstraint(*rbA, frameInA,
							constraintData.useReferenceFrameA);
					PRINT_DEBUG("DOF6_SPRING addConstraintToNodes(): " << targetA.get_name()
							<< " - WORLD");
					activateBodies(rbA);				}
				break;
				case ConstraintType::DOF6_SPRING_2:
				{
					constraint = new btGeneric6DofSpring2Constraint(*rbA, frameInA,
							constraintData.rotOrder);
					PRINT_DEBUG("DOF6_SPRING_2 addConstraintToNodes(): " << targetA.get_name()
							<< " - WORLD");
					activateBodies(rbA);				}
				break;
				case ConstraintType::FIXED:
				{
					PRINT_DEBUG("ERROR - FIXED addConstraintToNodes(): " << targetA.get_name()
							<< " - WORLD");
				}
				break;
				case ConstraintType::SLIDER:
				{
					constraint = new btSliderConstraint(*rbA, frameInA,
							constraintData.useReferenceFrameA);
					PRINT_DEBUG("SLIDER addConstraintToNodes(): " << targetA.get_name()
							<< " - WORLD");
					activateBodies(rbA);				}
				break;
				default:
				break;
			}
		}
	}
	break;
	case ConstraintType::HINGE_2:
	case ConstraintType::UNIVERSAL:
	{
		LVector3f axis1Data = constraintData.axis1;
		LVector3f axis2Data = constraintData.axis2;
		//check axes' lengths
		if ((axis1Data.length_squared() == 0.0) ||
				(axis2Data.length_squared() == 0.0))
		{
			axis1Data = LVector3f::unit_x();
			axis2Data = LVector3f::unit_z();
		}
		//make sure axes are orthogonal
		{
			float len2 = axis2Data.length();
			LVector3f cross12 = axis1Data.cross(axis2Data);
			cross12.normalize();
			LVector3f cross2 = cross12.cross(axis1Data);
			cross2.normalize();
			axis2Data = len2 * cross2;
		}
		btVector3 anchor = LVecBase3_to_btVector3(constraintData.anchor);
		btVector3 axis1 = LVecBase3_to_btVector3(axis1Data);
		axis1.normalize();
		btVector3 axis2 = LVecBase3_to_btVector3(axis2Data);
		axis2.normalize();
		//
		if (!targetB.is_empty() && rbB)
		{
			//create constraint A <--> B
			switch (constraintData.type)
			{
				case ConstraintType::HINGE_2:
				{
					constraint = new btHinge2Constraint(*rbA, *rbB, anchor, axis1, axis2);
					PRINT_DEBUG("HINGE_2 addConstraintToNodes(): " << targetA.get_name()
							<< " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				break;
				case ConstraintType::UNIVERSAL:
				{
					constraint = new btUniversalConstraint(*rbA, *rbB, anchor, axis1, axis2);
					PRINT_DEBUG("HINGE_2 addConstraintToNodes(): " << targetA.get_name()
							<< " - " << targetB.get_name());
					activateBodies(rbA, rbB);
				}
				break;
				default:
				break;
			}
		}
		else
		{
			//create constraint A <--> WORLD
			switch (constraintData.type)
			{
				case ConstraintType::HINGE_2:
				{
					PRINT_DEBUG("ERROR - HINGE_2 addConstraintToNodes(): " << targetA.get_name()
								<< " - WORLD");
				}
				break;
				case ConstraintType::UNIVERSAL:
				{
					PRINT_DEBUG("ERROR - UNIVERSAL addConstraintToNodes(): " << targetA.get_name()
								<< " - WORLD");

				}
				break;
				default:
				break;
			}
		}
	}
	break;
	default:
	break;
	}
	//
	if (constraint)
	{
		physics.dynamicsWorld->addConstraint(constraint,
				constraintData.disableCollisionsBetweenLinkedBodies);
	}
	return constraint;
}

bool removeConstraintFromNodes(btTypedConstraint* constraint, const Physics& physics)
{
	bool result = false;
	//remove the constraint from the dynamics world and delete them
	string rbAName, rbBName;
	for (int i = 0; i < physics.dynamicsWorld->getNumConstraints(); i++)
	{
		if (constraint == physics.dynamicsWorld->getConstraint(i))
		{
			// get the nodes
			NodePath rbANP = getNodeFromPhysicsObject(
					&constraint->getRigidBodyA());
			NodePath rbBNP = getNodeFromPhysicsObject(
					&constraint->getRigidBodyB());
			rbAName = (!rbANP.is_empty() ? rbANP.get_name() : "WORLD");
			rbBName = (!rbBNP.is_empty() ? rbBNP.get_name() : "WORLD");
			physics.dynamicsWorld->removeConstraint(constraint);
			delete constraint;
			result = true;
			break;
		}
	}
#ifdef ELY_DEBUG
	if (result)
	{
		PRINT_DEBUG(
				"removeConstraintFromNodes(): " << rbAName << " and " << rbBName);

	}
	else
	{
		PRINT_DEBUG("not removeConstraintFromNodes()");
	}
#endif
	//
	return result;
}

bool removeConstraintsFromNode(const NodePath& target, const Physics& physics)
{
	bool result = true;
	if (target.is_empty())
	{
		result = false;
	}

	// rigid body is mandatory
	btRigidBody* body = btRigidBody::upcast(
			getPhysicsObjectOfNode(target, physics));
	if (!body)
	{
		result = false;
	}
	// remove all constraints from the body
	vector<btTypedConstraint*> constraints = getNodeConstraints(target,
			physics);
	if (!constraints.empty() && !body->isActive())
	{
		body->activate();
	}
	for (auto constraint : constraints)
	{
		physics.dynamicsWorld->removeConstraint(constraint);
	}
	PRINT_DEBUG(
			(result ? "" : "not ") << "removeConstraintsFromNode(): " << target.get_name());
	//
	return result;
}

}
