/**
 * \file vehicle.cpp
 *
 * \date 2017-05-27
 * \author consultit
 */

#include "vehicle.h"
#include "physics.h"

namespace
{
void setCoordinateSystemAndComputeData(btRaycastVehicle* vehicle,
		const NodePath& chassisNP, const NodePath (&wheelNP)[2],
		const bt3::VehicleData& vhData, int numWheels,
		/*out*/float (&whRadius)[2],/*out*/btVector3& whDirectionCS0,
		/*out*/btVector3& whAxleCS, /*out*/vector<btVector3>& connPointCS0)
{
	// modelCenter = <origin of coordinate system> - deltaCenter
	LVecBase3f dims;
	LVector3f deltaCenter;
	// compute dims of chassisNP
	getBoundingDimensions(chassisNP, dims, deltaCenter, vhData.cs);
	btVector3 chDims = bt3::LVecBase3_to_btVector3_verbatim(dims);
//	btVector3 chDeltaCenter = bt3::LVecBase3_to_btVector3(deltaCenter);fixme
	// compute dims of wheelNP
	btVector3 whDims[2];
//	btVector3 whDeltaCenter[2];
	whRadius[0] = getBoundingDimensions(wheelNP[0], dims, deltaCenter, vhData.cs);
	whDims[0] = bt3::LVecBase3_to_btVector3_verbatim(dims);
//	whDeltaCenter[0] = bt3::LVecBase3_to_btVector3(deltaCenter);fixme
	whRadius[1] = getBoundingDimensions(wheelNP[1], dims, deltaCenter, vhData.cs);
	whDims[1] = bt3::LVecBase3_to_btVector3_verbatim(dims);
//	whDeltaCenter[1] = bt3::LVecBase3_to_btVector3(deltaCenter);fixme
	//
	int rightIdx, upIdx, forwardIdx;
	float connHeight[2];
	connHeight[0] = vhData.connHeightF[0] * whRadius[0];
	connHeight[1] = vhData.connHeightF[1] * whRadius[1];
	// 4 wheels: FL,FR,RR,RL:
	//  FL----|----FR
	//        |
	//        |
	//  RL----|----RR
	// 2 wheels: F,R:
	//  |F
	//  |
	//  |
	//  |R
	connPointCS0.resize(numWheels);
	float driveShaftWidth[2], driveShaftDepth[2];
	switch (vhData.cs)
	{
	case bt3::Panda3dUpAxis::X_up:	// -> Bullet3 X up
	{
		// Bullet3 frame
		//    |y
		// z--o
		rightIdx = bt3::p3dBt3AxisSwitch(1);
		upIdx = bt3::p3dBt3AxisSwitch(0);
		forwardIdx = bt3::p3dBt3AxisSwitch(2);
		whDirectionCS0 = btVector3(-1, 0, 0);
		whAxleCS = btVector3(0, 0, -1);
		driveShaftWidth[0] = chDims.getZ() / 2.0
				- (vhData.connWidthF[0] * whDims[0].getZ() / 2.0);
		driveShaftWidth[1] = chDims.getZ() / 2.0
				- (vhData.connWidthF[1] * whDims[1].getZ() / 2.0);
		driveShaftDepth[0] = chDims.getY() / 2.0
				- (vhData.connDepthF[0] * whRadius[0]);
		driveShaftDepth[1] = chDims.getY() / 2.0
				- (vhData.connDepthF[1] * whRadius[1]);
	}
		break;
	case bt3::Panda3dUpAxis::Y_up:		// -> Bullet3 Z up
	{
		// Bullet3 frame
		//    |x
		// y--o
		rightIdx = bt3::p3dBt3AxisSwitch(2);
		upIdx = bt3::p3dBt3AxisSwitch(1);
		forwardIdx = bt3::p3dBt3AxisSwitch(0);
		whDirectionCS0 = btVector3(0, 0, -1);
		whAxleCS = btVector3(0, -1, 0);
		driveShaftWidth[0] = chDims.getY() / 2.0
				- (vhData.connWidthF[0] * whDims[0].getY() / 2.0);
		driveShaftWidth[1] = chDims.getY() / 2.0
				- (vhData.connWidthF[1] * whDims[1].getY() / 2.0);
		driveShaftDepth[0] = chDims.getX() / 2.0
				- (vhData.connDepthF[0] * whRadius[0]);
		driveShaftDepth[1] = chDims.getX() / 2.0
				- (vhData.connDepthF[1] * whRadius[1]);
	}
		break;
	default: // bt3::Panda3dUpAxis::Z_up -> Bullet3 Y up
	{
		// Bullet3 frame
		//    |z
		// x--o
		rightIdx = bt3::p3dBt3AxisSwitch(0);
		upIdx = bt3::p3dBt3AxisSwitch(2);
		forwardIdx = bt3::p3dBt3AxisSwitch(1);
		whDirectionCS0 = btVector3(0, -1, 0);
		whAxleCS = btVector3(-1, 0, 0);
		driveShaftWidth[0] = chDims.getX() / 2.0
				- (vhData.connWidthF[0] * whDims[0].getX() / 2.0);
		driveShaftWidth[1] = chDims.getX() / 2.0
				- (vhData.connWidthF[1] * whDims[1].getX() / 2.0);
		driveShaftDepth[0] = chDims.getZ() / 2.0
				- (vhData.connDepthF[0] * whRadius[0]);
		driveShaftDepth[1] = chDims.getZ() / 2.0
				- (vhData.connDepthF[1] * whRadius[1]);
	}
		break;
	}
	// compute connection points
	if (numWheels == 4)
	{
		//front
		connPointCS0[0] = btVector3(driveShaftWidth[0], -connHeight[0],
				-driveShaftDepth[0]);
		connPointCS0[1] = btVector3(-driveShaftWidth[0], -connHeight[0],
				-driveShaftDepth[0]);
		//rear
		connPointCS0[2] = btVector3(-driveShaftWidth[1], -connHeight[1],
				driveShaftDepth[1]);
		connPointCS0[3] = btVector3(driveShaftWidth[1], -connHeight[1],
				driveShaftDepth[1]);
	}
	else // numWheels == 2
	{
		//front
		connPointCS0[0] = btVector3(0, -connHeight[0], -driveShaftDepth[0]);
		//rear
		connPointCS0[1] = btVector3(0, -connHeight[1], driveShaftDepth[1]);
	}
	//
	vehicle->setCoordinateSystem(rightIdx, upIdx, forwardIdx);
}
} //anonymous namespace

namespace bt3
{

void VehicleManager::update(float dt)
{
	// update each vehicle
	for (int v = 0; v < mPhysics.raycastVehicles.size(); v++)
	{
		btRaycastVehicle* vehicle = mPhysics.raycastVehicles[v];
		for (int i = 0; i < vehicle->getNumWheels(); i++)
		{
			//synchronize the wheels with the (interpolated) chassis world transform
			vehicle->updateWheelTransform(i, true);
			// update motion state of graphic object
			btWheelInfo& wheelInfo = vehicle->getWheelInfo(i);
			static_cast<DynamicMotionState*>(wheelInfo.m_clientInfo)->setWorldTransform(
					wheelInfo.m_worldTransform);
		}
	}
}

btRaycastVehicle* addVehicle(const NodePath& chassisNP, const NodePath (&wheelNP)[2],
		const VehicleData& vhData)
{
	if (chassisNP.is_empty())
	{
		return nullptr;
	}

	VehicleManager& manager = VehicleManager::GetSingleton();

	// get chassis if any
	btRigidBody* chassis = btRigidBody::upcast(
			getPhysicsObjectOfNode(chassisNP, manager.getPhysics()));
	if (!chassis)
	{
		return nullptr;
	}

	// create vehicle
	btRaycastVehicle* vehicle = new btRaycastVehicle(vhData.tuning, chassis,
			manager.getRayCaster());
	// never deactivate the vehicle
	chassis->setActivationState(DISABLE_DEACTIVATION);

	// add vehicle to world
	manager.getPhysics().dynamicsWorld->addAction(vehicle);
	manager.getPhysics().raycastVehicles.push_back(vehicle);

	// set coordinate system and compute wheel data
	int numWheels = (vhData.numWheels < 4 ? 2 : 4);
	float whRadius[2];
	btVector3 whDirectionCS0, whAxleCS;
	vector<btVector3> connPointCS0;

	// check if wheel models are not empty
	if (wheelNP[0].is_empty() || wheelNP[1].is_empty())
	{
		return vehicle;
	}
	setCoordinateSystemAndComputeData(vehicle, chassisNP, wheelNP, vhData,
			numWheels, whRadius, whDirectionCS0, whAxleCS, connPointCS0);

	// add the wheels to the vehicle
	if (numWheels == 4)
	{
		//front
		vehicle->addWheel(connPointCS0[0], whDirectionCS0, whAxleCS,
				vhData.suspRestLengthF[0] * whRadius[0], whRadius[0],
				vhData.tuning, true);
		vehicle->addWheel(connPointCS0[1], whDirectionCS0, whAxleCS,
				vhData.suspRestLengthF[0] * whRadius[0], whRadius[0],
				vhData.tuning, true);
		//rear
		vehicle->addWheel(connPointCS0[2], whDirectionCS0, whAxleCS,
				vhData.suspRestLengthF[1] * whRadius[1], whRadius[1],
				vhData.tuning, false);
		vehicle->addWheel(connPointCS0[3], whDirectionCS0, whAxleCS,
				vhData.suspRestLengthF[1] * whRadius[1], whRadius[1],
				vhData.tuning, false);
	}
	else // numWheels == 2
	{
		//front
		vehicle->addWheel(connPointCS0[0], whDirectionCS0, whAxleCS,
				vhData.suspRestLengthF[0] * whRadius[0], whRadius[0],
				vhData.tuning, true);
		//rear
		vehicle->addWheel(connPointCS0[1], whDirectionCS0, whAxleCS,
				vhData.suspRestLengthF[1] * whRadius[1], whRadius[1],
				vhData.tuning, false);
	}

	// configure parameters
	for (int i = 0; i < vehicle->getNumWheels(); i++)
	{
		btWheelInfo& wheel = vehicle->getWheelInfo(i);
		// create an instance of the front/rear modelNP
		NodePath wheelInstNP(chassisNP.get_name() +"_wheel_" + str(i));
		wheel.m_bIsFrontWheel ?
				wheelNP[0].instance_to(wheelInstNP)://front
				wheelNP[1].instance_to(wheelInstNP);//rear
		// reparent wheelInstNP to the chassisNP's parent if any
		if (chassisNP.has_parent())
		{
			wheelInstNP.reparent_to(chassisNP.get_parent());
		}
		// create a motion state for the wheelInstNP graphic object
		wheel.m_clientInfo = static_cast<void*>(makeMotionState(wheelInstNP));
	}

	// reset
	manager.getPhysics().dynamicsWorld->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(
			chassis->getBroadphaseHandle(),
			manager.getPhysics().dynamicsWorld->getDispatcher());
	vehicle->resetSuspension();

	PRINT_DEBUG("addVehicle(): chassis " << chassisNP.get_name());
	//
	return vehicle;
}

bool removeVehicle(btRaycastVehicle* vehicle)
{
	if (!vehicle)
	{
		return false;
	}

	// find the vehicle
	VehicleManager& manager = VehicleManager::GetSingleton();
	auto& vehicles = manager.getPhysics().raycastVehicles;
	int index = vehicles.findLinearSearch2(vehicle);
	if (index == -1)
	{
		return false;
	}
	NodePath chassisNP = getNodeFromPhysicsObject(vehicles[index]->getRigidBody());

	// remove each wheel's graphic object and delete the motion state
	for(int w = 0; w < vehicle->getNumWheels(); w++)
	{
		DynamicMotionState* motionState =
				static_cast<DynamicMotionState*>(vehicle->getWheelInfo(w).m_clientInfo);
		motionState->getGraphicsObject().remove_node();
		delete motionState;
	}
	// remove vehicle from world and delete it
	manager.getPhysics().dynamicsWorld->removeAction(vehicle);
	vehicles.remove(vehicle);
	delete vehicle;

	//report
	if (!chassisNP.is_empty())
	{
		PRINT_DEBUG("removeVehicle(): chassis " << chassisNP.get_name());
	}
	else
	{
		PRINT_DEBUG("removeVehicle(): no graphic node");
	}
	//
	return true;
}

} //namespace bt3
