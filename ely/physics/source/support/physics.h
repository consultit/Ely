/**
 * \file physics.h
 *
 * \date 2016-12-10
 * \author consultit
 */

#ifndef PHYSICS_H_
#define PHYSICS_H_

#include "../bullet3/src/BulletSoftBody/btSoftRigidDynamicsWorld.h"
#include "../bullet3/src/BulletCollision/CollisionDispatch/btGhostObject.h"
#include "../bullet3/src/BulletDynamics/Character/btKinematicCharacterController.h"
#include "../bullet3/src/BulletSoftBody/btDefaultSoftBodySolver.h"
#include "../bullet3/src/BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.h"
#include "../bullet3/src/BulletDynamics/Vehicle/btRaycastVehicle.h"
#include "../bullet3/src/LinearMath/btAlignedObjectArray.h"
#include "debugDrawing.h"
#include <unordered_map>

namespace bt3
{
/// Bullet
struct PhysicsInfo
{
	enum class WorldType: unsigned char
	{
		SoftRigid,
		Discrete,
		DiscreteMt,
		MultiBody,
		SoftMultiBody,
		Simple,
		Fracture,//TODO see examples
	};
	enum class BroadphaseType: unsigned char
	{
		AxisSweep3,
		AxisSweep3_32Bit,
		Dbvt,
		Simple,
	};
	enum class ConstraintSolverType: unsigned char
	{
		SequentialImpulse,
		MultiBody,
		NNCG,
		PoolMt,
		MLCP,
		///Multi-Threading
		SequentialImpulseMt,
	};
	enum class MLCPSolverType: unsigned char
	{
		Dantzig,
		Lemke,
		ProjectedGaussSeidel,
	};
	PhysicsInfo():
		worldType(PhysicsInfo::WorldType::SoftRigid),
		broadPhaseType(PhysicsInfo::BroadphaseType::AxisSweep3),
		solverType(PhysicsInfo::ConstraintSolverType::SequentialImpulse),
		mlcpSolverType(PhysicsInfo::MLCPSolverType::Dantzig),
		softBodySolverType(btSoftBodySolver::SolverTypes::DEFAULT_SOLVER),
		numPoolMtSolvers(16),
		worldDims(btVector3(2000.0, 2000.0, 2000.0)),
		maxHandles(16384),
		airDensity(1.2),
		waterDensity(0.0),
		waterOffset(0.0),
		maxDisplacement(1000.0),
		waterNormal(btVector3(0.0, 0.0, 0.0)),
		///Multi-Threading
		maxPersistentManifoldPoolSize(80000),
		maxCollisionAlgorithmPoolSize(80000),
		grainSize(40)
	{}
	//data
	WorldType worldType;
	BroadphaseType broadPhaseType;
	ConstraintSolverType solverType;
	MLCPSolverType mlcpSolverType;
	btSoftBodySolver::SolverTypes softBodySolverType;
	int numPoolMtSolvers;
	btVector3 worldDims;
	unsigned short int maxHandles;
	//Specific to soft world
	float airDensity, waterDensity, waterOffset, maxDisplacement;
	btVector3 waterNormal;
	///Multi-Threading
	int maxPersistentManifoldPoolSize, maxCollisionAlgorithmPoolSize, grainSize;
};

struct BroadphaseFilterCallback;
struct Physics
{
	//constructor,destructor
	Physics(const PhysicsInfo& info = PhysicsInfo
	{ });
	~Physics()
	{
	}
	//function members
	//earth=-9.807, -moon=-1.622, mars=-3.711, Jupiter=-24.79
	void createDynamicsWorld(
			const btVector3& gravity = btVector3(0, -9.807, 0));
	void deleteDynamicsWorld();

	void printPositionsAllObjects();

	void registerBroadphaseCollisionCallback(
			btOverlapFilterCallback * filterCallback);

	//dynamics world's data
	PhysicsInfo physicsInfo;
	btBroadphaseInterface* broadphase;
	btCollisionDispatcher* dispatcher;
	btConstraintSolver* solver;
	btDefaultCollisionConfiguration* collisionConfiguration;
	btDynamicsWorld* dynamicsWorld;
	btGhostPairCallback ghostCallback;
	btSoftBodyWorldInfo softBodyWorldInfo;
	//debug drawer
	DebugDrawer* debugDrawer;
	bool doDebugDraw;
	//data members
	unordered_map<btCollisionShape*, int> collisionShapes;	// <shape, refCount>
	unordered_map<btCollisionShape*, btTriangleIndexVertexArray*> indexVertexArrays;
	unordered_map<btCollisionShape*, float*> heightFieldDataArrays;
	btAlignedObjectArray<btRaycastVehicle*> raycastVehicles;
	btAlignedObjectArray<btKinematicCharacterController*> kinematicCharacterControllers;
	btConstraintSolver* createSolverByType(PhysicsInfo::ConstraintSolverType t,
			PhysicsInfo::MLCPSolverType mlcpT);
	///Multi-Threading
	class btTaskSchedulerManager
	{
		btAlignedObjectArray<btITaskScheduler*> m_taskSchedulers;
		btAlignedObjectArray<btITaskScheduler*> m_allocatedTaskSchedulers;

	public:
		btTaskSchedulerManager()
		{
		}
		void init()
		{
			addTaskScheduler(btGetSequentialTaskScheduler());
			if (btITaskScheduler* ts = btCreateDefaultTaskScheduler())
			{
				m_allocatedTaskSchedulers.push_back(ts);
				addTaskScheduler(ts);
			}
			addTaskScheduler(btGetOpenMPTaskScheduler());
			addTaskScheduler(btGetTBBTaskScheduler());
			addTaskScheduler(btGetPPLTaskScheduler());
			if (getNumTaskSchedulers() > 1)
			{
				// prefer a non-sequential scheduler if available
				btSetTaskScheduler(m_taskSchedulers[1]);
			}
			else
			{
				btSetTaskScheduler(m_taskSchedulers[0]);
			}
		}
		void shutdown()
		{
			for (int i = 0; i < m_allocatedTaskSchedulers.size(); ++i)
			{
				delete m_allocatedTaskSchedulers[i];
			}
			m_allocatedTaskSchedulers.clear();
		}

		void addTaskScheduler(btITaskScheduler* ts)
		{
			if (ts)
			{
				// if initial number of threads is 0 or 1,
				if (ts->getNumThreads() <= 1)
				{
					// for OpenMP, TBB, PPL set num threads to number of logical cores
					ts->setNumThreads(ts->getMaxNumThreads());
				}
				m_taskSchedulers.push_back(ts);
			}
		}
		int getNumTaskSchedulers() const
		{
			return m_taskSchedulers.size();
		}
		btITaskScheduler* getTaskScheduler(int i)
		{
			return m_taskSchedulers[i];
		}
	};
	btTaskSchedulerManager taskSchedulerMgr;
};
} //namespace bt3

#endif /* PHYSICS_H_ */
