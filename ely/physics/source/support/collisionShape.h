/**
 * \file collisionShape.h
 *
 * \date 2017-03-07
 * \author consultit
 */
#ifndef COLLISIONSHAPE_H_
#define COLLISIONSHAPE_H_

#include <nodePath.h>
#include "physics.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btCompoundShape.h"

namespace bt3
{

btCollisionShape* createCollisionShapeForNode(NodePath& target,
		Physics& physics, int shapeType, Panda3dUpAxis upAxis =
				Panda3dUpAxis::Z_up, void* userData = nullptr,
		BroadphaseNativeTypes childShapeType = INVALID_SHAPE_PROXYTYPE,
		btCollisionShape* sharedShape = nullptr);
void destroyCollisionShape(btCollisionShape* shape, Physics& physics);

inline btConvexShape* upcastConvexShape(btCollisionShape* shape)
{
	if (shape->getShapeType() >= CONCAVE_SHAPES_START_HERE)
	{
		return nullptr;
	}
	return static_cast<btConvexShape*>(shape);
}

inline btConcaveShape* upcastConcaveShape(btCollisionShape* shape)
{
	if ((shape->getShapeType() < CONCAVE_SHAPES_START_HERE)
			|| (shape->getShapeType() > CONCAVE_SHAPES_END_HERE))
	{
		return nullptr;
	}
	return static_cast<btConcaveShape*>(shape);
}

inline btCompoundShape* upcastCompoundShape(btCollisionShape* shape)
{
	if (shape->getShapeType() != COMPOUND_SHAPE_PROXYTYPE)
	{
		return nullptr;
	}
	return static_cast<btCompoundShape*>(shape);
}

} //namespace bt3

#endif /* COLLISIONSHAPE_H_ */
