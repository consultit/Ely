/**
 * \file convexDecomposition.h
 *
 * \date 2017-03-07
 * \author consultit
 */
#ifndef CONVEXDECOMPOSITION_H_
#define CONVEXDECOMPOSITION_H_

#include "common.h"
#include "../bullet3/src/BulletCollision/BroadphaseCollision/btBroadphaseProxy.h"

///convex decomposition
#define _CRT_SECURE_NO_WARNINGS
//#define _CRTDBG_MAP_ALLOC
#ifdef _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#include <stdlib.h>
#endif // _CRTDBG_MAP_ALLOC
#include "../v-hacd/src/VHACD_Lib/public/VHACD.h"
#ifdef OPENCL_FOUND
#include "../v-hacd/src/test/inc/oclHelper.h"
#endif

namespace bt3
{
/// Convex decomposition
class ConvexDecompositionCallback: public VHACD::IVHACD::IUserCallback
{
public:
	ConvexDecompositionCallback(void)
	{
	}
	~ConvexDecompositionCallback()
	{
	}
	;
	void Update(const double overallProgress, const double stageProgress,
			const double operationProgress, const char* const stage,
			const char* const operation)
	{
		cout << setfill(' ') << setw(3) << (int) (overallProgress + 0.5) << "% "
				<< "[ " << stage << " " << setfill(' ') << setw(3)
				<< (int) (stageProgress + 0.5) << "% ] " << operation << " "
				<< setfill(' ') << setw(3) << (int) (operationProgress + 0.5)
				<< "%" << endl;
	}
	;
};
class ConvexDecompositionLogger: public VHACD::IVHACD::IUserLogger
{
public:
	ConvexDecompositionLogger(void)
	{
	}
	ConvexDecompositionLogger(const string& fileName)
	{
		OpenFile(fileName);
	}
	~ConvexDecompositionLogger()
	{
	}
	;
	void Log(const char* const msg)
	{
		if (m_file.is_open())
		{
			m_file << msg;
			m_file.flush();
		}
	}
	void OpenFile(const string& fileName)
	{
		m_file.open(fileName.c_str());
	}

private:
	ofstream m_file;
};
struct ConvexDecompositionParameters
{
	unsigned int m_oclPlatformID;
	unsigned int m_oclDeviceID;
	string m_fileNameLog;
	VHACD::IVHACD::Parameters m_paramsVHACD;
	ConvexDecompositionParameters() :
			m_oclPlatformID(0), m_oclDeviceID(0), m_fileNameLog(
					"convex_decompose.log")
	{
	}
};
#ifdef CL_VERSION_1_1
bool InitOCL(const unsigned int oclPlatformID, const unsigned int oclDeviceID, OCLHelper& oclHelper, ostringstream& msg);
#endif // CL_VERSION_1_1
btCollisionShape* convexDecompose(NodePath& target, Physics& physics,
		BroadphaseNativeTypes childShapeType = CONVEX_HULL_SHAPE_PROXYTYPE,
		const ConvexDecompositionParameters& params =
				ConvexDecompositionParameters());

}

#endif /* CONVEXDECOMPOSITION_H_ */
