/**
 * \file softBody.h
 *
 * \date 2017-03-08
 * \author consultit
 */
#ifndef SOFTBODY_H_
#define SOFTBODY_H_

#include "common.h"
#include "physics_includes.h"
#include <lpoint3.h>
#include <nurbsCurveEvaluator.h>

struct btSoftBody;
namespace bt3
{

//soft body
struct SoftBodyData
{
	enum class SoftBodyType: unsigned char
	{
		ROPE,
		PATCH,
		ELLIPSOID,
		TRIANGLE_MESH,//works with models having one GeomNode holding one or more Geoms.
		CONVEX_HULL,
		TETGEN_MESH,
		INVALID_TYPE,
	};
	struct RopeData
	{
		LPoint3f from, to;
		int res;
		//or-ed flags: 0x0=free ends|0x1=fixed first|0x2=fixed last
		int fixeds;
	};
	struct PatchData
	{
		LPoint3f corner[2][2];
		int resx, resy;
		//or-ed flags: 0x0=free corners|0x1=fixed (0,0)|0x2=fixed (1,0)
		// 			   |0x4=fixed (0,1)|0x8=fixed (1,1)
		int fixeds;
		bool gendiags;
	};
	struct EllipsoidData
	{
		LPoint3f center;
		LVecBase3f radius;
		int res;
	};
	struct TriangleMeshData
	{
		bool randomizeConstraints;
	};
	struct TetgenMeshData
	{
		string ele, face, node;
		bool bfacelinks, btetralinks, bfacesfromtetras;
	};
	//
	SoftBodyData():
		type{SoftBodyType::INVALID_TYPE},
		totalMass{0.0},
		fromFaces{false}
	{
	}
	~SoftBodyData(){}
	//
	SoftBodyType type;
	float totalMass;
	bool fromFaces;
	union
	{
		RopeData ropeData;
		PatchData patchData;
		EllipsoidData ellipsoidData;
		TriangleMeshData triangleMeshData;
	};
	TetgenMeshData tetgenMeshData;
	void resetData()
	{
		switch (type) {
			case SoftBodyType::ROPE:
				ropeData = {LPoint3f::zero(), LPoint3f::zero(), 0, 0};
				break;
			case SoftBodyType::PATCH:
				patchData = {LPoint3f::zero(), LPoint3f::zero(),
						LPoint3f::zero(), LPoint3f::zero(), 0, 0, 0, false};
				break;
			case SoftBodyType::ELLIPSOID:
				ellipsoidData = {LPoint3f::zero(), LVector3f::zero(), 0};
				break;
			case SoftBodyType::TRIANGLE_MESH:
				triangleMeshData = {false};
				break;
			case SoftBodyType::TETGEN_MESH:
				tetgenMeshData = {string(), string(), string(),
						false, false, false};
				break;
			default:
				break;
		}
		type = SoftBodyType::INVALID_TYPE;
	}
};
class SoftBodyManager: public Singleton<SoftBodyManager>, public TypedReferenceCount
{
public:
	struct UpdateData
	{
		UpdateData() :
				body{nullptr}, node{nullptr}, curve{nullptr}
		{
			geoms.clear();
		}
		//
		btSoftBody* body;
		PT(PandaNode) node;
		PT(NurbsCurveEvaluator) curve;
		vector<PT(Geom)> geoms;
		vector<int> geomsStartIndexes;
		vector<int> indexMap;		//TODO move into geom(?)
	};
	//
	SoftBodyManager()
	{
		mUpdatedData.clear();
	}
	virtual ~SoftBodyManager()
	{
		mUpdatedData.clear();
	}
	virtual void update(float dt);
	vector<UpdateData>& getUpdateData()
	{
		return mUpdatedData;
	}
	//utilities
	PT(Geom) make_geom(btSoftBody *body, vector<int>& indexMap, bool use_faces);
	void make_texcoords_for_patch(Geom *geom, int resx, int resy);
	void make_triangle_mesh_no_duplicate(const float* inVerts, int inVertCount,
			const int* inTris, int inTriCount, const float** outVerts, int* outVertCount,
			const int** outTris, vector<int>& indexMap, float delta = 1e-5f);

private:
	vector<UpdateData> mUpdatedData;
};
void addSoftBodyToNode(NodePath& target, Physics& physics, SoftBodyData& sfData,
		BitMask32 groupMask = BitMask32::all_on(),
		BitMask32 collideMask = BitMask32::all_on());
bool removeSoftBodyFromNode(NodePath& target, const Physics& physics);

} //namespace bt3

#endif /* SOFTBODY_H_ */
