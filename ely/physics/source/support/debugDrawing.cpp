/**
 * \file debugDrawing.cpp
 *
 * \date 2016-12-10
 * \author consultit
 */

#include "debugDrawing.h"
#include <geomLines.h>
#include <geomVertexWriter.h>

namespace bt3
{
void DebugDrawer::drawLine(const btVector3 &from, const btVector3 &to,
		const btVector3 &color)
{
	PN_stdfloat r = color.getX();
	PN_stdfloat g = color.getY();
	PN_stdfloat b = color.getZ();

	// Hack to get rid of triangle normals.  The hack is based on the assumption
	// that only normals are drawn in yellow.
	if (_normals == false && r == 1.0f && g == 1.0f && b == 0.0f)
		return;

	Line line;

	line._p0 = btVector3_to_LVecBase3(from);
	line._p1 = btVector3_to_LVecBase3(to);
	line._color = UnalignedLVecBase4((PN_stdfloat) r, (PN_stdfloat) g,
			(PN_stdfloat) b, 1.0f);

	_lines.push_back(line);
}

void DebugDrawer::drawContactPoint(const btVector3 &point,
		const btVector3 &normal, btScalar distance, int lifetime,
		const btVector3 &color)
{
	const btVector3 to = point + normal * distance;
	const btVector3 &from = point;

	drawLine(from, to, color);
}

void DebugDrawer::draw3dText(const btVector3 &location, const char *text)
{
	reportErrorWarning("draw3dText - not yet implemented!");
}

void DebugDrawer::doDebugDraw()
{
	// Render lines
	{
		PT(GeomVertexData)vdata;
		PT(GeomLines) prim;

		vdata = new GeomVertexData("", GeomVertexFormat::get_v3c4(), Geom::UH_stream);

		prim = new GeomLines(Geom::UH_stream);
		prim->set_shade_model(Geom::SM_uniform);

		GeomVertexWriter vwriter = GeomVertexWriter(vdata, InternalName::get_vertex());
		GeomVertexWriter cwriter = GeomVertexWriter(vdata, InternalName::get_color());

		int v = 0;
		pvector<Line>::const_iterator lit;
		for (lit = _lines.begin(); lit != _lines.end(); lit++)
		{
			Line line = *lit;

			vwriter.add_data3(line._p0);
			vwriter.add_data3(line._p1);
			cwriter.add_data4(LVecBase4(line._color));
			cwriter.add_data4(LVecBase4(line._color));

			prim->add_vertex(v++);
			prim->add_vertex(v++);
			prim->close_primitive();
		}

		_geom = new Geom(vdata);
		_geom->add_primitive(prim);
		_geomNode->add_geom(_geom);
	}
}

} //namespace bt3

