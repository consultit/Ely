/**
 * \file ghost.cpp
 *
 * \date 2017-03-08
 * \author consultit
 */

#include "ghost.h"
#include "physics.h"
#include "collisionShape.h"

namespace
{
btPairCachingGhostObject* createGhostForNode(const NodePath& target, btCollisionShape* shape)
{
	ASSERT_TRUE(shape && (shape->getShapeType() != INVALID_SHAPE_PROXYTYPE))

	// create a ghost
	btPairCachingGhostObject* ghost = new btPairCachingGhostObject();
	// set no contact response
	ghost->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);

	// set initial ghost's world transform
	// make a temporary dynamic motion state
	bt3::DynamicMotionState* motionState = bt3::makeMotionState(target);
	motionState->getWorldTransform(ghost->getWorldTransform());
	// delete the temporary dynamic motion state
	delete motionState;

	// add shape to the ghost
	ghost->setCollisionShape(shape);

	// set target's PandaNode as ghost's user pointer
	ghost->setUserPointer((void*) target.node());

	// return the just created rigid body
	return ghost;
}

} //namespace anonymous

namespace bt3
{

void addGhostToNode(NodePath& target, Physics& physics, bool visible,
		btCollisionShape* sharedShape, BroadphaseNativeTypes shapeType,
		Panda3dUpAxis upAxis, BitMask32 groupMask, BitMask32 collideMask,
		void* shapeData, BroadphaseNativeTypes childShapeType)
{
	if (target.is_empty())
	{
		return;
	}
	// get a collision shape
	btCollisionShape* shape = createCollisionShapeForNode(target, physics, shapeType, upAxis,
				shapeData, childShapeType, sharedShape);
	// add a ghost to node
	btPairCachingGhostObject* ghost = createGhostForNode(target, shape);
	// add the ghost to the dynamics world
	physics.dynamicsWorld->addCollisionObject(ghost, groupMask.get_word(),
			collideMask.get_word());
	//add the "physics" tag to target node
	if (!target.has_tag("physics"))
	{
		target.set_tag("physics", "yes");
	}
	//handle visibility
	visible ? target.show() : target.hide();
	//
	PRINT_DEBUG("addGhostToNode(): " << target.get_name());
}

bool removeGhostFromNode(NodePath& target, Physics& physics)
{
	bool result = false;
	if (target.is_empty())
	{
		return result;
	}

	//delete the rigid body from the dynamics world and delete it
	for (int i = 0; i < physics.dynamicsWorld->getNumCollisionObjects(); i++)
	{
		btCollisionObject* obj =
				physics.dynamicsWorld->getCollisionObjectArray()[i];
		if (reinterpret_cast<PandaNode*>(obj->getUserPointer())
				== target.node())
		{
			//check if it is a ghost
			btGhostObject* ghost = btPairCachingGhostObject::upcast(obj);
			if (ghost)
			{
				//try to destroy collision shape
				destroyCollisionShape(ghost->getCollisionShape(), physics);
				//complete ghost removal
				physics.dynamicsWorld->removeCollisionObject(ghost);
				delete ghost;
				//remove the "physics" tag from target node
				if (target.has_tag("physics"))
				{
					target.clear_tag("physics");
				}
				//removed: exit loop
				result = true;
				break;
			}
		}
	}
	PRINT_DEBUG(
			(result ? "" : "not ") << "removeGhostFromNode(): " << target.get_name());
	//
	return result;
}

} //namespace bt3
