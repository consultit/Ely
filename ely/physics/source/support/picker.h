/**
 * \file picker.h
 *
 * \date 2017-03-07
 * \author consultit
 */
#ifndef PICKER_H_
#define PICKER_H_

#include "physics.h"
#include "physics_includes.h"
#include <mouseWatcher.h>

namespace bt3
{

/**
 * A class for picking (physics) objects.
 * \note Bullet based.
 */
class Picker
{
public:
	Picker(const NodePath &render, const NodePath &camera,
			MouseWatcher *watcher, const Physics &physics,
			const string &pickKeyOn, const string &pickKeyOff);
	virtual ~Picker();

	//getters/setters
	inline btTypedConstraintType getConstraintType() const;
	inline void setConstraintType(btTypedConstraintType type);
	inline uint32_t getGroupMask() const;
	inline void setGroupMask(uint32_t mask);
	inline uint32_t getCollideMask() const;
	inline void setCollideMask(uint32_t mask);
	inline float getCfm() const;
	inline void setCfm(float value);
	inline float getErp() const;
	inline void setErp(float value);

	//raycast
	const btCollisionObject* rayCast();
	inline LPoint3f getHitCastPos() const;
	inline LPoint3f getHitCastFromPos() const;
	inline LPoint3f getHitCastToPos() const;
	inline LVector3f getHitCastNormal() const;
	inline float getHitCastFraction() const;

private:
	///Render, camera node paths.
	NodePath mRender, mCamera;
	///Mouse watcher.
	PT(MouseWatcher) mMouseWatcher;
	///Camera lens reference.
	PT(Lens) mCamLens;
	///Bullet world.
	btDynamicsWorld *mDynamicsWorld;
	bool mSoftWorld;
	/**
	 * \name Picking logic data.
	 */
	///@{
	uint32_t mGroupMask;
	uint32_t mCollideMask;
	btTypedConstraint *mPickedConstraint;
	btTypedConstraintType mConstraintType;
	btScalar mCfm, mErp;

	//soft body
	btSoftBody::sRayCast mSoftResults;
	btSoftBody::Node *mSoftNode;
	btVector3 mSoftImpact, mSoftGoal;
	bool mSoftDrag;
	///@}

	/**
	 * \name Pick body event callback data.
	 */
	///@{
	PT(EventCallbackInterface<Picker>::EventCallbackData) mPickBodyData;
	void pickBody(const Event *event);
	btRigidBody *mPickedBody;
	int mSavedState;
	btVector3 mOldPickingPos, mHitPos;
	btScalar mOldPickingDist;
	string mPickKeyOn, mPickKeyOff;
	///@}

	/**
	 * \name Last ray cast hit results data.
	 */
	///@{
	LPoint3f mHitCastPos, mHitCastFromPos, mHitCastToPos;
	LVector3f mHitCastNormal;
	float mHitCastFraction;
	///@}

	///@{
	PT(TaskInterface<Picker>::TaskData) mUpdateData;
	PT(AsyncTask) mUpdateTask;
	AsyncTask::DoneStatus movePickedBody(GenericAsyncTask *task);
	///@}
};

inline btTypedConstraintType Picker::getConstraintType() const
{
	return mConstraintType;
}

inline void Picker::setConstraintType(btTypedConstraintType type)
{
	mConstraintType = type;
}

inline uint32_t Picker::getGroupMask() const
{
	return mGroupMask;
}

inline void Picker::setGroupMask(uint32_t mask)
{
	mGroupMask = mask;
}

inline uint32_t Picker::getCollideMask() const
{
	return mCollideMask;
}

inline void Picker::setCollideMask(uint32_t mask)
{
	mCollideMask = mask;
}

inline float Picker::getCfm() const
{
	return mCfm;
}

inline void Picker::setCfm(float value)
{
	mCfm = value;
}

inline float Picker::getErp() const
{
	return mErp;
}

inline void Picker::setErp(float value)
{
	mErp = value;
}

inline LPoint3f Picker::getHitCastPos() const
{
	return mHitCastPos;
}

inline LPoint3f Picker::getHitCastFromPos() const
{
	return mHitCastFromPos;
}

inline LPoint3f Picker::getHitCastToPos() const
{
	return mHitCastToPos;
}

inline LVector3f Picker::getHitCastNormal() const
{
	return mHitCastNormal;
}

inline float Picker::getHitCastFraction() const
{
	return mHitCastFraction;
}

} //namespace bt3

#endif /* PICKER_H_ */
