/**
 * \file vehicle.h
 *
 * \date 2017-05-27
 * \author consultit
 */
#ifndef VEHICLE_H_
#define VEHICLE_H_

#include "common.h"
#include "physics.h"

namespace bt3
{

struct physics;
//vehicle
struct VehicleData
{
	VehicleData():
		cs{Panda3dUpAxis::Z_up},
		tuning{},
		numWheels{4},//2 or 4 wheels should be specified
		connHeightF{1.0,1.0},
		connWidthF{0.3,0.3},
		connDepthF{1.0,1.0},
		suspRestLengthF{1.2,1.2}
	{
	}
	Panda3dUpAxis cs;
	btRaycastVehicle::btVehicleTuning tuning;
	int numWheels;
	// front,rear specifications
	float connHeightF[2], connWidthF[2], connDepthF[2];
	float suspRestLengthF[2];
};
class VehicleManager: public Singleton<VehicleManager>, public TypedReferenceCount
{
public:
	VehicleManager(Physics& physics) :
		mPhysics{physics},
		mRayCaster{new btDefaultVehicleRaycaster(physics.dynamicsWorld)}
	{
	}
	virtual ~VehicleManager()
	{
		delete mRayCaster;
	}
	virtual void update(float dt);
	Physics& getPhysics()
	{
		return mPhysics;
	}
	btVehicleRaycaster* getRayCaster()
	{
		return mRayCaster;
	}

private:
	Physics& mPhysics;
	btVehicleRaycaster* mRayCaster;
};
///Note: wheels (i.e. instances of the wheelNP models) are re-parented to the
/// chassisNP' parent if any.
btRaycastVehicle* addVehicle(const NodePath& chassisNP, const NodePath (&wheelNP)[2],
		const VehicleData& vhData = VehicleData());
bool removeVehicle(btRaycastVehicle* vehicle);

}

#endif /* VEHICLE_H_ */
