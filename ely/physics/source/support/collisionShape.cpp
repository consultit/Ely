/**
 * \file collisionShape.cpp
 *
 * \date 2017-03-07
 * \author consultit
 */

#include "collisionShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btConvexShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btStaticPlaneShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btSphereShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btCapsuleShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btCylinderShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btConeShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btConvexHullShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btMultiSphereShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btTriangleMesh.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btBvhTriangleMeshShape.h"
#include "../bullet3/src/BulletCollision/Gimpact/btGImpactShape.h"
#include "../bullet3/src/BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"
#include "convexDecomposition.h"
#include <geoMipTerrain.h>

namespace bt3
{

btCollisionShape* createCollisionShapeForNode(NodePath& target, Physics& physics,
		int shapeType, Panda3dUpAxis upAxis, void* userData,
		BroadphaseNativeTypes childShapeType, btCollisionShape* sharedShape)
{
	// first check shared shape
	if (sharedShape
			&& (physics.collisionShapes.find(sharedShape)
					!= physics.collisionShapes.end()))
	{
		// increment reference count
		++physics.collisionShapes[sharedShape];
		PRINT_DEBUG("createCollisionShapeForNode(): " << target.get_name()
				<< "; shape users = " << physics.collisionShapes[sharedShape]);
		return sharedShape;
	}

	btCollisionShape* shape = nullptr;
	LVecBase3f modelDims;
	LVector3f modelDeltaCenter;
	float modelRadius = getBoundingDimensions(target, modelDims,
			modelDeltaCenter, upAxis);
	// switch shape type
	switch (shapeType)
	{
	/*TODO
	TRIANGLE_SHAPE_PROXYTYPE,//btTriangleShape
	TETRAHEDRAL_SHAPE_PROXYTYPE,//btTetrahedronShapeEx
	CONVEX_TRIANGLEMESH_SHAPE_PROXYTYPE,//btConvexTriangleMeshShape
	CONVEX_POINT_CLOUD_SHAPE_PROXYTYPE,//btConvexPointCloudShape
	CUSTOM_POLYHEDRAL_SHAPE_TYPE,//NONE
	UNIFORM_SCALING_SHAPE_PROXYTYPE,//btUniformScalingShape
	MINKOWSKI_SUM_SHAPE_PROXYTYPE,//NONE
	MINKOWSKI_DIFFERENCE_SHAPE_PROXYTYPE,//btMinkowskiSumShape
	BOX_2D_SHAPE_PROXYTYPE,//btBox2dShape
	CONVEX_2D_SHAPE_PROXYTYPE,//btConvex2dShape
	CUSTOM_CONVEX_SHAPE_TYPE,//NONE
	SCALED_TRIANGLE_MESH_SHAPE_PROXYTYPE,//btScaledBvhTriangleMeshShape
	FAST_CONCAVE_MESH_PROXYTYPE,//NONE
	MULTIMATERIAL_TRIANGLE_MESH_PROXYTYPE,//btMultimaterialTriangleMeshShape
	EMPTY_SHAPE_PROXYTYPE,//btEmptyShape
	CUSTOM_CONCAVE_SHAPE_TYPE,//NONE
	HFFLUID_SHAPE_PROXYTYPE,//NONE
	HFFLUID_BUOYANT_CONVEX_SHAPE_PROXYTYPE,//NONE
	*/
	case STATIC_PLANE_PROXYTYPE:
	{
		shape = new btStaticPlaneShape(btVector3(0.0, 1.0, 0.0), 0.0);
	}
		break;
	case BOX_SHAPE_PROXYTYPE:
	{
		shape = new btBoxShape(
				bt3::LVecBase3_to_btVector3_abs(modelDims) / 2.0);
	}
		break;
	case SPHERE_SHAPE_PROXYTYPE:
	{
		shape = new btSphereShape(modelRadius);
	}
		break;
	case CAPSULE_SHAPE_PROXYTYPE:
	{
		if (upAxis == Panda3dUpAxis::X_up)
		{
			float h = max(0.0, modelDims.get_x() - 2.0 * modelRadius);
			shape = new btCapsuleShapeX(modelRadius, h);
		}
		else if (upAxis == Panda3dUpAxis::Y_up)
		{
			float h = max(0.0, modelDims.get_y() - 2.0 * modelRadius);
			shape = new btCapsuleShapeZ(modelRadius, h);
		}
		else if (upAxis == Panda3dUpAxis::Z_up)
		{
			float h = max(0.0, modelDims.get_z() - 2.0 * modelRadius);
			shape = new btCapsuleShape(modelRadius, h);
		}
		else
		{
			shape = nullptr;
		}
	}
		break;
	case CYLINDER_SHAPE_PROXYTYPE:
	{
		btVector3 halfExtents = bt3::LVecBase3_to_btVector3_abs(modelDims)
				/ 2.0;
		if (upAxis == Panda3dUpAxis::X_up)
		{
			shape = new btCylinderShapeX(halfExtents);
		}
		else if (upAxis == Panda3dUpAxis::Y_up)
		{
			shape = new btCylinderShapeZ(halfExtents);
		}
		else if (upAxis == Panda3dUpAxis::Z_up)
		{
			shape = new btCylinderShape(halfExtents);
		}
		else
		{
			shape = nullptr;
		}
	}
		break;
	case CONE_SHAPE_PROXYTYPE:
	{
		if (upAxis == Panda3dUpAxis::X_up)
		{
			shape = new btConeShapeX(modelRadius, modelDims.get_x());
		}
		else if (upAxis == Panda3dUpAxis::Y_up)
		{
			shape = new btConeShapeZ(modelRadius, modelDims.get_y());
		}
		else if (upAxis == Panda3dUpAxis::Z_up)
		{
			shape = new btConeShape(modelRadius, modelDims.get_z());
		}
		else
		{
			shape = nullptr;
		}
	}
		break;
	case MULTI_SPHERE_SHAPE_PROXYTYPE:
	{
		/*get 8 points of the cuboid
		*  7--------6
		*  |\       |\
		*  | \      | \
		*  |  4--------5
		*  3--|-----2  |
		*   \ |      \ |
		*    \|       \|
		*     0--------1
		*/
		btVector3 positions[8];
		positions[0] = LVecBase3_to_btVector3(
				/*modelDeltaCenter
				 +*/LVector3f(-modelDims.get_x(), -modelDims.get_y(),
						-modelDims.get_z()) / 2.0);
		positions[1] = LVecBase3_to_btVector3(
				/*modelDeltaCenter
				 + */LVector3f(modelDims.get_x(), -modelDims.get_y(),
						-modelDims.get_z()) / 2.0);
		positions[2] = LVecBase3_to_btVector3(
				/*modelDeltaCenter
				 + */LVector3f(modelDims.get_x(), modelDims.get_y(),
						-modelDims.get_z()) / 2.0);
		positions[3] = LVecBase3_to_btVector3(
				/*modelDeltaCenter
				 + */LVector3f(-modelDims.get_x(), modelDims.get_y(),
						-modelDims.get_z()) / 2.0);
		positions[4] = LVecBase3_to_btVector3(
				/*modelDeltaCenter
				 + */LVector3f(-modelDims.get_x(), -modelDims.get_y(),
						modelDims.get_z()) / 2.0);
		positions[5] = LVecBase3_to_btVector3(
				/*modelDeltaCenter
				 + */LVector3f(modelDims.get_x(), -modelDims.get_y(),
						modelDims.get_z()) / 2.0);
		positions[6] = LVecBase3_to_btVector3(
				/*modelDeltaCenter
				 + */LVector3f(modelDims.get_x(), modelDims.get_y(),
						modelDims.get_z()) / 2.0);
		positions[7] = LVecBase3_to_btVector3(
				/*modelDeltaCenter
				 + */LVector3f(-modelDims.get_x(), modelDims.get_y(),
						modelDims.get_z()) / 2.0);
		float radius = modelRadius / 10.0;
		btScalar radi[] =
		{ radius, radius, radius, radius, radius, radius, radius, radius };
		shape = new btMultiSphereShape(positions, radi, 8);
	}
		break;
	case CONVEX_HULL_SHAPE_PROXYTYPE:
	{
		shape = new btConvexHullShape();
		//save and reset target's pos and hpr (not scale)
		LPoint3f pos = target.get_pos();
		LVecBase3f hpr = target.get_hpr();
		target.set_pos_hpr(modelDeltaCenter, LVecBase3f::zero());
		//get geometry
		GeometryProcessor proc;
		proc.load(target, NodePath());
		for (int i = 0; i < proc.getVertCount(); i++)
		{
			float x = proc.getVerts()[i * 3];
			float y = proc.getVerts()[i * 3 + 1];
			float z = proc.getVerts()[i * 3 + 2];
			static_cast<btConvexHullShape*>(shape)->addPoint(
					LVecBase3_to_btVector3(LPoint3f(x, y, z)));
		}
		//restore target's pos and hpr (not scale)
		target.set_pos_hpr(pos, hpr);
	}
		break;
	case GIMPACT_SHAPE_PROXYTYPE:
	case TRIANGLE_MESH_SHAPE_PROXYTYPE:
	{
		//create the underlying tri mesh
		btTriangleMesh* mesh = new btTriangleMesh(true, true);
		//save and reset target's pos and hpr (not scale)
		LPoint3f pos = target.get_pos();
		LVecBase3f hpr = target.get_hpr();
		target.set_pos_hpr(modelDeltaCenter, LVecBase3f::zero());
		//get geometry
		GeometryProcessor proc;
		proc.load(target, NodePath());
		for (int i = 0; i < proc.getTriCount(); i++)
		{
			const int* tris = proc.getTris();
			const float* verts = proc.getVerts();
			//v0
			int v0I = tris[i * 3];
			LPoint3f v0(verts[v0I * 3], verts[v0I * 3 + 1], verts[v0I * 3 + 2]);
			//v1
			int v1I = tris[i * 3 + 1];
			LPoint3f v1(verts[v1I * 3], verts[v1I * 3 + 1], verts[v1I * 3 + 2]);
			//v2
			int v2I = tris[i * 3 + 2];
			LPoint3f v2(verts[v2I * 3], verts[v2I * 3 + 1], verts[v2I * 3 + 2]);
			//add triangle to mesh
			mesh->addTriangle(LVecBase3_to_btVector3(v0),
					LVecBase3_to_btVector3(v1), LVecBase3_to_btVector3(v2),
					true);
		}
		if (shapeType == GIMPACT_SHAPE_PROXYTYPE)
		{
			shape = new btGImpactMeshShape(mesh);
			static_cast<btGImpactMeshShape*>(shape)->updateBound();
		}
		else if (shapeType == TRIANGLE_MESH_SHAPE_PROXYTYPE)
		{
			shape = new btBvhTriangleMeshShape(mesh, true, true);
		}
		//restore target's pos and hpr (not scale)
		target.set_pos_hpr(pos, hpr);
		//store the underlying mesh
		physics.indexVertexArrays[shape] = mesh;
	}
		break;
	case TERRAIN_SHAPE_PROXYTYPE:
	{
		///NOTE: TERRAIN_SHAPE_PROXYTYPE is the only shape with local scaling applied
		btVector3 localScaling(1.0, 1.0, 1.0);
		btVector3 tmpS(1.0, 1.0, 1.0);
		if (upAxis == Panda3dUpAxis::X_up)
		{
			tmpS = LVecBase3_to_btVector3_abs(
					LVecBase3f(target.get_sy(), target.get_sz(),
							target.get_sx()));
		}
		else if (upAxis == Panda3dUpAxis::Y_up)
		{
			tmpS = LVecBase3_to_btVector3_abs(
					LVecBase3f(target.get_sz(), target.get_sx(),
							target.get_sy()));
		}
		else
		{
			tmpS = LVecBase3_to_btVector3_abs(
					LVecBase3f(target.get_sx(), target.get_sy(),
							target.get_sz()));
		}
		localScaling.setX(tmpS.getX());
		localScaling.setY(tmpS.getY());
		localScaling.setZ(tmpS.getZ());
		///
		PNMImage& hf = *reinterpret_cast<PNMImage*>(userData);
		ASSERT_TRUE(hf.get_color_type() == PNMImage::CT_grayscale)
		int xSize = hf.get_x_size();
		int ySize = hf.get_y_size();
		int hfSize = xSize * ySize;
		PRINT_DEBUG("terrain x size: " << xSize);
		PRINT_DEBUG("terrain y size: " << ySize);
		PRINT_DEBUG("terrain total size: " << hfSize);
		//store heights in column major order
		float minHeight = FLT_MAX;
		float maxHeight = FLT_MIN;
		float* heights = new float[hfSize];
		// xh = (xSize -1) - x
		// yh = (ySize -1) - y
		for (int y = 0; y < ySize; y++)
		{
			for (int x = 0; x < xSize; x++)
			{
				float xyValue = hf.get_gray(x, y);
				int xh = (xSize -1) - x;
				int yh = (ySize -1) - y;
				heights[yh * xSize + xh] = xyValue;
				if (xyValue < minHeight)
				{
					minHeight = xyValue;
				}
				if (xyValue > maxHeight)
				{
					maxHeight = xyValue;
				}
			}
		}
		///
		shape = new btHeightfieldTerrainShape(xSize, ySize, heights, 1.0, minHeight,
				maxHeight, (int) upAxis, PHY_FLOAT, false);
		shape->setLocalScaling(localScaling);
		//store the underlying height field data array
		physics.heightFieldDataArrays[shape] = heights;
	}
		break;
	case COMPOUND_SHAPE_PROXYTYPE:
	{
		shape = convexDecompose(target, physics, childShapeType);
	}
		break;
	default:
		break;
	}
	//
	physics.collisionShapes[shape] = 1;
	PRINT_DEBUG("createCollisionShapeForNode(): " << target.get_name()
			<< "; shape users = " << physics.collisionShapes[shape]);
	return shape;
}

void destroyCollisionShape(btCollisionShape* shape, Physics& physics)
{
	// first check shape
	if (!shape
			|| (physics.collisionShapes.find(shape)
					== physics.collisionShapes.end()))
	{
		return;
	}
	// (pre)decrement reference count and check > 0
	if (--physics.collisionShapes[shape] > 0)
	{
		PRINT_DEBUG(
				"destroyCollisionShape(); shape users = " << physics.collisionShapes[shape]);
		return;
	}
	// shape not used: to be deleted
	// pre deletion actions for special shape type cases
	BroadphaseNativeTypes shapeType =
			(BroadphaseNativeTypes) shape->getShapeType();
	switch (shapeType)
	{
	case GIMPACT_SHAPE_PROXYTYPE:
	case TRIANGLE_MESH_SHAPE_PROXYTYPE:
	{
		btTriangleIndexVertexArray* mesh = physics.indexVertexArrays[shape];
		physics.indexVertexArrays.erase(shape);
		delete mesh;
	}
		break;
	case TERRAIN_SHAPE_PROXYTYPE:
	{
		float* array = physics.heightFieldDataArrays[shape];
		physics.heightFieldDataArrays.erase(shape);
		delete[] array;
	}
		break;
	case COMPOUND_SHAPE_PROXYTYPE:
	{
		btCompoundShape* compShape = static_cast<btCompoundShape*>(shape);
		for (int i = 0; i < compShape->getNumChildShapes(); i++)
		{
			btCollisionShape* childShape = compShape->getChildShape(i);
			compShape->removeChildShapeByIndex(i);
			// recursively remove child shape
			destroyCollisionShape(childShape, physics);
		}
	}
		break;
	default:
		break;
	}
	// delete shape
	PRINT_DEBUG("destroyCollisionShape(); shape users = " << physics.collisionShapes[shape]);
	physics.collisionShapes.erase(shape);
	delete shape;
}

} //namespace bt3
