/**
 * \file characterController.cpp
 *
 * \date 2017-06-14
 * \author consultit
 */

#include "characterController.h"
#include "physics.h"
#include "collisionShape.h"

namespace bt3
{

void CharacterControllerManager::update(float dt)
{
	// update each character controller
	for (int cc = 0; cc < mPhysics.kinematicCharacterControllers.size(); cc++)
	{
		btKinematicCharacterController* characterController =
				mPhysics.kinematicCharacterControllers[cc];
		// update the related motion state
		btTransform trn = characterController->getGhostObject()->getWorldTransform();
		mCharacterMotionMap[characterController]->setWorldTransform(trn);
	}
}

btKinematicCharacterController* addCharacterController(const NodePath& ghostNP,
		const CharacterControllerData& chData)
{
	if (ghostNP.is_empty())
	{
		return nullptr;
	}

	CharacterControllerManager& manager =
			CharacterControllerManager::GetSingleton();

	// get ghost if any
	btGhostObject* ghost = btGhostObject::upcast(
			getPhysicsObjectOfNode(ghostNP, manager.getPhysics()));
	if (!ghost)
	{
		return nullptr;
	}

	// check convex shape
	btConvexShape* convexShape = upcastConvexShape(ghost->getCollisionShape());
	if (!convexShape)
	{
		return nullptr;
	}

	// set ghost collision's flags
	ghost->setCollisionFlags(btCollisionObject::CF_CHARACTER_OBJECT);

	/// HACK: save ghost's world transform because btKinematicCharacterController's
	/// constructor will change it.
	btTransform worldTrans  = ghost->getWorldTransform();

	// create characterController
	btKinematicCharacterController* characterController =
			new btKinematicCharacterController(
					static_cast<btPairCachingGhostObject*>(ghost), convexShape,
					chData.stepHeight, LVecBase3_to_btVector3(chData.up));

	/// HACK: restore ghost's world transform
	ghost->setWorldTransform(worldTrans);

	// set initial parameters
	characterController->setGravity(
			-LVecBase3_to_btVector3(chData.up) * chData.gravity);
	characterController->setLinearDamping(chData.linearDamping);
	characterController->setAngularDamping(chData.angularDamping);
	characterController->setFallSpeed(chData.fallSpeed);
	characterController->setJumpSpeed(chData.jumpSpeed);
	characterController->setMaxSlope(chData.maxSlope);
	characterController->setMaxPenetrationDepth(chData.maxPenetrationDepth);
	characterController->setMaxJumpHeight(chData.maxJumpHeight);

	// create a motion state for the ghostNP graphic object and
	DynamicMotionState* motionState = makeMotionState(ghostNP);
	// store its reference into the manager's map indexed by characterController
	manager.getCharacterMotionMap().insert(make_pair(characterController, motionState));

	// add characterController to world
	manager.getPhysics().dynamicsWorld->addAction(characterController);
	manager.getPhysics().kinematicCharacterControllers.push_back(
			characterController);

	// initial reset
	manager.getPhysics().dynamicsWorld->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(
			ghost->getBroadphaseHandle(),
			manager.getPhysics().dynamicsWorld->getDispatcher());
	characterController->reset(manager.getPhysics().dynamicsWorld);

	PRINT_DEBUG("addCharacterController(): ghost " << ghostNP.get_name());
	//
	return characterController;
}

bool removeCharacterController(
		btKinematicCharacterController* characterController)
{
	if (!characterController)
	{
		return false;
	}

	// find the characterController
	CharacterControllerManager& manager =
			CharacterControllerManager::GetSingleton();
	auto& characterControllers =
			manager.getPhysics().kinematicCharacterControllers;
	int index = characterControllers.findLinearSearch2(characterController);
	if (index == -1)
	{
		return false;
	}

	btPairCachingGhostObject* ghost =
			characterControllers[index]->getGhostObject();
	NodePath ghostNP = getNodeFromPhysicsObject(ghost);

	// reset ghost collision's flags
	ghost->setCollisionFlags(
			ghost->getCollisionFlags()
					& (~btCollisionObject::CF_CHARACTER_OBJECT));

	// delete the motion state and its reference from the manager's map
	delete manager.getCharacterMotionMap()[characterController];
	manager.getCharacterMotionMap().erase(characterController);

	// remove characterController from world and delete it
	manager.getPhysics().dynamicsWorld->removeAction(characterController);
	characterControllers.remove(characterController);
	delete characterController;

	//report
	if (!ghostNP.is_empty())
	{
		PRINT_DEBUG(
				"removeCharacterController(): ghost " << ghostNP.get_name());
	}
	else
	{
		PRINT_DEBUG("removeCharacterController(): no graphic node");
	}
	//
	return true;
}

} //namespace bt3

