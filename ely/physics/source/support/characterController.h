/**
 * \file characterController.h
 *
 * \date 2017-06-14
 * \author consultit
 */
#ifndef CHARACTERCONTROLLER_H_
#define CHARACTERCONTROLLER_H_

#include "common.h"
#include "physics.h"
#include <unordered_map>

namespace bt3
{

struct physics;
//character controller
struct CharacterControllerData
{
	CharacterControllerData() :
			up{ LVector3f(0.0, 0.0, 1.0) },
			gravity{29.4},// == 3G
			stepHeight{1.0},
			linearDamping{0.0},
			angularDamping{0.0},
			fallSpeed{55.0},
			jumpSpeed{10.0},
			maxSlope{45.0},
			maxPenetrationDepth{0.2},
			maxJumpHeight{1.0}
	{
	}
	LVector3f up;
	float gravity, stepHeight;
	float linearDamping, angularDamping, fallSpeed, jumpSpeed,
	maxSlope/*in degrees*/, maxPenetrationDepth, maxJumpHeight;
};
class CharacterControllerManager: public Singleton<CharacterControllerManager>,
		public TypedReferenceCount
{
public:
	CharacterControllerManager(Physics& physics) :
			mPhysics
			{ physics }
	{
		mCharacterMotionMap.clear();
	}
	virtual ~CharacterControllerManager()
	{
	}
	virtual void update(float dt);
	Physics& getPhysics()
	{
		return mPhysics;
	}
	unordered_map<btKinematicCharacterController*, DynamicMotionState*>& getCharacterMotionMap()
	{
		return mCharacterMotionMap;
	}

private:
	Physics& mPhysics;
	unordered_map<btKinematicCharacterController*, DynamicMotionState*> mCharacterMotionMap;
};

btKinematicCharacterController* addCharacterController(const NodePath& ghostNP,
		const CharacterControllerData& chData = CharacterControllerData());
bool removeCharacterController(
		btKinematicCharacterController* characterController);

}

#endif /* CHARACTERCONTROLLER_H_ */
