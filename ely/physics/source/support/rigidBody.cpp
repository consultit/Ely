/**
 * \file rigidBody.cpp
 *
 * \date 2017-03-07
 * \author consultit
 */

#include "rigidBody.h"
#include "physics.h"
#include "collisionShape.h"

namespace
{
btRigidBody* createRigidBodyForNode(const NodePath& target, btCollisionShape* shape,
		btScalar mass, const bt3::RigidBodyData& rbData)
{
	ASSERT_TRUE(shape && (shape->getShapeType() != INVALID_SHAPE_PROXYTYPE))

	// make a dynamic motion state
	bt3::DynamicMotionState* motionState = bt3::makeMotionState(target);

	// rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.0);
	btVector3 localInertia(0.0, 0.0, 0.0);
	if (isDynamic)
		shape->calculateLocalInertia(mass, localInertia);

	// setup construction parameters and create a rigid body
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, shape,
			localInertia);
	rbInfo.m_linearDamping = rbData.linearDamping;
	rbInfo.m_angularDamping = rbData.angularDamping;
	rbInfo.m_friction = rbData.friction;
	rbInfo.m_rollingFriction = rbData.rollingFriction;
	rbInfo.m_spinningFriction = rbData.spinningFriction;
	rbInfo.m_restitution = rbData.restitution;
	rbInfo.m_linearSleepingThreshold = rbData.linearSleepingThreshold;
	rbInfo.m_angularSleepingThreshold = rbData.angularSleepingThreshold;
	rbInfo.m_additionalDamping = rbData.additionalDamping;
	rbInfo.m_additionalDampingFactor = rbData.additionalDampingFactor;
	rbInfo.m_additionalLinearDampingThresholdSqr =
			rbData.additionalLinearDampingThresholdSqr;
	rbInfo.m_additionalAngularDampingThresholdSqr =
			rbData.additionalAngularDampingThresholdSqr;
	rbInfo.m_additionalAngularDampingFactor =
			rbData.additionalAngularDampingFactor;
	btRigidBody* rigidBody = new btRigidBody(rbInfo);

	// set target's PandaNode as rigid body's user pointer
	rigidBody->setUserPointer((void*) target.node());

	// return the just created rigid body
	return rigidBody;
}
} //anonymous namespace

namespace bt3
{
void addRigidBodyToNode(NodePath& target, Physics& physics, float mass,
		const RigidBodyData& rbData, btCollisionShape* sharedShape,
		BroadphaseNativeTypes shapeType, Panda3dUpAxis upAxis,
		BitMask32 groupMask, BitMask32 collideMask, void* shapeData,
		BroadphaseNativeTypes childShapeType)
{
	if (target.is_empty())
	{
		return;
	}
	// get a collision shape
	btCollisionShape* shape = createCollisionShapeForNode(target, physics,
			shapeType, upAxis, shapeData, childShapeType, sharedShape);
	// add a rigid body to node
	btRigidBody* body = createRigidBodyForNode(target, shape, mass, rbData);
	// add the rigid body to the dynamics world
	physics.dynamicsWorld->addRigidBody(body, groupMask.get_word(),
			collideMask.get_word());
	//add the "physics" tag to target node
	if (!target.has_tag("physics"))
	{
		target.set_tag("physics", "yes");
	}
	PRINT_DEBUG("addRigidBodyToNode(): " << target.get_name());
}

bool removeRigidBodyFromNode(NodePath& target, Physics& physics)
{
	bool result = false;
	if (target.is_empty())
	{
		return result;
	}

	//delete the rigid body from the dynamics world and delete it
	for (int i = 0; i < physics.dynamicsWorld->getNumCollisionObjects(); i++)
	{
		btCollisionObject* obj =
				physics.dynamicsWorld->getCollisionObjectArray()[i];
		if (reinterpret_cast<PandaNode*>(obj->getUserPointer())
				== target.node())
		{
			//check if it is a rigid body
			btRigidBody* body = btRigidBody::upcast(obj);
			if (body)
			{
				if (!body->isActive())
				{
					///HACK: activate contacting rigid bodies if sleeping
					int numManifolds =
							physics.dynamicsWorld->getDispatcher()->getNumManifolds();
					for (int i = 0; i < numManifolds; i++)
					{
						btPersistentManifold* contactManifold =
								physics.dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(
										i);
						const btRigidBody* obA = btRigidBody::upcast(
								contactManifold->getBody0());
						const btRigidBody* obB = btRigidBody::upcast(
								contactManifold->getBody1());
						if (obA && obB)
						{
							if ((obA == obj) && !obB->isActive())
							{
								obB->activate();
							}
							else if ((obB == obj) && !obA->isActive())
							{
								obA->activate();
							}
						}
					}
				}
				// remove all constraints from the body
				for (auto constraint: getNodeConstraints(target, physics))
				{
					physics.dynamicsWorld->removeConstraint(constraint);
				}
				//remove motion state
				if (body->getMotionState())
				{
					delete body->getMotionState();
				}
				//try to destroy collision shape
				destroyCollisionShape(body->getCollisionShape(), physics);
				//complete body removal
				physics.dynamicsWorld->removeRigidBody(body);
				delete body;
				//remove the "physics" tag from target node
				if (target.has_tag("physics"))
				{
					target.clear_tag("physics");
				}
				//removed: exit loop
				result = true;
				break;
			}
		}
	}
	PRINT_DEBUG(
			(result ? "" : "not ") << "removeRigidBodyFromNode(): " << target.get_name());
	//
	return result;
}

void switchRigidBodyTypeOfNode(const NodePath& target, const Physics& physics, RigidBodyType bodyType, float mass)
{
	btCollisionObject* colObj = getPhysicsObjectOfNode(target, physics);
	if (!colObj)
	{
		return;
	}
	btRigidBody* body = btRigidBody::upcast(colObj);
	if (!body)
	{
		return;
	}

	btVector3 localInertia(0.0, 0.0, 0.0);
	//preliminary checks
	if ((bodyType == RigidBodyType::DYNAMIC) && (mass == 0.0))
	{
		return;
	}
	if (((bodyType == RigidBodyType::STATIC)
			|| (bodyType == RigidBodyType::KINEMATIC)) && (mass != 0.0))
	{
		return;
	}
	// backup original body filters (i.e. group/collide masks)
	int filterGroupOrig = body->getBroadphaseHandle()->m_collisionFilterGroup;
	int filterMaskOrig = body->getBroadphaseHandle()->m_collisionFilterMask;
	switch (bodyType)
	{
	case RigidBodyType::DYNAMIC:
	case RigidBodyType::STATIC:
	{
		//remove body from dynamics world
		physics.dynamicsWorld->removeRigidBody(body);
		//reset collision object data
		body->setCollisionFlags(0);
		body->forceActivationState(ACTIVE_TAG);
		body->setDeactivationTime(0.0);
		//reset rigid body data
		body->getCollisionShape()->calculateLocalInertia(mass, localInertia);
		body->setMassProps(mass, localInertia);
		//re-add body to dynamics world
		physics.dynamicsWorld->addRigidBody(body);
		PRINT_DEBUG(
				(bodyType == RigidBodyType::DYNAMIC ? "DYNAMIC " : "STATIC ") << "switchRigidBodyTypeOfNode(): " << target.get_name());
	}
		break;
	case RigidBodyType::KINEMATIC:
	{
		//remove body from dynamics world
		physics.dynamicsWorld->removeRigidBody(body);
		//reset collision object data
		body->setCollisionFlags(0);
		body->setActivationState(DISABLE_DEACTIVATION);
		body->setDeactivationTime(0.0);
		//reset rigid body data
		body->getCollisionShape()->calculateLocalInertia(mass, localInertia);
		body->setMassProps(mass, localInertia);
		//set kinematic flag (as explained in docs)
		body->setCollisionFlags(
				body->getCollisionFlags()
						| btCollisionObject::CF_KINEMATIC_OBJECT);
		//re-add body to dynamics world
		physics.dynamicsWorld->addRigidBody(body);
		PRINT_DEBUG(
				"KINEMATIC switchRigidBodyTypeOfNode(): " << target.get_name());
	}
		break;
	default:
		break;
	}
	// restore original body filters (i.e. group/collide masks)
	body->getBroadphaseHandle()->m_collisionFilterGroup = filterGroupOrig;
	body->getBroadphaseHandle()->m_collisionFilterMask = filterMaskOrig;
}

vector<btTypedConstraint*> getNodeConstraints(const NodePath& target,
		const Physics& physics)
{
	vector<btTypedConstraint*> result;
	if (target.is_empty())
	{
		return result;
	}

	// check if target has a rigid body
	btCollisionObject* obj = getPhysicsObjectOfNode(target, physics);
	if (obj)
	{
		btRigidBody* body = btRigidBody::upcast(obj);
		if (body)
		{
			// get all constraints
			for (int i = 0; i < physics.dynamicsWorld->getNumConstraints(); i++)
			{
				// check if one of the constraint's objects is body
				btTypedConstraint* constraint =
						physics.dynamicsWorld->getConstraint(i);
				if ((&constraint->getRigidBodyA() == body)
						|| (&constraint->getRigidBodyB() == body))
				{
					// add constraint to the list
					result.push_back(constraint);
				}
			}
		}
	}
	//
	return result;
}

} //namespace bt3
