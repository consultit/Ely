/**
 * \file debugDrawing.h
 *
 * \date 2016-12-10
 * \author consultit
 */

#ifndef DEBUGDRAWING_H_
#define DEBUGDRAWING_H_

#include "common.h"
#include <nodePath.h>
#include <geomNode.h>
#include "../bullet3/src/LinearMath/btIDebugDraw.h"

namespace bt3
{
// from pandabullet
struct Line
{
	LVecBase3 _p0;
	LVecBase3 _p1;
	UnalignedLVecBase4 _color;
};

class DebugDrawer: public btIDebugDraw
{

public:
	DebugDrawer(const NodePath& render) :
			_geomNode(new GeomNode("DebugDraw")), _geom(nullptr), _normals(false), _mode(
					0)
	{
		render.attach_new_node(_geomNode);
	}

	virtual ~DebugDrawer()
	{
		NodePath::any_path(_geomNode).remove_node();
	}

	void initialize()
	{
		clear();
	}

	void finalize()
	{
		doDebugDraw();
	}

	void clear()
	{
		_lines.clear();
		_geomNode->remove_all_geoms();
		_geom.clear();
	}

	virtual void setDebugMode(int mode)
	{
		_mode = mode;
	}

	virtual int getDebugMode() const
	{
		return _mode;
	}

	virtual void reportErrorWarning(const char *warning)
	{
		cerr << warning << endl;
	}

	virtual void drawLine(const btVector3 &from, const btVector3 &to,
			const btVector3 &color);
	virtual void drawContactPoint(const btVector3 &point,
			const btVector3 &normal, btScalar distance, int lifetime,
			const btVector3 &color);
	virtual void draw3dText(const btVector3 &location, const char *text);

	void show_normals(bool show)
	{
		_normals = show;
	}

private:
	PT(GeomNode)_geomNode;
	PT(Geom) _geom;
	pvector<Line> _lines;

	bool _normals;
	int _mode;

	void doDebugDraw();
};

} //namespace bt3

#endif /* DEBUGDRAWING_H_ */
