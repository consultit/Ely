/**
 * \file rigidBody.h
 *
 * \date 2017-03-07
 * \author consultit
 */
#ifndef RIGIDBODY_H_
#define RIGIDBODY_H_

#include "common.h"
#include "../bullet3/src/BulletCollision/BroadphaseCollision/btBroadphaseProxy.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btTypedConstraint.h"

namespace bt3
{

struct physics;
//rigid body
enum class RigidBodyType: unsigned char
{
	DYNAMIC,
	STATIC,
	KINEMATIC,
	INVALID_TYPE
};
struct RigidBodyData
{
	RigidBodyData():
		linearDamping{0.0},
		angularDamping{0.0},
		friction{0.5},
		rollingFriction{0.0},
		spinningFriction{0.0},
		restitution{0.0},
		linearSleepingThreshold{0.8},
		angularSleepingThreshold{1.0},
		additionalDamping{false},
		additionalDampingFactor{0.005},
		additionalLinearDampingThresholdSqr{0.01},
		additionalAngularDampingThresholdSqr{0.01},
		additionalAngularDampingFactor{0.01}
	{
	}
	float linearDamping;
	float angularDamping;
	float friction;
	float rollingFriction;
	float spinningFriction;
	float restitution;
	float linearSleepingThreshold;
	float angularSleepingThreshold;
	bool additionalDamping;
	float additionalDampingFactor;
	float additionalLinearDampingThresholdSqr;
	float additionalAngularDampingThresholdSqr;
	float additionalAngularDampingFactor;
};
void addRigidBodyToNode(NodePath& target, Physics& physics, float mass,
		const RigidBodyData& rbData = RigidBodyData(),
		btCollisionShape* sharedShape = nullptr,
		BroadphaseNativeTypes shapeType = INVALID_SHAPE_PROXYTYPE,
		Panda3dUpAxis upAxis = Panda3dUpAxis::Z_up, BitMask32 groupMask =
				BitMask32::all_on(), BitMask32 collideMask =
				BitMask32::all_on(), void* shapeData = nullptr,
		BroadphaseNativeTypes childShapeType = INVALID_SHAPE_PROXYTYPE);
bool removeRigidBodyFromNode(NodePath& target, Physics& physics);
void switchRigidBodyTypeOfNode(const NodePath& target, const Physics& physics,
		RigidBodyType bodyType, float mass);
vector<btTypedConstraint*> getNodeConstraints(const NodePath& target,
		const Physics& physics);

}

#endif /* RIGIDBODY_H_ */
