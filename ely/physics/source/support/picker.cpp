/**
 * \file picker.cpp
 *
 * \date 2017-03-07
 * \author consultit
 */

#include "picker.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btPoint2PointConstraint.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btGeneric6DofConstraint.h"
#include "../bullet3/src/BulletCollision/NarrowPhaseCollision/btRaycastCallback.h"
#include <mouseWatcher.h>
#include <asyncTaskManager.h>

// Picker
#ifdef ELY_DEBUG
namespace
{
void print_debug_hit(const btCollisionWorld::ClosestRayResultCallback &callback)
{
	cout << "panda node: "
			<< static_cast<PandaNode*>(callback.m_collisionObject->getUserPointer())->get_name()
			<< "| hit pos: " << "("
			<< bt3::btVector3_to_LVecBase3(callback.m_hitPointWorld) << ")"
			<< "| hit normal: " << "("
			<< bt3::btVector3_to_LVecBase3(callback.m_hitNormalWorld) << ")"
			<< "| hit fraction: " << "(" << callback.m_closestHitFraction << ")"
			<< "| from pos: " << "("
			<< bt3::btVector3_to_LVecBase3(callback.m_rayFromWorld) << ")"
			<< "| to pos: " << "("
			<< bt3::btVector3_to_LVecBase3(callback.m_rayToWorld) << ")"
			<< endl;
}
}
#endif

namespace bt3
{

Picker::Picker(const NodePath &render, const NodePath &camera,
		MouseWatcher *watcher, const Physics &physics, const string &pickKeyOn,
		const string &pickKeyOff) :
		mRender(render), mCamera(camera), mMouseWatcher(watcher), mDynamicsWorld(
				physics.dynamicsWorld), mSoftWorld(false), mGroupMask(
				btBroadphaseProxy::AllFilter), mCollideMask(
				btBroadphaseProxy::AllFilter), mPickedConstraint(nullptr), mConstraintType(
				POINT2POINT_CONSTRAINT_TYPE), mCfm(0.5), mErp(0.5), mSoftNode(
				nullptr), mSoftDrag(false), mPickedBody(nullptr), mSavedState(
				0), mOldPickingPos(btVector3(0.0, 0.0, 0.0)), mHitPos(
				btVector3(0.0, 0.0, 0.0)), mOldPickingDist(0)

{
	//get camera lens
	mCamLens = DCAST(Camera, mCamera.node())->get_lens();
	// setup event callback for picking body
	mPickKeyOn = pickKeyOn;
	mPickKeyOff = pickKeyOff;
	mPickBodyData = new EventCallbackInterface<Picker>::EventCallbackData(this,
			&Picker::pickBody);
	EventHandler::get_global_event_handler()->add_hook(mPickKeyOn,
			&EventCallbackInterface<Picker>::eventCallbackFunction,
			reinterpret_cast<void*>(mPickBodyData.p()));
	EventHandler::get_global_event_handler()->add_hook(mPickKeyOff,
			&EventCallbackInterface<Picker>::eventCallbackFunction,
			reinterpret_cast<void*>(mPickBodyData.p()));
	//soft body
	if (physics.physicsInfo.worldType == PhysicsInfo::WorldType::SoftRigid)
	{
		mSoftWorld = true;
	}
	mSoftResults.fraction = 1.f;
	//ray cast data
	mHitCastPos = LPoint3f();
	mHitCastNormal = LVector3f();
	mHitCastFraction = 0.0;
	mHitCastFromPos = LPoint3f();
	mHitCastToPos = LPoint3f();
	//update task stuff
	mUpdateData.clear();
	mUpdateTask.clear();
	//create the task for updating picked body motion
	mUpdateData = new TaskInterface<Picker>::TaskData(this,
			&Picker::movePickedBody);
	//create pick body motion update task
	mUpdateTask = new GenericAsyncTask("Picker::movePickedBody",
			&TaskInterface<Picker>::taskFunction,
			reinterpret_cast<void*>(mUpdateData.p()));
	//set sort/priority
	mUpdateTask->set_sort(0);
	mUpdateTask->set_priority(0);
}

Picker::~Picker()
{
	// remove event callback for picking body
	EventHandler::get_global_event_handler()->remove_hooks_with(
			reinterpret_cast<void*>(mPickBodyData.p()));
	//remove and destroy pick body motion update task
	if (mUpdateTask)
	{
		AsyncTaskManager::get_global_ptr()->remove(mUpdateTask);
	}
	mUpdateTask.clear();
}

void Picker::pickBody(const Event *event)
{
	// handle body picking
	if (event->get_name() == mPickKeyOn)
	{
		// check mouse position
		if (mMouseWatcher->has_mouse())
		{
			// Get to and from pos in camera coordinates
			LPoint2f pMouse = mMouseWatcher->get_mouse();
			//
			LPoint3f pFrom, pTo;
			if (mCamLens->extrude(pMouse, pFrom, pTo))
			{
				//Transform to global coordinates
				const btVector3 &rayFromWorld = LVecBase3_to_btVector3(
						mRender.get_relative_point(mCamera, pFrom));
				const btVector3 &rayToWorld = LVecBase3_to_btVector3(
						mRender.get_relative_point(mCamera, pTo));
				//cast a ray to detect a body
				btCollisionWorld::ClosestRayResultCallback rayCallback(
						rayFromWorld, rayToWorld);
				rayCallback.m_flags |= btTriangleRaycastCallback::kF_UseGjkConvexCastRaytest;
				//hit all objects
				rayCallback.m_collisionFilterGroup = mGroupMask;
				rayCallback.m_collisionFilterMask = mCollideMask;
				mDynamicsWorld->rayTest(rayFromWorld, rayToWorld, rayCallback);
				//
				if (rayCallback.hasHit())
				{
					btVector3 pickPos = rayCallback.m_hitPointWorld;
					// check if rigid body
					btRigidBody *body = (btRigidBody*) btRigidBody::upcast(
							rayCallback.m_collisionObject);
					if (body)
					{
						//other exclusions?
						if (!(body->isStaticObject()
								|| body->isKinematicObject()))
						{
							//set body as active and not deactivable
							mPickedBody = body;
							mSavedState = mPickedBody->getActivationState();
							mPickedBody->setActivationState(
							DISABLE_DEACTIVATION);
							btVector3 localPivot =
									body->getCenterOfMassTransform().inverse()
											* pickPos;
							switch (mConstraintType)
							{
							case POINT2POINT_CONSTRAINT_TYPE:
							{
								//point to point
								btPoint2PointConstraint *p2p =
										new btPoint2PointConstraint(*body,
												localPivot);
								mPickedConstraint = p2p;
								btScalar mousePickClamping = 30.f;
								p2p->m_setting.m_impulseClamp =
										mousePickClamping;
								//very weak constraint for picking
								p2p->m_setting.m_tau = 0.001f;

							}
								break;
							case D6_CONSTRAINT_TYPE:
							{
								//generic
								btTransform trans(btTransform::getIdentity());
								trans.setOrigin(localPivot);
								btGeneric6DofConstraint *dof6 =
										new btGeneric6DofConstraint(
												*mPickedBody, trans, true);
								mPickedConstraint = dof6;
								//set parameters
								dof6->setAngularLowerLimit(btVector3(0, 0, 0));
								dof6->setAngularUpperLimit(btVector3(0, 0, 0));
								// define the 'strength' of our constraint (each axis)
								dof6->setParam(BT_CONSTRAINT_STOP_CFM, mCfm, 0);
								dof6->setParam(BT_CONSTRAINT_STOP_CFM, mCfm, 1);
								dof6->setParam(BT_CONSTRAINT_STOP_CFM, mCfm, 2);
								dof6->setParam(BT_CONSTRAINT_STOP_CFM, mCfm, 3);
								dof6->setParam(BT_CONSTRAINT_STOP_CFM, mCfm, 4);
								dof6->setParam(BT_CONSTRAINT_STOP_CFM, mCfm, 5);
								// define the 'error reduction' of our constraint (each axis)
								dof6->setParam(BT_CONSTRAINT_STOP_ERP, mErp, 0);
								dof6->setParam(BT_CONSTRAINT_STOP_ERP, mErp, 1);
								dof6->setParam(BT_CONSTRAINT_STOP_ERP, mErp, 2);
								dof6->setParam(BT_CONSTRAINT_STOP_ERP, mErp, 3);
								dof6->setParam(BT_CONSTRAINT_STOP_ERP, mErp, 4);
								dof6->setParam(BT_CONSTRAINT_STOP_ERP, mErp, 5);
							}
								break;
							default:
								break;
							}
							//and attach it to the world
							mDynamicsWorld->addConstraint(mPickedConstraint,
									true);
							//add pick body motion update task
							AsyncTaskManager::get_global_ptr()->add(
									mUpdateTask);
						}
					}
					else if (mSoftWorld)
					{
						// check if soft body
						btSoftBody *sBody = (btSoftBody*) btSoftBody::upcast(
								rayCallback.m_collisionObject);
						if (sBody)
						{
							mSoftResults.fraction = 1.f;
							btSoftBodyArray &sbs =
									static_cast<btSoftRigidDynamicsWorld*>(mDynamicsWorld)->getSoftBodyArray();
							for (int ib = 0; ib < sbs.size(); ++ib)
							{
								btSoftBody *psb = sbs[ib];
								btSoftBody::sRayCast res;
								if (psb->rayTest(rayFromWorld, rayToWorld, res))
								{
									mSoftResults = res;
								}
							}
							if (mSoftResults.fraction < 1.f)
							{
								mSoftImpact = rayFromWorld
										+ (rayToWorld - rayFromWorld)
												* mSoftResults.fraction;
								mSoftDrag = true;
								mSoftNode = 0;
								switch (mSoftResults.feature)
								{
								case btSoftBody::eFeature::Tetra:
								{
									btSoftBody::Tetra &tet =
											mSoftResults.body->m_tetras[mSoftResults.index];
									mSoftNode = tet.m_n[0];
									for (int i = 1; i < 4; ++i)
									{
										if ((mSoftNode->m_x - mSoftImpact).length2()
												> (tet.m_n[i]->m_x - mSoftImpact).length2())
										{
											mSoftNode = tet.m_n[i];
										}
									}
									break;
								}
								case btSoftBody::eFeature::Face:
								{
									btSoftBody::Face &f =
											mSoftResults.body->m_faces[mSoftResults.index];
									mSoftNode = f.m_n[0];
									for (int i = 1; i < 3; ++i)
									{
										if ((mSoftNode->m_x - mSoftImpact).length2()
												> (f.m_n[i]->m_x - mSoftImpact).length2())
										{
											mSoftNode = f.m_n[i];
										}
									}
								}
									break;
								default:
									break;
								}
								if (mSoftNode)
								{
									mSoftGoal = mSoftNode->m_x;
								}
								//add pick body motion update task
								AsyncTaskManager::get_global_ptr()->add(
										mUpdateTask);
							}
						}
					}
					mOldPickingPos = rayToWorld;
					mHitPos = pickPos;
					mOldPickingDist = (pickPos - rayFromWorld).length();
#ifdef ELY_DEBUG
					//print hit body
					cout << "Picker::pickBody(): ";
					print_debug_hit(rayCallback);
#endif
				}
			}
		}
	}
	else
	{
		if (mPickedConstraint)
		{
			//remove pick body motion update task
			AsyncTaskManager::get_global_ptr()->remove(mUpdateTask);
			mPickedBody->forceActivationState(mSavedState);
			mPickedBody->activate();
			mDynamicsWorld->removeConstraint(mPickedConstraint);
			delete mPickedConstraint;
			mPickedConstraint = nullptr;
			mPickedBody = nullptr;
		}
		else if (mSoftDrag)
		{
			//unset callback
			AsyncTaskManager::get_global_ptr()->remove(mUpdateTask);
			mSoftResults.fraction = 1.f;
			mSoftDrag = false;
		}
	}
}

AsyncTask::DoneStatus Picker::movePickedBody(GenericAsyncTask *task)
{
	float timeStep = ClockObject::get_global_clock()->get_dt();
	// handle picked body if any
	if (mPickedBody && mPickedConstraint)
	{
		// check mouse position
		if (mMouseWatcher->has_mouse())
		{
			// Get to and from pos in camera coordinates
			LPoint2f pMouse = mMouseWatcher->get_mouse();
			//
			LPoint3f pNear, pFar;
			if (mCamLens->extrude(pMouse, pNear, pFar))
			{
				// Transform to global coordinates
				pNear = mRender.get_relative_point(mCamera, pNear);
				pFar = mRender.get_relative_point(mCamera, pFar);
				const btVector3 &rayFromWorld = LVecBase3_to_btVector3(pNear);
				const btVector3 &rayToWorld = LVecBase3_to_btVector3(pFar);

				//keep it at the same picking distance
				btVector3 newPivotB;
				btVector3 dir = rayToWorld - rayFromWorld;
				dir.normalize();
				dir *= mOldPickingDist;
				newPivotB = rayFromWorld + dir;
				switch (mConstraintType)
				{
				case POINT2POINT_CONSTRAINT_TYPE:
					static_cast<btPoint2PointConstraint*>(mPickedConstraint)->setPivotB(
							newPivotB);
					break;
				case D6_CONSTRAINT_TYPE:
					static_cast<btGeneric6DofConstraint*>(mPickedConstraint)->getFrameOffsetA().setOrigin(
							newPivotB);
					break;
				default:
					break;
				}
			}
		}
	}
	else if (mSoftDrag)
	{
		// check mouse position
		if (mMouseWatcher->has_mouse())
		{
			// Get to and from pos in camera coordinates
			LPoint2f pMouse = mMouseWatcher->get_mouse();
			//
			LPoint3f pNear, pFar;
			if (mCamLens->extrude(pMouse, pNear, pFar))
			{
				// Transform to global coordinates
				pFar = mRender.get_relative_point(mCamera, pFar);

				mSoftNode->m_v *= 0;
				// new pos
				const btVector3 rayFrom = LVecBase3_to_btVector3(
						mCamera.get_pos(mRender));
				const btVector3 rayTo = LVecBase3_to_btVector3(pFar);
				const btVector3 rayDir = (rayTo - rayFrom).normalized();
				const btVector3 N = LVecBase3_to_btVector3(
						mRender.get_relative_vector(mCamera,
								-LVector3f::forward()));
				const btScalar O = btDot(mSoftImpact, N);
				const btScalar den = btDot(N, rayDir);
				if ((den * den) > 0)
				{
					const btScalar num = O - btDot(N, rayFrom);
					const btScalar hit = num / den;
					if ((hit > 0) && (hit < 1500))
					{
						mSoftGoal = rayFrom + rayDir * hit;
					}
				}
				btVector3 delta = mSoftGoal - mSoftNode->m_x;
				static const btScalar maxdrag = 10;
				if (delta.length2() > (maxdrag * maxdrag))
				{
					delta = delta.normalized() * maxdrag;
				}
				mSoftNode->m_v += delta / timeStep;
			}
		}
	}
	//
	return AsyncTask::DS_cont;
}

const btCollisionObject* Picker::rayCast()
{
	// handle ray casting
	// check mouse position
	if (mMouseWatcher->has_mouse())
	{
		// Get to and from pos in camera coordinates
		LPoint2f pMouse = mMouseWatcher->get_mouse();
		//
		LPoint3f pFrom, pTo;
		if (mCamLens->extrude(pMouse, pFrom, pTo))
		{
			//Transform to global coordinates
			const btVector3 &rayFromWorld = LVecBase3_to_btVector3(
					mRender.get_relative_point(mCamera, pFrom));
			const btVector3 &rayToWorld = LVecBase3_to_btVector3(
					mRender.get_relative_point(mCamera, pTo));
			//cast a ray to detect a body
			btCollisionWorld::ClosestRayResultCallback rayCallback(rayFromWorld,
					rayToWorld);
			//hit all objects
			rayCallback.m_collisionFilterGroup = mGroupMask;
			rayCallback.m_collisionFilterMask = mCollideMask;
			mDynamicsWorld->rayTest(rayFromWorld, rayToWorld, rayCallback);
			//
			if (rayCallback.hasHit())
			{
#ifdef ELY_DEBUG
				//print hit body
				cout << "Picker::rayCast(): ";
				print_debug_hit(rayCallback);
#endif
				mHitCastPos = bt3::btVector3_to_LVecBase3(
						rayCallback.m_hitPointWorld);
				mHitCastNormal = bt3::btVector3_to_LVecBase3(
						rayCallback.m_hitNormalWorld);
				mHitCastFraction = rayCallback.m_closestHitFraction;
				mHitCastFromPos = bt3::btVector3_to_LVecBase3(
						rayCallback.m_rayFromWorld);
				mHitCastToPos = bt3::btVector3_to_LVecBase3(
						rayCallback.m_rayToWorld);
				return rayCallback.m_collisionObject;
			}
		}
	}
	return nullptr;
}

}
