/*
 Bullet Continuous Collision Detection and Physics Library
 Copyright (c) 2003-2007 Erwin Coumans  http://continuousphysics.com/Bullet/

 This software is provided 'as-is', without any express or implied warranty.
 In no event will the authors be held liable for any damages arising from the use of this software.
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it freely,
 subject to the following restrictions:

 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.
 */

///-----includes_start-----
#include "physics.h"
#include "../bullet3/src/BulletSoftBody/btSoftBodyRigidBodyCollisionConfiguration.h"
#include "../bullet3/src/BulletDynamics/Featherstone/btMultiBodyDynamicsWorld.h"
#include "../bullet3/src/BulletDynamics/Dynamics/btSimpleDynamicsWorld.h"
#include "../bullet3/src/BulletSoftBody/btSoftMultiBodyDynamicsWorld.h"
#include "../bullet3/src/BulletDynamics/Dynamics/btDiscreteDynamicsWorldMt.h"
#include "../bullet3/src/BulletCollision/BroadphaseCollision/btAxisSweep3.h"
#include "../bullet3/src/BulletCollision/BroadphaseCollision/btSimpleBroadphase.h"
#include "../bullet3/src/BulletCollision/CollisionDispatch/btCollisionDispatcherMt.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolverMt.h"
#include "../bullet3/src/BulletDynamics/Featherstone/btMultiBodyConstraintSolver.h"
#include "../bullet3/src/BulletDynamics/ConstraintSolver/btNNCGConstraintSolver.h"
#include "../bullet3/src/BulletDynamics/MLCPSolvers/btMLCPSolverInterface.h"
#include "../bullet3/src/BulletDynamics/MLCPSolvers/btDantzigSolver.h"
#include "../bullet3/src/BulletDynamics/MLCPSolvers/btLemkeSolver.h"
#include "../bullet3/src/BulletDynamics/MLCPSolvers/btSolveProjectedGaussSeidel.h"
#include "../bullet3/src/BulletDynamics/MLCPSolvers/btMLCPSolver.h"

namespace bt3
{

///Physics
Physics::Physics(const PhysicsInfo& info):
		physicsInfo(info),
		broadphase(nullptr),
		dispatcher(nullptr),
		solver(nullptr),
		collisionConfiguration(nullptr),
		dynamicsWorld(nullptr),
		debugDrawer(nullptr),
		doDebugDraw(false)
{
	collisionShapes.clear();
	indexVertexArrays.clear();
	heightFieldDataArrays.clear();
	raycastVehicles.clear();
	kinematicCharacterControllers.clear();
}

void Physics::createDynamicsWorld(const btVector3& gravity)
{
	///1: collision configuration contains default setup for memory, collision setup
//	collisionConfiguration = new btDefaultCollisionConfiguration();
	collisionConfiguration = new btSoftBodyRigidBodyCollisionConfiguration();

	//collisionConfiguration->setConvexConvexMultipointIterations();

	///2: use the default collision dispatcher. For parallel processing (Multi-threading) you can use a different dispatcher (see Extras/BulletMultiThreaded)
	dispatcher = new btCollisionDispatcher(collisionConfiguration);

	///3: broad phase collision detection
	switch (physicsInfo.broadPhaseType)
	{
	case PhysicsInfo::BroadphaseType::AxisSweep3_32Bit:
	{
		btVector3 halfDims = physicsInfo.worldDims / 2.0;
		broadphase = new bt32BitAxisSweep3(-halfDims, halfDims,
				physicsInfo.maxHandles);
	}
		break;
	case PhysicsInfo::BroadphaseType::Dbvt:
	{
		broadphase = new btDbvtBroadphase();
	}
		break;
	case PhysicsInfo::BroadphaseType::Simple:
	{
		broadphase = new btSimpleBroadphase();
	}
		break;
	case PhysicsInfo::BroadphaseType::AxisSweep3:
	default:
	{
		btVector3 halfDims = physicsInfo.worldDims / 2.0;
		broadphase = new btAxisSweep3(-halfDims, halfDims,
				physicsInfo.maxHandles);

	}
		break;
	}

	///4: the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
	solver = createSolverByType(physicsInfo.solverType, physicsInfo.mlcpSolverType);

	///5: dynamics world
	switch (physicsInfo.worldType)
	{
	case PhysicsInfo::WorldType::Discrete:
	{
		dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase,
				solver, collisionConfiguration);
	}
		break;
	case PhysicsInfo::WorldType::DiscreteMt:///Multi-Threading
	{
		if (taskSchedulerMgr.getNumTaskSchedulers() == 0)
		{
			taskSchedulerMgr.init();
		}

		ASSERT_TRUE(
				(btGetTaskScheduler() != nullptr)
						&& (taskSchedulerMgr.getNumTaskSchedulers() > 1))

		// recreate the collision configuration
		delete collisionConfiguration;
		btDefaultCollisionConstructionInfo cci;
		cci.m_defaultMaxPersistentManifoldPoolSize =
				physicsInfo.maxPersistentManifoldPoolSize;
		cci.m_defaultMaxCollisionAlgorithmPoolSize =
				physicsInfo.maxCollisionAlgorithmPoolSize;
		collisionConfiguration = new btDefaultCollisionConfiguration(cci);

		// recreate the collision dispatcher
		delete dispatcher;
		dispatcher = new btCollisionDispatcherMt(collisionConfiguration,
				physicsInfo.grainSize);

		// recreate the constraint solver
		delete solver;
		btConstraintSolverPoolMt* solverPool;
		{
			PhysicsInfo::ConstraintSolverType poolSolverType =
					physicsInfo.solverType;
			if (poolSolverType
					== PhysicsInfo::ConstraintSolverType::SequentialImpulseMt)
			{
				// pool solvers shouldn't be parallel solvers, we don't allow that kind of
				// nested parallelism because of performance issues
				poolSolverType =
						PhysicsInfo::ConstraintSolverType::SequentialImpulse;
			}
			btConstraintSolver* solvers[BT_MAX_THREAD_COUNT];
			int maxThreadCount = BT_MAX_THREAD_COUNT;
			for (int i = 0; i < maxThreadCount; ++i)
			{
				solvers[i] = createSolverByType(poolSolverType, physicsInfo.mlcpSolverType);
			}
			solverPool = new btConstraintSolverPoolMt(solvers, maxThreadCount);
			solver = solverPool;
		}
		btSequentialImpulseConstraintSolverMt* solverMt = nullptr;
		if (physicsInfo.solverType
				== PhysicsInfo::ConstraintSolverType::SequentialImpulseMt)
		{
			solverMt = new btSequentialImpulseConstraintSolverMt();
		}
		dynamicsWorld = new btDiscreteDynamicsWorldMt(dispatcher, broadphase,
				solverPool, solverMt, collisionConfiguration);
	}
		break;
	case PhysicsInfo::WorldType::MultiBody:
	{
		ASSERT_TRUE(physicsInfo.solverType == PhysicsInfo::ConstraintSolverType::MultiBody)

		btMultiBodyConstraintSolver* solverMB =
				static_cast<btMultiBodyConstraintSolver*>(solver);
		dynamicsWorld = new btMultiBodyDynamicsWorld(dispatcher, broadphase,
				solverMB, collisionConfiguration);
	}
		break;
	case PhysicsInfo::WorldType::Simple:
	{
		dynamicsWorld = new btSimpleDynamicsWorld(dispatcher, broadphase,
				solver, collisionConfiguration);
	}
		break;
	case PhysicsInfo::WorldType::Fracture:
	{
		//TODO see examples
	}
		break;
	case PhysicsInfo::WorldType::SoftMultiBody:
	case PhysicsInfo::WorldType::SoftRigid:
	default:
	{
		btSoftBodySolver* softSolver;
		switch (physicsInfo.softBodySolverType)
		{
		case btSoftBodySolver::SolverTypes::CPU_SOLVER:
		case btSoftBodySolver::SolverTypes::CL_SOLVER:
		case btSoftBodySolver::SolverTypes::CL_SIMD_SOLVER:
		case btSoftBodySolver::SolverTypes::DX_SOLVER:
		case btSoftBodySolver::SolverTypes::DX_SIMD_SOLVER:
		{
			softSolver = nullptr;
		}
			break;
		case btSoftBodySolver::SolverTypes::DEFAULT_SOLVER:
		default:
		{
			softSolver = new btDefaultSoftBodySolver();
		}
			break;
		}
		//
		if(physicsInfo.worldType == PhysicsInfo::WorldType::SoftMultiBody)
		{
			ASSERT_TRUE(physicsInfo.solverType == PhysicsInfo::ConstraintSolverType::MultiBody)

			btMultiBodyConstraintSolver* solverMB =
					static_cast<btMultiBodyConstraintSolver*>(solver);
			dynamicsWorld = new btSoftMultiBodyDynamicsWorld(dispatcher, broadphase,
					solverMB, collisionConfiguration, softSolver);
		}
		else
		{
			//default
			dynamicsWorld = new btSoftRigidDynamicsWorld(dispatcher, broadphase,
				solver, collisionConfiguration, softSolver);
		}
		//set softBodyWorldInfo
		softBodyWorldInfo.air_density = physicsInfo.airDensity;
		softBodyWorldInfo.water_density = physicsInfo.waterDensity;
		softBodyWorldInfo.water_offset = physicsInfo.waterOffset;
		softBodyWorldInfo.m_maxDisplacement = physicsInfo.maxDisplacement;
		softBodyWorldInfo.water_normal = physicsInfo.waterNormal;
		softBodyWorldInfo.m_dispatcher = dispatcher;
		softBodyWorldInfo.m_broadphase = broadphase;
		softBodyWorldInfo.m_gravity = gravity;
		///TODO check the needs of the following two calls
		softBodyWorldInfo.m_sparsesdf.Initialize();
		softBodyWorldInfo.m_sparsesdf.Reset();
		///

	}
		break;
	}
	//
	dynamicsWorld->setGravity(gravity);
	dynamicsWorld->getDispatchInfo().m_enableSPU = true;

	// For the ghost object to work correctly, we need to add a callback to our world.
	dynamicsWorld->getPairCache()->setInternalGhostPairCallback(&ghostCallback);
}

void Physics::deleteDynamicsWorld()
{
	//cleanup in the reverse order of creation/initialization
	if (dynamicsWorld)
	{
		//remove the constraints from the dynamics world and delete them
		for (int i = dynamicsWorld->getNumConstraints() - 1; i >= 0; i--)
		{
			btTypedConstraint* constraint = dynamicsWorld->getConstraint(i);
			dynamicsWorld->removeConstraint(constraint);
			delete constraint;
		}

		//remove all the collision objects from the dynamics world and delete them
		for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
		{
			btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
			//rigid body
			btRigidBody* body = btRigidBody::upcast(obj);
			if (body && body->getMotionState())
			{
				delete body->getMotionState();
			}
			dynamicsWorld->removeCollisionObject(obj);
			delete obj;
		}
	}

	//delete index vertex arrays (ie triangle meshes)
	for (auto item: indexVertexArrays)
	{
		// items == (shape*, mesh*)
		indexVertexArrays.erase(item.first);
		delete item.second;
	}
	//next line is optional: it will be cleared by the destructor when the array goes out of scope
	indexVertexArrays.clear();

	//delete height field data arrays (used by btHeightfieldTerrainShape(s))
	for (auto item: heightFieldDataArrays)
	{
		// items == (shape*, array*)
		heightFieldDataArrays.erase(item.first);
		delete item.second;
	}
	//next line is optional: it will be cleared by the destructor when the array goes out of scope
	heightFieldDataArrays.clear();

	//delete collision shapes
	for (auto item: collisionShapes)
	{
		// items == (shape*, refCount)
		heightFieldDataArrays.erase(item.first);
		delete item.first;
	}
	//next line is optional: it will be cleared by the destructor when the array goes out of scope
	collisionShapes.clear();

	//delete raycast vehicle data arrays
	for (int j = 0; j < raycastVehicles.size(); j++)
	{
		btRaycastVehicle* vehicle = raycastVehicles[j];
		raycastVehicles[j] = 0;
		delete vehicle;
	}
	//next line is optional: it will be cleared by the destructor when the array goes out of scope
	raycastVehicles.clear();

	//delete kinematic character controller data arrays
	for (int j = 0; j < kinematicCharacterControllers.size(); j++)
	{
		btKinematicCharacterController* character = kinematicCharacterControllers[j];
		kinematicCharacterControllers[j] = 0;
		delete character;
	}
	//next line is optional: it will be cleared by the destructor when the array goes out of scope
	kinematicCharacterControllers.clear();

	//delete dynamics world
	delete dynamicsWorld;

	//delete solver
	delete solver;

	//delete broadphase
	delete broadphase;

	//delete dispatcher
	delete dispatcher;

	//delete collision configuration
	delete collisionConfiguration;

	//delete debug drawer
	delete debugDrawer;
}

//print positions of all objects
void Physics::printPositionsAllObjects()
{
	//print positions of all objects
	for (int j = dynamicsWorld->getNumCollisionObjects() - 1; j >= 0; j--)
	{
		btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[j];
		btRigidBody* body = btRigidBody::upcast(obj);
		btTransform trans;
		if (body && body->getMotionState())
		{
			body->getMotionState()->getWorldTransform(trans);

		}
		else
		{
			trans = obj->getWorldTransform();
		}
		cout << "world pos object " << j << " = ("
				<< float(trans.getOrigin().getX()) << ", "
				<< float(trans.getOrigin().getY()) << ", "
				<< float(trans.getOrigin().getZ()) << ")" << endl;
	}
}

void Physics::registerBroadphaseCollisionCallback(
		btOverlapFilterCallback* filterCallback)
{
	dynamicsWorld->getPairCache()->setOverlapFilterCallback(filterCallback);
}

btConstraintSolver* Physics::createSolverByType(PhysicsInfo::ConstraintSolverType t,
		PhysicsInfo::MLCPSolverType mlcpT)
{
	switch (t)
	{
	case PhysicsInfo::ConstraintSolverType::SequentialImpulse:
		return new btSequentialImpulseConstraintSolver();
	case PhysicsInfo::ConstraintSolverType::MultiBody:
		return new btMultiBodyConstraintSolver();
	case PhysicsInfo::ConstraintSolverType::NNCG:
		return new btNNCGConstraintSolver();
	case PhysicsInfo::ConstraintSolverType::PoolMt:
		return new btConstraintSolverPoolMt(physicsInfo.numPoolMtSolvers);
	case PhysicsInfo::ConstraintSolverType::MLCP:
	{
		btMLCPSolverInterface* mlcpSolver = nullptr;
		switch (mlcpT)
		{
		case PhysicsInfo::MLCPSolverType::Dantzig:
			mlcpSolver = new btDantzigSolver();
			break;
		case PhysicsInfo::MLCPSolverType::Lemke:
			mlcpSolver = new btLemkeSolver();
			break;
		case PhysicsInfo::MLCPSolverType::ProjectedGaussSeidel:
			mlcpSolver = new btSolveProjectedGaussSeidel();
			break;
		default:
			break;
		}
		return new btMLCPSolver(mlcpSolver);
	}
	///Multi-Threading
	case PhysicsInfo::ConstraintSolverType::SequentialImpulseMt:
		return new btSequentialImpulseConstraintSolverMt();
	default:
		return nullptr;
	}
}

} //namespace bt3
