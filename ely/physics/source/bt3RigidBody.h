/**
 * \file bt3RigidBody.h
 *
 * \date 2017-03-07
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_BT3RIGIDBODY_H_
#define PHYSICS_SOURCE_BT3RIGIDBODY_H_

#include "gamePhysicsManager.h"
#include "physics_includes.h"
#include "geoMipTerrain.h"
#include "bt3CollisionObjectAttrs.h"

#ifndef CPPPARSER
#include "support/rigidBody.h"
#endif //CPPPARSER

/**
 * BT3RigidBody is a PandaNode representing a "rigid body" of the Bullet
 * library.
 *
 * It constructs a rigid body with the single specified collision shape type
 * along with relevant parameters.\n
 *
 * \note Before destroying a BT3RigidBody all constraints applied to it must be
 * destroyed, otherwise there will be memory leaks.\n
 *
 * \note All physical quantities are expressed in the same units of measurement
 * used by Bullet.
 *
 * > **BT3RigidBody text parameters**:
 * param | type | default | note
 * ------|------|---------|-----
 * | *object*  									|single| - | -
 * | *shape_type*  								|single| *sphere* | values: box,plane,sphere,capsule,cylinder,cone,multi_sphere,convex_hull,gimpact,triangle_mesh,terrain,compound
 * | *child_shape_type*  						|single| *convex_hull* | values: box,plane,sphere,capsule,cylinder,cone,multi_sphere,convex_hull,gimpact,triangle_mesh,terrain,compound
 * | *use_shape_of*								|single| - | -
 * | *up_axis*  								|single| *z* | values: x,y,z,no_up
 * | *group_mask*  								|single| *all_on* | -
 * | *collide_mask*  							|single| *all_on* | -
 * | *mass*  									|single| 1.0 | -
 * | *linear_damping*  							|single| 0.0 | -
 * | *angular_damping*  						|single| 0.0 | -
 * | *friction*  								|single| 0.5 | -
 * | *rolling_friction*  						|single| 0.0 | -
 * | *spinning_friction*  						|single| 0.0 | -
 * | *restitution*  							|single| 0.0 | -
 * | *linear_sleeping_threshold*  				|single| 0.8 | -
 * | *angular_sleeping_threshold*  				|single| 1.0 | -
 * | *additional_damping*  						|single| *false* | -
 * | *additional_damping_factor*  				|single| 0.005 | -
 * | *additional_linear_damping_threshold_sqr*  |single| 0.01 | -
 * | *additional_angular_damping_threshold_sqr* |single| 0.01 | -
 * | *additional_angular_damping_factor*  		|single| 0.01 | -
 *
 * \note parts inside [] are optional.\n
 */
class EXPCL_PHYSICS BT3RigidBody: public PandaNode
{
PUBLISHED:

	/**
	 * Equivalent to bt3::RigidBodyType.
	 * It may change during the BT3RigidBody's lifetime.
	 */
	enum BT3BodyType: unsigned char
	{
#ifndef CPPPARSER
		DYNAMIC = static_cast<unsigned char>(bt3::RigidBodyType::DYNAMIC), //!< mass != 0.0, physics driven (default)
		STATIC = static_cast<unsigned char>(bt3::RigidBodyType::STATIC), //!< mass == 0.0, no driven
		KINEMATIC = static_cast<unsigned char>(bt3::RigidBodyType::KINEMATIC), //!< mass == 0.0, user driven
		INVALID_TYPE = static_cast<unsigned char>(bt3::RigidBodyType::INVALID_TYPE) //!< invalid body type
#else
		DYNAMIC,STATIC,KINEMATIC,INVALID_TYPE
#endif //CPPPARSER
	};

	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	virtual ~BT3RigidBody();
#endif //CPPPARSER

	/**
	 * \name OWNER OBJECT
	 */
	///@{
	INLINE void set_owner_object(const NodePath& object);
	INLINE NodePath get_owner_object() const;
	// Python Properties
	MAKE_PROPERTY(owner_object, get_owner_object, set_owner_object);
	///@}

	/**
	 * \name RIGIDBODY
	 */
	///@{
	int setup();
	int cleanup();
	void update(float dt);
	///@}

	/**
	 * \name CONSTRUCTION PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE void set_shape_type(GamePhysicsManager::BT3ShapeType value);
	INLINE GamePhysicsManager::BT3ShapeType get_shape_type() const;
	INLINE void set_child_shape_type(GamePhysicsManager::BT3ShapeType value);
	INLINE GamePhysicsManager::BT3ShapeType get_child_shape_type() const;
	INLINE void set_group_mask(const CollideMask& value);
	INLINE const CollideMask& get_group_mask() const;
	INLINE void set_collide_mask(const CollideMask& value);
	INLINE const CollideMask& get_collide_mask() const;
	INLINE void set_use_shape_of(const string& value);
	INLINE const string& get_use_shape_of() const;
	INLINE void set_up_axis(GamePhysicsManager::BT3UpAxis value);
	INLINE GamePhysicsManager::BT3UpAxis get_up_axis() const;
	INLINE void set_mass(float value);
	INLINE float get_mass() const;
	INLINE void set_user_data(TypedObject* userData);
	// Python Properties
	MAKE_PROPERTY(shape_type, get_shape_type, set_shape_type);
	MAKE_PROPERTY(child_shape_type, get_child_shape_type, set_child_shape_type);
	MAKE_PROPERTY(group_mask, get_group_mask, set_group_mask);
	MAKE_PROPERTY(collide_mask, get_collide_mask, set_collide_mask);
	MAKE_PROPERTY(use_shape_of, get_use_shape_of, set_use_shape_of);
	MAKE_PROPERTY(up_axis, get_up_axis, set_up_axis);
	MAKE_PROPERTY(mass, get_mass, set_mass);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	void switch_body_type(BT3BodyType bodyType);
	BT3BodyType get_body_type() const;
	INLINE void set_linear_damping(float lin_damping);
	INLINE float get_linear_damping() const;
	INLINE void set_angular_damping(float ang_damping);
	INLINE float get_angular_damping() const;
	INLINE void set_linear_factor(const LVecBase3f& linearFactor);
	INLINE LVecBase3f get_linear_factor() const;
	INLINE void set_angular_factor(const LVecBase3f& angFac);
	INLINE LVecBase3f get_angular_factor() const;
	INLINE void set_linear_sleeping_threshold(float linear);
	INLINE float get_linear_sleeping_threshold() const;
	INLINE void set_angular_sleeping_threshold(float angular);
	INLINE float get_angular_sleeping_threshold() const;
	INLINE void set_flags(int flags);
	INLINE int get_flags() const;
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	INLINE PyObject* get_collision_object_attrs();
	// Python Properties
	MAKE_PROPERTY(collision_object_attrs, get_collision_object_attrs);
#else
	inline CollisionObjectAttrs* get_collision_object_attrs();
#endif //PYTHON_BUILD
	// Python Properties
	MAKE_PROPERTY(body_type, get_body_type, switch_body_type);
	MAKE_PROPERTY(linear_damping, get_linear_damping, set_linear_damping);
	MAKE_PROPERTY(angular_damping, get_angular_damping, set_angular_damping);
	MAKE_PROPERTY(linear_factor, get_linear_factor, set_linear_factor);
	MAKE_PROPERTY(angular_factor, get_angular_factor, set_angular_factor);
	MAKE_PROPERTY(linear_sleeping_threshold, get_linear_sleeping_threshold, set_linear_sleeping_threshold);
	MAKE_PROPERTY(angular_sleeping_threshold, get_angular_sleeping_threshold, set_angular_sleeping_threshold);
	MAKE_PROPERTY(flags, get_flags, set_flags);
	///@}

	/**
	 * \name PHYSICAL ACTIONS
	 */
	///@{
	INLINE void set_linear_velocity(const LVector3f& lin_vel);
	INLINE LVector3f get_linear_velocity() const;
	INLINE void set_angular_velocity(const LVector3f& ang_vel);
	INLINE LVector3f get_angular_velocity() const;
	INLINE void apply_force(const LVector3f& force, const LPoint3f& rel_pos);
	INLINE void apply_torque(const LVector3f& torque);
	INLINE void apply_central_force(const LVector3f& force);
	INLINE void apply_impulse(const LVector3f& impulse, const LPoint3f& rel_pos);
	INLINE void apply_central_impulse(const LVector3f& impulse);
	INLINE void apply_torque_impulse(const LVector3f& torque);
	INLINE LVector3f get_total_force() const;
	INLINE LVector3f get_total_torque() const;
	INLINE void clear_forces();
	INLINE void transform(CPT(TransformState) trs);
	// Python Properties
	MAKE_PROPERTY(linear_velocity, get_linear_velocity, set_linear_velocity);
	MAKE_PROPERTY(angular_velocity, get_angular_velocity, set_angular_velocity);
	MAKE_PROPERTY(total_force, get_total_force);
	MAKE_PROPERTY(total_torque, get_total_torque);
	///@}

	/**
	 * \name OUTPUT
	 */
	///@{
	void output(ostream &out) const;
	///@}

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	/**
	 * \name PYTHON UPDATE CALLBACK
	 */
	///@{
	void set_update_callback(PyObject *clbk);
	///@}
#else
	/**
	 * \name C++ UPDATE CALLBACK
	 */
	///@{
	typedef void (*UPDATECALLBACKFUNC)(PT(BT3RigidBody));
	void set_update_callback(UPDATECALLBACKFUNC value);
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name TypedWritable API
	 */
	///@{
	void post_process_from_bam();
	///@}

public:
	/**
	 * \name C++ ONLY
	 * Library & support low level related methods.
	 */
	///@{
	inline btRigidBody& get_rigid_body();
	inline const btRigidBody& get_rigid_body() const;
	///@}

	// Explicitly deleted copy constructor and copy assignment operator.
	BT3RigidBody(const BT3RigidBody&) = delete;
	BT3RigidBody& operator=(const BT3RigidBody&) = delete;

#ifndef CPPPARSER
protected:
	friend void unref_delete<BT3RigidBody>(BT3RigidBody*);
	friend class GamePhysicsManager;

	BT3RigidBody(const string& name);
	virtual ~BT3RigidBody();

private:
	///The owner object this BT3RigidBody is attached to.
	NodePath mObjectNP;
	///The reference node path.
	NodePath mReferenceNP;
	///The setup flag.
	bool mSetup;
	///Bullet rigid body reference.
	btRigidBody* mRigidBody;
	///@{
	///Physical parameters.
	//construction.
	float mMass;
	GamePhysicsManager::BT3ShapeType mShapeType;
	GamePhysicsManager::BT3ShapeType mChildShapeType;
	BitMask32 mGroupMask, mCollideMask;
	string mUseShapeOfId;
	GamePhysicsManager::BT3UpAxis mUpAxis;
	void* mUserData;
	bt3::RigidBodyData mRigidBodyData;
	//dynamic
#if defined(PYTHON_BUILD)
	PyObject *mCollisionObjectAttrsPy;
#endif //PYTHON_BUILD
	CollisionObjectAttrs* mCollisionObjectAttrs;
	///@}

	inline void do_reset();
	void do_initialize();
	void do_finalize();

	///Helpers.
	GamePhysicsManager::BT3ShapeType do_get_shape_type(const string& name) const;
	BitMask32 do_get_mask(const string& name) const;
	CollisionObjectAttrs* do_create_collision_object_attrs();
#if defined(PYTHON_BUILD)
	PyObject *do_create_collision_object_attrs_py();
#endif //PYTHON_BUILD

#if defined(PYTHON_BUILD)
	/**
	 * \name Python callback.
	 */
	///@{
	PyObject *mSelf, *mUpdateCallback;
	///@}
#else
	/**
	 * \name C++ callback.
	 */
	///@{
	UPDATECALLBACKFUNC mUpdateCallback;
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name SERIALIZATION ONLY SETTINGS.
	 */
	///@{
	// persistent storage for serialized data
	bool mBuildFromBam;
	Datagram mOutDatagram;
	///@}
#endif //CPPPARSER

public:
	/**
	 * \name TypedWritable API
	 */
	///@{
	static void register_with_read_factory();
	virtual void write_datagram (BamWriter *manager, Datagram &dg) override;
	virtual int complete_pointers(TypedWritable **p_list, BamReader *manager) override;
	///@}

#ifndef CPPPARSER
protected:
	static TypedWritable *make_from_bam(const FactoryParams &params);
	virtual void fillin(DatagramIterator &scan, BamReader *manager) override;
	void do_write_rigid_body_data(Datagram &dg);
	void do_read_rigid_body_data(DatagramIterator &scan);
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(BT3RigidBody,PandaNode)
};

INLINE ostream &operator << (ostream &out, const BT3RigidBody & rigid_body);

///inline
#include "bt3RigidBody.I"

#endif /* PHYSICS_SOURCE_BT3RIGIDBODY_H_ */
