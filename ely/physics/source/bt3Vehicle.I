/**
 * \file bt3Vehicle.I
 *
 * \date 2017-07-26
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_BT3VEHICLE_I_
#define PHYSICS_SOURCE_BT3VEHICLE_I_

///BT3Vehicle inline definitions

/**
 * Sets the owner object.
 * \note: owner object's node must be a BT3RigidBody, to be used as chassis. It
 * should be parented to the scene's graph in some way.
 */
INLINE void BT3Vehicle::set_owner_object(const NodePath& value)
{
	RETURN_ON_COND(mSetup,)

	mObjectNP = value;
}

/**
 * Returns the owner object.
 */
INLINE NodePath BT3Vehicle::get_owner_object() const
{
	return mObjectNP;
}

/**
 * Sets the up axis.
 */
INLINE void BT3Vehicle::set_up_axis(GamePhysicsManager::BT3UpAxis value)
{
	RETURN_ON_COND(mSetup,)

	mVehicleData.cs = (bt3::Panda3dUpAxis)value;
}

/**
 * Returns the up axis.
 */
INLINE GamePhysicsManager::BT3UpAxis BT3Vehicle::get_up_axis() const
{
	return (GamePhysicsManager::BT3UpAxis) mVehicleData.cs;
}

/**
 * Sets the BT3VehicleTuning for construction.
 */
INLINE void BT3Vehicle::set_vehicle_tuning(const BT3VehicleTuning& tuning)
{
	RETURN_ON_COND(mSetup,)

	mVehicleData.tuning = tuning;
}

/**
 * Returns the BT3VehicleTuning for construction.
 */
INLINE BT3VehicleTuning BT3Vehicle::get_vehicle_tuning() const
{
	return mVehicleData.tuning;
}

/**
 * Sets the number of wheels for construction.
 */
INLINE void BT3Vehicle::set_wheels_number(int wheelNum)
{
	RETURN_ON_COND(mSetup,)

	if(wheelNum <= 2)
	{
		mVehicleData.numWheels = 2;
	}
	else
	{
		mVehicleData.numWheels = 4;
	}
}

/**
 * Returns the number of wheels for construction.
 */
INLINE int BT3Vehicle::get_wheels_number() const
{
	return mVehicleData.numWheels;
}

/**
 * Sets the front/rear wheel's model for construction.
 */
INLINE void BT3Vehicle::set_wheel_model(const NodePath& model, bool isFront)
{
	RETURN_ON_COND(mSetup,)

	isFront ? mWheelModel[0] = model: mWheelModel[1] = model;
}

/**
 * Returns the front/rear wheel's model for construction.
 */
INLINE NodePath BT3Vehicle::get_wheel_model(bool isFront) const
{
	return isFront ? mWheelModel[0]: mWheelModel[1];
}

/**
 * Sets the front/rear wheel's connection height factor for construction.
 */
INLINE void BT3Vehicle::set_connection_height_factor(float value, bool isFront)
{
	RETURN_ON_COND(mSetup,)

	isFront ? mVehicleData.connHeightF[0] = value : mVehicleData.connHeightF[1] =
					value;
}

/**
 * Returns the front/rear wheel's connection height factor for construction.
 */
INLINE float BT3Vehicle::get_connection_height_factor(bool isFront) const
{
	return isFront ? mVehicleData.connHeightF[0] : mVehicleData.connHeightF[1];
}

/**
 * Sets the front/rear wheel's connection width factor for construction.
 */
INLINE void BT3Vehicle::set_connection_width_factor(float value, bool isFront)
{
	RETURN_ON_COND(mSetup,)

	isFront ? mVehicleData.connWidthF[0] = value : mVehicleData.connWidthF[1] =
					value;
}

/**
 * Returns the front/rear wheel's connection width factor for construction.
 */
INLINE float BT3Vehicle::get_connection_width_factor(bool isFront) const
{
	return isFront ? mVehicleData.connWidthF[0]: mVehicleData.connWidthF[1];
}

/**
 * Sets the front/rear wheel's connection depth factor for construction.
 */
INLINE void BT3Vehicle::set_connection_depth_factor(float value, bool isFront)
{
	RETURN_ON_COND(mSetup,)

	isFront ? mVehicleData.connDepthF[0] = value : mVehicleData.connDepthF[1] =
					value;
}

/**
 * Returns the front/rear wheel's connection depth factor for construction.
 */
INLINE float BT3Vehicle::get_connection_depth_factor(bool isFront) const
{
	return isFront ? mVehicleData.connDepthF[0]: mVehicleData.connDepthF[1];
}

/**
 * Sets the front/rear wheel's suspension rest lenght factor for construction.
 */
INLINE void BT3Vehicle::set_suspension_rest_lenght_factor(float value, bool isFront)
{
	RETURN_ON_COND(mSetup,)

	isFront ?
		mVehicleData.suspRestLengthF[0] = value : mVehicleData.suspRestLengthF[1] =
				value;
}

/**
 * Returns the front/rear wheel's suspension rest lenght factor for
 * construction.
 */
INLINE float BT3Vehicle::get_suspension_rest_lenght_factor(bool isFront) const
{
	return isFront ?
			mVehicleData.suspRestLengthF[0]: mVehicleData.suspRestLengthF[1];
}

/**
 * Returns the BT3RigidBody chassis.
 */
INLINE PT(BT3RigidBody) BT3Vehicle::get_chassis() const
{
	RETURN_ON_COND(!mSetup, nullptr)

	// after setup BT3Vehicle has a BT3RigidBody owner object as chassis
	return DCAST(BT3RigidBody, mObjectNP.node());
}

/**
 * Returns a BT3WheelInfo object to set/get several parameters for the given
 * wheel.
 */
INLINE BT3WheelInfo BT3Vehicle::get_wheel_info(int wheel) const
{
	RETURN_ON_COND(!mSetup || !((wheel >= 0) && (wheel < get_wheels_number())),
			BT3WheelInfo::_dummy)

	return mVehicle->getWheelInfo(wheel);
}

/**
 * Sets the max engine force (>=0).
 */
INLINE void BT3Vehicle::set_max_engine_force(float value)
{
	mMaxEngineForce = abs(value);
}

/**
 * Returns the max engine force (>=0).
 */
INLINE float BT3Vehicle::get_max_engine_force() const
{
	return mMaxEngineForce;
}

/**
 * Sets the max brake (>=0).
 */
INLINE void BT3Vehicle::set_max_brake(float value)
{
	mMaxBrake = abs(value);
}

/**
 * Returns the max brake (>=0).
 */
INLINE float BT3Vehicle::get_max_brake() const
{
	return mMaxBrake;
}

/**
 * Sets the steering clamp (>=0 and <M_PI).
 */
INLINE void BT3Vehicle::set_steering_clamp(float value)
{
	mSteeringClamp = abs(value);
}

/**
 * Returns the steering clamp in degrees (>=0 and <M_PI).
 */
INLINE float BT3Vehicle::get_steering_clamp() const
{
	return mSteeringClamp;
}

/**
 * Sets the steering increment (>=0).
 */
INLINE void BT3Vehicle::set_steering_increment(float value)
{
	mSteeringIncrement = abs(value);
}

/**
 * Returns the steering increment (>=0).
 */
INLINE float BT3Vehicle::get_steering_increment() const
{
	return mSteeringIncrement;
}

/**
 * Sets the steering decrement (>=0).
 */
INLINE void BT3Vehicle::set_steering_decrement(float value)
{
	mSteeringDecrement = abs(value);
}

/**
 * Returns the steering decrement (>=0).
 */
INLINE float BT3Vehicle::get_steering_decrement() const
{
	return mSteeringDecrement;
}

/**
 * Enables/disables forward movement (default: enabled).
 */
INLINE void BT3Vehicle::set_enable_forward(bool value)
{
	mForwardKey = value;
	if(!value)
	{
		mForward = false;
	}
}

/**
 * Returns if forward movement is enabled/disabled (default: enabled).
 */
INLINE bool BT3Vehicle::get_enable_forward() const
{
	return mForwardKey;
}

/**
 * Enables/disables backward movement (default: enabled).
 */
INLINE void BT3Vehicle::set_enable_backward(bool value)
{
	mBackwardKey = value;
	if(!value)
	{
		mBackward = false;
	}
}

/**
 * Returns if backward movement is enabled/disabled (default: enabled).
 */
INLINE bool BT3Vehicle::get_enable_backward() const
{
	return mBackwardKey;
}

/**
 * Enables/disables brake (default: enabled).
 */
INLINE void BT3Vehicle::set_enable_brake(bool value)
{
	mBrakeKey = value;
	if(!value)
	{
		mBrake = false;
	}
}

/**
 * Returns if brake is enabled/disabled (default: enabled).
 */
INLINE bool BT3Vehicle::get_enable_brake() const
{
	return mBrakeKey;
}

/**
 * Enables/disables head left rotation (default: enabled).
 */
INLINE void BT3Vehicle::set_enable_head_left(bool value)
{
	mHeadLeftKey = value;
	if(!value)
	{
		mHeadLeft = false;
	}
}

/**
 * Returns if head left rotation is enabled/disabled (default: enabled).
 */
INLINE bool BT3Vehicle::get_enable_head_left() const
{
	return mHeadLeftKey;
}

/**
 * Enables/disables head right rotation (default: enabled).
 */
INLINE void BT3Vehicle::set_enable_head_right(bool value)
{
	mHeadRightKey = value;
	if(!value)
	{
		mHeadRight = false;
	}
}

/**
 * Returns if head right rotation is enabled/disabled (default: enabled).
 */
INLINE bool BT3Vehicle::get_enable_head_right() const
{
	return mHeadRightKey;
}

/**
 * Activates/deactivates forward movement.
 */
INLINE void BT3Vehicle::set_move_forward(bool value)
{
	if(mForwardKey)
	{
		mForward = value;
	}
}

/**
 * Returns if the forward movement is activated/deactivated.
 */
INLINE bool BT3Vehicle::get_move_forward() const
{
	return mForward;
}

/**
 * Activates/deactivates backward movement.
 */
INLINE void BT3Vehicle::set_move_backward(bool value)
{
	if(mBackwardKey)
	{
		mBackward = value;
	}
}

/**
 * Returns if the backward movement is activated/deactivated.
 */
INLINE bool BT3Vehicle::get_move_backward() const
{
	return mBackward;
}

/**
 * Activates/deactivates brake.
 */
INLINE void BT3Vehicle::set_brake(bool value)
{
	if(mBrakeKey)
	{
		mBrake = value;
	}
}

/**
 * Returns if the brake is activated/deactivated.
 */
INLINE bool BT3Vehicle::get_brake() const
{
	return mBrake;
}

/**
 * Activates/deactivates head left rotation.
 */
INLINE void BT3Vehicle::set_rotate_head_left(bool value)
{
	if(mHeadLeftKey)
	{
		mHeadLeft = value;
	}
}

/**
 * Returns if the head left rotation is activated/deactivated.
 */
INLINE bool BT3Vehicle::get_rotate_head_left() const
{
	return mHeadLeft;
}

/**
 * Activates/deactivates head right rotation.
 */
INLINE void BT3Vehicle::set_rotate_head_right(bool value)
{
	if(mHeadRightKey)
	{
		mHeadRight = value;
	}
}

/**
 * Returns if the head right rotation is activated/deactivated.
 */
INLINE bool BT3Vehicle::get_rotate_head_right() const
{
	return mHeadRight;
}

/**
 * Enables/disables the front/rear wheel apply steering.
 */
INLINE void BT3Vehicle::set_wheel_apply_steering(bool enable, bool isFront)
{
	isFront ? mWheelApplySteering[0] = enable: mWheelApplySteering[1] = enable;
}

/**
 * Returns if the front/rear wheel apply steering is enabled.
 */
INLINE bool BT3Vehicle::get_wheel_apply_steering(bool isFront) const
{
	return isFront ? mWheelApplySteering[0]: mWheelApplySteering[1];
}

/**
 * Enables/disables the front/rear wheel apply engine force.
 */
INLINE void BT3Vehicle::set_wheel_apply_engine_force(bool enable, bool isFront)
{
	isFront ? mWheelApplyEngineForce[0] = enable : mWheelApplyEngineForce[1] =
						enable;
}

/**
 * Returns if the front/rear wheel apply engine force is enabled.
 */
INLINE bool BT3Vehicle::get_wheel_apply_engine_force(bool isFront) const
{
	return isFront ? mWheelApplyEngineForce[0]: mWheelApplyEngineForce[1];
}

/**
 * Enables/disables the front/rear wheel apply brake.
 */
INLINE void BT3Vehicle::set_wheel_apply_brake(bool enable, bool isFront)
{
	isFront ? mWheelApplyBrake[0] = enable: mWheelApplyBrake[1] = enable;
}

/**
 * Returns if the front/rear wheel apply brake is enabled.
 */
INLINE bool BT3Vehicle::get_wheel_apply_brake(bool isFront) const
{
	return isFront ? mWheelApplyBrake[0]: mWheelApplyBrake[1];
}

/**
 * Applies a steering value to the given wheel.
 */
INLINE void BT3Vehicle::apply_steering_value(float steering, int wheel)
{
	RETURN_ON_COND(!mSetup || !((wheel >= 0) && (wheel < get_wheels_number())),)

	mVehicle->setSteeringValue(steering, wheel);
}

/**
 * Applies a steering force to the given wheel.
 */
INLINE void BT3Vehicle::apply_engine_force(float force, int wheel)
{
	RETURN_ON_COND(!mSetup || !((wheel >= 0) && (wheel < get_wheels_number())),)

	mVehicle->applyEngineForce(force, wheel);
}

/**
 * Applies a steering force to the given wheel.
 */
INLINE void BT3Vehicle::apply_brake(float brake, int wheel)
{
	RETURN_ON_COND(!mSetup || !((wheel >= 0) && (wheel < get_wheels_number())),)

	mVehicle->setBrake(brake, wheel);
}

/**
 * Sets the given pitch control.
 */
INLINE void BT3Vehicle::set_pitch_control(float pitch)
{
	RETURN_ON_COND(!mSetup,)

	mVehicle->setPitchControl(pitch);
}

/**
 * Resets the suspension.
 */
INLINE void BT3Vehicle::reset_suspension()
{
	RETURN_ON_COND(!mSetup,)

	mVehicle->resetSuspension();
}

/**
 * Returns the current forward vector.
 */
INLINE LVector3f BT3Vehicle::get_forward_vector() const
{
	RETURN_ON_COND(!mSetup, LVector3f::zero())

	return bt3::btVector3_to_LVecBase3(mVehicle->getForwardVector());
}

/**
 * Returns the current speed in km/h.
 */
INLINE float BT3Vehicle::get_current_speed_km_hour() const
{
	RETURN_ON_COND(!mSetup, RESULT_ERROR)

	return mVehicle->getCurrentSpeedKmHour();
}

/**
 * Returns the modifiable btRaycastVehicle.
 */
inline btRaycastVehicle& BT3Vehicle::get_vehicle_object()
{
	return *mVehicle;
}

/**
 * Returns the unmodifiable btRaycastVehicle.
 */
inline const btRaycastVehicle& BT3Vehicle::get_vehicle_object() const
{
	return *mVehicle;
}

/**
 * Resets the BT3Vehicle.
 * \note Internal use only.
 */
inline void BT3Vehicle::do_reset()
{
	//
	mObjectNP.clear();
	mReferenceNP.clear();
	mSetup = false;
	mVehicle = nullptr;
	mVehicleData = bt3::VehicleData();
	mWheelModel[0].clear();
	mWheelModel[1].clear();
	mMaxEngineForce = mMaxBrake = 0.0;
	mSteering = mSteeringClamp = mSteeringIncrement = mSteeringDecrement = 0.0;
	mWheelApplySteering[0] = mWheelApplySteering[1] = false;
	mWheelApplyEngineForce[0] = mWheelApplyEngineForce[1] = false;
	mWheelApplyBrake[0] = mWheelApplyBrake[1] = false;
	mForward = mBackward = mBrake = mHeadLeft = mHeadRight = false;
	mForwardKey = mBackwardKey = mBrakeKey = mHeadLeftKey = mHeadRightKey = false;
	//
	mMove = ThrowEventData();
	mSteady = ThrowEventData();
#ifdef PYTHON_BUILD
	mSelf = nullptr;
#endif //PYTHON_BUILD
	mUpdateCallback = nullptr;
	//serialization
	mBuildFromBam = false;
	mOutDatagram.clear();
}

INLINE ostream &operator <<(ostream &out, const BT3Vehicle& vehicle)
{
	vehicle.output(out);
	return out;
}

#endif /* PHYSICS_SOURCE_BT3VEHICLE_I_ */
