/**
 * \file bt3Constraint.h
 *
 * \date 2017-03-18
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_BT3CONSTRAINT_H_
#define PHYSICS_SOURCE_BT3CONSTRAINT_H_

#include "gamePhysicsManager.h"
#include "physics_includes.h"

#ifndef CPPPARSER
#include "support/constraint.h"
#endif //CPPPARSER

class TypedConstraintAttrs;

/**
 * BT3Constraint is a PandaNode representing a "constraint" of the Bullet
 * library.
 *
 * It constructs a constraint among two objects or one object and the world.\n
 *
 * \note A BT3Constraint must be destroyed before any of the object
 * (BT3RigidBody), to which it is applied, is destroyed.\n
 *
 * \note All physical quantities are expressed in the same units of measurement
 * used by Bullet.
 *
 * > **BT3Constraint text parameters**:
 * param | type | default | note
 * ------|------|---------|-----
 * | *objectA* 						|single| - | -
 * | *objectB* 						|single| - | -
 * | *contraint_type*				|single| *point2point* | values: point2point,hinge,hinge_accumulated,hinge2,cone_twist,dof6,dof6_spring,dof6_spring2,fixed,universal,slider,contact,gear
 * | *use_world_reference*			|single| *false* | -
 * | *pivot_references*				|single| *0.0,0.0,0.0:0.0,0.0,0.0* | specified as "px1,py1,pz1[:px2,py2,pz2]"
 * | *axis_references*				|single| *1.0,1.0,1.0:1.0,1.0,1.0* | specified as "vx1,vy1,vz1[:vx2,vy2,vz2]"
 * | *ratio* 						|single| *0.0* | -
 * | *rotation_references*			|single| *0.0,0.0,0.0:0.0,0.0,0.0* | specified as "h1,p1,r1[:h2,p2,r2]"
 * | *rotation_order* 				|single| *xyz* | values: xyz,xzy,yxz,yzx,zxy,zyx
 * | *world_anchor*					|single| *0.0,0.0,0.0* | specified as "px1,py1,pz1"
 * | *world_axes*					|single| *1.0,1.0,1.0:1.0,1.0,1.0* | specified as "vx1,vy1,vz1:vx2,vy2,vz2"
 * | *use_reference_frame_a*		|single| *false* | -
 * | *disable_objects_collisions*	|single| *false* | -
 *
 * \note parts inside [] are optional.\n
 *
 */
class EXPCL_PHYSICS BT3Constraint: public PandaNode //fixme should be derived from TypedWritableReferenceCount and Namable only ?
{
PUBLISHED:

	/**
	 * Equivalent to bt3::ConstraintType.
	 * It may change during the BT3Constraint's lifetime.
	 */
	enum BT3ConstraintType: unsigned char
	{
#ifndef CPPPARSER
		POINT2POINT = static_cast<unsigned char>(bt3::ConstraintType::POINT2POINT),
		HINGE = static_cast<unsigned char>(bt3::ConstraintType::HINGE),
		HINGE_ACCUMULATED = static_cast<unsigned char>(bt3::ConstraintType::HINGE_ACCUMULATED),
		HINGE_2 = static_cast<unsigned char>(bt3::ConstraintType::HINGE_2),
		CONETWIST = static_cast<unsigned char>(bt3::ConstraintType::CONETWIST),
		DOF6 = static_cast<unsigned char>(bt3::ConstraintType::DOF6),
		DOF6_SPRING = static_cast<unsigned char>(bt3::ConstraintType::DOF6_SPRING),
		DOF6_SPRING_2 = static_cast<unsigned char>(bt3::ConstraintType::DOF6_SPRING_2),
		FIXED = static_cast<unsigned char>(bt3::ConstraintType::FIXED),
		UNIVERSAL = static_cast<unsigned char>(bt3::ConstraintType::UNIVERSAL),
		SLIDER = static_cast<unsigned char>(bt3::ConstraintType::SLIDER),
		CONTACT = static_cast<unsigned char>(bt3::ConstraintType::CONTACT),
		GEAR = static_cast<unsigned char>(bt3::ConstraintType::GEAR),
		INVALID_TYPE = static_cast<unsigned char>(bt3::ConstraintType::INVALID_TYPE)
#else
		POINT2POINT,HINGE,HINGE_ACCUMULATED,HINGE_2,CONETWIST,DOF6,DOF6_SPRING,
		DOF6_SPRING_2,FIXED,UNIVERSAL,SLIDER,CONTACT,GEAR,INVALID_TYPE
#endif //CPPPARSER
	};

	/**
	 * Equivalent to btConstraintParams.
	 */
	enum BT3ConstraintParams: unsigned char
	{
#ifndef CPPPARSER
		CONSTRAINT_ERP = btConstraintParams::BT_CONSTRAINT_ERP,
		CONSTRAINT_STOP_ERP = btConstraintParams::BT_CONSTRAINT_STOP_ERP,
		CONSTRAINT_CFM = btConstraintParams::BT_CONSTRAINT_CFM,
		CONSTRAINT_STOP_CFM = btConstraintParams::BT_CONSTRAINT_STOP_CFM,
#else
		CONSTRAINT_ERP,CONSTRAINT_STOP_ERP,CONSTRAINT_CFM,CONSTRAINT_STOP_CFM
#endif //CPPPARSER
	};

	/**
	 * Equivalent to RotateOrder.
	 */
	enum BT3RotateOrder: unsigned char
	{
	#ifndef CPPPARSER
		RO_XYZ = RotateOrder::RO_XZY,//XYZ
		RO_XZY = RotateOrder::RO_XYZ,//XZY
		RO_YXZ = RotateOrder::RO_ZXY,//YXZ
		RO_YZX = RotateOrder::RO_ZYX,//YZX
		RO_ZXY = RotateOrder::RO_YXZ,//ZXY
		RO_ZYX = RotateOrder::RO_YZX,//ZYX
	#else
		RO_XYZ,RO_XZY,RO_YXZ,RO_YZX,RO_ZXY,RO_ZYX,
	#endif //CPPPARSER
	};

	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	virtual ~BT3Constraint();
#endif //CPPPARSER

	/**
	 * \name OWNER OBJECTS
	 */
	///@{
	INLINE void set_owner_objects(const ValueList_NodePath& objects);
	INLINE ValueList_NodePath get_owner_objects() const;
	// Python Properties
	MAKE_PROPERTY(owner_objects, get_owner_objects, set_owner_objects);
	///@}

	/**
	 * \name CONSTRAINT
	 */
	///@{
	int setup();
	int cleanup();
	void update(float dt);
	///@}

	/**
	 * \name CONSTRUCTION PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE void set_constraint_type(BT3ConstraintType type);
	INLINE BT3ConstraintType get_constraint_type() const;
	INLINE void set_use_world_reference(bool value);
	INLINE bool get_use_world_reference() const;
	INLINE void set_pivot_references(const ValueList_LPoint3f& points);
	INLINE ValueList_LPoint3f get_pivot_references() const;
	INLINE void set_axis_references(const ValueList_LVector3f& vectors);
	INLINE ValueList_LVector3f get_axis_references() const;
	INLINE void set_ratio(float ratio);
	INLINE float get_ratio() const;
	INLINE void set_rotation_references(const ValueList_LVecBase3f& hpr);
	INLINE ValueList_LVecBase3f get_rotation_references() const;
	INLINE void set_rotation_order(BT3RotateOrder order);
	INLINE BT3RotateOrder get_rotation_order() const;
	INLINE void set_world_anchor(const LPoint3f& anchor);
	INLINE const LPoint3f& get_world_anchor() const;
	INLINE void set_world_axes(const ValueList_LVector3f& vectors);
	INLINE ValueList_LVector3f get_world_axes() const;
	INLINE void set_use_reference_frame_a(bool value);
	INLINE bool get_use_reference_frame_a() const;
	// Python Properties
	MAKE_PROPERTY(constraint_type, get_constraint_type, set_constraint_type);
	MAKE_PROPERTY(use_world_reference, get_use_world_reference, set_use_world_reference);
	MAKE_PROPERTY(pivot_references, get_pivot_references, set_pivot_references);
	MAKE_PROPERTY(axis_references, get_axis_references, set_axis_references);
	MAKE_PROPERTY(ratio, get_ratio, set_ratio);
	MAKE_PROPERTY(rotation_references, get_rotation_references, set_rotation_references);
	MAKE_PROPERTY(rotation_order, get_rotation_order, set_rotation_order);
	MAKE_PROPERTY(world_anchor, get_world_anchor, set_world_anchor);
	MAKE_PROPERTY(world_axes, get_world_axes, set_world_axes);
	MAKE_PROPERTY(use_reference_frame_a, get_use_reference_frame_a, set_use_reference_frame_a);
	///@}

	/**
	 * \name DYNAMIC PARAMETERS' GETTERS/SETTERS
	 */
	///@{
	INLINE void set_disable_objects_collisions(bool value);
	INLINE bool get_disable_objects_collisions() const;
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	INLINE PyObject* get_constraints_attrs();
	// Python Properties
	MAKE_PROPERTY(constraints_attrs, get_constraints_attrs);
#else
	inline TypedConstraintAttrs* get_constraints_attrs();
#endif //PYTHON_BUILD
	// Python Properties
	MAKE_PROPERTY(disable_objects_collisions, get_disable_objects_collisions, set_disable_objects_collisions);
	///@}

	/**
	 * \name OUTPUT
	 */
	///@{
	void output(ostream &out) const;
	///@}

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	/**
	 * \name PYTHON UPDATE CALLBACK
	 */
	///@{
	void set_update_callback(PyObject *clbk);
	///@}
#else
	/**
	 * \name C++ UPDATE CALLBACK
	 */
	///@{
	typedef void (*UPDATECALLBACKFUNC)(PT(BT3Constraint));
	void set_update_callback(UPDATECALLBACKFUNC value);
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name TypedWritable API
	 */
	///@{
	void post_process_from_bam();
	///@}

public:
	/**
	 * \name C++ ONLY
	 * Library & support low level related methods.
	 */
	///@{
	inline btTypedConstraint& get_constraint();
	inline const btTypedConstraint& get_constraint() const;
	///@}

	// Explicitly deleted copy constructor and copy assignment operator.
	BT3Constraint(const BT3Constraint&) = delete;
	BT3Constraint& operator=(const BT3Constraint&) = delete;

#ifndef CPPPARSER
protected:
	friend void unref_delete<BT3Constraint>(BT3Constraint*);
	friend class GamePhysicsManager;

	BT3Constraint(const string& name);
	virtual ~BT3Constraint();

private:
	///The owner objects this BT3Constraint is attached to.
	NodePath mObjectANP, mObjectBNP;
	///The reference node path.
	NodePath mReferenceNP;
	///The setup flag.
	bool mSetup;
	///Bullet constraint reference.
	btTypedConstraint* mConstraint;
	///@{
	///Physical parameters.
	//construction
	bt3::ConstraintData mConstraintData;
	//dynamic
#if defined(PYTHON_BUILD)
	PyObject *mConstraintAttrsPy;
#endif //PYTHON_BUILD
	TypedConstraintAttrs* mConstraintAttrs;
	///@}

	inline void do_reset();
	void do_initialize();
	void do_finalize();

	///Helpers.
	BT3ConstraintType do_get_constraint_type(const string& name) const;
	BT3RotateOrder do_get_rotation_order(const string& name) const;
	void do_get_vec_base_values(const string& references, LVecBase3f& ref1,
			LVecBase3f& ref2);
	void do_convert_references_from_global_to_local();
	TypedConstraintAttrs* do_create_constraint_attrs();
#if defined(PYTHON_BUILD)
	PyObject *do_create_constraint_attrs_py();
#endif //PYTHON_BUILD

#if defined(PYTHON_BUILD)
	/**
	 * \name Python callback.
	 */
	///@{
	PyObject *mSelf, *mUpdateCallback;
	///@}
#else
	/**
	 * \name C++ callback.
	 */
	///@{
	UPDATECALLBACKFUNC mUpdateCallback;
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name SERIALIZATION ONLY SETTINGS.
	 */
	// persistent storage for serialized data
	bool mBuildFromBam;
	Datagram mOutDatagram;
	///@}
#endif //CPPPARSER

public:
	/**
	 * \name TypedWritable API
	 */
	///@{
	static void register_with_read_factory();
	virtual void write_datagram (BamWriter *manager, Datagram &dg) override;
	virtual int complete_pointers(TypedWritable **p_list, BamReader *manager) override;
	///@}

#ifndef CPPPARSER
protected:
	static TypedWritable *make_from_bam(const FactoryParams &params);
	virtual void fillin(DatagramIterator &scan, BamReader *manager) override;
#endif //CPPPARSER

TYPED_OBJECT_API_DECL(BT3Constraint,PandaNode)
};

INLINE ostream &operator << (ostream &out, const BT3Constraint & rigid_body);

///inline
#include "bt3Constraint.I"

#endif /* PHYSICS_SOURCE_BT3CONSTRAINT_H_ */
