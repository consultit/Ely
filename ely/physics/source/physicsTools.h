/**
 * \file physicsTools.h
 *
 * \date 2017-04-29
 * \author consultit
 */

#ifndef PHYSICS_SOURCE_PHYSICSTOOLS_H_
#define PHYSICS_SOURCE_PHYSICSTOOLS_H_

#ifndef CPPPARSER
#include "support/physics.h"
#include "bullet3/src/BulletDynamics/ConstraintSolver/btGeneric6DofConstraint.h"
#include "bullet3/src/BulletDynamics/ConstraintSolver/btGeneric6DofSpring2Constraint.h"
#endif //CPPPARSER

///BT3PhysicsInfo: Equivalent to bt3::PhysicsInfo.
struct EXPCL_PHYSICS BT3PhysicsInfo
{
PUBLISHED:
	enum BT3WorldType: unsigned char
	{
#ifndef CPPPARSER
		SOFT_RIGID_WORLD = static_cast<unsigned char>(bt3::PhysicsInfo::WorldType::SoftRigid),
		DISCRETE_WORLD = static_cast<unsigned char>(bt3::PhysicsInfo::WorldType::Discrete),
		DISCRETE_MT_WORLD = static_cast<unsigned char>(bt3::PhysicsInfo::WorldType::DiscreteMt),
		MULTIBODY_WORLD = static_cast<unsigned char>(bt3::PhysicsInfo::WorldType::MultiBody),
		SOFT_MULTIBODY_WORLD = static_cast<unsigned char>(bt3::PhysicsInfo::WorldType::SoftMultiBody),
		SIMPLE_WORLD = static_cast<unsigned char>(bt3::PhysicsInfo::WorldType::Simple),
		FRACTURE_WORLD = static_cast<unsigned char>(bt3::PhysicsInfo::WorldType::Fracture),
#else
		SOFT_RIGID_WORLD,DISCRETE_WORLD,DISCRETE_MT_WORLD,MULTIBODY_WORLD,
		SOFT_MULTIBODY_WORLD,SIMPLE_WORLD,FRACTURE_WORLD,
#endif //CPPPARSER
	};
	enum BT3BroadphaseType: unsigned char
	{
#ifndef CPPPARSER
		AXIS_SWEEP3_BROADPHASE = static_cast<unsigned char>(bt3::PhysicsInfo::BroadphaseType::AxisSweep3),
		AXIS_SWEEP3_32BIT_BROADPHASE = static_cast<unsigned char>(bt3::PhysicsInfo::BroadphaseType::AxisSweep3_32Bit),
		DBVT_BROADPHASE = static_cast<unsigned char>(bt3::PhysicsInfo::BroadphaseType::Dbvt),
		SIMPLE_BROADPHASE = static_cast<unsigned char>(bt3::PhysicsInfo::BroadphaseType::Simple),
#else
		AXIS_SWEEP3_BROADPHASE,AXIS_SWEEP3_32BIT_BROADPHASE,
		DBVT_BROADPHASE,SIMPLE_BROADPHASE,
#endif //CPPPARSER
	};
	enum BT3ConstraintSolverType: unsigned char
	{
#ifndef CPPPARSER
		SEQUENTIAL_IMPULSE_CSOLVER = static_cast<unsigned char>(bt3::PhysicsInfo::ConstraintSolverType::SequentialImpulse),
		SEQUENTIAL_IMPULSE_CSOLVER_MT = static_cast<unsigned char>(bt3::PhysicsInfo::ConstraintSolverType::SequentialImpulseMt),
		MLCP_CSOLVER = static_cast<unsigned char>(bt3::PhysicsInfo::ConstraintSolverType::MLCP),
		MULTIBODY_CSOLVER = static_cast<unsigned char>(bt3::PhysicsInfo::ConstraintSolverType::MultiBody),
		NNCG_CSOLVER = static_cast<unsigned char>(bt3::PhysicsInfo::ConstraintSolverType::NNCG),
#else
		SEQUENTIAL_IMPULSE_CSOLVER,SEQUENTIAL_IMPULSE_CSOLVER_MT,MLCP_CSOLVER,MULTIBODY_CSOLVER,NNCG_CSOLVER,
#endif //CPPPARSER
	};
	enum BT3MLCPSolverType: unsigned char
	{
#ifndef CPPPARSER
		DANTZIG_MLCPSOLVER = static_cast<unsigned char>(bt3::PhysicsInfo::MLCPSolverType::Dantzig),
		LEMKE_MLCPSOLVER = static_cast<unsigned char>(bt3::PhysicsInfo::MLCPSolverType::Lemke),
		PROJECTED_GAUSS_SEIDEL_MLCPSOLVER = static_cast<unsigned char>(bt3::PhysicsInfo::MLCPSolverType::ProjectedGaussSeidel),
#else
		DANTZIG_MLCPSOLVER,LEMKE_MLCPSOLVER,PROJECTED_GAUSS_SEIDEL_MLCPSOLVER,
#endif //CPPPARSER
	};
	enum BT3SolverTypes: unsigned char
	{
#ifndef CPPPARSER
		DEFAULT_SOLVER = btSoftBodySolver::SolverTypes::DEFAULT_SOLVER,
		CPU_SOLVER = btSoftBodySolver::SolverTypes::CPU_SOLVER,
		CL_SOLVER = btSoftBodySolver::SolverTypes::CL_SOLVER,
		CL_SIMD_SOLVER = btSoftBodySolver::SolverTypes::CL_SIMD_SOLVER,
		DX_SOLVER = btSoftBodySolver::SolverTypes::DX_SOLVER,
		DX_SIMD_SOLVER = btSoftBodySolver::SolverTypes::DX_SIMD_SOLVER,
#else
		DEFAULT_SOLVER,CPU_SOLVER,CL_SOLVER,CL_SIMD_SOLVER,DX_SOLVER,
		DX_SIMD_SOLVER,
#endif //CPPPARSER
	};
	BT3PhysicsInfo();
	BT3PhysicsInfo(
			BT3WorldType worldType,
			BT3BroadphaseType broadPhaseType,
			BT3ConstraintSolverType solverType,
			BT3MLCPSolverType mlcpSolverType,
			BT3SolverTypes softBodySolverType,
			int numPoolMtSolvers,
			const LVecBase3f& worldDims,
			unsigned short int maxHandles,
			float airDensity,
			float waterDensity,
			float waterOffset,
			float maxDisplacement,
			const LVector3f& waterNormal,
			///Multi-Threading
			int persistentManifoldPoolSize,
			int collisionAlgorithmPoolSize,
			int grainSize
			);
	INLINE ~BT3PhysicsInfo();
	INLINE void set_world_type(BT3WorldType value);
	INLINE BT3WorldType get_world_type() const;
	INLINE void set_broad_phase_type(BT3BroadphaseType value);
	INLINE BT3BroadphaseType get_broad_phase_type() const;
	INLINE void set_solver_type(BT3ConstraintSolverType value);
	INLINE BT3ConstraintSolverType get_solver_type() const;
	INLINE void set_mlcp_solver_type(BT3MLCPSolverType value);
	INLINE BT3MLCPSolverType get_mlcp_solver_type() const;
	INLINE void set_soft_body_solver_type(BT3SolverTypes value);
	INLINE BT3SolverTypes get_soft_body_solver_type() const;
	INLINE void set_num_pool_mt_solvers(int value);
	INLINE int get_num_pool_mt_solvers() const;
	INLINE void set_world_dims(const LVecBase3f& value);
	INLINE LVecBase3f get_world_dims() const;
	INLINE void set_max_handles(unsigned short int value);
	INLINE unsigned short int get_max_handles() const;
	INLINE void set_air_density(float value);
	INLINE float get_air_density() const;
	INLINE void set_water_density(float value);
	INLINE float get_water_density() const;
	INLINE void set_water_offset(float value);
	INLINE float get_water_offset() const;
	INLINE void set_max_displacement(float value);
	INLINE float get_max_displacement() const;
	INLINE void set_water_normal(const LVector3f& value);
	INLINE LVector3f get_water_normal() const;
	///Multi-Threading
	INLINE void set_max_persistent_manifold_pool_size(int value);
	INLINE int get_max_persistent_manifold_pool_size() const;
	INLINE void set_max_collision_algorithm_pool_size(int value);
	INLINE int get_max_collision_algorithm_pool_size() const;
	INLINE void set_grain_size(int value);
	INLINE int get_grain_size() const;
	// Python Properties
	MAKE_PROPERTY(world_type, get_world_type, set_world_type);
	MAKE_PROPERTY(broad_phase_type, get_broad_phase_type, set_broad_phase_type);
	MAKE_PROPERTY(solver_type, get_solver_type, set_solver_type);
	MAKE_PROPERTY(mlcp_solver_type, get_mlcp_solver_type, set_mlcp_solver_type);
	MAKE_PROPERTY(soft_body_solver_type, get_soft_body_solver_type, set_soft_body_solver_type);
	MAKE_PROPERTY(num_pool_mt_solvers, get_num_pool_mt_solvers, set_num_pool_mt_solvers);
	MAKE_PROPERTY(world_dims, get_world_dims, set_world_dims);
	MAKE_PROPERTY(max_handles, get_max_handles, set_max_handles);
	MAKE_PROPERTY(air_density, get_air_density, set_air_density);
	MAKE_PROPERTY(water_density, get_water_density, set_water_density);
	MAKE_PROPERTY(water_offset, get_water_offset, set_water_offset);
	MAKE_PROPERTY(max_displacement, get_max_displacement, set_max_displacement);
	MAKE_PROPERTY(water_normal, get_water_normal, set_water_normal);
	///Multi-Threading
	MAKE_PROPERTY(max_persistent_manifold_pool_size, get_max_persistent_manifold_pool_size, set_max_persistent_manifold_pool_size);
	MAKE_PROPERTY(max_collision_algorithm_pool_size, get_max_collision_algorithm_pool_size, set_max_collision_algorithm_pool_size);
	MAKE_PROPERTY(grain_size, get_grain_size, set_grain_size);

#ifndef CPPPARSER
	const bt3::PhysicsInfo& get_physics_info() const
	{
		return mInfo;
	}
private:
	bt3::PhysicsInfo mInfo;
#endif //CPPPARSER
};

///BT3SoftBodyMaterial: wrapper to btSoftBody::Material.
struct EXPCL_PHYSICS BT3SoftBodyMaterial
{
PUBLISHED:
	enum BT3fMaterial: int
	{
#ifndef CPPPARSER
		DebugDraw = btSoftBody::fMaterial::DebugDraw,
		Default = btSoftBody::fMaterial::Default,
		END = btSoftBody::fMaterial::END
#else
		DebugDraw,Default,END
#endif //CPPPARSER
	};
	INLINE ~BT3SoftBodyMaterial();
	INLINE void set_linear_stiffness_coefficient(float value);
	INLINE float get_linear_stiffness_coefficient() const;
	INLINE void set_angular_stiffness_coefficient(float value);
	INLINE float get_angular_stiffness_coefficient() const;
	INLINE void set_volume_stiffness_coefficient(float value);
	INLINE float get_volume_stiffness_coefficient() const;
	INLINE void set_flags(int value);
	INLINE int get_flags() const;
	// Python Properties
	MAKE_PROPERTY(linear_stiffness_coefficient, get_linear_stiffness_coefficient, set_linear_stiffness_coefficient);
	MAKE_PROPERTY(angular_stiffness_coefficient, get_angular_stiffness_coefficient, set_angular_stiffness_coefficient);
	MAKE_PROPERTY(volume_stiffness_coefficient, get_volume_stiffness_coefficient, set_volume_stiffness_coefficient);
	MAKE_PROPERTY(flags, get_flags, set_flags);
	void output(ostream &out) const;

public:
	BT3SoftBodyMaterial() : _material(_dummy)
	{
	}
	BT3SoftBodyMaterial(btSoftBody::Material& material) :
		_material(material)
	{
	}
	static btSoftBody::Material _dummy;
#ifndef CPPPARSER
	operator btSoftBody::Material&() const
	{
		return _material;
	}

private:
	btSoftBody::Material& _material;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3SoftBodyMaterial & material);

///BT3SoftBodyConfig: wrapper to btSoftBody::Config.
struct EXPCL_PHYSICS BT3SoftBodyConfig
{
PUBLISHED:
	enum BT3AeroModel: unsigned char
	{
	#ifndef CPPPARSER
		Vertex_Point = btSoftBody::eAeroModel::_::V_Point,
		Vertex_TwoSided = btSoftBody::eAeroModel::_::V_TwoSided,
		Vertex_TwoSidedLiftDrag = btSoftBody::eAeroModel::_::V_TwoSidedLiftDrag,
		Vertex_OneSided = btSoftBody::eAeroModel::_::V_OneSided,
		Face_TwoSided = btSoftBody::eAeroModel::_::F_TwoSided,
		Face_TwoSidedLiftDrag = btSoftBody::eAeroModel::_::F_TwoSidedLiftDrag,
		Face_OneSided = btSoftBody::eAeroModel::_::F_OneSided,
		AeroModel_END = btSoftBody::eAeroModel::_::END
	#else
		Vertex_Point,Vertex_TwoSided,Vertex_TwoSidedLiftDrag,Vertex_OneSided,
		Face_TwoSided,Face_TwoSidedLiftDrag,Face_OneSided,AeroModel_END
	#endif //CPPPARSER
	};
	enum BT3Collision: unsigned short
	{
	#ifndef CPPPARSER
		Rigid_vs_SoftMask = btSoftBody::fCollision::RVSmask,
		SDF_Rigid_Soft = btSoftBody::fCollision::SDF_RS,
		Cluster_Rigid_Soft = btSoftBody::fCollision::CL_RS,
		Soft_SoftMask = btSoftBody::fCollision::SVSmask,
		Vertex_Face_Soft_Soft = btSoftBody::fCollision::VF_SS,
		Cluster_Soft_Soft = btSoftBody::fCollision::CL_SS,
		Cluster_SELF = btSoftBody::fCollision::CL_SELF,
		Collision_Default = btSoftBody::fCollision::SDF_RS,
		Collision_END = btSoftBody::fCollision::END
	#else
		Rigid_vs_SoftMask,SDF_Rigid_Soft,Cluster_Rigid_Soft,Soft_SoftMask,
		Vertex_Face_Soft_Soft,Cluster_Soft_Soft,Cluster_SELF,Collision_Default,
		Collision_END
	#endif //CPPPARSER
	};

	INLINE ~BT3SoftBodyConfig();
	INLINE void set_aerodynamic_model(BT3AeroModel value);
	INLINE BT3AeroModel get_aerodynamic_model() const;
	INLINE void set_velocities_correction_factor(float value);
	INLINE float get_velocities_correction_factor() const;
	INLINE void set_damping_coefficient(float value);
	INLINE float get_damping_coefficient() const;
	INLINE void set_drag_coefficient(float value);
	INLINE float get_drag_coefficient() const;
	INLINE void set_lift_coefficient(float value);
	INLINE float get_lift_coefficient() const;
	INLINE void set_pressure_coefficient(float value);
	INLINE float get_pressure_coefficient() const;
	INLINE void set_volume_conversation_coefficient(float value);
	INLINE float get_volume_conversation_coefficient() const;
	INLINE void set_dynamic_friction_coefficient(float value);
	INLINE float get_dynamic_friction_coefficient() const;
	INLINE void set_pose_matching_coefficient(float value);
	INLINE float get_pose_matching_coefficient() const;
	INLINE void set_rigid_contacts_hardness(float value);
	INLINE float get_rigid_contacts_hardness() const;
	INLINE void set_kinetic_contacts_hardness(float value);
	INLINE float get_kinetic_contacts_hardness() const;
	INLINE void set_soft_contacts_hardness(float value);
	INLINE float get_soft_contacts_hardness() const;
	INLINE void set_anchors_hardness(float value);
	INLINE float get_anchors_hardness() const;
	INLINE void set_soft_rigid_hardness(float value);
	INLINE float get_soft_rigid_hardness() const;
	INLINE void set_soft_kinetic_hardness(float value);
	INLINE float get_soft_kinetic_hardness() const;
	INLINE void set_soft_soft_hardness(float value);
	INLINE float get_soft_soft_hardness() const;
	INLINE void set_soft_rigid_impulse_split(float value);
	INLINE float get_soft_rigid_impulse_split() const;
	INLINE void set_soft_kinetic_impulse_split(float value);
	INLINE float get_soft_kinetic_impulse_split() const;
	INLINE void set_soft_soft_impulse_split(float value);
	INLINE float get_soft_soft_impulse_split() const;
	INLINE void set_maximum_volume_ratio_pose(float value);
	INLINE float get_maximum_volume_ratio_pose() const;
	INLINE void set_time_scale(float value);
	INLINE float get_time_scale() const;
	INLINE void set_velocities_solver_iterations(int value);
	INLINE int get_velocities_solver_iterations() const;
	INLINE void set_positions_solver_iterations(int value);
	INLINE int get_positions_solver_iterations() const;
	INLINE void set_drift_solver_iterations(int value);
	INLINE int get_drift_solver_iterations() const;
	INLINE void set_cluster_solver_iterations(int value);
	INLINE int get_cluster_solver_iterations() const;
	INLINE void set_collisions_flags(int value);
	INLINE int get_collisions_flags() const;
	// Python Properties
	MAKE_PROPERTY(aerodynamic_model, get_aerodynamic_model, set_aerodynamic_model);
	MAKE_PROPERTY(velocities_correction_factor, get_velocities_correction_factor, set_velocities_correction_factor);
	MAKE_PROPERTY(damping_coefficient, get_damping_coefficient, set_damping_coefficient);
	MAKE_PROPERTY(drag_coefficient, get_drag_coefficient, set_drag_coefficient);
	MAKE_PROPERTY(lift_coefficient, get_lift_coefficient, set_lift_coefficient);
	MAKE_PROPERTY(pressure_coefficient, get_pressure_coefficient, set_pressure_coefficient);
	MAKE_PROPERTY(volume_conversation_coefficient, get_volume_conversation_coefficient, set_volume_conversation_coefficient);
	MAKE_PROPERTY(dynamic_friction_coefficient, get_dynamic_friction_coefficient, set_dynamic_friction_coefficient);
	MAKE_PROPERTY(pose_matching_coefficient, get_pose_matching_coefficient, set_pose_matching_coefficient);
	MAKE_PROPERTY(rigid_contacts_hardness, get_rigid_contacts_hardness, set_rigid_contacts_hardness);
	MAKE_PROPERTY(kinetic_contacts_hardness, get_kinetic_contacts_hardness, set_kinetic_contacts_hardness);
	MAKE_PROPERTY(soft_contacts_hardness, get_soft_contacts_hardness, set_soft_contacts_hardness);
	MAKE_PROPERTY(anchors_hardness, get_anchors_hardness, set_anchors_hardness);
	MAKE_PROPERTY(soft_rigid_hardness, get_soft_rigid_hardness, set_soft_rigid_hardness);
	MAKE_PROPERTY(soft_kinetic_hardness, get_soft_kinetic_hardness, set_soft_kinetic_hardness);
	MAKE_PROPERTY(soft_soft_hardness, get_soft_soft_hardness, set_soft_soft_hardness);
	MAKE_PROPERTY(soft_rigid_impulse_split, get_soft_rigid_impulse_split, set_soft_rigid_impulse_split);
	MAKE_PROPERTY(soft_kinetic_impulse_split, get_soft_kinetic_impulse_split, set_soft_kinetic_impulse_split);
	MAKE_PROPERTY(soft_soft_impulse_split, get_soft_soft_impulse_split, set_soft_soft_impulse_split);
	MAKE_PROPERTY(maximum_volume_ratio_pose, get_maximum_volume_ratio_pose, set_maximum_volume_ratio_pose);
	MAKE_PROPERTY(time_scale, get_time_scale, set_time_scale);
	MAKE_PROPERTY(velocities_solver_iterations, get_velocities_solver_iterations, set_velocities_solver_iterations);
	MAKE_PROPERTY(positions_solver_iterations, get_positions_solver_iterations, set_positions_solver_iterations);
	MAKE_PROPERTY(drift_solver_iterations, get_drift_solver_iterations, set_drift_solver_iterations);
	MAKE_PROPERTY(cluster_solver_iterations, get_cluster_solver_iterations, set_cluster_solver_iterations);
	MAKE_PROPERTY(collisions_flags, get_collisions_flags, set_collisions_flags);

	void output(ostream &out) const;

public:
	BT3SoftBodyConfig(btSoftBody::Config& config) :
		_config(config)
	{
	}
	static btSoftBody::Config _dummy;
#ifndef CPPPARSER
	operator btSoftBody::Config&() const
	{
		return _config;
	}

private:
	btSoftBody::Config& _config;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3SoftBodyConfig & config);

///BT3SoftBodyNode: wrapper to btSoftBody::Node.
struct EXPCL_PHYSICS BT3SoftBodyNode
{
PUBLISHED:
	INLINE ~BT3SoftBodyNode();
	INLINE LPoint3f get_position() const;
	INLINE LVector3f get_velocity() const;
	INLINE LVector3f get_normal() const;
	INLINE float get_inv_mass() const;
	INLINE float get_area() const;
	INLINE bool get_attached() const;
	// Python Properties
	MAKE_PROPERTY(position, get_position);
	MAKE_PROPERTY(velocity, get_velocity);
	MAKE_PROPERTY(normal, get_normal);
	MAKE_PROPERTY(inv_mass, get_inv_mass);
	MAKE_PROPERTY(area, get_area);
	MAKE_PROPERTY(attached, get_attached);

	void output(ostream &out) const;

public:
	BT3SoftBodyNode() :	_node(_dummy), _materialUid(0)
	{
	}
	BT3SoftBodyNode(btSoftBody::Node& node) :
		_node(node), _materialUid(0)
	{
	}
	static btSoftBody::Node _dummy;
#ifndef CPPPARSER
	operator btSoftBody::Node&() const
	{
		return _node;
	}

private:
	btSoftBody::Node& _node;
	unsigned int _materialUid;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
	inline void set_material_uid(unsigned int uid);
	inline unsigned int get_material_uid() const;
};
INLINE ostream &operator << (ostream &out, const BT3SoftBodyNode & node);

///BT3SoftBodyAJointControl: wrapper to btSoftBody::AJoint::IControl.
struct EXPCL_PHYSICS BT3SoftBodyAJointControl: public btSoftBody::AJoint::IControl
{
PUBLISHED:
	enum BT3ControlType
	{
		MOTOR,
		STEER,
		DEFAULT
	};
	INLINE BT3SoftBodyAJointControl(BT3ControlType type = DEFAULT, float sign = 1);
	INLINE ~BT3SoftBodyAJointControl();
	INLINE BT3ControlType get_type() const;
	INLINE void set_goal(float goal);
	INLINE float get_goal() const;
	INLINE void set_max_torque(float maxtorque);
	INLINE float get_max_torque() const;
	INLINE void set_angle(float angle);
	INLINE float get_angle() const;
	INLINE void set_sign(float sign);
	INLINE float get_sign() const;
	// Python Properties
	MAKE_PROPERTY(type, get_type);
	MAKE_PROPERTY(goal, get_goal, set_goal);
	MAKE_PROPERTY(max_torque, get_max_torque, set_max_torque);
	MAKE_PROPERTY(angle, get_angle, set_angle);
	MAKE_PROPERTY(sign, get_sign, set_sign);

	void output(ostream &out) const;

public:
	void Prepare(btSoftBody::AJoint* joint);
	float Speed(btSoftBody::AJoint *joint, float current);

#ifndef CPPPARSER
private:
	BT3ControlType _type;
	float _goal, _maxtorque, _angle, _sign;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3SoftBodyAJointControl & control);

///BT3RotationalLimitMotor: wrapper to btRotationalLimitMotor.
struct EXPCL_PHYSICS BT3RotationalLimitMotor
{
PUBLISHED:
	INLINE ~BT3RotationalLimitMotor();
	INLINE void set_lower_limit(float value);
	INLINE float get_lower_limit() const;
	INLINE void set_upper_limit(float value);
	INLINE float get_upper_limit() const;
	INLINE void set_target_velocity(float value);
	INLINE float get_target_velocity() const;
	INLINE void set_max_motor_force(float value);
	INLINE float get_max_motor_force() const;
	INLINE void set_max_limit_force(float value);
	INLINE float get_max_limit_force() const;
	INLINE void set_damping(float value);
	INLINE float get_damping() const;
	INLINE void set_limit_softness(float value);
	INLINE float get_limit_softness() const;
	INLINE void set_normal_cfm(float value);
	INLINE float get_normal_cfm() const;
	INLINE void set_stop_erp(float value);
	INLINE float get_stop_erp() const;
	INLINE void set_stop_cfm(float value);
	INLINE float get_stop_cfm() const;
	INLINE void set_bounce(float value);
	INLINE float get_bounce() const;
	INLINE void set_enable_motor(bool value);
	INLINE bool get_enable_motor() const;
	INLINE float get_current_limit_error() const;
	INLINE float get_current_position() const;
	INLINE int get_current_limit() const;
	INLINE float get_accumulated_impulse() const;
	// Python Properties
	MAKE_PROPERTY(low_limit, get_lower_limit, set_lower_limit);
	MAKE_PROPERTY(high_limit, get_upper_limit, set_upper_limit);
	MAKE_PROPERTY(target_velocity, get_target_velocity, set_target_velocity);
	MAKE_PROPERTY(max_motor_force, get_max_motor_force, set_max_motor_force);
	MAKE_PROPERTY(max_limit_force, get_max_limit_force, set_max_limit_force);
	MAKE_PROPERTY(damping, get_damping, set_damping);
	MAKE_PROPERTY(limit_softness, get_limit_softness, set_limit_softness);
	MAKE_PROPERTY(normal_cfm, get_normal_cfm, set_normal_cfm);
	MAKE_PROPERTY(stop_erp, get_stop_erp, set_stop_erp);
	MAKE_PROPERTY(stop_cfm, get_stop_cfm, set_stop_cfm);
	MAKE_PROPERTY(bounce, get_bounce, set_bounce);
	MAKE_PROPERTY(enable_motor, get_enable_motor, set_enable_motor);
	MAKE_PROPERTY(current_limit_error, get_current_limit_error);
	MAKE_PROPERTY(current_position, get_current_position);
	MAKE_PROPERTY(current_limit, get_current_limit);
	MAKE_PROPERTY(accumulated_impulse, get_accumulated_impulse);

	void output(ostream &out) const;

public:
	BT3RotationalLimitMotor(btRotationalLimitMotor& motor) :
		_motor(motor)
	{
	}
	static btRotationalLimitMotor _dummy;
#ifndef CPPPARSER
	operator btRotationalLimitMotor&() const
	{
		return _motor;
	}

private:
	btRotationalLimitMotor& _motor;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3RotationalLimitMotor & motor);

///BT3TranslationalLimitMotor: wrapper to btTranslationalLimitMotor.
//fixme: Check carefully whether the physical quantities are or not dependent on
//the axis direction; particularly: upper/lower limits, erp, cfm, target
//velocity, max motor force, accumulated impulse, current limit error, current
//linear diff.
struct EXPCL_PHYSICS BT3TranslationalLimitMotor
{
PUBLISHED:
	INLINE ~BT3TranslationalLimitMotor();
	INLINE void set_lower_limit(const LVector3f& value);
	INLINE LVector3f get_lower_limit() const;
	INLINE void set_upper_limit(const LVector3f& value);
	INLINE LVector3f get_upper_limit() const;
	INLINE void set_limit_softness(float value);
	INLINE float get_limit_softness() const;
	INLINE void set_damping(float value);
	INLINE float get_damping() const;
	INLINE void set_restitution(float value);
	INLINE float get_restitution() const;
	INLINE void set_normal_cfm(const LVecBase3f& value);
	INLINE LVecBase3f get_normal_cfm() const;
	INLINE void set_stop_erp(const LVecBase3f& value);
	INLINE LVecBase3f get_stop_erp() const;
	INLINE void set_stop_cfm(const LVecBase3f& value);
	INLINE LVecBase3f get_stop_cfm() const;
	INLINE void set_enable_motor(int index, bool value);//index:0=x,1=y,2=z
	INLINE bool get_enable_motor(int index) const;//index:0=x,1=y,2=z
	INLINE void set_target_velocity(const LVector3f& value);
	INLINE LVector3f get_target_velocity() const;
	INLINE void set_max_motor_force(const LVecBase3f& value);
	INLINE LVecBase3f get_max_motor_force() const;
	INLINE LVector3f get_accumulated_impulse() const;
	INLINE LVector3f get_current_limit_error() const;
	INLINE LVector3f get_current_linear_diff() const;
	INLINE int get_current_limit(int index) const;//index:0=x,1=y,2=z
	// Python Properties
	MAKE_PROPERTY(lower_limit, get_lower_limit, set_lower_limit);
	MAKE_PROPERTY(upper_limit, get_limit_softness, set_upper_limit);
	MAKE_PROPERTY(limit_softness, get_limit_softness, set_limit_softness);
	MAKE_PROPERTY(damping, get_damping, set_damping);
	MAKE_PROPERTY(restitution, get_restitution, set_restitution);
	MAKE_PROPERTY(normal_cfm, get_normal_cfm, set_normal_cfm);
	MAKE_PROPERTY(stop_erp, get_stop_erp, set_stop_erp);
	MAKE_PROPERTY(stop_cfm, get_stop_cfm, set_stop_cfm);
	MAKE_PROPERTY(target_velocity, get_target_velocity, set_target_velocity);
	MAKE_PROPERTY(max_motor_force, get_max_motor_force, set_max_motor_force);
	MAKE_PROPERTY(accumulated_impulse, get_accumulated_impulse);
	MAKE_PROPERTY(current_limit_error, get_current_limit_error);
	MAKE_PROPERTY(current_linear_diff, get_current_linear_diff);

	void output(ostream &out) const;

public:
	BT3TranslationalLimitMotor(btTranslationalLimitMotor& motor) :
		_motor(motor)
	{
	}
	static btTranslationalLimitMotor _dummy;
#ifndef CPPPARSER
	operator btTranslationalLimitMotor&() const
	{
		return _motor;
	}

private:
	btTranslationalLimitMotor& _motor;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3TranslationalLimitMotor & motor);

///BT3RotationalLimitMotor2: wrapper to btRotationalLimitMotor2.
struct EXPCL_PHYSICS BT3RotationalLimitMotor2
{
PUBLISHED:
	INLINE ~BT3RotationalLimitMotor2();
	INLINE void set_lower_limit(float value);
	INLINE float get_lower_limit() const;
	INLINE void set_upper_limit(float value);
	INLINE float get_upper_limit() const;
	INLINE void set_target_velocity(float value);
	INLINE float get_target_velocity() const;
	INLINE void set_max_motor_force(float value);
	INLINE float get_max_motor_force() const;
	INLINE void set_servo_motor(bool value);
	INLINE bool get_servo_motor() const;
	INLINE void set_servo_target(float value);
	INLINE float get_servo_target() const;
	INLINE void set_motor_erp(float value);
	INLINE float get_motor_erp() const;
	INLINE void set_motor_cfm(float value);
	INLINE float get_motor_cfm() const;
	INLINE void set_stop_erp(float value);
	INLINE float get_stop_erp() const;
	INLINE void set_stop_cfm(float value);
	INLINE float get_stop_cfm() const;
	INLINE void set_bounce(float value);
	INLINE float get_bounce() const;
	INLINE void set_enable_motor(bool value);
	INLINE bool get_enable_motor() const;
	INLINE void set_enable_spring(bool value);
	INLINE bool get_enable_spring() const;
	INLINE void set_spring_stiffness(float value);
	INLINE float get_spring_stiffness() const;
	INLINE void set_spring_stiffness_limited(bool value);
	INLINE bool get_spring_stiffness_limited() const;
	INLINE void set_spring_damping(float value);
	INLINE float get_spring_damping() const;
	INLINE void set_spring_damping_limited(bool value);
	INLINE bool get_spring_damping_limited() const;
	INLINE void set_equilibrium_point(bool value);
	INLINE bool get_equilibrium_point() const;
	INLINE float get_current_limit_error() const;
	INLINE float get_current_position() const;
	INLINE int get_current_limit() const;
	INLINE float get_current_limit_error_upper() const;
	// Python Properties
	MAKE_PROPERTY(low_limit, get_lower_limit, set_lower_limit);
	MAKE_PROPERTY(high_limit, get_upper_limit, set_upper_limit);
	MAKE_PROPERTY(target_velocity, get_target_velocity, set_target_velocity);
	MAKE_PROPERTY(max_motor_force, get_max_motor_force, set_max_motor_force);
	MAKE_PROPERTY(servo_motor, get_servo_motor, set_servo_motor);
	MAKE_PROPERTY(servo_target, get_servo_target, set_servo_target);
	MAKE_PROPERTY(motor_erp, get_motor_erp, set_motor_erp);
	MAKE_PROPERTY(motor_cfm, get_motor_cfm, set_motor_cfm);
	MAKE_PROPERTY(stop_erp, get_stop_erp, set_stop_erp);
	MAKE_PROPERTY(stop_cfm, get_stop_cfm, set_stop_cfm);
	MAKE_PROPERTY(bounce, get_bounce, set_bounce);
	MAKE_PROPERTY(enable_motor, get_enable_motor, set_enable_motor);
	MAKE_PROPERTY(enable_spring, get_enable_spring, set_enable_spring);
	MAKE_PROPERTY(spring_stiffness, get_spring_stiffness, set_spring_stiffness);
	MAKE_PROPERTY(spring_stiffness_limited, get_spring_stiffness_limited, set_spring_stiffness_limited);
	MAKE_PROPERTY(spring_damping, get_spring_damping, set_spring_damping);
	MAKE_PROPERTY(spring_damping_limited, get_spring_damping_limited, set_spring_damping_limited);
	MAKE_PROPERTY(equilibrium_point, get_equilibrium_point, set_equilibrium_point);
	MAKE_PROPERTY(current_limit_error, get_current_limit_error);
	MAKE_PROPERTY(current_position, get_current_position);
	MAKE_PROPERTY(current_limit, get_current_limit);
	MAKE_PROPERTY(current_limit_error_upper, get_current_limit_error_upper);

	void output(ostream &out) const;

public:
	BT3RotationalLimitMotor2(btRotationalLimitMotor2& motor) :
		_motor(motor)
	{
	}
	static btRotationalLimitMotor2 _dummy;
#ifndef CPPPARSER
	operator btRotationalLimitMotor2&() const
	{
		return _motor;
	}

private:
	btRotationalLimitMotor2& _motor;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3RotationalLimitMotor2 & motor);

///BT3TranslationalLimitMotor2: wrapper to btTranslationalLimitMotor2.
//fixme: Check carefully whether the physical quantities are or not dependent on
//the axis direction; particularly: upper/lower limits, bounce, equilibrium
//point, erp, cfm, target velocity, max motor force, servo target, spring
//stiffness, spring damping, current limit error upper, current limit error,
//current linear diff.
struct EXPCL_PHYSICS BT3TranslationalLimitMotor2
{
PUBLISHED:
	INLINE ~BT3TranslationalLimitMotor2();
	INLINE void set_lower_limit(const LVector3f& value);
	INLINE LVector3f get_lower_limit() const;
	INLINE void set_upper_limit(const LVector3f& value);
	INLINE LVector3f get_upper_limit() const;
	INLINE void set_bounce(const LVecBase3f& value);
	INLINE LVecBase3f get_bounce() const;
	INLINE void set_equilibrium_point(const LPoint3f& value);
	INLINE LPoint3f get_equilibrium_point() const;
	INLINE void set_motor_erp(const LVecBase3f& value);
	INLINE LVecBase3f get_motor_erp() const;
	INLINE void set_motor_cfm(const LVecBase3f& value);
	INLINE LVecBase3f get_motor_cfm() const;
	INLINE void set_stop_erp(const LVecBase3f& value);
	INLINE LVecBase3f get_stop_erp() const;
	INLINE void set_stop_cfm(const LVecBase3f& value);
	INLINE LVecBase3f get_stop_cfm() const;
	INLINE void set_enable_motor(int index, bool value);//index:0=x,1=y,2=z
	INLINE bool get_enable_motor(int index) const;//index:0=x,1=y,2=z
	INLINE void set_target_velocity(const LVector3f& value);
	INLINE LVector3f get_target_velocity() const;
	INLINE void set_max_motor_force(const LVecBase3f& value);
	INLINE LVecBase3f get_max_motor_force() const;
	INLINE void set_servo_motor(int index, bool value);//index:0=x,1=y,2=z
	INLINE bool get_servo_motor(int index) const;//index:0=x,1=y,2=z
	INLINE void set_servo_target(const LVecBase3f& value);
	INLINE LVecBase3f get_servo_target() const;
	INLINE void set_enable_spring(int index, bool value);//index:0=x,1=y,2=z
	INLINE bool get_enable_spring(int index) const;//index:0=x,1=y,2=z
	INLINE void set_spring_stiffness(const LVecBase3f& value);
	INLINE LVecBase3f get_spring_stiffness() const;
	INLINE void set_spring_stiffness_limited(int index, bool value);//index:0=x,1=y,2=z
	INLINE bool get_spring_stiffness_limited(int index) const;//index:0=x,1=y,2=z
	INLINE void set_spring_damping(const LVecBase3f& value);
	INLINE LVecBase3f get_spring_damping() const;
	INLINE void set_spring_damping_limited(int index, bool value);//index:0=x,1=y,2=z
	INLINE bool get_spring_damping_limited(int index) const;//index:0=x,1=y,2=z
	INLINE LVector3f get_current_limit_error_upper() const;
	INLINE LVector3f get_current_limit_error() const;
	INLINE LVector3f get_current_linear_diff() const;
	INLINE int get_current_limit(int index) const;//index:0=x,1=y,2=z
	// Python Properties
	MAKE_PROPERTY(lower_limit, get_lower_limit, set_lower_limit);
	MAKE_PROPERTY(upper_limit, get_upper_limit, set_upper_limit);
	MAKE_PROPERTY(bounce, get_bounce, set_bounce);
	MAKE_PROPERTY(equilibrium_point, get_equilibrium_point, set_equilibrium_point);
	MAKE_PROPERTY(motor_erp, get_motor_erp, set_motor_erp);
	MAKE_PROPERTY(motor_cfm, get_motor_cfm, set_motor_cfm);
	MAKE_PROPERTY(stop_erp, get_stop_erp, set_stop_erp);
	MAKE_PROPERTY(stop_cfm, get_stop_cfm, set_stop_cfm);
	MAKE_PROPERTY(target_velocity, get_target_velocity, set_target_velocity);
	MAKE_PROPERTY(max_motor_force, get_max_motor_force, set_max_motor_force);
	MAKE_PROPERTY(servo_target, get_servo_target, set_servo_target);
	MAKE_PROPERTY(spring_stiffness, get_spring_stiffness, set_spring_stiffness);
	MAKE_PROPERTY(spring_damping, get_spring_damping, set_spring_damping);
	MAKE_PROPERTY(current_limit_error_upper, get_current_limit_error_upper);
	MAKE_PROPERTY(current_limit_error, get_current_limit_error);
	MAKE_PROPERTY(current_linear_diff, get_current_linear_diff);

	void output(ostream &out) const;

public:
	BT3TranslationalLimitMotor2(btTranslationalLimitMotor2& motor) :
		_motor(motor)
	{
	}
	static btTranslationalLimitMotor2 _dummy;
#ifndef CPPPARSER
	operator btTranslationalLimitMotor2&() const
	{
		return _motor;
	}

private:
	btTranslationalLimitMotor2& _motor;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3TranslationalLimitMotor2 & motor);

///BT3SoftBodyJointSpecs: wrapper to btSoftBody::Joint::Specs.
struct EXPCL_PHYSICS BT3SoftBodyJointSpecs
{
PUBLISHED:
	INLINE ~BT3SoftBodyJointSpecs();
	INLINE void set_erp(float value);
	INLINE float get_erp() const;
	INLINE void set_cfm(float value);
	INLINE float get_cfm() const;
	INLINE void set_split(float value);
	INLINE float get_split() const;
	// Python Properties
	MAKE_PROPERTY(erp, get_erp, set_erp);
	MAKE_PROPERTY(cfm, get_cfm, set_cfm);
	MAKE_PROPERTY(split, get_split, set_split);
	void output(ostream &out) const;

public:
	BT3SoftBodyJointSpecs(btSoftBody::Joint::Specs& specs) :
		_specs(specs)
	{
	}
	static btSoftBody::Joint::Specs _dummy;
#ifndef CPPPARSER
	operator btSoftBody::Joint::Specs&() const
	{
		return _specs;
	}

private:
	btSoftBody::Joint::Specs& _specs;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3SoftBodyJointSpecs & specs);

///BT3SoftBodyLJointSpecs: wrapper to btSoftBody::LJoint::Specs.
struct EXPCL_PHYSICS BT3SoftBodyLJointSpecs: public BT3SoftBodyJointSpecs
{
PUBLISHED:
	INLINE BT3SoftBodyLJointSpecs();
	INLINE ~BT3SoftBodyLJointSpecs();
	INLINE void set_position(const LPoint3f& value);
	INLINE LPoint3f get_position() const;
	// Python Properties
	MAKE_PROPERTY(position, get_position, set_position);
	void output(ostream &out) const;

#ifndef CPPPARSER
	operator btSoftBody::LJoint::Specs() const
	{
		return _specs;
	}

private:
	btSoftBody::LJoint::Specs _specs;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3SoftBodyLJointSpecs & specs);

///BT3SoftBodyAJointSpecs: wrapper to btSoftBody::AJoint::Specs.
struct EXPCL_PHYSICS BT3SoftBodyAJointSpecs: public BT3SoftBodyJointSpecs
{
PUBLISHED:
	INLINE BT3SoftBodyAJointSpecs();
	INLINE ~BT3SoftBodyAJointSpecs();
	INLINE void set_axis(const LVector3f& value);
	INLINE LVector3f get_axis() const;
	INLINE void set_icontrol(BT3SoftBodyAJointControl* value);
	INLINE BT3SoftBodyAJointControl* get_icontrol() const;
	// Python Properties
	MAKE_PROPERTY(axis, get_axis, set_axis);
	MAKE_PROPERTY(icontrol, get_icontrol, set_icontrol);
	void output(ostream &out) const;

#ifndef CPPPARSER
	operator btSoftBody::AJoint::Specs() const
	{
		return _specs;
	}
	static BT3SoftBodyAJointControl _dummy;

private:
	btSoftBody::AJoint::Specs _specs;
	BT3SoftBodyAJointControl::BT3ControlType _controlType;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3SoftBodyAJointSpecs & specs);

///BT3VehicleTuning: wrapper to btRaycastVehicle::btVehicleTuning.
struct EXPCL_PHYSICS BT3VehicleTuning
{
PUBLISHED:
	INLINE BT3VehicleTuning();
	INLINE ~BT3VehicleTuning();
	INLINE void set_suspension_stiffness(float value);
	INLINE float get_suspension_stiffness() const;
	INLINE void set_suspension_compression(float value);
	INLINE float get_suspension_compression() const;
	INLINE void set_suspension_damping(float value);
	INLINE float get_suspension_damping() const;
	INLINE void set_max_suspension_travel_cm(float value);
	INLINE float get_max_suspension_travel_cm() const;
	INLINE void set_friction_slip(float value);
	INLINE float get_friction_slip() const;
	INLINE void set_max_suspension_force(float value);
	INLINE float get_max_suspension_force() const;
	// Python Properties
	MAKE_PROPERTY(suspension_stiffness, get_suspension_stiffness, set_suspension_stiffness);
	MAKE_PROPERTY(suspension_compression, get_suspension_compression, set_suspension_compression);
	MAKE_PROPERTY(suspension_damping, get_suspension_damping, set_suspension_damping);
	MAKE_PROPERTY(max_suspension_travel_cm, get_max_suspension_travel_cm, set_max_suspension_travel_cm);
	MAKE_PROPERTY(friction_slip, get_friction_slip, set_friction_slip);
	MAKE_PROPERTY(max_suspension_force, get_max_suspension_force, set_max_suspension_force);
	void output(ostream &out) const;

public:
	BT3VehicleTuning(const btRaycastVehicle::btVehicleTuning& tuning) :
	_tuning(tuning)
	{
	}
#ifndef CPPPARSER
	operator btRaycastVehicle::btVehicleTuning() const
	{
		return _tuning;
	}

private:
	btRaycastVehicle::btVehicleTuning _tuning;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3VehicleTuning & tuning);

///BT3WheelInfo: wrapper to btWheelInfo.
struct EXPCL_PHYSICS BT3WheelInfo
{
PUBLISHED:
	INLINE ~BT3WheelInfo();
	INLINE void set_max_suspension_travel_cm(float value);
	INLINE float get_max_suspension_travel_cm() const;
	INLINE void set_suspension_stiffness(float value);
	INLINE float get_suspension_stiffness() const;
	INLINE void set_wheels_damping_compression(float value);
	INLINE float get_wheels_damping_compression() const;
	INLINE void set_wheels_damping_relaxation(float value);
	INLINE float get_wheels_damping_relaxation() const;
	INLINE void set_friction_slip(float value);
	INLINE float get_friction_slip() const;
	INLINE void set_roll_influence(float value);
	INLINE float get_roll_influence() const;
	INLINE void set_max_suspension_force(float value);
	INLINE float get_max_suspension_force() const;
	INLINE LPoint3f get_chassis_connection_point_cs() const;
	INLINE LVector3f get_wheel_direction_cs() const;
	INLINE LVector3f get_wheel_axle_cs() const;
	INLINE float get_suspension_rest_length() const;
	INLINE float get_wheels_radius() const;
	INLINE bool get_is_front_wheel() const;
	INLINE float get_steering() const;
	INLINE float get_rotation() const;
	INLINE float get_delta_rotation() const;
	INLINE float get_engine_force() const;
	INLINE float get_brake() const;
	//raycast infos
	INLINE LVector3f get_raycast_contact_normal_ws() const;
	INLINE LPoint3f get_raycast_contact_point_ws() const;
	INLINE LPoint3f get_raycast_hard_point_ws() const;
	INLINE LVector3f get_raycast_wheel_direction_ws() const;
	INLINE LVector3f get_raycast_wheel_axle_ws() const;
	INLINE float get_raycast_suspension_length() const;
	INLINE bool get_raycast_is_in_contact() const;
	INLINE NodePath get_raycast_ground_object() const;
	// Python Properties
	MAKE_PROPERTY(max_suspension_travel_cm, get_max_suspension_travel_cm, set_max_suspension_travel_cm);
	MAKE_PROPERTY(suspension_stiffness, get_suspension_stiffness, set_suspension_stiffness);
	MAKE_PROPERTY(wheels_damping_compression, get_wheels_damping_compression, set_wheels_damping_compression);
	MAKE_PROPERTY(wheels_damping_relaxation, get_wheels_damping_relaxation, set_wheels_damping_relaxation);
	MAKE_PROPERTY(friction_slip, get_friction_slip, set_friction_slip);
	MAKE_PROPERTY(roll_influence, get_roll_influence, set_roll_influence);
	MAKE_PROPERTY(max_suspension_force, get_max_suspension_force, set_max_suspension_force);
	MAKE_PROPERTY(chassis_connection_point_cs, get_chassis_connection_point_cs);
	MAKE_PROPERTY(wheel_direction_cs, get_wheel_direction_cs);
	MAKE_PROPERTY(wheel_axle_cs, get_wheel_axle_cs);
	MAKE_PROPERTY(suspension_rest_length, get_suspension_rest_length);
	MAKE_PROPERTY(wheels_radius, get_wheels_radius);
	MAKE_PROPERTY(is_front_wheel, get_is_front_wheel);
	MAKE_PROPERTY(steering, get_steering);
	MAKE_PROPERTY(rotation, get_rotation);
	MAKE_PROPERTY(delta_rotation, get_delta_rotation);
	MAKE_PROPERTY(engine_force, get_engine_force);
	MAKE_PROPERTY(brake, get_brake);
	MAKE_PROPERTY(raycast_contact_normal_ws, get_raycast_contact_normal_ws);
	MAKE_PROPERTY(raycast_contact_point_ws, get_raycast_contact_point_ws);
	MAKE_PROPERTY(raycast_hard_point_ws, get_raycast_hard_point_ws);
	MAKE_PROPERTY(raycast_wheel_direction_ws, get_raycast_wheel_direction_ws);
	MAKE_PROPERTY(raycast_wheel_axle_ws, get_raycast_wheel_axle_ws);
	MAKE_PROPERTY(raycast_suspension_length, get_raycast_suspension_length);
	MAKE_PROPERTY(raycast_is_in_contact, get_raycast_is_in_contact);
	MAKE_PROPERTY(raycast_ground_object, get_raycast_ground_object);

	void output(ostream &out) const;

public:
	BT3WheelInfo(btWheelInfo& info) :
		_info(info)
	{
	}
	static btWheelInfo _dummy;
#ifndef CPPPARSER
	operator btWheelInfo&() const
	{
		return _info;
	}

private:
	btWheelInfo& _info;
#endif //CPPPARSER

public:
	void write_datagram(Datagram &dg) const;
	void read_datagram(DatagramIterator &scan);
};
INLINE ostream &operator << (ostream &out, const BT3WheelInfo & info);

///BT3RayTestResult
struct EXPCL_PHYSICS BT3RayTestResult
{
PUBLISHED:
	enum BT3HitType: unsigned char
	{
		CLOSEST,
		ALL,
		NONE
	};

	INLINE ~BT3RayTestResult();

	NodePath get_hit_object(int i) const;
	INLINE int get_num_hit_objects() const;
	MAKE_SEQ(get_hit_objects, get_num_hit_objects, get_hit_object);
	LPoint3f get_hit_point(int i) const;
	INLINE int get_num_hit_points() const;
	MAKE_SEQ(get_hit_points, get_num_hit_points, get_hit_point);
	LVector3f get_hit_normal(int i) const;
	INLINE int get_num_hit_normals() const;
	MAKE_SEQ(get_hit_normals, get_num_hit_normals, get_hit_normal);
	float get_hit_fraction(int i) const;
	INLINE int get_num_hit_fractions() const;
	MAKE_SEQ(get_hit_fractions, get_num_hit_fractions, get_hit_fraction);
	INLINE bool	get_has_hit() const;
	// Python Properties
	MAKE_PROPERTY(num_hit_objects, get_num_hit_objects);
	MAKE_PROPERTY(num_hit_points, get_num_hit_points);
	MAKE_PROPERTY(num_hit_normals, get_num_hit_normals);
	MAKE_PROPERTY(num_hit_fractions, get_num_hit_fractions);
	MAKE_PROPERTY(has_hit, get_has_hit);
	MAKE_SEQ_PROPERTY(hit_objects, get_num_hit_objects, get_hit_object);
	MAKE_SEQ_PROPERTY(hit_points, get_num_hit_points, get_hit_point);
	MAKE_SEQ_PROPERTY(hit_normals, get_num_hit_normals, get_hit_normal);
	MAKE_SEQ_PROPERTY(hit_fractions, get_num_hit_fractions, get_hit_fraction);

public:
	BT3RayTestResult() = delete;
	BT3RayTestResult(const BT3RayTestResult&) = default;
	BT3RayTestResult(BT3RayTestResult&& other) noexcept:
			_result(other._result), _type(other._type)
	{
		other._result = nullptr;
	}
	typedef btCollisionWorld::RayResultCallback ResultType;
	operator ResultType&() const
	{
		return *_result;
	}

#ifndef CPPPARSER
protected:
	friend class GamePhysicsManager;
	typedef btCollisionWorld::ClosestRayResultCallback ClosestResultType;
	typedef btCollisionWorld::AllHitsRayResultCallback AllResultsType;

	BT3RayTestResult(BT3HitType type, const LPoint3f& rayFromWorld,
			const LPoint3f& rayToWorld) :
	_result(nullptr), _type(type)
	{
		if (_type == BT3HitType::CLOSEST)
		{
			_result = new ClosestResultType(
					bt3::LVecBase3_to_btVector3(rayFromWorld),
					bt3::LVecBase3_to_btVector3(rayToWorld));
		}
		else if (_type == BT3HitType::ALL)
		{
			_result = new AllResultsType(
					bt3::LVecBase3_to_btVector3(rayFromWorld),
					bt3::LVecBase3_to_btVector3(rayToWorld));
		}
		else
		{
			_type = NONE;
		}
	}

private:
	ResultType* _result;
	BT3HitType _type;
#endif //CPPPARSER
};

///BT3ConvexTestResult
struct EXPCL_PHYSICS BT3ConvexTestResult
{
PUBLISHED:
	enum BT3HitType: unsigned char
	{
		CLOSEST,
		NONE
	};

	INLINE ~BT3ConvexTestResult();

	NodePath get_hit_object(int i) const;
	INLINE int get_num_hit_objects() const;
	MAKE_SEQ(get_hit_objects, get_num_hit_objects, get_hit_object);
	LPoint3f get_hit_point(int i) const;
	INLINE int get_num_hit_points() const;
	MAKE_SEQ(get_hit_points, get_num_hit_points, get_hit_point);
	LVector3f get_hit_normal(int i) const;
	INLINE int get_num_hit_normals() const;
	MAKE_SEQ(get_hit_normals, get_num_hit_normals, get_hit_normal);
	float get_hit_fraction(int i) const;
	INLINE int get_num_hit_fractions() const;
	MAKE_SEQ(get_hit_fractions, get_num_hit_fractions, get_hit_fraction);
	INLINE bool	get_has_hit() const;
	// Python Properties
	MAKE_PROPERTY(num_hit_objects, get_num_hit_objects);
	MAKE_PROPERTY(num_hit_points, get_num_hit_points);
	MAKE_PROPERTY(num_hit_normals, get_num_hit_normals);
	MAKE_PROPERTY(num_hit_fractions, get_num_hit_fractions);
	MAKE_PROPERTY(has_hit, get_has_hit);
	MAKE_SEQ_PROPERTY(hit_objects, get_num_hit_objects, get_hit_object);
	MAKE_SEQ_PROPERTY(hit_points, get_num_hit_points, get_hit_point);
	MAKE_SEQ_PROPERTY(hit_normals, get_num_hit_normals, get_hit_normal);
	MAKE_SEQ_PROPERTY(hit_fractions, get_num_hit_fractions, get_hit_fraction);

public:
	BT3ConvexTestResult() = delete;
	BT3ConvexTestResult(const BT3ConvexTestResult&) = default;
	BT3ConvexTestResult(BT3ConvexTestResult&& other) noexcept:
			_result(other._result), _type(other._type)
	{
		other._result = nullptr;
	}
	typedef btCollisionWorld::ConvexResultCallback ResultType;
	operator ResultType&() const
	{
		return *_result;
	}

#ifndef CPPPARSER
protected:
	friend class GamePhysicsManager;
	typedef btCollisionWorld::ClosestConvexResultCallback ClosestResultType;

	BT3ConvexTestResult(BT3HitType type, const LPoint3f& convexFromWorld,
			const LPoint3f& convexToWorld) :
	_result(nullptr), _type(type)
	{
		if (_type == BT3HitType::CLOSEST)
		{
			_result = new ClosestResultType(
					bt3::LVecBase3_to_btVector3(convexFromWorld),
					bt3::LVecBase3_to_btVector3(convexToWorld));
		}
		else
		{
			_type = NONE;
		}
	}

private:
	ResultType* _result;
	BT3HitType _type;
#endif //CPPPARSER
};

///*** Inherited from bullet3/examples/Collision/Internal/Bullet2CollisionSdk.cpp ***
#ifndef CPPPARSER
///Bullet2ContactResultCallback
struct lwContactPoint
{
	float m_ptOnAWorld[3];
	float m_ptOnBWorld[3];
	float m_normalOnB[3];
	float m_distance;
};
struct bt2PairContactResultCallback: public btCollisionWorld::ContactResultCallback
{
	int m_numContacts;
	lwContactPoint* m_pointsOut;
	int m_pointCapacity;

	bt2PairContactResultCallback(lwContactPoint* pointsOut, int pointCapacity) :
			m_numContacts(0), m_pointsOut(pointsOut), m_pointCapacity(
					pointCapacity)
	{
	}
	virtual btScalar addSingleResult(btManifoldPoint& cp,
			const btCollisionObjectWrapper* colObj0Wrap, int partId0,
			int index0, const btCollisionObjectWrapper* colObj1Wrap,
			int partId1, int index1)
	{
		if (m_numContacts < m_pointCapacity)
		{
			doSetContactPoint(cp);
			m_numContacts++;
		}

		return 1.f;
	}

	void doSetContactPoint(btManifoldPoint& cp)
	{
			lwContactPoint& ptOut = m_pointsOut[m_numContacts];
			ptOut.m_distance = cp.m_distance1;
			ptOut.m_normalOnB[0] = cp.m_normalWorldOnB.getX();
			ptOut.m_normalOnB[1] = cp.m_normalWorldOnB.getY();
			ptOut.m_normalOnB[2] = cp.m_normalWorldOnB.getZ();
			ptOut.m_ptOnAWorld[0] = cp.m_positionWorldOnA[0];
			ptOut.m_ptOnAWorld[1] = cp.m_positionWorldOnA[1];
			ptOut.m_ptOnAWorld[2] = cp.m_positionWorldOnA[2];
			ptOut.m_ptOnBWorld[0] = cp.m_positionWorldOnB[0];
			ptOut.m_ptOnBWorld[1] = cp.m_positionWorldOnB[1];
			ptOut.m_ptOnBWorld[2] = cp.m_positionWorldOnB[2];
	}
};
struct bt2SingleContactResultCallback: public bt2PairContactResultCallback
{
	unordered_map<int, pair<const btCollisionObject*, const btCollisionObject*>>
		m_objectsPerPointIdx;

	bt2SingleContactResultCallback(lwContactPoint* pointsOut, int pointCapacity) :
		bt2PairContactResultCallback(pointsOut, pointCapacity)
	{
		m_objectsPerPointIdx.clear();
	}
	virtual btScalar addSingleResult(btManifoldPoint& cp,
			const btCollisionObjectWrapper* colObj0Wrap, int partId0,
			int index0, const btCollisionObjectWrapper* colObj1Wrap,
			int partId1, int index1)
	{
		if (m_numContacts < m_pointCapacity)
		{
			doSetContactPoint(cp);
			m_objectsPerPointIdx[m_numContacts] =
					make_pair(colObj0Wrap->m_collisionObject, colObj1Wrap->m_collisionObject);
			m_numContacts++;
		}
		return 1.f;
	}
};
#endif //CPPPARSER

///BT3ContactPoint
struct EXPCL_PHYSICS BT3ContactPoint
{
PUBLISHED:
	INLINE ~BT3ContactPoint();

	INLINE LPoint3f get_point_on_a() const;
	INLINE LPoint3f get_point_on_b() const;
	INLINE LVector3f get_normal_on_b() const;
	INLINE float get_distance() const;
	// Python Properties
	MAKE_PROPERTY(point_on_a, get_point_on_a);
	MAKE_PROPERTY(point_on_b, get_point_on_b);
	MAKE_PROPERTY(normal_on_b, get_normal_on_b);
	MAKE_PROPERTY(distance, get_distance);

public:
	BT3ContactPoint(lwContactPoint& contact) :
		_contact(contact)
	{
	}
	static lwContactPoint _dummy;
#ifndef CPPPARSER
	operator lwContactPoint&() const
	{
		return _contact;
	}

private:
	lwContactPoint& _contact;
#endif //CPPPARSER
};

///BT3ContactTestResult
struct EXPCL_PHYSICS BT3ContactTestResult
{
PUBLISHED:
	enum BT3ContactType: unsigned char
	{
		SINGLE,
		PAIR,
		NONE
	};

	INLINE ~BT3ContactTestResult();

	BT3ContactPoint get_contact_point(int i) const;
	INLINE int get_num_contact_points() const;
	MAKE_SEQ(get_contact_points, get_num_contact_points, get_contact_point);
	ValueList_NodePath get_contact_objects(int i) const;
	INLINE float get_closest_distance_threshold() const;
	INLINE int get_contact_point_capacity() const;
	// Python Properties
	MAKE_PROPERTY(num_contact_points, get_num_contact_points);
	MAKE_PROPERTY(closest_distance_threshold, get_closest_distance_threshold);
	MAKE_PROPERTY(point_capacity, get_contact_point_capacity);
	MAKE_SEQ_PROPERTY(contact_points, get_num_contact_points, get_contact_point);

public:
	BT3ContactTestResult() = delete;
	BT3ContactTestResult(const BT3ContactTestResult&) = default;
	BT3ContactTestResult(BT3ContactTestResult&& other) noexcept:
			_result(other._result), _type(other._type),
			_pointsOut(other._pointsOut)
	{
		other._result = nullptr;
		other._pointsOut = nullptr;
	}
	typedef btCollisionWorld::ContactResultCallback ResultType;
	operator ResultType&() const
	{
		return *_result;
	}

#ifndef CPPPARSER
protected:
	friend class GamePhysicsManager;
	typedef bt2SingleContactResultCallback SingleResultType;
	typedef bt2PairContactResultCallback PairResultType;

	BT3ContactTestResult(BT3ContactType type, int pointCapacity) :
	_result(nullptr), _type(type), _pointsOut(nullptr)
	{
		if (_type == BT3ContactType::SINGLE)
		{
			_pointsOut = new lwContactPoint[pointCapacity];
			_result = new SingleResultType(_pointsOut, pointCapacity);
		}
		else if (_type == BT3ContactType::PAIR)
		{
			_pointsOut = new lwContactPoint[pointCapacity];
			_result = new PairResultType(_pointsOut, pointCapacity);
		}
		else
		{
			_type = NONE;
		}
	}

private:
	ResultType* _result;
	BT3ContactType _type;
	lwContactPoint* _pointsOut;
#endif //CPPPARSER
};

///inline
#include "physicsTools.I"

#endif /* PHYSICS_SOURCE_PHYSICSTOOLS_H_ */
