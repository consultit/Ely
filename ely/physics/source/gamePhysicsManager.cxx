/**
 * \file gamePhysicsManager.cxx
 *
 * \date 2016-10-09
 * \author consultit
 */

#include "gamePhysicsManager.h"
#include "asyncTaskManager.h"
#include "bamFile.h"
#include "nodePathCollection.h"
#include "throw_event.h"

#include "bt3RigidBody.h"
#include "bt3SoftBody.h"
#include "bt3Ghost.h"
#include "bt3Constraint.h"
#include "bt3Vehicle.h"
#include "bt3CharacterController.h"

#include "bullet3/src/BulletCollision/NarrowPhaseCollision/btRaycastCallback.h"

///GamePhysicsManager definitions
/**
 *
 */
GamePhysicsManager::GamePhysicsManager(int taskSort, const NodePath& root,
		const CollideMask& groupMask, const CollideMask& collideMask,
		const BT3PhysicsInfo& info, int debugMode,int drawSoftFlags):
		mPhysics{info.get_physics_info()},
		mTaskSort{taskSort},
		mReferenceNP{NodePath("ReferenceNode")},
		mGroupMask{groupMask},
		mCollideMask{collideMask},
		mSoftBodyManager{nullptr},
		mVehicleManager{nullptr},
		mCharacterControllerManager{nullptr},
		mRef{0},
		mUtils{root, collideMask},
		mPicker{nullptr},
		mReferenceDebugNP{NodePath("ReferenceDebugNode")}
{
	PRINT_DEBUG("GamePhysicsManager::GamePhysicsManager: creating the singleton manager.");

	// rigid bodies
	mRigidBodies.clear();
	mRigidBodiesParameterTable.clear();
	set_parameters_defaults(RIGIDBODY);
	// soft bodies
	mSoftBodies.clear();
	mSoftBodiesParameterTable.clear();
	set_parameters_defaults(SOFTBODY);
	// ghosts
	mGhosts.clear();
	mGhostsParameterTable.clear();
	set_parameters_defaults(GHOST);
	// constraints
	mConstraints.clear();
	mConstraintsParameterTable.clear();
	set_parameters_defaults(CONSTRAINT);
	// vehicles
	mVehicles.clear();
	mVehiclesParameterTable.clear();
	set_parameters_defaults(VEHICLE);
	// characterControllers
	mCharacterControllers.clear();
	mCharacterControllersParameterTable.clear();
	set_parameters_defaults(CHARACTERCONTROLLER);
	//
	mUpdateData.clear();
	mUpdateTask.clear();
	// create the dynamics world
	mPhysics.createDynamicsWorld(btVector3(0, -9.807, 0));
	//create a soft body manager (after dynamics world's creation)
	mSoftBodyManager = new bt3::SoftBodyManager();
	//create a vehicle manager (after dynamics world's creation)
	mVehicleManager = new bt3::VehicleManager(mPhysics);
	//create a character controller manager (after dynamics world's creation)
	mCharacterControllerManager = new bt3::CharacterControllerManager(mPhysics);
	//set default collision notify data
	mCollision.mEnable = false;
	mCollision.mFrequency = 30.0;
	//clear the colliding pair set
	mCollidingNodePairs.clear();
#ifdef ELY_DEBUG
	//set custom debug drawer
	mPhysics.debugDrawer = new bt3::DebugDrawer(mReferenceDebugNP);
	mPhysics.debugDrawer->clear();
	mPhysics.dynamicsWorld->setDebugDrawer(mPhysics.debugDrawer);
	mPhysics.dynamicsWorld->getDebugDrawer()->setDebugMode(debugMode);
	//set soft body world only flags
	if(mPhysics.dynamicsWorld->getWorldType() == BT_SOFT_RIGID_DYNAMICS_WORLD)
	{
		static_cast<btSoftRigidDynamicsWorld*>(mPhysics.dynamicsWorld)->setDrawFlags(
				drawSoftFlags);
	}
	mPhysics.doDebugDraw = false;
#endif //ELY_DEBUG
}

/**
 *
 */
GamePhysicsManager::~GamePhysicsManager()
{
	PRINT_DEBUG("GamePhysicsManager::~GamePhysicsManager: destroying the singleton manager.");

	//stop any default update
	stop_default_update();
	{
		//destroy all BT3RigidBodys
		PTA(PT(BT3RigidBody))::iterator iterA = mRigidBodies.begin();
		while (iterA != mRigidBodies.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to BT3RigidBody to cleanup itself before being destroyed.
			(*iterA)->do_finalize();
			//remove the BT3RigidBodys from the inner list (&& from the update task)
			iterA = mRigidBodies.erase(iterA);
		}

		//destroy all BT3SoftBodys
		PTA(PT(BT3SoftBody))::iterator iterB = mSoftBodies.begin();
		while (iterB != mSoftBodies.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to BT3SoftBody to cleanup itself before being destroyed.
			(*iterB)->do_finalize();
			//remove the BT3SoftBodys from the inner list (&& from the update task)
			iterB = mSoftBodies.erase(iterB);
		}

		//destroy all BT3Ghosts
		PTA(PT(BT3Ghost))::iterator iterC = mGhosts.begin();
		while (iterC != mGhosts.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to BT3Ghost to cleanup itself before being destroyed.
			(*iterC)->do_finalize();
			//remove the BT3Ghosts from the inner list (&& from the update task)
			iterC = mGhosts.erase(iterC);
		}

		//destroy all BT3Constraints
		PTA(PT(BT3Constraint))::iterator iterD = mConstraints.begin();
		while (iterD != mConstraints.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to BT3Constraint to cleanup itself before being destroyed.
			(*iterD)->do_finalize();
			//remove the BT3Constraints from the inner list (&& from the update task)
			iterD = mConstraints.erase(iterD);
		}

		//destroy all BT3Vehicles
		PTA(PT(BT3Vehicle))::iterator iterE = mVehicles.begin();
		while (iterE != mVehicles.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to BT3Vehicle to cleanup itself before being destroyed.
			(*iterE)->do_finalize();
			//remove the BT3Vehicles from the inner list (&& from the update task)
			iterE = mVehicles.erase(iterE);
		}

		//destroy all BT3Characters
		PTA(PT(BT3CharacterController))::iterator iterF = mCharacterControllers.begin();
		while (iterF != mCharacterControllers.end())
		{
			//\see http://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it
			//give a chance to BT3CharacterController to cleanup itself before being destroyed.
			(*iterF)->do_finalize();
			//remove the BT3Characters from the inner list (&& from the update task)
			iterF = mCharacterControllers.erase(iterF);
		}
	}
	// delete the dynamics world
	mPhysics.deleteDynamicsWorld();
	//clear parameters' tables
	mRigidBodiesParameterTable.clear();
	mSoftBodiesParameterTable.clear();
	mGhostsParameterTable.clear();
	mConstraintsParameterTable.clear();
	mVehiclesParameterTable.clear();
	mCharacterControllersParameterTable.clear();
	//delete Picker
	delete mPicker;
}

/**
 * Creates a BT3RigidBody with a given (mandatory && not empty) name.
 * Returns a NodePath to the new BT3RigidBody,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GamePhysicsManager::create_rigid_body(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(BT3RigidBody) newRigidBody = new BT3RigidBody(name);
	nassertr_always(newRigidBody, NodePath::fail())

	// set reference node
	newRigidBody->mReferenceNP = mReferenceNP;
	// reparent to reference node
	NodePath np = mReferenceNP.attach_new_node(newRigidBody);
	// initialize the new BT3RigidBody (could use mReferenceNP, mThisNP)
	newRigidBody->do_initialize();

	// add the new BT3RigidBody to the inner list (&& to the update task)
	mRigidBodies.push_back(newRigidBody);
	//
	return np;
}

/**
 * Destroys a BT3RigidBody.
 * Returns false on error.
 * \note If the BT3RigidBody has some constraints applied to it, these must be
 * destroyed before destroying the BT3RigidBody itself, otherwise there will be
 * memory leaks.
 */
bool GamePhysicsManager::destroy_rigid_body(NodePath rigidBodyNP)
{
	CONTINUE_IF_ELSE_R(
			rigidBodyNP.node()->is_of_type(BT3RigidBody::get_class_type()),
			false)

	PT(BT3RigidBody)rigid_body = DCAST(BT3RigidBody, rigidBodyNP.node());
	RigidBodyList::iterator iter = find(mRigidBodies.begin(),
			mRigidBodies.end(), rigid_body);
	CONTINUE_IF_ELSE_R(iter != mRigidBodies.end(), false)

	// give a chance to BT3RigidBody to cleanup itself before being destroyed.
	rigid_body->do_finalize();
	//remove the BT3RigidBody from the inner list (&& from the update task)
	mRigidBodies.erase(iter);
	//
	return true;
}

/**
 * Gets an BT3RigidBody by index, or nullptr on error.
 */
PT(BT3RigidBody) GamePhysicsManager::get_rigid_body(int index) const
{
	nassertr_always((index >= 0) && (index < (int) mRigidBodies.size()),
			nullptr)

	return mRigidBodies[index];
}

/**
 * Creates a BT3SoftBody with a given (mandatory && not empty) name.
 * Returns a NodePath to the new BT3SoftBody,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GamePhysicsManager::create_soft_body(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(BT3SoftBody)newSoftBody = new BT3SoftBody(name);
	nassertr_always(newSoftBody, NodePath::fail())

	// set reference node
	newSoftBody->mReferenceNP = mReferenceNP;
	// reparent to reference node
	NodePath np = mReferenceNP.attach_new_node(newSoftBody);
	// initialize the new BT3SoftBody (could use mReferenceNP, mThisNP)
	newSoftBody->do_initialize();

	// add the new BT3SoftBody to the inner list (&& to the update task)
	mSoftBodies.push_back(newSoftBody);
	//
	return np;
}

/**
 * Destroys a BT3SoftBody.
 * Returns false on error.
 */
bool GamePhysicsManager::destroy_soft_body(NodePath softBodyNP)
{
	CONTINUE_IF_ELSE_R(
			softBodyNP.node()->is_of_type(BT3SoftBody::get_class_type()),
			false)

	PT(BT3SoftBody)soft_body = DCAST(BT3SoftBody, softBodyNP.node());
	SoftBodyList::iterator iter = find(mSoftBodies.begin(),
			mSoftBodies.end(), soft_body);
	CONTINUE_IF_ELSE_R(iter != mSoftBodies.end(), false)

	// give a chance to BT3SoftBody to cleanup itself before being destroyed.
	soft_body->do_finalize();
	//remove the BT3SoftBody from the inner list (&& from the update task)
	mSoftBodies.erase(iter);
	//
	return true;
}

/**
 * Gets an BT3SoftBody by index, or nullptr on error.
 */
PT(BT3SoftBody) GamePhysicsManager::get_soft_body(int index) const
{
	nassertr_always((index >= 0) && (index < (int) mSoftBodies.size()),
			nullptr)

	return mSoftBodies[index];
}

/**
 * Creates a BT3Ghost with a given (mandatory && not empty) name.
 * Returns a NodePath to the new BT3Ghost,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GamePhysicsManager::create_ghost(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(BT3Ghost)newGhost = new BT3Ghost(name);
	nassertr_always(newGhost, NodePath::fail())

	// set reference node
	newGhost->mReferenceNP = mReferenceNP;
	// reparent to reference node
	NodePath np = mReferenceNP.attach_new_node(newGhost);
	// initialize the new BT3Ghost (could use mReferenceNP, mThisNP, mWin)
	newGhost->do_initialize();

	// add the new BT3Ghost to the inner list (&& to the update task)
	mGhosts.push_back(newGhost);
	//
	return np;
}

/**
 * Destroys a BT3Ghost.
 * Returns false on error.
 */
bool GamePhysicsManager::destroy_ghost(NodePath ghostNP)
{
	CONTINUE_IF_ELSE_R(
			ghostNP.node()->is_of_type(BT3Ghost::get_class_type()),
			false)

	PT(BT3Ghost)ghost = DCAST(BT3Ghost, ghostNP.node());
	GhostList::iterator iter = find(mGhosts.begin(),
			mGhosts.end(), ghost);
	CONTINUE_IF_ELSE_R(iter != mGhosts.end(), false)

	// give a chance to BT3Ghost to cleanup itself before being destroyed.
	ghost->do_finalize();
	//remove the BT3Ghost from the inner list (&& from the update task)
	mGhosts.erase(iter);
	//
	return true;
}

/**
 * Gets an BT3Ghost by index, or nullptr on error.
 */
PT(BT3Ghost) GamePhysicsManager::get_ghost(int index) const
{
	nassertr_always((index >= 0) && (index < (int) mGhosts.size()),
			nullptr)

	return mGhosts[index];
}

/**
 * Creates a BT3Constraint with a given (mandatory && not empty) name.
 * Returns a NodePath to the new BT3Constraint,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GamePhysicsManager::create_constraint(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(BT3Constraint) newConstraint = new BT3Constraint(name);
	nassertr_always(newConstraint, NodePath::fail())

	// set reference node
	newConstraint->mReferenceNP = mReferenceNP;
	// reparent to reference node
	NodePath np = mReferenceNP.attach_new_node(newConstraint);
	// initialize the new BT3Constraint (could use mReferenceNP, mThisNP)
	newConstraint->do_initialize();

	// add the new BT3Constraint to the inner list (&& to the update task)
	mConstraints.push_back(newConstraint);
	//
	return np;
}

/**
 * Destroys a BT3Constraint.
 * Returns false on error.
 */
bool GamePhysicsManager::destroy_constraint(NodePath constraintNP)
{
	CONTINUE_IF_ELSE_R(
			constraintNP.node()->is_of_type(BT3Constraint::get_class_type()),
			false)

	PT(BT3Constraint)constraint = DCAST(BT3Constraint, constraintNP.node());
	ConstraintList::iterator iter = find(mConstraints.begin(),
			mConstraints.end(), constraint);
	CONTINUE_IF_ELSE_R(iter != mConstraints.end(), false)

	// give a chance to BT3Constraint to cleanup itself before being destroyed.
	constraint->do_finalize();
	//remove the BT3Constraint from the inner list (&& from the update task)
	mConstraints.erase(iter);
	//
	return true;
}

/**
 * Gets an BT3Constraint by index, or nullptr on error.
 */
PT(BT3Constraint) GamePhysicsManager::get_constraint(int index) const
{
	nassertr_always((index >= 0) && (index < (int) mConstraints.size()),
			nullptr)

	return mConstraints[index];
}

/**
 * Creates a BT3Vehicle with a given (mandatory && not empty) name.
 * Returns a NodePath to the new BT3Vehicle,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GamePhysicsManager::create_vehicle(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(BT3Vehicle)newVehicle = new BT3Vehicle(name);
	nassertr_always(newVehicle, NodePath::fail())

	// set reference node
	newVehicle->mReferenceNP = mReferenceNP;
	// reparent to reference node
	NodePath np = mReferenceNP.attach_new_node(newVehicle);
	// initialize the new BT3Vehicle (could use mReferenceNP, mThisNP, mWin)
	newVehicle->do_initialize();

	// add the new BT3Vehicle to the inner list (&& to the update task)
	mVehicles.push_back(newVehicle);
	//
	return np;
}

/**
 * Destroys a BT3Vehicle.
 * Returns false on error.
 */
bool GamePhysicsManager::destroy_vehicle(NodePath vehicleNP)
{
	CONTINUE_IF_ELSE_R(
			vehicleNP.node()->is_of_type(BT3Vehicle::get_class_type()),
			false)

	PT(BT3Vehicle)vehicle = DCAST(BT3Vehicle, vehicleNP.node());
	VehicleList::iterator iter = find(mVehicles.begin(),
			mVehicles.end(), vehicle);
	CONTINUE_IF_ELSE_R(iter != mVehicles.end(), false)

	// give a chance to BT3Vehicle to cleanup itself before being destroyed.
	vehicle->do_finalize();
	//remove the BT3Vehicle from the inner list (&& from the update task)
	mVehicles.erase(iter);
	//
	return true;
}

/**
 * Gets an BT3Vehicle by index, or nullptr on error.
 */
PT(BT3Vehicle) GamePhysicsManager::get_vehicle(int index) const
{
	nassertr_always((index >= 0) && (index < (int) mVehicles.size()),
			nullptr)

	return mVehicles[index];
}

/**
 * Creates a BT3CharacterController with a given (mandatory && not empty) name.
 * Returns a NodePath to the new BT3CharacterController,or an empty NodePath with the
 * ET_fail error type set on error.
 */
NodePath GamePhysicsManager::create_character_controller(const string& name)
{
	nassertr_always(!name.empty(), NodePath::fail())

	PT(BT3CharacterController)newCharacterController = new BT3CharacterController(name);
	nassertr_always(newCharacterController, NodePath::fail())

	// set reference node
	newCharacterController->mReferenceNP = mReferenceNP;
	// reparent to reference node
	NodePath np = mReferenceNP.attach_new_node(newCharacterController);
	// initialize the new BT3CharacterController (could use mReferenceNP, mThisNP, mWin)
	newCharacterController->do_initialize();

	// add the new BT3CharacterController to the inner list (&& to the update task)
	mCharacterControllers.push_back(newCharacterController);
	//
	return np;
}

/**
 * Destroys a BT3CharacterController.
 * Returns false on error.
 */
bool GamePhysicsManager::destroy_character_controller(NodePath characterControllerNP)
{
	CONTINUE_IF_ELSE_R(
			characterControllerNP.node()->is_of_type(
					BT3CharacterController::get_class_type()), false)

	PT(BT3CharacterController)characterController =
			DCAST(BT3CharacterController, characterControllerNP.node());
	CharacterControllerList::iterator iter = find(mCharacterControllers.begin(),
			mCharacterControllers.end(), characterController);
	CONTINUE_IF_ELSE_R(iter != mCharacterControllers.end(), false)

	// give a chance to BT3CharacterController to cleanup itself before being destroyed.
	characterController->do_finalize();
	//remove the BT3CharacterController from the inner list (&& from the update task)
	mCharacterControllers.erase(iter);
	//
	return true;
}

/**
 * Gets an BT3CharacterController by index, or nullptr on error.
 */
PT(BT3CharacterController) GamePhysicsManager::get_character_controller(int index) const
{
	nassertr_always((index >= 0) && (index < (int) mCharacterControllers.size()),
			nullptr)

	return mCharacterControllers[index];
}


/**
 * Performs a raycast on all objects and the returned BT3RayTestResult contains
 * the closest/all hit/hits data, if get_has_hit() is true.
 */
BT3RayTestResult GamePhysicsManager::ray_test(const LPoint3f& rayFromWorld,
		const LPoint3f& rayToWorld, BT3RayTestResult::BT3HitType hitType,
	const CollideMask& groupMask, const CollideMask& collideMask) const
{
	///Note: code is redundant for performance reason
	if (hitType == BT3RayTestResult::CLOSEST)
	{
		BT3RayTestResult result(hitType, rayFromWorld, rayToWorld);
		BT3RayTestResult::ResultType& btCallback =
				static_cast<BT3RayTestResult::ResultType&>(result);
		btCallback.m_flags |= btTriangleRaycastCallback::kF_FilterBackfaces;
		btCallback.m_collisionFilterGroup = groupMask.get_word();
		btCallback.m_collisionFilterMask = collideMask.get_word();
		// perform ray cast test
		mPhysics.dynamicsWorld->rayTest(bt3::LVecBase3_to_btVector3(rayFromWorld),
				bt3::LVecBase3_to_btVector3(rayToWorld), result);
		//
		return result;
	}
	else if (hitType == BT3RayTestResult::ALL)
	{
		BT3RayTestResult result(hitType, rayFromWorld, rayToWorld);
		BT3RayTestResult::ResultType& btCallback =
				static_cast<BT3RayTestResult::ResultType&>(result);
		btCallback.m_flags |= (btTriangleRaycastCallback::kF_KeepUnflippedNormal |
				btTriangleRaycastCallback::kF_UseSubSimplexConvexCastRaytest);
		btCallback.m_collisionFilterGroup = groupMask.get_word();
		btCallback.m_collisionFilterMask = collideMask.get_word();
		// perform ray cast test
		mPhysics.dynamicsWorld->rayTest(bt3::LVecBase3_to_btVector3(rayFromWorld),
				bt3::LVecBase3_to_btVector3(rayToWorld), result);
		//
		return result;
	}
	else
	{
		return 	BT3RayTestResult(BT3RayTestResult::NONE, LPoint3f::zero(),
				LPoint3f::zero());
	}
}

/**
 * Performs a swept convex cast on all objects and the returned
 * BT3ConvexTestResult contains the closest hit data, if get_has_hit() is true.
 * \note the test is performed against convexObject's collision (convex) shape.
 */
BT3ConvexTestResult GamePhysicsManager::convex_sweep_test(const NodePath& convexObject,
		CPT(TransformState) convexFromWorld, CPT(TransformState) convexToWorld,
		BT3ConvexTestResult::BT3HitType hitType, float allowedCcdPenetration,
		const CollideMask& groupMask, const CollideMask& collideMask) const
{
	///Note: code is redundant for performance reason
	BT3ConvexTestResult badResult(BT3ConvexTestResult::NONE, LPoint3f::zero(),
			LPoint3f::zero());
	RETURN_ON_COND(convexObject.is_empty(), badResult)

	btCollisionObject* object = bt3::getPhysicsObjectOfNode(convexObject,
			mPhysics);
	RETURN_ON_COND(!object, badResult)

	if (hitType == BT3ConvexTestResult::CLOSEST)
	{
		btConvexShape* castShape =
				bt3::upcastConvexShape(object->getCollisionShape());
		RETURN_ON_COND(!castShape, badResult)

		btTransform fromTrans, toTrans;
		fromTrans.setIdentity();
		toTrans.setIdentity();
		fromTrans.setOrigin(bt3::LVecBase3_to_btVector3(convexFromWorld->get_pos()));
		toTrans.setOrigin(bt3::LVecBase3_to_btVector3(convexToWorld->get_pos()));
		fromTrans.setRotation(bt3::LQuaternion_to_btQuat(convexFromWorld->get_quat()));
		toTrans.setRotation(bt3::LQuaternion_to_btQuat(convexToWorld->get_quat()));
		BT3ConvexTestResult result(hitType, convexFromWorld->get_pos(),
				convexToWorld->get_pos());
		BT3ConvexTestResult::ResultType& btCallback =
				static_cast<BT3ConvexTestResult::ResultType&>(result);
		btCallback.m_collisionFilterGroup = groupMask.get_word();
		btCallback.m_collisionFilterMask = collideMask.get_word();
		// perform convex sweep test
		mPhysics.dynamicsWorld->convexSweepTest(castShape, fromTrans, toTrans,
				result, allowedCcdPenetration);
		//
		return result;
	}
	else
	{
		return badResult;
	}
}

/**
 * Performs a discrete collision test between an object against all objects or
 * between two objects and the returned BT3ContactTestResult contains one or
 * more contact points for every overlapping object (including the one with
 * deepest penetration).
 */
BT3ContactTestResult GamePhysicsManager::contact_test(const NodePath& colObjA,
		const NodePath& colObjB, BT3ContactTestResult::BT3ContactType contactType,
		int maxNumContact, const CollideMask& groupMask,
		const CollideMask& collideMask) const
{
	///Note: code is redundant for performance reason
	BT3ContactTestResult badResult(BT3ContactTestResult::NONE, 0);
	RETURN_ON_COND(colObjA.is_empty(), badResult)

	btCollisionObject* objectA = bt3::getPhysicsObjectOfNode(colObjA, mPhysics);
	RETURN_ON_COND(!objectA, badResult)

	if ((contactType == BT3ContactTestResult::SINGLE) && colObjB.is_empty())
	{
		BT3ContactTestResult result(contactType, maxNumContact);
		BT3ContactTestResult::ResultType& btCallback =
				static_cast<BT3ContactTestResult::ResultType&>(result);
		btCallback.m_collisionFilterGroup = groupMask.get_word();
		btCallback.m_collisionFilterMask = collideMask.get_word();
		// perform contact test
		mPhysics.dynamicsWorld->contactTest(objectA, result);
		//
		return result;
	}
	else if ((contactType == BT3ContactTestResult::PAIR) && !colObjB.is_empty())
	{
		btCollisionObject* objectB = bt3::getPhysicsObjectOfNode(colObjB,
				mPhysics);
		RETURN_ON_COND(!objectB, badResult)

		BT3ContactTestResult result(contactType, maxNumContact);
		BT3ContactTestResult::ResultType& btCallback =
				static_cast<BT3ContactTestResult::ResultType&>(result);
		btCallback.m_collisionFilterGroup = groupMask.get_word();
		btCallback.m_collisionFilterMask = collideMask.get_word();
		// perform contact test
		mPhysics.dynamicsWorld->contactPairTest(objectA, objectB, result);
		//
		return result;
	}
	else
	{
		return badResult;
	}
}

/**
 * Sets a multi-valued parameter to a multi-value overwriting the existing one(s).
 */
void GamePhysicsManager::set_parameter_values(BT3PhysicType type, const string& paramName,
		const ValueList_string& paramValues)
{
	switch (type)
	{
	case RIGIDBODY:
		::set_parameter_values(mRigidBodiesParameterTable, paramName,
				paramValues);
		break;
	case SOFTBODY:
		::set_parameter_values(mSoftBodiesParameterTable, paramName,
				paramValues);
		break;
	case GHOST:
		::set_parameter_values(mGhostsParameterTable, paramName, paramValues);
		break;
	case CONSTRAINT:
		::set_parameter_values(mConstraintsParameterTable, paramName,
				paramValues);
		break;
	case VEHICLE:
		::set_parameter_values(mVehiclesParameterTable, paramName, paramValues);
		break;
	case CHARACTERCONTROLLER:
		::set_parameter_values(mCharacterControllersParameterTable, paramName, paramValues);
		break;
	default:
		break;
	}
}

/**
 * Gets the multiple values of a (actually set) parameter.
 */
ValueList_string GamePhysicsManager::get_parameter_values(BT3PhysicType type,
		const string& paramName) const
{
	switch (type)
	{
	case RIGIDBODY:
		return ::get_parameter_values(mRigidBodiesParameterTable, paramName);
	case SOFTBODY:
		return ::get_parameter_values(mSoftBodiesParameterTable, paramName);
	case GHOST:
		return ::get_parameter_values(mGhostsParameterTable, paramName);
	case CONSTRAINT:
		return ::get_parameter_values(mConstraintsParameterTable, paramName);
	case VEHICLE:
		return ::get_parameter_values(mVehiclesParameterTable, paramName);
	case CHARACTERCONTROLLER:
		return ::get_parameter_values(mCharacterControllersParameterTable, paramName);
	default:
		break;
	}
	return ValueList_string();
}

/**
 * Sets a multi/single-valued parameter to a single value overwriting the existing one(s).
 */
void GamePhysicsManager::set_parameter_value(BT3PhysicType type, const string& paramName,
		const string& value)
{
	ValueList_string valueList;
	valueList.add_value(value);
	set_parameter_values(type, paramName, valueList);
}

/**
 * Gets a single value (i.e. the first one) of a parameter.
 */
string GamePhysicsManager::get_parameter_value(BT3PhysicType type,
		const string& paramName) const
{
	ValueList_string valueList = get_parameter_values(type, paramName);
	return (valueList.size() != 0 ? valueList[0] : string(""));
}

/**
 * Gets a list of the names of the parameters actually set.
 */
ValueList_string GamePhysicsManager::get_parameter_name_list(BT3PhysicType type) const
{
	switch (type)
	{
	case RIGIDBODY:
		return ::get_parameter_name_list(mRigidBodiesParameterTable);
	case SOFTBODY:
		return ::get_parameter_name_list(mSoftBodiesParameterTable);
	case GHOST:
		return ::get_parameter_name_list(mGhostsParameterTable);
	case CONSTRAINT:
		return ::get_parameter_name_list(mConstraintsParameterTable);
	case VEHICLE:
		return ::get_parameter_name_list(mVehiclesParameterTable);
	case CHARACTERCONTROLLER:
		return ::get_parameter_name_list(mCharacterControllersParameterTable);
	default:
		break;
	}
	return ValueList_string();
}

/**
 * Sets all parameters to their default values (if any).
 * \note: After reading objects from bam files, the objects' creation parameters
 * which reside in the manager, are reset to their default values.
 */
void GamePhysicsManager::set_parameters_defaults(BT3PhysicType type)
{
	switch (type)
	{
	case RIGIDBODY:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"shape_type", "sphere"},
			{"child_shape_type", "convex_hull"},
			{"up_axis", "z"},
			{"group_mask", "all_on"},
			{"collide_mask", "all_on"},
			{"mass", "1.0"},
			{"linear_damping", "0.0"},
			{"angular_damping", "0.0"},
			{"friction", "0.5"},
			{"rolling_friction", "0.0"},
			{"spinning_friction", "0.0"},
			{"restitution", "0.0"},
			{"linear_sleeping_threshold", "0.8"},
			{"angular_sleeping_threshold", "1.0"},
			{"additional_damping", "false"},
			{"additional_damping_factor", "0.005"},
			{"additional_linear_damping_threshold_sqr", "0.01"},
			{"additional_angular_damping_threshold_sqr", "0.01"},
			{"additional_angular_damping_factor", "0.01"},
		};
		::set_parameters_defaults(mRigidBodiesParameterTable, nameValueList);
	}
		break;
	case SOFTBODY:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"body_type", "rope"},
			{"group_mask", "all_on"},
			{"collide_mask", "all_on"},
			{"total_mass", "1.0"},
			{"from_faces", "false"},
			{"rope_points", "0.0,0.0,0.0:1.0,1.0,1.0"},
			{"rope_fixeds", "0"},
			{"rope_res", "1"},
			{"patch_points", "-1.0,-1.0,0.0:1.0,-1.0,0.0:-1.0,1.0,0.0:1.0,1.0,0.0"},
			{"patch_fixeds", "0"},
			{"patch_resxy", "1:1"},
			{"patch_gendiags", "false"},
			{"ellipsoid_center", "0.0,0.0,0.0"},
			{"ellipsoid_radius", "1.0,1.0,1.0"},
			{"ellipsoid_res", "1"},
			{"triangle_mesh_rand_constraints", "false"},
			{"tetgen_facelinks", "false"},
			{"tetgen_tetralinks", "false"},
			{"tetgen_facesfromtetras", "false"},
		};
		::set_parameters_defaults(mSoftBodiesParameterTable, nameValueList);
	}
		break;
	case GHOST:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"thrown_events", "overlap@@30.0"},
			{"visible", "false"},
			{"shape_type", "sphere"},
			{"child_shape_type", "convex_hull"},
			{"up_axis", "z"},
			{"group_mask", "all_on"},
			{"collide_mask", "all_on"},
			{"kinematic", "true"},
		};
		::set_parameters_defaults(mGhostsParameterTable, nameValueList);
	}
		break;
	case CONSTRAINT:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"contraint_type", "point2point"},
			{"disable_objects_collisions", "false"},
			{"use_world_reference", "false"},
			{"pivot_references", "0.0,0.0,0.0:0.0,0.0,0.0"},
			{"axis_references", "1.0,1.0,1.0:1.0,1.0,1.0"},
			{"ratio", "0.0"},
			{"rotation_references", "0.0,0.0,0.0:0.0,0.0,0.0"},
			{"rotation_order", "xyz"},
			{"world_anchor", "0.0,0.0,0.0"},
			{"world_axes", "1.0,1.0,1.0:1.0,1.0,1.0"},
			{"use_reference_frame_a", "false"},
		};
		::set_parameters_defaults(mConstraintsParameterTable, nameValueList);
	}
		break;
	case VEHICLE:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			{"up_axis", "z"},
			{"wheels_number", "4"},
			{"wheel_apply_steering", "true:false"},
			{"wheel_apply_engine_force", "false:true"},
			{"wheel_apply_brake", "false:true"},
			{"connection_height_factor", "1.5:1.5"},
			{"connection_width_factor", "0.3:0.3"},
			{"connection_depth_factor", "1.5:1.5"},
			{"suspension_rest_lenght_factor", "1.2:1.2"},
			{"max_engine_force", "3.0"},
			{"max_brake", "1.5"},
			{"steering_clamp", "0.79"},
			{"steering_increment", "2.09"},
			{"steering_decrement", "1.05"},
			{"forward", "enabled"},
			{"backward", "enabled"},
			{"brake", "enabled"},
			{"head_left", "enabled"},
			{"head_right", "enabled"},
			{"suspension_stiffness", "5.88"},
			{"suspension_compression", "0.83"},
			{"suspension_damping", "0.88"},
			{"max_suspension_travel_cm", "500.0"},
			{"friction_slip", "10.5"},
			{"max_suspension_force", "6000.0"},
		};
		::set_parameters_defaults(mVehiclesParameterTable, nameValueList);
	}
		break;
	case CHARACTERCONTROLLER:
	{
		ValueList<Pair<string,string>> nameValueList
		{
			 {"up_vector", "0.0,0.0,1.0"},
			 {"gravity", "29.4"},
			 {"step_height", "1.0"},
			 {"max_linear_speed", "6.0"},
			 {"max_angular_speed", "1.6"},
			 {"linear_accel", "3.0"},
			 {"angular_accel", "0.8"},
			 {"linear_friction", "2.0"},
			 {"angular_friction", "2.0"},
			 {"stop_threshold", "0.01"},
			 {"local_linear_movement", "true"},
			 {"linear_damping", "0.0"},
			 {"angular_damping", "0.0"},
			 {"fall_speed", "55.0"},
			 {"jump_speed", "10.0"},
			 {"max_slope", "0.79"},
			 {"max_penetration_depth", "0.2"},
			 {"max_jump_height", "1.0"},
			 {"forward", "enabled"},
			 {"backward", "enabled"},
			 {"up", "enabled"},
			 {"down", "enabled"},
			 {"head_left", "enabled"},
			 {"head_right", "enabled"},
			 {"strafe_left", "enabled"},
			 {"strafe_right", "enabled"},
			 {"jump", "enabled"},
		};
		::set_parameters_defaults(mCharacterControllersParameterTable, nameValueList);
	}
		break;
	default:
		break;
	}
}

/**
 * Updates physics objects.
 *
 * Will be called automatically in a task.
 */
AsyncTask::DoneStatus GamePhysicsManager::update(GenericAsyncTask* task)
{
	float dt = ClockObject::get_global_clock()->get_dt();

#ifdef TESTING
	dt = 0.016666667; //60 fps
#endif

	// call all BT3RigidBodys' update functions, passing delta time
	for (auto rigidBody: mRigidBodies)
	{
		rigidBody->update(dt);
	}
	// call all BT3SoftBodys' update functions, passing delta time
	for (auto softBody: mSoftBodies)
	{
		softBody->update(dt);
	}
	// call all BT3Ghosts' update functions, passing delta time
	for (auto ghost: mGhosts)
	{
		ghost->update(dt);
	}
	// call all BT3Constraints' update functions, passing delta time
	for (auto constraint: mConstraints)
	{
		constraint->update(dt);
	}
	// call all BT3Vehicles' update functions, passing delta time
	for (auto vehicle: mVehicles)
	{
		vehicle->update(dt);
	}
	// call all BT3Characters' update functions, passing delta time
	for (auto characterController: mCharacterControllers)
	{
		characterController->update(dt);
	}

	// do physics step simulation
	///-----stepsimulation_start-----
	mPhysics.dynamicsWorld->stepSimulation(dt, 10);

	//soft body manager update
	mSoftBodyManager->update(dt);

	//vehicle manager update
	mVehicleManager->update(dt);

	//character controller manager update
	mCharacterControllerManager->update(dt);

	//debug draw
	if (mPhysics.doDebugDraw)
	{
		mPhysics.debugDrawer->initialize();
		mPhysics.dynamicsWorld->debugDrawWorld();
		mPhysics.debugDrawer->finalize();
	}

	/// Handle events
	// Collision notifications
	if (mCollision.mEnable)
	{
		//update general count:
		//only actual colliding object pairs have their counts updated,
		//while just stopped to collide object pairs will be erased from the set
		++mCollision.mCount;

		//elaborate current colliding object pair list
		if (mPhysics.dispatcher->getNumManifolds() > 0)
		{
			//update elapsed time
			mCollision.mTimeElapsed += dt;
			// iterate through all of the manifolds in the dispatcher
			for (int i = 0; i < mPhysics.dispatcher->getNumManifolds(); ++i)
			{
				// get the manifold
				btPersistentManifold* pManifold =
						mPhysics.dispatcher->getManifoldByIndexInternal(i);

				// ignore manifolds that have
				// no contact points.
				if (pManifold->getNumContacts() > 0)
				{
					// get the two rigid bodies' panda nodes involved in the collision
					PandaNode *node0 =
							reinterpret_cast<PandaNode*>(pManifold->getBody0()->getUserPointer());
					PandaNode *node1 =
							reinterpret_cast<PandaNode*>(pManifold->getBody1()->getUserPointer());
					//insert a default: check of equality is done only on CollidingNodePair::mPnode member
					pair<pset<CollidingNodePair>::iterator, bool> res =
							mCollidingNodePairs.insert(
									CollidingNodePair(node0, node1));
					if (res.second)
					{
						// this is a "new" colliding object pair
						string nodeName0 = node0->get_name();
						string nodeName1 = node1->get_name();
						//alphabetically compare
						if (nodeName0 < nodeName1)
						{
							// set the event name: <nodeName0>_<nodeName1>_Collision
							(res.first)->mCollidingNodePairData->mEventName =
									(mCollision.mEventName.empty()?
									nodeName0 + "_" + nodeName1 + "_Collision"
									:
									mCollision.mEventName);
							// set the parameters
							(res.first)->mCollidingNodePairData->mEventParameters[0] =
									EventParameter(node0);
							(res.first)->mCollidingNodePairData->mEventParameters[1] =
									EventParameter(node1);
						}
						else
						{
							// set the event name
							(res.first)->mCollidingNodePairData->mEventName =
									(mCollision.mEventName.empty() ?
									nodeName1 + "_" + nodeName0 + "_Collision"
									:
									mCollision.mEventName);
							// set the parameters
							(res.first)->mCollidingNodePairData->mEventParameters[0] =
									EventParameter(node1);
							(res.first)->mCollidingNodePairData->mEventParameters[1] =
									EventParameter(node0);
						}
						// throw the event
						throw_event(
								(res.first)->mCollidingNodePairData->mEventName,
								(res.first)->mCollidingNodePairData->mEventParameters[0],
								(res.first)->mCollidingNodePairData->mEventParameters[1]);
					}
					else
					{
						// this is an "old" colliding object pair
						if (mCollision.mTimeElapsed
								>= mCollision.mPeriod)
						{
							// throw the event
							throw_event(
									(res.first)->mCollidingNodePairData->mEventName,
									(res.first)->mCollidingNodePairData->mEventParameters[0],
									(res.first)->mCollidingNodePairData->mEventParameters[1]);
						}
					}
					// update count flag
					(res.first)->mCollidingNodePairData->mCount =
							mCollision.mCount;
				}
			}
			// update elapsed time
			if (mCollision.mTimeElapsed >= mCollision.mPeriod)
			{
				mCollision.mTimeElapsed -= mCollision.mPeriod;
			}
		}
		else
		{
			mCollision.mTimeElapsed = 0.0;
		}

		//erase just stopped to collide object pairs (which have not the count flag updated)
		for (pset<CollidingNodePair>::iterator iter =
				mCollidingNodePairs.begin(); iter != mCollidingNodePairs.end();)
		{
			//check if it has a previous count
			if (iter->mCollidingNodePairData->mCount != (int) mCollision.mCount)
			{
				//throw the "off" event
				//event name: <nodeName0>_<nodeName1>_CollisionOff
				throw_event(iter->mCollidingNodePairData->mEventName + "Off",
						iter->mCollidingNodePairData->mEventParameters[0],
						iter->mCollidingNodePairData->mEventParameters[1]);
				//erase the object
				mCollidingNodePairs.erase(iter++);
			}
			else
			{
				++iter;
			}
		}
	}
	//
	return AsyncTask::DS_cont;
}

/**
 * Adds a task to repeatedly call physics updates.
 */
void GamePhysicsManager::start_default_update()
{
	//create the task for updating AI objects
	mUpdateData = new TaskInterface<GamePhysicsManager>::TaskData(this,
			&GamePhysicsManager::update);
	mUpdateTask = new GenericAsyncTask(string("GamePhysicsManager::update"),
			&TaskInterface<GamePhysicsManager>::taskFunction,
			reinterpret_cast<void*>(mUpdateData.p()));
	mUpdateTask->set_sort(mTaskSort);
	//Adds mUpdateTask to the active queue.
	AsyncTaskManager::get_global_ptr()->add(mUpdateTask);
}

/**
 * Removes a task to repeatedly call physics updates.
 */
void GamePhysicsManager::stop_default_update()
{
	if (mUpdateTask)
	{
		AsyncTaskManager::get_global_ptr()->remove(mUpdateTask);
	}
	//
	mUpdateData.clear();
	mUpdateTask.clear();
}

/**
 * Gets bounding dimensions (AABB) of a model NodePath.
 * Puts results into the out parameters: modelDims, modelDeltaCenter && returns
 * modelRadius.
 * - modelDims = absolute dimensions of the model
 * - modelCenter + modelDeltaCenter = <origin of coordinate system>
 * - modelRadius = radius of the containing sphere
 * \note modelRadius is computed on the plane orthogonal to upAxis.
 */
float GamePhysicsManager::get_bounding_dimensions(NodePath modelNP,
		LVecBase3f& modelDims, LVector3f& modelDeltaCenter, BT3UpAxis upAxis) const
{
	return bt3::getBoundingDimensions(modelNP, modelDims, modelDeltaCenter,
			static_cast<bt3::Panda3dUpAxis>(upAxis));
}

/**
 * Throws a ray downward (-z direction default) from fromPos.
 * If collisions are found returns a Pair_bool_float == (true, height), with
 * height equal to the z-value of the first one.
 * If collisions are not found returns a Pair_bool_float == (false, 0.0).
 */
Pair_bool_float GamePhysicsManager::get_collision_height(const LPoint3f& fromPos)
{
	//cast a ray downward starting at fromPos
	LPoint3f toPos = LPoint3f(fromPos.get_x(), fromPos.get_y(), -FLT_MAX);
	//Transform to bullet
	const btVector3& rayFromWorld = bt3::LVecBase3_to_btVector3(fromPos);
	const btVector3& rayToWorld = bt3::LVecBase3_to_btVector3(toPos);
	//cast a ray to detect a body
	btCollisionWorld::ClosestRayResultCallback rayCallback(rayFromWorld,
			rayToWorld);
	//hit all objects
	rayCallback.m_collisionFilterGroup = mGroupMask.get_word();
	rayCallback.m_collisionFilterMask = mCollideMask.get_word();
	mPhysics.dynamicsWorld->rayTest(rayFromWorld, rayToWorld, rayCallback);
	//
	if (rayCallback.hasHit())
	{
		return Pair<bool, float>(true, rayCallback.m_hitPointWorld.getZ());
	}
	//
	return Pair<bool, float>(false, 0.0);
}

/**
 * Enables/disables physics notifications through events.
 * - *Collision notification*:
 * for each pair of physic PandaNodes that are colliding the
 * "<nodeName0>_<nodeName1>_Collision" (or "eventName" if eventName is not
 * empty) event is emitted at the given frequency; for each pair of physic
 * PandaNodes that stop colliding the "<nodeName0>_<nodeName1>_CollisionOff" (or
 * "eventNameOff" if eventName is not empty) event is emitted once; both events
 * have as arguments the two PandaNodes (note1: <objectName0>
 * lexicographically precedes <objectName1>).
 */
void GamePhysicsManager::enable_throw_event(BT3EventThrown event, bool enable,
		float eventFreq, const string& eventName)
{
	ThrowEventData eventData(eventName, abs(eventFreq));
	eventData.mEnable = enable;
	switch (event)
	{
	case COLLISIONNOTIFY:
		mCollision = eventData;
		break;
	default:
		break;
	}
}

/**
 * Writes to a bam file the entire collections of physics objects && related
 * geometries (i.e. models' NodePaths)
 */
bool GamePhysicsManager::write_to_bam_file(const string& fileName)
{
	string errorReport;
	// write to bam file
	BamFile outBamFile;
	if (outBamFile.open_write(Filename(fileName)))
	{
		cout << "Current system Bam version: "
				<< outBamFile.get_current_major_ver() << "."
				<< outBamFile.get_current_minor_ver() << endl;
		// just write the reference node
		if (!outBamFile.write_object(mReferenceNP.node()))
		{
			errorReport += string("Error writing ") + mReferenceNP.get_name()
					+ string(" node in ") + fileName + string("\n");
		}
		// close the file
		outBamFile.close();
	}
	else
	{
		errorReport += string("\nERROR: cannot open ") + fileName;
	}
	//check
	if (errorReport.empty())
	{
		cout
				<< "SUCCESS: all physics object collections were written to "
				<< fileName << endl;
	}
	else
	{
		cerr << errorReport << endl;
	}
	return errorReport.empty();
}

/**
 * Reads from a bam file the entire hierarchy of physics objects && related
 * geometries (i.e. models' NodePaths)
 */
bool GamePhysicsManager::read_from_bam_file(const string& fileName)
{
	string errorReport;
	//read from bamFile
	BamFile inBamFile;
	if (inBamFile.open_read(Filename(fileName)))
	{
		cout << "Current system Bam version: "
				<< inBamFile.get_current_major_ver() << "."
				<< inBamFile.get_current_minor_ver() << endl;
		cout << "Bam file version: " << inBamFile.get_file_major_ver() << "."
				<< inBamFile.get_file_minor_ver() << endl;
		// just read the reference node
		TypedWritable* reference = inBamFile.read_object();
		if (reference)
		{
			//resolve pointers
			if (!inBamFile.resolve())
			{
				errorReport += string("Error resolving pointers in ") + fileName
						+ string("\n");
			}
		}
		else
		{
			errorReport += string("Error reading ") + fileName + string("\n");
		}
		// close the file
		inBamFile.close();
		// post process after restoring
		// restore reference node
		mReferenceNP = NodePath::any_path(DCAST(PandaNode, reference));
		/// post processing from bam for all managed objects: two step process
		for (int i = 0; i < 2; i++)
		{
			for (auto rigidBody : mRigidBodies)
			{
				rigidBody->post_process_from_bam();
			}
			for (auto ghost : mGhosts)
			{
				ghost->post_process_from_bam();
			}
			for (auto constraint : mConstraints)
			{
				constraint->post_process_from_bam();
			}
			for (auto softBody : mSoftBodies)
			{
				softBody->post_process_from_bam();
			}
			for (auto vehicle : mVehicles)
			{
				vehicle->post_process_from_bam();
			}
			for (auto characterController : mCharacterControllers)
			{
				characterController->post_process_from_bam();
			}
		}
	}
	else
	{
		errorReport += string("\nERROR: cannot open ") + fileName;
	}
	//check
	if (errorReport.empty())
	{
		cout << "SUCCESS: all physics objects were read from "
				<< fileName << endl;
	}
	else
	{
		cerr << errorReport << endl;
	}
	return errorReport.empty();
}

/**
 * Enables/disables debug drawing.
 */
void GamePhysicsManager::debug(bool enable)
{
#ifdef ELY_DEBUG
	if (enable)
	{
		if (!mPhysics.doDebugDraw)
		{
			mPhysics.doDebugDraw = true;
			mPhysics.debugDrawer->clear();
		}
	}
	else
	{
		if (mPhysics.doDebugDraw)
		{
			mPhysics.doDebugDraw = false;
			mPhysics.debugDrawer->clear();
		}
	}
#endif //ELY_DEBUG
}

TYPED_OBJECT_API_DEF(GamePhysicsManager)
