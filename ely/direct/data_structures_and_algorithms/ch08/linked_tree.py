# Copyright 2013, Michael H. Goldwasser
#
# Developed for use with the book:
#
#    Data Structures and Algorithms in Python
#    Michael T. Goodrich, Roberto Tamassia, and Michael H. Goldwasser
#    John Wiley & Sons, 2013
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .tree import Tree
from ..ch07.linked_queue import LinkedQueue

class LinkedTree(Tree):
    """Linked representation of a tree structure."""

    #-------------------------- nested _Node class --------------------------
    class _Node:
        """Lightweight, nonpublic class for storing a node."""
        __slots__ = '_element', '_parent', '_children' # streamline memory usage

        def __init__(self, element, parent=None, children=None):
            self._element = element
            self._parent = parent
            if not children:
                self._children = []
            elif not isinstance(children, list):
                raise TypeError('children must be (or derived from) a list')
            else:
                self._children = children

    #-------------------------- nested Position class --------------------------
    class Position(Tree.Position):
        """An abstraction representing the location of a single element."""

        def __init__(self, container, node):
            """Constructor should not be invoked by user."""
            self._container = container
            self._node = node

        def element(self):
            """Return the element stored at this Position."""
            return self._node._element

        def __eq__(self, other):
            """Return True if other is a Position representing the same location."""
            return type(other) is type(self) and other._node is self._node

    #------------------------------- utility methods -------------------------------
    def _validate(self, p):
        """Return associated node, if position is valid."""
        if not isinstance(p, self.Position):
            raise TypeError('p must be proper Position type')
        if p._container is not self:
            raise ValueError('p does not belong to this container')
        if p._node._parent is p._node:      # convention for deprecated nodes
            raise ValueError('p is no longer valid')
        return p._node

    def _make_position(self, node):
        """Return Position instance for given node (or None if no node)."""
        return self.Position(self, node) if node is not None else None

    #-------------------------- tree constructor --------------------------
    def __init__(self):
        """Create an initially empty tree."""
        self._root = None
        self._size = 0

    #-------------------------- public accessors --------------------------
    def __len__(self):
        """Return the total number of elements in the tree."""
        return self._size
    
    def root(self):
        """Return the root Position of the tree (or None if tree is empty)."""
        return self._make_position(self._root)

    def parent(self, p):
        """Return the Position of p's parent (or None if p is root)."""
        node = self._validate(p)
        return self._make_position(node._parent)

    def num_children(self, p):
        """Return the number of children of Position p."""
        node = self._validate(p)
        return len(node._children)
    
    def child(self, p, i):
        """Return the Position of p's i-th child (or None if no i-th child)."""
        node = self._validate(p)
        if (i >= 0) and (i < len(node._children)):
            return self._make_position(node._children[i])
        else:
            return None
        
    def siblings(self, p):
        """Return a Position list representing p's siblings (or None if no siblings)."""
        parent = self.parent(p)
        if parent is None:                    # p must be the root
            return None                         # root has no siblings
        else:
            node = self._validate(p)
            nIdx = parent._children.index(node)
            siblings = parent._children[:nIdx] + parent._children[nIdx + 1:]
            return [self._make_position(sibling) for sibling in siblings]

    def children(self, p):
        """Generate an iteration of Positions representing p's children."""
        node = self._validate(p)
        for child in node._children:
            yield self._make_position(child)

    #-------------------------- nonpublic mutators --------------------------
    def _add_root(self, e):
        """Place element e at the root of an empty tree and return new Position.

        Raise ValueError if tree nonempty.
        """
        if self._root is not None:
            raise ValueError('Root exists')
        self._size = 1
        self._root = self._Node(e)
        return self._make_position(self._root)

    def _add_child(self, p, e):
        """Create a new child for Position p, storing element e.

        Return the Position of new node.
        Raise ValueError if Position p is invalid or p already has a child.
        """
        node = self._validate(p)
        self._size += 1
        node._children.append(self._Node(e, node))                  # node is its parent
        return self._make_position(node._children[-1])

    def _replace(self, p, e):
        """Replace the element at position p with e, and return old element."""
        node = self._validate(p)
        old = node._element
        node._element = e
        return old

    def _delete(self, p):
        """Delete the node at Position p, and replace it with its child, if any.

        Return the element that had been stored at Position p.
        Raise ValueError if Position p is invalid or p has more than one child.
        """
        node = self._validate(p)
        if self.num_children(p) > 1:
            raise ValueError('Position has more than one child')
        child = node._children[0] if len(node._children) == 1 else None # might be None
        if child is not None:
            child._parent = node._parent   # child's grandparent becomes parent
        if node is self._root:
            self._root = child             # child becomes root
        else:
            parent = node._parent
            childPIdx = parent._children.index(node)
            if child:
                # replace it with its child in the parent's child list
                parent._children[childPIdx] = child
            else:
                # None: remove it from the parent's child list
                del parent._children[childPIdx]
                
        self._size -= 1
        node._parent = node              # convention for deprecated node
        return node._element
    
    def _attach(self, p, t1):
        """Attach tree t1 as subtree of the external Position p.

        As a side effect, set t1 to empty.
        Raise TypeError if trees t1 do not match type of this tree.
        Raise ValueError if Position p is invalid or not external.
        """
        node = self._validate(p)
        if not self.is_leaf(p):
            raise ValueError('position must be leaf')
        if not type(self) is type(t1):    # all 2 trees must be same type
            raise TypeError('Tree types must match')
        self._size += len(t1)
        if not t1.is_empty():         # attached t1 as subtree of node
            t1._root._parent = node
            node._children.append(t1._root)
            t1._root = None             # set t1 instance to empty
            t1._size = 0
            
    def _destroy(self, p):
        """Destroy the (sub)tree at Position p."""
        node = self._validate(p)
        size = 0
        for c in self._subtree_preorder(p):
            size += 1
        parent = node._parent
        if parent:
            idx = parent._children.index(node)
            parent._children = parent._children[:idx] + parent._children[idx + 1:]
        if node is self._root:
            self._root = None
        self._size -= size
        
    def _make_copy(self, copyElement=None):
        """Return a copy of this tree.
        
        The element of each node of the copy tree is established 
        by the argument 'copyElement', which can be either a 
        callable which takes the source element as its argument 
        and returns the destination element, or None which indicates 
        that each destination element is None.
        """
        treeCopy = LinkedTree()
        pQueue = LinkedQueue()
        for p in self.breadthfirst():
            childNum = self.num_children(p)
            element = copyElement(p.element()) if copyElement else None
            if self.is_root(p): # add root node
                pC = treeCopy._add_root(element)
            else: # add remaining nodes
                pC = pQueue.dequeue()
                pC = treeCopy._add_child(pC, element)
            while childNum > 0:
                pQueue.enqueue(pC)
                childNum -= 1
        return treeCopy
    
    
