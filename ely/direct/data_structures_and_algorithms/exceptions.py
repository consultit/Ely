'''
Created on Dec 26, 2017

@author: consultit
'''

class Empty(Exception):
    '''
    Error attempting to access an element from an empty container.
    '''
    pass
        