#!/usr/bin/env python

'''
Created on Dec 2, 2014

Walks model's joint hierarchy.

@author: consultit
'''

# #####
from panda3d.core import NodePath, LPoint3f, CardMaker, TextNode, Filename, \
    load_prc_file_data, LineSegs, CharacterJoint, LVecBase4f, LVector3f
from direct.actor.Actor import Actor
from direct.showbase.ShowBase import ShowBase
import sys
import os
import argparse
import textwrap

load_prc_file_data('', 'win-size 1024 768')
load_prc_file_data('', 'show-frame-rate-meter #t')
load_prc_file_data('', 'sync-video #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')


def drawJoint(nodeJoint, dimFactor, render, scale, color, drawBin):
    jointCard = CardMaker(nodeJoint.get_name())
    cardDim = 0.3 * dimFactor
    jointCard.set_frame(-cardDim, cardDim, -cardDim, cardDim)
    jointSquare = NodePath(jointCard.generate())
    jointSquare.reparent_to(nodeJoint)
    jointSquare.set_hpr(render, 0, 0, 0)
    jointSquare.set_scale(scale)
    jointSquare.set_color(color)
    jointSquare.set_two_sided(True)
    jointSquare.set_bin('fixed', drawBin)
    jointSquare.set_depth_write(False)
    jointSquare.set_depth_test(False)


def drawBone(nodeJoint, parentNode, thickness, drawBin, color):
    bone = LineSegs()
    bone.set_thickness(thickness)
    # bone.set_color(random(), random(), random())
    bone.move_to(0, 0, 0)
    bone.draw_to(nodeJoint.get_pos(parentNode))
    lnp = parentNode.attach_new_node(bone.create())
    lnp.set_bin('fixed', drawBin)
    lnp.set_depth_write(False)
    lnp.set_depth_test(False)
    # set color: brighter at parent
    # bone.set_color(color)
    bone.set_vertex_color(0, color * 1.5)
    bone.set_vertex_color(1, color * 0.5)


def drawAxis(pos, toward, length, color, thickness, parent, drawBin):
    axis = LineSegs()
    axis.set_thickness(thickness)
    axis.set_color(color)
    axis.move_to(pos)
    axis.draw_to(pos + toward * length)
    axisnp = parent.attach_new_node(axis.create())
    axisnp.set_bin('fixed', drawBin)
    axisnp.set_depth_write(False)
    axisnp.set_depth_test(False)


def drawFrame(nodeFrame, np, pos, length, thickness, drawBin):
    x = nodeFrame.get_relative_vector(np, LVector3f.right()).normalized()
    drawAxis(pos, x, length, LVecBase4f(1, 0, 0, 1), thickness,
             nodeFrame, drawBin)
    y = nodeFrame.get_relative_vector(np, LVector3f.forward()).normalized()
    drawAxis(pos, y, length, LVecBase4f(0, 1, 0, 1), thickness,
             nodeFrame, drawBin)
    z = nodeFrame.get_relative_vector(np, LVector3f.up()).normalized()
    drawAxis(pos, z, length, LVecBase4f(0, 0, 1, 1), thickness,
             nodeFrame, drawBin)


def drawText(nodeText, render, pos, hpr, part, color, textScale, drawBin):
    nodeText.set_pos_hpr(render, pos, hpr)
    text = TextNode(part.get_name())
    text.set_text(part.get_name())
    text.set_card_color(color)
    text.set_card_as_margin(0, 0, 0, 0)
    text.set_card_decal(True)
    textNodePath = nodeText.attach_new_node(text)
    textNodePath.set_scale(textScale)
    textNodePath.set_color(0, 0, 0)
    textNodePath.set_bin('fixed', drawBin)
    textNodePath.set_depth_write(False)
    textNodePath.set_depth_test(False)
    textNodePath.set_billboard_point_eye()
    return text


def walkJointHierarchy(actor, part, indent1='', indent2='', parentNode=None):
    global app, nodeDecorator, jointRoot, textRoot, frameRoot, jointBodyNP, \
        actorScale, bodyRadius, textScale
    # return value
    jointNameList = []
    if not part:
        # there is no joint hierarchy
        return jointNameList
    if parentNode == None:
        # start hierarchy
        nodeDecorator = app.render.attach_new_node('nodeDecorator')
        nodeJoint = nodeDecorator.attach_new_node(
            '_ROOTBONE_' + part.get_name())
        nodeText = nodeDecorator.attach_new_node(
            '_ROOTTEXT_' + part.get_name())
        nodeFrame = nodeDecorator.attach_new_node(
            '_ROOTFRAME_' + part.get_name())
        jointRoot[0] = nodeJoint
        textRoot[0] = nodeText
        frameRoot[0] = nodeFrame
    if not isinstance(part, CharacterJoint):
        # it is a PartGroup or descendant thereof
        parentNode = jointRoot[0]
        # print part name
        jointNameList.append(part.get_name())
        print(indent1 + str(part.get_type().get_name()) + '.' + part.get_name())
        indent1 = indent1 + indent2
    elif isinstance(part, CharacterJoint):
        # it is a CharacterJoint
        jointNameList.append(part.get_name())
        np = actor.expose_joint(None, 'modelRoot', part.get_name())
        nodeJoint = parentNode.attach_new_node('_BONE_' + part.get_name())
        nodeText = textRoot[0].attach_new_node('_TEXT_' + part.get_name())
        nodeFrame = frameRoot[0].attach_new_node('_FRAME_' + part.get_name())
        pos = np.get_pos(app.render)
        hpr = np.get_hpr(app.render)
        nodeJoint.set_pos_hpr(app.render, pos, hpr)

        # draw joint
        drawJoint(nodeJoint, bodyRadius, app.render, scale,
                  LVecBase4f(255.0 / 255.0, 128.0 / 255.0, 0.0 / 255.0, 1), 45)

        # draw bone
        if parentNode and parentNode.get_name().startswith('_BONE_'):
            drawBone(nodeJoint, parentNode, 5.0, 40,
                     LVecBase4f(102.0 / 255.0, 0.0 / 255.0, 51.0 / 255.0, 1.0))

        parentNode = nodeJoint

        # draw joint frame's axes
        drawFrame(nodeFrame, np, pos, 1.2 * bodyRadius, 1.5, 51)

        # draw joint name
        text = drawText(nodeText, app.render, pos, hpr, part,
                        LVecBase4f(1, 1, 0.5, 1), textScale, 50)
        print(indent1 + text.get_text())
        indent1 = indent1 + indent2

    for child in part.get_children():
        childJointNameList = walkJointHierarchy(
            actor, child, indent1, indent2, parentNode)
        jointNameList += childJointNameList
    # return joint name list
    return jointNameList


def toggleActor():
    global actor
    if actor.is_hidden():
        actor.show()
    else:
        actor.hide()


def toggleDecoration(rootNode):
    if rootNode[0].is_hidden():
        rootNode[0].show()
    else:
        rootNode[0].hide()


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will walk the joints' hierarchy and prints it
    using the characters [indent1] and [indent2]
      
    It will print also the 'egg-optchar' command that will create
    into the [reparentDir] the new 'egg' files with all joints 
    reparented  wrt root, by specifying the [originalEggsFiles].
      
    Commands:
        - 'F1': to hide/show the model.
        - 'F2': to hide/show the joints.
        - 'F3': to hide/show the joints' names.
        - 'F4': to hide/show the joints' frames.
        - 'shift-mouse1': hitting it two consecutive 
              times on two joints will print their 
              distance and the maximum radius of the
              actor bone capsule shape.
    '''))
    # set up arguments
    parser.add_argument('-a', '--actor', type=str,
                        default='panda', help='the actor model')
    parser.add_argument('-s', '--scale', type=float,
                        default=1.0, help='the actor scale')
    parser.add_argument('-d', '--data-dir', type=str,
                        action='append', help='the data dir(s)')
    parser.add_argument('--indent1', type=str, default='+',
                        help='the indent1 character')
    parser.add_argument('--indent2', type=str, default='-',
                        help='the indent2 character')
    parser.add_argument('--reparent-dir', type=str,
                        default='.', help='the reparent directory')
    parser.add_argument('-e', '--original-egg-file', type=str, action='append',
                        help='the original egg file(s)')
    # parse arguments
    args = parser.parse_args()
    # do actions
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    # create app
    app = ShowBase()
    # actor model
    actor = Actor(args.actor)
    actor.reparent_to(app.render)
    actor.set_bin('background', 1)
    actor.show_tight_bounds()
    # actor scale
    scale = args.scale
    actor.set_scale(scale)
    # get 'tight' dimensions of actor
    minP = LPoint3f()
    maxP = LPoint3f()
    actor.calc_tight_bounds(minP, maxP)
    delta = maxP - minP
    actorDims = LVector3f(abs(delta.get_x()), abs(
        delta.get_y()), abs(delta.get_z()))
    #minDim = min(min(actorDims.get_x(), actorDims.get_y()), actorDims.get_z())
    #maxDim = max(max(actorDims.get_x(), actorDims.get_y()), actorDims.get_z())
    mediumDim = (actorDims.get_x() + actorDims.get_y() +
                 actorDims.get_z()) / 3.0
    # body radius of body node path
    bodyRadius = mediumDim / 50.0
    # text dimension
    textScale = mediumDim / 75.0
    #
    app.accept('f1', toggleActor)
    # indent1 char
    indent1 = args.indent1
    # indent2 char
    indent2 = args.indent2
    #
    # hiding/showing decorations logics
    nodeDecorator = None
    jointRoot = [NodePath('jointRoot')]
    textRoot = [NodePath('textRoot')]
    frameRoot = [NodePath('frameRoot')]

    app.accept('f2', toggleDecoration, [jointRoot])
    app.accept('f3', toggleDecoration, [textRoot])
    app.accept('f4', toggleDecoration, [frameRoot])
    #
    jointBodyNP = {}
    # show the joint Hierarchy
    print('############ Joint Hierarchy ############')
    jointNameList = walkJointHierarchy(
        actor, actor.get_part_bundle('modelRoot'), indent1, indent2)
    # print command to reparent joints to root
    print('\n############ Command to reparent all joints to root ############')
    # reparent dir
    reparentDir = args.reparent_dir
    cmdStr = 'egg-optchar -d ' + reparentDir
    for jointName in jointNameList:
        cmdStr = cmdStr + ' -p ' + jointName + ','
    cmdStr = cmdStr.rstrip(',')
    # original eggs files
    originalEggsFiles = ' '
    if args.original_egg_file:
        for origEggFile in args.original_egg_file:
            originalEggsFiles += origEggFile + ' '
    cmdStr = cmdStr + originalEggsFiles.rstrip()
    print(cmdStr)
    #
    xC = (minP.get_x() + maxP.get_x()) / 2.0
    yC = minP.get_y() - actorDims.get_z() * 3.8 / 2.0
    zC = minP.get_z() + actorDims.get_z() * 2.0 / 4.0
    xL = (minP.get_x() + maxP.get_x()) / 2.0
    yL = (minP.get_y() + maxP.get_y()) / 2.0
    zL = (minP.get_z() + maxP.get_z()) / 2.0
    #
    app.trackball.node().set_origin(LVector3f(xL, yL, zL))
    app.trackball.node().set_pos(-xC + xL, -yC + yL, -zC + zL)
    app.trackball.node().set_hpr(0, 0, 0)
    #
    app.run()
