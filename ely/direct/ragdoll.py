'''
Created on Jan 08, 2018

Class implementing physics based ragdoll.

@author: consultit
'''

from math import radians
import textwrap
from panda3d.core import BitMask32, CharacterJoint, NodePath, LMatrix4f, \
    GeomVertexArrayFormat, InternalName, Geom, GeomTriangles, GeomVertexData, \
    GeomVertexWriter, LPoint3f, GeomVertexFormat, GeomNode, LVecBase3f, \
    TransformState, OmniBoundingVolume, LVector3f
from direct.actor.Actor import Actor
from .data_structures_and_algorithms.ch08.linked_tree import LinkedTree
from .data_structures_and_algorithms.ch07.linked_queue import LinkedQueue
from ..libtools import *
from ..physics import GamePhysicsManager, BT3RigidBody, BT3Constraint


class ActorRagDollExcpt(Exception):
    '''Generic error about ActorRagDoll.
    '''
    pass


class ActorRagDoll(Actor):
    '''ActorRagDoll implements a physics based ragdoll.

    The ragdoll will be assembled from (a subset of) the first CharacterJoint's
    hierarchy found inside the Actor.

    :param physicsMgr: the game physics manager object.
    :type physicsMgr: class: `ely.physics.GamePhysicsManager`
    :param jointHierarchyArray: this is the 'array based representation'
        of the ragdoll's joint hierarchy tree. More precisely, the joint
        hierarchy is considered an 'ordered' tree, that is there is an
        arbitrary meaningful linear order among the children of each
        joint and this order must be consistent across all the data
        structures belonging or related to an ActorRagDoll object. In an
        'array based representation' the joints of each level are stored
        consecutively, maintaining the aforementioned ordering of the
        children, starting from level 0.
        Not all joints of the original joint hierarchy need to
        participate in the definition of the ragdoll hierarchy tree,
        indeed, generally only a significant subset of joints are chosen
        to obtain a physically credible ragdoll. Each array element
        represents the parameters of a joint and is specified as a
        dictionary with the following keys:
            - 'joint': the name of the joint(string).
            - 'children': the number of the joint's children(int).
            - 'jointP': the name of joint's original parent(name). This
                parameter is optional, and has to be specified only if
                the actual parent differs from that of the original
                hierarchy.
            - 'bone': the attributes of the rigid body attached to this
                joint, which simulates the bone of the ragdoll attached
                between this joint and its parent and whose transform
                will control this joint. The attributes are specified as
                a dictionary with the following keys:
                - 'type'(GamePhysicsManager.BT3ShapeType): the shape
                    type of the body; actually only 
                    (GamePhysicsManager.) SPHERE, CAPSULE and BOX have
                    been tested.
                - 'mass'(float): the mass of the body. 
                - 'dims'(float list): a list of the bounding box's
                    dimensions of the body; 1 to 3 dimensions must be
                    specified.
                - 'upAxis'(GamePhysicsManager.BT3UpAxis): the up axis of
                    the body shape; it is optional and defaults to
                    GamePhysicsManager.Z_up.
            - 'constraint': the attributes of the constraint attached
                between the rigid body of this joint and that of its
                parent, which simulates the juncture between bones of
                the ragdoll. The attributes are specified as a
                dictionary with the following keys:
                - 'type'(BT3Constraint.BT3ConstraintType): the type of
                    the constraint; actually only (BT3Constraint.)
                    CONETWIST, DOF6 and POINT2POINT have been tested.
                - 'deltaPivot'(LVector3f): the position of the
                    constraint's pivot point is computed by summing the
                    position of the joint with this offset. This
                    parameter is optional.
                - 'swingSpan1'(float),'swingSpan2'(float),'twistSpan'
                    (float), 'softness'(float),'biasFactor'(float),
                    'relaxationFactor'(float): these parameters are
                    optional and could be specified only for the
                    BT3Constraint.CONETWIST constraint type.
                - 'limits'(list),'params'(list): these parameters are
                    optional and could be specified only for the
                    BT3Constraint.DOF6 constraint type; each element of
                    'limits' list is a tuple (axis, lo, hi)
                    corresponding to the arguments of the
                    Dof6Attrs.set_limit() method; each element of the 
                    'params' list is a tuple (num, value, axis)
                    corresponding to the arguments of the
                    TypedConstraintAttrs.set_param() method.
    :type jointHierarchyArray: list
    :param groupMask: the group mask common to all ragdoll's rigid
        bodies (bones).
    :type groupMask: class: `panda3d.core.BitMask32`
    :param collideMask: the collide mask common to all ragdoll's
        rigid bodies (bones).
    :type collideMask: class: `panda3d.core.BitMask32`
    :param actorKwArgs: keyword arguments passed directly to the Actor parent
        class.
    :type actorKwArgs: dict
    '''

    def __init__(self, physicsMgr, jointHierarchyArray=[],
                 groupMask=BitMask32.all_on(), collideMask=BitMask32.all_on(),
                 **actorKwArgs):
        '''Constructor
        '''

        #Actor.__init__(self, **actorKwArgs)
        super(ActorRagDoll, self).__init__(**actorKwArgs)
        # data members
        self.jHierarchy = None
        self.jHierarchyArray = jointHierarchyArray[:]
        # helper member
        self.jNetTransMat = LMatrix4f()
        # physics data
        self.physicsMgr = (physicsMgr if isinstance(physicsMgr,
                                                    GamePhysicsManager)
                           else GamePhysicsManager.get_global_ptr())
        self.render = self.physicsMgr.get_reference_node_path()
        # setup common physics parameters
        self.gMask = str((groupMask if isinstance(groupMask, BitMask32)
                          else BitMask32(groupMask)).get_word())
        self.cMask = str((collideMask if isinstance(collideMask, BitMask32)
                          else BitMask32(collideMask)).get_word())

    def setup(self, app, updateSort=20):
        '''Setups the ragdoll.

        This method will perform the setup by first creating the ragdoll's joint
        hierarchy tree and the bones' rigid bodies, then stopping any animations
        and attaching the constraints according to the actual pose; finally a
        task is installed to update the transforms of the Actor's joint
        hierarchy.


        :param app: the application object
        :type app: class:`direct.showbase.ShowBase.ShowBase`
        :param updateSort: the sort parameter for the update task
        :type updateSort: int
        '''

        if self.jHierarchy:
            return
        # set physics mask
        self.physicsMgr.set_parameter_value(
            GamePhysicsManager.RIGIDBODY, 'group_mask', self.gMask)
        self.physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY,
                                            'collide_mask', self.cMask)
        # 1: create the joint hierarchy's tree
        self._createRagdollJointHierarchyTree()
        # debug
        # print(self.getJointHierarchyArray(self.jHierarchy))
        # 2: create the bones' rigid bodies
        self._createBoneRigidBodies()

        # stop any animation
        self.stop()
        # set actor with no transform wrt parent
        aTrans = self.get_transform(self.render)
        self.aWSTransInv = TransformState.make_scale(
            aTrans.get_scale()).get_inverse()
        # disable culling
        self.node().set_bounds(OmniBoundingVolume())  # XXX check the need
        # If any part of actor is visible, draw all of him. This is necessary
        # because we might be animating some of his joints outside of his
        # original bounding volume.
        self.node().set_final(True)  # XXX check the need

        # arrange each bone to its suitable place
        for p in self.jHierarchy.preorder():
            joint = p.element()['joint']
            name = joint.get_name()
            boneRB = p.element()['boneRB']
            # control the joint
            p.element()['jointCN'] = self.control_joint(
                None, 'modelRoot', name)
            # compute net transform
            joint.get_net_transform(self.jNetTransMat)
            jWorldTransOrig = aTrans.compose(
                TransformState.make_mat(self.jNetTransMat))
            jWorldPos = jWorldTransOrig.get_pos()
            jWorldHPR = jWorldTransOrig.get_hpr()
            boneRB.transform(TransformState.make_pos_hpr(jWorldPos, jWorldHPR))
            # get the parent element
            pP = self.jHierarchy.parent(p)
            if pP:
                # create a constraint between the two two bones
                boneRBPNP = NodePath.any_path(pP.element()['boneRB'])
                params = p.element()['constraint']
                # set the constraint's pivot point
                boneRBNP = NodePath.any_path(boneRB)
                if 'deltaPivot' in params:
                    deltaPivot = LVector3f(params['deltaPivot'])
                    deltaPivot.componentwise_mult(self.get_scale())
                    p1 = boneRBNP.get_pos() + deltaPivot
                else:
                    p1 = boneRBNP.get_pos()
                csRB = self._createConstraint(boneRBNP.get_name() + '_CS',
                                              boneRBNP, boneRBPNP,
                                              params['type'], True, p1)
                csRB.setup()
                self._setConstraintParams(csRB, params)
                p.element()['constraintNode'] = csRB
            # switch the rigid body as dynamic
            boneRB.set_mass(p.element()['bone']['mass'])
            boneRB.switch_body_type(BT3RigidBody.DYNAMIC)

        # setup update task
        app.task_mgr.add(self._update, '_update', sort=updateSort)

    def reset(self, app):
        '''Resets the ragdoll.

        This method will clear all ragdoll related data structures, leaving
        ActorRagDoll in the un-animated final pose.

        :param app: the application object
        :type app: class:`direct.showbase.ShowBase.ShowBase`
        '''

        if not self.jHierarchy:
            return
        # setup update task
        app.task_mgr.remove('_update')
        # by doing a 'postorder' traversal we can, in a safe way, remove
        # constraints, rigid bodies, release joints, remove control nodes
        for p in self.jHierarchy.postorder():
            pP = self.jHierarchy.parent(p)
            if pP:
                # remove the constraint with the parent
                csRB = p.element()['constraintNode']
                self.physicsMgr.destroy_constraint(NodePath.any_path(csRB))
            # remove the rigid body (all constraints attached have been already
            # removed)
            boneRB = p.element()['boneRB']
            self.physicsMgr.destroy_rigid_body(NodePath.any_path(boneRB))
            # release the joint
            self.release_joint('modelRoot', p.element()['joint'].get_name())
            # remove control node
            p.element()['jointCN'].remove_node()

        # remove the jHierarchy tree's nodes
        self.jHierarchy._destroy(self.jHierarchy.root())
        self.jHierarchy = None

    def getJointHierarchyArray(self, jointHierarchyTree=None):
        '''Returns the array based representation of the joint hierarchy tree.

        If jointHierarchyTree is None it returns and prints the array based
        representation of the 'complete' joint hierarchy tree..

        :param jointHierarchyTree: the the joint hierarchy tree
        :type jointHierarchyTree: class: `data_structures_and_algorithms.ch08.linked_tree.LinkedTree`
        :return: the array based representation
        :rtype: list
        '''

        # find the first CharacterJoint's hierarchy, that is the root joint
        jRootBundle = self.get_part_bundle('modelRoot')

        # check the presence of joint hierarchy
        if not jRootBundle:
            raise ActorRagDollExcpt('ERROR: there is no joint hierarchy')

        jArray = []
        if jointHierarchyTree:
            jHierarchy = jointHierarchyTree
            # closing
            for p in jHierarchy.breadthfirst():
                name = p.element()['joint'].get_name()
                children = jHierarchy.num_children(p)
                #
                item = {'joint': name, 'children': children,
                        'bone': p.element()['bone'],
                        'constraint': p.element()['constraint'], }
                if 'jointP' in p.element():
                    item['jointP'] = p.element()['jointP']
                jArray.append(item)
        else:
            # build the complete joint hierarchy's tree
            jHierarchy = LinkedTree()
            jRoot = None
            fringe = LinkedQueue()
            fringe.enqueue(jRootBundle)
            while not fringe.is_empty():
                part = fringe.dequeue()
                if isinstance(part, CharacterJoint):
                    # found root joint
                    jRoot = part
                    break
                for child in part.children:
                    fringe.enqueue(child)
            # build the tree starting at jRoot; use breadthfirst traversal like
            # algorithm
            p = jHierarchy._add_root({'joint': jRoot})
            fringe = LinkedQueue()
            fringe.enqueue((p, jRoot))
            while not fringe.is_empty():
                pP, joint = fringe.dequeue()
                for child in joint.children:
                    if isinstance(child, CharacterJoint):
                        p = jHierarchy._add_child(pP, {'joint': child})
                        fringe.enqueue((p, child))
            # closing
            SPHERE = GamePhysicsManager.SPHERE
            CAPSULE = GamePhysicsManager.CAPSULE
            CONETWIST = BT3Constraint.CONETWIST
            jArrayStr = textwrap.dedent('''
            SPHERE = GamePhysicsManager.SPHERE
            CAPSULE = GamePhysicsManager.CAPSULE
            BOX = GamePhysicsManager.BOX
            Xup = GamePhysicsManager.X_up
            Yup = GamePhysicsManager.Y_up
            CONETWIST = BT3Constraint.CONETWIST
            POINT2POINT = BT3Constraint.POINT2POINT
            DOF6 = BT3Constraint.DOF6
            STOP_ERP = BT3Constraint.CONSTRAINT_STOP_ERP
            STOP_CFM = BT3Constraint.CONSTRAINT_STOP_CFM
            jointHierarchyArray = [
            ''')
            treeLevel = -1
            for p in jHierarchy.breadthfirst():
                name = p.element()['joint'].get_name()
                children = jHierarchy.num_children(p)
                if jHierarchy.is_root(p):
                    bone = {'type': SPHERE, 'mass': 1.0, 'dims': [1.0]}
                    boneStr = '''{'type': SPHERE, 'mass': 1.0, 'dims':[1.0]}'''
                    constraint = None
                    constraintStr = 'None'
                else:
                    bone = {'type': CAPSULE, 'mass': 1.0,
                            'dims': [1.0, 1.0, 1.0]}
                    boneStr = '''{'type': CAPSULE, 'mass': 1.0, 'dims':[1.0, 1.0, 1.0]}'''
                    constraint = {'type': CONETWIST}
                    constraintStr = '''{'type': CONETWIST}'''
                #
                item = {'joint': name, 'children': children,
                        'bone': bone, 'constraint': constraint}
                itemStr = (' {\'joint\':\'' + name + '\', \'children\':' +
                           str(children) + ',\n'
                           '  \'bone\':' + boneStr + ',\n'
                           '  \'constraint\':' + constraintStr + ',\n'
                           ' },\n')
                jArray.append(item)
                if jHierarchy.height(p) > treeLevel:
                    treeLevel += 1
                    jArrayStr += ' # level ' + str(treeLevel) + '\n'
                jArrayStr += itemStr
            jArrayStr += ']'
            print(jArrayStr)
        # return the array
        return jArray

    def _update(self, task):
        '''Updates the ragdoll, i.e. the transforms of the Actor's joint hierarchy.

        Internal use only.

        :param task: the task object
        :type task: class: `panda3d.core.PythonTask`
        :return: the done status
        :rtype:  class: `panda3d.core.AsyncTask.DoneStatus`
        '''

        aTrans = self.get_transform(self.render)
        aRotScaleInv = TransformState.make_pos_hpr_scale(
            LPoint3f.zero(), aTrans.get_hpr(), aTrans.get_scale()).get_inverse()
        aPos = aTrans.get_pos()
        aInvQuat = aRotScaleInv.get_quat()
        for p in self.jHierarchy.preorder():
            jointCN = p.element()['jointCN']
            boneRBNP = NodePath.any_path(p.element()['boneRB'])
            # set jointCN local transform
            jWorldTrans = boneRBNP.get_transform()
            jNetPos = aRotScaleInv.compose(TransformState.make_pos(
                jWorldTrans.get_pos() - aPos)).get_pos()
            jNetRot = aInvQuat * jWorldTrans.get_quat()
            pP = self.jHierarchy.parent(p)
            if not pP:
                # root jointCN local transform
                jLocalTrans = TransformState.make_pos_quat_scale(
                    jNetPos, jNetRot, 1)
            else:
                # jointCN local transform
                if 'jointP' in p.element():
                    p.element()['jointP'].get_net_transform(self.jNetTransMat)
                    jPNetTrans = TransformState.make_mat(self.jNetTransMat)
                else:
                    jPWorldTrans = NodePath.any_path(
                        pP.element()['boneRB']).get_transform()
                    jPNetRot = aInvQuat * jPWorldTrans.get_quat()
                    jPNetPos = aRotScaleInv.compose(TransformState.make_pos(
                        jPWorldTrans.get_pos() - aPos)).get_pos()
                    jPNetTrans = TransformState.make_pos_quat_scale(
                        jPNetPos, jPNetRot, 1)
                jNetTrans = TransformState.make_pos_quat_scale(
                    jNetPos, jNetRot, 1)
                jLocalTrans = jPNetTrans.get_inverse().compose(jNetTrans)
            # apply joint local transform
            jointCN.set_transform(jLocalTrans)
        #
        return task.cont

    def _setConstraintParams(self, cs, csParams):
        '''Sets the constraint's parameters.

        Internal use only.

        :param cs: the constraint
        :type cs: class: `ely.physics.BT3Constraint`
        :param csParams: the table of the constraint parameter
        :type csParams: dict
        '''

        if csParams['type'] == BT3Constraint.CONETWIST:
            # see ConeTwistAttrs
            params = [radians(45.0), radians(
                45.0), radians(45.0), 1.0, 0.3, 1.0]
            if 'swingSpan1' in csParams:
                params[0] = radians(csParams['swingSpan1'])
            if 'swingSpan2' in csParams:
                params[1] = radians(csParams['swingSpan2'])
            if 'twistSpan' in csParams:
                params[2] = radians(csParams['twistSpan'])
            if 'softness' in csParams:
                params[3] = csParams['softness']
            if 'biasFactor' in csParams:
                params[4] = csParams['biasFactor']
            if 'relaxationFactor' in csParams:
                params[5] = csParams['relaxationFactor']
            cs.constraints_attrs.set_limit(*params)
        elif csParams['type'] == BT3Constraint.DOF6:
            # see Dof6Attrs, TypedConstraintAttrs
            if 'limits' in csParams:
                limits = csParams['limits']
            else:
                limits = [(0, 0, 0), (1, 0, 0), (2, 0, 0),
                          (3, 0, 0), (4, 0, 0), (5, 0, 0)]
            for limit in limits:
                cs.constraints_attrs.set_limit(*limit)
            if 'params' in csParams:
                params = csParams['params']
            else:
                STOP_ERP = BT3Constraint.CONSTRAINT_STOP_ERP
                STOP_CFM = BT3Constraint.CONSTRAINT_STOP_CFM
                params = ([(STOP_CFM, 0.0, 0), (STOP_CFM, 0.0, 1),
                           (STOP_CFM, 0.0, 2), (STOP_CFM, 0.0, 3),
                           (STOP_CFM, 0.0, 4), (STOP_CFM, 0.0, 5)] +
                          [(STOP_ERP, 0.9, 0), (STOP_ERP, 0.9, 1),
                           (STOP_ERP, 0.9, 2), (STOP_ERP, 0.9, 3),
                           (STOP_ERP, 0.9, 4), (STOP_ERP, 0.9, 5)])
            for param in params:
                cs.constraints_attrs.set_param(*param)

    def _createConstraint(self, name, rbANP, rbBNP, csType,
                          useWorldReference=False,
                          p1=LPoint3f.zero(), p2=LPoint3f.zero(),
                          a1=LVector3f.zero(), a2=LVector3f.zero(),
                          hpr1=LVecBase3f.zero(), hpr2=LVecBase3f.zero(),
                          anchor=LPoint3f.zero(), ratio=0.0,
                          order=BT3Constraint.RO_XYZ,
                          useReferenceFrameA=False):
        '''Create a constraint between two rigid bodies.

        Internal use only.

        :param name: the constraint name
        :type name: class: `ely.physics.BT3Constraint`
        :param rbANP: the first constraint owner object
        :type rbANP: class: `panda3d.core.NodePath`
        :param rbBNP: the second constraint owner object
        :type rbBNP: class: `panda3d.core.NodePath`
        :param useWorldReference: the world/local references flag 
        :type useWorldReference: bool
        :param p1: the first constraint pivot point's reference
        :type p1: class: `panda3d.core.LPoint3f`
        :param p2: the second constraint pivot point's reference
        :type p2: class: `panda3d.core.LPoint3f`
        :param a1: the first constraint axis reference
        :type a1: class: `ely.physics.LVector3f`
        :param a2: the second constraint axis reference
        :type a2: class: `ely.physics.LVector3f`
        :param hpr1: the first constraint rotation reference
        :type hpr1: class: `ely.physics.LVecBase3f`
        :param hpr2: the second constraint rotation reference
        :type hpr2: class: `ely.physics.LVecBase3f`
        :param anchor: the constraint world anchor
        :type anchor: class: `panda3d.core.LPoint3f`
        :param ratio: the constraint ratio
        :type ratio: float
        :param order: the constraint rotation order
        :type order: class: `ely.physics.BT3Constraint.BT3RotateOrder`
        :param useReferenceFrameA: the constraint 'use reference frame' a flag
        :type useReferenceFrameA: bool
        :return: the created constraint
        :rtype: class: `ely.physics.BT3Constraint`
        '''

        # create the constraint and get a reference to it
        cs = self.physicsMgr.create_constraint(name).node()
        # set the constraint's type
        cs.set_constraint_type(csType)
        # set the objects, ie rigid bodies of the constraint
        objects = [rbANP, rbBNP]
        cs.set_owner_objects(objects)
        # # set the constraint's parameters
        # world/local references
        cs.set_use_world_reference(useWorldReference)
        # pivot point's references
        points = [p1, p2]
        cs.set_pivot_references(points)
        # axes references
        axes = [a1, a2]
        cs.set_axis_references(axes)
        # rotation's references
        hpr = [hpr1, hpr2]
        cs.set_rotation_references(hpr)
        # ratio, rotation order, world anchor, world axes, use reference frame
        # a
        cs.set_ratio(ratio)
        cs.set_rotation_order(order)
        cs.set_world_anchor(anchor)
        cs.set_world_axes(axes)
        cs.set_use_reference_frame_a(useReferenceFrameA)
        #
        return cs

    def _createRagdollJointHierarchyTree(self):
        '''Creates the ragdoll joint hierarchy's tree.

        Internal use only.
        '''

        # check the presence of joint hierarchy
        if not self.jHierarchyArray:
            raise ActorRagDollExcpt('ERROR: there is no joint hierarchy')

        # create a (empty) joint hierarchy tree
        self.jHierarchy = LinkedTree()
        # FIFO queue of positions
        pQueue = LinkedQueue()
        # node insertion use breadthfirst traversal like algorithm
        for node in self.jHierarchyArray:
            joint = self.get_joints(jointName=node['joint'])[0]
            element = {'joint': joint,
                       'bone': node['bone'],
                       'constraint': node['constraint'],
                       }
            if 'jointP' in node:
                element['jointP'] = self.get_joints(
                    jointName=node['jointP'])[0]
            children = node['children']
            if self.jHierarchyArray.index(node) == 0:
                # add base joint
                p = self.jHierarchy._add_root(element)
            else:
                # add remaining nodes
                p = pQueue.dequeue()
                p = self.jHierarchy._add_child(p, element)
            #
            while children > 0:
                pQueue.enqueue(p)
                children -= 1

    def _createJointHierarchyTreeComplete(self):
        '''Creates the complete joint hierarchy's tree.

        Internal use only.
        .. seealso::
            http://www.panda3d.org/forums/viewtopic.php?t=11792

        :return: the complete joint hierarchy's tree
        :rtype: class: `data_structures_and_algorithms.ch08.linked_tree.LinkedTree`
        '''

        # build the joint hierarchy's tree
        jHierarchyTree = LinkedTree()

        # find the first CharacterJoint's hierarchy, that is the root joint
        jRootBundle = self.get_part_bundle('modelRoot')
        # check the presence of joint hierarchy
        if not jRootBundle:
            raise ActorRagDollExcpt('ERROR: there is no joint hierarchy')

        jRoot = None
        fringe = LinkedQueue()
        fringe.enqueue(jRootBundle)
        while not fringe.is_empty():
            part = fringe.dequeue()
            if isinstance(part, CharacterJoint):
                # found root joint
                jRoot = part
                break
            for child in part.children:
                fringe.enqueue(child)
        # build the tree starting at jRoot; use breadthfirst traversal like
        # algorithm
        element = {'joint': jRoot,
                   'bone': {},
                   'constraint': {},
                   }
        p = jHierarchyTree._add_root(element)
        fringe = LinkedQueue()
        fringe.enqueue((p, jRoot))
        while not fringe.is_empty():
            pP, joint = fringe.dequeue()
            for child in joint.children:
                if isinstance(child, CharacterJoint):
                    element = {'joint': child,
                               'bone': {},
                               'constraint': {},
                               }
                    p = jHierarchyTree._add_child(pP, element)
                    fringe.enqueue((p, child))
        # return the tree
        return jHierarchyTree

    def _createBoneRigidBodies(self):
        '''Creates the rigid bodies for the bones of the joint hierarchy's tree.

        Internal use only.
        '''

        # # initially expose the joints
        for p in self.jHierarchy.breadthfirst():
            name = p.element()['joint'].get_name()
            p.element()['npExp'] = self.expose_joint(None, 'modelRoot', name)

        # # create a rigid body for each bone
        for p in self.jHierarchy.postorder():
            name = p.element()['joint'].get_name()
            pExp = p.element()['npExp']
            shapeType = p.element()['bone']['type']
            if 'upAxis' in p.element()['bone']:
                upAxis = p.element()['bone']['upAxis']
            else:
                upAxis = GamePhysicsManager.Z_up
            dims = p.element()['bone']['dims']
            # create a placeholder NodePath
            placeholderNP = self._createPlaceholderNP(
                pExp.get_name(), LVecBase3f(*dims), model=None)
            boneRB = GamePhysicsManager.get_global_ptr(
            ).create_rigid_body(name + '_boneRB').node()
            boneRB.set_shape_type(shapeType)
            boneRB.set_up_axis(upAxis)
            boneRB.set_owner_object(placeholderNP)
            boneRB.setup()
            # set as kinematic
            boneRB.set_mass(0.0)
            boneRB.switch_body_type(BT3RigidBody.KINEMATIC)
            # remove the placeholder
            placeholderNP.remove_node()
            # save the rigid body NodePath
            p.element()['boneRB'] = boneRB
            # stop joint exposition and remove the exposed NodePath
            self.stop_joint('modelRoot', name)
            p.element()['npExp'].remove_node()
            # remove the no more needed keys
            p.element().pop('npExp', None)

    def _createPlaceholderNP(self, name, dims, model=None):
        '''Creates a (width * depth * height) box shaped GeomNode.

        If model is not None then an instance of it is created, and (width,
        depth, height) are interpreted as scale factors.
        Internal use only.

        :param name: the name of the GeomNode
        :type name: str
        :param dims: the (width, depth, height) box dimensions
        :type dims: tuple
        :param model: the model to instance
        :type model: class: `panda3d.core.NodePath`
        :return: the created box shaped GeomNode
        :rtype: class: `panda3d.core.GeomNode`
        '''

        width, depth, height = dims.get_x(), dims.get_y(), dims.get_z()
        placeholderNP = None
        if isinstance(model, NodePath):
            # create an instance of model
            placeholderNP = NodePath(model.get_name() + 'I')
            model.instance_to(placeholderNP)
            placeholderNP.set_scale(width, depth, height)
        else:
            # create a box: width x depth x height
            # Vertex Format
            arrayFormat = GeomVertexArrayFormat()
            arrayFormat.add_column(InternalName.make(
                'vertex'), 3, Geom.NT_float32, Geom.C_point)
            vertexFormat = GeomVertexFormat()
            vertexFormat.add_array(arrayFormat)
            vertexFormatAdded = GeomVertexFormat.register_format(vertexFormat)
            # Vertex Data
            vertexData = GeomVertexData(
                'plane', vertexFormatAdded, Geom.UH_static)
            vertexW = GeomVertexWriter(vertexData, 'vertex')
            # compute coords according to z-up axis
            vertices = [None] * 8
            w2 = width / 2.0
            d2 = depth / 2.0
            h2 = height / 2.0
            for h in range(2):
                signH = -1 if (h % 2) == 0 else 1
                for d in range(2):
                    signD = -1 if (d % 2) == 0 else 1
                    for w in range(2):
                        signW = -1 if (w % 2) == 0 else 1
                        vertices[w + 2 * d + 4 *
                                 h] = LPoint3f(signW * w2, signD * d2,
                                               signH * h2)
            # fill-up vertex data (box)
            for vertex in vertices:
                vertexW.add_data3f(vertex)
            # Creating the GeomPrimitive objects for box
            boxTriangles = GeomTriangles(Geom.UH_static)
            # bottom triangles
            boxTriangles.add_vertices(0, 3, 1)
            boxTriangles.add_vertices(0, 2, 3)
            # left triangles
            boxTriangles.add_vertices(0, 4, 6)
            boxTriangles.add_vertices(0, 6, 2)
            # rear triangles
            boxTriangles.add_vertices(2, 6, 7)
            boxTriangles.add_vertices(2, 7, 3)
            # up triangles
            boxTriangles.add_vertices(4, 5, 7)
            boxTriangles.add_vertices(4, 7, 6)
            # right triangles
            boxTriangles.add_vertices(1, 3, 7)
            boxTriangles.add_vertices(1, 7, 5)
            # front triangles
            boxTriangles.add_vertices(0, 1, 5)
            boxTriangles.add_vertices(0, 5, 4)
            # Putting your new geometry in the scene graph
            boxGeom = Geom(vertexData)
            boxGeom.add_primitive(boxTriangles)
            boxNode = GeomNode(name + 'PH')
            boxNode.add_geom(boxGeom)
            placeholderNP = NodePath(boxNode)
        #
        return placeholderNP
