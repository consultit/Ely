'''
Created on Sep 22, 2017

Class managing smooth transitions between animations.

@author: consultit
'''

from abc import ABCMeta, abstractmethod
from collections.abc import Iterable, Sequence
from direct.interval.LerpInterval import LerpFunctionInterval
from numbers import Number
from math import ceil
import sys
import os


class AnimTransitionBase(metaclass=ABCMeta):
    '''AnimTransition manages smooth timed based transition between two animations.

    Before transition only the 'from' animation is played and after the
    transition only one the 'to' animation is played.
    There a two kinds of transition: 'smooth' (the default) and 'frozen. 
    During a smooth transition both are 
    played: the weight of the 'from' animation linearly decreases from 1.0 to
    0.0 to the end of transition when the animation is stopped, while the 'to'
    animation is started at the beginning of transition with the weight linearly
    increasing from 0.0 to 1.0 at the end.
    At the end of transition a doneEvent will be thrown. This event must be
    unique for each of the AnimTransition objects, otherwise the corresponding
    transitions interfere with each other. By default, each AnimTransition
    object sets a (reasonably) unique event.

    :param app: the application object
    :type app: class:`direct.showbase.ShowBase.ShowBase`
    :param animCtrlFrom: the transition's start animation control
    :type animCtrlFrom: object implementing interface of class:`panda3d.core.AnimControl`
    :param animCtrlTo: the transition's target animation control
    :type animCtrlTo: object implementing interface of class:`panda3d.core.AnimControl`

    .. seealso:: '12 Animations Systems.' Game Engine Architecture, by Jason Gregory,
            3rd ed., CRC Press, Taylor &amp; Francis Group, 2019, pp. 721–816.
    '''

    def __init__(self, app, animCtrlFrom=None, animCtrlTo=None):
        '''Constructor
        '''

        self.app = app
        self._inTransition = False
        # disable print
        sys.stdout = open(os.devnull, 'w')
        self.animCtrlFrom = animCtrlFrom
        self.animCtrlTo = animCtrlTo
        # re-enable print
        sys.stdout = sys.__stdout__
        self.frozen = False
        self.animToFrame = 0
        self.blendType = None
        self.frameBlend = None
        self.playRate = 1.0
        self.playRateRatios = ()
        self.usePose = False
        self.loop = True
        self._setup()
        # after setup configuration
        self.duration = 1.0
        # set a unique done event for this transition
        AnimTransitionBase.count += 1
        self._count = AnimTransitionBase.count
        self.doneEvent = ('_AnimTransition_' + str(self._count) + '_Done')

    def startTransition(self):
        '''This method will start the transition from animCtrlFrom to animCtrlTo.
        '''

        if (not hasattr(self, '_animCtrlFrom')) or (not hasattr(self, '_animCtrlTo')):
            print('Make sure that animCtrlFrom and animCtrlTo attributes have '
                  'been defined.')
            return
        self._set_blend(True, self._frameBlend, self._blendType)
        self._animCtrlTo.get_part().set_control_effect(self._animCtrlTo, 0.0)
        animFromFrame = self._animCtrlFrom.get_frame()
        self._animCtrlTo.pose(self._animToFrameMap[animFromFrame])
        if self._playRateRatios and (not self._frozen):
            self._animCtrlTo.set_play_rate(self._PRToStart)
            if not self._usePose:
                self.ACTION(self._animCtrlTo, **self.EXTRAARGS)
        else:
            if self._frozen:
                animFromFrameF = self._animCtrlFrom.get_full_fframe()
                self._animCtrlFrom.pose(animFromFrameF)
                self._animCtrlTo.pose(
                    self._animToFrameMap[int(ceil(animFromFrameF))])
            self._animCtrlTo.set_play_rate(self._playRate)
            self.ACTION(self._animCtrlTo, **self.EXTRAARGS)
        self._inTransition = True
        self._start()

    def stopTransition(self):
        '''This method will stop the transition from animCtrlFrom to animCtrlTo in advance.

        This will stop the transition and move its state to its final state, as
        if it had played to the end, thus will emit the 'done' event.
        '''

        if (not hasattr(self, '_animCtrlFrom')) or (not hasattr(self, '_animCtrlTo')):
            print('Make sure that animCtrlFrom and animCtrlTo attributes have'
                  ' been defined.')
            return

        self._stop()

    def pauseTransition(self):
        '''This method will pause the transition.
        '''

        if (not hasattr(self, '_animCtrlFrom')) or (not hasattr(self, '_animCtrlTo')):
            print('Make sure that animCtrlFrom and animCtrlTo attributes have '
                  'been defined.')
            return

        self._pause()

    def resumeTransition(self):
        '''This method will resume the transition.
        '''
        if (not hasattr(self, '_animCtrlFrom')) or (not hasattr(self, '_animCtrlTo')):
            print('Make sure that animCtrlFrom and animCtrlTo attributes have '
                  'been defined.')
            return

        self._resume()

    def transitionDone(self):
        '''Called at transition termination.

        A user defined callback responding to the 'done' event should call this
        method too.
        '''

        self._inTransition = False
        self._animCtrlFrom.stop()
        self._set_blend(False, self._frameBlend, self._blendType)
        if self._playRateRatios and self.usePose:
            self.ACTION(self._animCtrlTo, **self.EXTRAARGS)

    @property
    def animCtrlFrom(self):
        '''The control of the animation from which transition begins.

        :return: the transition's start animation control
        :rtype: object implementing interface of class:`panda3d.core.AnimControl` or None
        '''
        return self._animCtrlFrom

    @animCtrlFrom.setter
    def animCtrlFrom(self, animCtrlFrom):
        '''Setter of the control of the animation from which transition begins.

        :param animCtrlFrom: the transition's start animation control
        :type animCtrlFrom: object implementing interface of class:`panda3d.core.AnimControl` or None
        '''
        if self._inTransition:
            return
        self._animCtrlFrom = animCtrlFrom
        if self._animCtrlFrom and hasattr(self, '_loop'):
            if self._loop:
                self.ACTION = self._animCtrlFrom.__class__.loop
                self.EXTRAARGS = {'restart': False}
            else:
                self.ACTION = self._animCtrlFrom.__class__.play
                self.EXTRAARGS = {}

    @property
    def animCtrlTo(self):
        '''The control of the animation to which transition ends.

        :return: the transition's target animation control
        :rtype: object implementing interface of class:`panda3d.core.AnimControl` or None
        '''
        return self._animCtrlTo

    @animCtrlTo.setter
    def animCtrlTo(self, animCtrlTo):
        '''Setter of the control of the animation to which transition ends.

        :param animCtrlTo: the transition's target animation control
        :type animCtrlTo: object implementing interface of class:`panda3d.core.AnimControl` or None
        '''
        if self._inTransition:
            return
        self._animCtrlTo = animCtrlTo
        if self._animCtrlTo and hasattr(self, '_loop'):
            if self._loop:
                self.ACTION = self._animCtrlTo.__class__.loop
                self.EXTRAARGS = {'restart': False}
            else:
                self.ACTION = self._animCtrlTo.__class__.play
                self.EXTRAARGS = {}

    @property
    def animToFrame(self):
        '''Function object which maps which maps FromFrames -> ToFrames.

        This is a function object which returns the frame number of the 'to' 
        animation corresponding to the current frame number of the 'from' 
        animation to ease the smoothing of the transition. 
        When animToFrame is assigned an inner mapping: FromFrames -> ToFrames is
        built (Default: 0).
        This attribute can be used alternatively to animToFrameMap.

        .. seealso:: discussions for playRateRatios and usePose.

        :return: function object which maps 'animation from' frames with 'animation to' ones
        :rtype: callable
        '''
        return self._animToFrame

    @animToFrame.setter
    def animToFrame(self, animToFrame):
        '''Setter of a function object which maps which maps FromFrames -> ToFrames.

        :param animToFrame: function object which maps 'animation from' frames with 'animation to' ones
        :type animToFrame: callable
        '''
        if self._inTransition:
            return
        if hasattr(self, '_animCtrlFrom') and self._animCtrlFrom:
            if callable(animToFrame):
                self._animToFrame = animToFrame
            else:
                self._animToFrame = lambda frame: frame * 0
            # create temporary map
            self._animToFrameMap = []
            animFromFrameNum = self._animCtrlFrom.get_num_frames()
            for frame in range(animFromFrameNum):
                self._animToFrameMap.append(self._animToFrame(frame))
            # append first frame as last one
            self._animToFrameMap.append(self._animToFrame(0))
        else:
            self._animToFrame = lambda frame: frame * 0

    @property
    def frozen(self):
        '''Flag indicating whether this animation is 'frozen'. 

        :return: the flag
        :rtype: bool
        '''
        return self._frozen

    @frozen.setter
    def frozen(self, frozen):
        '''Setter of the flag indicating whether this animation is 'frozen'.

        :param frozen: the flag
        :type frozen: bool
        '''
        if self._inTransition:
            return
        self._frozen = frozen

    @property
    def animToFrameMap(self):
        '''The inner mapping: FromFrames -> ToFrames.

        Sequence whose element at index 'fromFrame' corresponds to 'toFrame'. 
        This attribute can be used alternatively to animToFrame.

        .. seealso::  discussions for playRateRatios and usePose.

        :return: the sequence assigned to the inner mapping: FromFrames -> ToFrames
        :rtype: sequence
        '''
        if hasattr(self, '_animToFrameMap'):
            return self._animToFrameMap

    @animToFrameMap.setter
    def animToFrameMap(self, toFrameMap):
        '''Setter of the inner mapping: FromFrames -> ToFrames.

        :param toFrameMap: the sequence assigned to the inner mapping: FromFrames -> ToFrames
        :type toFrameMap: sequence
        '''
        if self._inTransition:
            return
        if hasattr(self, '_animCtrlFrom'):
            if isinstance(toFrameMap, Sequence):
                self._animToFrameMap = toFrameMap

    @property
    def blendType(self):
        '''The blend algorithm type (defaults to None).

        .. seealso:: `Actor.set_blend()`

        :return: the blend algorithm type
        :rtype: int or None
        '''
        return self._blendType

    @blendType.setter
    def blendType(self, blendType):
        '''Setter of the blend algorithm type.

        :param blendType: the blend algorithm type
        :type blendType: int or None
        '''
        if self._inTransition:
            return
        if isinstance(blendType, Number):
            self._blendType = int(blendType)
        else:
            self._blendType = None

    @property
    def frameBlend(self):
        '''The smooth interpolation flag (defaults to None).

        .. seealso:: `Actor.set_blend()`

        :return: the smooth interpolation flag
        :rtype: bool or None
        '''
        return self._frameBlend

    @frameBlend.setter
    def frameBlend(self, frameBlend):
        '''Setter of the smooth interpolation flag.

        :param frameBlend: the smooth interpolation flag
        :type frameBlend: bool or None
        '''
        if self._inTransition:
            return
        if isinstance(frameBlend, bool):
            self._frameBlend = frameBlend
        else:
            self._frameBlend = None

    @property
    def playRate(self):
        '''The common play rate of the animations (defaults to 1.0).

        :return: the common play rate
        :rtype: float
       '''
        return self._playRate

    @playRate.setter
    def playRate(self, playRate):
        '''Setter of the common play rate of the animations.

        :param playRate: the common play rate
        :type playRate: float
        '''
        if self._inTransition:
            return
        if isinstance(playRate, Number):
            self._playRate = float(playRate)
        else:
            self._playRate = 1.0

    @property
    def playRateRatios(self):
        '''A 2 element iterable with the form: (PFromEndRatio, PToStartRatio)

        During the transition the 'from' animation' play rate will be lerped
        from playRate to playRate * PFromEndRatio, while that of the 'to'
        animation will be lerped from playRate * PToStartRatio to playRate.
        If playRateRatios is set to None no lerping takes place and both
        animations will play at the given playRate. This parameter is
        specially useful for animation cycles, where the two animations have 
        similar overlapping movements but with different rates of the cycles:
        for example walk and run move feet (and hands) similarly during a cycle
        but at a different rate. In this case to ease transition's smoothing,
        the cycles should be completed at the same time so, for example, to
        transit from walk to run, walk should increase its play rate during
        transition until a rate that allow the cycle completion as the run play
        rate, and the run should start with a decreased play rate that allow the
        cycle completion as the walk play rate; so given walkCycleFrames as the 
        number of frames of a walk cycle and runCycleFrames as the number of
        frames of a run cycle, the playRateRatios would be equal to: 
           (walkCycleFrames/runCycleFrames, runCycleFrames/walkCycleFrames).
        (defaults to None).
        This parameter is not used with 'frozen' transitions.

        :return: the 2-iterable(pair) object
        :rtype: 2-iterable(pair) or None
        '''
        if self._playRateRatios:
            return (self._PRFromEnd / self._playRate,
                    self._PRToStart / self._playRate)
        return self._playRateRatios

    @playRateRatios.setter
    def playRateRatios(self, playRateRatios):
        '''Setter of a 2 element iterable with the form: (PFromEndRatio, PToStartRatio)

        :param playRateRatios: the 2-iterable(pair) object
        :type playRateRatios: 2-iterable(pair) or None
        '''
        if self._inTransition:
            return
        try:
            iter(playRateRatios)
            if (len(playRateRatios) >= 2) and \
                    isinstance(playRateRatios[0], Number) and \
                    isinstance(playRateRatios[1], Number):
                self._playRateRatios = True
                self._PRFromEnd = self._playRate * playRateRatios[0]
                self._PRToStart = self._playRate * playRateRatios[1]
                self._DeltaPRFrom = self._PRFromEnd - self._playRate
                self._DeltaPRTo = self._playRate - self._PRToStart
            else:
                self._playRateRatios = None
        except Exception as e:
            print(e)
            self._playRateRatios = None

    @property
    def usePose(self):
        '''Flag indicating direct setting of the frames of the 'to' animation. 

        True if the frame of the 'to' animation is set at each iteration through
        animToFrame function, False otherwise. This parameter has effect only
        when playRateRatios != None (defaults to False).
        This parameter is not used with 'frozen' transitions.

        :return: the flag
        :rtype: bool
        '''
        return self._usePose

    @usePose.setter
    def usePose(self, usePose):
        '''Setter of the flag indicating direct setting of frames of 'to' animation.

        :param usePose: the flag
        :type usePose: bool
        '''
        if self._inTransition:
            return
        self._usePose = usePose

    @property
    def loop(self):
        '''Flag indicating whether animations should loop.

        True if the 'to' animation is looped (Default), False if it is played
        once (defaults to True).

        :return: the flag
        :rtype: bool
        '''
        return self._loop

    @loop.setter
    def loop(self, loop):
        '''Setter of the flag indicating whether animations should loop.

        :param loop: the flag
        :type loop: bool
        '''
        if self._inTransition:
            return
        self._loop = loop
        # update action
        self.animCtrlFrom = self._animCtrlFrom
        self.animCtrlTo = self._animCtrlTo

    @property
    def duration(self):
        '''The transition duration in seconds (defaults to 1.0 seconds).

        :return: the transition duration
        :rtype: float
        '''
        return self._duration

    @duration.setter
    def duration(self, duration):
        '''Setter of the transition duration in seconds. 

        :param duration: the transition duration
        :type duration: float
        '''
        if self._inTransition:
            return
        if isinstance(duration, Number):
            self._duration = duration
            self._setDuration()

    @property
    def doneEvent(self):
        '''The event thrown at the end of the transition (defaults to '_AnimTransition_N_Done', with N=unique integer number).

        :return: the end of transition event
        :rtype: str
        '''
        return self._doneEvent

    @doneEvent.setter
    def doneEvent(self, doneEvent):
        '''Setter of the event thrown at the end of the transition.

        :param doneEvent: the end of transition event
        :type doneEvent: str
        '''
        if self._inTransition:
            return
        if (hasattr(self, '_doneEvent') and
                self.app.is_accepting(self._doneEvent)):
            self.app.ignore(self._doneEvent)
        self._doneEvent = str(doneEvent)
        self.app.accept(self._doneEvent, self.transitionDone)
        self._setDoneEvent()

    # unique number
    count = 0

    @abstractmethod
    def _setup(self):
        '''Custom setup. To be overridden by derived classes.
        '''

    @abstractmethod
    def _start(self):
        '''Custom start. To be overridden by derived classes.
        '''

    @abstractmethod
    def _stop(self):
        '''Custom stop. To be overridden by derived classes.
        '''

    @abstractmethod
    def _pause(self):
        '''Custom pause. To be overridden by derived classes.
        '''

    @abstractmethod
    def _resume(self):
        '''Custom resume. To be overridden by derived classes.
        '''

    @abstractmethod
    def _setDuration(self):
        '''Custom set duration. To be overridden by derived classes.
        '''

    @abstractmethod
    def _setDoneEvent(self):
        '''Custom set done event. To be overridden by derived classes
        '''

    def _set_blend(self, animBlend, frameBlend, blendType):
        '''Helper
        '''
        if hasattr(self, '_animCtrlFrom') and hasattr(self, '_animCtrlTo'):
            for bundle in (self._animCtrlFrom.get_part(), self._animCtrlTo.get_part()):
                if blendType is not None:
                    bundle.set_blend_type(blendType)
                if animBlend is not None:
                    bundle.set_anim_blend_flag(animBlend)
                if frameBlend is not None:
                    bundle.set_frame_blend_flag(frameBlend)

    def _transitionStep(self, lerpValue):
        '''Compute a 'lerped' step of the transition.
        '''

        self._animCtrlFrom.get_part().set_control_effect(
            self._animCtrlFrom, 1.0 - lerpValue)
        self._animCtrlTo.get_part().set_control_effect(self._animCtrlTo, lerpValue)
        if self._playRateRatios and (not self._frozen):
            # PR = PRStart + lerpValue * (PREnd - PRStart)
            # playRateFrom =
            #   self._playRate + lerpValue * (self._PRFromEnd - self._playRate)
            # playRateTo =
            #   self._PRToStart + lerpValue * (self._playRate - self._PRToStart)
            self._animCtrlFrom.set_play_rate(
                self._playRate + lerpValue * self._DeltaPRFrom)
            self._animCtrlTo.set_play_rate(
                self._PRToStart + lerpValue * self._DeltaPRTo)
            if self._usePose:
                animFromFrame = int(ceil(self._animCtrlFrom.get_full_fframe()))
                self._animCtrlTo.pose(self._animToFrameMap[animFromFrame])


class AnimTransition(AnimTransitionBase):
    '''AnimTransition manages smooth timed based transition between two animations.

    This kind of transition is based on an underlying LerpFunctionInterval.
    '''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.doneEvent = self._interval.getName() + '_Done'

    def _setup(self):
        self._interval = LerpFunctionInterval(self._transitionStep,
                                              duration=0.0,
                                              fromData=0.0,
                                              toData=1.0,
                                              blendType='noBlend',  # 'easeInOut',
                                              extraArgs=[],
                                              name='_AnimTransitionInterval_%d')

    def _start(self):
        self._interval.start()

    def _stop(self):
        self._interval.finish()

    def _pause(self):
        self._interval.pause()
        if self._playRateRatios and self.usePose:
            self.ACTION(self._animCtrlTo, **self.EXTRAARGS)

    def _resume(self):
        self._interval.resume()
        if not (self._playRateRatios and self.usePose):
            self.ACTION(self._animCtrlTo, **self.EXTRAARGS)

    def _setDuration(self):
        self._interval.duration = self._duration

    def _setDoneEvent(self):
        self._interval.setDoneEvent(self._doneEvent)


class AnimTransitionParam(AnimTransitionBase):
    '''AnimTransition manages smooth timed based transition between two animations.

    This kind of transition is driven by a underlying task: when this transition
    starts it reads repeatedly an external parameter with which it executes an
    'lerped' step of the transition. The transition stops as soon as the
    parameter value falls outside the interval [0.0, 1.0] or the duration time
    has elapsed.
    '''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lerpValue = lambda: 1.0
        self._lerpValueArgs = []

    @property
    def lerpValue(self):
        '''The object returning the external parameter.

        This object (callable or iterable) returns the value of the external
        parameter, presumably of float type, used to perform the transition's
        'lerped' step (defaults to a function that returns 1.0).

        :return: the object returning the external parameter
        :rtype: lerpValueArg could be a callable or a (callable, extraArgs=[]) iterable.
        '''
        return self._lerpValue

    @lerpValue.setter
    def lerpValue(self, lerpValueArg):
        '''Setter of the object returning the external parameter.   

        :param lerpValueArg: the object returning the external parameter
        :type lerpValueArg: lerpValueArg could be a callable or a (callable, extraArgs=[]) iterable.
        '''

        if self._inTransition:
            return
        if hasattr(self, '_animCtrlFrom') and hasattr(self, '_animCtrlTo'):
            if isinstance(lerpValueArg, Iterable) and (len(lerpValueArg) >= 2):
                lerpValue = lerpValueArg[0]
                extraArgs = lerpValueArg[1]
            else:
                lerpValue = lerpValueArg
                extraArgs = []
            try:
                if (callable(lerpValue) and isinstance(extraArgs, Iterable)):
                    self._lerpValue = lerpValue
                    self._lerpValueArgs = extraArgs
                else:
                    raise Exception('ERROR: \'' + str(lerpValueArg) +
                                    '\' should be a callable or a '
                                    '(callable, extraArgs=[]) iterable.')
            except Exception as e:
                print(e)
                self._lerpValue = lambda: 1.0
                self._lerpValueArgs = []

    def _setup(self):
        '''Not implemented.
        '''

    def _start(self):
        if not hasattr(self, '_task'):
            self._task = self.app.add_task(
                self._animTransTask, '_lerpTask' + str(self._count), sort=10)

    def _stop(self):
        if hasattr(self, '_task'):
            self.app.remove_task(self._task)
            delattr(self, '_task')

    def _pause(self):
        '''Not implemented.
        '''

    def _resume(self):
        '''Not implemented.
        '''

    def _setDuration(self):
        '''Not implemented.
        '''

    def _setDoneEvent(self):
        '''Not implemented.
        '''

    def _animTransTask(self, task):
        '''Animation's transition task.
        '''

        lerpValue = self._lerpValue(*self._lerpValueArgs)
        if ((lerpValue < 0.0) or (lerpValue >= 1.0) or
                (task.time > self.duration)):
            self._stop()
            self.app.messenger.send(self._doneEvent)
        else:
            self._transitionStep(lerpValue)
        #
        return task.cont
