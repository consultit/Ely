'''
Created on Nov 2, 2017

Class implementing the IK Cyclic-Coordinate Descent algorithm.

@author: consultit
'''

from math import pow, radians, pi as PI, degrees, atan2, modf
from numbers import Number
import numpy as np
from panda3d.core import CardMaker, NodePath, LVecBase4f, LVecBase3f, \
    LPoint3f, LRotationf, LVector3f, LineSegs, TransparencyAttrib, LMatrix4f, \
    TransformState, GeomNode
from .data_structures_and_algorithms.ch08.linked_tree import LinkedTree
from .data_structures_and_algorithms.ch07.linked_queue import LinkedQueue

# Some constant values
PIHALFINV = 0.636619772  # 2.0 / PI


class CCDik(object):
    '''CCDik implements the IK Cyclic-Coordinate Descent algorithm.

    This can be used for a (sub)hierarchy of inextensible joints of a skeleton.
    Features currently supported:
        - 'ball-and-socket' rotation joint
        - joint rotation limits (2 Dofs) specified as angles in [0,180] degrees
          interval
        - incorporating constraints as multiple end effectors
    Features currently not supported:
        - other kind of rotation joint types (but some of them could be
          simulated with 'ball-and-socket type', e.g. hinge)
        - translation joint type
        - orientation limits (1 Dof)

    The constructor builds a tree type attribute called 'jHierarchyTree', in
    which each node refers to one joint of the (sub-)hierarchy and contains a
    dictionary of the parameters of the joint

    :param actor: the actor whose joint (sub)hierarchies are subject to the
    application of the CCD algorithm.
    :type actor: class: `direct.actor.Actor.Actor`
    :param jointHierarchyArray: the joint hierarchy is considered an 'ordered'
    tree, that is there is an arbitrary meaningful linear order among the
    children of each node and this order must be consistent across all the data
    structures belonging or related to an CCDik object. This parameter must be
    specified with an 'array based representation', that is the nodes of each
    level are stored consecutively, maintaining the aforementioned ordering of
    the children, starting from level 0. Note: after the tree construction, you
    can obtain an array (of nodes) ordered the same way by using the
    'breadthfirst' traversal. Therefore each node is represented by a pair,
    whose first element represents the parameters of the node and the second the
    number of its children. The first joint is the 'base joint'. The parameters
    of each joint are specified as a dictionary with the following keys:
        - 'name': the name of the joint.
        - 'upAxis': for each joint a right-handed orthonormal local frame is
            defined and, by default, this is identified by a forward axis having
            the reverse direction of the bone connecting the joint to its
            parent, and by an up axis right axis both lying on the normal plane
            to the forward axis. The value of this key should be an vector
            (LVector3f), specified in global coordinates and not necessarily
            normalized, whose projection on the aforementioned plane defines the
            up axis. Mandatory parameter.
        - 'baseForwardAxis': this key can be specified only for the base joint
            and its value is a vector (LVector3f) which would overwrite the base
            joint default forward axis which has the reverse direction of the
            bone connecting the base joint to its parent joint. It should be
            specified as the up axis. Optional parameter.
        - 'endEffector': if True this parameter states taht this joint is an end
            effector. Note: leaf joints are always end effectors. Optional
            parameter.
        - 'angles': these are the rotational angle limits relative the parent
            joint's local frame, that the bone connecting this joint with its
            parent undergoes; the value of this key should a 4-tuple specifying
            the 4 angles of the rotational limit; each angle should be between
            in [0, 180] degrees; angles[0] and angles[2] are relative to
            positive and negative up axis respectively, while angles[1] and
            angles[3] are relative to positive and negative right axis
            respectively. All angles are measured with respect to forward axis.
            Specifying all of the angles=180 is equivalent to specify None for
            the joint's limits.
            Note: any joint angle limit incorrectly specified which cannot be
            converted to allowed values is set to None, ie no limit. Optional
            parameter (but the base joint).
        - 'w':  the value of this key (float) is the stiffness of the joint. 
            Mandatory parameter for all joints but base joint.
    :type jointHierarchyArray: list
    :param render: the parent of the hierarchy of the control joints. This is
    the 'global' coordinate system w.r.t. all computations are performed.
    :type render: class: `NodePath`, optional, defaults to actor's parent node
    :param alpha:this is the scaling factor to make the algorithm's behavior
    scale-invariant; it should be inversely proportional to the overall global
    scale W (that is alpha = k/W).
    :type alpha: float, optional, defaults to 1.0

    .. seealso::
        - 'INVERSE KINEMATICS AND GEOMETRIC CONSTRAINTS FOR ARTICULATED FIGURE
            MANIPULATION' - Chris Welman - Masters Thesis - SIMON FRASER
            UNIVERSITY (1993)
        - '3D Game Engine Design' - David H. Eberly - 2nd Edition, pages 339-346
            - The Morgan Kaufmann Series in Interactive 3D Technology (2007)
    '''

    def __init__(self, actor, jointHierarchyArray, render=None, alpha=1.0):
        '''Constructor method
        '''

        # all computations are performed in global (self.render) coordinates
        # (unless otherwise specified)
        self.render = render
        if not self.render:
            self.render = actor.get_parent()
        self.actor = actor
        # actor needs to be parented to render
        self.actor.reparent_to(self.render)
        self.alpha = abs(alpha)
        # node insertion use breadthfirst traversal like algorithm
        # create a (empty) joint hierarchy tree
        self.jHierarchyTree = LinkedTree()
        # FIFO queue of positions
        pQueue = LinkedQueue()
        for node in jointHierarchyArray:
            childNum = node['children']
            element = dict(node)
            element.pop('children', None)
            if jointHierarchyArray.index(node) == 0:
                # add base joint
                p = self.jHierarchyTree._add_root(element)
            else:
                # add remaining nodes
                p = pQueue.dequeue()
                p = self.jHierarchyTree._add_child(p, element)
            #
            while childNum > 0:
                pQueue.enqueue(p)
                childNum -= 1
        # correct incorrectly specified parameters
        for p in self.jHierarchyTree.breadthfirst():
            jParams = p.element()
            # correct upAxis (mandatory)
            try:
                LVector3f(jParams['upAxis'])
            except Exception as e:
                print(e)
                jParams['upAxis'] = LVector3f(0, -1, 0)
            # correct w (0 <= w <= 1.0) (stiffness)(mandatory but base joint)
            if not self.jHierarchyTree.is_root(p):
                if isinstance(jParams['w'], Number):
                    w = abs(jParams['w'])
                    if w > 1.0:
                        jParams['w'] = modf(w)[0]
                else:
                    jParams['w'] = 1.0
            # correct baseForwardAxis
            if self.jHierarchyTree.is_root(p) and ('baseForwardAxis' in jParams):
                try:
                    LVector3f(jParams['baseForwardAxis'])
                except Exception as e:
                    print(e)
                    jParams['baseForwardAxis'] = LVector3f(0, 0, -1)
            # correct angles (optional but base joint)
            angles = []
            if not self.jHierarchyTree.is_root(p):
                if 'angles' in jParams:
                    if jParams['angles'] != None:
                        try:
                            iter(jParams['angles'])
                            for j in range(4):
                                # convert angles in radians
                                angleDeg = abs(jParams['angles'][j])
                                if angleDeg > 180.0:
                                    angleDeg = 180.0
                                angles.append(radians(angleDeg))
                            # if all angles are nearly PI radians set limit to
                            # None, ie no limit
                            PIlim = PI * 0.999944444
                            if (angles[0] > PIlim) and (angles[1] > PIlim) and \
                                    (angles[2] > PIlim) and (angles[3] > PIlim):
                                jParams['angles'] = None
                            else:
                                jParams['angles'] = tuple(angles)
                        except Exception as e:
                            print(e)
                            jParams['angles'] = None
                else:
                    jParams['angles'] = None
        # compute end effectors dependencies (using postorder traversal)
        self.jEndEffectors = []
        for p in self.jHierarchyTree.postorder():
            jParams = p.element()
            jParams['jEndEffectors'] = []
            # check if itself is leaf or specified as end effectors
            if self.jHierarchyTree.is_leaf(p) or (
                    ('endEffector' in jParams) and (jParams['endEffector'])):
                # add itself to its own end effectors' list
                jParams['jEndEffectors'].append(p)
                # increment the list of all end effectors
                self.jEndEffectors.append(p)
            # inherits the children' end effectors
            for pC in self.jHierarchyTree.children(p):
                jParams['jEndEffectors'].extend(pC.element()['jEndEffectors'])
        # initialize joints data
        self._setupJointsData()
        # debug
        self.debugUpdate = False

    def iterateIK(self, Pd, Od, tolerance=0.01, relError=0.01,
                  maxIterations=100):
        '''Performs an iteration of the CCD algorithm's.

        :param Pd: the desired (global) positions of the end effectors (i.e. 
        leaf joints); this parameter should be specified as a dictionary with
        each item equal to a (jointName, LPoint3f) pair, with the point
        representing the position.
        :type Pd: dict
        :param Od: the desired (global) orientations of the end effectors (i.e.
        leaf joints); this parameter should be specified as a dictionary with
        each item equal to a (jointName, (LVector3f,LVector3f,LVector3f)) pair,
        with the vector triple representing the orthonormal rows of the rotation
        matrix (i.e. each row represents the corresponding rotated unit vector
        of the global frame). This parameter could be None, in which case
        desired (global) orientations will be ignored during the iteration.
        :type Od: dict, None
        :param tolerance: the maximum error measure for which desired target is
        considered reached (i.e. for which iteration would stop).
        :type tolerance: float, optional, defaults to 0.01 
        :param relError: the maximum percentage error for which it is considered
        that the target is not approaching, that is, if after two consecutive
        iterations the error measure, even if greater than the tolerance, in
        percentage is lower than this value then the error measure is considered
        it will not be able to be significantly reduced, so the iterations are
        interrupted.
        :type relError: float, optional, defaults to 0.01
        :param maxIterations: the maximum number of iterations allowed.
        :type maxIterations: int, optional, defaults to 100

        .. note::
            base position is constant.
        '''

        #  Store target's pos and orientation axes
        self.targetPos = Pd
        if Od:
            self.targetOrient = Od
            ud = Od
        else:
            self.targetOrient = None
        # The end-effector can be placed as close as possible to some desired
        # position Pd and orientation Od by finding a joint vector q which
        # minimizes the error measure compute initial data and error measure
        Ep, Eo = 0.0, 0.0
        for pJEE in self.jEndEffectors:
            jeeParam = pJEE.element()
            name = jeeParam['name']
            Pc = jeeParam['jGlobalPositions']
            Ep += (Pd[name] - Pc).length_squared()
            uc = jeeParam['axes']
            if Od:
                for j in range(3):
                    Eo += pow((ud[name][j].dot(uc[j]) - 1), 2)
        E = Ep + Eo
        deltaE = E
        # debug only
        self.iterationCycleCount = 0
        while (E > tolerance) and (self.iterationCycleCount < maxIterations):
            if deltaE < relError * E:
                # the target is not approaching
                break
            #
            # Each iteration involves a single traversal of the manipulator from
            # the most distal link inward towards the manipulator base
            for p in self.jHierarchyTree.postorder():
                # skip leaf nodes
                if self.jHierarchyTree.is_leaf(p):
                    continue
                jParams = p.element()
                # compute values
                k1, k2, k3 = 0.0, 0.0, 0.0
                for pJEE in jParams['jEndEffectors']:
                    # skip if itself is an end effector
                    if p == pJEE:
                        continue
                    jeeParam = pJEE.element()
                    name = jeeParam['name']
                    Pc = jeeParam['jGlobalPositions']
                    Pid = Pd[name] - jParams['jGlobalPositions']
                    Pic = Pc - jParams['jGlobalPositions']
                    axisi = Pic.cross(Pid).normalized()
                    pidLen = Pid.length()
                    picLen = Pic.length()
                    rho = min(pidLen, picLen) / max(pidLen, picLen)
                    wp = self.alpha * (1 + rho)
                    wo = 1.0
                    # ks' sub computations
                    k1p = (Pid.dot(axisi)) * (Pic.dot(axisi))
                    k1o = 0.0
                    if Od:
                        for j in range(3):
                            k1o += ud[name][j].dot(axisi) * uc[j].dot(axisi)
                    k2p = Pid.dot(Pic)
                    k2o = 0.0
                    if Od:
                        for j in range(3):
                            k2o += ud[name][j].dot(uc[j])
                    k3p = Pic.cross(Pid)
                    k3o = LVector3f(LVector3f.zero())
                    if Od:
                        for j in range(3):
                            k3o += uc[j].cross(ud[name][j])
                    #
                    k1 += wp * k1p + wo * k1o
                    k2 += wp * k2p + wo * k2o
                    k3 += axisi.dot(k3p * wp + k3o * wo)

                # compute angle fi (in degrees) and the related rotation for
                # each children
                for pC in self.jHierarchyTree.children(p):
                    jParamsChild = pC.element()
                    # child is an end effector?
                    fi = degrees(jParamsChild['w'] * atan2(k3, k2 - k1))
                    rot = LRotationf(axisi, fi)
                    # compute the direction of the frame's forward axis of the
                    # joint[i + 1]
                    forward = LVector3f(rot.xform(jParamsChild['axes'][1]))
                    # check if correction of rotation is needed
                    rotCorr = None
                    if jParamsChild['angles'] != None:
                        ANGLES = jParamsChild['angles']
                        AXES = jParams['axes']
                        # find the octant in the joint[i]'s frame where is the
                        # forward axis
                        pR = forward.dot(AXES[0])  # r coordinate
                        pU = forward.dot(AXES[2])  # u coordinate
                        if (pR >= 0.0) and (pU >= 0):
                            # I, V octants
                            rotCorr = self._checkRotCorrection(
                                ANGLES, forward, 0, 1, pR, pU, AXES)
                        elif (pR < 0.0) and (pU >= 0):
                            # II, VI octants
                            rotCorr = self._checkRotCorrection(
                                ANGLES, forward, 0, 3, -pR, pU, AXES)
                        elif (pR < 0.0) and (pU < 0):
                            # III, VII octants
                            rotCorr = self._checkRotCorrection(
                                ANGLES, forward, 2, 3, -pR, -pU, AXES)
                        elif (pR >= 0.0) and (pU < 0):
                            # IV, VIII octants
                            rotCorr = self._checkRotCorrection(
                                ANGLES, forward, 2, 1, pR, -pU, AXES)
                    # apply corrective rotation, if any
                    if rotCorr != None:
                        rot = rot * rotCorr
                    # rotate the hierarchy till the end-effector updating each
                    # joint's data in preorder trasversal rooted at p
                    for pCR in self.jHierarchyTree._subtree_preorder(pC):
                        jParamsCR = pCR.element()
                        # update frame of joint[j]
                        ucj = jParamsCR['axes']
                        right = LVector3f(rot.xform(ucj[0]))
                        forward = LVector3f(rot.xform(ucj[1]))
                        up = LVector3f(rot.xform(ucj[2]))
                        jParamsCR['axes'] = (right, forward, up)
                        # update position of joint[j]
                        jParamsCP = self.jHierarchyTree.parent(pCR).element()
                        jParamsCR['jGlobalPositions'] = (jParamsCP['jGlobalPositions'] +
                                                         forward * jParamsCR['jLengths'])
            # recompute initial data and error measure
            Ep, Eo = 0.0, 0.0
            for pJEE in self.jEndEffectors:
                jeeParam = pJEE.element()
                name = jeeParam['name']
                Pc = jeeParam['jGlobalPositions']
                Ep += (Pd[name] - Pc).length_squared()
                uc = jeeParam['axes']
                if Od:
                    for j in range(3):
                        Eo += pow((ud[name][j].dot(uc[j]) - 1), 2)
            EPrev = E
            E = Ep + Eo
            deltaE = abs(E - EPrev)
            # debug only
            self.iterationCycleCount += 1

    def applyBaseJointTransform(self, globalTrans):
        '''Must be called when base joint goes through a global external transformation.

        If the base joint is subject to a global external transformation after
        carrying out an IK iteration, this method must be called before the next
        iteration, giving the 'difference' transformation between the new state
        and the previous one as an argument.

        :param globalTrans: the global 'difference' transform applied to the
        base joint.
        :type globalTrans: class: `TransformState`.
        '''

        # update each joint's global position, rotation and local frame axes
        if not globalTrans.is_identity():
            matW = globalTrans.get_mat()
            for p in self.jHierarchyTree.breadthfirst():
                jParams = p.element()
                jParams['jGlobalPositions'] = matW.xform_point(
                    jParams['jGlobalPositions'])
                jParams['axes'] = (
                    LVector3f(matW.xform_vec(jParams['axes'][0])).normalized(),
                    LVector3f(matW.xform_vec(jParams['axes'][1])).normalized(),
                    LVector3f(matW.xform_vec(jParams['axes'][2])).normalized())

    def applyJointsTransforms(self, globalTransArray):
        '''Must be called if the joints go through global external transformations.

        If all the joints are subject to global external transformations after
        carrying out an IK iteration, this method must be called before the next
        iteration, giving the 'difference' transformations between the new state
        and the previous one as an argument.

        :param globalTransArray: the list of global 'difference' transformations
        (class: `TransformState`) applied to joints; this list should be stored
        as an 'array based' representation that mirrors the joint hierarchy
        tree. See discussion about jointHierarchyList constructor's parameter
        for an explanation.
        :type globalTransArray: list
        '''

        # globalTransArray must have exactly len(self.jHierarchyTree) elements
        if len(globalTransArray) != len(self.jHierarchyTree):
            return

        # update each joint's global position, rotation and local frame axes
        transIter = iter(globalTransArray)
        for p in self.jHierarchyTree.breadthfirst():
            trans = transIter.__next__()
            jParams = p.element()
            matW = trans.get_mat()
            jParams['jGlobalPositions'] = matW.xform_point(
                jParams['jGlobalPositions'])
            jParams['axes'] = (
                LVector3f(matW.xform_vec(jParams['axes'][0])).normalized(),
                LVector3f(matW.xform_vec(jParams['axes'][1])).normalized(),
                LVector3f(matW.xform_vec(jParams['axes'][2])).normalized())

    @staticmethod
    def initializeJointNodes(actor, jHierarchyTree, jointBaseParent):
        '''Prepares the actor joints belonging to the hierarchy for subsequent IK updates.

        For each actor joint of the CCDIK.jHierarchyTree this method adds a
        NodePath controlling it and and having the same name.
        The joint parent of the base joint will be exposed to a NodePath. 

        :param actor: a CCDIK.actor.
        :type actor: class: `direct.actor.Actor.Actor`
        :param jHierarchyTree: a CCDIK.jHierarchyTree.
        :type jHierarchyTree: class: `LinkedTree`
        :param jointBaseParent: the parent joint's name of the base joint of the
        hierarchy, or a panda node if the base joint is the root of the entire
        skeleton.
        :type jointBaseParent: str or class: `NodePath`
        :return: a pair (jBaseParent, jControlNodes), where jBaseParent is the
        exposed NodePath of the joint base parent and jControlNodes is a
        dictionary of the NodePaths controlling the joints keyed by joint name
        :rtype: tuple
        '''

        jControlNodes = {}
        # initialize joint base's parent joint
        if isinstance(jointBaseParent, NodePath):
            jBaseParent = jointBaseParent
        else:
            jBaseParent = actor.expose_joint(
                None, 'modelRoot', jointBaseParent)
            jBaseParent.set_name(jointBaseParent)
        # create a control node for each joint: using preorder traversal
        for p in jHierarchyTree.preorder():
            jParams = p.element()
            jointName = jParams['joints'].get_name()
            npC = actor.control_joint(None, 'modelRoot', jointName)
            npC.set_name(jointName)
            if jHierarchyTree.is_root(p):
                npC.reparent_to(jBaseParent)
                jParams['jBaseParent'] = jBaseParent
            else:
                npC.reparent_to(jHierarchyTree.parent(
                    p).element()['jControlNodes'])
            jParams['jControlNodes'] = npC
            jControlNodes[jointName] = npC
        # return
        return (jBaseParent, jControlNodes)

    @staticmethod
    def releaseJointNodes(actor, jHierarchyTree):
        '''Removes the actor joints belonging to the hierarchy from subsequent updates.

        :param actor: a CCDIK.actor.
        :type actor: class: `direct.actor.Actor.Actor`
        :param jHierarchyTree: a CCDIK.jHierarchyTree.
        :type jHierarchyTree: class: `LinkedTree`
        '''

        jParams = jHierarchyTree.root().element()
        # stop base joint's parent joint from animating the external node
        actor.stop_joint('modelRoot', jParams['jBaseParent'].get_name())
        # remove the control node
        jParams['jBaseParent'].remove_node()
        # remove the dictionary key
        jParams.pop('jBaseParent', None)
        #  traverse the hierarchy in postorder
        for p in jHierarchyTree.postorder():
            jParams = p.element()
            # restore the joint to its normal animation
            actor.release_joint(
                'modelRoot', jParams['jControlNodes'].get_name())
            # remove the control node
            jParams['jControlNodes'].remove_node()
            # remove the dictionary key
            jParams.pop('jControlNodes', None)

    @staticmethod
    def updateJointNodesLocalTransforms(render, jHierarchyTree):
        '''Updates the transforms of each control node in local space.

        Since the IK algorithm works in global space, the local transformations
        of the joint control nodes should be updated after each IK iteration by
        calling this method; it must also be called when the whole system is
        initialized.
        This is a default implementation of the procedure, others can be
        provided.

        :param render: the render object.
        :type render: class: `NodePath`
        :param jHierarchyTree: a CCDIK.jHierarchyTree.
        :type jHierarchyTree: class: `LinkedTree`
        '''

        # remember control node are for each joint but end effector
        # compute base joint local rotation defined by axis and angle (in
        # degrees) traverse the tree in preorder
        for p in jHierarchyTree.preorder():
            # compute joint local rotation defined by axis and angle (in
            # degrees)
            numChildren = jHierarchyTree.num_children(p)
            if numChildren > 0:
                jParams = p.element()
                if jHierarchyTree.is_root(p):
                    jParent = jParams['jBaseParent']
                else:
                    jParent = jHierarchyTree.parent(
                        p).element()['jControlNodes']
                childRot = []
                for pC in jHierarchyTree.children(p):
                    jParamsChild = pC.element()
                    Fapex = (jParamsChild['jControlNodes'].get_transform().get_mat().xform_point(
                        LPoint3f.zero()))
                    vecI = (jParent.get_relative_vector(jParams['jControlNodes'],
                                                        Fapex).normalized())
                    vecF = (jParent.get_relative_vector(render, (jParamsChild['jGlobalPositions'] -
                                                                 jParams['jGlobalPositions'])).normalized())
                    angle = vecI.angle_deg(vecF)
                    if angle != 0.0:
                        childRot.append(LRotationf(
                            vecI.cross(vecF).normalized(), angle))
                    else:
                        childRot.append(LRotationf.ident_quat())
                if numChildren == 1:
                    rot = childRot[0]
                else:  # numChildren > 1
                    # compute average rotation among children (all weights are
                    # 1.0)
                    Q = np.array([CCDik._getQuatRow(r) for r in childRot])
                    avgQuat = CCDik._quatWAvgMarkley(Q, [1.0] * len(childRot))
                    rot = LRotationf(*avgQuat)
                # set control node p local transform = composition of original
                # and a rotation
                jParams['jControlNodes'].set_quat(
                    jParams['jControlNodes'].get_quat() * rot)

    def _setupJointsData(self):
        '''Must be called to (re)initialize each joint's global position and local frame axes.

        Initializes global positions (in global coordinates) for each joint.
        Initializes joint frame's axes (in global coordinates) for each joint.
        Computes lengths of each bone (i.e. distance between adjacent joints).

        .. note::
            distances between joints need not to be recomputed because the
            skeleton's joints are inextensible.
        '''

        # initialize joints and their positions
        partBundle = self.actor.get_part_bundle('modelRoot')
        for p in self.jHierarchyTree.breadthfirst():
            jParams = p.element()
            npE = self.actor.expose_joint(None, 'modelRoot', jParams['name'])
            jParams['jGlobalPositions'] = npE.get_pos(self.render)
            self.actor.stop_joint('modelRoot', jParams['name'])
            npE.remove_node()
            jParams['joints'] = partBundle.find_child(jParams['name'])

        # initialize each joint local frame's axes, and
        # compute the length of each bone: the distance of a joint from its
        # parent
        for p in self.jHierarchyTree.breadthfirst():
            jParams = p.element()
            if self.jHierarchyTree.is_root(p):
                jParams['jLengths'] = 0.0
                forward = jParams['baseForwardAxis'].normalized()
            else:
                # compute the default forward axis
                jParamsParent = self.jHierarchyTree.parent(p).element()
                jVector = jParams['jGlobalPositions'] - \
                    jParamsParent['jGlobalPositions']
                jParams['jLengths'] = jVector.length()
                forward = jVector.normalized()
            # compute up axis
            v = jParams['upAxis']
            vp = forward * (forward.dot(v))
            vn = v - vp
            up = vn.normalized()
            # compute right axis
            right = forward.cross(up).normalized()
            # store result
            jParams['axes'] = (right, forward, up)

    def _checkRotCorrection(self, ANGLES, forward, aUI, aRI, pR, pU, axes):
        '''Internal use only.
        '''

        tethaNorm = atan2(pR, pU) * PIHALFINV
        alphaLim = degrees(ANGLES[aUI] + tethaNorm *
                           (ANGLES[aRI] - ANGLES[aUI]))
        alphaForw = axes[1].angle_deg(forward)
        if alphaForw > alphaLim:
            # correction needed
            axisCorr = axes[1].cross(forward).normalized()
            angleCorr = alphaLim - alphaForw
            # return corrective rotation
            return LRotationf(axisCorr, angleCorr)
        # no correction
        return None

    @staticmethod
    def _getQuatRow(q):
        return [q.get_r(), q.get_i(), q.get_j(), q.get_k()]

    @staticmethod
    def _quatWAvgMarkley(Q, weights):
        '''Averaging Quaternions.

        :param Q: an Mx4 ndarray of quaternions.
        :type Q: class: `numpy.ndarray`
        :param weights: an M elements list, a weight for each quaternion.
        :type weights: list
        :return: returns the average quaternion.
        :rtype: class: `numpy.ndarray`

        .. seealso::
            - Markley, F.L., Cheng, Y., Crassidis, J.L., and Oshman, Y.,
              'Averaging Quaternions,' AIAA Journal of Guidance, Control, and
              Dynamics, Vol. 30, No. 4, July-Aug. 2007, pp. 1193-1196.
              (http://www.acsu.buffalo.edu/~johnc/ave_quat07.pdf)
            - https://stackoverflow.com/questions/12374087/average-of-multiple-quaternions
            - https://it.mathworks.com/matlabcentral/fileexchange/40098-tolgabirdal-averaging-quaternions
        '''

        # Form the symmetric accumulator matrix
        A = np.zeros((4, 4))
        M = Q.shape[0]
        wSum = 0

        for i in range(M):
            q = Q[i, :]
            w_i = weights[i]
            A += w_i * (np.outer(q, q))  # rank 1 update
            wSum += w_i

        # scale
        A /= wSum

        # Get the eigenvector corresponding to largest eigen value
        return np.linalg.eigh(A)[1][:, -1]

    @staticmethod
    def _debugDraw(ccdikObj, app, model='smiley', color=LVecBase4f(0.5, 0, 0, 1), reset=False):
        '''Debug draw.
        '''

        def _drawModel(model, name, scale, pos, hpr, color, parent, binDraw):
            nonlocal ccdikObj, app
            if not ccdikObj.debugUpdate:
                debugModelAttr = 'debug' + str(model)
                if not hasattr(ccdikObj, debugModelAttr):
                    setattr(ccdikObj, debugModelAttr,
                            app.loader.loadModel(model))
                    ccdikObj.modelAttrs.append(debugModelAttr)
                debugModel = getattr(ccdikObj, debugModelAttr)
                debugModelInst = NodePath(name)
                debugModel.instance_to(debugModelInst)
                debugModelInst.reparent_to(parent)
                # set other attrs
                debugModelInst.set_scale(scale)
                if color != None:
                    debugModelInst.set_texture_off(1)
                    debugModelInst.set_color(color)
                debugModelInst.set_depth_write(False)
                debugModelInst.set_depth_test(False)
                debugModelInst.set_bin('fixed', binDraw)
                ccdikObj.debugModels[name] = debugModelInst
            # update transform
            ccdikObj.debugModels[name].set_pos_hpr(pos, hpr)
            return ccdikObj.debugModels[name]

        def _drawSquare(name, scale, pos, color, parent, binDraw, hpr=LVecBase3f.zero(),
                        twoSided=False, transparency=False, billboard=True):
            nonlocal ccdikObj
            if not ccdikObj.debugUpdate:
                square = CardMaker(name)
                square.set_frame(-0.1, 0.1, -0.1, 0.1)
                squarenp = parent.attach_new_node(square.generate())
                squarenp.set_scale(scale)
                squarenp.set_color(color)
                squarenp.set_bin('fixed', binDraw)
                squarenp.set_depth_write(False)
                squarenp.set_depth_test(False)
                if billboard:
                    squarenp.set_billboard_point_eye()
                if twoSided:
                    squarenp.set_two_sided(True)
                if transparency:
                    squarenp.set_attrib(TransparencyAttrib.make(
                        TransparencyAttrib.M_alpha))
                ccdikObj.debugSquares[name] = squarenp
            # update transform
            ccdikObj.debugSquares[name].set_pos_hpr(pos, hpr)
            return ccdikObj.debugSquares[name]

        def _drawSegment(name, pos, toward, length, color, thickness, parent, drawBin):
            nonlocal ccdikObj
            if not ccdikObj.debugUpdate:
                segment = LineSegs(name)
                segment.set_thickness(thickness)
                segment.set_color(color)
                segmentGN = GeomNode(name + '_GN')
                segmentnp = parent.attach_new_node(segmentGN)
                segmentnp.set_bin('fixed', drawBin)
                segmentnp.set_depth_write(False)
                segmentnp.set_depth_test(False)
                ccdikObj.debugSegments[name] = (segment, segmentnp)
            # update transform
            segment = ccdikObj.debugSegments[name][0]
            geomnode = ccdikObj.debugSegments[name][1].node()
            segment.move_to(pos)
            segment.draw_to(pos + toward * length)
            if geomnode.get_num_geoms() > 0:
                geomnode.remove_geom(0)
            segment.create(geomnode, True)
            return ccdikObj.debugSegments[name][1]

        if reset:
            ccdikObj.debugUpdate = False
            if hasattr(ccdikObj, 'debugModels'):
                for name in ccdikObj.debugModels:
                    ccdikObj.debugModels[name].remove_node()
                for i in len(ccdikObj.modelAttrs):
                    delattr(ccdikObj, ccdikObj.modelAttrs[i])
            if hasattr(ccdikObj, 'debugSquares'):
                for name in ccdikObj.debugSquares:
                    ccdikObj.debugSquares[name].remove_node()
            if hasattr(ccdikObj, 'debugSegments'):
                for name in ccdikObj.debugSegments:
                    ccdikObj.debugSegments[name][1].remove_node()
            return

        if not ccdikObj.debugUpdate:
            ccdikObj.modelAttrs = []
            ccdikObj.debugModels = {}
            ccdikObj.debugSquares = {}
            ccdikObj.debugSegments = {}

        # all computations are in global space
        # get 'tight' dimensions of actor
        minP = LPoint3f()
        maxP = LPoint3f()
        ccdikObj.actor.calc_tight_bounds(minP, maxP)
        delta = maxP - minP
        actorDims = LVector3f(abs(delta.get_x()), abs(delta.get_y()),
                              abs(delta.get_z()))
        # minDim = min(min(actorDims.get_x(), actorDims.get_y()), actorDims.get_z())
        # maxDim = max(max(actorDims.get_x(), actorDims.get_y()), actorDims.get_z())
        mediumDim = (actorDims.get_x() + actorDims.get_y() +
                     actorDims.get_z()) / 3.0
        # body radius of body node path
        bodyRadius = mediumDim / 50.0
        #
        sphereScale = bodyRadius * 0.4
        planeScale = bodyRadius * 12.0
        segmentLen = 1.2 * bodyRadius
        limitLen = segmentLen * 2
        # draw targets
        if hasattr(ccdikObj, 'targetPos'):
            if ccdikObj.targetOrient:
                for name in ccdikObj.targetPos:
                    _drawSquare(name + 'TP', 1.4, ccdikObj.targetPos[name], LVecBase4f(
                        0.5, 0.5, 0.5, 1), ccdikObj.render, 44)
                    _drawSegment(name + 'TR', ccdikObj.targetPos[name], ccdikObj.targetOrient[name]
                                 [0], segmentLen, LVecBase4f(1, 0, 0, 1), 1.5, ccdikObj.render, 48)
                    _drawSegment(name + 'TF', ccdikObj.targetPos[name], ccdikObj.targetOrient[name]
                                 [1], segmentLen, LVecBase4f(0, 1, 0, 1), 1.5, ccdikObj.render, 48)
                    _drawSegment(name + 'TU', ccdikObj.targetPos[name], ccdikObj.targetOrient[name]
                                 [2], segmentLen, LVecBase4f(0, 0, 1, 1), 1.5, ccdikObj.render, 48)
        else:
            for pJEE in ccdikObj.jEndEffectors:
                name = pJEE.element()['name']
                _drawSquare(name + 'TP', 1.4, LPoint3f.zero(), LVecBase4f(0.5, 0.5, 0.5, 1),
                            ccdikObj.render, 44)
                _drawSegment(name + 'TR', LPoint3f.zero(), LVector3f.right(),
                             segmentLen, LVecBase4f(1, 0, 0, 1), 1.5, ccdikObj.render, 48)
                _drawSegment(name + 'TF', LPoint3f.zero(), LVector3f.forward(),
                             segmentLen, LVecBase4f(0, 1, 0, 1), 1.5, ccdikObj.render, 48)
                _drawSegment(name + 'TU', LPoint3f.zero(), LVector3f.up(),
                             segmentLen, LVecBase4f(0, 0, 1, 1), 1.5, ccdikObj.render, 48)
        # draw joint hierarchy in preorder traversal
        for p in ccdikObj.jHierarchyTree.preorder():
            jParams = p.element()
            name = jParams['name']
            # final positions
            pos = jParams['jGlobalPositions']
            _drawModel(model, 'joint_' + name, sphereScale, pos,
                       LVecBase3f.zero(), color, ccdikObj.render, 45)
            # bones
            if not ccdikObj.jHierarchyTree.is_root(p):
                posPrec = ccdikObj.jHierarchyTree.parent(
                    p).element()['jGlobalPositions']
                length = (pos - posPrec).length()
                toward = (pos - posPrec).normalized()
                _drawSegment('bone_' + name, posPrec, toward, length,
                             LVecBase4f(0.4, 0, 0.2, 1), 5.0, ccdikObj.render, 43)
            # local frame: plane + axes and angle limits
            AXES = jParams['axes']
            # plane
            mat = LMatrix4f()
            mat.set_translate_mat(LVector3f.zero())
            mat.set_row(0, -AXES[0])
            mat.set_row(1, -AXES[1])
            mat.set_row(2, AXES[2])
            hpr = TransformState.make_mat(mat).get_hpr()
            hpr = LVecBase3f(hpr.get_x(), hpr.get_y(), hpr.get_z())
            _drawSquare('plane_ru_' + name, planeScale, pos, LVecBase4f(0.8, 0.8, 0, 0.8),
                        ccdikObj.render, 44, hpr=hpr, twoSided=True, transparency=True, billboard=False)
            # axes
            _drawSegment('axis_r_' + name, pos,
                         AXES[0], segmentLen, LVecBase4f(1, 0, 0, 1), 1.5, ccdikObj.render, 48)
            _drawSegment('axis_f_' + name, pos,
                         AXES[1], segmentLen, LVecBase4f(0, 1, 0, 1), 1.5, ccdikObj.render, 48)
            _drawSegment('axis_u_' + name, pos,
                         AXES[2], segmentLen, LVecBase4f(0, 0, 1, 1), 1.5, ccdikObj.render, 48)
            for pC in ccdikObj.jHierarchyTree.children(p):
                jParamsChild = pC.element()
                if ('angles' in jParamsChild) and jParamsChild['angles']:
                    nameC = jParamsChild['name']
                    # angle limit U positive: 0
                    rot = LRotationf(AXES[0], degrees(
                        jParamsChild['angles'][0]))
                    toward = rot.xform(AXES[1])
                    _drawSegment('aUpos_' + nameC, pos, toward, limitLen,
                                 LVecBase4f(0, 0.8, 0.8, 1), 2.0, ccdikObj.render, 49)
                    # angle limit R positive: 1
                    rot = LRotationf(
                        AXES[2], degrees(-jParamsChild['angles'][1]))
                    toward = rot.xform(AXES[1])
                    _drawSegment('aRpos_' + nameC, pos, toward, limitLen,
                                 LVecBase4f(0.8, 0, 0.8, 1), 2.0, ccdikObj.render, 49)
                    # angle limit U negative: 2
                    rot = LRotationf(
                        AXES[0], degrees(-jParamsChild['angles'][2]))
                    toward = rot.xform(AXES[1])
                    _drawSegment('aUneg_' + nameC, pos, toward, limitLen,
                                 LVecBase4f(0, 0.4, 0.4, 1), 2.0, ccdikObj.render, 49)
                    # angle limit R negative: 3
                    rot = LRotationf(AXES[2], degrees(
                        jParamsChild['angles'][3]))
                    toward = rot.xform(AXES[1])
                    _drawSegment('aRneg_' + nameC, pos, toward, limitLen,
                                 LVecBase4f(0.4, 0, 0.4, 1), 2.0, ccdikObj.render, 49)
            # real joints
            jPos = jParams['jControlNodes'].get_pos(ccdikObj.render)
            # _drawSquare('sq_' + str(jPos.length()), 0.7, jPos, color * 0.6, ccdikObj.render, 46)
            _drawModel(model, 'realjoint' + name, sphereScale * 0.7,
                       jPos, LVecBase3f.zero(), color * 0.6, ccdikObj.render, 46)
        # set debugUpdate
        if not ccdikObj.debugUpdate:
            ccdikObj.debugUpdate = True
