/**
 * PANDA 3D SOFTWARE
 * Copyright (c) Carnegie Mellon University.  All rights reserved.
 *
 * All use of this software is subject to the terms of the revised BSD
 * license.  You should have received a copy of this license along
 * with this source code in a file named "LICENSE."
 *
 * @file rocketFileInterface.h
 * @author rdb
 * @date 2011-11-03
 */
/**
 * \file p3guiFileInterface.h
 *
 * \date 2019-10-03
 * \author consultit
 */

#ifndef P3GUI_FILE_INTERFACE_H
#define P3GUI_FILE_INTERFACE_H

#include "config_module.h"
#include "p3gui_includes.h"
#include "virtualFile.h"

#ifndef CPPPARSER
#include "RmlUi/Include/RmlUi/Core/FileInterface.h"
#endif //CPPPARSER

class VirtualFileSystem;

/**
 * Implementation of FileInterface to allow RmlUi to read files from the
 * virtual file system.
 */
class P3GUIFileInterface : public Rml::FileInterface {
public:
  P3GUIFileInterface(VirtualFileSystem *vfs = nullptr);
  virtual ~P3GUIFileInterface() {};

  Rml::FileHandle Open(const string& path);
  void Close(Rml::FileHandle file);

  size_t Read(void* buffer, size_t size, Rml::FileHandle file);
  bool Seek(Rml::FileHandle file, long offset, int origin);
  size_t Tell(Rml::FileHandle file);

  size_t Length(Rml::FileHandle file);

protected:
  struct VirtualFileHandle {
    PT(VirtualFile) _file;
    std::istream *_stream;
  };

  VirtualFileSystem* _vfs;
};

#endif
