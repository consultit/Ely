/**
 * \file p3guisymbols.h
 *
 * \date 2019-10-02
 * \author consultit
 */
#ifndef GUI_SOURCE_P3GUISYMBOLS_H_
#define GUI_SOURCE_P3GUISYMBOLS_H_

#ifdef PYTHON_BUILD_p3gui
#	define EXPCL_P3GUI EXPORT_CLASS
#	define EXPTP_P3GUI EXPORT_TEMPL
#else
#	define EXPCL_P3GUI IMPORT_CLASS
#	define EXPTP_P3GUI IMPORT_TEMPL
#endif

#endif /* GUI_SOURCE_P3GUISYMBOLS_H_ */
