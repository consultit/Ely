/**
 * \file p3guiDataStructures.h
 *
 * \date 2019-10-09
 * \author consultit
 */

#ifndef GUI_SOURCE_P3GUIDATASTRUCTURES_H_
#define GUI_SOURCE_P3GUIDATASTRUCTURES_H_

#include "config_module.h"
#include "p3gui_includes.h"
#include "p3guiRegion.h"
#include "p3guiElements.h"

#ifndef CPPPARSER
#include "RmlUi/Include/RmlUi/Core.h"
#endif //CPPPARSER

/// P3GUIDictionary
class EXPCL_P3GUI P3GUIDictionary
{
PUBLISHED:
	INLINE P3GUIDictionary();
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIDictionary();
#endif //CPPPARSER

	INLINE void set_string(const string &key, const string &value);
	INLINE void clear();
	INLINE bool is_empty() const;
	INLINE int size() const;

public:
	Rml::Dictionary rmlObject;
};

typedef P3GUIDictionary P3GUIXMLAttributes;

/// P3GUIVariantPtr
class EXPCL_P3GUI P3GUIVariantPtr
{
PUBLISHED:
	INLINE P3GUIVariantPtr();
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIVariantPtr();
#endif //CPPPARSER

	INLINE bool is_empty() const;
	MAKE_PROPERTY(empty, is_empty);

public:
	inline P3GUIVariantPtr(Rml::Variant *_rmlObject);
	inline Rml::Variant& get_rml_object();
	Rml::Variant *rmlObject;
};

/// inline definitions
#include "p3guiDataStructures.I"

#endif /* GUI_SOURCE_P3GUIDATASTRUCTURES_H_ */
