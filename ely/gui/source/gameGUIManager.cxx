/**
 * \file gameGUIManager.cxx
 *
 * \date 2019-10-04
 * \author consultit
 */

#include "gameGUIManager.h"
#include "p3guiRegion.h"
#include "default_font.h"

#include "RmlUi/Include/RmlUi/Debugger.h"
#include "RmlUi/Include/RmlUi/Core.h"

#ifdef PYTHON_BUILD
#include "py_panda.h"
extern Dtool_PyTypedObject Dtool_P3GUIElementDocumentPtr;
extern Dtool_PyTypedObject Dtool_P3GUIEventPtr;
#endif //PYTHON_BUILD

GameGUIManager::GameGUIManager(PT(GraphicsWindow) win, NodePath mw) :
		mWin
		{ win }, mMw
		{ mw }
{
	PRINT_DEBUG(
			"GameGUIManager::GameGUIManager: creating the singleton manager.");

	do_reset();
}

/**
 *
 */
INLINE GameGUIManager::~GameGUIManager()
{
	PRINT_DEBUG(
			"GameGUIManager::~GameGUIManager: destroying the singleton manager.");

	//clean up
	gui_cleanup();
}

/**
 * Sets up the (RmlUi) GUI system.
 * Should be called after setting paths of main/exit menus.
 * It can be called only once.
 */
void GameGUIManager::gui_setup()
{
	RETURN_ON_COND(
			mSetup or mGuiMainMenuPath.empty() or mGuiExitMenuPath.empty(),)

	///RmlUi initialization
	mFi = new P3GUIFileInterface;
	Rml::SetFileInterface(mFi);

	mSi = new P3GUISystemInterface;
	Rml::SetSystemInterface(mSi);

	Rml::Initialise();

#ifdef COMPILE_IN_DEFAULT_FONT
#ifdef HAVE_FREETYPE
	// Load Panda's default compiled-in freetype font (Perspective Sans).
	Rml::LoadFontFace(default_font_data, default_font_size, "FreeType",
			Rml::Style::FontStyle::Normal, Rml::Style::FontWeight::Normal);
#endif
#endif

	//load fonts
	for (int i = 0; i < mGuiFontPaths.get_num_values(); ++i)
	{
		Rml::LoadFontFace(string(mGuiFontPaths[i]));
	}

	//initialize one region for all documents
	mRegion = P3GUIRegion::make("elyRmlUi", mWin);
	mRegion->set_active(true);

	//set input handler
	PT(P3GUIInputHandler) inputHandler = new P3GUIInputHandler();
	mMw.attach_new_node(inputHandler);
	mRegion->set_input_handler(inputHandler);

	//set global context variable
	mGuiRmlUiContext = P3GUIContextPtr(mRegion->get_context());

	//register the main EventListenerInstancer: used by all the application documents
	mEventListenerInstancer = new MainEventListenerInstancer();
	Rml::Factory::RegisterEventListenerInstancer(mEventListenerInstancer);

#ifdef ELY_DEBUG
	Rml::Debugger::Initialise(&mGuiRmlUiContext.get_rml_object());
	Rml::Debugger::SetVisible(true);
#endif
	mSetup = true;
}

/**
 * Clean up the (RmlUi) GUI system.
 * It should be called before exiting.
 * It can be called only once and only after gui_setup().
 */
void GameGUIManager::gui_cleanup()
{
	RETURN_ON_COND(mCleanup or !mSetup,)

	//reset global context variable
	mGuiRmlUiContext = P3GUIContextPtr(nullptr);

	// Shutdown RmlUi.
	Rml::Shutdown();

	// delete interfaces
	delete mFi;
	delete mSi;

	do_reset();
	mCleanup = true;
}

/**
 * Resets members.
 */
void GameGUIManager::do_reset()
{
	//
	mGuiMainMenuPath.clear();
	mGuiExitMenuPath.clear();
	mGuiFontPaths.clear();
	mRegion.clear();
	mSetup = false;
	mCleanup = false;
	mGuiOnExitFunction = nullptr;
	mGuiMainPresetsCommits = false;
	mEventListenerInstancer = nullptr;
#ifdef PYTHON_BUILD
	for (auto func : mGuiAddElementsFunctions)
	{
		Py_DECREF(func);
	}
	for (auto nameFunc : mGuiEventHandlers)
	{
		Py_DECREF(nameFunc.second);
	}
	for (auto func : mGuiPresetFunctions)
	{
		Py_DECREF(func);
	}
	for (auto func : mGuiCommitFunctions)
	{
		Py_DECREF(func);
	}
	Py_XDECREF(mGuiOnExitFunction);
#endif //PYTHON_BUILD
	mGuiOnExitFunction = nullptr;
	mGuiAddElementsFunctions.clear();
	mGuiEventHandlers.clear();
	mGuiPresetFunctions.clear();
	mGuiCommitFunctions.clear();
#ifdef ELY_DEBUG
	mDebugEnabled = false;
#endif // ELY_DEBUG
	mFi = nullptr;
	mSi = nullptr;
	mRef = 0;
}

/**
 * Shows the main menu.
 */
void GameGUIManager::show_main_menu()
{
	RETURN_ON_COND(mGuiMainMenuPath.empty() or mGuiExitMenuPath.empty(),)
	//return if already shown or we are asking to exit
	RETURN_ON_COND(
			!mGuiRmlUiContext.get_document("main_menu").is_empty()
					or !mGuiRmlUiContext.get_document("exit_menu").is_empty(),)

	//call all registered preset functions
	for (auto func : mGuiPresetFunctions)
	{
#ifdef PYTHON_BUILD
		// call python func
		PY_ZERO_ARGS_CALL(func,
				string(" GameGUIManager.show_main_menu(): Error calling '"),
				string("' \"preset\" function"));
#else
		// call c++ func
		func();
#endif //PYTHON_BUILD
	}
	//presets & commits are executing by main menu
	mGuiMainPresetsCommits = true;

	// Load and show the main document.
	mGuiMainMenu = mGuiRmlUiContext.load_document(mGuiMainMenuPath);
	if (!mGuiMainMenu.is_empty())
	{
		mGuiMainMenu.get_element_by_id("title").set_inner_rml(
				mGuiMainMenu.get_title());
		//call all registered add elements functions
		for (auto func : mGuiAddElementsFunctions)
		{
#ifdef PYTHON_BUILD
			// call python func
			PY_ONE_ARGS_CALL(func, (void*) &mGuiMainMenu,
					Dtool_P3GUIElementDocumentPtr,
					string(" GameGUIManager.show_main_menu(): Error calling '"),
					string("' \"add elements\" function"));
#else
			// call c++ func
			func(mGuiMainMenu);
#endif //PYTHON_BUILD
		}
		mGuiMainMenu.show();
	}
}

/**
 * Shows the exit menu.
 */
void GameGUIManager::show_exit_menu()
{
	RETURN_ON_COND(mGuiMainMenuPath.empty() or mGuiExitMenuPath.empty(),)
	//return if we are already asking to exit
	RETURN_ON_COND(!mGuiRmlUiContext.get_document("exit_menu").is_empty(),)

	//if presets & commits are not being executed by main menu
	if (not mGuiMainPresetsCommits)
	{
		//call all registered preset functions
		for (auto func : mGuiPresetFunctions)
		{
#ifdef PYTHON_BUILD
			// call python func
			PY_ZERO_ARGS_CALL(func,
					string(" GameGUIManager.show_exit_menu(): Error calling '"),
					string("' \"preset\" function"));
#else
			// call c++ func
			func();
#endif //PYTHON_BUILD
		}
	}

	// Load and show the exit menu modal document.
	mGuiExitMenu = mGuiRmlUiContext.load_document(mGuiExitMenuPath);
	if (!mGuiExitMenu.is_empty())
	{
		mGuiExitMenu.get_element_by_id("title").set_inner_rml(
				mGuiExitMenu.get_title());
		//
		mGuiExitMenu.show(m_MODAL);
	}
}

#ifdef PYTHON_BUILD
/**
 * Registers a gui add elements function.
 * Registered by subsystems to add their element (tags) to main menu.
 */
void GameGUIManager::register_gui_add_elements_function(PyObject *func)
{
	PY_ADD_FUNC_IN_CONTAINER(func, mGuiAddElementsFunctions,
			string(" MainEventListener.ProcessEvent(): Error registering '"),
			string("' \"add elements\" function"));
}
/**
 * Registers a gui event handler function corresponding to an event name.
 * Registered by subsystems to handle their events.
 */
void GameGUIManager::register_gui_event_handler(const string &name,
		PyObject *func)
{
	PY_ADD_FUNC_IN_TABLE(func, mGuiEventHandlers, name,
			string(" MainEventListener.ProcessEvent(): Error registering '"),
			string("' \"event handler\" function"));
}
/**
 * Returns a gui event handler function corresponding to an event name.
 */
PyObject* GameGUIManager::get_gui_event_handler(const string &name)
{
	auto func = mGuiEventHandlers[name];
	Py_INCREF(func);
	return func;
}
/**
 * Registers a gui preset function.
 * Registered by some subsystems that need to preset themselves before
 * main/exit menus are shown.
 */
void GameGUIManager::register_gui_preset_function(PyObject *func)
{
	PY_ADD_FUNC_IN_CONTAINER(func, mGuiPresetFunctions,
			string(" MainEventListener.ProcessEvent(): Error registering '"),
			string("' \"preset\" function"));
}
/**
 * Registers a gui commit function.
 * Registered by some subsystems that need to commit their changes after
 * main/exit menus are closed.
 */
void GameGUIManager::register_gui_commit_function(PyObject *func)
{
	PY_ADD_FUNC_IN_CONTAINER(func, mGuiCommitFunctions,
			string(" MainEventListener.ProcessEvent(): Error registering '"),
			string("' \"commit\" function"));
}
/**
 * Registers a gui on exit function.
 */
void GameGUIManager::set_gui_on_exit_function(PyObject *func)
{
	PY_SET_FUNC_BODY(mGuiOnExitFunction, func,
			"GameGUIManager.set_gui_on_exit_function()");
}
#else
/**
 * Registers a gui add elements function.
 * Registered by subsystems to add their element (tags) to main menu.
 */
void GameGUIManager::register_gui_add_elements_function(ADD_ELEMENT_FUNC func)
{
	mGuiAddElementsFunctions.push_back(func);
}

/**
 * Registers a gui event handler function corresponding to an event name.
 * Registered by subsystems to handle their events.
 */
void GameGUIManager::register_gui_event_handler(const string &name,
		EVENT_HANDLER func)
{
	mGuiEventHandlers[name] = func;
}

/**
 * Returns a gui event handler function corresponding to an event name.
 */
GameGUIManager::EVENT_HANDLER GameGUIManager::get_gui_event_handler(
		const string &name)
{
	return mGuiEventHandlers[name];
}

/**
 * Registers a gui preset function.
 * Registered by some subsystems that need to preset themselves before
 * main/exit menus are shown.
 */
void GameGUIManager::register_gui_preset_function(PRESET_FUNC func)
{
	mGuiPresetFunctions.push_back(func);
}

/**
 * Registers a gui commit function.
 * Registered by some subsystems that need to commit their changes after
 * main/exit menus are closed.
 */
void GameGUIManager::register_gui_commit_function(COMMIT_FUNC func)
{
	mGuiCommitFunctions.push_back(func);
}
/**
 * Registers a gui on exit function.
 */
void GameGUIManager::set_gui_on_exit_function(COMMIT_FUNC func)
{
	mGuiOnExitFunction = func;
}
#endif //PYTHON_BUILD

///MainEventListener stuff
MainEventListener::MainEventListener(const string &value) :
		EventListener(value)
{
}

MainEventListener::~MainEventListener()
{
}

void MainEventListener::ProcessEvent(Rml::Event &event)
{
#ifdef ELY_DEBUG
	if (GameGUIManager::get_global_ptr()->mDebugEnabled)
	{
		Rml::EventPhase phase = event.GetPhase();
		string phaseStr;
		switch (phase)
		{
		case Rml::EventPhase::Bubble:
			phaseStr = "PHASE_BUBBLE";
			break;
		case Rml::EventPhase::Capture:
			phaseStr = "PHASE_CAPTURE";
			break;
		case Rml::EventPhase::Target:
			phaseStr = "PHASE_TARGET";
			break;
		case Rml::EventPhase::None:
		default:
			phaseStr = "PHASE_None";
			break;
		}
		PRINT_DEBUG("Event type: " << event.GetType());
		PRINT_DEBUG("Event value: " << mValue);
		PRINT_DEBUG("Event phase: " << phaseStr);
		PRINT_DEBUG(
				"Event target element tag: " << event.GetTargetElement()->GetTagName());
		PRINT_DEBUG(
				"Event current element tag: " << event.GetTargetElement()->GetTagName());
		int pos = 0;
		string key, valueAsStr;
		for (auto item : event.GetParameters())
		{
			auto key = item.first;
			auto value = item.second;
			valueAsStr = value.Get<string>();
			PRINT_DEBUG(
					"Event parameter: pos = " << pos++ << " key = \"" << key << "\"" << " value = " << valueAsStr);
		}
		PRINT_DEBUG("");
	}
#endif // ELY_DEBUG

	if (mValue == "MAIN::ENTER_GAME")
	{
		//call all registered commit functions
		for (auto func : GameGUIManager::get_global_ptr()->mGuiCommitFunctions)
		{
#ifdef PYTHON_BUILD
			// call python func
			PY_ZERO_ARGS_CALL(func,
					string(
							" MainEventListener.ProcessEvent(): Error calling '"),
					string("' \"commit\" function"));
#else
			// call c++ func
			func();
#endif //PYTHON_BUILD
		}
		//presets & commits are not being executed by main menu any more
		GameGUIManager::get_global_ptr()->set_gui_main_presets_commits(false);
		//close (i.e. unload) the main document and set as closed..
		GameGUIManager::get_global_ptr()->mGuiMainMenu.close();
	}
	else if (mValue == "MAIN::EXIT_GAME")
	{
		GameGUIManager::get_global_ptr()->show_exit_menu();
	}
	else if (mValue == "EXIT::SUBMIT_EXIT")
	{
		string paramValue;
		//check if ok or cancel
		paramValue = event.GetParameter<string>("submit", "cancel");
		//close (i.e. unload) the exit menu and set as closed..
		GameGUIManager::get_global_ptr()->mGuiExitMenu.close();
		if (paramValue == "ok")
		{
			//user wants to exit: unload all documents
			GameGUIManager::get_global_ptr()->mGuiRmlUiContext.unload_all_documents();
			// call on exit function
			if (GameGUIManager::get_global_ptr()->mGuiOnExitFunction)
			{
#ifdef PYTHON_BUILD
				// call python func
				PY_ZERO_ARGS_CALL(
						GameGUIManager::get_global_ptr()->mGuiOnExitFunction,
						string(
								" MainEventListener.ProcessEvent(): Error calling '"),
						string("' \"on exit\" function"));
#else
				// call c++ func
				GameGUIManager::get_global_ptr()->mGuiOnExitFunction();
#endif //PYTHON_BUILD
			}
		}
		//if presets & commits are not being executed by main menu
		if (not GameGUIManager::get_global_ptr()->get_gui_main_presets_commits())
		{
			//call all registered commit functions
			for (auto func : GameGUIManager::get_global_ptr()->mGuiCommitFunctions)
			{
#ifdef PYTHON_BUILD
				// call python func
				PY_ZERO_ARGS_CALL(func,
						string(
								" MainEventListener.ProcessEvent(): Error calling '"),
						string("' \"commit\" function"));
#else
				// call c++ func
				func();
#endif //PYTHON_BUILD
			}
		}
	}
	else
	{
		//check if it is a registered event name
		if (GameGUIManager::get_global_ptr()->mGuiEventHandlers.find(mValue)
				!= GameGUIManager::get_global_ptr()->mGuiEventHandlers.end())
		{
			//call the registered event handler
			auto func =
					GameGUIManager::get_global_ptr()->mGuiEventHandlers[mValue];
#ifdef PYTHON_BUILD
			// call python func
			P3GUIEventPtr p3guiEvent(&event);
			string value(mValue);
			PY_ONE_STR_ARGS_CALL(func, (void*) &p3guiEvent, value,
					Dtool_P3GUIEventPtr,
					string(
							" MainEventListener.ProcessEvent(): Error calling '"),
					string("' \"event handler\" function"));
#else
			// call c++ func
			func(P3GUIEventPtr(&event), mValue);
#endif //PYTHON_BUILD
		}
	}
}

TYPED_OBJECT_API_DEF(GameGUIManager)
