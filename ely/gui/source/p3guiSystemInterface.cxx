/**
 * PANDA 3D SOFTWARE
 * Copyright (c) Carnegie Mellon University.  All rights reserved.
 *
 * All use of this software is subject to the terms of the revised BSD
 * license.  You should have received a copy of this license along
 * with this source code in a file named "LICENSE."
 *
 * @file rocketSystemInterface.cxx
 * @author rdb
 * @date 2011-11-03
 */
/**
 * \file p3guiSystemInterface.cxx
 *
 * \date 2019-10-03
 * \author consultit
 */

#include "p3guiSystemInterface.h"
#include "clockObject.h"

/**
 * Get the number of seconds elapsed since the start of the application.
 */
double P3GUISystemInterface::
GetElapsedTime() {
  ClockObject *clock = ClockObject::get_global_clock();
  // XXX not sure exactly how RmlUi uses uses it, maybe get_frame_time is
  // better?
  return clock->get_real_time();
}

/**
 * Log the specified message.  Returns true to continue execution, false to
 * break into the debugger.
 */
bool P3GUISystemInterface::
LogMessage(Rml::Log::Type type, const string& message) {
  switch(type) {
  case Rml::Log::LT_ALWAYS:
  case Rml::Log::LT_ERROR:
  case Rml::Log::LT_ASSERT:
    p3gui_cat->error() << message << "\n";
    return true;
  case Rml::Log::LT_WARNING:
    p3gui_cat->warning() << message << "\n";
    return true;
  case Rml::Log::LT_INFO:
    p3gui_cat->info() << message << "\n";
    return true;
  case Rml::Log::LT_DEBUG:
    p3gui_cat->debug() << message << "\n";
    return true;
  case Rml::Log::LT_MAX:
    // Not really sent; just to keep compiler happy
    break;
  }
  return true;
}
