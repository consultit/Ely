/**
 * \file gameGUIManager.I
 *
 * \date 2019-10-09
 * \author consultit
 */

#ifndef GUI_SOURCE_GAMEGUIMANAGER_I_
#define GUI_SOURCE_GAMEGUIMANAGER_I_

///GamePhysicsManager inline definitions

/**
 * Returns the singleton pointer.
 */
INLINE GameGUIManager* GameGUIManager::get_global_ptr()
{
	return Singleton < GameGUIManager > ::GetSingletonPtr();
}

/**
 * Returns the main menu document.
 */
INLINE P3GUIElementDocumentPtr GameGUIManager::get_main_menu() const
{
	return mGuiMainMenu;
}

/**
 * Returns the exit menu document.
 */
INLINE P3GUIElementDocumentPtr GameGUIManager::get_exit_menu() const
{
	return mGuiExitMenu;
}

/**
 * Sets gui main menu document path.
 */
INLINE void GameGUIManager::set_gui_main_menu_path(const string &path)
{
	mGuiMainMenuPath = path;
}

/**
 * Returns gui main menu document path.
 */
INLINE string GameGUIManager::get_gui_main_menu_path() const
{
	return mGuiMainMenuPath;
}

/**
 * Sets gui exit menu document path.
 */
INLINE void GameGUIManager::set_gui_exit_menu_path(const string &path)
{
	mGuiExitMenuPath = path;
}

/**
 * Returns gui exit menu document path.
 */
INLINE string GameGUIManager::get_gui_exit_menu_path() const
{
	return mGuiExitMenuPath;
}

INLINE P3GUIContextPtr GameGUIManager::get_rml_context() const
{
	return mGuiRmlUiContext;
}

/**
 * Sets the gui fonts path list.
 */
INLINE void GameGUIManager::set_gui_font_paths(const ValueList_string &paths)
{
	mGuiFontPaths = paths;
}

/**
 * Sets the gui fonts path list.
 */
INLINE ValueList_string GameGUIManager::get_gui_font_paths() const
{
	return mGuiFontPaths;
}

/**
 * Sets gui main presets commits flag.
 */
INLINE void GameGUIManager::set_gui_main_presets_commits(bool value)
{
	mGuiMainPresetsCommits = value;
}

/**
 * Returns gui main presets commits flag.
 */
INLINE bool GameGUIManager::get_gui_main_presets_commits() const
{
	return mGuiMainPresetsCommits;
}

/**
 *
 */
inline int GameGUIManager::unique_ref()
{
	return ++mRef;
}

/**
 * Enables/disables debug drawing.
 */
INLINE void GameGUIManager::debug(bool enable)
{
#ifdef ELY_DEBUG
	mDebugEnabled = enable;
#endif // ELY_DEBUG
}

#endif /* GUI_SOURCE_GAMEGUIMANAGER_I_ */
