/**
 * \file p3guiObjects.h
 *
 * \date 2019-10-05
 * \author consultit
 */

#ifndef GUI_SOURCE_P3GUIOBJECTS_H_
#define GUI_SOURCE_P3GUIOBJECTS_H_

#include "config_module.h"
#include "p3gui_includes.h"
#include "p3guiRegion.h"
#include "p3guiElements.h"
#include "p3guiDataStructures.h"

#ifndef CPPPARSER
#include "RmlUi/Include/RmlUi/Core.h"
#endif //CPPPARSER

/// P3GUIContextPtr
class EXPCL_P3GUI P3GUIContextPtr
{
PUBLISHED:
	INLINE P3GUIContextPtr(PT(P3GUIRegion) region);
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIContextPtr();
#endif //CPPPARSER
	INLINE bool is_empty() const;
	MAKE_PROPERTY(empty, is_empty);

	INLINE P3GUIElementDocumentPtr get_document(const string &name);
	INLINE P3GUIElementDocumentPtr load_document(const string &name);
	INLINE void unload_all_documents();

public:
	inline P3GUIContextPtr();
	inline P3GUIContextPtr(Rml::Context *_rmlObject);
	inline Rml::Context& get_rml_object();
	Rml::Context *rmlObject;
};

/// P3GUIFactory
class EXPCL_P3GUI P3GUIFactory
{
PUBLISHED:
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIFactory();
#endif //CPPPARSER

///	INLINE static ContextInstancer* RegisterContextInstancer(ContextInstancer* instancer);
///	INLINE static Context* InstanceContext(const String& name);
///	INLINE static ElementInstancer* RegisterElementInstancer(const String& name, ElementInstancer* instancer);
///	INLINE static ElementInstancer* GetElementInstancer(const String& tag);
	INLINE static P3GUIElementOwnerPtr instance_element(P3GUIElementPtr parent, const string& instancer, const string& tag, const P3GUIXMLAttributes& attributes);
	INLINE static bool instance_element_text(P3GUIElementPtr parent, const string& text);
///	INLINE static bool InstanceElementStream(Element* parent, Stream* stream);
///	INLINE static ElementDocument* InstanceDocumentStream(Rml::Context* context, Stream* stream);
///	INLINE static DecoratorInstancer* RegisterDecoratorInstancer(const String& name, DecoratorInstancer* instancer);
///	INLINE static Decorator* InstanceDecorator(const String& name, const PropertyDictionary& properties);
///	INLINE static FontEffectInstancer* RegisterFontEffectInstancer(const String& name, FontEffectInstancer* instancer);
///	INLINE static FontEffect* InstanceFontEffect(const String& name, const PropertyDictionary& properties);
///	INLINE static StyleSheet* InstanceStyleSheetString(const String& string);
///	INLINE static StyleSheet* InstanceStyleSheetFile(const String& file_name);
///	INLINE static StyleSheet* InstanceStyleSheetStream(Stream* stream);
///	INLINE static void ClearStyleSheetCache();
///	INLINE static void ClearTemplateCache();
///	INLINE static EventInstancer* RegisterEventInstancer(EventInstancer* instancer);
///	INLINE static Event* InstanceEvent(Element* target, const String& name, const Dictionary& parameters, bool interruptible);
///	INLINE static EventListenerInstancer* RegisterEventListenerInstancer(EventListenerInstancer* instancer);
///	INLINE static EventListener* InstanceEventListener(const String& value, Element* element);
};

/// inline definitions
#include "p3guiObjects.I"

#endif /* GUI_SOURCE_P3GUIOBJECTS_H_ */
