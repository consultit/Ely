/**
 * \file p3guiDataStructures.I
 *
 * \date 2019-10-09
 * \author consultit
 */

#ifndef GUI_SOURCE_P3GUIDATASTRUCTURES_I_
#define GUI_SOURCE_P3GUIDATASTRUCTURES_I_

/// P3GUIDictionary inline definitions
INLINE P3GUIDictionary::P3GUIDictionary()
{
}
INLINE void P3GUIDictionary::clear()
{
	rmlObject.clear();
}
INLINE bool P3GUIDictionary::is_empty() const
{
	return rmlObject.empty();
}
INLINE int P3GUIDictionary::size() const
{
	return rmlObject.size();
}
INLINE void P3GUIDictionary::set_string(const string &key, const string &value)
{
	rmlObject[key] = value;
}

/// P3GUIVariantPtr
INLINE P3GUIVariantPtr::P3GUIVariantPtr() :
		rmlObject(nullptr)
{
}
inline P3GUIVariantPtr::P3GUIVariantPtr(Rml::Variant *_rmlObject) :
		rmlObject(_rmlObject)
{
}
inline Rml::Variant& P3GUIVariantPtr::get_rml_object()
{
	return *rmlObject;
}
INLINE bool P3GUIVariantPtr::is_empty() const
{
	return rmlObject == nullptr;
}

#endif /* GUI_SOURCE_P3GUIDATASTRUCTURES_I_ */
