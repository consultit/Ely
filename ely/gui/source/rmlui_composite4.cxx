/**
 * \file rmlui_composite4.cxx
 *
 * \date 2019-10-03
 * \author consultit
 */

///library
#include "RmlUi/Source/Core/StyleSheetFactory.cpp"
#include "RmlUi/Source/Core/DataController.cpp"
#include "RmlUi/Source/Core/DataControllerDefault.cpp"
#include "RmlUi/Source/Core/DataExpression.cpp"
#include "RmlUi/Source/Core/DataModel.cpp"
#include "RmlUi/Source/Core/DataTypeRegister.cpp"
#include "RmlUi/Source/Core/DataVariable.cpp"
#include "RmlUi/Source/Core/DataView.cpp"
#include "RmlUi/Source/Core/DataViewDefault.cpp"

///support
