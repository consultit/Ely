/**
 * \file p3gui_includes.h
 *
 * \date 2019-10-02
 * \author consultit
 */

#ifndef GUI_SOURCE_P3GUI_INCLUDES_H_
#define GUI_SOURCE_P3GUI_INCLUDES_H_

#include "p3guisymbols.h"

#include "../../common/source/commonTools.h"
#include "../../common/source/commonMacros.h"

#include "pairs_valueLists.h"
#include "utilities.h"

#ifdef CPPPARSER
//Panda3d interrogate fake declarations

struct MainEventListenerInstancer;

namespace Rml
{
	struct RenderInterface;
	struct Vector2f;
	struct Vector2i;
	struct TextureHandle;
	struct CompiledGeometryHandle;
	struct String;
	struct byte;
	struct FileHandle;
	struct Element;
	struct ElementPtr;
	struct ElementDocument;
	struct Dictionary;
	struct XMLAttributes;
	struct Variant;
	namespace Log
	{
		struct Type;
	}
	struct ElementFormControl;
	struct ElementFormControlSelect;
	struct ElementFormControlInput;
} // namespace Rml

#endif //CPPPARSER

#endif /* GUI_SOURCE_P3GUI_INCLUDES_H_ */
