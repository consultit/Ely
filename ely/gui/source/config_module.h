#ifndef GUI_SOURCE_CONFIG_MODULE_H_
#define GUI_SOURCE_CONFIG_MODULE_H_

#pragma once

#include "p3guisymbols.h"
#include "pandabase.h"
#include "notifyCategoryProxy.h"
#include "configVariableDouble.h"
#include "configVariableString.h"
#include "configVariableInt.h"

NotifyCategoryDecl(p3gui, EXPCL_P3GUI, EXPTP_P3GUI);

extern void init_libp3gui();

#endif //GUI_SOURCE_CONFIG_MODULE_H_
