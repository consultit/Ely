/**
 * PANDA 3D SOFTWARE
 * Copyright (c) Carnegie Mellon University.  All rights reserved.
 *
 * All use of this software is subject to the terms of the revised BSD
 * license.  You should have received a copy of this license along
 * with this source code in a file named "LICENSE."
 *
 * @file rocketSystemInterface.h
 * @author rdb
 * @date 2011-11-03
 */
/**
 * \file p3guiSystemInterface.h
 *
 * \date 2019-10-03
 * \author consultit
 */

#ifndef P3GUI_SYSTEM_INTERFACE_H
#define P3GUI_SYSTEM_INTERFACE_H

#include "config_module.h"
#include "p3gui_includes.h"

#ifndef CPPPARSER
#include "RmlUi/Include/RmlUi/Core/SystemInterface.h"
#include "RmlUi/Include/RmlUi/Core/Log.h"
#endif //CPPPARSER

/**
 * This is an implementation of SystemInterface that redirects the log output
 * to Panda's notify system.
 */
class P3GUISystemInterface : public Rml::SystemInterface {
public:
  double GetElapsedTime();
  bool LogMessage(Rml::Log::Type type, const string& message);
};

#endif
