/**
 * PANDA 3D SOFTWARE
 * Copyright (c) Carnegie Mellon University.  All rights reserved.
 *
 * All use of this software is subject to the terms of the revised BSD
 * license.  You should have received a copy of this license along
 * with this source code in a file named "LICENSE."
 *
 * @file rocketRenderInterface.h
 * @author rdb
 * @date 2011-11-04
 */
/**
 * \file p3guiRenderInterface.h
 *
 * \date 2019-10-03
 * \author consultit
 */

#ifndef P3GUI_RENDER_INTERFACE_H
#define P3GUI_RENDER_INTERFACE_H

#include "config_module.h"
#include "p3gui_includes.h"
#include "cullTraverser.h"
#include "cullTraverserData.h"
#include "geom.h"
#include "renderState.h"
#include "transformState.h"

#ifndef CPPPARSER
#include "RmlUi/Include/RmlUi/Core/RenderInterface.h"
#endif //CPPPARSER

/**
 * Class that provides the main render interface for RmlUi integration.
 */
class P3GUIRenderInterface : public Rml::RenderInterface {
public:
  void render(Rml::Context* context, CullTraverser *trav);

protected:
  struct CompiledGeometry {
    CPT(Geom) _geom;
    CPT(RenderState) _state;
  };

  PT(Geom) make_geom(Rml::Vertex* vertices,
                     int num_vertices, int* indices, int num_indices,
                     GeomEnums::UsageHint uh, const LVecBase2 &tex_scale);
  void render_geom(const Geom* geom, const RenderState* state, const Rml::Vector2f& translation);

  void RenderGeometry(Rml::Vertex* vertices, int num_vertices, int* indices, int num_indices, Rml::TextureHandle texture, const Rml::Vector2f& translation);
  Rml::CompiledGeometryHandle CompileGeometry(Rml::Vertex* vertices, int num_vertices, int* indices, int num_indices, Rml::TextureHandle texture);
  void RenderCompiledGeometry(Rml::CompiledGeometryHandle geometry, const Rml::Vector2f& translation);
  void ReleaseCompiledGeometry(Rml::CompiledGeometryHandle geometry);

  bool LoadTexture(Rml::TextureHandle& texture_handle,
                   Rml::Vector2i& texture_dimensions,
                   const string& source);
  bool GenerateTexture(Rml::TextureHandle& texture_handle,
                       const Rml::byte* source,
                       const Rml::Vector2i& source_dimensions);
  void ReleaseTexture(Rml::TextureHandle texture_handle);

  void EnableScissorRegion(bool enable);
  void SetScissorRegion(int x, int y, int width, int height);

private:
  Mutex _lock;

  // Hold the scissor settings and whether or not to enable scissoring.
  bool _enable_scissor;
  LVecBase4 _scissor;

  // These are temporarily filled in by render().
  CullTraverser *_trav;
  CPT(TransformState) _net_transform;
  CPT(RenderState) _net_state;
  Rml::Vector2i _dimensions;

};

#endif
