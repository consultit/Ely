/**
 * \file p3guiElements.h
 *
 * \date 2019-10-06
 * \author consultit
 */

#ifndef GUI_SOURCE_P3GUIELEMENTS_H_
#define GUI_SOURCE_P3GUIELEMENTS_H_

#include "config_module.h"
#include "p3gui_includes.h"
#include "p3guiRegion.h"
#include "p3guiDataStructures.h"

#ifndef CPPPARSER
#include "RmlUi/Include/RmlUi/Core.h"
#endif //CPPPARSER
class P3GUIElementDocumentPtr;
class P3GUIElementPtr;

/// P3GUIElementOwnerPtr
class EXPCL_P3GUI P3GUIElementOwnerPtr
{
PUBLISHED:
	INLINE P3GUIElementOwnerPtr(const P3GUIElementOwnerPtr & other);
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIElementOwnerPtr();
#endif //CPPPARSER

public:
	inline P3GUIElementOwnerPtr(Rml::ElementPtr &&_rmlObject);
	Rml::ElementPtr rmlObject;
};

/// P3GUIElementPtr
class EXPCL_P3GUI P3GUIElementPtr
{
PUBLISHED:
	INLINE P3GUIElementPtr();
	INLINE P3GUIElementPtr(const P3GUIElementOwnerPtr & other);
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIElementPtr();
#endif //CPPPARSER

	INLINE bool is_empty() const;
	MAKE_PROPERTY(empty, is_empty);

	INLINE void set_inner_rml(const string &name);
	INLINE string get_property_string(const string &name);
	INLINE bool set_property(const string &name, const string &value);
	INLINE void set_attribute_string(const string &name, const string &value);
	INLINE string get_attribute_string(const string &name,
			const string &default_value) const;
	INLINE void set_attribute_bool(const string &name, bool value);
	INLINE bool get_attribute_bool(const string &name,
			bool default_value) const;
	INLINE void set_attribute_float(const string &name, float value);
	INLINE float get_attribute_float(const string &name,
			float default_value) const;
	INLINE P3GUIVariantPtr get_attribute(const string &name) const;
	INLINE bool has_attribute(const string &name);
	INLINE void remove_attribute(const string &name);
	INLINE P3GUIElementPtr insert_before(P3GUIElementOwnerPtr child,
			P3GUIElementPtr adjacent_element);
	INLINE P3GUIElementPtr get_element_by_id(const string &name);
	INLINE P3GUIElementDocumentPtr get_owner_document();
	INLINE P3GUIElementPtr get_parent_node() const;
	MAKE_PROPERTY(owner_document, get_owner_document);
	MAKE_PROPERTY(parent_node, get_parent_node);

public:
	inline P3GUIElementPtr(Rml::Element *_rmlObject);
	inline Rml::Element& get_rml_object();
	Rml::Element *rmlObject;
};

/// P3GUIElementDocumentPtr
BEGIN_PUBLISH
enum FocusFlag
{
#ifndef CPPPARSER
	f_NONE = static_cast<uint8_t>(Rml::FocusFlag::None),
	f_DOCUMENT = static_cast<uint8_t>(Rml::FocusFlag::Document),
	f_KEEP = static_cast<uint8_t>(Rml::FocusFlag::Keep),
	f_AUTO = static_cast<uint8_t>(Rml::FocusFlag::Auto)
#else
	f_NONE, f_DOCUMENT, f_KEEP, f_AUTO
#endif //CPPPARSER
};

enum ModalFlag
{
#ifndef CPPPARSER
	m_NONE = static_cast<uint8_t>(Rml::ModalFlag::None),
	m_MODAL = static_cast<uint8_t>(Rml::ModalFlag::Modal),
	m_KEEP = static_cast<uint8_t>(Rml::ModalFlag::Keep)
#else
	m_NONE, m_MODAL, m_KEEP
#endif //CPPPARSER
};
END_PUBLISH
class EXPCL_P3GUI P3GUIElementDocumentPtr: public P3GUIElementPtr
{
PUBLISHED:
	INLINE P3GUIElementDocumentPtr(P3GUIElementPtr element);
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIElementDocumentPtr();
#endif //CPPPARSER

	INLINE string get_title() const;
	MAKE_PROPERTY(title, get_title);
	INLINE void show(ModalFlag modal_flag=m_KEEP, FocusFlag focus_flag = f_AUTO);
	INLINE void hide();
	INLINE void close();

public:
	inline P3GUIElementDocumentPtr();
	inline Rml::ElementDocument& get_rml_object();
};

/// P3GUIElementFormControlPtr
class EXPCL_P3GUI P3GUIElementFormControlPtr: public P3GUIElementPtr
{
PUBLISHED:
	INLINE P3GUIElementFormControlPtr(P3GUIElementPtr element);
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIElementFormControlPtr();
#endif //CPPPARSER

	INLINE void set_disabled(bool disable);

public:
	inline P3GUIElementFormControlPtr();
	inline Rml::ElementFormControl& get_rml_object();
};

/// P3GUIElementFormControlInputPtr
class EXPCL_P3GUI P3GUIElementFormControlInputPtr: public P3GUIElementFormControlPtr
{
PUBLISHED:
	INLINE P3GUIElementFormControlInputPtr(P3GUIElementPtr element);
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIElementFormControlInputPtr();
#endif //CPPPARSER

	INLINE void set_value(const string &value);
	INLINE string get_value() const;
	MAKE_PROPERTY(value, get_value, set_value);

public:
	inline P3GUIElementFormControlInputPtr();
	inline Rml::ElementFormControlInput& get_rml_object();
};

/// P3GUIElementFormControlSelectPtr
class EXPCL_P3GUI P3GUIElementFormControlSelectPtr: public P3GUIElementFormControlPtr
{
PUBLISHED:
	INLINE P3GUIElementFormControlSelectPtr(P3GUIElementPtr element);
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIElementFormControlSelectPtr();
#endif //CPPPARSER

	INLINE int add(const string &rml, const string &value, int before = -1,
			bool selectable = true);
	INLINE void remove(int index);
	INLINE void remove_all();
	INLINE void set_selection(int selection);

public:
	inline P3GUIElementFormControlSelectPtr();
	inline Rml::ElementFormControlSelect& get_rml_object();
};

/// inline definitions
#include "p3guiElements.I"

#endif /* GUI_SOURCE_P3GUIELEMENTS_H_ */
