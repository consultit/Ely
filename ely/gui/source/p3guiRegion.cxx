/**
 * PANDA 3D SOFTWARE
 * Copyright (c) Carnegie Mellon University.  All rights reserved.
 *
 * All use of this software is subject to the terms of the revised BSD
 * license.  You should have received a copy of this license along
 * with this source code in a file named "LICENSE."
 *
 * @file rocketRegion.cxx
 * @author rdb
 * @date 2011-11-30
 */
/**
 * \file p3guiRegion.cxx
 *
 * \date 2019-10-03
 * \author consultit
 */

#include "p3guiRegion.h"
#include "graphicsOutput.h"
#include "orthographicLens.h"
#include "pStatTimer.h"

#ifndef CPPPARSER
#include "RmlUi/Include/RmlUi/Core.h"
#endif //CPPPARSER

#if defined(ELY_DEBUG) && !defined(CPPPARSER)
#include "RmlUi/Include/RmlUi/Debugger.h"
#endif

#ifdef BUILD_PYTHON
#include "py_panda.h"
#endif

TypeHandle P3GUIRegion::_type_handle;

/**
 * Make sure that context_name is unique.
 */
P3GUIRegion::
P3GUIRegion(GraphicsOutput *window, const LVecBase4 &dr_dimensions,
             const std::string &context_name) :
  DisplayRegion(window, dr_dimensions) {

  // A hack I don't like.  RmlUi's decorator system has a bug somewhere,
  // and this seems to be a workaround.
  if (Rml::GetRenderInterface() == nullptr) {
    Rml::SetRenderInterface(&_interface);
  }

  int pl, pr, pb, pt;
  get_pixels(pl, pr, pb, pt);
  Rml::Vector2i dimensions (pr - pl, pt - pb);

  if (p3gui_cat.is_debug()) {
    p3gui_cat.debug()
      << "Setting initial context dimensions to ("
      << dimensions.x << ", " << dimensions.y << ")\n";
  }

  _context = Rml::CreateContext(context_name,
                                         dimensions, &_interface);
  nassertv(_context != nullptr);

  _lens = new OrthographicLens;
  _lens->set_film_size(dimensions.x, -dimensions.y);
  _lens->set_film_offset(dimensions.x * 0.5, dimensions.y * 0.5);
  _lens->set_near_far(-1, 1);

  PT(Camera) cam = new Camera(context_name, _lens);
  set_camera(NodePath(cam));
}

/**
 *
 */
P3GUIRegion::
~P3GUIRegion() {
  if (Rml::GetRenderInterface() == &_interface) {
    Rml::SetRenderInterface(nullptr);
  }


  if (_context != nullptr) {

    // We need to do this because RmlUi may call into Python code to throw
    // destruction events.
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
    PyGILState_STATE gstate;
    gstate = PyGILState_Ensure();
#endif // PYTHON_BUILD

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
    PyGILState_Release(gstate);
#endif // PYTHON_BUILD
  }
}

/**
 * Performs a cull traversal for this region.
 */
void P3GUIRegion::
do_cull(CullHandler *cull_handler, SceneSetup *scene_setup,
        GraphicsStateGuardian *gsg, Thread *current_thread) {

  PStatTimer timer(get_cull_region_pcollector(), current_thread);

  // We (unfortunately) need to do this because RmlUi may call into Python
  // code to throw events.
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
  PyGILState_STATE gstate;
  gstate = PyGILState_Ensure();
#endif // PYTHON_BUILD

  int pl, pr, pb, pt;
  get_pixels(pl, pr, pb, pt);
  Rml::Vector2i dimensions (pr - pl, pt - pb);

  if (_context->GetDimensions() != dimensions) {
    if (p3gui_cat.is_debug()) {
      p3gui_cat.debug() << "Setting context dimensions to ("
        << dimensions.x << ", " << dimensions.y << ")\n";
    }

    _context->SetDimensions(dimensions);

    _lens->set_film_size(dimensions.x, -dimensions.y);
    _lens->set_film_offset(dimensions.x * 0.5, dimensions.y * 0.5);
  }

  if (_input_handler != nullptr) {
    _input_handler->update_context(_context, pl, pb);
  } else {
    _context->Update();
  }

  CullTraverser *trav = get_cull_traverser();
  trav->set_cull_handler(cull_handler);
  trav->set_scene(scene_setup, gsg, get_incomplete_render());
  trav->set_view_frustum(nullptr);

  _interface.render(_context, trav);

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
  PyGILState_Release(gstate);
#endif // PYTHON_BUILD

  trav->end_traverse();
}

/**
 * Initializes the RmlUi debugger.  This will return false if the debugger
 * failed to initialize, or if support for the debugger has not been built in
 * (for example in an optimize=4 build).
 */
bool P3GUIRegion::
init_debugger() {
#ifdef ELY_DEBUG
  return Rml::Debugger::Initialise(_context);
#else
  return false;
#endif
}

/**
 * Sets whether the debugger should be visible.
 */
void P3GUIRegion::
set_debugger_visible(bool visible) {
#ifdef ELY_DEBUG
  Rml::Debugger::SetVisible(visible);
#endif
}

/**
 * Returns true if the debugger is visible.
 */
bool P3GUIRegion::
is_debugger_visible() const {
#ifdef ELY_DEBUG
  return Rml::Debugger::IsVisible();
#else
  return false;
#endif
}
