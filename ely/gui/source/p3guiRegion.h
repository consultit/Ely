/**
 * PANDA 3D SOFTWARE
 * Copyright (c) Carnegie Mellon University.  All rights reserved.
 *
 * All use of this software is subject to the terms of the revised BSD
 * license.  You should have received a copy of this license along
 * with this source code in a file named "LICENSE."
 *
 * @file rocketRegion.h
 * @author rdb
 * @date 2011-11-30
 */
/**
 * \file p3guiRegion.h
 *
 * \date 2019-10-03
 * \author consultit
 */

#ifndef P3GUI_REGION_H
#define P3GUI_REGION_H

#include "config_module.h"
#include "p3gui_includes.h"
#include "p3guiRenderInterface.h"
#include "p3guiInputHandler.h"
#include "displayRegion.h"

class OrthographicLens;

/**
 * Represents a region in a window or buffer where the RmlUi UI will be
 * rendered to.
 */
class EXPCL_P3GUI P3GUIRegion : public DisplayRegion {
protected:
  P3GUIRegion(GraphicsOutput *window, const LVecBase4 &dimensions,
               const std::string &context_name);

  virtual void do_cull(CullHandler *cull_handler, SceneSetup *scene_setup,
                       GraphicsStateGuardian *gsg, Thread *current_thread);

PUBLISHED:
  virtual ~P3GUIRegion();

  INLINE static P3GUIRegion* make(const std::string &context_name,
                                   GraphicsOutput *window);
  INLINE static P3GUIRegion* make(const std::string &context_name,
                                   GraphicsOutput *window,
                                   const LVecBase4 &dimensions);
#ifndef CPPPARSER
  inline Rml::Context* get_context() const;
#endif

  INLINE void set_input_handler(P3GUIInputHandler *handler);
  INLINE P3GUIInputHandler *get_input_handler() const;
  MAKE_PROPERTY(input_handler, get_input_handler, set_input_handler);

  bool init_debugger();
  void set_debugger_visible(bool visible);
  bool is_debugger_visible() const;
  MAKE_PROPERTY(debugger_visible, is_debugger_visible, set_debugger_visible);

private:
  P3GUIRenderInterface _interface;
  Rml::Context* _context;
  PT(OrthographicLens) _lens;
  PT(P3GUIInputHandler) _input_handler;

public:
  static TypeHandle get_class_type() {
    return _type_handle;
  }
  static void init_type() {
    DisplayRegion::init_type();
    register_type(_type_handle, "P3GUIRegion",
                  DisplayRegion::get_class_type());
  }
  virtual TypeHandle get_type() const {
    return get_class_type();
  }
  virtual TypeHandle force_init_type() {init_type(); return get_class_type();}

private:
  static TypeHandle _type_handle;
};

#include "p3guiRegion.I"

#endif /* P3GUI_REGION_H */
