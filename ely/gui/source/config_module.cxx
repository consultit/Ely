#include "config_module.h"
#include "dconfig.h"
#include "pandaSystem.h"
#include "default_font.h"

#include "p3guiFileInterface.h"
#include "p3guiInputHandler.h"
#include "p3guiRegion.h"
#include "p3guiSystemInterface.h"
#include "gameGUIManager.h"

// This is defined by both Panda and RmlUi.
#define Factory RmlUiFactory
#include "Include/RmlUi/Core/Core.h"
#undef Factory

Configure(config_p3gui);
NotifyCategoryDef(p3gui, "");

ConfigureFn( config_p3gui )
{
	init_libp3gui();
}

/**
 * Initializes the library.  This must be called at least once before any of
 * the functions or classes in this library can be used.  Normally it will be
 * called by the static initializers and need not be called explicitly, but
 * special cases exist.
 */
void init_libp3gui()
{
	static bool initialized = false;
	if (initialized)
	{
		return;
	}
	initialized = true;

	// Init your dynamic types here, e.g.:
	P3GUIInputHandler::init_type();
	P3GUIRegion::init_type();
	GameGUIManager::init_type();

	if (p3gui_cat->is_debug())
	{
		p3gui_cat->debug() << "Initializing RmlUi library.\n";
	}

	return;
}

