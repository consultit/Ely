/**
 * \file gameGUIManager.h
 *
 * \date 2019-10-09
 * \author consultit
 */

#ifndef GUI_SOURCE_GAMEGUIMANAGER_H_
#define GUI_SOURCE_GAMEGUIMANAGER_H_

#include "config_module.h"
#include "p3gui_includes.h"
#include "p3guiObjects.h"
#include "p3guiEvent.h"
#include "p3guiFileInterface.h"
#include "p3guiSystemInterface.h"
#include "graphicsWindow.h"

#ifndef CPPPARSER
#include "RmlUi/Include/RmlUi/Core.h"

/**
 * Abstract class interface for GUI (RmlUi) event listeners.
 *
 * A concrete derived class must implement:
 * \code
 * virtual void ProcessEvent(Rml::Event& event);
 * \endcode
 */
class EventListener: public Rml::EventListener
{
public:
	EventListener(const string &value) :
			mValue(value)
	{
		PRINT_DEBUG("Creating EventListener: " << mValue << " " << this);
	}
	virtual ~EventListener()
	{
		PRINT_DEBUG("Destroying EventListener: " << mValue << " " << this);
	}

	virtual void OnAttach(Rml::Element*RMLUI_UNUSED(element))
	{
		PRINT_DEBUG("Attaching EventListener: " << mValue << " " << this);
	}

	virtual void OnDetach(Rml::Element*RMLUI_UNUSED(element))
	{
		PRINT_DEBUG("Detaching EventListener: " << mValue << " " << this);
	}

protected:
	virtual void OnReferenceDeactivate()
	{
		delete this;
	}

	string mValue;
};

/**
 * Abstract class interface for RmlUi event listener instancers.
 *
 * A concrete derived class must implement:
 * \code
 * virtual Rml::EventListener* InstanceEventListener(
 const string& value, Rml::Element* element);
 * \endcode
 * This interface is designed so that method would create a ely::EventListener
 * derived class.\n
 */
class EventListenerInstancer: public Rml::EventListenerInstancer
{
public:
	EventListenerInstancer()
	{
		PRINT_DEBUG("Creating EventListenerInstancer " << " " << this);
	}
	virtual ~EventListenerInstancer()
	{
		PRINT_DEBUG("Destroying EventListenerInstancer " << " " << this);
	}

	virtual void Release()
	{
		delete this;
	}

};

/**
 * Gui (RmlUi) event framework class.
 */
class MainEventListener: public EventListener
{
public:
	MainEventListener(const string &value);

	virtual ~MainEventListener();

	virtual void ProcessEvent(Rml::Event &event);

};

class MainEventListenerInstancer: public EventListenerInstancer
{
public:
	MainEventListenerInstancer()
	{
	}
	virtual ~MainEventListenerInstancer()
	{
	}
	virtual Rml::EventListener* InstanceEventListener(const string &value,
			Rml::Element *element)
	{
		return new MainEventListener(value);
	}
};

#endif // CPPPARSER

/**
 * GameGUIManager Singleton class.
 *
 * To use this RmlUi based framework, these documents must be
 * supplied:
 * - "main_menu" that responds, at least, to "MAIN::ENTER_GAME",
 * "MAIN::EXIT_GAME" events
 * - "exit_menu" that responds, at least, to "EXIT::SUBMIT_EXIT" events
 *
 * On "MAIN::ENTER_GAME", all "commits functions" are executed.
 * On "MAIN::EXIT_GAME" the "exit_menu" will be shown.
 * On "EXIT::SUBMIT_EXIT"
 *
 */
class EXPCL_P3GUI GameGUIManager: public TypedReferenceCount, public Singleton<
		GameGUIManager>
{
PUBLISHED:

	GameGUIManager(PT(GraphicsWindow) win, NodePath mw);
	virtual ~GameGUIManager();

	/**
	 * \name GUI (RmlUi) MAIN/EXIT MENUS
	 */
	///@{
	void show_main_menu();
	void show_exit_menu();
	INLINE P3GUIElementDocumentPtr get_main_menu() const;
	INLINE P3GUIElementDocumentPtr get_exit_menu() const;
	INLINE void set_gui_main_menu_path(const string &path);
	INLINE string get_gui_main_menu_path() const;
	INLINE void set_gui_exit_menu_path(const string &path);
	INLINE string get_gui_exit_menu_path() const;
#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	void set_gui_on_exit_function(PyObject *func);
#else
	typedef void (*ON_EXIT_FUNC)();
	void set_gui_on_exit_function(ON_EXIT_FUNC func);
#endif //PYTHON_BUILD
	MAKE_PROPERTY(main_menu, get_main_menu);
	MAKE_PROPERTY(exit_menu, get_exit_menu);
	MAKE_PROPERTY(gui_main_menu_path, get_gui_main_menu_path, set_gui_main_menu_path);
	MAKE_PROPERTY(gui_exit_menu_path, get_gui_exit_menu_path, set_gui_exit_menu_path);
	///@}

	/**
	 * \name GUI (RmlUi) RELATED VARIABLES' GETTERS/SETTERS
	 */
	///@{
	INLINE P3GUIContextPtr get_rml_context() const;
	INLINE void set_gui_font_paths(const ValueList_string &paths);
	INLINE ValueList_string get_gui_font_paths() const;
	INLINE void set_gui_main_presets_commits(bool value);
	INLINE bool get_gui_main_presets_commits() const;
	// Python Properties
	MAKE_PROPERTY(rml_context, get_rml_context);
	MAKE_PROPERTY(gui_font_paths, get_gui_font_paths, set_gui_font_paths);
	MAKE_PROPERTY(gui_main_presets_commits, get_gui_main_presets_commits, set_gui_main_presets_commits);
	///@}

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
	/**
	 * \name PYTHON GUI (RmlUi) REGISTERED FUNCTIONS
	 */
	///@{
	void register_gui_add_elements_function(PyObject *func);
	void register_gui_event_handler(const string &name, PyObject *func);
	PyObject* get_gui_event_handler(const string &name);
	void register_gui_preset_function(PyObject *func);
	void register_gui_commit_function(PyObject *func);
	///@}
#else
	/**
	 * \name C++ GUI (RmlUi) REGISTERED FUNCTIONS
	 */
	///@{
	typedef void (*ADD_ELEMENT_FUNC)(P3GUIElementDocumentPtr);
	void register_gui_add_elements_function(ADD_ELEMENT_FUNC func);
	typedef void (*EVENT_HANDLER)(P3GUIEventPtr, const string&);
	void register_gui_event_handler(const string &name, EVENT_HANDLER func);
	EVENT_HANDLER get_gui_event_handler(const string &name);
	typedef void (*PRESET_FUNC)();
	void register_gui_preset_function(PRESET_FUNC func);
	typedef void (*COMMIT_FUNC)();
	void register_gui_commit_function(COMMIT_FUNC func);
	///@}
#endif //PYTHON_BUILD

	/**
	 * \name GUI SETUP.
	 */
	///@{
	virtual void gui_setup();
	virtual void gui_cleanup();
	///@}

	/**
	 * \name SINGLETON
	 */
	///@{
	INLINE static GameGUIManager* get_global_ptr();
	///@}

	/**
	 * \name DEBUG
	 */
	///@{
	INLINE void debug(bool enable);
	///@}

public:
	///Unique ref producer.
	inline int unique_ref();

#ifndef CPPPARSER
private:
	friend class MainEventListener;

	///The reference graphic window.
	PT(GraphicsWindow) mWin;
	///The mouse watcher node path.
	NodePath mMw;
	///The RmlUi's event listener instancer.
	MainEventListenerInstancer *mEventListenerInstancer;
	/// RmlUi objects;
	P3GUIContextPtr mGuiRmlUiContext;
	P3GUIElementDocumentPtr mGuiMainMenu;
	P3GUIElementDocumentPtr mGuiExitMenu;
	string mGuiMainMenuPath;
	string mGuiExitMenuPath;
	ValueList_string mGuiFontPaths;
	PT(P3GUIRegion) mRegion;
	bool mSetup, mCleanup;
#if defined(PYTHON_BUILD)
	PyObject *mGuiOnExitFunction;
#else
	ON_EXIT_FUNC mGuiOnExitFunction;
#endif //PYTHON_BUILD
	bool mGuiMainPresetsCommits;
#if defined(PYTHON_BUILD)
	/// Python - Registering functions data storages
	//registered by subsystems to add their element (tags) to main menu
	pvector<PyObject*> mGuiAddElementsFunctions;
	//registered by subsystems to handle their events
	pmap<string, PyObject*> mGuiEventHandlers;
	//registered by some subsystems that need to preset themselves before
	//main/exit menus are shown
	pvector<PyObject*> mGuiPresetFunctions;
	//registered by some subsystems that need to commit their changes after
	//main/exit menus are closed
	pvector<PyObject*> mGuiCommitFunctions;
#else
	/// C++ - Registering functions data storages
	//registered by subsystems to add their element (tags) to main menu
	pvector<ADD_ELEMENT_FUNC> mGuiAddElementsFunctions;
	//registered by subsystems to handle their events
	pmap<string, EVENT_HANDLER> mGuiEventHandlers;
	//registered by some subsystems that need to preset themselves before
	//main/exit menus are shown
	pvector<PRESET_FUNC> mGuiPresetFunctions;
	//registered by some subsystems that need to commit their changes after
	//main/exit menus are closed
	pvector<COMMIT_FUNC> mGuiCommitFunctions;
#endif //PYTHON_BUILD

	///References to interfaces.
	P3GUIFileInterface *mFi;
	P3GUISystemInterface *mSi;

	///Unique ref.
	int mRef;

	///helper
	void do_reset();

#ifdef ELY_DEBUG
	friend class MainEventListener;
	///Debug enabled flag;
	bool mDebugEnabled;
#endif // ELY_DEBUG

#endif //CPPPARSER

TYPED_OBJECT_API_DECL(GameGuiManager,TypedReferenceCount)
};

///inline
#include "gameGUIManager.I"

#endif /* GUI_SOURCE_GAMEGUIMANAGER_H_ */
