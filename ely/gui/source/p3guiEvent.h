/**
 * \file p3guiEvent.h
 *
 * \date 2019-10-06
 * \author consultit
 */

#ifndef GUI_SOURCE_P3GUIEVENT_H_
#define GUI_SOURCE_P3GUIEVENT_H_

#include "config_module.h"
#include "p3gui_includes.h"
#include "p3guiRegion.h"
#include "p3guiElements.h"

#ifndef CPPPARSER
#include "RmlUi/Include/RmlUi/Core.h"
#endif //CPPPARSER

/// P3GUIEventPtr
class EXPCL_P3GUI P3GUIEventPtr
{
PUBLISHED:
	INLINE P3GUIEventPtr();
	// To avoid interrogatedb warning.
#ifdef CPPPARSER
	INLINE virtual ~P3GUIEventPtr();
#endif //CPPPARSER

	INLINE string get_parameter_string(const string &key,
			const string &default_value);
	INLINE float get_parameter_float(const string &key, float default_value);
	INLINE P3GUIElementPtr get_target_element() const;
	MAKE_PROPERTY(target_element, get_target_element);

public:
	inline P3GUIEventPtr(Rml::Event *rmlObject);
	Rml::Event *rmlObject;
};

/// inline definitions
#include "p3guiEvent.I"

#endif /* GUI_SOURCE_P3GUIEVENT_H_ */
