/**
 * \file pairs_valueLists_basic.h
 *
 * \date 2017-07-11
 * \author consultit
 */

#ifndef LIBTOOLS_SOURCE_PAIRS_VALUELISTS_BASIC_H_
#define LIBTOOLS_SOURCE_PAIRS_VALUELISTS_BASIC_H_

#include "tools_includes.h"
#include "pairs_valueLists_defs.h"
#include "pairs_valueLists_macros.h"
#include "lpoint3.h"
#include "nodePath.h"

///BASIC PAIRS' (EXPANDED) DECLARATIONS
#define PAIR_EXPANDABLE_PLACEHOLDER BASICPAIR_EXPANDABLE
///BASIC VALUELISTS' (EXPANDED) DECLARATIONS
#define VALUELIST_EXPANDABLE_PLACEHOLDER BASICVALUELIST_EXPANDABLE

#include "support/pairs_valueLists_src.h"

#undef PAIR_EXPANDABLE_PLACEHOLDER
#undef VALUELIST_EXPANDABLE_PLACEHOLDER

///inline
#include "pairs_valueLists_basic.I"

#endif /* LIBTOOLS_SOURCE_PAIRS_VALUELISTS_BASIC_H_ */
