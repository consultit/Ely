/**
 * \file utilities.cxx
 *
 * \date 2017-06-18
 * \author consultit
 */

#include "utilities.h"
#include "virtualFileSystem.h"
#include "texturePool.h"
#include "loaderFileTypeRegistry.h"
#include "loader.h"
#include "geomTristrips.h"
#include "textureAttrib.h"
#include "geomVertexWriter.h"
#include "config_module.h"

///Utilities
LoaderOptions Utilities::_loader_options;

Utilities::Utilities(const NodePath& root, const CollideMask& mask):
		mRoot(root),
		mMask(mask),
		mCollisionHandler(nullptr),
		mPickerRay(nullptr),
		mCTrav(nullptr)
{
	//
	if (!mRoot.is_empty())
	{
		mCTrav = new CollisionTraverser();
		mCollisionHandler = new CollisionHandlerQueue();
		mPickerRay = new CollisionRay();
		PT(CollisionNode)pickerNode = new CollisionNode(string("Utilities::pickerNode"));
		pickerNode->add_solid(mPickerRay);
		pickerNode->set_from_collide_mask(mMask);
		pickerNode->set_into_collide_mask(BitMask32::all_off());
		mCTrav->add_collider(mRoot.attach_new_node(pickerNode),
				mCollisionHandler);
	}
}

Utilities::~Utilities()
{
	delete mCTrav;
}

/**
 * Gets bounding dimensions of a model NodePath.
 * Puts results into the out parameters: modelDims, modelDeltaCenter and returns
 * modelRadius.
 * - modelDims = absolute dimensions of the model
 * - modelCenter + modelDeltaCenter = origin of coordinate system
 * - modelRadius = radius of the containing sphere
 */
float Utilities::get_bounding_dimensions(NodePath modelNP,
		LVecBase3f& modelDims, LVector3f& modelDeltaCenter)
{
	//get "tight" dimensions of model
	LPoint3f minP, maxP;
	modelNP.calc_tight_bounds(minP, maxP);
	//
	LVecBase3 delta = maxP - minP;
	LVector3f deltaCenter = -(minP + delta / 2.0);
	//
	modelDims.set(abs(delta.get_x()), abs(delta.get_y()), abs(delta.get_z()));
	modelDeltaCenter.set(deltaCenter.get_x(), deltaCenter.get_y(),
			deltaCenter.get_z());
	float modelRadius = max(max(modelDims.get_x(), modelDims.get_y()),
			modelDims.get_z()) / 2.0;
	return modelRadius;
}

/**
 * Load a model from a file.
 */
NodePath Utilities::load_model(Filename filename)
{
	cout << "Loading " << filename << "\n";

	// If the filename already exists where it is, or if it is fully qualified,
	// don't search along the model path for it.
	VirtualFileSystem *vfs = VirtualFileSystem::get_global_ptr();
	bool search = !(filename.is_fully_qualified() || vfs->exists(filename));

	// We allow loading image files here.  Check to see if it might be an image
	// file, based on the filename extension.
	bool is_image = false;
	string extension = filename.get_extension();
	if (extension == "pz" || extension == "gz")
	{
		extension = Filename(filename.get_basename_wo_extension()).get_extension();
	}
	TexturePool *texture_pool = TexturePool::get_global_ptr();
	LoaderFileType *model_type = NULL;

	if (!extension.empty())
	{
		LoaderFileTypeRegistry *reg = LoaderFileTypeRegistry::get_global_ptr();
		model_type = reg->get_type_from_extension(extension);

		if (model_type == (LoaderFileType *) NULL)
		{
			// The extension isn't a known model file type; is it a known image file
			// extension?
			TexturePool *texture_pool = TexturePool::get_global_ptr();
			if (texture_pool->get_texture_type(extension) != NULL)
			{
				// It is a known image file extension.
				is_image = true;
			}
		}
	}

	LoaderOptions options = Utilities::_loader_options;
	if (search)
	{
		options.set_flags(options.get_flags() | LoaderOptions::LF_search);
	}
	else
	{
		options.set_flags(options.get_flags() & ~LoaderOptions::LF_search);
	}

	Loader loader;
	PT(PandaNode)node;
	if (is_image)
	{
		node = do_load_image_as_model(filename);
	}
	else
	{
		node = loader.load_sync(filename, options);

		// It failed to load.  Is it because the extension isn't recognised?  If
		// so, then we just got done printing out the known scene types, and we
		// should also print out the supported texture types.
		if (node == (PandaNode *) NULL && !is_image && model_type == NULL)
		{
			texture_pool->write_texture_types(nout, 2);
		}
	}

	if (node == (PandaNode *) NULL)
	{
		nout << "Unable to load " << filename << "\n";
		return NodePath::not_found();
	}

	return NodePath(node);
}

/**
 * load_model()'s helper function.
 */
PT(PandaNode) Utilities::do_load_image_as_model(const Filename &filename)
{
	PT(Texture) tex = TexturePool::load_texture(filename);
	if (tex == NULL)
	{
		return NULL;
	}

	// Yes, it is an image file; make a texture out of it.
	tex->set_minfilter(SamplerState::FT_linear_mipmap_linear);
	tex->set_magfilter(SamplerState::FT_linear);
	tex->set_wrap_u(SamplerState::WM_clamp);
	tex->set_wrap_v(SamplerState::WM_clamp);
	tex->set_wrap_w(SamplerState::WM_clamp);

	// Ok, now make a polygon to show the texture.
	bool has_alpha = true;
	LVecBase2 tex_scale = tex->get_tex_scale();

	// Get the size from the original image (the texture may have scaled it to
	// make a power of 2).
	int x_size = tex->get_orig_file_x_size();
	int y_size = tex->get_orig_file_y_size();

	// Choose the dimensions of the polygon appropriately.
	PN_stdfloat left,right,top,bottom;
	static const PN_stdfloat scale = 10.0;
	if (x_size > y_size)
	{
		left = -scale;
		right = scale;
		top = (scale * y_size) / x_size;
		bottom = -(scale * y_size) / x_size;
	}
	else if (y_size != 0)
	{
		left = -(scale * x_size) / y_size;
		right = (scale * x_size) / y_size;
		top = scale;
		bottom = -scale;
	}
	else
	{
		p3tools_cat.warning()
		<< "Texture size is 0 0: " << *tex << "\n";

		left = -scale;
		right = scale;
		top = scale;
		bottom = -scale;
	}

	PT(GeomNode) card_node = new GeomNode("card");
	card_node->set_attrib(TextureAttrib::make(tex));
	if (has_alpha)
	{
		card_node->set_attrib(TransparencyAttrib::make(TransparencyAttrib::M_alpha));
	}

	bool is_3d = false;
	if (tex->get_texture_type() == Texture::TT_3d_texture ||
			tex->get_texture_type() == Texture::TT_cube_map)
	{
		// For a 3-d texture, generate a cube, instead of a plain card.
		is_3d = true;
	}

	CPT(GeomVertexFormat) vformat;
	if (!is_3d)
	{
		// Vertices and 2-d texture coordinates, all we need.
		vformat = GeomVertexFormat::get_v3t2();

	}
	else
	{
		// Vertices and 3-d texture coordinates.
		vformat = GeomVertexFormat::register_format
		(new GeomVertexArrayFormat
				(InternalName::get_vertex(), 3,
						GeomEnums::NT_stdfloat, GeomEnums::C_point,
						InternalName::get_texcoord(), 3,
						GeomEnums::NT_stdfloat, GeomEnums::C_texcoord));
	}

	PT(GeomVertexData) vdata = new GeomVertexData("card", vformat, Geom::UH_static);
	GeomVertexWriter vertex(vdata, InternalName::get_vertex());
	GeomVertexWriter texcoord(vdata, InternalName::get_texcoord());

	if (!is_3d)
	{
		// A normal 2-d card.
		vertex.add_data3(LVertex::rfu(left, 0.02, top));
		vertex.add_data3(LVertex::rfu(left, 0.02, bottom));
		vertex.add_data3(LVertex::rfu(right, 0.02, top));
		vertex.add_data3(LVertex::rfu(right, 0.02, bottom));

		texcoord.add_data2(0.0f, tex_scale[1]);
		texcoord.add_data2(0.0f, 0.0f);
		texcoord.add_data2(tex_scale[0], tex_scale[1]);
		texcoord.add_data2(tex_scale[0], 0.0f);

	}
	else
	{
		// The eight vertices of a 3-d cube.
		vertex.add_data3(-1.0f, -1.0f, 1.0f);// 0
		vertex.add_data3(-1.0f, -1.0f, -1.0f);// 1
		vertex.add_data3(1.0f, -1.0f, -1.0f);// 2
		vertex.add_data3(1.0f, -1.0f, 1.0f);// 3
		vertex.add_data3(1.0f, 1.0f, 1.0f);// 4
		vertex.add_data3(1.0f, 1.0f, -1.0f);// 5
		vertex.add_data3(-1.0f, 1.0f, -1.0f);// 6
		vertex.add_data3(-1.0f, 1.0f, 1.0f);// 7

		texcoord.add_data3(-1.0f, -1.0f, 1.0f);// 0
		texcoord.add_data3(-1.0f, -1.0f, -1.0f);// 1
		texcoord.add_data3(1.0f, -1.0f, -1.0f);// 2
		texcoord.add_data3(1.0f, -1.0f, 1.0f);// 3
		texcoord.add_data3(1.0f, 1.0f, 1.0f);// 4
		texcoord.add_data3(1.0f, 1.0f, -1.0f);// 5
		texcoord.add_data3(-1.0f, 1.0f, -1.0f);// 6
		texcoord.add_data3(-1.0f, 1.0f, 1.0f);// 7
	}

	PT(GeomTristrips) strip = new GeomTristrips(Geom::UH_static);

	if (!is_3d)
	{
		// The two triangles that make up a quad.
		strip->add_consecutive_vertices(0, 4);
		strip->close_primitive();

	}
	else
	{
		// The twelve triangles (six quads) that make up a cube.
		strip->add_vertex(7);
		strip->add_vertex(0);
		strip->add_vertex(4);
		strip->add_vertex(3);
		strip->close_primitive();

		strip->add_vertex(1);
		strip->add_vertex(6);
		strip->add_vertex(2);
		strip->add_vertex(5);
		strip->close_primitive();

		strip->add_vertex(5);
		strip->add_vertex(4);
		strip->add_vertex(2);
		strip->add_vertex(3);
		strip->add_vertex(1);
		strip->add_vertex(0);
		strip->add_vertex(6);
		strip->add_vertex(7);
		strip->add_vertex(5);
		strip->add_vertex(4);
		strip->close_primitive();
	}

	PT(Geom) geom = new Geom(vdata);
	geom->add_primitive(strip);

	card_node->add_geom(geom);

	return card_node.p();
}

/**
 * Throws a ray downward (-z direction default) from rayOrigin.
 * If collisions are found returns a Pair_bool_float == (true, height),
 * with height equal to the z-value of the first one.
 * If collisions are not found returns a Pair_bool_float == (false, 0.0).
 */
Pair_bool_float Utilities::get_collision_height(const LPoint3f& rayOrigin,
		const NodePath& space) const
{
	//traverse downward starting at rayOrigin
	mPickerRay->set_direction(LVecBase3f(0.0, 0.0, -1.0));
	mPickerRay->set_origin(rayOrigin);
	mCTrav->traverse(mRoot);
	if (mCollisionHandler->get_num_entries() > 0)
	{
		mCollisionHandler->sort_entries();
		CollisionEntry *entry0 =
				mCollisionHandler->get_entry(0);
		LPoint3f target = entry0->get_surface_point(space);
		float collisionHeight = target.get_z();
		return Pair_bool_float(true, collisionHeight);
	}
	//
	return Pair_bool_float(false, 0.0);
}
