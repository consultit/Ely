/**
 * \file utilities.h
 *
 * \date 2017-06-18
 * \author consultit
 */

#ifndef LIBTOOLS_SOURCE_UTILITIES_H_
#define LIBTOOLS_SOURCE_UTILITIES_H_

#include "tools_includes.h"
#include "nodePath.h"
#include "collisionTraverser.h"
#include "collisionHandlerQueue.h"
#include "collisionRay.h"
#include "pairs_valueLists.h"

/**
 * \name UTILITIES
 */
struct EXPCL_TOOLS Utilities
{
	PUBLISHED:
	Utilities(const NodePath& root,
			const CollideMask& mask);
	~Utilities();
	static float get_bounding_dimensions(NodePath modelNP, LVecBase3f& modelDims,
			LVector3f& modelDeltaCenter);
	static NodePath load_model(Filename filename);
	Pair_bool_float get_collision_height(const LPoint3f& origin,
			const NodePath& space = NodePath()) const;
	INLINE const CollideMask& get_collide_mask() const;
	INLINE NodePath get_collision_root() const;
	INLINE CollisionTraverser* get_collision_traverser() const;
	INLINE CollisionHandlerQueue* get_collision_handler() const;
	INLINE CollisionRay* get_collision_ray() const;
	// Python Properties
	MAKE_PROPERTY(collide_mask, get_collide_mask);
	MAKE_PROPERTY(collision_root, get_collision_root);
	MAKE_PROPERTY(collision_traverser, get_collision_traverser);
	MAKE_PROPERTY(collision_handler, get_collision_handler);
	MAKE_PROPERTY(collision_ray, get_collision_ray);

#ifndef CPPPARSER
private:
	///Utilities' data.
	static LoaderOptions _loader_options;
	NodePath mRoot;
	CollideMask mMask;//a.k.a. BitMask32
	CollisionTraverser* mCTrav;
	CollisionHandlerQueue* mCollisionHandler;
	CollisionRay* mPickerRay;
	static PT(PandaNode)do_load_image_as_model(const Filename &filename);
#endif //CPPPARSER
};

///inline
#include "utilities.I"

#endif /* LIBTOOLS_SOURCE_UTILITIES_H_ */
