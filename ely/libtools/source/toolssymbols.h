/**
 * \file toolssymbols.h
 *
 * \date 2017-06-21
 * \author consultit
 */
#ifndef LIBTOOLS_SOURCE_TOOLSSYMBOLS_H_
#define LIBTOOLS_SOURCE_TOOLSSYMBOLS_H_

#ifdef PYTHON_BUILD_libtools
#	define EXPCL_TOOLS EXPORT_CLASS
#	define EXPTP_TOOLS EXPORT_TEMPL
#else
#	define EXPCL_TOOLS IMPORT_CLASS
#	define EXPTP_TOOLS IMPORT_TEMPL
#endif

#endif /* LIBTOOLS_SOURCE_TOOLSSYMBOLS_H_ */
