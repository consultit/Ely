/**
 * \file utilities.I
 *
 * \date 2017-06-18
 * \author consultit
 */

#ifndef LIBTOOLS_SOURCE_UTILITIES_I_
#define LIBTOOLS_SOURCE_UTILITIES_I_

///Utilities inline definitions

///Utilities
/**
 * Returns the collide mask.
 */
INLINE const CollideMask& Utilities::get_collide_mask() const
{
	return mMask;
}
/**
 * Returns the collision root.
 */
INLINE NodePath Utilities::get_collision_root() const
{
	return mRoot;
}
/**
 * Returns the collision traverser.
 */
INLINE CollisionTraverser* Utilities::get_collision_traverser() const
{
	return mCTrav;
}
/**
 * Returns the collision handler.
 */
INLINE CollisionHandlerQueue* Utilities::get_collision_handler() const
{
	return mCollisionHandler;
}
/**
 * Returns the collision ray.
 */
INLINE CollisionRay* Utilities::get_collision_ray() const
{
	return mPickerRay;
}

#endif /* LIBTOOLS_SOURCE_UTILITIES_I_ */
