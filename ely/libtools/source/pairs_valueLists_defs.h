/**
 * \file pair_valueLists_defs.h
 *
 * \date 2018-09-15
 * \author consultit
 */

#ifndef LIBTOOLS_SOURCE_PAIR_VALUELISTS_DEFS_H_
#define LIBTOOLS_SOURCE_PAIR_VALUELISTS_DEFS_H_

/// BASIC TYPES ///

//BASIC PAIR: templated types preliminary defines
#define Pair_bool_float_base Pair<bool, float>
#define Pair_LPoint3f_int_base Pair<LPoint3f, int>

//BASIC PAIR: types to be expanded
#define BASICPAIR_EXPANDABLE \
EXPAND(Pair_bool_float, Pair_bool_float_base, bool, float)\
EXPAND(Pair_LPoint3f_int, Pair_LPoint3f_int_base, LPoint3f, int)

//BASIC VALUELISTS: templated types preliminary defines
#define ValueList_string_base ValueList<string>
#define ValueList_NodePath_base ValueList<NodePath>
#define ValueList_int_base ValueList<int>
#define ValueList_float_base ValueList<float>
#define ValueList_LVecBase3f_base ValueList<LVecBase3f>
#define ValueList_LVector3f_base ValueList<LVector3f>
#define ValueList_LPoint3f_base ValueList<LPoint3f>

//BASIC VALUELISTS: types to be expanded
#define BASICVALUELIST_EXPANDABLE \
EXPAND(ValueList_string, ValueList_string_base, string)\
EXPAND(ValueList_NodePath, ValueList_NodePath_base, NodePath)\
EXPAND(ValueList_int, ValueList_int_base, int)\
EXPAND(ValueList_float, ValueList_float_base, float)\
EXPAND(ValueList_LVecBase3f, ValueList_LVecBase3f_base, LVecBase3f)\
EXPAND(ValueList_LVector3f, ValueList_LVector3f_base, LVector3f)\
EXPAND(ValueList_LPoint3f, ValueList_LPoint3f_base, LPoint3f)\

/// NOT BASIC TYPES (i.e. dependent on the BASIC TYPES) ///

//NOT BASIC PAIRS: templated types preliminary defines
#define Pair_LVector3f_ValueList_float_base Pair<LVector3f, ValueList_float>

//NOT BASIC PAIRS: types to be expanded
#define NOTBASICPAIR_EXPANDABLE \
EXPAND(Pair_LVector3f_ValueList_float, Pair_LVector3f_ValueList_float_base, LVector3f, ValueList_float)\

//NOT BASIC VALUELISTS: templated types preliminary defines
#define ValueList_Pair_LPoint3f_int_base ValueList<Pair_LPoint3f_int>

//NOT BASIC VALUELISTS: types to be expanded
#define NOTBASICVALUELIST_EXPANDABLE \
EXPAND(ValueList_Pair_LPoint3f_int, ValueList_Pair_LPoint3f_int_base, Pair_LPoint3f_int)\

#endif /* LIBTOOLS_SOURCE_PAIR_VALUELISTS_DEFS_H_ */
