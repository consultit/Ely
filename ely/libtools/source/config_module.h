#ifndef LIBTOOLS_SOURCE_CONFIG_TOOLS_H_
#define LIBTOOLS_SOURCE_CONFIG_TOOLS_H_

#pragma once

#include "toolssymbols.h"
#include "pandabase.h"
#include "notifyCategoryProxy.h"
#include "configVariableDouble.h"
#include "configVariableString.h"
#include "configVariableInt.h"


NotifyCategoryDecl(p3tools, EXPCL_TOOLS, EXPORT_TEMPL);

extern void init_libp3tools();

#endif //LIBTOOLS_SOURCE_CONFIG_TOOLS_H_
