/**
 * \file pairs_valueLists_macros.h
 *
 * \date 2017-07-11
 * \author consultit
 */

#ifndef LIBTOOLS_SOURCE_PAIRS_VALUELISTS_MACROS_H_
#define LIBTOOLS_SOURCE_PAIRS_VALUELISTS_MACROS_H_

///Pair common declarations
#undef PAIRCTORSDECL
#define PAIRCTORSDECL(classtype,valuetype1,valuetype2) \
	classtype();\
	classtype(const valuetype1& first, const valuetype2& second);\
	classtype& operator =(const classtype &copy);\

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
#undef PAIRCTORSPYTHONDECL
#define PAIRCTORSPYTHONDECL(classtype) \
	classtype(PyObject *obj);\
	classtype& operator =(PyObject *obj);\

#endif //PYTHON_BUILD

#undef PAIRMEMBERSDECL
#define PAIRMEMBERSDECL(classtype,valuetype1,valuetype2) \
	bool operator== (const classtype &other) const;\
	void set_first(const valuetype1& first);\
	valuetype1 get_first() const;\
	void set_second(const valuetype2& second);\
	valuetype2 get_second() const;\
	MAKE_PROPERTY(first, get_first, set_first);\
	MAKE_PROPERTY(second, get_second, set_second);\
	void output(ostream &out) const;\
public:\
	static TypeHandle get_class_type()\
	{\
		return _type_handle;\
	}\
	static void init_type()\
	{\
		register_type(_type_handle, #classtype);\
	}\
private:\
	static TypeHandle _type_handle;\

///Pair common inline definitions
#undef PAIRINLINEDEFS
#define PAIRINLINEDEFS(classtype,valuetype1,valuetype2) \
INLINE ostream &operator <<(ostream &out, const classtype& pair)\
{\
	pair.output(out);\
	return out;\
}\

///ValueList common declarations
#undef VALUELISTCTORSDECL
#define VALUELISTCTORSDECL(classtype) \
	classtype(unsigned int size=0);\
	classtype(const classtype &copy);\
	INLINE ~classtype();\
	classtype& operator =(const classtype &copy);\

#if defined(PYTHON_BUILD) || defined(CPPPARSER)
#undef VALUELISTCTORSPYTHONDECL
#define VALUELISTCTORSPYTHONDECL(classtype) \
	classtype(PyObject *obj);\
	classtype& operator =(PyObject *obj);\

#endif //PYTHON_BUILD

#undef VALUELISTMEMBERSDECL
#define VALUELISTMEMBERSDECL(classtype,valuetype) \
	bool operator== (const classtype &other) const;\
	void add_value(const valuetype& value);\
	bool remove_value(const valuetype& value);\
	bool has_value(const valuetype& value) const;\
	void add_values_from(const classtype &other);\
	void remove_values_from(const classtype &other);\
	void clear();\
	int get_num_values() const;\
	void set_value(int index, const valuetype& value);\
	valuetype get_value(int index) const;\
	MAKE_SEQ(get_values, get_num_values, get_value);\
	MAKE_SEQ_PROPERTY(values, get_num_values, get_value);\
	valuetype operator [](int index) const;\
	int size() const;\
	void operator +=(const classtype &other);\
	classtype operator +(const classtype &other) const;\
	void output(ostream &out) const;\
public:\
	static TypeHandle get_class_type()\
	{\
		return _type_handle;\
	}\
	static void init_type()\
	{\
		register_type(_type_handle, #classtype);\
	}\
private:\
	static TypeHandle _type_handle;\

///ValueList common inline definitions
#undef VALUELISTINLINEDEFS
#define VALUELISTINLINEDEFS(classtype) \
INLINE classtype::~classtype()\
{\
}\
INLINE ostream &operator <<(ostream &out, const classtype& lst)\
{\
	lst.output(out);\
	return out;\
}\

#endif /* LIBTOOLS_SOURCE_PAIRS_VALUELISTS_MACROS_H_ */
