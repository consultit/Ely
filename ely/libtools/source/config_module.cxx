
#include "config_module.h"
#include "dconfig.h"
#include "pairs_valueLists.h"

Configure( config_p3tools );
NotifyCategoryDef( p3tools , "");

ConfigureFn( config_p3tools ) {
  init_libp3tools();
}

void
init_libp3tools() {
  static bool initialized = false;
  if (initialized) {
    return;
  }
  initialized = true;

  // Init your dynamic types here, e.g.:
  // MyDynamicClass::init_type();
  Pair_bool_float::init_type();
  Pair_LPoint3f_int::init_type();
  Pair_LVector3f_ValueList_float::init_type();
  ValueList_string::init_type();
  ValueList_NodePath::init_type();
  ValueList_int::init_type();
  ValueList_float::init_type();
  ValueList_LVecBase3f::init_type();
  ValueList_LVector3f::init_type();
  ValueList_LPoint3f::init_type();
  ValueList_Pair_LPoint3f_int::init_type();

  return;
}

