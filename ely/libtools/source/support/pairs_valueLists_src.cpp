/**
 * \file pairs_valueLists_src.cxx
 *
 * \date 2018-09-16
 * \author consultit
 */

///PAIRS' SOURCE DEFINITIONS
//always defined
#define EXPAND(classtype,basetype,type1,type2) \
classtype::classtype(): basetype()\
{\
}\
classtype::classtype(const type1& first, const type2& second) :\
		basetype(first, second)\
{\
}\
void classtype::output(ostream &out) const\
{\
	basetype::output(out);\
}\
void classtype::set_first(const type1& first)\
{\
	basetype::set_first(first);\
}\
type1 classtype::get_first() const\
{\
	return basetype::get_first();\
}\
void classtype::set_second(const type2& second)\
{\
	basetype::set_second(second);\
}\
type2 classtype::get_second() const\
{\
	return basetype::get_second();\
}\
bool classtype::operator== (const classtype &other) const\
{\
	return basetype::operator==(other);\
}\
classtype& classtype::operator =(const classtype &copy)\
{\
	basetype::operator =(copy);\
	return *this;\
}\
TypeHandle classtype::_type_handle;\

PAIR_EXPANDABLE_PLACEHOLDER
#undef EXPAND

#if !defined(CPPPARSER) && !defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type1,type2) \
	classtype::classtype(const basetype& value):basetype(value)\
	{\
	}\
	classtype::classtype(const pair<type1, type2>& value):basetype(value)\
	{\
	}\

	PAIR_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // !defined(CPPPARSER) && !defined(PYTHON_BUILD)

#if !defined(CPPPARSER) && defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type1,type2) \
	classtype::classtype(PyObject *obj):basetype(obj)\
	{\
	}\
	classtype& classtype::operator =(PyObject *obj)\
	{\
		basetype::operator =(obj);\
		return *this;\
	}\
	classtype::classtype(const basetype& value):basetype(value)\
	{\
	}\
	classtype::classtype(const pair<type1, type2>& value):basetype(value)\
	{\
	}\

	PAIR_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // !defined(CPPPARSER) && defined(PYTHON_BUILD)

#if defined(CPPPARSER) && defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type1,type2) \
	classtype::classtype(PyObject *obj):basetype(obj)\
	{\
	}\
	classtype& classtype::operator =(PyObject *obj)\
	{\
		basetype::operator =(obj);\
		return *this;\
	}\

	PAIR_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // defined(CPPPARSER) && defined(PYTHON_BUILD)

///VALUELISTS' SOURCE DEFINITIONS
//always defined
#define EXPAND(classtype,basetype,type) \
classtype::classtype(unsigned int size): basetype(size)\
{\
}\
classtype::classtype(const classtype &copy): basetype(copy)\
{\
}\
bool classtype::remove_value(const type& value)\
{\
	return basetype::remove_value(value);\
}\
void classtype::add_values_from(const classtype &other)\
{\
	basetype::add_values_from(other);\
}\
void classtype::remove_values_from(const classtype& other)\
{\
	basetype::remove_values_from(other);\
}\
bool classtype::has_value(const type& value) const\
{\
	return basetype::has_value(value);\
}\
void classtype::output(ostream &out) const\
{\
	basetype::output(out);\
}\
void classtype::add_value(const type& value)\
{\
	basetype::add_value(value);\
}\
void classtype::set_value(int index, const type& value)\
{\
	basetype::set_value(index, value);\
}\
type classtype::get_value(int index) const\
{\
	return basetype::get_value(index);\
}\
type classtype::operator [](int index) const\
{\
	return basetype::operator [](index);\
}\
classtype& classtype::operator =(const classtype &copy)\
{\
	basetype::operator =(copy);\
	return *this;\
}\
bool classtype::operator== (const classtype &other) const\
{\
	return basetype::operator== (other);\
}\
void classtype::clear()\
{\
	basetype::clear();\
}\
int classtype::get_num_values() const\
{\
	return basetype::get_num_values();\
}\
int classtype::size() const\
{\
	return basetype::size();\
}\
void classtype::operator +=(const classtype &other)\
{\
	basetype::operator +=(other);\
}\
classtype classtype::operator +(const classtype &other) const\
{\
	return classtype(basetype::operator +(other));\
}\
TypeHandle classtype::_type_handle;\

VALUELIST_EXPANDABLE_PLACEHOLDER
#undef EXPAND

#if !defined(CPPPARSER) && !defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type) \
	classtype::classtype(const basetype& value):basetype(value)\
	{\
	}\
	classtype::classtype(initializer_list<type> lst):basetype(lst)\
	{\
	}\
	classtype::classtype(const pvector<type>& lst):basetype(lst)\
	{\
	}\
	classtype::classtype(const plist<type>& lst):basetype(lst)\
	{\
	}\

	VALUELIST_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // !defined(CPPPARSER) && !defined(PYTHON_BUILD)

#if !defined(CPPPARSER) && defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type) \
	classtype::classtype(PyObject *obj):basetype(obj)\
	{\
	}\
	classtype& classtype::operator =(PyObject *obj)\
	{\
		basetype::operator =(obj);\
		return *this;\
	}\
	classtype::classtype(const basetype& value):basetype(value)\
	{\
	}\
	classtype::classtype(initializer_list<type> lst):basetype(lst)\
	{\
	}\
	classtype::classtype(const pvector<type>& lst):basetype(lst)\
	{\
	}\
	classtype::classtype(const plist<type>& lst):basetype(lst)\
	{\
	}\

	VALUELIST_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // !defined(CPPPARSER) && defined(PYTHON_BUILD)

#if defined(CPPPARSER) && defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type) \
	classtype::classtype(PyObject *obj):basetype(obj)\
	{\
	}\
	classtype& classtype::operator =(PyObject *obj)\
	{\
		basetype::operator =(obj);\
		return *this;\
	}\

	VALUELIST_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // defined(CPPPARSER) && defined(PYTHON_BUILD)
