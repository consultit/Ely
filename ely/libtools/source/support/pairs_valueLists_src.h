/**
 * \file pairs_valueLists_src.h
 *
 * \date 2018-09-16
 * \author consultit
 */

///PAIRS' SOURCE DECLARATIONS
#if !defined(CPPPARSER) && !defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type1,type2) \
	struct EXPCL_TOOLS classtype\
	: public basetype\
	{\
	PUBLISHED:\
		PAIRCTORSDECL(classtype,type1,type2)\
		PAIRMEMBERSDECL(classtype,type1,type2)\
	public:\
		classtype(const basetype& value);\
		classtype(const pair<type1, type2>& value);\
	};\
	INLINE ostream &operator << (ostream &out, const classtype & pair);\

	PAIR_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // !defined(CPPPARSER) && !defined(PYTHON_BUILD)

#if !defined(CPPPARSER) && defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type1,type2) \
	struct EXPCL_TOOLS classtype\
	: public basetype\
	{\
	PUBLISHED:\
		PAIRCTORSDECL(classtype,type1,type2)\
		PAIRCTORSPYTHONDECL(classtype)\
		PAIRMEMBERSDECL(classtype,type1,type2)\
	public:\
		classtype(const basetype& value);\
		classtype(const pair<type1, type2>& value);\
	};\
	INLINE ostream &operator << (ostream &out, const classtype & pair);\

	PAIR_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // !defined(CPPPARSER) && defined(PYTHON_BUILD)

#if defined(CPPPARSER) && !defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type1,type2) \
	struct EXPCL_TOOLS classtype\
	{\
	PUBLISHED:\
		PAIRCTORSDECL(classtype,type1,type2)\
		PAIRCTORSPYTHONDECL(classtype)\
		PAIRMEMBERSDECL(classtype,type1,type2)\
	};\
	INLINE ostream &operator << (ostream &out, const classtype & pair);\

	PAIR_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // defined(CPPPARSER) && !defined(PYTHON_BUILD)

#if defined(CPPPARSER) && defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type1,type2) \
	struct EXPCL_TOOLS classtype\
	{\
	PUBLISHED:\
		PAIRCTORSDECL(classtype,type1,type2)\
		PAIRCTORSPYTHONDECL(classtype)\
		PAIRMEMBERSDECL(classtype,type1,type2)\
	};\
	INLINE ostream &operator << (ostream &out, const classtype & pair);\

	PAIR_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // defined(CPPPARSER) && defined(PYTHON_BUILD)

///VALUELISTS' SOURCE DECLARATIONS
#if !defined(CPPPARSER) && !defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type) \
	class EXPCL_TOOLS classtype\
	: public basetype\
	{\
	PUBLISHED:\
		VALUELISTCTORSDECL(classtype)\
		VALUELISTMEMBERSDECL(classtype,type)\
	public:\
		classtype(const basetype& value);\
		classtype(initializer_list<type> lst);\
		classtype(const pvector<type>& lst);\
		classtype(const plist<type>& lst);\
	};\
	INLINE ostream &operator << (ostream &out, const classtype & lst);\

	VALUELIST_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // !defined(CPPPARSER) && !defined(PYTHON_BUILD)

#if !defined(CPPPARSER) && defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type) \
	class EXPCL_TOOLS classtype\
	: public basetype\
	{\
	PUBLISHED:\
		VALUELISTCTORSDECL(classtype)\
		VALUELISTCTORSPYTHONDECL(classtype)\
		VALUELISTMEMBERSDECL(classtype,type)\
	public:\
		classtype(const basetype& value);\
		classtype(initializer_list<type> lst);\
		classtype(const pvector<type>& lst);\
		classtype(const plist<type>& lst);\
	};\
	INLINE ostream &operator << (ostream &out, const classtype & lst);\

	VALUELIST_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // !defined(CPPPARSER) && defined(PYTHON_BUILD)

#if defined(CPPPARSER) && !defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type) \
	class EXPCL_TOOLS classtype\
	{\
	PUBLISHED:\
		VALUELISTCTORSDECL(classtype)\
		VALUELISTCTORSPYTHONDECL(classtype)\
		VALUELISTMEMBERSDECL(classtype,type)\
	};\
	INLINE ostream &operator << (ostream &out, const classtype & lst);\

	VALUELIST_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // defined(CPPPARSER) && !defined(PYTHON_BUILD)

#if defined(CPPPARSER) && defined(PYTHON_BUILD)
	#define EXPAND(classtype,basetype,type) \
	class EXPCL_TOOLS classtype\
	{\
	PUBLISHED:\
		VALUELISTCTORSDECL(classtype)\
		VALUELISTCTORSPYTHONDECL(classtype)\
		VALUELISTMEMBERSDECL(classtype,type)\
	};\
	INLINE ostream &operator << (ostream &out, const classtype & lst);\

	VALUELIST_EXPANDABLE_PLACEHOLDER
	#undef EXPAND
#endif // defined(CPPPARSER) && defined(PYTHON_BUILD)
