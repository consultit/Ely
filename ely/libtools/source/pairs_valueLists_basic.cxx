/**
 * \file pairs_valueLists_basic.cxx
 *
 * \date 2017-07-11
 * \author consultit
 */

#include "pairs_valueLists_basic.h"

///BASIC PAIRS' (EXPANDED) DEFINITIONS
#define PAIR_EXPANDABLE_PLACEHOLDER BASICPAIR_EXPANDABLE
///BASIC VALUELISTS' (EXPANDED) DEFINITIONS
#define VALUELIST_EXPANDABLE_PLACEHOLDER BASICVALUELIST_EXPANDABLE

#include "support/pairs_valueLists_src.cpp"

#undef PAIR_EXPANDABLE_PLACEHOLDER
#undef VALUELIST_EXPANDABLE_PLACEHOLDER
