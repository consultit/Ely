
Ely
===

*Ely* is a Python/C++ framework for creating games based on the open source [Panda3d](https://www.panda3d.org) game engine and some more libraries.

*Ely* tries to add as easily and efficiently as possible, some features that are not natively 
available in Panda3d, which, according to the authors, remains one of the best and most complete 
open source game engines currently available on the net.

*Ely*, in addition to Panda3d, is based on several open source libraries (incorporated as sub-modules) 
for its operation:

* [RecastNavigation](https://github.com/recastnavigation/recastnavigation.git) (ai)
* [OpenSteer](http://svn.code.sf.net/p/opensteer/code) (ai)
* [RmlUi](https://github.com/mikke89/RmlUi.git) (gui)
* [bullet3](https://github.com/bulletphysics/bullet3.git) (physics)
* [clew](https://github.com/martijnberger/clew.git) (physics)
* [v-hacd](https://github.com/kmammou/v-hacd.git) (physics)

*Ely* is in beta-test stage, and is developed on Debian/Ubuntu platforms.

*Ely* is a LGPL licensed open source project. 

Building Ely
------------

First clone *Ely* with these commands:
    
    $ git clone https://gitlab.com/consultit/Ely.git
    $ cd Ely
    $ git submodule update --init --recursive

(or with newer versions of *git*:

	$ git clone https://gitlab.com/consultit/Ely.git --recurse-submodules
)

Then follow INSTALL for building/installing/updating *Ely*.

Sample programs
---------------

*Ely* comes with fully functional sample programs inside the test folder.
