'''
Created on Feb 9, 2020

@author: consultit
'''

from setuptools import setup, find_packages
from subprocess import run
import os, sys

setupdir = os.path.dirname(os.path.realpath(__file__))

def get_version(version_tuple):
    # additional handling of a,b,rc tags, this can
    # be simpler depending on your versioning scheme
    if not isinstance(version_tuple[-1], int):
        return '.'.join(map(str, version_tuple[:-1])) + version_tuple[-1]
    return '.'.join(map(str, version_tuple))

# path to the packages __init__ module in project
# source tree
init = os.path.join(setupdir, 'ely', '__init__.py')
version_line = list(filter(lambda l: l.startswith('VERSION'), open(init)))[0]

# VERSION is a tuple so we need to eval 'version_line'.
# We could simply import it from the package but we
# cannot be sure that this package is importable before
# installation is done.
VERSION = get_version(eval(version_line.split('=')[-1]))

def read_md(file_path):
    with open(file_path, 'r') as f:
        return f.read()

README = os.path.join(setupdir, 'README.md')

SAMPLE_PACKAGES = (
    ['ely.samples'] + 
    ['ely.samples.assets'] + 
    ['ely.samples.' + pkg for pkg in find_packages(
        where=os.path.join('test', 'python'),
        exclude=['*.unit_test', '*.unit_test.*', 'game', 'game.*',
                 'p3dtest', 'p3dtest.*', ])])

# build ely modules during source dist creation
if sys.platform.startswith('linux'):
    libsuff = '.so'
elif sys.platform.startswith('win'):
    libsuff = '.dll'
ELY_MODULES = ['ai', 'audio', 'control', 'gui', 'network', 'physics']
if (len(sys.argv) > 1) and (sys.argv[1] == 'sdist'):
    os.chdir(setupdir)
    pythonCmd = sys.executable
    if not pythonCmd:
        pythonCmd = 'python'
    # build libtools
    run([pythonCmd, 'ely_setup.py'])
    # build other modules
    for module in ELY_MODULES:
        run([pythonCmd, 'ely_setup.py', '-m', module, '-n'])

setup(
    name='ely',
    version=VERSION,
    author='consultit',
    author_email='consultit25@gmail.com',
    maintainer='consultit',
    maintainer_email='consultit25@gmail.com',
    url='https://gitlab.com/consultit/Ely.git',
    description='Python/C++ framework based on Panda3d game engine plus other libraries',
    long_description=read_md(README),
    long_description_content_type='text/markdown',
    license='LGPLv3',
    package_dir={'ely.samples': os.path.join('test', 'python'),
                'ely.samples.assets': os.path.join('test')},
    packages=SAMPLE_PACKAGES + find_packages(
        exclude=['ely.scripts', 'ely.scripts.*', 'test', 'test.*',
                 'assets', 'assets.*', ]),
    include_package_data=True,
    package_data={
        'ely': [m + libsuff for m in ELY_MODULES],
        'ely.samples.assets':
            [os.path.join(setupdir, 'assets', 'misc', 'test', '*'),
             os.path.join(setupdir, 'assets', 'models', 'test', '*'),
             os.path.join(setupdir, 'assets', 'sounds', 'test', '*'),
             os.path.join(setupdir, 'assets', 'textures', 'test', '*'), ],
    },
    install_requires=['panda3d (>=1.10)'],
    python_requires='>=3.4',
    provides=['ely'],
    classifiers=[  # Optional
        'Development Status :: 4 - Beta',
        'Operating System :: POSIX :: Linux',
#         'Operating System :: Microsoft :: Windows',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Games/Entertainment',
        'Topic :: Software Development',
        'License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
)
