'''
Created on Aug 7, 2018

@author: consultit
'''

from ely.network import constUInt32, NetworkObject, Int8, Float
from ely.libtools import Utilities
from ely.control import GameControlManager
from common import GameObject, Colors
import common
from panda3d.core import LVector3f, LVecBase3f, BitMask32, NodePath
from RoboInputState import RoboInputState
from enum import IntEnum, unique


class Panda(GameObject):
    '''
    Panda class.
    '''

    PAND = constUInt32(b'P', b'A', b'N', b'D')

    @unique
    class EReplicationState(IntEnum):
        Pose = 1 << 0
        PlayerId = 1 << 1
        Color = 1 << 2
        AllState = Pose | PlayerId | Color

    def __init__(self):
        '''
        Constructor
        '''

        # GameObject.__init__(self, NetworkObject.get_class_id_int('PAND'))
        super(Panda, self).__init__(NetworkObject.get_class_id_int('PAND'))
        self.mPlayerId = Int8(0)
        self.driverVelX = Float(0.0)
        self.driverVelY = Float(0.0)
        self.driverVelH = Float(0.0)
        self.set_owner_object(Utilities.load_model('john'))
        self.set_scale(0.15)
        modelDims = LVecBase3f()
        modelDeltaCenter = LVector3f()
        Utilities.get_bounding_dimensions(self.get_owner_object(), modelDims,
                                          modelDeltaCenter)
        self.set_collision_radius(modelDims.get_y() / 2.0)
        self.set_rotation(LVecBase3f.zero())
        self.set_color(Colors.White)
        self.driver = None
        # add r/w members
        members = self.get_members()
        # use assign to change its value
        members.add_variable_int8_t('mPlayerId', self.mPlayerId)
        # use assign to change its value
        members.add_variable_float('driverVelX', self.driverVelX)
        # use assign to change its value
        members.add_variable_float('driverVelY', self.driverVelY)
        # use assign to change its value
        members.add_variable_float('driverVelH', self.driverVelH)
        # set r/w members' dirty states
        members.set_dirty_state('posX', Panda.EReplicationState.Pose)
        members.set_dirty_state('posY', Panda.EReplicationState.Pose)
        members.set_dirty_state('posZ', 0)
        members.set_dirty_state('rotH', Panda.EReplicationState.Pose)
        members.set_dirty_state('rotP', 0)
        members.set_dirty_state('rotR', 0)
        members.set_dirty_state('color', Panda.EReplicationState.Color)
        members.set_dirty_state('mPlayerId', Panda.EReplicationState.PlayerId)
        members.set_dirty_state('driverVelX', Panda.EReplicationState.Pose)
        members.set_dirty_state('driverVelY', Panda.EReplicationState.Pose)
        members.set_dirty_state('driverVelH', Panda.EReplicationState.Pose)
        # callbacks
#         self.set_update_callback (clbk)
        self.set_all_state_mask_callback(Panda.get_all_state_mask_clbk)
#         self.set_handle_dying_callback(clbk)
        self.set_pre_write_callback(Panda.pre_write_clbk)
#         self.set_post_write_callback(clbk)
#         self.set_pre_read_callback(clbk)
        self.set_post_read_callback(Panda.post_read_clbk)

    @staticmethod
    def get_all_state_mask_clbk(networkObject):
        return Panda.EReplicationState.AllState

    @staticmethod
    def pre_write_clbk(THIS, writeState):
        # owner object . r/w members synchronization
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if writeState.get_word() & Panda.EReplicationState.Pose:
            vel = downTHIS.driver.get_linear_speed()
            downTHIS.driverVelX.assign(vel.get_x())
            downTHIS.driverVelY.assign(vel.get_y())
            downTHIS.SetPosVar(downTHIS.get_location())
            downTHIS.driverVelH.assign(downTHIS.driver.get_angular_speeds()[0])
            downTHIS.SetRotVar(downTHIS.get_rotation())
        if writeState.get_word() & Panda.EReplicationState.Color:
            downTHIS.SetColVar(downTHIS.get_color())

    @staticmethod
    def post_read_clbk(THIS, readState):
        # r/w members -> owner object synchronization
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if readState.get_word() & Panda.EReplicationState.Pose:
            downTHIS.driver.set_linear_speed(LVector3f(downTHIS.driverVelX.value,
                                                       downTHIS.driverVelY.value, 0.0))
            downTHIS.set_location(downTHIS.GetPosVar())
            downTHIS.driver.set_angular_speeds(
                [downTHIS.driverVelH.value, 0.0])
            downTHIS.set_rotation(downTHIS.GetRotVar())
        if readState.get_word() & Panda.EReplicationState.Color:
            downTHIS.set_color(downTHIS.GetColVar())

    def ProcessInput(self, inDeltaTime, inInputState):
        # process our input....

        state = RoboInputState.get_global_ptr()
        self.driver.set_move_forward(state.get_move_forward(inInputState))
        self.driver.set_move_backward(state.get_move_backward(inInputState))
        self.driver.set_rotate_head_left(
            state.get_rotate_head_left(inInputState))
        self.driver.set_rotate_head_right(
            state.get_rotate_head_right(inInputState))

    def SimulateMovement(self, inDeltaTime):
        # simulate us...
        self.driver.update(inDeltaTime)

    def SetPlayerId(self, inPlayerId):
        self.mPlayerId.assign(inPlayerId)

    def GetPlayerId(self):
        return self.mPlayerId.value

    def setDriverParametersBeforeCreation(self):
        '''set driver's various creation parameters as string'''

        ctrlMgr = GameControlManager.get_global_ptr()
        # set driver's parameters
        ctrlMgr.set_parameter_value(
            GameControlManager.DRIVER, 'max_angular_speed', '100.0')
        ctrlMgr.set_parameter_value(
            GameControlManager.DRIVER, 'angular_accel', '50.0')
        ctrlMgr.set_parameter_value(
            GameControlManager.DRIVER, 'max_linear_speed', '8.0')
        ctrlMgr.set_parameter_value(
            GameControlManager.DRIVER, 'linear_accel', '1.0')
        ctrlMgr.set_parameter_value(
            GameControlManager.DRIVER, 'linear_friction', '0.5')
        ctrlMgr.set_parameter_value(
            GameControlManager.DRIVER, 'angular_friction', '5.0')

    def setupDriver(self):
        # setup the driver
        self.setDriverParametersBeforeCreation()
        # create the driver (attached to the reference node)
        self.driver = GameControlManager.get_global_ptr().create_driver('PandaDriver').node()
        return NodePath.any_path(self.driver)
