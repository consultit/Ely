'''
Created on Aug 7, 2018

@author: consultit
'''

from ely.network import constUInt32, NetworkObject, Int8, Float, NetworkWorld  # , Bool
from ely.libtools import Utilities
from common import GameObject, Colors, HALF_WORLD_HEIGHT, HALF_WORLD_WIDTH
import common
from Mouse import Mouse
from Yarn import Yarn
from RoboInputState import RoboInputState
from panda3d.core import LVector3f, LVecBase3f, BitMask32
from enum import IntEnum, unique


class RoboCat(GameObject):
    '''
    RoboCat class.
    '''

    RCAT = constUInt32(b'R', b'C', b'A', b'T')

    @unique
    class ECatReplicationState(IntEnum):
        ECRS_Pose = 1 << 0
        ECRS_Color = 1 << 1
        ECRS_PlayerId = 1 << 2
        ECRS_Health = 1 << 3
        ECRS_AllState = ECRS_Pose | ECRS_Color | ECRS_PlayerId | ECRS_Health

    def __init__(self):
        '''
        Constructor
        '''

        # GameObject.__init__(self, NetworkObject.get_class_id_int('RCAT'))
        super(RoboCat, self).__init__(NetworkObject.get_class_id_int('RCAT'))
        self.mVelocity = LVector3f(0.0, 0.0, 0.0)
        self.mMaxLinearSpeed = float(100.0)
        self.mMaxRotationSpeed = float(25.0)
        # bounce fraction when hitting various things
        self.mWallRestitution = float(0.1)
        self.mCatRestitution = float(0.1)
        self.mPlayerId = Int8(0)
        # /move down here for padding reasons...
        self.mLastMoveTimestamp = float(0.0)
        self.mThrustDir = Float(0.0)
# *fixme optimization
#         self.mThrustDir = float(0.0)
#         # (!mThrustDirPos && !mThrustDirNeg) == true -> mThrustDir == 0.0
#         self.mThrustDirPos = Bool(False)
#         self.mThrustDirNeg = Bool(False)
# *
        self.mHealth = Int8(10)
        self.mIsShooting = False
        self.set_owner_object(Utilities.load_model('ralph'))
        self.set_scale(0.5)
        modelDims = LVecBase3f()
        modelDeltaCenter = LVector3f()
        Utilities.get_bounding_dimensions(
            self.get_owner_object(), modelDims, modelDeltaCenter)
        self.set_collision_radius(modelDims.get_y() / 2.0)
        self.set_rotation(LVecBase3f.zero())
        self.set_color(Colors.White)
        # add r/w members
        members = self.get_members()
        # use assign to change its value
        members.add_variable_int8_t('mPlayerId', self.mPlayerId)
        # use assign to change its value
        members.add_variable_lvector3f('mVelocity', self.mVelocity)
        # use assign to change its value
        members.add_variable_float('mThrustDir', self.mThrustDir)
        # use assign to change its value
        members.add_variable_int8_t('mHealth', self.mHealth)
# *fixme optimization
#         members.add_variable_bool('mThrustDirPos', self.mThrustDirPos)
#         members.add_variable_bool('mThrustDirNeg', self.mThrustDirNeg)
# *
        # set r/w members' dirty states
        members.set_dirty_state('posX', BitMask32(
            RoboCat.ECatReplicationState.ECRS_Pose))
        members.set_dirty_state('posY', BitMask32(
            RoboCat.ECatReplicationState.ECRS_Pose))
        members.set_dirty_state('posZ', 0)
        members.set_dirty_state('rotH', BitMask32(
            RoboCat.ECatReplicationState.ECRS_Pose))
        members.set_dirty_state('rotP', 0)
        members.set_dirty_state('rotR', 0)
        members.set_dirty_state('color', BitMask32(
            RoboCat.ECatReplicationState.ECRS_Color))
        members.set_dirty_state('mPlayerId', BitMask32(
            RoboCat.ECatReplicationState.ECRS_PlayerId))
        members.set_dirty_state('mVelocity', BitMask32(
            RoboCat.ECatReplicationState.ECRS_Pose))
        # already constructed with BitMsk32.all_on()
        members.set_dirty_state('mThrustDir', BitMask32(0xFFFFFFFF))
# *fixme optimization
#         members.set_dirty_state('mThrustDirPos', BitMask32(0xFFFFFFFF)) #already constructed with BitMsk32.all_on()
#         members.set_dirty_state('mThrustDirNeg', BitMask32(0xFFFFFFFF)) #already constructed with BitMsk32.all_on()
# *
        members.set_dirty_state('mHealth', BitMask32(
            RoboCat.ECatReplicationState.ECRS_Health))
        # callbacks
#         self.set_update_callback (clbk)
        self.set_all_state_mask_callback(RoboCat.get_all_state_mask_clbk)
#         set_handle_dying_callback(clbk)
        self.set_pre_write_callback(RoboCat.pre_write_clbk)
#         self.set_post_write_callback(clbk)
#         self.set_pre_read_callback(clbk)
        self.set_post_read_callback(RoboCat.post_read_clbk)

    @staticmethod
    def get_all_state_mask_clbk(NetworkObject):
        return RoboCat.ECatReplicationState.ECRS_AllState

    @staticmethod
    def pre_write_clbk(THIS, writeState):
        # owner object . r/w members synchronization
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if writeState.get_word() & RoboCat.ECatReplicationState.ECRS_Pose:
            downTHIS.SetPosVar(downTHIS.get_location())
            downTHIS.SetRotVar(downTHIS.get_rotation())
        if writeState.get_word() & RoboCat.ECatReplicationState.ECRS_Color:
            downTHIS.SetColVar(downTHIS.get_color())
# *fixme optimization
#         # always write mThrustDir- it's just two bits
#         # never happens that (mThrustDirPos && mThrustDirNeg) == true
#         downTHIS = common.NetObjGameObjMap[THIS.this]
#         if downTHIS.GetThrustDir() > 0.0:
#             downTHIS.SetThrustDirPos(True)
#             downTHIS.SetThrustDirNeg(False)
#         elif downTHIS.GetThrustDir() > 0.0:
#             downTHIS.SetThrustDirPos(False)
#             downTHIS.SetThrustDirNeg(True)
#         else:
#             downTHIS.SetThrustDirPos(False)
#             downTHIS.SetThrustDirNeg(False)
# *

    @staticmethod
    def post_read_clbk(THIS, readState):
        # r/w members . owner object synchronization
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if readState.get_word() & RoboCat.ECatReplicationState.ECRS_Pose:
            downTHIS.set_location(downTHIS.GetPosVar())
            downTHIS.set_rotation(downTHIS.GetRotVar())
        if readState.get_word() & RoboCat.ECatReplicationState.ECRS_Color:
            downTHIS.set_color(downTHIS.GetColVar())
# *fixme optimization
#         # always adjust mThrustDir
#         # never happens that (mThrustDirPos && mThrustDirNeg) == true
#         if downTHIS.GetThrustDirPos():
#             downTHIS.SetThrustDir(1.0)
#         elif downTHIS.GetThrustDirNeg():
#             downTHIS.SetThrustDir(-1.0)
#         else:
#             downTHIS.SetThrustDir(0.0)
# *

    def ProcessInput(self, inDeltaTime, inInputState):
        # process our input....
        state = RoboInputState.get_global_ptr()
        # turning...
        newRotation = (self.get_rotation().get_x() + state.GetDesiredHorizontalDelta(inInputState)
                       * self.mMaxRotationSpeed * inDeltaTime)
        self.set_rotation(LVecBase3f(newRotation, self.get_rotation().get_y(),
                                     self.get_rotation().get_z()))
        # moving...
        inputForwardDelta = state.GetDesiredVerticalDelta(inInputState)
        self.mThrustDir.assign(inputForwardDelta)
        self.mIsShooting = state.IsShooting(inInputState)

    def AdjustVelocityByThrust(self, inDeltaTime):
        # just set the velocity based on the thrust direction -- no thrust will lead to 0 velocity
        # simulating acceleration makes the client prediction a bit more
        # complex
        forwardVector = self.GetForwardVector()
        self.mVelocity.assign(
            forwardVector * (self.mThrustDir.value * inDeltaTime * self.mMaxLinearSpeed))

    def SimulateMovement(self, inDeltaTime):
        # simulate us...
        self.AdjustVelocityByThrust(inDeltaTime)
        self.set_location(self.get_location() + self.mVelocity * inDeltaTime)
        self.ProcessCollisions()

    def ProcessCollisions(self):
        # right now just bounce off the sides..
        self.ProcessCollisionsWithScreenWalls()
        sourceRadius = self.get_collision_radius()
        sourceLocation = self.get_location()
        # now let's iterate through the world and see what we hit...
        # note: since there's a small number of objects in our game, this is fine.
        # but in a real game, brute-force checking collisions against every other object is not efficient.
        # it would be preferable to use a quad tree or some other structure to minimize the
        # number of collisions that need to be tested.
        netObjs = NetworkWorld.get_global_ptr().get_network_objects()
        for goIt in netObjs:
            # target.netObj == goIt
            target = common.NetObjGameObjMap[goIt.this]
            if (target != self) and (not target.get_does_want_to_die()):
                # simple collision test for spheres- are the radii summed less
                # than the distance?
                targetLocation = target.get_location()
                targetRadius = target.get_collision_radius()
                delta = targetLocation - sourceLocation
                distSq = delta.length_squared()
                collisionDist = (sourceRadius + targetRadius)
                if distSq < (collisionDist * collisionDist):
                    # first, tell the other guy there was a collision with a
                    # cat, so it can do something...
                    collision = False
                    if target.get_class_id() == Mouse.MOUS:
                        collision = target.HandleCollisionWithCat(self)
                        break
                    elif target.get_class_id() == Yarn.YARN:
                        collision = target.HandleCollisionWithCat(self)
                        break
                    if collision:
                        # okay, you hit something!
                        # so, project your location far enough that you're not
                        # colliding
                        dirToTarget = delta
                        dirToTarget.normalize()
                        acceptableDeltaFromSourceToTarget = dirToTarget * collisionDist
                        # important note- we only move this cat. the other cat
                        # can take care of moving itself
                        self.set_location(
                            targetLocation - acceptableDeltaFromSourceToTarget)
                        relVel = self.mVelocity
                        # if other object is a cat, it might have velocity, so
                        # there might be relative velocity...
                        if target.get_class_id() == RoboCat.RCAT:
                            relVel -= target.mVelocity
                        # got vel with dir between objects to figure out if they're moving towards each other
                        # and if so, the magnitude of the impulse ( since
                        # they're both just balls )
                        relVelDotDir = relVel.dot(dirToTarget)
                        if relVelDotDir > 0.0:
                            impulse = relVelDotDir * dirToTarget
                            if target.get_class_id() == RoboCat.RCAT:
                                self.mVelocity -= impulse  # in-place operator
                                self.mVelocity *= self.mCatRestitution  # in-place operator
                            else:
                                self.mVelocity -= impulse * 2.0  # in-place operator
                                self.mVelocity *= self.mWallRestitution  # in-place operator

    def ProcessCollisionsWithScreenWalls(self):
        location = self.get_location()
        x = location.get_x()
        y = location.get_y()
        vx = self.mVelocity.get_x()
        vy = self.mVelocity.get_y()
        radius = self.get_collision_radius()
        # if the cat collides against a wall, the quick solution is to push it
        # off
        if ((y + radius) >= HALF_WORLD_HEIGHT) and (vy > 0):
            self.mVelocity[1] = -vy * self.mWallRestitution
            location[1] = HALF_WORLD_HEIGHT - radius
            self.set_location(location)
        elif (y <= (-HALF_WORLD_HEIGHT - radius)) and (vy < 0):
            self.mVelocity[1] = -vy * self.mWallRestitution
            location[1] = -HALF_WORLD_HEIGHT - radius
            self.set_location(location)
        if ((x + radius) >= HALF_WORLD_WIDTH) and (vx > 0):
            self.mVelocity[0] = -vx * self.mWallRestitution
            location[0] = HALF_WORLD_WIDTH - radius
            self.set_location(location)
        elif (x <= (-HALF_WORLD_WIDTH - radius)) and (vx < 0):
            self.mVelocity[0] = -vx * self.mWallRestitution
            location[0] = -HALF_WORLD_WIDTH - radius
            self.set_location(location)

    def SetPlayerId(self, inPlayerId):
        self.mPlayerId.assign(inPlayerId)

    def GetPlayerId(self):
        return self.mPlayerId.value

    def SetVelocity(self, inVelocity):
        self.mVelocity.assign(inVelocity)

    def GetVelocity(self):
        return self.mVelocity

    def GetForwardVector(self):
        # should we cache this when you turn?
        return self.get_owner_object().get_parent().get_relative_vector(self.get_owner_object(),
                                                                        -LVector3f.forward())

    def SetHealth(self, value):
        self.mHealth.assign(value)

    def GetHealth(self):
        return self.mHealth.value

# *fixme optimization
#     def SetThrustDir(self, value):
#         self.mThrustDir = value
#
#     def GetThrustDir(self):
#         return self.mThrustDir
#
#     def SetThrustDirPos(self, value):
#         self.mThrustDirPos.assign(value)
#
#     def GetThrustDirPos(self):
#         return self.mThrustDirPos.value
#
#     def SetThrustDirNeg(self, value):
#         self.mThrustDirNeg.assign(value)
#
#     def GetThrustDirNeg(self):
#         return self.mThrustDirNeg.value
# *
