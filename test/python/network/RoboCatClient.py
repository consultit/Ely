'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import GameNetworkManagerClient, InputManager, Timing
from RoboCat import RoboCat
from HUD import HUD
from common import GameObject, Is2DVectorEqual, Lerp
import common
from panda3d.core import LVecBase3f, LPoint3f, LVector3f

ELY_DEBUG = common.ELY_DEBUG
LOG = print


class RoboCatClient(RoboCat):
    '''
    RoboCatClient class.
    '''

    def __init__(self):
        '''
        Constructor
        '''

        # RoboCat.__init__(self)
        super(RoboCatClient, self).__init__()
        self.mTimeLocationBecameOutOfSync = float(0.0)
        self.mTimeVelocityBecameOutOfSync = float(0.0)
        self.mOldRotation = LVecBase3f()
        self.mOldLocation = LPoint3f()
        self.mOldVelocity = LVector3f()
        # add r/w members
        # set r/w members' dirty states
        # callbacks
        self.set_update_callback(RoboCatClient.update_clbk)
    #    self.set_all_state_mask_callback(clbk)
        self.set_handle_dying_callback(RoboCatClient.handle_dying_clbk)
    #    self.set_pre_write_callback(clbk)
    #    self.set_post_write_callback(clbk)
        self.set_pre_read_callback(RoboCatClient.pre_read_clbk)
        self.set_post_read_callback(RoboCatClient.post_read_clbk)

    @staticmethod
    def StaticCreate():
        go = RoboCatClient()
        go.get_owner_object().reparent_to(common.render)
        return go

    @staticmethod
    def update_clbk(THIS):
        downTHIS = common.NetObjGameObjMap[THIS.this]
        # is this the cat owned by us?
        if downTHIS.GetPlayerId() == GameNetworkManagerClient.get_global_ptr().get_player_id():
            pendingMove = InputManager.get_global_ptr().get_pending_move()
            # in theory, only do this if we want to sample input this frame /
            # if there's a new move ( since we have to keep in sync with server
            # )
            if pendingMove:  # is it time to sample a new move...
                deltaTime = pendingMove.get_delta_time()
                # apply that input
                downTHIS.ProcessInput(deltaTime, pendingMove.get_input_state())
                # and simulate!
                downTHIS.SimulateMovement(deltaTime)
                # LOG( 'Client Move Time: %3.4f deltaTime: %3.4f left rot at %3.4f', latestMove.GetTimestamp(), deltaTime, get_rotation() )
        else:
            downTHIS.SimulateMovement(Timing.get_global_ptr().get_delta_time())
            if Is2DVectorEqual(downTHIS.GetVelocity(), LVector3f.zero()):
                # we're in sync if our velocity is 0
                downTHIS.mTimeLocationBecameOutOfSync = 0.0
        if ELY_DEBUG:
            GameObject.update_clbk(THIS)

    @staticmethod
    def handle_dying_clbk(THIS):
        GameObject.handle_dying_clbk(THIS)
        downTHIS = common.NetObjGameObjMap[THIS.this]
        # and if we're local, tell the hud so our health goes away!
        if downTHIS.GetPlayerId() == GameNetworkManagerClient.get_global_ptr().get_player_id():
            HUD.sInstance.SetPlayerHealth(0)

    @staticmethod
    def pre_read_clbk(THIS):
        downTHIS = common.NetObjGameObjMap[THIS.this]
        downTHIS.mOldRotation.assign(downTHIS.get_rotation())
        downTHIS.mOldLocation.assign(downTHIS.get_location())
        downTHIS.mOldVelocity.assign(downTHIS.GetVelocity())

    @staticmethod
    def post_read_clbk(THIS, readState):
        # r/w members . owner object synchronization
        RoboCat.post_read_clbk(THIS, readState)
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if downTHIS.GetPlayerId() == GameNetworkManagerClient.get_global_ptr().get_player_id():
            # did we get health? if so, tell the hud!
            if (readState.get_word() & RoboCat.ECatReplicationState.ECRS_Health) != 0:
                HUD.sInstance.SetPlayerHealth(downTHIS.GetHealth())
            downTHIS.DoClientSidePredictionAfterReplicationForLocalCat(
                readState.get_word())
            # if this is a create packet, don't interpolate
            if (readState.get_word() & RoboCat.ECatReplicationState.ECRS_PlayerId) == 0:
                downTHIS.InterpolateClientSidePrediction(downTHIS.mOldRotation,
                                                         downTHIS.mOldLocation, downTHIS.mOldVelocity, False)
        else:
            downTHIS.DoClientSidePredictionAfterReplicationForRemoteCat(
                readState.get_word())
            # will this smooth us out too? it'll interpolate us just 10% of the
            # way there...
            if (readState.get_word() & RoboCat.ECatReplicationState.ECRS_PlayerId) == 0:
                downTHIS.InterpolateClientSidePrediction(downTHIS.mOldRotation,
                                                         downTHIS.mOldLocation, downTHIS.mOldVelocity, True)

    def DoClientSidePredictionAfterReplicationForLocalCat(self, inReadState):
        if (inReadState & RoboCat.ECatReplicationState.ECRS_Pose) != 0:
            # simulate pose only if we received new pose- might have just gotten thrustDir
            # in which case we don't need to replay moves because we haven't warped backwards
            # all processed moves have been removed, so all that are left are unprocessed moves
            # so we must apply them...
            moveList = InputManager.get_global_ptr().get_move_list()
            for move in moveList:
                deltaTime = move.get_delta_time()
                self.ProcessInput(deltaTime, move.get_input_state())
                self.SimulateMovement(deltaTime)

    def DoClientSidePredictionAfterReplicationForRemoteCat(self, inReadState):
        if (inReadState & RoboCat.ECatReplicationState.ECRS_Pose) != 0:
            # simulate movement for an additional RTT
            rtt = GameNetworkManagerClient.get_global_ptr().get_round_trip_time()
            # LOG( 'Other cat came in, simulating for an extra %f', rtt )
            # let's break into framerate sized chunks though so that we don't
            # run through walls and do crazy things...
            deltaTime = 1.0 / 30.0
            while True:
                if rtt < deltaTime:
                    self.SimulateMovement(rtt)
                    break
                else:
                    self.SimulateMovement(deltaTime)
                    rtt -= deltaTime

    def InterpolateClientSidePrediction(self, inOldRotation, inOldLocation, inOldVelocity,
                                        inIsForRemoteCat):
        if (inOldRotation != self.get_rotation()) and (not inIsForRemoteCat):
            LOG('ERROR! Move replay ended with incorrect rotation!', 0)
        roundTripTime = GameNetworkManagerClient.get_global_ptr().get_round_trip_time()
        if not Is2DVectorEqual(inOldLocation, self.get_location()):
            # LOG( 'ERROR! Move replay ended with incorrect location!', 0 )
            # have we been out of sync, or did we just become out of sync?
            time = Timing.get_global_ptr().get_frame_start_time()
            if self.mTimeLocationBecameOutOfSync == 0.0:
                self.mTimeLocationBecameOutOfSync = time
            durationOutOfSync = time - self.mTimeLocationBecameOutOfSync
            if durationOutOfSync < roundTripTime:
                self.set_location(
                    Lerp(inOldLocation, self.get_location(),
                         (durationOutOfSync / roundTripTime) if inIsForRemoteCat else 0.1))
        else:
            # we're in sync
            self.mTimeLocationBecameOutOfSync = 0.0
        if not Is2DVectorEqual(inOldVelocity, self.GetVelocity()):
            # LOG( 'ERROR! Move replay ended with incorrect velocity!', 0 )
            # have we been out of sync, or did we just become out of sync?
            time = Timing.get_global_ptr().get_frame_start_time()
            if self.mTimeVelocityBecameOutOfSync == 0.0:
                self.mTimeVelocityBecameOutOfSync = time
            # now interpolate to the correct value...
            durationOutOfSync = time - self.mTimeVelocityBecameOutOfSync
            if durationOutOfSync < roundTripTime:
                self.SetVelocity(
                    Lerp(inOldVelocity, self.GetVelocity(),
                                (durationOutOfSync / roundTripTime) if inIsForRemoteCat else 0.1))
            # otherwise, fine...
        else:
            # we're in sync
            self.mTimeVelocityBecameOutOfSync = 0.0
