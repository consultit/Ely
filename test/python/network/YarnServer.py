'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import GameNetworkManagerServer, Timing
from Yarn import Yarn
from common import HALF_WORLD_WIDTH, GameObject
import common


class YarnServer(Yarn):
    '''
    YarnServer class.
    '''

    def __init__(self):
        '''
        Constructor
        '''

        # Yarn.__init__(self)
        super(YarnServer, self).__init__()
        self.mTimeToDie = float(0.0)
# *fixme
#         self.get_owner_object().remove_node()
#         # add a collision node to render
#         collSolid = CollisionSphere(0, 0, 0, get_collision_radius())
#         collNode = CollisionNode('collNode')
#         collNode.add_solid(collSolid)
#         collNodeNP = render.attach_new_node(collNode)
#         self.set_owner_object(collNodeNP)
# *
        # yarn lives a second...
        ttl = HALF_WORLD_WIDTH / self.mMuzzleSpeed if self.mMuzzleSpeed > 0 else 1.0
        self.mTimeToDie = Timing.get_global_ptr().get_frame_start_time() + ttl
        # add r/w members
        # set r/w members' dirty states
        # callbacks
        self.set_update_callback(YarnServer.update_clbk)
#         self.set_all_state_mask_callback (clbk)
        self.set_handle_dying_callback(YarnServer.handle_dying_clbk)
#         self.set_pre_write_callback(clbk)
#         self.set_post_write_callback(clbk)
#         self.set_pre_read_callback(clbk)
#         self.set_post_read_callback(clbk)

    @staticmethod
    def StaticCreate():
        go = YarnServer()
        go.get_owner_object().reparent_to(common.render)
        return GameNetworkManagerServer.get_global_ptr().register_and_return(go)

    @staticmethod
    def update_clbk(THIS):
        Yarn.update_clbk(THIS)
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if Timing.get_global_ptr().get_frame_start_time() > downTHIS.mTimeToDie:
            THIS.set_does_want_to_die(True)

    @staticmethod
    def handle_dying_clbk(THIS):
        GameObject.handle_dying_clbk(THIS)
        GameNetworkManagerServer.get_global_ptr().unregister_network_object(THIS)

    def HandleCollisionWithCat(self, inCat):
        if inCat.GetPlayerId() != self.GetPlayerId():
            # kill yourself!
            self.set_does_want_to_die(True)
            inCat.TakeDamage(self.GetPlayerId())
        return False
