'''
Created on Aug 7, 2018

@author: consultit
'''

from ely.network import constUInt32, NetworkObject
from ely.libtools import Utilities
from panda3d.core import LVecBase3f, LVector3f, BitMask32
from common import GameObject, Colors
from enum import IntEnum, unique
import common


class Mouse(GameObject):
    '''
    Mouse class.
    '''

    MOUS = constUInt32(b'M', b'O', b'U', b'S')

    @unique
    class EMouseReplicationState(IntEnum):
        EMRS_Pose = 1 << 0
        EMRS_Color = 1 << 1
        EMRS_AllState = EMRS_Pose | EMRS_Color

    def __init__(self):
        '''
        Constructor
        '''

        # GameObject.__init__(self, NetworkObject.get_class_id_int('MOUS'))
        super(Mouse, self).__init__(NetworkObject.get_class_id_int('MOUS'))
        self.set_owner_object(Utilities.load_model('eve'))
        self.set_scale(0.2)
        modelDims = LVecBase3f()
        modelDeltaCenter = LVector3f()
        Utilities.get_bounding_dimensions(self.get_owner_object(), modelDims,
                                          modelDeltaCenter)
        self.set_collision_radius(modelDims.get_y() / 2.0)
        self.set_rotation(LVecBase3f.zero())
        self.set_color(Colors.White)
        # add r/w members
        members = self.get_members()
        # set r/w members' dirty states
        members.set_dirty_state('posX', BitMask32(
            Mouse.EMouseReplicationState.EMRS_Pose))
        members.set_dirty_state('posY', BitMask32(
            Mouse.EMouseReplicationState.EMRS_Pose))
        members.set_dirty_state('posZ', 0)
        members.set_dirty_state('rotH', BitMask32(
            Mouse.EMouseReplicationState.EMRS_Pose))
        members.set_dirty_state('rotP', 0)
        members.set_dirty_state('rotR', 0)
        members.set_dirty_state('color', BitMask32(
            Mouse.EMouseReplicationState.EMRS_Color))
        # callbacks
    #    set_update_callback (clbk)
        self.set_all_state_mask_callback(Mouse.get_all_state_mask_clbk)
    #    self.set_handle_dying_callback(clbk)
        self.set_pre_write_callback(Mouse.pre_write_clbk)
    #    self.set_post_write_callback(clbk)
    #    self.set_pre_read_callback(clbk)
        self.set_post_read_callback(Mouse.post_read_clbk)

    @staticmethod
    def get_all_state_mask_clbk(networkObject):
        return Mouse.EMouseReplicationState.EMRS_AllState

    @staticmethod
    def pre_write_clbk(THIS, writeState):
        # owner object . r/w members synchronization
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if writeState.get_word() & Mouse.EMouseReplicationState.EMRS_Pose:
            downTHIS.SetPosVar(downTHIS.get_location())
            downTHIS.SetRotVar(downTHIS.get_rotation())
        if writeState.get_word() & Mouse.EMouseReplicationState.EMRS_Color:
            downTHIS.SetColVar(downTHIS.get_color())

    @staticmethod
    def post_read_clbk(THIS, readState):
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if readState.get_word() & Mouse.EMouseReplicationState.EMRS_Pose:
            downTHIS.set_location(downTHIS.GetPosVar())
            downTHIS.set_rotation(downTHIS.GetRotVar())
        if readState.get_word() & Mouse.EMouseReplicationState.EMRS_Color:
            downTHIS.set_color(downTHIS.GetColVar())

    def HandleCollisionWithCat(self, inCat):
        return False
