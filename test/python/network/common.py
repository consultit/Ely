'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import NetworkObject, Float
from panda3d.core import LPoint3f, LVecBase3f, LVecBase4f, TextNode, NodePath
import numpy
import random

NetObjGameObjMap = {}
GameObjToRemoveKey = []
render = NodePath()
ELY_DEBUG = True

# zoom hardcoded at 100...if we want to lock players on screen, this could
# be calculated from zoom
HALF_WORLD_HEIGHT = 12.5
HALF_WORLD_WIDTH = 15.0
WORLD_STEP = 1.0
WORLD_MIN = LPoint3f(-HALF_WORLD_WIDTH, -HALF_WORLD_HEIGHT, 0.0)
WORLD_MAX = LPoint3f(HALF_WORLD_WIDTH, HALF_WORLD_HEIGHT, 0.0)


def Is2DVectorEqual(inA, inB):
    return inA == inB


def Lerp(inA, inB, t):
    return inA + (inB - inA) * t


def GetRandomFloat():
    return numpy.float(random.random())


def GetRandomVector(inMin, inMax):
    r = LVecBase3f(GetRandomFloat(), GetRandomFloat(), GetRandomFloat())
    d = inMax - inMin
    d.componentwise_mult(r)
    return inMin + d


class GameObject(NetworkObject):
    '''
    GameObject class.
    '''

    def __init__(self, inCode):
        '''
        Constructor
        '''

        # NetworkObject.__init__(self, inCode)
        super(GameObject, self).__init__(inCode)
        NetObjGameObjMap[self.this] = self
        self.mCollisionRadius = 0.5
        self.posX = Float()
        self.posY = Float()
        self.posZ = Float()
        self.rotH = Float()
        self.rotP = Float()
        self.rotR = Float()
        self.color = LVecBase4f()
        # add r/w members
        members = self.get_members()
        # use assign to change its value
        members.add_variable_float('posX', self.posX)
        # use assign to change its value
        members.add_variable_float('posY', self.posY)
        # use assign to change its value
        members.add_variable_float('posZ', self.posZ)
        # use assign to change its value
        members.add_variable_float('rotH', self.rotH)
        # use assign to change its value
        members.add_variable_float('rotP', self.rotP)
        # use assign to change its value
        members.add_variable_float('rotR', self.rotR)
        # use assign to change its value
        members.add_variable_lvecbase4f('color', self.color)
        # set r/w members' dirty states
        # callbacks
#         self.set_update_callback(clbk)
#         self.set_all_state_mask_callback (clbk)
        self.set_handle_dying_callback(GameObject.handle_dying_clbk)
#         self.set_pre_write_callback(clbk)
#         self.set_post_write_callback(clbk)
#         self.set_pre_read_callback(clbk)
#         self.set_post_read_callback(clbk)
        if ELY_DEBUG:
            self.text = render.attach_new_node(TextNode('TextNode'))
            # self.text.set_p(-90.0)
            self.text.set_color(0.0, 0.0, 1.0, 1.0)  # blue
            self.text.set_scale(0.1)
            self.text.set_two_sided(True)
            self.text.set_billboard_point_eye()

    if ELY_DEBUG:

        @staticmethod
        def update_clbk(THIS):
            downTHIS = NetObjGameObjMap[THIS.this]
            loc = downTHIS.get_location()
            downTHIS.text.set_pos(loc.get_x(), loc.get_y(
            ), 4.5 * downTHIS.get_collision_radius())
            locStr = '(' + str(loc.get_x()) + ',' + str(loc.get_y()) + ')'
            downTHIS.text.node().set_text(locStr)

    @staticmethod
    def handle_dying_clbk(THIS):
        GameObjToRemoveKey.append(THIS.this)

    def set_rotation(self, inRotation):
        # should we normalize using fmodf?
        owner_object = self.get_owner_object()
        owner_object.set_hpr(-inRotation.get_x(), inRotation.get_y(),
                             inRotation.get_z())

    def get_rotation(self):
        owner_object = self.get_owner_object()
        return LVecBase3f(-owner_object.get_h(), owner_object.get_p(),
                          owner_object.get_r())

    def set_location(self, inLocation):
        owner_object = self.get_owner_object()
        owner_object.set_pos(inLocation)

    def get_location(self):
        owner_object = self.get_owner_object()
        return owner_object.get_pos()

    def set_scale(self, inScale):
        owner_object = self.get_owner_object()
        owner_object.set_scale(inScale)

    def get_scale(self):
        owner_object = self.get_owner_object()
        return owner_object.get_scale()

    def set_color(self, inColor):
        owner_object = self.get_owner_object()
        owner_object.set_color(inColor)

    def get_color(self):
        owner_object = self.get_owner_object()
        return owner_object.get_color()

    def set_collision_radius(self, inRadius):
        self.mCollisionRadius = inRadius

    def get_collision_radius(self):
        return self.mCollisionRadius

    def SetPosVar(self, pos):
        self.posX.assign(pos.get_x())
        self.posY.assign(pos.get_y())
        self.posZ.assign(pos.get_z())

    def GetPosVar(self):
        return LPoint3f(self.posX.value, self.posY.value, self.posZ.value)

    def SetRotVar(self, rot):
        self.rotH.assign(rot.get_x())
        self.rotP.assign(rot.get_y())
        self.rotR.assign(rot.get_z())

    def GetRotVar(self):
        return LVecBase3f(self.rotH.value, self.rotP.value, self.rotR.value)

    def SetColVar(self, col):
        self.color.assign(col)

    def GetColVar(self):
        return self.color


class Colors:
    Black = LVecBase4f(0.0, 0.0, 0.0, 1.0)
    White = LVecBase4f(1.0, 1.0, 1.0, 1.0)
    Red = LVecBase4f(1.0, 0.0, 0.0, 1.0)
    Green = LVecBase4f(0.0, 1.0, 0.0, 1.0)
    Blue = LVecBase4f(0.0, 0.0, 1.0, 1.0)
    LightYellow = LVecBase4f(1.0, 1.0, 0.88, 1.0)
    LightBlue = LVecBase4f(0.68, 0.85, 0.9, 1.0)
    LightPink = LVecBase4f(1.0, 0.71, 0.76, 1.0)
    LightGreen = LVecBase4f(0.56, 0.93, 0.56, 1.0)
