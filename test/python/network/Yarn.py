'''
Created on Aug 7, 2018

@author: consultit
'''

from ely.network import constUInt32, NetworkObject, Int8, Timing
from ely.libtools import Utilities
from common import GameObject, Colors
from panda3d.core import LVector3f, LVecBase3f, BitMask32
from enum import IntEnum, unique
import common


class Yarn(GameObject):
    '''
    Yarn class.
    '''

    YARN = constUInt32(b'Y', b'A', b'R', b'N')

    @unique
    class EYarnReplicationState(IntEnum):
        EYRS_Pose = 1 << 0
        EYRS_Color = 1 << 1
        EYRS_PlayerId = 1 << 2
        EYRS_AllState = EYRS_Pose | EYRS_Color | EYRS_PlayerId

    def __init__(self):
        '''
        Constructor
        '''

        # GameObject.__init__(self, NetworkObject.get_class_id_int('YARN'))
        super(Yarn, self).__init__(NetworkObject.get_class_id_int('YARN'))
        self.mVelocity = LVector3f()
        self.mMuzzleSpeed = float(6.0)
        self.mPlayerId = Int8(0)
        self.set_owner_object(Utilities.load_model('ball'))
        self.set_scale(0.5)
        modelDims = LVecBase3f()
        modelDeltaCenter = LVector3f()
        Utilities.get_bounding_dimensions(
            self.get_owner_object(), modelDims, modelDeltaCenter)
        self.set_collision_radius(modelDims.get_y() / 2.0)
        self.set_rotation(LVecBase3f.zero())
        self.set_color(Colors.White)
        # add r/w members
        members = self.get_members()
        # use assign to change its value
        members.add_variable_int8_t('mPlayerId', self.mPlayerId)
        # use assign to change its value
        members.add_variable_lvector3f('mVelocity', self.mVelocity)
        # set r/w members' dirty states
        members.set_dirty_state('posX', BitMask32(
            Yarn.EYarnReplicationState.EYRS_Pose))
        members.set_dirty_state('posY', BitMask32(
            Yarn.EYarnReplicationState.EYRS_Pose))
        members.set_dirty_state('posZ', 0)
        members.set_dirty_state('rotH', BitMask32(
            Yarn.EYarnReplicationState.EYRS_Pose))
        members.set_dirty_state('rotP', 0)
        members.set_dirty_state('rotR', 0)
        members.set_dirty_state('color', BitMask32(
            Yarn.EYarnReplicationState.EYRS_Color))
        members.set_dirty_state('mPlayerId', BitMask32(
            Yarn.EYarnReplicationState.EYRS_PlayerId))
        members.set_dirty_state('mVelocity', BitMask32(
            Yarn.EYarnReplicationState.EYRS_Pose))
        # callbacks
        self.set_update_callback(Yarn.update_clbk)
        self.set_all_state_mask_callback(Yarn.get_all_state_mask_clbk)
#         self.set_handle_dying_callback (clbk)
        self.set_pre_write_callback(Yarn.pre_write_clbk)
#         set_post_write_callback(clbk)
#         set_pre_read_callback(clbk)
        self.set_post_read_callback(Yarn.post_read_clbk)

    @staticmethod
    def update_clbk(THIS):
        downTHIS = common.NetObjGameObjMap[THIS.this]
        deltaTime = Timing.get_global_ptr().get_delta_time()
        downTHIS.set_location(downTHIS.get_location() +
                              downTHIS.mVelocity * deltaTime)
        # we'll let the cats handle the collisions

    @staticmethod
    def get_all_state_mask_clbk(networkObject):
        return Yarn.EYarnReplicationState.EYRS_AllState

    @staticmethod
    def pre_write_clbk(THIS, writeState):
        # owner object . r/w members synchronization
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if writeState.get_word() & Yarn.EYarnReplicationState.EYRS_Pose:
            downTHIS.SetPosVar(downTHIS.get_location())
            downTHIS.SetRotVar(downTHIS.get_rotation())
        if writeState.get_word() & Yarn.EYarnReplicationState.EYRS_Color:
            downTHIS.SetColVar(downTHIS.get_color())

    @staticmethod
    def post_read_clbk(THIS, readState):
        # r/w members . owner object synchronization
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if readState.get_word() & Yarn.EYarnReplicationState.EYRS_Pose:
            downTHIS.set_location(downTHIS.GetPosVar())
            downTHIS.set_rotation(downTHIS.GetRotVar())
        if readState.get_word() & Yarn.EYarnReplicationState.EYRS_Color:
            downTHIS.set_color(downTHIS.GetColVar())

    def SetVelocity(self, inVelocity):
        self.mVelocity.assign(inVelocity)

    def GetVelocity(self):
        return self.mVelocity

    def SetPlayerId(self, inPlayerId):
        self.mPlayerId.assign(inPlayerId)

    def GetPlayerId(self):
        return self.mPlayerId.value

    def InitFromShooter(self, inShooter):
        self.set_color(inShooter.get_color())
        self.SetPlayerId(inShooter.GetPlayerId())
        forward = inShooter.GetForwardVector()
        self.SetVelocity(inShooter.GetVelocity() + forward * self.mMuzzleSpeed)
        self.set_location(inShooter.get_location())  # + forward * 0.55f)
        self.set_rotation(inShooter.get_rotation())

    def HandleCollisionWithCat(self, inCat):
        # you hit a cat, so look like you hit a cat
        return False
