'''
Created on Aug 6, 2018

@author: consultit
'''

from Mouse import Mouse
from common import GameObject
import common

ELY_DEBUG = common.ELY_DEBUG


class MouseClient(Mouse):
    '''
    MouseClient class.
    '''

    def __init__(self):
        '''
        Constructor
        '''

        # Mouse.__init__(self)
        super(MouseClient, self).__init__()
        # add r/w members
        # set r/w members' dirty states
        # callbacks
        if ELY_DEBUG:
            self.set_update_callback(GameObject.update_clbk)
#         self.set_all_state_mask_callback(clbk)
#         self.set_handle_dying_callback(clbk)
#         self.set_pre_write_callback(clbk)
#         self.set_post_write_callback(clbk)
#         self.set_pre_read_callback(clbk)
#         self.set_post_read_callback(clbk)

    @staticmethod
    def StaticCreate():
        go = MouseClient()
        go.get_owner_object().reparent_to(common.render)
        return go
