'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import GameNetworkManagerServer
from Mouse import Mouse
from ScoreBoardManager import ScoreBoardManager
from common import GameObject
import common


class MouseServer(Mouse):
    '''
    MouseServer class.
    '''

    def __init__(self):
        '''
        Constructor
        '''

        # Mouse.__init__(self)
        super(MouseServer, self).__init__()
# * fixme
#         self.get_owner_object().remove_node()
#         # add a collision node to render
#         collSolid = CollisionSphere(0,0,0,self.get_collision_radius())
#         collNode = CollisionNode('collNode')
#         collNode.add_solid(collSolid)
#         collNodeNP = render.attach_new_node(collNode)
#         self.set_owner_object(collNodeNP)
# *
        # add r/w members
        # set r/w members' dirty states
        # callbacks
#         self.set_update_callback (clbk)
#         self.set_all_state_mask_callback(clbk)
        self.set_handle_dying_callback(MouseServer.handle_dying_clbk)
#         self.set_pre_write_callback(clbk)
#         self.set_post_write_callback(clbk)
#         self.set_pre_read_callback(clbk)
#         self.set_post_read_callback(clbk)

    @staticmethod
    def StaticCreate():
        go = MouseServer()
        go.get_owner_object().reparent_to(common.render)
        return GameNetworkManagerServer.get_global_ptr().register_and_return(go)

    @staticmethod
    def handle_dying_clbk(THIS):
        GameObject.handle_dying_clbk(THIS)
        GameNetworkManagerServer.get_global_ptr().unregister_network_object(THIS)

    def HandleCollisionWithCat(self, inCat):
        # kill yourself!
        self.set_does_want_to_die(True)
        ScoreBoardManager.sInstance.IncScore(inCat.GetPlayerId(), 1)
        return False
