'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import GameNetworkManagerClient
from ely.libtools import ValueList_NodePath
from panda3d.core import LPoint3f, LVecBase3f, LVector3f, FontPool, NodePath, \
    TextNode
from common import Colors
from ScoreBoardManager import ScoreBoardManager

app = None
window = None


class HUD(object):
    '''
    HUD class.
    '''

    def __init__(self):
        '''
        Constructor
        '''

        self.mScoreBoardOrigin = LPoint3f(50.0, 60.0, 0.0)
        self.mBandwidthOrigin = LPoint3f(50.0, 10.0, 0.0)
        self.mRoundTripTimeOrigin = LPoint3f(50.0, 10.0, 0.0)
        self.mScoreOffset = LVecBase3f(0.0, 50.0, 0.0)
        self.mHealthOffset = LVecBase3f(1000.0, 10.0, 0.0)
        self.mHealth = int(0)
        self.m_textNodes = ValueList_NodePath(size=0)
        self.m_textNodeIdx = int(0)
        self.m_prevTextNodeIdx = int(-1)
        self.m_textNodesSize = int(0)
        self.m_textScale = float(0.05)
        self.m_font = None
        self.Initialize()

    @staticmethod
    def StaticInit():
        global window
        if not HUD.sInstance:
            HUD.sInstance = HUD()
            win = window
            ratio = win.get_sbs_left_x_size() / win.get_sbs_left_y_size()
            x0 = -ratio * 0.85
            HUD.sInstance.mBandwidthOrigin = LPoint3f(x0, 0.0, 1.0 * 0.9)
            HUD.sInstance.mRoundTripTimeOrigin = LPoint3f(x0, 0.0, 1.0 * 0.8)
            HUD.sInstance.mScoreBoardOrigin = LPoint3f(x0, 0.0, 1.0 * 0.7)
            HUD.sInstance.mScoreOffset = LVector3f(0.0, 0.0, -0.1)
            HUD.sInstance.mHealthOffset = LVector3f(
                ratio * 0.65, 0.0, 1.0 * 0.9)
            HUD.sInstance.m_font = FontPool.load_font('Carlito-Regular.ttf')

    # singleton variable
    sInstance = None

    def Render(self):
        self.Initialize()
        self.RenderBandWidth()
        self.RenderRoundTripTime()
        self.RenderScoreBoard()
        self.RenderHealth()

    def SetPlayerHealth(self, inHealth):
        self.mHealth = int(inHealth)

    def RenderBandWidth(self):
        bandwidth = 'In {:d}  Bps, Out {:d} Bps'.format(
            int(GameNetworkManagerClient.get_global_ptr(
            ).get_bytes_received_per_second().get_value()),
            int(GameNetworkManagerClient.get_global_ptr().get_bytes_sent_per_second().get_value()))
        self.RenderText(bandwidth, self.mBandwidthOrigin, Colors.White)

    def RenderRoundTripTime(self):
        rttMS = int(GameNetworkManagerClient.get_global_ptr(
        ).get_avg_round_trip_time().get_value() * 1000.0)
        roundTripTime = 'RTT {:d} ms'.format(rttMS)
        self.RenderText(roundTripTime, self.mRoundTripTimeOrigin, Colors.White)

    def RenderScoreBoard(self):
        entries = ScoreBoardManager.sInstance.GetEntries()
        offset = LPoint3f()
        offset.assign(self.mScoreBoardOrigin)
        for entry in entries:
            self.RenderText(entry.GetFormattedNameScore(),
                            offset, entry.GetColor())
            offset[0] += self.mScoreOffset[0]
            offset[2] += self.mScoreOffset[2]

    def RenderHealth(self):
        if self.mHealth > 0:
            healthString = 'Health {:d}'.format(self.mHealth)
            self.RenderText(healthString, self.mHealthOffset, Colors.Red)

    def RenderText(self, inStr, origin, inColor):
        # dynamically allocate TextNodes if necessary
        if self.m_textNodeIdx >= self.m_textNodesSize:
            # allocate a new TextNode
            textNum = str(self.m_textNodeIdx)
            self.m_textNodes.add_value(
                NodePath(TextNode('TextNode-' + textNum)))
            # common TextNodes setup
            self.m_textNodes[-1].reparent_to(app.aspect2d)
            self.m_textNodes[-1].set_scale(self.m_textScale)
            self.m_textNodes[-1].set_bin('fixed', 50)
            self.m_textNodes[-1].set_depth_write(False)
            self.m_textNodes[-1].set_depth_test(False)
            self.m_textNodes[-1].set_billboard_point_eye()
            self.m_textNodes[-1].node().set_font(self.m_font)
            # update number of TextNodes
            self.m_textNodesSize = self.m_textNodes.get_num_values()
        # setup current TextNode
        self.m_textNodes[self.m_textNodeIdx].node().set_text(inStr)
        self.m_textNodes[self.m_textNodeIdx].set_pos(origin)
        self.m_textNodes[self.m_textNodeIdx].set_color(inColor)
        # increase index
        self.m_textNodeIdx += 1

    def Initialize(self):
        # clear text on not used text nodes
        for idx in range(self.m_textNodeIdx, self.m_prevTextNodeIdx, 1):
            self.m_textNodes[idx].node().clear_text()
        # reset current and previous TextNode indexes
        self.m_prevTextNodeIdx = self.m_textNodeIdx - 1
        self.m_textNodeIdx = 0
