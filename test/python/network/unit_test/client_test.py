'''
Created on Aug 10, 2018

@author: consultit
'''

import panda3d.core
import unittest
import suite
import ely.libtools
from direct.showbase.ShowBase import ShowBase
from panda3d.core import load_prc_file_data, LPoint3f
from ely.network import OutputMemoryBitStream, InputMemoryBitStream
from suite import RandInt32
# import through filenames
from importlib import util
import os
import pathlib
import sys
pDir = pathlib.Path(__file__).parent.absolute()
ppDir = pDir.parent.absolute()
sys.path.append(str(ppDir))
# common
spec = util.spec_from_file_location('common', os.path.join(ppDir, 'common.py'))
common = util.module_from_spec(spec)
spec.loader.exec_module(common)
Colors = common.Colors
# HUD
spec = util.spec_from_file_location('HUD', os.path.join(ppDir, 'HUD.py'))
HUD = util.module_from_spec(spec)
spec.loader.exec_module(HUD)
# ScoreBoardManager
spec = util.spec_from_file_location(
    'ScoreBoardManager', os.path.join(ppDir, 'ScoreBoardManager.py'))
ScoreBoardManager = util.module_from_spec(spec)
spec.loader.exec_module(ScoreBoardManager)
ScoreBoardManagerModule = ScoreBoardManager
ScoreBoardManager = ScoreBoardManagerModule.ScoreBoardManager

DIM = suite.DIM
MAX32 = suite.MAX32
MAX64 = suite.MAX64
MAXFLOAT = suite.MAXFLOAT
MAXDOUBLE = suite.MAXDOUBLE

modelDir = str(pDir)


def setUpModule():
    load_prc_file_data('', 'model-path ' + modelDir)


def tearDownModule():
    pass


class HUDTEST(unittest.TestCase):
    app = None

    def setUp(self):
        if not HUDTEST.app:
            #load_prc_file_data('', 'window-type    none')
            HUDTEST.app = ShowBase()

    def tearDown(self):
        pass

    def test(self):
        HUD.app = HUDTEST.app
        HUD.window = HUDTEST.app.win
        HUD.HUD.StaticInit()
        HUD.HUD.sInstance.RenderText(
            'Hello World!', LPoint3f.zero(), Colors.Green)
        self.assertTrue(True)

    @unittest.expectedFailure
    def testAppRun(self):
        HUDTEST.app.run()


class ScoreBoardManagerTEST(unittest.TestCase):

    def setUp(self):
        ScoreBoardManager.StaticInit()

    def tearDown(self):
        pass

    def test(self):
        mgr = ScoreBoardManager.sInstance
        mgr.AddEntry(1, 'player1')
        mgr.AddEntry(2, 'player2')
        #mgr.entries = ['player1', 'player2']
        self.assertEqual(mgr.GetEntry(1).GetPlayerName(), 'player1')
        mgr.IncScore(2, 10)
        self.assertEqual(mgr.GetEntry(2).GetScore(), 10)
        self.assertEqual(mgr.GetEntry(2).GetColor(), Colors.LightPink)
        mgr.RemoveEntry(1)
        #mgr.entries = ['player2']
        self.assertEqual(len(mgr.GetEntries()), 1)
        mgr.RemoveEntry(2)
        #mgr.entries = []
        self.assertEqual(len(mgr.GetEntries()), 0)

    def testStreams(self):
        mgr = ScoreBoardManager.sInstance
        self.assertEqual(len(mgr.GetEntries()), 0)
        numPlayer = abs(RandInt32())
        for playerId in range(1, numPlayer + 1):
            mgr.AddEntry(playerId, 'player' + str(playerId))
            mgr.IncScore(playerId, abs(RandInt32()))
        # write mgr's state
        outStream = OutputMemoryBitStream()
        mgr.Write(outStream)
        # reset mgr
        entriesOld = mgr.GetEntries()[:]
        playerIdsOld = [entry.GetPlayerId() for entry in entriesOld]
        for playerId in playerIdsOld:
            mgr.RemoveEntry(playerId)
        self.assertEqual(len(mgr.GetEntries()), 0)
        # restore mgr's state
        inStream = InputMemoryBitStream(
            outStream.get_buffer_ptr(), outStream.get_bit_length())
        mgr.Read(inStream)
        for playerId in range(1, numPlayer + 1):
            entry = mgr.GetEntry(playerId)
            entryOld = next(
                e for e in entriesOld if e.GetPlayerId() == playerId)
            entryAttrs = (entry.GetColor(), entry.GetPlayerName(), entry.GetScore(),
                          entry.GetFormattedNameScore())
            entryOldAttrs = (entryOld.GetColor(), entryOld.GetPlayerName(), entryOld.GetScore(),
                             entryOld.GetFormattedNameScore())
            self.assertEqual(entryAttrs, entryOldAttrs)


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((HUDTEST('test'), HUDTEST('testAppRun')))
    suite.addTests((ScoreBoardManagerTEST('test'),
                    ScoreBoardManagerTEST('testStreams')))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
