'''
Created on Aug 10, 2018

@author: consultit
'''

import panda3d.core
import unittest
import suite
from ely.network import NetworkObjectRegistry, NetworkObject, \
    GameNetworkManagerServer, NetworkWorld
from suite import RandFloat, RandInt32

DIM = suite.DIM
MAX32 = suite.MAX32
MAX64 = suite.MAX64
MAXFLOAT = suite.MAXFLOAT
MAXDOUBLE = suite.MAXDOUBLE


def setUpModule():
    pass


def tearDownModule():
    pass


class NetworkObjectTEST(unittest.TestCase):

    def setUp(self):
        if not GameNetworkManagerServer.get_global_ptr():
            self.netMgrServer = GameNetworkManagerServer()
        if not NetworkWorld.get_global_ptr():
            self.netWorld = NetworkWorld()
        if not NetworkObjectRegistry.get_global_ptr():
            self.netObjReg = NetworkObjectRegistry()

    def tearDown(self):
        pass

    @staticmethod
    def createObject1():
        go = NetworkObject(NetworkObject.get_class_id_int('OBJ1'))
        return GameNetworkManagerServer.get_global_ptr().register_and_return(go)

    def test1(self):
        print('NetworkObject creation 1')
        NetworkObjectRegistry.get_global_ptr().register_creation_function(
            NetworkObject.get_class_id_int('OCF1'), NetworkObjectTEST.createObject1)
        OBJ1 = NetworkObjectRegistry.get_global_ptr().create_network_object(
            NetworkObject.get_class_id_int('OCF1'))
        self.assertEqual(OBJ1.get_class_id(),
                         NetworkObject.get_class_id_int('OBJ1'))

    @staticmethod
    def createObject2():
        go = NetworkObject(NetworkObject.get_class_id_int('OBJ2'))
        return GameNetworkManagerServer.get_global_ptr().register_and_return(go)

    def test2(self):
        print('NetworkObject creation 2')
        NetworkObjectRegistry.get_global_ptr().register_creation_function(
            NetworkObject.get_class_id_int('OCF2'), NetworkObjectTEST.createObject2)
        OBJ2 = NetworkObjectRegistry.get_global_ptr().create_network_object(
            NetworkObject.get_class_id_int('OCF2'))
        self.assertEqual(OBJ2.get_class_id(),
                         NetworkObject.get_class_id_int('OBJ2'))
        NetworkWorld.get_global_ptr().remove_network_object(OBJ2)
        print('NetworkObject destruction 2')
        self.assertEqual(OBJ2.get_index_in_world(), -1)
        self.assertEqual(OBJ2.get_class_id(),
                         NetworkObject.get_class_id_int('OBJ2'))


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((NetworkObjectTEST('test1'), NetworkObjectTEST('test2')))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
