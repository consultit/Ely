'''
Created on Aug 7, 2018

@author: consultit
'''

from panda3d.core import LPoint3f, LVecBase3f, LVector3f, LVecBase4f, \
    LQuaternionf
import unittest
import suite
import ely.libtools
from ely.network import OutputMemoryBitStream, InputMemoryBitStream, AckRange, \
    TransmissionData, InFlightPacket, Int32, Int8, String, Float, Bool, \
    DataVariables, constUInt32
from suite import RandFloat, RandInt32

DIM = suite.DIM
MAX32 = suite.MAX32
MAX64 = suite.MAX64
MAXFLOAT = suite.MAXFLOAT
MAXDOUBLE = suite.MAXDOUBLE


def setUpModule():
    pass


def tearDownModule():
    pass


class MemoryStreamTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        outStream = OutputMemoryBitStream()
        inStream = InputMemoryBitStream(outStream.get_buffer_ptr(), 16)
        pStart = [LPoint3f()] * DIM
        pEnd = [LPoint3f()] * DIM
        vStrings = [String('Hello'), String('World'), String('!')]
        for i in range(DIM):
            pStart[i] = LPoint3f(RandFloat(), RandFloat(), RandFloat())
            outStream.write(pStart[i])
        for s in vStrings:
            outStream.write(s)
#         inStream = InputMemoryBitStream(outStream.get_buffer_ptr(), outStream.get_bit_length())
        for i in range(DIM):
            inStream.read(pEnd[i])
            self.assertEqual(pStart[i], pEnd[i])
        for s in vStrings:
            strS = String()
            inStream.read(strS)
            print(strS.value)
            self.assertEqual(strS.value, s.value)


class TransmissionDataTest(TransmissionData):

    def __init__(self):
        TransmissionData.__init__(self)
        self.i = 0

    def handle_delivery_failure(self, dnMgr):
        TransmissionData.handle_delivery_failure(self, dnMgr)

    def handle_delivery_success(self, dnMgr):
        TransmissionData.handle_delivery_success(self, dnMgr)


class PacketsTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        print('AckRange')
        outStream = OutputMemoryBitStream()
        ackRStart = AckRange(DIM)
        ackRStart.extend_if_should(DIM + 1)
        ackRStart.write(outStream)
        inStream = InputMemoryBitStream(
            outStream.get_buffer_ptr(), outStream.get_bit_length())
        ackREnd = AckRange()
        ackREnd.read(inStream)
        self.assertEqual(ackRStart.get_start(), ackREnd.get_start())
        self.assertEqual(ackRStart.get_count(), ackREnd.get_count())
        print('InFlightPacket')
        flightPacket = InFlightPacket(DIM)
        txData = TransmissionDataTest()
        txData.i = RandInt32()
        inKey = abs(int(RandInt32()))
        flightPacket.set_transmission_data(inKey, txData)
        transmissionDataEqual = (
            flightPacket.get_transmission_data(inKey) == txData)
        self.assertTrue(transmissionDataEqual)


class DataTypesTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def setValue(self, var, value):
        var.assign(value)

    def test(self):
        print('String')
        v = String()
        self.assertEqual(v.value, '')
        v.assign('s.assign()')
        self.assertEqual(v.value, 's.assign()')
        self.setValue(v, 'self.setValue()')
        self.assertEqual(v.value, 'self.setValue()')
        self.assertEqual(type(v.value), type(str()))
        v.assign(String('String'))
        self.assertEqual(v.value, 'String')
        self.assertTrue(v == String('String'))
        self.assertTrue(v == 'String')
        print('Float')
        v = Float()
        self.assertEqual(v.value, 0.0)
        v.assign(2.71828)
        self.assertAlmostEqual(v.value, 2.71828, delta=0.0001)
        self.setValue(v, 3.14159)
        self.assertAlmostEqual(v.value, 3.14159, delta=0.0001)
        self.assertEqual(type(v.value), type(float()))
        v.assign(Float(2.71828e10))
        self.assertAlmostEqual(v.value, 2.71828e10, delta=1000)
        self.assertTrue(v == Float(2.71828e10))
        self.assertTrue(v == 2.71828e10)
        print('Int32')
        v = Int32()
        self.assertEqual(v.value, 0)
        v.assign(271828)
        self.assertEqual(v.value, 271828)
        self.setValue(v, 314159)
        self.assertEqual(v.value, 314159)
        self.assertEqual(type(v.value), type(int()))
        v.assign(Int32(2048))
        self.assertEqual(v.value, 2048)
        self.assertTrue(v == Int32(2048))
        self.assertTrue(v == 2048)
        print('Int8')
        v = Int8()
        self.assertEqual(v.value, 0)
        v.assign(27)
        self.assertEqual(v.value, 27)
        self.setValue(v, -123)
        self.assertEqual(v.value, -123)
        self.assertEqual(type(v.value), type(int()))
        v.assign(Int8(23))
        self.assertEqual(v.value, 23)
        self.assertTrue(v == Int8(23))
        self.assertTrue(v == 23)
        print('Bool')
        v = Bool()
        self.assertEqual(v.value, False)
        v.assign(True)
        self.assertEqual(v.value, True)
        self.setValue(v, False)
        self.assertEqual(v.value, False)
        self.assertEqual(type(v.value), type(bool()))
        v.assign(Bool(False))
        self.assertEqual(v.value, False)
        self.assertTrue(v == Bool(False))
        self.assertTrue(v == False)


class DataVariablesTEST(unittest.TestCase):

    def setUp(self):
        self.Int32S = Int32(RandInt32())
        self.Int32E = Int32()
        self.StringS = String('Hello World')
        self.StringE = String()
        self.FloatS = Float(RandFloat())
        self.FloatE = Float()
        self.BoolS = Bool(True)
        self.BoolE = Bool()
        self.LVecBase3fS = LVecBase3f(RandFloat(), RandFloat(), RandFloat())
        self.LVecBase3fE = LVecBase3f()
        self.LPoint3fS = LPoint3f(RandFloat(), RandFloat(), RandFloat())
        self.LPoint3fE = LPoint3f()
        self.LVector3fS = LVector3f(RandFloat(), RandFloat(), RandFloat())
        self.LVector3fE = LVector3f()
        self.LVecBase4fS = LVecBase4f(
            RandFloat(), RandFloat(), RandFloat(), RandFloat())
        self.LVecBase4fE = LVecBase4f()
        self.LQuaternionfS = LQuaternionf(
            RandFloat(), RandFloat(), RandFloat(), RandFloat())
        self.LQuaternionfE = LQuaternionf()
        self.LQuaternionfS.normalize()
        self.dataVarsS = DataVariables()
        self.dataVarsE = DataVariables()
        self.outStream = OutputMemoryBitStream()
        self.inStream = InputMemoryBitStream(
            self.outStream.get_buffer_ptr(), 0)

    def tearDown(self):
        pass

    def FILLDATAORDERED(self, name, prim):
        self.dataVarsS.add_variable(name, prim)
        self.dataVarsE.add_variable(name, prim)

    def MAKEINTERNALCHECK(self, name, valueS, valueE, TYPE, typesuffix):
        valueE.assign(TYPE().get_value())
        getattr(self.dataVarsS, 'set_value_' + typesuffix)(name, valueS)
        getattr(self.dataVarsS, 'get_value_' + typesuffix)(name, valueE)
        self.assertEqual(valueS, valueE)

    def MAKEINTERNALCHECK1(self, name, valueS, valueE, TYPE, typesuffix):
        valueE.assign(TYPE())
        getattr(self.dataVarsS, 'set_value_' + typesuffix)(name, valueS)
        getattr(self.dataVarsS, 'get_value_' + typesuffix)(name, valueE)
        self.assertEqual(valueS, valueE)

    def MAKESTREAMCHECK(self, name, valueS, valueE, TYPE, typesuffix):
        valueE.assign(TYPE().get_value())
        getattr(self.dataVarsE, 'get_value_' + typesuffix)(name, valueE)
        self.assertEqual(valueE, valueS)

    def MAKESTREAMCHECK1(self, name, valueS, valueE, TYPE, typesuffix):
        valueE.assign(TYPE())
        getattr(self.dataVarsE, 'get_value_' + typesuffix)(name, valueE)
        self.assertEqual(valueE, valueS)

    def test(self):
        self.FILLDATAORDERED('mInt32', DataVariables.EPT_Int32)
        self.FILLDATAORDERED('mString', DataVariables.EPT_String)
        self.FILLDATAORDERED('mFloat', DataVariables.EPT_Float)
        self.FILLDATAORDERED('mBool', DataVariables.EPT_Bool)
        self.FILLDATAORDERED('mLVecBase3f', DataVariables.EPT_LVecBase3f)
        self.FILLDATAORDERED('mLPoint3f', DataVariables.EPT_LPoint3f)
        self.FILLDATAORDERED('mLVector3f', DataVariables.EPT_LVector3f)
        self.FILLDATAORDERED('mLVecBase4f', DataVariables.EPT_LVecBase4f)
        self.FILLDATAORDERED('mLQuaternionf', DataVariables.EPT_LQuaternionf)
        print('DataVariables INTERNAL CHECK')
        self.MAKEINTERNALCHECK('mInt32', self.Int32S,
                               self.Int32E, Int32, 'int32_t')
        self.MAKEINTERNALCHECK('mString', self.StringS,
                               self.StringE, String, 'string')
        self.MAKEINTERNALCHECK('mFloat', self.FloatS,
                               self.FloatE, Float, 'float')
        self.MAKEINTERNALCHECK('mBool', self.BoolS, self.BoolE, Bool, 'bool')
        self.MAKEINTERNALCHECK1(
            'mLVecBase3f', self.LVecBase3fS, self.LVecBase3fE, LVecBase3f, 'lvecbase3f')
        self.MAKEINTERNALCHECK1(
            'mLPoint3f', self.LPoint3fS, self.LPoint3fE, LPoint3f, 'lpoint3f')
        self.MAKEINTERNALCHECK1(
            'mLVector3f', self.LVector3fS, self.LVector3fE, LVector3f, 'lvector3f')
        self.MAKEINTERNALCHECK1(
            'mLVecBase4f', self.LVecBase4fS, self.LVecBase4fE, LVecBase4f, 'lvecbase4f')
        self.MAKEINTERNALCHECK1('mLQuaternionf', self.LQuaternionfS,
                                self.LQuaternionfE, LQuaternionf, 'lquaternionf')
        print('DataVariables STREAM CHECK')
        self.dataVarsS.write(self.outStream)
        self.dataVarsE.read(self.inStream)
        self.MAKESTREAMCHECK('mInt', self.Int32S,
                             self.Int32E, Int32, 'int32_t')
        self.MAKESTREAMCHECK('mString', self.StringS,
                             self.StringE, String, 'string')
        self.MAKESTREAMCHECK('mFloat', self.FloatS,
                             self.FloatE, Float, 'float')
        self.MAKESTREAMCHECK('mBool', self.BoolS, self.BoolE, Bool, 'bool')
        self.MAKESTREAMCHECK1('mLVecBase3f', self.LVecBase3fS,
                              self.LVecBase3fE, LVecBase3f, 'lvecbase3f')
        self.MAKESTREAMCHECK1('mLPoint3f', self.LPoint3fS,
                              self.LPoint3fE, LPoint3f, 'lpoint3f')
        self.MAKESTREAMCHECK1('mLVector3f', self.LVector3fS,
                              self.LVector3fE, LVector3f, 'lvector3f')
        self.MAKESTREAMCHECK1('mLVecBase4f', self.LVecBase4fS,
                              self.LVecBase4fE, LVecBase4f, 'lvecbase4f')
        # #streams' R/W for LQuaternionf use 'fixed point' approximation
        self.LQuaternionfE.assign(LQuaternionf())
        self.dataVarsE.get_value_lquaternionf(
            'mLQuaternionf', self.LQuaternionfE)
        tolerance = MAXFLOAT * (2.0 / 65535.0)
        for i in range(4):
            self.assertAlmostEqual(
                self.LQuaternionfE[i], self.LQuaternionfS[i], delta=tolerance)

    @unittest.expectedFailure
    def testQuat(self):
        print('LQuaternionf: Expected Failure')
        self.FILLDATAORDERED('mLQuaternionf', DataVariables.EPT_LQuaternionf)
        self.MAKEINTERNALCHECK1('mLQuaternionf', self.LQuaternionfS,
                                self.LQuaternionfE, LQuaternionf, 'lquaternionf')
        self.MAKESTREAMCHECK1('mLQuaternionf', self.LQuaternionfS,
                              self.LQuaternionfE, LQuaternionf, 'lquaternionf')


class GameNetworkManagerTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        print('constUInt32')
        id32Str = 'ABCD'
        id32 = constUInt32(bytes(id32Str[0], encoding='utf-8'), bytes(id32Str[1], encoding='utf-8'),
                           bytes(id32Str[2], encoding='utf-8'), bytes(id32Str[3], encoding='utf-8'))
        id32Py = ((ord(id32Str[0]) & 0x000000FF) << 24 |
                  (ord(id32Str[1]) & 0x000000FF) << 16 |
                  (ord(id32Str[2]) & 0x000000FF) << 8 |
                  (ord(id32Str[3]) & 0x000000FF))
        self.assertEqual(id32, id32Py)
        id32 = constUInt32(b'E', b'F', b'G', b'H')
        id32Py = ((ord('E') & 0x000000FF) << 24 | (ord('F') & 0x000000FF) << 16 |
                  (ord('G') & 0x000000FF) << 8 | (ord('H') & 0x000000FF))
        self.assertEqual(id32, id32Py)


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTest(MemoryStreamTEST('test'))
    suite.addTest(PacketsTEST('test'))
    suite.addTest(DataTypesTEST('test'))
    suite.addTests((DataVariablesTEST('test'), DataVariablesTEST('testQuat')))
    suite.addTest(GameNetworkManagerTEST('test'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
