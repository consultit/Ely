'''
Created on Oct 20, 2018

@author: consultit
'''

import panda3d.core
import suite
from ely.network import NetworkObjectRegistry, NetworkObject, \
    GameNetworkManagerServer, NetworkWorld
from suite import RandFloat, RandInt32
from panda3d.core import NodePath
import sys

DIM = suite.DIM
MAX32 = suite.MAX32
MAX64 = suite.MAX64
MAXFLOAT = suite.MAXFLOAT
MAXDOUBLE = suite.MAXDOUBLE


def initTests():
    global netMgrServer, netWorld, netObjReg
    netMgrServer = GameNetworkManagerServer()
    netWorld = NetworkWorld()
    netObjReg = NetworkObjectRegistry()

###


def createObject():
    go = NetworkObject(NetworkObject.get_class_id_int('OBJ1'))
    return GameNetworkManagerServer.get_global_ptr().register_and_return(go)


def testCreateDestory(i):
    OBJ1 = NetworkObjectRegistry.get_global_ptr().create_network_object(
        NetworkObject.get_class_id_int('OCF1'))
    print(OBJ1.get_class_id() == NetworkObject.get_class_id_int('OBJ1'))
    print(OBJ1.get_index_in_world())
    NetworkWorld.get_global_ptr().remove_network_object(OBJ1)
    GameNetworkManagerServer.get_global_ptr().unregister_network_object(OBJ1)
    print(OBJ1.get_class_id() == NetworkObject.get_class_id_int('OBJ1'))
    print(OBJ1.get_index_in_world() == -1)
    GameNetworkManagerServer.get_global_ptr().register_and_return(OBJ1)
    NetworkWorld.get_global_ptr().add_network_object(OBJ1)
    print(OBJ1.get_index_in_world())
    NetworkWorld.get_global_ptr().remove_network_object(OBJ1)
    GameNetworkManagerServer.get_global_ptr().unregister_network_object(OBJ1)
    print(OBJ1.get_index_in_world() == -1)
    print('THE END' + str(i))


def makeTestCreateDestory():
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('OCF1'), createObject)
    for i in range(10000000):
        testCreateDestory(i)
    print('END OF makeTestCreateDestory')

###


class GameObjectDerived(NetworkObject):

    def __init__(self, inCode):
        global NetObjGameObjMapDerived
        super(GameObjectDerived, self).__init__(inCode)
        NetObjGameObjMapDerived[self.this] = self
        self.set_owner_object(NodePath('NodePath'))


def createGameObjectDerived():
    go = GameObjectDerived(NetworkObject.get_class_id_int('GODV'))
    return go


def makeTestGameObjectDerived():
    global NetObjGameObjMapDerived
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('GODV'), createGameObjectDerived)
    NGOBJ1 = NetworkObjectRegistry.get_global_ptr().create_network_object(
        NetworkObject.get_class_id_int('GODV'))
    print(NGOBJ1.get_ref_count())
    print(sys.getrefcount(NGOBJ1))
    print(NetObjGameObjMapDerived[NGOBJ1.this].get_ref_count())
    print(sys.getrefcount(NetObjGameObjMapDerived[NGOBJ1.this]))
    NetworkWorld.get_global_ptr().remove_network_object(NGOBJ1)
    print(NGOBJ1.get_ref_count())
    print(sys.getrefcount(NGOBJ1))
    print(NetObjGameObjMapDerived[NGOBJ1.this].get_ref_count())
    print(sys.getrefcount(NetObjGameObjMapDerived[NGOBJ1.this]))
    print('END OF makeTestGameObjectDerived')

###


class GameObjectComposed(object):

    def __init__(self, inCode):
        global NetObjGameObjMapComposed
        super(GameObjectComposed, self).__init__()
        self.netObj = NetworkObject(inCode)
#         NetObjGameObjMapComposed[self.netObj.this] = self
        self.netObj.set_owner_object(NodePath('NodePath'))


def createGameObjectComposed():
    go = GameObjectComposed(NetworkObject.get_class_id_int('GOCM'))
    return go.netObj


def makeTestGameObjectComposed():
    global NetObjGameObjMapComposed
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('GOCM'), createGameObjectComposed)
#     NGOBJ1 = NetworkObjectRegistry.get_global_ptr().create_network_object(
#         NetworkObject.get_class_id_int('GOCM'))
#     print(NGOBJ1.get_ref_count())
#     print(sys.getrefcount(NGOBJ1))
#     print(NetObjGameObjMapComposed[NGOBJ1.this].netObj.get_ref_count())
#     print(sys.getrefcount(NetObjGameObjMapComposed[NGOBJ1.this]))
#     NetworkWorld.get_global_ptr().remove_network_object(NGOBJ1)
#     print(NGOBJ1.get_ref_count())
#     print(sys.getrefcount(NGOBJ1))
#     print(NetObjGameObjMapComposed[NGOBJ1.this].netObj.get_ref_count())
#     print(sys.getrefcount(NetObjGameObjMapComposed[NGOBJ1.this]))
    NetworkObjectRegistry.get_global_ptr().create_network_object(
        NetworkObject.get_class_id_int('GOCM'))
    for netObj in NetworkWorld.get_global_ptr().get_network_objects():
        print(netObj.get_ref_count())
        print(sys.getrefcount(netObj))
    for netObj in NetworkWorld.get_global_ptr().get_network_objects():
        NetworkWorld.get_global_ptr().remove_network_object(netObj)
    print('END OF makeTestGameObjectComposed')


def UpdateCallback(netObject):
    pass


def AllStateMaskCallback(netObject):
    pass


def HandleDyingCallback(netObject):
    pass


def PreWriteCallback(netObject, bitMask):
    pass


def PostWriteCallback(netObject):
    pass


def PreReadCallback(netObject):
    pass


def PostReadCallback(netObject, bitMask):
    pass


def makeTestCallBacks():
    netObj = NetworkObject(NetworkObject.get_class_id_int('NETO'))
    netObj.set_update_callback(UpdateCallback)
    netObj.set_all_state_mask_callback(AllStateMaskCallback)
    netObj.set_handle_dying_callback(HandleDyingCallback)
    netObj.set_pre_write_callback(PreWriteCallback)
    netObj.set_post_write_callback(PostWriteCallback)
    netObj.set_pre_read_callback(PreReadCallback)
    netObj.set_post_read_callback(PostReadCallback)
    # random
    numCycles = RandInt32() % 10
    for i in range(numCycles):
        flag = RandInt32() % 2
        netObj.set_update_callback(UpdateCallback if flag else None)
        flag = RandInt32() % 2
        netObj.set_all_state_mask_callback(
            AllStateMaskCallback if flag else None)
        flag = RandInt32() % 2
        netObj.set_handle_dying_callback(HandleDyingCallback if flag else None)
        flag = RandInt32() % 2
        netObj.set_pre_write_callback(PreWriteCallback if flag else None)
        flag = RandInt32() % 2
        netObj.set_post_write_callback(PostWriteCallback if flag else None)
        flag = RandInt32() % 2
        netObj.set_pre_read_callback(PreReadCallback if flag else None)
        flag = RandInt32() % 2
        netObj.set_post_read_callback(PostReadCallback if flag else None)


netMgrServer = None
netWorld = None
netObjReg = None
NetObjGameObjMapDerived = {}
NetObjGameObjMapComposed = {}

if __name__ == '__main__':
    initTests()
#     makeTestCreateDestory()
#     makeTestGameObjectDerived()
#     makeTestGameObjectComposed()
    makeTestCallBacks()
    print('THE END')
