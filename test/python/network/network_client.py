'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import Timing, SocketUtil, SocketAddressFactory, \
    GameNetworkManagerClient, NetworkWorld, InputStateFactory, InputManager, \
    NetworkObjectRegistry, NetworkObject
from ely.control import GameControlManager
from RoboInputState import RoboInputState
from ScoreBoardManager import ScoreBoardManager
from HUD import HUD
from PandaClient import PandaClient
from RoboCatClient import RoboCatClient
from MouseClient import MouseClient
from YarnClient import YarnClient
from common import ELY_DEBUG, HALF_WORLD_WIDTH, HALF_WORLD_HEIGHT, WORLD_STEP
import common
from panda3d.core import load_prc_file_data, WindowProperties, NodePath, \
    BitMask32, LineSegs, LPoint3f
from direct.showbase.ShowBase import ShowBase
import os
import argparse
import textwrap
import sys
from numpy import rint, arange

LOG = print

# # #SOURCES


def main():
    global parseArguments, loadPandaFramework, setupPandaFramework, \
        loadAndSetupNetworkFramework, loadAndSetupGameFramework, atExitCleanup, \
        msg, framework, address, player
    # #INTIALIZATION
    parseArguments()
    msg = 'ClientMain - address: \'' + address + '\' - name: \'' + player + '\''
    # #SETUP
    loadPandaFramework()
    setupPandaFramework()
    loadAndSetupNetworkFramework()
    loadAndSetupGameFramework()
    # #ATEXIT
    framework.win.set_close_request_event('close_request_event')
    framework.accept('close_request_event', atExitCleanup)
    # #RUN
    framework.run()


def parseArguments():
    '''parse arguments'''
    global address, player, msg, windowOn, dirs, timeBetweenInputs, \
        delayBeforeAck, timeBetweenHellos, timeBetweenInputPackets
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''
    This script will perform ClientMain.
    '''))
    # set up arguments
    parser.add_argument('-a', '--address', type=str, default='127.0.0.1:45000',
                        help='the server ipaddress:port')
    parser.add_argument('-u', '--user-name', type=str, default='player',
                        help='the player''s user name')
    parser.add_argument('-w', '--window-on', action='store_true',
                        help='window on/off flag')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    parser.add_argument('-i', '--time-between-inputs', type=float, default=0.03,
                        help='the time between inputs')
    parser.add_argument('-k', '--delay-before-ack', type=float, default=0.5,
                        help='the delay before ack')
    parser.add_argument('-l', '--time-between-hellos', type=float, default=1.0,
                        help='the time between hellos')
    parser.add_argument('-p', '--time-between-input-packets', type=float,
                        default=0.033, help='the time between input packets')
    # parse arguments
    args = parser.parse_args()
    # do actions
    address = args.address
    player = args.user_name
    windowOn = args.window_on
    if not windowOn:
        windowOn = True
    dirs = args.data_dir
    timeBetweenInputs = args.time_between_inputs
    delayBeforeAck = args.delay_before_ack
    timeBetweenHellos = args.time_between_hellos
    timeBetweenInputPackets = args.time_between_input_packets


def loadPandaFramework():
    '''Load your application's configuration'''

    global dataDirs, dirs, windowOn, msg, framework, window, render
    for dataDir in dataDirs:
        load_prc_file_data('', 'model-path ' + dataDir)
    if dirs:
        for dataDir in dirs:
            load_prc_file_data('', 'model-path ' + dataDir)
    if windowOn:
        load_prc_file_data('', 'win-size 1024 768')
        load_prc_file_data('', 'show-frame-rate-meter #t')
        load_prc_file_data('', 'sync-video #t')
        # load_prc_file_data('', 'want-directtools #t')
        # load_prc_file_data('', 'want-tk #t')
    else:
        load_prc_file_data('', 'window-type    none')
    # Setup your application
    framework = ShowBase()
    if windowOn:
        window = framework.win
        props = WindowProperties()
        props.setTitle(msg)
        window.requestProperties(props)
        print('Opened the window successfully!')
        render = framework.render
        # place camera
        trackball = framework.trackball.node()
        trackball.set_pos(0.0, 60.0, -5.0)
        trackball.set_hpr(0.0, 20.0, 0.0)
    else:
        render = NodePath('render')
        print(msg)
    common.render = render


keyDefs = {
    'forward': 'w', 'forwardStop': 'w-up',
    'backward': 's', 'backwardStop': 's-up',
    'left': 'a', 'leftStop': 'a-up',
    'right': 'd', 'rightStop': 'd-up',
    'shoot': 'k', 'shootStop': 'k-up',
    # P3Driver
    'drv_forward': 'arrow_up', 'drv_forwardStop': 'arrow_up-up',
    'drv_backward': 'arrow_down', 'drv_backwardStop': 'arrow_down-up',
    'drv_left': 'arrow_left', 'drv_leftStop': 'arrow_left-up',
    'drv_right': 'arrow_right', 'drv_rightStop': 'arrow_right-up',
}
if ELY_DEBUG:
    keyDefsDbg = {
        'latencyInc': 't', 'latencyDec': 'shift-t',
        'dropPacketInc': 'y', 'dropPacketDec': 'shift-y',
        'jitterInc': 'u', 'jitterDec': 'shift-u',
    }
    keyDefs.update(keyDefsDbg)


def setupPandaFramework():
    '''setup panda framework'''

    global controlMgr, render, framework, doFrameSort, handleEventClient, \
        doFrameClient, drawField
    # create a control manager
    controlMgr = GameControlManager(
        framework.win, 5, render, BitMask32.all_on())
    # reparent the reference node to render
    controlMgr.get_reference_node_path().reparent_to(render)
# * fixme manual update of driver
    # start the default update task for all drivers
#     controlMgr.start_default_update()
# *
    # add event handling
    for keyDef in keyDefs:
        framework.accept(keyDefs[keyDef], handleEventClient, [keyDefs[keyDef]])
    # create the task for DoFrameClient
    # Adds mDoFrameTask to the active queue.
    framework.taskMgr.add(doFrameClient, 'DoFrameClient', doFrameSort,
                          appendTask=True)
    # draw field
    drawField(render)


def loadAndSetupNetworkFramework():
    '''load and setup network framework'''

    global address, player, timeBetweenInputs, timeMgr, netMgrClient, \
        netWorld, inpStateFactory, roboInpState, inpMgr, netObjRegistry
    timeMgr = Timing()
    SocketUtil.static_init()
    serverAddress = SocketAddressFactory.create_ipv4_from_string(address)
    netMgrClient = GameNetworkManagerClient()
    netMgrClient.init(serverAddress, player)
    netWorld = NetworkWorld()
    # manage custom InputState
    inpStateFactory = InputStateFactory()
    roboInpState = RoboInputState(
        InputStateFactory.get_global_ptr().get_input_state())
    inpMgr = InputManager()
    inpMgr.get_global_ptr().set_time_between_input_samples(timeBetweenInputs)
    netObjRegistry = NetworkObjectRegistry()


def loadAndSetupGameFramework():
    '''load and setup game framework'''

    global delayBeforeAck, timeBetweenHellos, timeBetweenInputPackets, \
        HandleScoreBoardState, framework
    ScoreBoardManager.StaticInit()
    import HUD as HUD_module
    HUD_module.app = framework
    HUD_module.window = framework.win
    HUD.StaticInit()
    #
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('PAND'), PandaClient.StaticCreate)
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('RCAT'), RoboCatClient.StaticCreate)
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('MOUS'), MouseClient.StaticCreate)
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('YARN'), YarnClient.StaticCreate)
    #
    GameNetworkManagerClient.get_global_ptr(
    ).set_handle_state_packet_callback(HandleScoreBoardState)
    GameNetworkManagerClient.get_global_ptr().set_delay_before_ack_timeout(delayBeforeAck)
    GameNetworkManagerClient.get_global_ptr().set_time_between_hellos(timeBetweenHellos)
    GameNetworkManagerClient.get_global_ptr(
    ).set_time_between_input_packets(timeBetweenInputPackets)


def atExitCleanup():
    '''at exit cleanup'''

    global framework, timeMgr, netMgrClient, netWorld, inpStateFactory, \
        roboInpState, inpMgr, netObjRegistry
    framework.taskMgr.remove('DoFrameClient')
    del netObjRegistry
    del inpMgr
    del roboInpState
    del inpStateFactory
    del netWorld
    del netMgrClient
    SocketUtil.clean_up()
    del timeMgr
    sys.exit(0)

# #


def UpdateDesireVariableFromKey(down):
    '''update desire Variable from key'''

    if (down):
        ioVariable = True
    else:
        ioVariable = False
    return ioVariable


def UpdateDesireFloatFromKey(down):
    '''update desire float from key'''

    if (down):
        ioVariable = 1.0
    else:
        ioVariable = 0.0
    return ioVariable


def handleEventClient(inEvent):
    '''handle event Client'''

    global keyDefs
    name = inEvent
    state = RoboInputState.get_global_ptr()
    inInputState = InputManager.get_global_ptr().get_input_state()
    if (name == keyDefs['forward']) or (name == keyDefs['forwardStop']):
        state.SetDesiredForwardAmount(inInputState,
                                      UpdateDesireFloatFromKey(name == keyDefs['forward']))
    if (name == keyDefs['backward']) or (name == keyDefs['backwardStop']):
        state.SetDesiredBackAmount(inInputState,
                                   UpdateDesireFloatFromKey(name == keyDefs['backward']))
    if (name == keyDefs['left']) or (name == keyDefs['leftStop']):
        state.SetDesiredLeftAmount(inInputState,
                                   UpdateDesireFloatFromKey(name == keyDefs['left']))
    if (name == keyDefs['right']) or (name == keyDefs['rightStop']):
        state.SetDesiredRightAmount(inInputState,
                                    UpdateDesireFloatFromKey(name == keyDefs['right']))
    if (name == keyDefs['shoot']) or (name == keyDefs['shootStop']):
        state.SetIsShooting(inInputState,
                            UpdateDesireVariableFromKey(name == keyDefs['shoot']))
    # P3Driver
    if (name == keyDefs['drv_forward']) or (name == keyDefs['drv_forwardStop']):
        state.set_move_forward(inInputState, True if name ==
                               keyDefs['drv_forward'] else False)
    if (name == keyDefs['drv_backward']) or (name == keyDefs['drv_backwardStop']):
        state.set_move_backward(
            inInputState, True if name == keyDefs['drv_backward'] else False)
    if (name == keyDefs['drv_left']) or (name == keyDefs['drv_leftStop']):
        state.set_rotate_head_left(
            inInputState, True if name == keyDefs['drv_left'] else False)
    if (name == keyDefs['drv_right']) or (name == keyDefs['drv_rightStop']):
        state.set_rotate_head_right(
            inInputState, True if name == keyDefs['drv_right'] else False)
    elif ELY_DEBUG:
        # latency, packet drop, jitter simulation
        latency = GameNetworkManagerClient.get_global_ptr().get_simulated_latency()
        chance = GameNetworkManagerClient.get_global_ptr().get_drop_packet_chance()
        jitter = GameNetworkManagerClient.get_global_ptr().get_simulated_max_jitter()
        if (name == keyDefs['latencyInc']) or (name == keyDefs['latencyDec']):
            if name == keyDefs['latencyInc']:
                latency += 0.1
                if latency > 0.5:
                    latency = 0.5
            else:
                latency -= 0.1
                if latency < 0.0:
                    latency = 0.0
            GameNetworkManagerClient.get_global_ptr().set_simulated_latency(latency)
            LOG('Simulated Latency: %f', latency)
        elif (name == keyDefs['dropPacketInc']) or (name == keyDefs['dropPacketDec']):
            if name == keyDefs['dropPacketInc']:
                chance += 0.05
                if chance > 1.0:
                    chance = 1.0
            else:
                chance -= 0.05
                if chance < 0.0:
                    chance = 0.0
            GameNetworkManagerClient.get_global_ptr().set_drop_packet_chance(chance)
            LOG('Drop Packet Chance: %f', chance)
        elif (name == keyDefs['jitterInc']) or (name == keyDefs['jitterDec']):
            if name == keyDefs['jitterInc']:
                jitter += 0.1
                if jitter > 0.5:
                    jitter = 0.5
            else:
                jitter -= 0.1
                if jitter < 0.0:
                    jitter = 0.0
            GameNetworkManagerClient.get_global_ptr().set_simulated_max_jitter(jitter)
            LOG('Simulated Max Jitter: %f', jitter)


def doFrameClient(task):
    '''do frame client'''

    global removeGameObjects
    Timing.get_global_ptr().update()
    InputManager.get_global_ptr().update()
    NetworkWorld.get_global_ptr().update()
    # the pending move has been processed by all objects: clear
    InputManager.get_global_ptr().clear_pending_move()
    GameNetworkManagerClient.get_global_ptr().process_incoming_packets()
    HUD.sInstance.Render()
    GameNetworkManagerClient.get_global_ptr().send_outgoing_packets()
    removeGameObjects()
    return task.cont


def removeGameObjects():
    while common.GameObjToRemoveKey:
        goKey = common.GameObjToRemoveKey.pop()
        go = common.NetObjGameObjMap[goKey]
        del common.NetObjGameObjMap[goKey]
        if ELY_DEBUG:
            go.text.remove_node()
        print('removed ' + str(go))


def HandleScoreBoardState(inInputStream):
    '''handle score board state'''

    ScoreBoardManager.sInstance.Read(inInputStream)


def drawField(render):
    field = LineSegs('field')
    field.set_color(1.0, 1.0, 0.0, 1.0)  # yellow
    field.set_thickness(1.0)
    A = LPoint3f(-HALF_WORLD_WIDTH, -HALF_WORLD_HEIGHT, 0.0)
    B = LPoint3f(HALF_WORLD_WIDTH, -HALF_WORLD_HEIGHT, 0.0)
    C = LPoint3f(HALF_WORLD_WIDTH, HALF_WORLD_HEIGHT, 0.0)
    D = LPoint3f(-HALF_WORLD_WIDTH, HALF_WORLD_HEIGHT, 0.0)
    # lower edge
    field.move_to(A)
    field.draw_to(B)
    # right edge
    field.move_to(B)
    field.draw_to(C)
    # upper edge
    field.move_to(C)
    field.draw_to(D)
    # left edge
    field.move_to(D)
    field.draw_to(A)
    # draw grid
    # columns
    for x in arange(-HALF_WORLD_WIDTH, HALF_WORLD_WIDTH, WORLD_STEP):
        if abs(x) < WORLD_STEP * 0.01:
            field.set_color(0.0, 1.0, 0.0, 1.0)  # green
        elif abs(x - rint(x)) < WORLD_STEP * 0.01:
            field.set_color(0.5, 0.25, 0.0, 1.0)  # brown
        else:
            field.set_color(1.0, 0.5, 0.0, 1.0)  # orange
        field.move_to(x, HALF_WORLD_HEIGHT, 0)
        field.draw_to(x, -HALF_WORLD_HEIGHT, 0)
    # rows
    for y in arange(HALF_WORLD_HEIGHT, -HALF_WORLD_HEIGHT, -WORLD_STEP):
        if abs(y) < WORLD_STEP * 0.01:
            field.set_color(1.0, 0.0, 0.0, 1.0)  # red
        elif abs(y - rint(y)) < WORLD_STEP * 0.01:
            field.set_color(0.5, 0.25, 0.0, 1.0)  # brown
        else:
            field.set_color(1.0, 0.5, 0.0, 1.0)  # orange
        field.move_to(-HALF_WORLD_WIDTH, y, 0)
        field.draw_to(HALF_WORLD_WIDTH, y, 0)
    # add to render
    render.attach_new_node(field.create())


# # global declarations/definitions
framework = None
window = None
render = None
scriptPath = os.path.dirname(os.path.abspath(__file__))
prefD = os.path.join(scriptPath, '..', 'assets')
dataDirs = [
    os.path.join(prefD, 'misc', 'test'),
    os.path.join(prefD, 'models', 'test'),
    os.path.join(prefD, 'textures', 'test'),
    os.path.join(prefD, 'sounds', 'test'),
    os.path.join(prefD, 'scripts', 'test'),
    os.path.join(prefD, 'shaders', 'test'), ]
# # static global declarations/definitions
doFrameSort = 10
controlMgr = None
address = None
player = None
msg = None
windowOn = None
dirs = None
timeBetweenInputs = None
delayBeforeAck = None
timeBetweenHellos = None
timeBetweenInputPackets = None
# needed to maintain references
timeMgr = None
netMgrClient = None
netWorld = None
inpStateFactory = None
roboInpState = None
inpMgr = None
netObjRegistry = None

if __name__ == '__main__':
    main()
