'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import Int32, String, UInt32
from common import Colors
from panda3d.core import LVector4f


class ScoreBoardManager(object):
    '''
    ScoreBoardManager class.
    '''

    def __init__(self):
        '''
        Constructor
        '''

        self.mEntries = []
        self.mDefaultColors = []
        self.mDefaultColors.append(Colors.LightYellow)
        self.mDefaultColors.append(Colors.LightBlue)
        self.mDefaultColors.append(Colors.LightPink)
        self.mDefaultColors.append(Colors.LightGreen)

    @staticmethod
    def StaticInit():
        if not ScoreBoardManager.sInstance:
            ScoreBoardManager.sInstance = ScoreBoardManager()

    # singleton variable
    sInstance = None

    class Entry:
        '''
        ScoreBoardManager class.
        '''

        def __init__(self, inPlayerId=0, inPlayerName='', inColor=Colors.White):
            '''
            Constructor
            '''

            self.mPlayerId = inPlayerId
            self.mPlayerName = inPlayerName
            self.mFormattedNameScore = str()
            self.mColor = LVector4f(inColor)
            self.SetScore(0)

        def GetColor(self):
            return self.mColor

        def GetPlayerId(self):
            return self.mPlayerId

        def GetPlayerName(self):
            return self.mPlayerName

        def GetFormattedNameScore(self):
            return self.mFormattedNameScore

        def GetScore(self):
            return self.mScore

        def SetScore(self, inScore):
            self.mScore = inScore
            self.mFormattedNameScore = '{} {}'.format(
                self.mPlayerName, self.mScore)

        def Write(self, inOutputStream):
            didSucceed = True
            inOutputStream.write(self.mColor)
            inOutputStream.write(UInt32(self.mPlayerId))
            inOutputStream.write(String(self.mPlayerName))
            inOutputStream.write(Int32(self.mScore))
            return didSucceed

        def Read(self, inInputStream):
            didSucceed = True
            inInputStream.read(self.mColor)
            player = UInt32()
            inInputStream.read(player)
            self.mPlayerId = player.value
            playerName = String()
            inInputStream.read(playerName)
            self.mPlayerName = playerName.value
            score = Int32()
            inInputStream.read(score)
            if didSucceed:
                self.SetScore(score.value)
            return didSucceed

    def GetEntry(self, inPlayerId):
        for entry in self.mEntries:
            if entry.GetPlayerId() == inPlayerId:
                return entry
        return None

    def RemoveEntry(self, inPlayerId):
        for eIt in self.mEntries:
            if eIt.GetPlayerId() == inPlayerId:
                self.mEntries.remove(eIt)
                return True
        return False

    def AddEntry(self, inPlayerId, inPlayerName):
        # if this player id exists already, remove it first- it would be crazy
        # to have two of the same id
        self.RemoveEntry(inPlayerId)
        self.mEntries.append(ScoreBoardManager.Entry(inPlayerId, inPlayerName,
                                                     self.mDefaultColors[inPlayerId % len(self.mDefaultColors)]))

    def IncScore(self, inPlayerId, inAmount):
        entry = self.GetEntry(inPlayerId)
        if entry:
            entry.SetScore(entry.GetScore() + inAmount)

    def Write(self, inOutputStream):
        entryCount = Int32(len(self.mEntries))
        # we don't know our player names, so it's hard to check for remaining space in the packet...
        # not really a concern now though
        inOutputStream.write(entryCount)
        for entry in self.mEntries:
            entry.Write(inOutputStream)
        return True

    def Read(self, inInputStream):
        entryCount = Int32()
        inInputStream.read(entryCount)
        # just replace everything that's here, it don't matter...
        entryCountOld = len(self.mEntries)
        if entryCount.value > entryCountOld:
            self.mEntries += [ScoreBoardManager.Entry()
                              for i in range(entryCount.value - entryCountOld)]
        elif entryCount.value < entryCountOld:
            self.mEntries = self.mEntries[:entryCount.value]
        for entry in self.mEntries:
            entry.Read(inInputStream)
        return True

    def GetEntries(self):
        return self.mEntries
