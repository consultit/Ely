'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import GameNetworkManagerClient
from Yarn import Yarn
from common import GameObject
import common

ELY_DEBUG = common.ELY_DEBUG


class YarnClient(Yarn):
    '''
    YarnClient class.
    '''

    def __init__(self):
        '''
        Constructor
        '''

        # Yarn.__init__(self)
        super(YarnClient, self).__init__()
        # add r/w members
        # set r/w members' dirty states
        # callbacks
        if ELY_DEBUG:
            self.set_update_callback(YarnClient.update_clbk)
#         self.set_all_state_mask_callback(clbk)
#         self.set_handle_dying_callback(clbk)
#         self.set_pre_write_callback(clbk)
#         self.set_post_write_callback(clbk)
#         self.set_pre_read_callback(clbk)
        self.set_post_read_callback(YarnClient.post_read_clbk)

    @staticmethod
    def StaticCreate():
        go = YarnClient()
        go.get_owner_object().reparent_to(common.render)
        return go

    if ELY_DEBUG:

        @staticmethod
        def update_clbk(THIS):
            Yarn.update_clbk(THIS)
            GameObject.update_clbk(THIS)

    @staticmethod
    def post_read_clbk(THIS, stateBit):
        # r/w members . owner object synchronization
        # due to correction (dead reckon) we don't use directly
        # Yarn.post_read_clbk(THIS, stateBit)
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if stateBit.get_word() & Yarn.EYarnReplicationState.EYRS_Pose:
            # first apply corrections (dead reckon) using r/w members: (posX,
            # posY, posZ) and mVelocity
            location = downTHIS.GetPosVar()
            velocity = downTHIS.GetVelocity()
            # /dead reckon ahead by rtt, since this was spawned a while ago!
            downTHIS.set_location(location + velocity
                                  * GameNetworkManagerClient.get_global_ptr().get_round_trip_time())
            # lastly update rotation
            downTHIS.set_rotation(downTHIS.GetRotVar())
        if stateBit.get_word() & Yarn.EYarnReplicationState.EYRS_Color:
            downTHIS.set_color(downTHIS.GetColVar())

    def HandleCollisionWithCat(self, inCat):
        if self.GetPlayerId() != inCat.GetPlayerId():
            self.get_owner_object().hide()
        return False
