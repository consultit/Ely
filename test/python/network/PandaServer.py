'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import GameNetworkManagerServer, Timing
from ely.control import GameControlManager
from Panda import Panda
from common import Is2DVectorEqual, GameObject
from enum import IntEnum, unique
import common


@unique
class EControlType(IntEnum):
    Human = 0
    AI = 1


class PandaServer(Panda):
    '''
    PandaServer class.
    '''

    def __init__(self):
        '''
        Constructor
        '''

        # Panda.__init__(self)
        super(PandaServer, self).__init__()
        self.mControlType = EControlType.Human
        # add r/w members
        # set r/w members' dirty states
        # callbacks
        self.set_update_callback(PandaServer.update_clbk)
#         self.set_all_state_mask_callback(clbk)
        self.set_handle_dying_callback(PandaServer.handle_dying_clbk)
#         self.set_pre_write_callback(clbk)
#         self.set_post_write_callback(clbk)
#         self.set_pre_read_callback(clbk)
#         self.set_post_read_callback(clbk)

    @staticmethod
    def StaticCreate():
        go = PandaServer()
        driverNP = go.setupDriver()
        go.get_owner_object().reparent_to(driverNP)
        go.set_owner_object(driverNP)
        return GameNetworkManagerServer.get_global_ptr().register_and_return(go)

    @staticmethod
    def handle_dying_clbk(THIS):
        GameObject.handle_dying_clbk(THIS)
        GameNetworkManagerServer.get_global_ptr().unregister_network_object(THIS)

    @staticmethod
    def update_clbk(THIS):
        downTHIS = common.NetObjGameObjMap[THIS.this]
        oldLocation = downTHIS.get_location()
        oldVelocity = downTHIS.driver.get_linear_speed()
        oldRotation = downTHIS.get_rotation()
        oldAngularVelocity = downTHIS.driver.get_angular_speeds()[0]
        # are you controlled by a player?
        # if so, is there a move we haven't processed yet?
        if downTHIS.mControlType == EControlType.Human:
            client = GameNetworkManagerServer.get_global_ptr(
            ).get_client_proxy(downTHIS.GetPlayerId())
            if client:
                moveList = client.get_unprocessed_move_list()
                for unprocessedMove in moveList:
                    currentState = unprocessedMove.get_input_state()
                    deltaTime = unprocessedMove.get_delta_time()
                    downTHIS.ProcessInput(deltaTime, currentState)
                    downTHIS.SimulateMovement(deltaTime)
                    # LOG( 'Server Move Time: %3.4f deltaTime: %3.4f left rot at %3.4f', unprocessedMove.GetTimestamp(), deltaTime, get_rotation() )
        else:
            # do some AI stuff
            downTHIS.SimulateMovement(Timing.get_global_ptr().get_delta_time())
        if ((not Is2DVectorEqual(oldLocation, downTHIS.get_location()))
                or (not Is2DVectorEqual(oldVelocity, downTHIS.driver.get_linear_speed()))
                or (oldRotation != downTHIS.get_rotation())
                or (oldAngularVelocity != downTHIS.driver.get_angular_speeds()[0])):
            GameNetworkManagerServer.get_global_ptr().set_state_dirty(THIS.get_network_id(),
                                                                      Panda.EReplicationState.Pose)

    def SetCatControlType(self, inControlType):
        self.mControlType = inControlType

    def setParametersBeforeCreation(self):
        Panda.setDriverParametersBeforeCreation()
        ctrlMgr = GameControlManager.get_global_ptr()
        # set driver's parameters
        ctrlMgr.set_parameter_value(
            GameControlManager.DRIVER, 'mouse_head', 'disabled')
        ctrlMgr.set_parameter_value(
            GameControlManager.DRIVER, 'mouse_pitch', 'disabled')
