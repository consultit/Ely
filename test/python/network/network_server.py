'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import Timing, SocketUtil, GameNetworkManagerServer, \
    NetworkWorld, InputStateFactory, NetworkObjectRegistry, NetworkObject
from ely.control import GameControlManager
from RoboInputState import RoboInputState
from PandaServer import PandaServer
from MouseServer import MouseServer
from YarnServer import YarnServer
from RoboCatServer import RoboCatServer
from common import WORLD_MIN, WORLD_MAX, GetRandomVector
import common
from ScoreBoardManager import ScoreBoardManager
from panda3d.core import load_prc_file_data, WindowProperties, NodePath, \
    BitMask32, LPoint3f
from direct.showbase.ShowBase import ShowBase
import os
import argparse
import textwrap
import sys

LOG = print
common.ELY_DEBUG = False


# # #SOURCES
def main():
    global msg, portString, parseArguments, loadPandaFramework, \
        setupPandaFramework, loadAndSetupNetworkFramework, atExitCleanup, \
        loadAndSetupGameFramework, framework
    # #INTIALIZATION
    parseArguments()
    msg = 'ServerMain - listening port: \'' + portString + '\''
    # #SETUP
    loadPandaFramework()
    setupPandaFramework()
    loadAndSetupNetworkFramework()
    loadAndSetupGameFramework()
    # #RUN
    framework.run()
    # #ATEXIT
    atExitCleanup()


def parseArguments():
    '''parse arguments'''

    global portString, msg, windowOn, dirs, noWorld, disconnectTimeout, \
        respawnDelay, delayBeforeAck
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''
    This script will perform ServerMain.
    '''))
    # set up arguments
    parser.add_argument('-p', '--port', type=str, default='45000',
                        help='the server port')
    parser.add_argument('-w', '--window-on', action='store_true',
                        help='window on/off flag')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    parser.add_argument('-n', '--no-world', action='store_true',
                        help='world on/off flag')
    parser.add_argument('-t', '--disconnect-timeout', type=float, default=5.0,
                        help='the disconnect timeout')
    parser.add_argument('-r', '--respawn-delay', type=float, default=3.0,
                        help='the respawn delay')
    parser.add_argument('-k', '--delay-before-ack', type=float, default=0.5,
                        help='the delay before ack')
    # parse arguments
    args = parser.parse_args()

    # do actions
    portString = args.port
    windowOn = args.window_on
    dirs = args.data_dir
    noWorld = args.no_world
    disconnectTimeout = args.disconnect_timeout
    respawnDelay = args.respawn_delay
    delayBeforeAck = args.delay_before_ack


def loadPandaFramework():
    '''Load your application's configuration'''

    global dataDirs, dirs, windowOn, msg, framework, window, render, \
        setShouldKeepRunning
    for dataDir in dataDirs:
        load_prc_file_data('', 'model-path ' + dataDir)
    if dirs:
        for dataDir in dirs:
            load_prc_file_data('', 'model-path ' + dataDir)
    if windowOn:
        load_prc_file_data('', 'win-size 1024 768')
        load_prc_file_data('', 'show-frame-rate-meter #t')
        load_prc_file_data('', 'sync-video #t')
        # load_prc_file_data('', 'want-directtools #t')
        # load_prc_file_data('', 'want-tk #t')
    else:
        load_prc_file_data('', 'window-type    none')
    # Setup your application
    framework = ShowBase()
    setShouldKeepRunning(True)
    if windowOn:
        window = framework.win
        props = WindowProperties()
        props.setTitle(msg)
        window.requestProperties(props)
        print('Opened the window successfully!')
        render = framework.render
    else:
        render = NodePath('render')
        print(msg)
    common.render = render


def setupPandaFramework():
    '''setup panda framework'''

    global controlMgr, ctrlMgrUpdateSort, render, framework, doFrameSort, \
        doFrameServer
    # create a control manager
    controlMgr = GameControlManager(GameControlManager.Output(),
                                    ctrlMgrUpdateSort, render, BitMask32.all_on())
    # reparent the reference node to render
    controlMgr.get_reference_node_path().reparent_to(render)
# * fixme manual update of driver
    # start the default update task for all drivers
#     controlMgr.start_default_update()
# *
    # create the task for DoFrameClient
    # Adds mDoFrameTask to the active queue.
    framework.taskMgr.add(doFrameServer, 'DoFrameServer',
                          doFrameSort, appendTask=True)


def loadAndSetupNetworkFramework():
    '''load and setup network framework'''

    global timeMgr, netMgrServer, netWorld, inpStateFactory, roboInpState, \
        netObjRegistry
    timeMgr = Timing()
    SocketUtil.static_init()
    port = int(portString)
    netMgrServer = GameNetworkManagerServer()
    if not netMgrServer.init(port):
        sys.exit(1)
    netWorld = NetworkWorld()
    # manage custom InputState
    inpStateFactory = InputStateFactory()
    roboInpState = RoboInputState(
        InputStateFactory.get_global_ptr().get_input_state())
    netObjRegistry = NetworkObjectRegistry()


def loadAndSetupGameFramework():
    '''load and setup game framework'''

    global portString, disconnectTimeout, delayBeforeAck, respawnDelay, \
        setupWorld, AddScoreBoardStateToPacket, handleNewClient, handleLostClient, \
        spawnCatForPlayer
    ScoreBoardManager.StaticInit()
    #
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('PAND'), PandaServer.StaticCreate)
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('RCAT'), RoboCatServer.StaticCreate)
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('MOUS'), MouseServer.StaticCreate)
    NetworkObjectRegistry.get_global_ptr().register_creation_function(
        NetworkObject.get_class_id_int('YARN'), YarnServer.StaticCreate)
    #
    setupWorld()
    #
    GameNetworkManagerServer.get_global_ptr(
    ).set_client_disconnect_timeout(disconnectTimeout)
    GameNetworkManagerServer.get_global_ptr(
    ).set_send_state_packet_to_client_callback(AddScoreBoardStateToPacket)
    GameNetworkManagerServer.get_global_ptr(
    ).set_handle_packet_from_new_client_callback(handleNewClient)
    GameNetworkManagerServer.get_global_ptr(
    ).set_handle_client_disconnected_callback(handleLostClient)
    GameNetworkManagerServer.get_global_ptr().set_delay_before_ack_timeout(delayBeforeAck)
    GameNetworkManagerServer.get_global_ptr(
    ).set_respawn_client_objects_delay(respawnDelay)
    GameNetworkManagerServer.get_global_ptr(
    ).set_respawn_client_objects_callback(spawnCatForPlayer)


def atExitCleanup():
    '''at exit cleanup'''

    global framework, timeMgr, netMgrServer, netWorld, inpStateFactory, \
        roboInpState, netObjRegistry
    framework.taskMgr.remove('DoFrameClient')
    del netObjRegistry
    del roboInpState
    del inpStateFactory
    del netWorld
    del netMgrServer
    SocketUtil.clean_up()
    del timeMgr


def doFrameServer(task):
    '''do frame server'''

    global removeGameObjects
    Timing.get_global_ptr().update()
    GameNetworkManagerServer.get_global_ptr().process_incoming_packets()
    GameNetworkManagerServer.get_global_ptr().check_for_disconnects()
    GameNetworkManagerServer.get_global_ptr().respawn_objects_for_clients()
    NetworkWorld.get_global_ptr().update()
    # the pending moves of all clients have been processed: clear all
    GameNetworkManagerServer.get_global_ptr().clear_all_client_moves()
    GameNetworkManagerServer.get_global_ptr().send_outgoing_packets()
    removeGameObjects()
    return task.cont


def removeGameObjects():
    while common.GameObjToRemoveKey:
        goKey = common.GameObjToRemoveKey.pop()
        go = common.NetObjGameObjMap[goKey]
        del common.NetObjGameObjMap[goKey]
        print('removed ' + str(go))


def CreateRandomMice(inMouseCount):
    '''create random mice'''

    mouseMin = LPoint3f(WORLD_MIN * 0.9)
    mouseMax = LPoint3f(WORLD_MAX * 0.9)
    # make a mouse somewhere- where will these come from?
    for _ in range(inMouseCount):
        netObj = NetworkObjectRegistry.get_global_ptr().create_network_object(
            NetworkObject.get_class_id_int('MOUS'))
        go = common.NetObjGameObjMap[netObj.this]
        mouseLocation = LPoint3f(GetRandomVector(mouseMin, mouseMax))
        go.set_location(mouseLocation)


def setupWorld():
    '''setup world'''

    global noWorld
    if not noWorld:
        # spawn some random mice
        CreateRandomMice(10)
        # spawn more random mice!
        CreateRandomMice(10)


def getCatForPlayer(inPlayerId):
    '''get cat for player'''

    # run through the objects till we find the cat...
    # it would be nice if we kept a pointer to the cat on the clientproxy
    # but then we'd have to clean it up when the cat died, etc.
    # this will work for now until it's a perf issue
    networkObjects = NetworkWorld.get_global_ptr().get_network_objects()
    for i in range(networkObjects.get_num_values()):
        netObj = networkObjects[i]
        go = common.NetObjGameObjMap[netObj.this]
        if ((go.get_class_id() == NetworkObject.get_class_id_int('RCAT'))
                and (go.GetPlayerId() == inPlayerId)):
            return go
    return None


def getPandaForPlayer(inPlayerId):
    '''get panda for player'''

    # run through the objects till we find the panda...
    # it would be nice if we kept a pointer to the panda on the clientproxy
    # but then we'd have to clean it up when the panda died, etc.
    # this will work for now until it's a perf issue
    networkObjects = NetworkWorld.get_global_ptr().get_network_objects()
    for i in range(networkObjects.get_num_values()):
        netObj = networkObjects[i]
        go = common.NetObjGameObjMap[netObj.this]
        if ((go.get_class_id() == NetworkObject.get_class_id_int('PAND'))
                and (go.GetPlayerId() == inPlayerId)):
            return go
    return None


def handleNewClient(inClientProxy):
    '''handle new client'''

    global spawnCatForPlayer
    playerId = inClientProxy.get_player_id()
    ScoreBoardManager.sInstance.AddEntry(playerId, inClientProxy.get_name())
    spawnCatForPlayer(playerId)
    # create a driven panda
    netObj = NetworkObjectRegistry.get_global_ptr().create_network_object(
        NetworkObject.get_class_id_int('PAND'))
    panda = common.NetObjGameObjMap[netObj.this]
    panda.set_color(ScoreBoardManager.sInstance.GetEntry(playerId).GetColor())
    panda.SetPlayerId(playerId)
    # gotta pick a better spawn location than this...
    panda.set_location(LPoint3f(10.0 - float(playerId), 0.0, 0.0))


def handleLostClient(inClientProxy):
    '''handle lost client'''

    # kill client's cat
    # remove client from scoreboard
    playerId = inClientProxy.get_player_id()
    ScoreBoardManager.sInstance.RemoveEntry(playerId)
    cat = getCatForPlayer(playerId)
    if cat:
        cat.set_does_want_to_die(True)
    # driven panda
    panda = getPandaForPlayer(playerId)
    if panda:
        panda.set_does_want_to_die(True)
    # was that the last client? if so, bye!
    if GameNetworkManagerServer.get_global_ptr().get_clients_num() == 0:
        setShouldKeepRunning(False)


def setShouldKeepRunning(value):
    '''set should keep running'''

    global framework
    if not value:
        framework.taskMgr.stop()


def AddScoreBoardStateToPacket(inOutputStream):
    '''add score board state to packet'''

    ScoreBoardManager.sInstance.Write(inOutputStream)


def spawnCatForPlayer(inPlayerId):
    '''spawn cat for player'''

    netObj = NetworkObjectRegistry.get_global_ptr().create_network_object(
        NetworkObject.get_class_id_int('RCAT'))
    cat = common.NetObjGameObjMap[netObj.this]
    cat.set_color(ScoreBoardManager.sInstance.GetEntry(inPlayerId).GetColor())
    cat.SetPlayerId(inPlayerId)
    # gotta pick a better spawn location than this...
    cat.set_location(LPoint3f(1.0 - float(inPlayerId), 0.0, 0.0))


# # global declarations/definitions
framework = None
window = None
render = None
scriptPath = os.path.dirname(os.path.abspath(__file__))
prefD = os.path.join(scriptPath, '..', 'assets')
dataDirs = [
    os.path.join(prefD, 'misc', 'test'),
    os.path.join(prefD, 'models', 'test'),
    os.path.join(prefD, 'textures', 'test'),
    os.path.join(prefD, 'sounds', 'test'),
    os.path.join(prefD, 'scripts', 'test'),
    os.path.join(prefD, 'shaders', 'test'), ]
# # static global declarations/definitions
doFrameSort = 10
controlMgr = None
ctrlMgrUpdateSort = 15
portString = None
msg = None
windowOn = None
dirs = None
noWorld = None
disconnectTimeout = None
respawnDelay = None
delayBeforeAck = None
# needed to maintain references
timeMgr = None
netMgrServer = None
netWorld = None
inpStateFactory = None
roboInpState = None
netObjRegistry = None

if __name__ == '__main__':
    main()
