'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import Float, Bool, DataVariables


class RoboInputState(object):
    '''
    RoboInputState class.
    '''

    def __init__(self, inputState):
        '''
        Constructor
        '''

        if RoboInputState.sInstance:
            raise Exception('Cannot create another RoboInputState instance')
        super(object, self).__init__()
        RoboInputState.sInstance = self
        inputState.get_members().add_variable('mDesiredRightAmount',
                                              DataVariables.EPT_Float)
        inputState.get_members().add_variable('mDesiredLeftAmount',
                                              DataVariables.EPT_Float)
        inputState.get_members().add_variable('mDesiredForwardAmount',
                                              DataVariables.EPT_Float)
        inputState.get_members().add_variable('mDesiredBackAmount',
                                              DataVariables.EPT_Float)
        inputState.get_members().add_variable('mIsShooting',
                                              DataVariables.EPT_Bool)
        inputState.get_members().add_variable('move_forward',
                                              DataVariables.EPT_Bool)
        inputState.get_members().add_variable('move_backward',
                                              DataVariables.EPT_Bool)
        inputState.get_members().add_variable('rotate_head_left',
                                              DataVariables.EPT_Bool)
        inputState.get_members().add_variable('rotate_head_right',
                                              DataVariables.EPT_Bool)

    # singleton variable
    sInstance = None

    @staticmethod
    def get_global_ptr():
        return RoboInputState.sInstance

    def GetDesiredHorizontalDelta(self, inputState):
        mDesiredRightAmount = Float(0.0)
        mDesiredLeftAmount = Float(0.0)
        inputState.get_members().get_value_float(
            'mDesiredRightAmount', mDesiredRightAmount)
        inputState.get_members().get_value_float(
            'mDesiredLeftAmount', mDesiredLeftAmount)
        return mDesiredRightAmount.value - mDesiredLeftAmount.value

    def GetDesiredVerticalDelta(self, inputState):
        mDesiredForwardAmount = Float()
        mDesiredBackAmount = Float()
        inputState.get_members().get_value_float(
            'mDesiredForwardAmount', mDesiredForwardAmount)
        inputState.get_members().get_value_float(
            'mDesiredBackAmount', mDesiredBackAmount)
        return mDesiredForwardAmount.value - mDesiredBackAmount.value

    def IsShooting(self, inputState):
        mIsShooting = Bool(False)
        inputState.get_members().get_value_bool('mIsShooting', mIsShooting)
        return mIsShooting.value

    def SetDesiredRightAmount(self, inputState, value):
        inputState.get_members().set_value_float('mDesiredRightAmount', value)

    def SetDesiredLeftAmount(self, inputState, value):
        inputState.get_members().set_value_float('mDesiredLeftAmount', value)

    def SetDesiredForwardAmount(self, inputState, value):
        inputState.get_members().set_value_float('mDesiredForwardAmount', value)

    def SetDesiredBackAmount(self, inputState, value):
        inputState.get_members().set_value_float('mDesiredBackAmount', value)

    def SetIsShooting(self, inputState, value):
        inputState.get_members().set_value_bool('mIsShooting', value)

    # P3Driver
    def set_move_forward(self, inputState, on):
        inputState.get_members().set_value_bool('move_forward', on)

    def get_move_forward(self, inputState):
        result = Bool(False)
        inputState.get_members().get_value_bool('move_forward', result)
        return result.value

    def set_move_backward(self, inputState, on):
        inputState.get_members().set_value_bool('move_backward', on)

    def get_move_backward(self, inputState):
        result = Bool(False)
        inputState.get_members().get_value_bool('move_backward', result)
        return result.value

    def set_rotate_head_left(self, inputState, on):
        inputState.get_members().set_value_bool('rotate_head_left', on)

    def get_rotate_head_left(self, inputState):
        result = Bool(False)
        inputState.get_members().get_value_bool('rotate_head_left', result)
        return result.value

    def set_rotate_head_right(self, inputState, on):
        inputState.get_members().set_value_bool('rotate_head_right', on)

    def get_rotate_head_right(self, inputState):
        result = Bool(False)
        inputState.get_members().get_value_bool('rotate_head_right', result)
        return result.value
