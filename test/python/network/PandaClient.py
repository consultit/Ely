'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import GameNetworkManagerClient, InputManager, Timing
from ely.control import GameControlManager
from Panda import Panda
from common import GameObject, Is2DVectorEqual, Lerp
import common
from panda3d.core import LVecBase3f, LPoint3f, LVector3f

ELY_DEBUG = common.ELY_DEBUG
LOG = print


class PandaClient(Panda):
    '''
    PandaClient class.
    '''

    def __init__(self):
        '''
        Constructor
        '''

        # Panda.__init__(self)
        super(PandaClient, self).__init__()
        self.mTimeLocationBecameOutOfSync = float(0.0)
        self.mTimeVelocityBecameOutOfSync = float(0.0)
        self.mTimeRotationBecameOutOfSync = float(0.0)
        self.mTimeAngularVelocityBecameOutOfSync = float(0.0)
        self.mOldRotation = LVecBase3f()
        self.mOldLocation = LPoint3f()
        self.mOldVelocity = LVector3f()
        self.mOldAngularVelocity = float(0.0)
        # add r/w members
        # set r/w members' dirty states
        # callbacks
        self.set_update_callback(PandaClient.update_clbk)
#         self.set_all_state_mask_callback(clbk)
#         self.set_handle_dying_callback(clbk)
#         self.set_pre_write_callback(clbk)
#         self.set_post_write_callback(clbk)
        self.set_pre_read_callback(PandaClient.pre_read_clbk)
        self.set_post_read_callback(PandaClient.post_read_clbk)

    @staticmethod
    def StaticCreate():
        go = PandaClient()
        driverNP = go.setupDriver()
        go.get_owner_object().reparent_to(driverNP)
        go.set_owner_object(driverNP)
        return go

    @staticmethod
    def update_clbk(THIS):
        downTHIS = common.NetObjGameObjMap[THIS.this]
        # is this the panda owned by us?
        if downTHIS.GetPlayerId() == GameNetworkManagerClient.get_global_ptr().get_player_id():
            pendingMove = InputManager.get_global_ptr().get_pending_move()
            # in theory, only do this if we want to sample input this frame /
            # if there's a new move ( since we have to keep in sync with server
            # )
            if pendingMove:  # is it time to sample a new move...
                deltaTime = pendingMove.get_delta_time()
                # apply that input
                downTHIS.ProcessInput(deltaTime, pendingMove.get_input_state())
                # and simulate!
                downTHIS.SimulateMovement(deltaTime)
                # LOG( 'Client Move Time: %3.4f deltaTime: %3.4f left rot at %3.4f', latestMove.GetTimestamp(), deltaTime, get_rotation() )
        else:
            downTHIS.SimulateMovement(Timing.get_global_ptr().get_delta_time())
# * fixme or xxx ?
#             if Is2DVectorEqual(downTHIS.driver.get_linear_speed(), LVector3f.zero()):
#                 #we're in sync if our velocity is 0
#                 downTHIS.mTimeLocationBecameOutOfSync = 0.0
#             if self.driver.get_angular_speeds()[0] == 0.0:
#                 #we're in sync if our angular velocity is 0
#                 downTHIS.mTimeRotationBecameOutOfSync = 0.0
# *
        if ELY_DEBUG:
            GameObject.update_clbk(THIS)

    @staticmethod
    def pre_read_clbk(THIS):
        downTHIS = common.NetObjGameObjMap[THIS.this]
        downTHIS.mOldRotation.assign(downTHIS.get_rotation())
        downTHIS.mOldLocation.assign(downTHIS.get_location())
        downTHIS.mOldVelocity.assign(downTHIS.driver.get_linear_speed())
        downTHIS.mOldAngularVelocity = downTHIS.driver.get_angular_speeds()[0]

    @staticmethod
    def post_read_clbk(THIS, readState):
        # r/w members . owner object synchronization
        Panda.post_read_clbk(THIS, readState)
        downTHIS = common.NetObjGameObjMap[THIS.this]
        if downTHIS.GetPlayerId() == GameNetworkManagerClient.get_global_ptr().get_player_id():
            downTHIS.DoClientSidePredictionAfterReplicationForLocalPanda(
                readState.get_word())
            # if this is a create packet, don't interpolate
            if (readState.get_word() & Panda.EReplicationState.PlayerId) == 0:
                downTHIS.InterpolateClientSidePrediction(downTHIS.mOldRotation,
                                                         downTHIS.mOldLocation, downTHIS.mOldVelocity,
                                                         downTHIS.mOldAngularVelocity, False)
        else:
            downTHIS.DoClientSidePredictionAfterReplicationForRemotePanda(
                readState.get_word())
            # will this smooth us out too? it'll interpolate us just 10% of the
            # way there...
            if (readState.get_word() & Panda.EReplicationState.PlayerId) == 0:
                downTHIS.InterpolateClientSidePrediction(downTHIS.mOldRotation,
                                                         downTHIS.mOldLocation, downTHIS.mOldVelocity,
                                                         downTHIS.mOldAngularVelocity, True)

    def DoClientSidePredictionAfterReplicationForLocalPanda(self, inReadState):
        if (inReadState & Panda.EReplicationState.Pose) != 0:
            # simulate pose only if we received new pose- might have just gotten thrustDir
            # in which case we don't need to replay moves because we haven't warped backwards
            # all processed moves have been removed, so all that are left are unprocessed moves
            # so we must apply them...
            moveList = InputManager.get_global_ptr().get_move_list()
            for move in moveList:
                deltaTime = move.get_delta_time()
                self.ProcessInput(deltaTime, move.get_input_state())
                self.SimulateMovement(deltaTime)

    def InterpolateClientSidePrediction(self, inOldRotation, inOldLocation, inOldVelocity,
                                        inOldAngularVelocity, inIsForRemotePanda):
        '''so what do we want to do here? need to do some kind of interpolation...'''

        if (inOldRotation != self.get_rotation()) and (not inIsForRemotePanda):
            LOG('ERROR! Move replay ended with incorrect rotation!', 0)
        roundTripTime = GameNetworkManagerClient.get_global_ptr().get_round_trip_time()
        # location
        if not Is2DVectorEqual(inOldLocation, self.get_location()):
            # LOG( 'ERROR! Move replay ended with incorrect location!', 0 )
            # have we been out of sync, or did we just become out of sync?
            time = Timing.get_global_ptr().get_frame_start_time()
            if self.mTimeLocationBecameOutOfSync == 0.0:
                self.mTimeLocationBecameOutOfSync = time
            durationOutOfSync = time - self.mTimeLocationBecameOutOfSync
            if durationOutOfSync < roundTripTime:
                self.set_location(
                    Lerp(inOldLocation, self.get_location(),
                         (durationOutOfSync / roundTripTime) if inIsForRemotePanda else 0.1))
        else:
            # we're in sync
            self.mTimeLocationBecameOutOfSync = 0.0
        # velocity
        if not Is2DVectorEqual(inOldVelocity, self.driver.get_linear_speed()):
            # LOG( 'ERROR! Move replay ended with incorrect velocity!', 0 )
            # have we been out of sync, or did we just become out of sync?
            time = Timing.get_global_ptr().get_frame_start_time()
            if self.mTimeVelocityBecameOutOfSync == 0.0:
                self.mTimeVelocityBecameOutOfSync = time
            # now interpolate to the correct value...
            durationOutOfSync = time - self.mTimeVelocityBecameOutOfSync
            if durationOutOfSync < roundTripTime:
                self.driver.set_linear_speed(
                    Lerp(inOldVelocity, self.driver.get_linear_speed(),
                         (durationOutOfSync / roundTripTime) if inIsForRemotePanda else 0.1))
            # otherwise, fine...
        else:
            # we're in sync
            self.mTimeVelocityBecameOutOfSync = 0.0
        # rotation
        if not Is2DVectorEqual(inOldRotation, self.get_rotation()):
            # LOG( 'ERROR! Move replay ended with incorrect rotation!', 0 )
            # have we been out of sync, or did we just become out of sync?
            time = Timing.get_global_ptr().get_frame_start_time()
            if self.mTimeRotationBecameOutOfSync == 0.0:
                self.mTimeRotationBecameOutOfSync = time
            durationOutOfSync = time - self.mTimeRotationBecameOutOfSync
            if durationOutOfSync < roundTripTime:
                self.set_rotation(
                    Lerp(inOldRotation, self.get_rotation(),
                         (durationOutOfSync / roundTripTime) if inIsForRemotePanda else 0.1))
        else:
            # we're in sync
            self.mTimeRotationBecameOutOfSync = 0.0
        # angular velocity
        if inOldAngularVelocity != self.driver.get_angular_speeds()[0]:
            # LOG( 'ERROR! Move replay ended with incorrect velocity!', 0 )
            # have we been out of sync, or did we just become out of sync?
            time = Timing.get_global_ptr().get_frame_start_time()
            if self.mTimeAngularVelocityBecameOutOfSync == 0.0:
                self.mTimeAngularVelocityBecameOutOfSync = time
            # now interpolate to the correct value...
            durationOutOfSync = time - self.mTimeAngularVelocityBecameOutOfSync
            if durationOutOfSync < roundTripTime:
                outAngularVelocity = Lerp(inOldAngularVelocity,
                                          self.driver.get_angular_speeds()[0],
                                          (durationOutOfSync / roundTripTime) if inIsForRemotePanda else 0.1)
                self.driver.set_angular_speeds([outAngularVelocity, 0.0])
            # otherwise, fine...
        else:
            # we're in sync
            self.mTimeAngularVelocityBecameOutOfSync = 0.0

    def DoClientSidePredictionAfterReplicationForRemotePanda(self, inReadState):
        if (inReadState & Panda.EReplicationState.Pose) != 0:
            # simulate movement for an additional RTT
            rtt = GameNetworkManagerClient.get_global_ptr().get_round_trip_time()
            # LOG( 'Other panda came in, simulating for an extra %f', rtt )
            # let's break into framerate sized chunks though so that we don't
            # run through walls and do crazy things...
            deltaTime = 1.0 / 30.0
            while True:
                if rtt < deltaTime:
                    self.SimulateMovement(rtt)
                    break
                else:
                    self.SimulateMovement(deltaTime)
                    rtt -= deltaTime

    def setDriverParametersBeforeCreation(self):
        '''set parameters as strings before drivers/chasers creation'''

        Panda.setDriverParametersBeforeCreation(self)
# *fixme no mouse rotation
#         ctrlMgr = GameControlManager.get_global_ptr()
#         # set driver's parameters
#         ctrlMgr.set_parameter_value(GameControlManager.DRIVER, 'mouse_head', 'enabled');
#         ctrlMgr.set_parameter_value(GameControlManager.DRIVER, 'mouse_pitch', 'enabled');
# *
