'''
Created on Aug 6, 2018

@author: consultit
'''

from ely.network import GameNetworkManagerServer, Timing, NetworkObject, NetworkObjectRegistry
from RoboCat import RoboCat
from ScoreBoardManager import ScoreBoardManager
from common import Is2DVectorEqual, GameObject
import common
from panda3d.core import LPoint3f, LVector3f, LVecBase3f
from enum import IntEnum, unique


@unique
class ECatControlType(IntEnum):
    ESCT_Human = 0
    ESCT_AI = 1


class RoboCatServer(RoboCat):
    '''
    RoboCatServer class.
    '''

    def __init__(self):
        '''
        Constructor
        '''

        # RoboCat.__init__(self)
        super(RoboCatServer, self).__init__()
        self.mCatControlType = ECatControlType.ESCT_Human
        self.mTimeOfNextShot = float(0.0)
        self.mTimeBetweenShots = float(0.2)
        self.oldLocation = LPoint3f(0.0, 0.0, 0.0)
        self.oldVelocity = LVector3f(0.0, 0.0, 0.0)
        self.oldRotation = LVecBase3f(0.0, 0.0, 0.0)
# *fixme
#         self.get_owner_object().remove_node()
#         # add a collision node to render
#         collSolid = CollisionSphere(0,0,0, self.get_collision_radius())
#         collNode = CollisionNode('collNode')
#         collNode.add_solid(collSolid)
#         collNodeNP = render.attach_new_node(collNode)
#         self.set_owner_object(collNodeNP)
# *
        # add r/w members
        # set r/w members' dirty states
        # callbacks
        self.set_update_callback(RoboCatServer.update_clbk)
#         self.set_all_state_mask_callback(clbk)
        self.set_handle_dying_callback(RoboCatServer.handle_dying_clbk)
#         self.set_pre_write_callback(clbk)
#         self.set_post_write_callback(clbk)
#         self.set_pre_read_callback(clbk)
#         self.set_post_read_callback(clbk)

    @staticmethod
    def StaticCreate():
        go = RoboCatServer()
        go.get_owner_object().reparent_to(common.render)
        return GameNetworkManagerServer.get_global_ptr().register_and_return(go)

    @staticmethod
    def handle_dying_clbk(THIS):
        GameObject.handle_dying_clbk(THIS)
        GameNetworkManagerServer.get_global_ptr().unregister_network_object(THIS)

    @staticmethod
    def update_clbk(THIS):
        downTHIS = common.NetObjGameObjMap[THIS.this]
        downTHIS.oldLocation.assign(downTHIS.get_location())
        downTHIS.oldVelocity.assign(downTHIS.GetVelocity())
        downTHIS.oldRotation.assign(downTHIS.get_rotation())
        # are you controlled by a player?
        # if so, is there a move we haven't processed yet?
        if downTHIS.mCatControlType == ECatControlType.ESCT_Human:
            client = GameNetworkManagerServer.get_global_ptr(
            ).get_client_proxy(downTHIS.GetPlayerId())
            if client:
                moveList = client.get_unprocessed_move_list()
                for unprocessedMove in moveList:
                    currentState = unprocessedMove.get_input_state()
                    deltaTime = unprocessedMove.get_delta_time()
                    downTHIS.ProcessInput(deltaTime, currentState)
                    downTHIS.SimulateMovement(deltaTime)
                    # LOG( 'Server Move Time: %3.4f deltaTime: %3.4f left rot at %3.4f', unprocessedMove.GetTimestamp(), deltaTime, get_rotation() )
        else:
            # do some AI stuff
            downTHIS.SimulateMovement(Timing.get_global_ptr().get_delta_time())
        downTHIS.HandleShooting()
        if ((not Is2DVectorEqual(downTHIS.oldLocation, downTHIS.get_location()))
                or (not Is2DVectorEqual(downTHIS.oldVelocity, downTHIS.GetVelocity()))
                or (downTHIS.oldRotation != downTHIS.get_rotation())):
            GameNetworkManagerServer.get_global_ptr().set_state_dirty(THIS.get_network_id(),
                                                                      RoboCat.ECatReplicationState.ECRS_Pose)

    def SetCatControlType(self, inCatControlType):
        self.mCatControlType = inCatControlType

    def TakeDamage(self, inDamagingPlayerId):
        self.mHealth.assign(self.mHealth.value - 1)
        if self.mHealth.value <= 0:
            # score one for damaging player...
            ScoreBoardManager.sInstance.IncScore(inDamagingPlayerId, 1)
            # and you want to die
            self.set_does_want_to_die(True)
            # tell the client proxy to make you a new cat
            clientProxy = GameNetworkManagerServer.get_global_ptr(
            ).get_client_proxy(self.GetPlayerId())
            if clientProxy:
                clientProxy.compute_time_to_respawn()
        # tell the world our health dropped
        GameNetworkManagerServer.get_global_ptr().set_state_dirty(
            self.get_network_id(), RoboCat.ECatReplicationState.ECRS_Health)

    def HandleShooting(self):
        time = Timing.get_global_ptr().get_frame_start_time()
        if self.mIsShooting and (Timing.get_global_ptr().get_frame_start_time() > self.mTimeOfNextShot):
            # not exact, but okay
            self.mTimeOfNextShot = time + self.mTimeBetweenShots
            # fire!
            yarn = NetworkObjectRegistry.get_global_ptr().create_network_object(
                NetworkObject.get_class_id_int('YARN'))
            common.NetObjGameObjMap[yarn.this].InitFromShooter(self)
