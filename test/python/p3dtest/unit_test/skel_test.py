'''
Created on Aug 25, 2018

@author: consultit
'''

import unittest
import random
import panda3d.core
import ely.libtools
from ely.p3dtest import BasicClass, TypedClass


def setUpModule():
    pass


def tearDownModule():
    pass


class BasicClassDerived(BasicClass):
    def set_value(self, n):
        BasicClass.set_value(self, n)
        print('BasicClassDerived set_value()')

    def set_value_alt(self, n):
        BasicClass.set_value(self, n)
        print('BasicClassDerived set_value_alt()')


class TypedClassDerived(TypedClass):
    def set_value(self, n):
        TypedClass.set_value(self, n)
        print('TypedClassDerived set_value()')

    def set_value_alt(self, n):
        TypedClass.set_value(self, n)
        print('TypedClassDerived set_value_alt()')


class PolymorphismTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        print("BasicClassDerived")
        b = BasicClassDerived()
        v = random.randint(0, 100)
        b.set_value(v)
        self.assertEqual(b.get_value(), v)
        v = random.randint(0, 100)
        b.set_value_alt(v)
        self.assertEqual(b.get_value_alt(), v)
        print("TypedClassDerived")
        t = TypedClassDerived()
        v = random.randint(0, 100)
        t.set_value(v)
        self.assertEqual(t.get_value(), v)
        v = random.randint(0, 100)
        t.set_value_alt(v)
        self.assertEqual(t.get_value_alt(), v)
        # (polymorphic) argument substitution
        print('b.set_typed_class(t)')
        #     void set_typed_class(TypedClass& cl);//1 attempt
        #     void set_typed_class(TypedClass* cl);//2 attempt
        #     void set_typed_class(const TypedClass& cl);//3 attempt
        #     void set_typed_class(PT(TypedClass) cl);//4 attempt
        b.set_typed_class(t)


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTest(PolymorphismTEST('test'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())
