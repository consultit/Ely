'''
Created on Aug 25, 2018

@author: consultit
'''

import unittest
import random
import panda3d.core
import ely.libtools
from ely.p3dtest import NetworkObject, NetworkWorld, get_class_id_str, \
    get_class_id_int, InputMemoryBitStream
from numpy import delete


def setUpModule():
    pass


def tearDownModule():
    pass


class MyNetObj(NetworkObject):

    classIdStr = 'MOBJ'

    def __init__(self):
        #NetworkObject.__init__(self, MyNetObj.getClassIdInt(MyNetObj.classIdStr))
        NetworkObject.__init__(self, get_class_id_int(MyNetObj.classIdStr))

    def update(self):
        print('MyNetObj.update()')

    @staticmethod
    def getClassIdStr(classId):
        classIdHexStr = '{0:0{1}x}'.format(classId, 8)
        classIdStr = ''
        for l in range(0, 8, 2):
            classIdStr += chr(int(classIdHexStr[l:l + 2], 16))
        return classIdStr

    @staticmethod
    def getClassIdInt(classIdStr):
        classIdStrInt = ''
        for l in range(4):
            classIdStrInt += hex(ord(classIdStr[l]))[-2:]
        return int(classIdStrInt, 16)


class NetTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def createObj(self):
        obj = MyNetObj()
        return obj

    def update_clbk(self, world):
        print('update_clbk started')
        for obj in world.get_network_objects():
            print(get_class_id_str(obj.get_class_id()))
        print('update_clbk completed')

    def test0(self):
        world = NetworkWorld()
        worldPtr = NetworkWorld.get_global_ptr()
        for i in range(10):
            obj = self.createObj()
            worldPtr.add_network_object(obj)
            obj.set_network_id(obj.get_index_in_world())
        for obj in worldPtr.get_network_objects():
            self.assertEqual(obj.get_index_in_world(), obj.get_network_id())
            self.assertEqual(MyNetObj.getClassIdStr(
                obj.get_class_id()), MyNetObj.classIdStr)
            self.assertEqual(get_class_id_str(
                obj.get_class_id()), MyNetObj.classIdStr)
        print('')
        print(NetworkObject.kHelloCC, ' = ', get_class_id_int('HELO'))
        print(NetworkObject.kWelcomeCC, ' = ', get_class_id_int('WLCM'))
        print(NetworkObject.kStateCC, ' = ', get_class_id_int('STAT'))
        print(NetworkObject.kInputCC, ' = ', get_class_id_int('INPT'))
        print(NetworkObject.kReplCC, ' = ', get_class_id_int('RPLM'))
        print('first attempt: update callback test')
        worldPtr.set_update_callback(self.update_clbk)
        worldPtr.update()
        print('second attempt: update callback test')
        for i in range(5):
            obj = self.createObj()
            worldPtr.add_network_object(obj)
            obj.set_network_id(obj.get_index_in_world())
        worldPtr.set_update_callback(self.update_clbk)
        worldPtr.update()

    def handle_welcome_packet_clbk(self, inInputStream):
        print(inInputStream.get_remaining_bit_count())
        inInputStream.reset_to_capacity(512)
        print(inInputStream.remaining_bit_count)

    def test1(self):
        world = NetworkWorld()
        worldPtr = NetworkWorld.get_global_ptr()
        inInputStream = InputMemoryBitStream()
        print(inInputStream.remaining_bit_count)
        print('first attempt: welcome packet callback')
        worldPtr.set_handle_welcome_packet_callback(
            self.handle_welcome_packet_clbk)
        worldPtr.call_inputstream_callbacks(inInputStream)
        print('second attempt: welcome packet callback')
        worldPtr.set_handle_welcome_packet_callback(
            self.handle_welcome_packet_clbk)
        worldPtr.call_inputstream_callbacks(inInputStream)


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTest(NetTEST('test0'))
    suite.addTest(NetTEST('test1'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())
