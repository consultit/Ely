from panda3d.core import LVector3f, LVecBase3f, CollideMask
from ely.libtools import Utilities 
from ely.physics import GamePhysicsManager, BT3Constraint

class Data:
    pass
data = Data()

## model data
data.modelName = 'ralph'
data.models='ralph.egg'
data.anims={'run':'ralph-run.egg', 'walk':'ralph-walk.egg', 'jump':'ralph-jump.egg', 
        'offbalance':'ralph-offbalance.egg'}
data.animData = {'startAnimPose':('walk', 0)}

## character controller data
data.ctrlData={'initState':'I', 'linAccel':60.0, 'linSpeed':53.0, 'linSpeedQ':117.5,
          'angAccel':0.8, 'angSpeed':1.6, 'angSpeedQ':3.2, 'jumpSpeed':10.0, 'jumpspeedQ':20.0}

# return the real actor to use with the character controller
# with corrections of its location applied if needed
def getRealActor(actor, referencNP):
    '''Returns (realActor, actorDims, actorDeltaCenter, actorRadius)'''
    actorDims = LVecBase3f()
    actorDeltaCenter = LVector3f()
    actorRadius = Utilities(referencNP, CollideMask.all_on()).get_bounding_dimensions(
        actor, actorDims, actorDeltaCenter)
    realDeltaPos = None
    if realDeltaPos:
        realActor = actor._createPlaceholderNP('realActor', actorDims)
        actor.reparent_to(realActor)
        actor.set_pos(realDeltaPos)
        realActor.hide()
        actor.show_through()
    else:
        realActor = actor
    return (realActor, actorDims, actorDeltaCenter, actorRadius)

data.getRealActor = getRealActor

## animations' transitions
def getAnimToFrameWR(animFromFrame):
    frameTo = (animFromFrame % 25.0) * 17.0 / 25.0 + 2.0
    if frameTo >= 17.0:
        frameTo -= 17.0
    return frameTo

def getAnimToFrameRW(animFromFrame):
    frameTo = (animFromFrame % 17.0) * 25.0 / 17.0 + 0.0
    if frameTo >= 25.0:
        frameTo -= 25.0
    return frameTo

def getAnimToFrameWJ(animFromFrame):
    frameTo = (animFromFrame % 25.0) * 14.0 / 25.0 + 12.0
    if frameTo >= 14.0:
        frameTo -= 14.0
    return frameTo

def getAnimToFrameJW(animFromFrame):
    frameTo = (animFromFrame % 14.0) * 25.0 / 14.0 + 7.0
    if frameTo >= 25.0:
        frameTo -= 25.0
    return frameTo

data.deltaSpeedInv = 1.0 / (data.ctrlData['linSpeedQ'] - data.ctrlData['linSpeed'])
def lerpValueWR(actorCtrl, deltaSpeedInv, ctrlData):
    return (abs(actorCtrl.linear_speed.y) - ctrlData['linSpeed']) * deltaSpeedInv

def lerpValueRW(actorCtrl, deltaSpeedInv, ctrlData):
    return (ctrlData['linSpeedQ'] - abs(actorCtrl.linear_speed.y)) * deltaSpeedInv

data.linDuration = ((data.ctrlData['linSpeedQ'] - data.ctrlData['linSpeed']) / 
                                                                        data.ctrlData['linAccel'])
data.animTransData = {
    'walk-run':{'type':'param', 'animFrom':'walk','animTo':'run','duration':data.linDuration,
                'animToFrame':getAnimToFrameWR, 'playRate':1.0,
                'playRateRatios':(1.4705882352941178, 0.68), 'usePose':True,'loop':True,
                'doneEvent':'TransDoneWR', 'lerpValue':lerpValueWR},
    'run-walk':{'type':'param', 'animFrom':'run','animTo':'walk','duration':data.linDuration, 
                'animToFrame':getAnimToFrameRW, 'playRate':1.0,
                'playRateRatios':(0.68, 1.4705882352941178), 'usePose':True,'loop':True,
                'doneEvent':'TransDoneRW', 'lerpValue':lerpValueRW},
    'walk-jump':{'type':'time', 'animFrom':'walk', 'animTo':'jump', 'duration':1.0,
                'animToFrame':getAnimToFrameWJ, 'playRate':1.0,
                'playRateRatios':(1.0, 1.0), 'usePose':False,'loop':True,
                'doneEvent':'TransDoneWJ', 'lerpValue':None},
    'jump-walk':{'type':'time', 'animFrom':'jump', 'animTo':'walk', 'duration':1.5,
                'animToFrame':getAnimToFrameJW, 'playRate':1.0,
                'playRateRatios':(1.0, 1.0), 'usePose':False,'loop':True,
                'doneEvent':'TransDoneJW', 'lerpValue':None}
    }

## ragdoll related data
data.SPHERE = GamePhysicsManager.SPHERE
data.CAPSULE = GamePhysicsManager.CAPSULE
data.BOX = GamePhysicsManager.BOX
data.Xup = GamePhysicsManager.X_up
data.Yup = GamePhysicsManager.Y_up
data.CONETWIST = BT3Constraint.CONETWIST
data.POINT2POINT = BT3Constraint.POINT2POINT
data.DOF6 = BT3Constraint.DOF6
data.STOP_ERP = BT3Constraint.CONSTRAINT_STOP_ERP
data.STOP_CFM = BT3Constraint.CONSTRAINT_STOP_CFM
# ralph ActorRagDoll
data.jointHierarchyArray = [
  # level 0
  {'joint':'Skeleton', 'children':3,
   'jointP':'Controls',
   'bone':{'type': data.SPHERE, 'mass': 10.0, 'dims':[0.25]},
   'constraint':None,
  },
  # level 1
  {'joint':'Chest', 'children':3,
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.5]},
   'constraint':{'type': data.CONETWIST},
  },
  {'joint':'LeftHip', 'children':1,
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.4]},
   'constraint':{'type': data.DOF6},
  },
  {'joint':'RightHip', 'children':1,
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.4]},
   'constraint':{'type': data.DOF6},
  },
  # level 2
  {'joint':'LeftShoulder', 'children':1,
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.3]},
   'constraint':{'type': data.DOF6},
  },
  {'joint':'HeadBone', 'children':0,
   'jointP':'Neck',
   'bone':{'type': data.SPHERE, 'mass': 5.0, 'dims':[1.5]},
   'constraint':{'type': data.DOF6},
  },
  {'joint':'RightShoulder', 'children':1,
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.3]},
   'constraint':{'type': data.DOF6},
  },
  {'joint':'LeftKnee', 'children':1,
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.4]},
   'constraint':{'type': data.CONETWIST},
  },
  {'joint':'RightKnee', 'children':1,
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.4]},
   'constraint':{'type': data.CONETWIST},
  },
  # level 3
  {'joint':'LeftElbow', 'children':1,
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.3]},
   'constraint':{'type': data.CONETWIST},
  },
  {'joint':'RightElbow', 'children':1,
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.3]},
   'constraint':{'type': data.CONETWIST},
  },
  {'joint':'LeftBall', 'children':0,
   'jointP':'LeftAnkle',
   'bone':{'type': data.BOX, 'mass': 1.0, 'dims':[0.4]},
   'constraint':{'type': data.CONETWIST},
  },
  {'joint':'RightBall', 'children':0,
   'jointP':'RightAnkle',
   'bone':{'type': data.BOX, 'mass': 1.0, 'dims':[0.4]},
   'constraint':{'type': data.CONETWIST},
  },
  # level 4
  {'joint':'LeftHand', 'children':0,
   'jointP':'LeftWrist',
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.2]},
   'constraint':{'type': data.CONETWIST},
  },
  {'joint':'RightHand', 'children':0,
   'jointP':'RightWrist',
   'bone':{'type': data.SPHERE, 'mass': 1.0, 'dims':[0.2]},
   'constraint':{'type': data.CONETWIST},
  },
]

## CCDik related data
# right CCDik object
data.jointBaseParentR = 'Skeleton'
data.jointHierarchyArrayR = [
    {'name':'RightHip', 'children': 1,
     'upAxis':LVector3f(0, -1, 0), 
     'baseForwardAxis': LVector3f(0, 0, -1)},
    {'name':'RightKnee', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(75, 45, 75, 45),
     'w':1.0}, #
    {'name':'RightAnkle', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(5, 15, 120, 15),
     'w':1.0}, #
    {'name':'RightBall', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
    'endEffector':True,
     'angles':(150, 15, 5, 15),
     'w':1.0}, #
    {'name':'RightToe', 'children': 0,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(95, 15, 0, 15),
     'w':1.0}, #
]
data.minZR = {'RightBall':0.175, 'RightToe':0.175}
# left CCDik object
data.jointBaseParentL = 'Skeleton'
data.jointHierarchyArrayL = [
    {'name':'LeftHip', 'children': 1,
     'upAxis':LVector3f(0, -1, 0), 
     'baseForwardAxis': LVector3f(0, 0, -1)},
    {'name':'LeftKnee', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(75, 45, 75, 45),
     'w':1.0}, #
    {'name':'LeftAnkle', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(5, 15, 120, 15),
     'w':1.0}, #
    {'name':'LeftBall', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
    'endEffector':True,
     'angles':(150, 15, 5, 15),
     'w':1.0}, #
    {'name':'LeftToe', 'children': 0,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(95, 15, 0, 15),
     'w':1.0}, #
]
data.minZL = {'LeftBall':0.175, 'LeftToe':0.175}
