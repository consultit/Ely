'''
Created on Jan 08, 2018

Class implementing physics based ragdoll.

@author: consultit
'''

from ely.physics import GamePhysicsManager
from panda3d.core import load_prc_file_data, Filename, BitMask32, NodePath, \
    TextureStage, Texture
from direct.showbase.ShowBase import ShowBase
from common import addPlane, loadActorRagDoll, setupRagdoll, resetRagdoll, \
    cycleAnims, toggleVisibility, toggleWireframe
from common import toggleDebugDraw
import argparse
import textwrap
import os

scriptPath = os.path.dirname(os.path.abspath(__file__))
prefD = os.path.join(scriptPath, '..', 'assets')
dataDirs = [
    os.path.join(prefD, 'misc', 'test'),
    os.path.join(prefD, 'models', 'test'),
    os.path.join(prefD, 'textures', 'test'),
    os.path.join(prefD, 'sounds', 'test'),
    os.path.join(prefD, 'scripts', 'test'),
    os.path.join(prefD, 'shaders', 'test'), ]

for dataDir in dataDirs:
    load_prc_file_data('', 'model-path ' + dataDir)
load_prc_file_data('', 'win-size 1024 768')
load_prc_file_data('', 'show-frame-rate-meter #t')
load_prc_file_data('', 'sync-video #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')

groupMask = BitMask32(0x10)
collideMask = BitMask32(0x10)
toggleDebugFlag = [False]
app = None

if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''
    This script will create a ragdoll from the given actor model
      
    Commands:
        - 's': to setup the ragdoll.
        - 'r': to reset the ragdoll.
        - 'v': to toggle model visibility.
        - 'w': to toggle model wireframe mode.
        - 'c': to cycle model animations.
        - 'd': to debug draw physics.
        - 'a': hold the key to pick-and-drag a joint's rigid body.
    '''))
    # set up arguments
    dataPath = os.path.dirname(os.path.abspath(__file__))
    parser.add_argument('--actor-data', type=str,
                        default=os.path.join(
                            dataPath, 'testRagdollData_john.py'),
                        help='the actor model data path')
    parser.add_argument('-s', '--scale', type=float, default=1.0,
                        help='the actor scale')
    parser.add_argument('-z', '--z-coord', type=float, default=10.0,
                        help='the actor''s z coordinate')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    actorData = args.actor_data
    actorScale = args.scale
    actorZ = args.z_coord
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    #
    app = ShowBase()

    # Setup our physics manager
    physicsMgr = GamePhysicsManager(10, app.render, groupMask, collideMask)
    # enable object picking
    physicsMgr.enable_picking(app.render, NodePath.any_path(app.camNode), app.mouseWatcher.node(),
                              'a', 'a-up')
    # reparent reference node to render
    physicsMgr.get_reference_node_path().reparent_to(app.render)
    # debug physics
    physicsMgr.get_reference_node_path_debug().reparent_to(app.render)
    app.accept('d', toggleDebugDraw, [toggleDebugFlag])
    # start physics default update task
    physicsMgr.start_default_update()

    # Add a plane to collide with
    plane = addPlane(app, width=30.0, depth=30.0, texture='dry-grass.png')
    planeTex = plane.owner_object.get_texture(TextureStage.default)
    planeTex.set_wrap_u(Texture.WM_mirror)
    planeTex.set_wrap_v(Texture.WM_mirror)
    plane.owner_object.set_tex_scale(TextureStage.default, 10.0, 10.0)

    # create the ragdoll
    ragdoll = loadActorRagDoll(actorData, physicsMgr, groupMask, collideMask)
    ragdoll.getJointHierarchyArray()

    # prepare ragdoll
    ragdoll.reparent_to(app.render)
    ragdoll.set_scale(actorScale)
    ragdoll.set_z(actorZ)

    # setup/reset ragdoll
    app.accept('s', setupRagdoll, [ragdoll, app])
    app.accept('r', resetRagdoll, [ragdoll, app])

    # cycle among ragdoll's animations
    animList = ragdoll.get_anim_names()
    i = [0]
    app.accept('c', cycleAnims, [ragdoll, animList, i])

    # toggle ragdoll's visibility
    app.accept('v', toggleVisibility, [ragdoll])

    # toggle ragdoll's wire frame mode on/off
    app.accept('w', toggleWireframe, [ragdoll])

    # setup camera and run
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 90.0, -15.0)
    trackball.set_hpr(0.0, 5.0, 0.0)
    app.run()
