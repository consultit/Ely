'''
Created on Dec 10, 2017

@author: consultit
'''

from panda3d.core import WindowProperties, LVecBase3f, LVector3f, ClockObject


class Driver(object):
    '''Driver class'''

    def __init__(self, app, ownerObjectNP, taskSort=0):
        self.app = app
        self.mWin = self.app.win
        self.mOwnerObjectNP = ownerObjectNP
        self.mTaskSort = taskSort
        self.__do_reset()
        self.__do_initialize()

    def __del__(self):
        self.__do_finalize()
        self.__do_reset()
        self.mOwnerObjectNP.clear()
        self.mWin = None

    def update(self, task):
        #
        dt = ClockObject.get_global_clock().get_dt()
        # handle mouse
        if self.mMouseMove and (self.mMouseEnabledH or self.mMouseEnabledP):
            md = self.mWin.get_pointer(0)
            deltaX = md.get_x() - self.mCentX
            deltaY = md.get_y() - self.mCentY

            if self.mWin.move_pointer(0, self.mCentX, self.mCentY):
                if self.mMouseEnabledH and (deltaX != 0.0):
                    self.mOwnerObjectNP.set_h(
                        self.mOwnerObjectNP.get_h() - deltaX * self.mSensX * self.mSignOfMouse)
                if self.mMouseEnabledP and (deltaY != 0.0):
                    self.mOwnerObjectNP.set_p(
                        self.mOwnerObjectNP.get_p() - deltaY * self.mSensY * self.mSignOfMouse)
            # if self.mMouseMoveKey is True we are controlling mouse movements
            # so we need to reset self.mMouseMove to False
            if self.mMouseMoveKey:
                self.mMouseMove = False
        # update position/orientation
        self.mOwnerObjectNP.set_y(self.mOwnerObjectNP,
                                  self.mActualSpeedXYZ.get_y() * dt * self.mSignOfTranslation)
        self.mOwnerObjectNP.set_x(self.mOwnerObjectNP,
                                  self.mActualSpeedXYZ.get_x() * dt * self.mSignOfTranslation)
        self.mOwnerObjectNP.set_z(
            self.mOwnerObjectNP, self.mActualSpeedXYZ.get_z() * dt)
        # head
        if self.mHeadLimitEnabled:
            head = self.mOwnerObjectNP.get_h() + self.mActualSpeedH * dt * self.mSignOfMouse
            if head > self.mHLimit:
                head = self.mHLimit
            elif head < -self.mHLimit:
                head = -self.mHLimit
            self.mOwnerObjectNP.set_h(head)
        else:
            self.mOwnerObjectNP.set_h(
                self.mOwnerObjectNP.get_h() + self.mActualSpeedH * dt * self.mSignOfMouse)
        # pitch
        if self.mPitchLimitEnabled:
            pitch = self.mOwnerObjectNP.get_p() + self.mActualSpeedP * \
                dt * self.mSignOfMouse
            if pitch > self.mPLimit:
                pitch = self.mPLimit
            elif pitch < -self.mPLimit:
                pitch = -self.mPLimit
            self.mOwnerObjectNP.set_p(pitch)
        else:
            self.mOwnerObjectNP.set_p(
                self.mOwnerObjectNP.get_p() + self.mActualSpeedP * dt * self.mSignOfMouse)

        # update speeds
        # y axis
        if self.mForward and (not self.mBackward):
            if self.mAccelXYZ.get_y() != 0.0:
                # accelerate
                self.mActualSpeedXYZ.set_y(
                    self.mActualSpeedXYZ.get_y() - self.mAccelXYZ.get_y() * dt)
                if self.mActualSpeedXYZ.get_y() < -self.mMaxSpeedXYZ.get_y():
                    # limit speed
                    self.mActualSpeedXYZ.set_y(-self.mMaxSpeedXYZ.get_y())
            else:
                # kinematic
                self.mActualSpeedXYZ.set_y(-self.mMaxSpeedXYZ.get_y())
        elif self.mBackward and (not self.mForward):
            if self.mAccelXYZ.get_y() != 0.0:
                # accelerate
                self.mActualSpeedXYZ.set_y(
                    self.mActualSpeedXYZ.get_y() + self.mAccelXYZ.get_y() * dt)
                if self.mActualSpeedXYZ.get_y() > self.mMaxSpeedXYZ.get_y():
                    # limit speed
                    self.mActualSpeedXYZ.set_y(self.mMaxSpeedXYZ.get_y())
            else:
                # kinematic
                self.mActualSpeedXYZ.set_y(self.mMaxSpeedXYZ.get_y())
        elif self.mActualSpeedXYZ.get_y() != 0.0:
            if self.mActualSpeedXYZ.get_y() * self.mActualSpeedXYZ.get_y() \
                    < self.mMaxSpeedSquaredXYZ.get_y() * self.mStopThreshold:
                # stop
                self.mActualSpeedXYZ.set_y(0.0)
            else:
                # decelerate
                self.mActualSpeedXYZ.set_y(
                    self.mActualSpeedXYZ.get_y() * (1.0 - min(self.mFrictionXYZ * dt, 1.0)))
        # x axis
        if self.mStrafeLeft and (not self.mStrafeRight):
            if self.mAccelXYZ.get_x() != 0.0:
                # accelerate
                self.mActualSpeedXYZ.set_x(
                    self.mActualSpeedXYZ.get_x() + self.mAccelXYZ.get_x() * dt)
                if self.mActualSpeedXYZ.get_x() > self.mMaxSpeedXYZ.get_x():
                    # limit speed
                    self.mActualSpeedXYZ.set_x(self.mMaxSpeedXYZ.get_x())
            else:
                # kinematic
                self.mActualSpeedXYZ.set_x(self.mMaxSpeedXYZ.get_x())
        elif self.mStrafeRight and (not self.mStrafeLeft):
            if self.mAccelXYZ.get_x() != 0.0:
                # accelerate
                self.mActualSpeedXYZ.set_x(
                    self.mActualSpeedXYZ.get_x() - self.mAccelXYZ.get_x() * dt)
                if self.mActualSpeedXYZ.get_x() < -self.mMaxSpeedXYZ.get_x():
                    # limit speed
                    self.mActualSpeedXYZ.set_x(-self.mMaxSpeedXYZ.get_x())
            else:
                # kinematic
                self.mActualSpeedXYZ.set_x(-self.mMaxSpeedXYZ.get_y())
        elif self.mActualSpeedXYZ.get_x() != 0.0:
            if self.mActualSpeedXYZ.get_x() * self.mActualSpeedXYZ.get_x() \
                    < self.mMaxSpeedSquaredXYZ.get_x() * self.mStopThreshold:
                # stop
                self.mActualSpeedXYZ.set_x(0.0)
            else:
                # decelerate
                self.mActualSpeedXYZ.set_x(
                    self.mActualSpeedXYZ.get_x() * (1.0 - min(self.mFrictionXYZ * dt, 1.0)))
        # z axis
        if self.mUp and (not self.mDown):
            if self.mAccelXYZ.get_z() != 0.0:
                # accelerate
                self.mActualSpeedXYZ.set_z(
                    self.mActualSpeedXYZ.get_z() + self.mAccelXYZ.get_z() * dt)
                if self.mActualSpeedXYZ.get_z() > self.mMaxSpeedXYZ.get_z():
                    # limit speed
                    self.mActualSpeedXYZ.set_z(self.mMaxSpeedXYZ.get_z())
            else:
                # kinematic
                self.mActualSpeedXYZ.set_z(self.mMaxSpeedXYZ.get_z())
        elif self.mDown and (not self.mUp):
            if self.mAccelXYZ.get_z() != 0.0:
                # accelerate
                self.mActualSpeedXYZ.set_z(
                    self.mActualSpeedXYZ.get_z() - self.mAccelXYZ.get_z() * dt)
                if self.mActualSpeedXYZ.get_z() < -self.mMaxSpeedXYZ.get_z():
                    # limit speed
                    self.mActualSpeedXYZ.set_z(-self.mMaxSpeedXYZ.get_z())
            else:
                # kinematic
                self.mActualSpeedXYZ.set_z(-self.mMaxSpeedXYZ.get_z())
        elif self.mActualSpeedXYZ.get_z() != 0.0:
            if self.mActualSpeedXYZ.get_z() * self.mActualSpeedXYZ.get_z() \
                    < self.mMaxSpeedSquaredXYZ.get_z() * self.mStopThreshold:
                # stop
                self.mActualSpeedXYZ.set_z(0.0)
            else:
                # decelerate
                self.mActualSpeedXYZ.set_z(
                    self.mActualSpeedXYZ.get_z() * (1.0 - min(self.mFrictionXYZ * dt, 1.0)))
        # rotation h
        if self.mHeadLeft and (not self.mHeadRight):
            if self.mAccelHP != 0.0:
                # accelerate
                self.mActualSpeedH += self.mAccelHP * dt
                if self.mActualSpeedH > self.mMaxSpeedHP:
                    # limit speed
                    self.mActualSpeedH = self.mMaxSpeedHP
            else:
                # kinematic
                self.mActualSpeedH = self.mMaxSpeedHP
        elif self.mHeadRight and (not self.mHeadLeft):
            if self.mAccelHP != 0.0:
                # accelerate
                self.mActualSpeedH -= self.mAccelHP * dt
                if self.mActualSpeedH < -self.mMaxSpeedHP:
                    # limit speed
                    self.mActualSpeedH = -self.mMaxSpeedHP
            else:
                # kinematic
                self.mActualSpeedH = -self.mMaxSpeedHP
        elif self.mActualSpeedH != 0.0:
            if self.mActualSpeedH * self.mActualSpeedH < self.mMaxSpeedSquaredHP * self.mStopThreshold:
                # stop
                self.mActualSpeedH = 0.0
            else:
                # decelerate
                self.mActualSpeedH = self.mActualSpeedH * \
                    (1.0 - min(self.mFrictionHP * dt, 1.0))
        # rotation p
        if self.mPitchUp and (not self.mPitchDown):
            if self.mAccelHP != 0.0:
                # accelerate
                self.mActualSpeedP += self.mAccelHP * dt
                if self.mActualSpeedP > self.mMaxSpeedHP:
                    # limit speed
                    self.mActualSpeedP = self.mMaxSpeedHP
            else:
                # kinematic
                self.mActualSpeedP = self.mMaxSpeedHP
        elif self.mPitchDown and (not self.mPitchUp):
            if self.mAccelHP != 0.0:
                # accelerate
                self.mActualSpeedP -= self.mAccelHP * dt
                if self.mActualSpeedP < -self.mMaxSpeedHP:
                    # limit speed
                    self.mActualSpeedP = -self.mMaxSpeedHP
            else:
                # kinematic
                self.mActualSpeedP = -self.mMaxSpeedHP
        elif self.mActualSpeedP != 0.0:
            if self.mActualSpeedP * self.mActualSpeedP < self.mMaxSpeedSquaredHP * self.mStopThreshold:
                # stop
                self.mActualSpeedP = 0.0
            else:
                # decelerate
                self.mActualSpeedP = self.mActualSpeedP * \
                    (1.0 - min(self.mFrictionHP * dt, 1.0))
                #
        return task.cont

    # enable/disable
    def enable(self):
        # if enabled return
        if self.mEnabled:
            return False
        # actual enabling
        self.__do_enable()
        #
        return True

    def disable(self):
        # if not enabled return
        if not self.mEnabled:
            return False

        # actual disabling
        self.__do_disable()
        #
        return True

    def is_enabled(self):
        return self.mEnabled

    # enable/disable controls
    def enable_forward(self, enable):
        if self.mForwardKey:
            self.mForward = enable

    def is_forward_enabled(self):
        return self.mForward

    def enable_backward(self, enable):
        if self.mBackwardKey:
            self.mBackward = enable

    def is_backward_enabled(self):
        return self.mBackward

    def enable_strafe_left(self, enable):
        if self.mStrafeLeftKey:
            self.mStrafeLeft = enable

    def is_strafe_left_enabled(self):

        return self.mStrafeLeft

    def enable_strafe_right(self, enable):
        if self.mStrafeRightKey:
            self.mStrafeRight = enable

    def is_strafe_right_enabled(self):
        return self.mStrafeRight

    def enable_up(self, enable):
        if self.mUpKey:
            self.mUp = enable

    def is_up_enabled(self):
        return self.mUp

    def enable_down(self, enable):
        if self.mDownKey:
            self.mDown = enable

    def is_down_enabled(self):
        return self.mDown

    def enable_head_left(self, enable):
        if self.mHeadLeftKey:
            self.mHeadLeft = enable

    def is_head_left_enabled(self):
        return self.mHeadLeft

    def enable_head_right(self, enable):
        if self.mHeadRightKey:
            self.mHeadRight = enable

    def is_head_right_enabled(self):
        return self.mHeadRight

    def enable_pitch_up(self, enable):
        if self.mPitchUpKey:
            self.mPitchUp = enable

    def is_pitch_up_enabled(self):
        return self.mPitchUp

    def enable_pitch_down(self, enable):
        if self.mPitchDownKey:
            self.mPitchDown = enable

    def is_pitch_down_enabled(self):
        return self.mPitchDown

    def enable_mouse_move(self, enable):
        if self.mMouseMoveKey:
            self.mMouseMove = enable

    def is_mouse_move_enabled(self):
        return self.mMouseMove

    # max values
    def set_head_limit(self, enabled, hLimit):
        self.mHeadLimitEnabled = enabled
        if hLimit >= 0.0:
            self.mHLimit = hLimit
        else:
            self.mHLimit = -hLimit

    def set_pitch_limit(self, enabled, pLimit):
        self.mPitchLimitEnabled = enabled
        if pLimit >= 0.0:
            self.mPLimit = pLimit
        else:
            self.mPLimit = -pLimit

    def set_max_linear_speed(self, maxLinearSpeed):
        self.mMaxSpeedXYZ = LVector3f(
            abs(maxLinearSpeed.get_x()),
            abs(maxLinearSpeed.get_y()),
            abs(maxLinearSpeed.get_z()))
        self.mMaxSpeedSquaredXYZ = LVector3f(
            maxLinearSpeed.get_x() * maxLinearSpeed.get_x(),
            maxLinearSpeed.get_y() * maxLinearSpeed.get_y(),
            maxLinearSpeed.get_z() * maxLinearSpeed.get_z())

    def set_max_angular_speed(self, maxAngularSpeed):
        self.mMaxSpeedHP = abs(maxAngularSpeed)
        self.mMaxSpeedSquaredHP = maxAngularSpeed * maxAngularSpeed

    def get_max_speeds(self):
        return (self.mMaxSpeedXYZ, self.mMaxSpeedHP)

    def set_linear_accel(self, linearAccel):
        self.mAccelXYZ = LVector3f(
            abs(linearAccel.get_x()),
            abs(linearAccel.get_y()),
            abs(linearAccel.get_z()))

    def set_angular_accel(self, angularAccel):
        self.mAccelHP = abs(angularAccel)

    def get_accels(self):
        return (self.mAccelXYZ, self.mAccelHP)

    def set_linear_friction(self, linearFriction):
        self.mFrictionXYZ = abs(linearFriction)

    def set_angular_friction(self, angularFriction):
        self.mFrictionHP = abs(angularFriction)

    def get_frictions(self):
        return (self.mFrictionXYZ, self.mFrictionHP)

    def set_sens(self, sensX, sensY):
        self.mSensX = abs(sensX)
        self.mSensY = abs(sensY)

    def get_sens(self):
        return (self.mSensX, self.mSensY)

    def set_fast_factor(self, factor):
        self.mFastFactor = abs(factor)

    def get_fast_factor(self):
        return self.mFastFactor

    # speed current values
    def get_current_speeds(self):
        angularSpeeds = []
        angularSpeeds.append(self.mActualSpeedH)
        angularSpeeds.append(self.mActualSpeedP)
        return (self.mActualSpeedXYZ, angularSpeeds)

    # private member functions
    def __do_reset(self):
        #
        self.mEnabled = False
        self.mForward = self.mBackward = self.mStrafeLeft = self.mStrafeRight = self.mUp = self.mDown = \
            self.mHeadLeft = self.mHeadRight = self.mPitchUp = self.mPitchDown = False
        # by default we consider mouse moved on every update, because
        # we want mouse poll by default this can be changed by calling
        # the enabler (for example by an handler responding to mouse-move
        # event if it is possible. See: http:#www.panda3d.org/forums/viewtopic.php?t=9326
        # http:#www.panda3d.org/forums/viewtopic.php?t=6049)
        self.mMouseMove = True
        self.mForwardKey = self.mBackwardKey = self.mStrafeLeftKey = self.mStrafeRightKey = self.mUpKey = self.mDownKey = \
            self.mHeadLeftKey = self.mHeadRightKey = self.mPitchUpKey = self.mPitchDownKey = \
            self.mMouseMoveKey = False
        self.mSpeedKey = 'shift'
        self.mMouseEnabledH = self.mMouseEnabledP = self.mHeadLimitEnabled = self.mPitchLimitEnabled = \
            False
        self.mHLimit = self.mPLimit = 0.0
        self.mSignOfTranslation = self.mSignOfMouse = 1
        self.mFastFactor = 0.0
        self.mActualSpeedXYZ = self.mMaxSpeedXYZ = self.mMaxSpeedSquaredXYZ = LVecBase3f()
        self.mActualSpeedH = self.mActualSpeedP = self.mMaxSpeedHP = self.mMaxSpeedSquaredHP = 0.0
        self.mAccelXYZ = LVecBase3f()
        self.mAccelHP = 0.0
        self.mFrictionXYZ = self.mFrictionHP = 0.0
        self.mStopThreshold = 0.0
        self.mSensX = self.mSensY = 0.0
        self.mCentX = self.mCentY = 0.0

    def __do_initialize(self):
        # inverted setting (1/-1): not inverted -> 1, inverted -> -1
        self.mSignOfTranslation = 1
        self.mSignOfMouse = 1
        # head limit: enabled@[limit] limit >= 0.0
        self.mHeadLimitEnabled = False
        self.mHLimit = 0.0
        # pitch limit: enabled@[limit] limit >= 0.0
        self.mPitchLimitEnabled = False
        self.mPLimit = 0.0
        # mouse movement setting
        self.mMouseEnabledH = False
        self.mMouseEnabledP = False
        # key events setting
        # backward key
        self.mBackwardKey = True
        # down key
        self.mDownKey = True
        # forward key
        self.mForwardKey = True
        # strafeLeft key
        self.mStrafeLeftKey = True
        # strafeRight key
        self.mStrafeRightKey = True
        # headLeft key
        self.mHeadLeftKey = True
        # headRight key
        self.mHeadRightKey = True
        # pitchUp key
        self.mPitchUpKey = True
        # pitchDown key
        self.mPitchDownKey = True
        # up key
        self.mUpKey = True
        # mouseMove key: enabled/disabled
        self.mMouseMoveKey = False
        # speedKey
        if not (self.mSpeedKey == 'control'
                or self.mSpeedKey == 'alt'
                or self.mSpeedKey == 'shift'):
            self.mSpeedKey = 'shift'
        #
        # max linear speed (>=0)
        self.mMaxSpeedXYZ = LVecBase3f(5.0, 5.0, 5.0)
        self.mMaxSpeedSquaredXYZ = LVector3f(self.mMaxSpeedXYZ.get_x() * self.mMaxSpeedXYZ.get_x(),
                                             self.mMaxSpeedXYZ.get_y() * self.mMaxSpeedXYZ.get_y(),
                                             self.mMaxSpeedXYZ.get_z() * self.mMaxSpeedXYZ.get_z())
        # max angular speed (>=0)
        self.mMaxSpeedHP = 5.0
        self.mMaxSpeedSquaredHP = self.mMaxSpeedHP * self.mMaxSpeedHP
        # linear accel (>=0)
        self.mAccelXYZ = LVecBase3f(5.0, 5.0, 5.0)
        # angular accel (>=0)
        self.mAccelHP = 5.0
        # reset actual speeds
        self.mActualSpeedXYZ = LVector3f()
        self.mActualSpeedH = 0.0
        self.mActualSpeedP = 0.0
        # linear friction (>=0)
        self.mFrictionXYZ = 5.0
        # angular friction (>=0)
        self.mFrictionHP = 5.0
        # stop threshold ([0.0, 1.0])
        self.mStopThreshold = 0.01
        # fast factor (>=0)
        self.mFastFactor = 5.0
        # sens x (>=0)
        self.mSensX = 0.2
        # sens_y (>=0)
        self.mSensY = 0.2
        #
        self.mCentX = self.mWin.get_properties().get_x_size() / 2
        self.mCentY = self.mWin.get_properties().get_y_size() / 2

    def __do_finalize(self):
        #if enabled: disable
        if self.mEnabled:
            # actual disabling
            self.__do_disable()

    def __do_enable(self):
        if self.mMouseEnabledH or self.mMouseEnabledP or self.mMouseMoveKey:
            # we want control through mouse movements
            # hide mouse cursor
            props = WindowProperties()
            props.set_cursor_hidden(True)
            self.mWin.request_properties(props)
            # reset mouse to start position
            self.mWin.move_pointer(0, self.mCentX, self.mCentY)
        #
        self.mEnabled = True

        # Add self.mUpdateTask to the active queue.
        self.app.taskMgr.add(self.update, 'Driver::update',
                             self.mTaskSort, appendTask=True)

    def __do_disable(self):
        if self.mMouseEnabledH or self.mMouseEnabledP or self.mMouseMoveKey:
            # we have control through mouse movements
            # show mouse cursor
            props = WindowProperties()
            props.set_cursor_hidden(False)
            self.mWin.request_properties(props)
        #
        self.mEnabled = False

        # Remove self.mUpdateTask from the active queue.
        self.app.taskMgr.remove('Driver::update')
