'''
Created on Sep 7, 2017

AnimTransition tests.

@author: consultit
'''

from ely.direct.animTransition import AnimTransition
from panda3d.core import load_prc_file_data, Filename, LVector3f
from direct.showbase.ShowBase import ShowBase
from common import loadActor, getDimensions
import argparse
import textwrap
import os

scriptPath = os.path.dirname(os.path.abspath(__file__))
prefD = os.path.join(scriptPath, '..', 'assets')
dataDirs = [
    os.path.join(prefD, 'misc', 'test'),
    os.path.join(prefD, 'models', 'test'),
    os.path.join(prefD, 'textures', 'test'),
    os.path.join(prefD, 'sounds', 'test'),
    os.path.join(prefD, 'scripts', 'test'),
    os.path.join(prefD, 'shaders', 'test'), ]

for dataDir in dataDirs:
    load_prc_file_data('', 'model-path ' + dataDir)
load_prc_file_data('', 'win-size 1024 768')
load_prc_file_data('', 'show-frame-rate-meter #t')
load_prc_file_data('', 'sync-video #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')


def getAnimToFrame(animFromFrame):
    global transData
    fromCycleLen = transData['animFrom']['cycleLen']
    toCycleLen = transData['animTo']['cycleLen']
    frameFrom = animFromFrame % fromCycleLen
    frameTo = frameFrom * toCycleLen / fromCycleLen + transData['delta']
    if frameTo >= toCycleLen:
        frameTo -= toCycleLen
    return frameTo


def getPlayRateRatios(animFrom, animTo):
    fromCycleLen = animFrom['cycleLen']
    toCycleLen = animTo['cycleLen']
    return (fromCycleLen / toCycleLen, toCycleLen / fromCycleLen)


def setTransData(transData):
    global actor, animTrans
    animTrans.animCtrlFrom = actor.get_anim_control(
        transData['animFrom']['name'])
    animTrans.animCtrlTo = actor.get_anim_control(
        transData['animTo']['name'])
    animTrans.loop = transData['loop']
    animTrans.usePose = transData['pose']
    animTrans.animToFrame = transData['frame']
    animTrans.duration = transData['duration']
    animTrans.playRateRatios = transData['ratios']


def switchAnimations():
    global transData, animTrans
    # swap from/to animations
    animFrom = transData['animTo']
    animTo = transData['animFrom']
    transData = transitions[animFrom['name'] + '-' + animTo['name']]
    setTransData(transData)
    print('next transition: ' + transData['animFrom']['name'] +
          ' -> ' + transData['animTo']['name'])
    print(animTrans._animToFrameMap)
    print(animTrans._animToFrameMap)
    print(animTrans._DeltaPRFrom, animTrans._DeltaPRTo)


def transDone(animTrans):
    animTrans.transitionDone()
    switchAnimations()


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will test smooth animation transitions
      
    Commands:
        - 's': start transition.
        - 'e': stop transition.
        - 'p': pause transition.
        - 'r': resume transition.
    '''))
    # set up arguments
    parser.add_argument('-m', '--model', type=str, default='john',
                        choices=['john', 'ralph', 'eve'],
                        help='the actor\'s model')
    parser.add_argument('-f', '--from-anim', type=str, default='walk',
                        choices=['walk', 'run', 'jump', 'offbalance'],
                        help='the actor\'s from animation')
    parser.add_argument('-t', '--to-anim', type=str, default='run',
                        choices=['walk', 'run', 'jump', 'offbalance'],
                        help='the actor\'s to animation')
    parser.add_argument('-z', '--frozen', action='store_true',
                        help='use frozen transition')
    parser.add_argument('-p', '--play-rate', type=float, default=1.0,
                        help='the animations\' play rate')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    #
    app = ShowBase()

    # get an actor
    actor = loadActor(args.model)
    actor.reparent_to(app.render)

    # # ANIMATION TRANSITION
    anims = {}
    if args.model == 'john':
        # john
        anims['walk'] = {
            'name': 'walk', 'cycleLen': 24.0, 'length': 48.0
        }
        anims['run'] = {
            'name': 'run', 'cycleLen': 17.0, 'length': 17.0
        }
    elif args.model == 'ralph':
        anims['walk'] = {
            'name': 'walk', 'cycleLen': 25.0, 'length': 25.0
        }
        anims['run'] = {
            'name': 'run', 'cycleLen': 17.0, 'length': 17.0
        }
        anims['jump'] = {
            'name': 'jump', 'cycleLen': 14.0, 'length': 14.0
        }
    elif args.model == 'eve':
        anims['walk'] = {
            'name': 'walk', 'cycleLen': 24.0, 'length': 24.0
        }
        anims['run'] = {
            'name': 'run', 'cycleLen': 16.0, 'length': 16.0
        }
        anims['offbalance'] = {
            'name': 'offbalance', 'cycleLen': 35.0, 'length': 35.0
        }
    # build transition's data
    animFrom = anims[args.from_anim]
    animTo = anims[args.to_anim]
    if not animFrom:
        animFrom = anims['walk']
    if not animTo:
        animFrom = anims['run']

    transitions = {}
    if args.model == 'john':
        transitions['walk-run'] = {
            'animFrom': anims['walk'], 'animTo': anims['run'], 'duration': 20.0,
            'frame': getAnimToFrame, 'delta': 3.0, 'loop': True, 'pose': True,
            'ratios': getPlayRateRatios(anims['walk'], anims['run'])
        }
        transitions['run-walk'] = {
            'animFrom': anims['run'], 'animTo': anims['walk'], 'duration': 20.0,
            'frame': getAnimToFrame, 'delta': 18.0, 'loop': True, 'pose': True,
            'ratios': getPlayRateRatios(anims['run'], anims['walk'])
        }
    elif args.model == 'ralph':
        transitions['walk-run'] = {
            'animFrom': anims['walk'], 'animTo': anims['run'], 'duration': 20.0,
            'frame': getAnimToFrame, 'delta': 2.0, 'loop': True, 'pose': True,
            'ratios': getPlayRateRatios(anims['walk'], anims['run'])
        }
        transitions['run-walk'] = {
            'animFrom': anims['run'], 'animTo': anims['walk'], 'duration': 20.0,
            'frame': getAnimToFrame, 'delta': 0.0, 'loop': True, 'pose': True,
            'ratios': getPlayRateRatios(anims['run'], anims['walk'])
        }
        transitions['walk-jump'] = {
            'animFrom': anims['walk'], 'animTo': anims['jump'], 'duration': 1.0,
            'frame': getAnimToFrame, 'delta': 12.0, 'loop': True, 'pose': False,
            'ratios': (1.0, 1.0)
        }
        transitions['jump-walk'] = {
            'animFrom': anims['jump'], 'animTo': anims['walk'], 'duration': 1.0,
            'frame': getAnimToFrame, 'delta': 7.0, 'loop': True, 'pose': False,
            'ratios': (1.0, 1.0)
        }
    elif args.model == 'eve':
        transitions['walk-run'] = {
            'animFrom': anims['walk'], 'animTo': anims['run'], 'duration': 20.0,
            'frame': getAnimToFrame, 'delta': 1.0, 'loop': True, 'pose': True,
            'ratios': getPlayRateRatios(anims['walk'], anims['run'])}
        transitions['run-walk'] = {
            'animFrom': anims['run'], 'animTo': anims['walk'], 'duration': 20.0,
            'frame': getAnimToFrame, 'delta': 0.0, 'loop': True, 'pose': True,
            'ratios': getPlayRateRatios(anims['run'], anims['walk'])}
        transitions['walk-offbalance'] = {
            'animFrom': anims['walk'], 'animTo': anims['offbalance'],
            'duration': 1.0, 'frame': getAnimToFrame, 'delta': 12.0,
            'loop': True, 'pose': False, 'ratios': (1.0, 1.0)
        }
        transitions['offbalance-walk'] = {
            'animFrom': anims['offbalance'], 'animTo': anims['walk'],
            'duration': 1.0, 'frame': getAnimToFrame, 'delta': 7.0,
            'loop': True, 'pose': False, 'ratios': (1.0, 1.0)
        }

    # # set transition data
    transData = transitions[animFrom['name'] + '-' + animTo['name']]
    animTrans = AnimTransition(app, actor.get_anim_control(
        animFrom['name']), actor.get_anim_control(animTo['name']))
    animTrans.actor = actor
    animTrans.playRate = args.play_rate
    animTrans.doneEvent = 'TransDone'
    animTrans.frozen = args.frozen
    setTransData(transData)

    # switch animation
    # start/stop & pause/resume transition
    def _transCmd(animTrans, cmd):
        if cmd == 's':
            print('transition started')
            animTrans.startTransition()
        elif cmd == 'e':
            print('transition stopped')
            animTrans.stopTransition()
        elif cmd == 'p':
            print('transition paused')
            animTrans.pauseTransition()
        elif cmd == 'r':
            print('transition resumed')
            animTrans.resumeTransition()

    app.accept('s', _transCmd, extraArgs=[animTrans, 's'])
    app.accept('e', _transCmd, extraArgs=[animTrans, 'e'])
    app.accept('p', _transCmd, extraArgs=[animTrans, 'p'])
    app.accept('r', _transCmd, extraArgs=[animTrans, 'r'])

    # transition done callback
    app.accept('TransDone', transDone, [animTrans])

    actor.set_play_rate(args.play_rate, transData['animFrom']['name'])
    actor.loop(transData['animFrom']['name'])
    actor.set_h(-45.0)
    print('next transition: ' + transData['animFrom']['name'] +
          ' -> ' + transData['animTo']['name'])
    print(animTrans._animToFrameMap)
    print(animTrans._DeltaPRFrom, animTrans._DeltaPRTo)

    # setup camera
    # get 'tight' dimensions of actor
    _, xC, yC, zC, xL, yL, zL = getDimensions(actor)
    #
    app.trackball.node().set_origin(LVector3f(xL, yL, zL))
    app.trackball.node().set_pos(-xC + xL, -yC + yL, -zC + zL)
    app.trackball.node().set_hpr(0, 0, 0)
    # run
    app.run()
