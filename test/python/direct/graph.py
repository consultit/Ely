'''
Created on May 8, 2019

@author: consultit
'''

from ely.direct.data_structures_and_algorithms.ch14.graph import Graph


def printGraph(g):
    print('\tis_directed: ', g.is_directed(), sep='')
    print('\tvertex_count: ', g.vertex_count(), sep='')
    print('\tedge_count: ', g.edge_count(), sep='')
    #
    print('\tvertices: [', end='')
    for v in g.vertices():
        print(v.element(), ',', sep='', end='')
    print(']')
    #
    print('\tedge: [', end='')
    for e in g.edges():
        u, v = e.endpoints()
        print('(', u.element(), ',', v.element(),
              ',', e.element(), '),', sep='', end='')
    print(']')


vLabels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'l']
edges = [('a', 'l'), ('a', 'b'), ('c', 'a'), ('c', 'd'), ('e', 'a'), ('e', 'f'), ('g', 'c'),
         ('g', 'h'), ('l', 'b'), ('f', 'c')]


def test(msg, directed, verticesToRemove, edgesToRemove):
    global vLabels, edges
    g = Graph(directed)
    vs = {}
    for l in vLabels:
        vs[l] = g.insert_vertex(l)
    for e in edges:
        g.insert_edge(vs[e[0]], vs[e[1]], vs[e[0]].element() +
                      '_' + vs[e[1]].element())
    print('Original ' + msg)
    printGraph(g)
    #
    for vLabel in verticesToRemove:
        g.remove_vertex(vs[vLabel])
    print('Vertices removed: ', verticesToRemove, sep='')
    printGraph(g)
    #
    for e in edgesToRemove:
        uLabel, vLabel = e[0], e[1]
        g.remove_edge(g.get_edge(vs[uLabel], vs[vLabel]))
    print('Edges removed: ', edgesToRemove, sep='')
    printGraph(g)
    print()


if __name__ == '__main__':
    test('undirected', directed=False, verticesToRemove=[
         'c'], edgesToRemove=[('g', 'h')])
    test('directed', directed=True, verticesToRemove=[
         'c'], edgesToRemove=[('g', 'h')])
    test('undirected remove only vertices', directed=False,
         verticesToRemove=['c', 'd', 'f'], edgesToRemove=[])
    test('directed remove only vertices', directed=True,
         verticesToRemove=['c', 'd', 'f'], edgesToRemove=[])
    test('undirected remove only edges', directed=False, verticesToRemove=[],
         edgesToRemove=[('a', 'b'), ('g', 'c'), ('c', 'd')])
    test('directed remove only edegs', directed=True, verticesToRemove=[],
         edgesToRemove=[('a', 'b'), ('g', 'c'), ('c', 'd')])
