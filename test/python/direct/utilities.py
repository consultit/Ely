'''
Created on Dec 15, 2017

@author: consultit
'''

from panda3d.core import CollisionTraverser, CollisionHandlerQueue, \
    CollisionRay, CollisionNode, BitMask32, LPoint3f, LVector3f


class Utilities(object):
    '''
    Utilities
    '''

    def __init__(self, root, mask):
        '''
        Constructor
        '''

        self.mRoot = root
        self.mMask = mask
        if not self.mRoot.is_empty():
            self.mCTrav = CollisionTraverser()
            self.mCollisionHandler = CollisionHandlerQueue()
            self.mPickerRay = CollisionRay()
            pickerNode = CollisionNode('Utilities.pickerNode')
            pickerNode.add_solid(self.mPickerRay)
            pickerNode.set_from_collide_mask(self.mMask)
            pickerNode.set_into_collide_mask(BitMask32.all_off())
            self.mCTrav.add_collider(self.mRoot.attach_new_node(pickerNode),
                                     self.mCollisionHandler)

    def get_collide_mask(self):
        '''
         * Returns the collide mask.
        '''

        return self.mMask

    def get_collision_root(self):
        '''
         * Returns the collision root.
        '''

        return self.mRoot

    def get_collision_traverser(self):
        '''
         * Returns the collision traverser.
         '''

        return self.mCTrav

    def get_collision_handler(self):
        '''
        * Returns the collision handler.
        '''

        return self.mCollisionHandler

    def get_collision_ray(self):
        '''
         * Returns the collision ray.
         '''

        return self.mPickerRay

    def get_bounding_dimensions(self, modelNP, modelDims, modelDeltaCenter):
        '''
         * Gets bounding dimensions of a model NodePath.
         * Puts results into the out parameters: modelDims, modelDeltaCenter and returns
         * modelRadius.
         * - modelDims = absolute dimensions of the model
         * - modelCenter + modelDeltaCenter = origin of coordinate system
         * - modelRadius = radius of the containing sphere
         '''

        # get 'tight' dimensions of model
        minP = LPoint3f()
        maxP = LPoint3f()
        modelNP.calc_tight_bounds(minP, maxP)
        #
        delta = maxP - minP
        deltaCenter = -(minP + delta / 2.0)
        #
        modelDims.set(abs(delta.get_x()), abs(
            delta.get_y()), abs(delta.get_z()))
        modelDeltaCenter.set(deltaCenter.get_x(),
                             deltaCenter.get_y(), deltaCenter.get_z())
        modelRadius = max(
            max(modelDims.get_x(), modelDims.get_y()), modelDims.get_z()) / 2.0
        return modelRadius

    def get_collision_height(self, rayOrigin, space):
        '''
         * Throws a ray downward (-z direction default) from rayOrigin.
         * If collisions are found returns a Pair_bool_float == (true, height),
         * with height equal to the z-value of the first one.
         * If collisions are not found returns a Pair_bool_float == (false, 0.0).
         '''

        # traverse downward starting at rayOrigin
        self.mPickerRay.set_direction(LVector3f(0.0, 0.0, -1.0))
        self.mPickerRay.set_origin(rayOrigin)
        self.mCTrav.traverse(self.mRoot)
        if (self.mCollisionHandler.get_num_entries() > 0):
            self.mCollisionHandler.sort_entries()
            entry0 = self.mCollisionHandler.get_entry(0)
            target = entry0.get_surface_point(space)
            collisionHeight = target.get_z()
            return (True, collisionHeight)
        #
        return (False, 0.0)
