'''
Created on Feb 24, 2018

@author: consultit
'''

from ely.direct.ragdoll import ActorRagDoll
from ely.physics import GamePhysicsManager
from panda3d.core import LPoint3f, LVecBase3f, GeomVertexArrayFormat, \
    InternalName, Geom, GeomVertexFormat, GeomVertexData, GeomVertexWriter, \
    LVector3f, GeomTriangles, GeomNode, NodePath
from direct.actor.Actor import Actor

global data


def getDimensions(actor):
    '''get 'tight' dimensions of actor'''

    minP = LPoint3f()
    maxP = LPoint3f()
    actor.calc_tight_bounds(minP, maxP)
    delta = maxP - minP
    actorDims = LVector3f(abs(delta.get_x()), abs(delta.get_y()),
                          abs(delta.get_z()))
    bodyRadius = ((actorDims.get_x() + actorDims.get_y() + actorDims.get_z())
                  / 150.0)
    xC = (minP.get_x() + maxP.get_x()) / 2.0
    yC = minP.get_y() - actorDims.get_z() * 3.8 / 2.0
    zC = minP.get_z() + actorDims.get_z() * 2.0 / 4.0
    xL = (minP.get_x() + maxP.get_x()) / 2.0
    yL = (minP.get_y() + maxP.get_y()) / 2.0
    zL = (minP.get_z() + maxP.get_z()) / 2.0
    return bodyRadius, xC, yC, zC, xL, yL, zL


def loadActorRagDoll(actorData, physicsMgr, groupMask, collideMask):
    # read ActorRagDoll data: jointHierarchyArray, models, anims
    exec(open(actorData).read(), globals())

    return ActorRagDoll(physicsMgr, data.jointHierarchyArray, groupMask, collideMask,
                        models=data.models, anims=data.anims)


def setupRagdoll(ragdoll, app):
    ragdoll.setup(app)


def resetRagdoll(ragdoll, app):
    ragdoll.reset(app)


def cycleAnims(ragdoll, animList, i):
    if i[0] < len(animList):
        ragdoll.loop(animList[i[0]])
        i[0] += 1
    else:
        ragdoll.stop()
        i[0] = 0


def toggleVisibility(ragdoll):
    if ragdoll.is_hidden():
        ragdoll.show()
    else:
        ragdoll.hide()


def toggleWireframe(ragdoll):
    if ragdoll.has_render_mode():
        ragdoll.clear_render_mode()
    else:
        ragdoll.set_render_mode_wireframe()


def addPlane(app, width, depth, texture):
    # Add a plane to collide with
    planeUpAxis = GamePhysicsManager.Z_up  # Z_up X_up Y_up
    sceneNP = loadPlane('SceneNP', app, width=width, depth=depth, upAxis=planeUpAxis,
                        texture=texture)
    # set sceneNP transform
    sceneNP.set_pos_hpr(LPoint3f.zero(), LVecBase3f.zero())
    # create scene's rigid_body (attached to the reference node)
    sceneRigidBodyNP = GamePhysicsManager.get_global_ptr(
    ).create_rigid_body('SceneRigidBody')
    # get a reference to the scene's rigid_body
    sceneRigidBody = sceneRigidBodyNP.node()
    # set some parameters
    sceneRigidBody.set_up_axis(planeUpAxis)
    sceneRigidBody.set_shape_type(GamePhysicsManager.PLANE)
    # set mass=0 before setup() for static (and kinematic) bodies
    sceneRigidBody.set_mass(0.0)
    # set the object for rigid body
    sceneRigidBody.set_owner_object(sceneNP)
    # setup
    sceneRigidBody.setup()
    #
    return sceneRigidBody


def loadActor(name):
    if name == 'john':
        model = Actor('john.egg',
                      {'run': 'john-run.egg',
                       'walk': 'john-walk.egg',
                       })
    elif name == 'ralph':
        model = Actor('ralph.egg',
                      {'run': 'ralph-run.egg',
                       'walk': 'ralph-walk.egg',
                       'jump': 'ralph-jump.egg',
                       })
    elif name == 'eve':
        model = Actor('eve.egg',
                      {'run': 'eve-run.egg',
                       'walk': 'eve-walk.egg',
                       'offbalance': 'eve-offbalance.egg',
                       })
    return model


def getInfos(actor):

    # generic infos
    actor.pprint()
    print('get_actor_info(): ', actor.get_actor_info())
    print('get_geom_node(): ', actor.get_geom_node())
    print('get_part_names(): ', actor.get_part_names())
    print('get_part_bundle_dict(): ', actor.get_part_bundle_dict())
    print('get_anim_names(): ', actor.get_anim_names())
    print('get_anim_control_dict(): ', actor.get_anim_control_dict())

    if actor.has_lod():
        print('get_lod_node(): ', actor.get_lod_node())
        print('get_lod_names(): ', actor.get_lod_names())


def getAnimCtrlAttr(ctrl, attr):

    attrs = ['frac', 'get_frac', 'frame', 'get_frame', 'frame_rate',
             'get_frame_rate', 'full_fframe', 'get_full_fframe', 'full_frame',
             'get_full_frame', 'next_frame', 'get_next_frame', 'num_frames',
             'get_num_frames', 'play_rate', 'get_play_rate', 'set_play_rate',
             'playing', 'is_playing', 'loop', 'pingpong', 'play', 'pose', 'stop',
             'get_anim', 'has_anim', 'get_anim_model', 'set_anim_model',
             'get_bound_joints', 'get_channel_index', 'get_part',
             'get_pending_done_event', 'set_pending_done_event', 'is_pending',
             'wait_pending']
    if attr in attrs:
        return getattr(ctrl, attr)
    else:
        print('not interesting or not existent attribute: ' + str(attr))
        return None


# debug flag
toggleDebugFlag = [False]


def toggleDebugDraw(toggleDebugFlag):
    '''toggle debug draw'''

    toggleDebugFlag[0] = not toggleDebugFlag[0]
    GamePhysicsManager.get_global_ptr().debug(toggleDebugFlag[0])


def loadPlane(name, app, width=30.0, depth=30.0, upAxis=GamePhysicsManager.Z_up,
              texture='dry-grass.png'):
    '''load plane stuff centered at (0,0,0)'''

    # Vertex Format
    arrayFormat = GeomVertexArrayFormat()
    arrayFormat.add_column(InternalName.make('vertex'), 3,
                           Geom.NT_float32, Geom.C_point)
    arrayFormat.add_column(InternalName.make('normal'), 3,
                           Geom.NT_float32, Geom.C_vector)
    arrayFormat.add_column(InternalName.make('texcoord'), 2,
                           Geom.NT_float32, Geom.C_texcoord)
    vertexFormat = GeomVertexFormat()
    vertexFormat.add_array(arrayFormat)
    # Pre-defined vertex formats
#         vertexFormatAdded = GeomVertexFormat.get_v3n3t2()
    vertexFormatAdded = GeomVertexFormat.register_format(vertexFormat)
    # Vertex Data
    vertexData = GeomVertexData('plane', vertexFormatAdded, Geom.UH_static)
    vertex = GeomVertexWriter(vertexData, 'vertex')
    normal = GeomVertexWriter(vertexData, 'normal')
    texcoord = GeomVertexWriter(vertexData, 'texcoord')
    # compute coords and normal according to up axis
    # 3------2      ^y           ^z           ^x
    # |      |      |            |            |
    # |      | +d   | Z_up   OR  | X_up   OR  | Y_up
    # |      |      |            |            |
    # 0------1      -----.x     -----.y     -----.z
    #    +w
    w = abs(width) / 0.5
    d = abs(depth) / 0.5
    # default: Z_up
    n = LVector3f(0.0, 0.0, 1.0)
    v0 = LPoint3f(-w, -d, 0.0)
    v1 = LPoint3f(w, -d, 0.0)
    v2 = LPoint3f(w, d, 0.0)
    v3 = LPoint3f(-w, d, 0.0)
    if upAxis == GamePhysicsManager.X_up:
        n = LVector3f(1.0, 0.0, 0.0)
        v0 = LPoint3f(0.0, -w, -d)
        v1 = LPoint3f(0.0, w, -d)
        v2 = LPoint3f(0.0, w, d)
        v3 = LPoint3f(0.0, -w, d)
    elif upAxis == GamePhysicsManager.Y_up:
        n = LVector3f(0.0, 1.0, 0.0)
        v0 = LPoint3f(-d, 0.0, -w)
        v1 = LPoint3f(d, 0.0, -w)
        v2 = LPoint3f(d, 0.0, w)
        v3 = LPoint3f(-d, 0.0, w)
    # normalize
    n.normalize()
    # fill-up vertex data (plane)
    # vertex 0
    vertex.add_data3f(v0)
    normal.add_data3f(n)
    texcoord.add_data2f(0.0, 0.0)
    # vertex 1
    vertex.add_data3f(v1)
    normal.add_data3f(n)
    texcoord.add_data2f(1.0, 0.0)
    # vertex 2
    vertex.add_data3f(v2)
    normal.add_data3f(n)
    texcoord.add_data2f(1.0, 1.0)
    # vertex 3
    vertex.add_data3f(v3)
    normal.add_data3f(n)
    texcoord.add_data2f(0.0, 1.0)
    # Creating the GeomPrimitive objects for plane
    planeTriangles = GeomTriangles(Geom.UH_static)
    # lower triangle
    planeTriangles.add_vertices(0, 1, 3)
    # higher triangle
    planeTriangles.add_vertices(2, 3, 1)
    # Putting your new geometry in the scene graph
    planeGeom = Geom(vertexData)
    planeGeom.add_primitive(planeTriangles)
    planeNode = GeomNode(name + 'Node')
    planeNode.add_geom(planeGeom)
    planeNP = NodePath(planeNode)
    # apply texture
    tex = app.loader.load_texture(texture)
    planeNP.set_texture(tex)
    #
    return planeNP
