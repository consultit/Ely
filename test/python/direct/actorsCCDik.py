'''
Created on Nov 2, 2017

CCDik method tests.

@author: consultit
'''

from ely.direct.ccdik import CCDik
from panda3d.core import load_prc_file_data, LVector3f, BitMask32, LVecBase4f
from direct.showbase.ShowBase import ShowBase
from common import loadActor, getDimensions
from picker import Picker
import argparse
import textwrap
import random
import os

random.seed()
randNum = random.random

scriptPath = os.path.dirname(os.path.abspath(__file__))
prefD = os.path.join(scriptPath, '..', 'assets')
dataDirs = [
    os.path.join(prefD, 'misc', 'test'),
    os.path.join(prefD, 'models', 'test'),
    os.path.join(prefD, 'textures', 'test'),
    os.path.join(prefD, 'sounds', 'test'),
    os.path.join(prefD, 'scripts', 'test'),
    os.path.join(prefD, 'shaders', 'test'), ]

for dataDir in dataDirs:
    load_prc_file_data('', 'model-path ' + dataDir)
load_prc_file_data('', 'win-size 1024 768')
load_prc_file_data('', 'show-frame-rate-meter #t')
load_prc_file_data('', 'sync-video #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')

# CCDik object's data
targetScale = 0.5
jointBaseParent = 'Hips'
jointHierarchyArray = [
    {'name': 'Spine', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     'baseForwardAxis': LVector3f(0, 0, 1)},
    {'name': 'Spine1', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     'angles': (15, 15, 15, 15),
     'w': 1.0},
    {'name': 'Spine2', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     'angles': (15, 15, 15, 15),
     'w': 1.0},
    {'name': 'Spine3', 'children': 3,
     'upAxis': LVector3f(0, -1, 0),
     'angles': (15, 15, 15, 15),
     'w': 1.0},
    {'name': 'LeftShoulder', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     'angles': (15, 60, 15, 0),  # (5, 170, 5, 0),
     'w': 1.0},
    {'name': 'Neck', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     'angles': (30, 30, 15, 30),  # (5, 0, 5, 170),
     'w': 1.0},
    {'name': 'RightShoulder', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     'angles': (15, 0, 15, 60),  # (35, 15, 35, 15),
     'w': 1.0},
    {'name': 'LeftArm', 'children': 1,
     'upAxis': LVector3f(0, 0, 1),
     'angles': (30, 90, 15, 0),  # (35, 15, 35, 15),
     'w': 1.0},
    {'name': 'Head', 'children': 0,
     'upAxis': LVector3f(0, -1, 0),
     'angles': (45, 45, 35, 45),
     'w': 1.0},
    {'name': 'RightArm', 'children': 1,
     'upAxis': LVector3f(0, 0, 1),
     'angles': (30, 0, 15, 90),
     'w': 1.0},
    {'name': 'LeftArmRoll', 'children': 1,
     'upAxis': LVector3f(0, 0, 1),
     'angles': (60, 80, 90, 5),
     'w': 1.0},
    {'name': 'RightArmRoll', 'children': 1,
     'upAxis': LVector3f(0, 0, 1),
     'angles': (60, 5, 90, 80),
     'w': 1.0},
    {'name': 'LeftForeArm', 'children': 1,
     'upAxis': LVector3f(0, 0, 1),
     'angles': (5, 5, 5, 5),
     'w': 1.0},
    {'name': 'RightForeArm', 'children': 1,
     'upAxis': LVector3f(0, 0, 1),
     'angles': (5, 5, 5, 5),
     'w': 1.0},
    {'name': 'LeftForeArmRoll', 'children': 1,
     'upAxis': LVector3f(0, 0, 1),
     'angles': (5, 120, 5, 5),
     'w': 1.0},
    {'name': 'RightForeArmRoll', 'children': 1,
     'upAxis': LVector3f(0, 0, 1),
     'angles': (5, 5, 5, 120),
     'w': 1.0},
    {'name': 'LeftHand', 'children': 0,
     'upAxis': LVector3f(0, 0, 1),
     'angles': (5, 5, 5, 5),
     'w': 1.0},
    {'name': 'RightHand', 'children': 0,
     'upAxis': LVector3f(0, 0, 1),
     'angles': (5, 5, 5, 5),
     'w': 1.0},
]
# regression test
# targetScale = 0.5
# jointBaseParent = 'Hips'
# jointHierarchyArray = [
#     {'name':'LeftUpLeg', 'children': 1,
#      'upAxis':LVector3f(0, -1, 0),
#      'baseForwardAxis': LVector3f(0, 0, -1)},
#     {'name':'LeftUpLegRoll', 'children': 1,
#      'upAxis':LVector3f(0, -1, 0),
#      'angles':(45, 120, 30, 0),#(35, 15, 35, 15),
#      'w':1.0}, #
#     {'name':'LeftLeg', 'children': 1,
#      'upAxis':LVector3f(0, -1, 0),
#      'endEffector':True,
#      'angles':(89, 15, 40, 15),
#      'w':1.0}, #
#     {'name':'LeftLegRoll', 'children': 1,
#      'upAxis':LVector3f(0, -1, 0),
#      'angles':(5, 5, 150, 5),
#      'w':1.0}, #
#     {'name':'LeftFoot', 'children': 1,
#      'upAxis':LVector3f(0, -1, 0),
#      'angles':(5, 5, 5, 5),
#      'w':1.0}, #
#     {'name':'LeftToeBase', 'children': 0,
#      'upAxis':LVector3f(0, -1, 0),
#      'angles':(95, 20, 0, 20),
#      'w':1.0}, #
# ]


def makeCCDikStep(render, jHierarchyTree, targets, targetTransPrev, baseJoint,
                  baseJointTransPrev, task):
    '''Performs, or not, an iteration of the CCD algorithm's
    '''

    # checks whether the base joint has undergone a transform get the new base
    # joint transform (w.r.t. render)
    baseJointTransNew = baseJoint.get_transform(render)
    # compute the base joint global 'difference' transformation between the
    # previous and the new one: that is, the transformation to be applied to
    # pass from the previous state to the new one
    baseJointDeltaTrans = baseJointTransNew.compose(
        baseJointTransPrev[0].get_inverse())
    # use heuristics to evaluate the 'magnitude' of the transformation: broken
    # down into the quantities of translation and rotation
    baseJointDeltaPosLen = baseJointDeltaTrans.get_pos().length_squared()
    baseJointDeltaRotLen = baseJointDeltaTrans.get_hpr().length_squared()
    # checks whether the movement of the base joint exceeds the thresholds to
    # consider it a true change of state
    if (baseJointDeltaPosLen > 0.0001) or (baseJointDeltaRotLen > 0.0001):
        # apply the transformation to the whole joint hierarchy
        ccdik.applyBaseJointTransform(baseJointDeltaTrans)
        # update the previous base joint transform
        baseJointTransPrev[0] = baseJointTransNew
        baseJointMoved = True
    else:
        baseJointMoved = False
    # compute the 'desired (global) positions and orientations' for the end
    # effectors, each of which 'follows' its reference target; also in this
    # case we use heuristics to calculate the 'magnitudes' of the translations
    # and rotations of the targets
    targetTransNew = {}
    Pd = {}
    Od = {}
    targetsDeltaPos = 0.0
    targetsDeltaRot = 0.0
    for name, target in targets.items():
        # get the new target transform (w.r.t. render)
        targetTransNew[name] = targets[name].get_transform(render)
        # set the desired (global) position for this target
        Pd[name] = targetTransNew[name].get_pos()
        # compute the contribution of this target for translation
        # heuristics
        targetsDeltaPos += (targetTransPrev[name].get_pos() -
                            Pd[name]).length_squared()
        # set the desired (global) orientation for this target
        Od[name] = (render.get_relative_vector(target, LVector3f.right()), render.get_relative_vector(
            target, LVector3f.forward()), render.get_relative_vector(target, LVector3f.up()))
        # compute the contribution of this target for orientation  heuristics
        targetsDeltaRot += (targetTransPrev[name].get_hpr() -
                            targetTransNew[name].get_hpr()).length_squared()
    # check if the previous movements, that of the base joint and those of
    # the targets, have exceeded the thresholds, and in this case the IK
    # should be applied
    if baseJointMoved or (targetsDeltaPos > 0.0001) or (targetsDeltaRot > 0.0001):
        # performs one iteration of the CCD algorithm's
        ccdik.iterateIK(Pd, Od, tolerance=0.01,
                        relError=0.01, maxIterations=100)
        # update the transform of each control node in local space
        CCDik.updateJointNodesLocalTransforms(render, jHierarchyTree)
        # update the previous base joint transform
        targetTransPrev.clear()
        targetTransPrev.update(targetTransNew)
        # debug drawing and info2
        CCDik._debugDraw(ccdik, app, color=LVecBase4f(0.0, 0.5, 1.0, 1.0))
        print('iterationCycleCount:' + str(ccdik.iterationCycleCount))
    return task.cont


def createTarget(model, pos, scale=1.0):
    '''Helper
    '''

    global app, PICKABLETAG
    target = app.loader.load_model(model)
    target.set_tag(PICKABLETAG, '')
    target.set_scale(scale)
    target.reparent_to(app.render)
    target.set_pos(pos)
    return target


def getBaseJoint(jHierarchyTree):
    '''Helper
    '''

    jBaseParams = jHierarchyTree.root().element()
    transBase = jBaseParams['jControlNodes'].get_transform()
    baseJoint = jBaseParams['jBaseParent'].attach_new_node('baseJoint')
    baseJoint.set_transform(transBase)
    return baseJoint


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''
    This script will test the IK Cyclic-Coordinate Descent algorithm
      
    Commands:
        - 'a': hold the key to pick-and-drag the targets with the mouse.
    '''))
    # set up arguments
    parser.add_argument('-m', '--model', type=str, default='john',
                        choices=['john', 'ralph'], help='the actor\'s model')
    parser.add_argument('-a', '--anim', type=str, default='walk',
                        choices=['walk', 'run'], help='the actor\'s animation')
    parser.add_argument('-d', '--data-dir', type=str,
                        action='append', help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    actorModel = args.model
    actorAnim = args.anim
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    #
    app = ShowBase()

    # get an actor
    actor = loadActor(actorModel)
    actor.reparent_to(app.render)

    # create the picker
    PICKABLETAG = 'pickable'
    PICKKEYON = 'a'
    PICKKEYOFF = 'a-up'
    picker = Picker(app, app.render, app.cam, app.mouseWatcher, PICKKEYON, PICKKEYOFF,
                    BitMask32.all_on(), PICKABLETAG)
    # make actor pickable
    actor.set_tag(PICKABLETAG, '')

    # set up the CCDik's hierarchy
    ccdik = CCDik(actor, jointHierarchyArray, alpha=10.0)
    CCDik.initializeJointNodes(
        actor, ccdik.jHierarchyTree, jointBaseParent=jointBaseParent)
    CCDik.updateJointNodesLocalTransforms(app.render, ccdik.jHierarchyTree)

    # right/left targets
    targets = {}
    targetTransPrev = {}
    for pJEE in ccdik.jEndEffectors:
        jeeParam = pJEE.element()
        name = jeeParam['name']
        pos = jeeParam['jGlobalPositions']
        targets[name] = createTarget('smiley', pos, targetScale)
        targetTransPrev[name] = targets[name].get_transform(app.render)

    # track right/left base joint transform
    baseJoint = getBaseJoint(ccdik.jHierarchyTree)
    baseJointTransPrev = [baseJoint.get_transform(app.render)]

    # IK update task
    app.task_mgr.add(makeCCDikStep, 'makeCCDikStep', appendTask=True, extraArgs=[
                     app.render, ccdik.jHierarchyTree, targets, targetTransPrev, baseJoint, baseJointTransPrev])

    # first debug draw
    CCDik._debugDraw(ccdik, app, color=LVecBase4f(1.0, 0.5, 0.0, 1.0))

    # setup camera
    # get 'tight' dimensions of actor
    _, xC, yC, zC, xL, yL, zL = getDimensions(actor)
    #
    app.trackball.node().set_origin(LVector3f(xL, yL, zL))
    app.trackball.node().set_pos(-xC + xL, -yC + yL, -zC + zL)
    app.trackball.node().set_hpr(0, 0, 0)
    # run
    app.run()
