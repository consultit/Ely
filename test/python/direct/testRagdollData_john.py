from panda3d.core import LVector3f, LVecBase3f, CollideMask, LPoint3f
from ely.libtools import Utilities 
from ely.physics import GamePhysicsManager, BT3Constraint

class Data:
    pass
data = Data()

## model data
data.modelName = 'john'
data.models='john.egg'
data.anims={'run':'john-run.egg', 'walk':'john-walk.egg'}
data.animData = {'startAnimPose':('walk', 0)}

## character controller data
data.ctrlData={'initState':'I', 'linAccel':150.0, 'linSpeed':125.0, 'linSpeedQ':275.0,
          'angAccel':0.8, 'angSpeed':1.6, 'angSpeedQ':3.2, 'jumpSpeed':10.0, 'jumpspeedQ':20.0}

# return the real actor to use with the character controller
# with corrections of its location applied if needed
def getRealActor(actor, referencNP):
    '''Returns (realActor, actorDims, actorDeltaCenter, actorRadius)'''
    actorDims = LVecBase3f()
    actorDeltaCenter = LVector3f()
    actorRadius = Utilities(referencNP, CollideMask.all_on()).get_bounding_dimensions(
        actor, actorDims, actorDeltaCenter)
    realDeltaPos = LPoint3f(0.0, 0.0, -actorDims.z / 2.0 - 0.5)
    if realDeltaPos:
        realActor = actor._createPlaceholderNP('realActor', actorDims)
        actor.reparent_to(realActor)
        actor.set_pos(realDeltaPos)
        realActor.hide()
        actor.show_through()
    else:
        realActor = actor
    return (realActor, actorDims, actorDeltaCenter, actorRadius)

data.getRealActor = getRealActor

## animations' transitions
def getAnimToFrameWR(animFromFrame):
    frameTo = (animFromFrame % 24.0) * 17.0 / 24.0 + 3.0
    if frameTo >= 17.0:
        frameTo -= 17.0
    return frameTo

def getAnimToFrameRW(animFromFrame):
    frameTo = (animFromFrame % 17.0) * 24.0 / 17.0 + 18.0
    if frameTo >= 24.0:
        frameTo -= 24.0
    return frameTo

data.deltaSpeedInv = 1.0 / (data.ctrlData['linSpeedQ'] - data.ctrlData['linSpeed'])
def lerpValueWR(actorCtrl, deltaSpeedInv, ctrlData):
    return (abs(actorCtrl.linear_speed.y) - ctrlData['linSpeed']) * deltaSpeedInv

def lerpValueRW(actorCtrl, deltaSpeedInv, ctrlData):
    return (ctrlData['linSpeedQ'] - abs(actorCtrl.linear_speed.y)) * deltaSpeedInv

data.linDuration = ((data.ctrlData['linSpeedQ'] - data.ctrlData['linSpeed']) / 
                                                                        data.ctrlData['linAccel'])
data.animTransData = {
    'walk-run':{'type':'param', 'animFrom':'walk','animTo':'run', 'duration':data.linDuration, 
                 'animToFrame':getAnimToFrameWR, 'playRate':1.0,
                 'playRateRatios':(1.411764705882353, 0.7083333333333334),
                 'usePose':True,'loop':True,'doneEvent':'TransDoneWR', 'lerpValue':lerpValueWR},
     'run-walk':{'type':'param', 'animFrom':'run','animTo':'walk','duration':data.linDuration,
                 'animToFrame':getAnimToFrameRW, 'playRate':1.0,
                 'playRateRatios':(0.7083333333333334, 1.411764705882353),
                 'usePose':True,'loop':True,'doneEvent':'TransDoneRW', 'lerpValue':lerpValueRW}
     }

## ragdoll related data
data.SPHERE = GamePhysicsManager.SPHERE
data.CAPSULE = GamePhysicsManager.CAPSULE
data.BOX = GamePhysicsManager.BOX
data.Xup = GamePhysicsManager.X_up
data.Yup = GamePhysicsManager.Y_up
data.CONETWIST = BT3Constraint.CONETWIST
data.POINT2POINT = BT3Constraint.POINT2POINT
data.DOF6 = BT3Constraint.DOF6
data.STOP_ERP = BT3Constraint.CONSTRAINT_STOP_ERP
data.STOP_CFM = BT3Constraint.CONSTRAINT_STOP_CFM
# john ActorRagDoll
data.jointHierarchyArray = [
    # level 0
    {'joint':'Hips', 'children': 3,
     'bone': {'type': data.SPHERE, 'mass': 10.0, 'dims':[0.5]},
     'constraint':None,
     },
    # level 1
    {'joint':'RightUpLegRoll', 'children': 1,
     'jointP': 'RightUpLeg',
     'bone': {'type': data.SPHERE, 'mass': 1.0, 'dims':[1.0]},
     'constraint':{'type': data.DOF6},
     },
    {'joint':'LeftUpLegRoll', 'children': 1,
     'jointP': 'LeftUpLeg',
     'bone': {'type': data.SPHERE, 'mass': 1.0, 'dims':[1.0]}, 
     'constraint':{'type': data.DOF6},
     },
    {'joint':'Spine', 'children': 1,
     'bone': {'type': data.BOX, 'mass': 1.0, 'dims':[2.0, 1.0, 1.0]},
     'constraint':{'type': data.CONETWIST, 'deltaPivot': LVector3f(0.0, -1.0, 0.0)},
     },
    # level 2
    {'joint':'RightLeg', 'children': 1,
     'bone': {'type': data.CAPSULE, 'upAxis': data.Yup, 'mass': 1.0, 'dims':[1.0, 1.5, 1.0]},
     'constraint':{'type': data.CONETWIST},
     },
    {'joint':'LeftLeg', 'children': 1,
     'bone': {'type': data.CAPSULE, 'upAxis': data.Yup, 'mass': 1.0, 'dims':[1.0, 1.5, 1.0]},
     'constraint':{'type': data.CONETWIST},
     },
    {'joint':'Spine3', 'children': 3,
     'jointP': 'Spine2',
     'bone': {'type': data.BOX, 'upAxis': data.Yup, 'mass': 1.0, 'dims':[2.0, 1.0, 1.0]},
     'constraint':{'type': data.CONETWIST, 'deltaPivot': LVector3f(0.0, -1.0, 0.0)},
     },
    # level 3
    {'joint':'RightLegRoll', 'children': 1,
     'bone': {'type': data.CAPSULE, 'upAxis': data.Yup, 'mass': 1.0, 'dims':[0.8, 1.5, 0.8]},
     'constraint':{'type': data.DOF6},
     },
    {'joint':'LeftLegRoll', 'children': 1,
     'bone': {'type': data.CAPSULE, 'upAxis': data.Yup, 'mass': 1.0, 'dims':[0.8, 1.5, 0.8]},
     'constraint':{'type': data.DOF6},
     },
    {'joint':'RightArmRoll', 'children': 1,
     'jointP': 'RightArm',
     'bone': {'type': data.SPHERE, 'mass': 1.0, 'dims':[1.0]},
     'constraint':{'type': data.DOF6},
     },
    {'joint':'LeftArmRoll', 'children': 1,
     'jointP': 'LeftArm',
     'bone': {'type': data.SPHERE, 'mass': 1.0, 'dims':[1.0]},
     'constraint':{'type': data.DOF6},
     },
    {'joint':'Neck', 'children': 1,
     'bone': {'type': data.SPHERE, 'mass': 1.0, 'dims':[0.2]},
     'constraint':{'type': data.CONETWIST},
     },
    # level 4
    {'joint':'RightFoot', 'children': 0,
     'bone': {'type': data.BOX, 'mass': 1.0, 'dims':[0.7]},
     'constraint':{'type': data.DOF6},
     },
    {'joint':'LeftFoot', 'children': 0,
     'bone': {'type': data.BOX, 'mass': 1.0, 'dims':[0.7]},
     'constraint':{'type': data.DOF6},
     },
    {'joint':'RightForeArm', 'children': 1,
     'bone': {'type': data.CAPSULE, 'upAxis': data.Yup, 'mass': 1.0, 'dims':[0.7, 1.05, 0.7]},
     'constraint':{'type': data.CONETWIST},
     },
    {'joint':'LeftForeArm', 'children': 1,
     'bone': {'type': data.CAPSULE, 'upAxis': data.Yup, 'mass': 1.0, 'dims':[0.7, 1.05, 0.7]},
     'constraint':{'type': data.CONETWIST},
     },
    {'joint':'Head', 'children': 0,
     'bone': {'type': data.SPHERE, 'mass': 1.0, 'dims':[1.75]},
     'constraint':{'type': data.DOF6},
     },
    # level 5
    {'joint':'RightHand', 'children': 0,
     'jointP': 'RightForeArmRoll',
     'bone': {'type': data.BOX, 'mass': 1.0, 'dims':[0.5]},
     'constraint':{'type': data.DOF6},
     },
    {'joint':'LeftHand', 'children': 0,
     'jointP': 'LeftForeArmRoll',
     'bone': {'type': data.BOX, 'mass': 1.0, 'dims':[0.5]},
     'constraint':{'type': data.DOF6},
     },
]

## CCDik related data
# right CCDik object
data.jointBaseParentR = 'Hips'
data.jointHierarchyArrayR = [
    {'name':'RightUpLeg', 'children': 1,
     'upAxis':LVector3f(0, -1, 0), 
     'baseForwardAxis': LVector3f(0, 0, -1)},
    {'name':'RightUpLegRoll', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(45, 120, 30, 0),
     'w':1.0}, #
    {'name':'RightLeg', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(89, 15, 40, 15),
     'w':1.0}, #
    {'name':'RightLegRoll', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(5, 5, 150, 5),
     'w':1.0}, #
    {'name':'RightFoot', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
    'endEffector':True,
     'angles':(5, 5, 5, 5),
     'w':1.0}, #
    {'name':'RightToeBase', 'children': 0,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(95, 20, 0, 20), 
     'w':1.0}, #
]
data.minZR = {'RightFoot':0.67, 'RightToeBase':0.65}
# left CCDik object
data.jointBaseParentL = 'Hips'
data.jointHierarchyArrayL = [
    {'name':'LeftUpLeg', 'children': 1,
     'upAxis':LVector3f(0, -1, 0), 
     'baseForwardAxis': LVector3f(0, 0, -1)},
    {'name':'LeftUpLegRoll', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(45, 120, 30, 0),
     'w':1.0}, #
    {'name':'LeftLeg', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(89, 15, 40, 15),
     'w':1.0}, #
    {'name':'LeftLegRoll', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(5, 5, 150, 5),
     'w':1.0}, #
    {'name':'LeftFoot', 'children': 1,
     'upAxis':LVector3f(0, -1, 0),
    'endEffector':True,
     'angles':(5, 5, 5, 5),
     'w':1.0}, #
    {'name':'LeftToeBase', 'children': 0,
     'upAxis':LVector3f(0, -1, 0),
     'angles':(95, 20, 0, 20), 
     'w':1.0}, #
]
data.minZL = {'LeftFoot':0.62, 'LeftToeBase':0.6}

