'''
Created on Nov 30, 2017

CCDik method tests.

@author: consultit
'''

from ely.direct.ccdik import CCDik
from panda3d.core import load_prc_file_data, LVector3f, BitMask32, LVecBase4f, \
    LPoint3f, LMatrix4f, TransformState, CardMaker, LVecBase3f, TextureStage
from direct.showbase.ShowBase import ShowBase
from common import loadActor
from picker import Picker
from driver import Driver
from utilities import Utilities as utils
import argparse
import textwrap
import random
import os

random.seed()
randNum = random.random

scriptPath = os.path.dirname(os.path.abspath(__file__))
prefD = os.path.join(scriptPath, '..', 'assets')
dataDirs = [
    os.path.join(prefD, 'misc', 'test'),
    os.path.join(prefD, 'models', 'test'),
    os.path.join(prefD, 'textures', 'test'),
    os.path.join(prefD, 'sounds', 'test'),
    os.path.join(prefD, 'scripts', 'test'),
    os.path.join(prefD, 'shaders', 'test'), ]

for dataDir in dataDirs:
    load_prc_file_data('', 'model-path ' + dataDir)
load_prc_file_data('', 'win-size 1024 768')
load_prc_file_data('', 'show-frame-rate-meter #t')
load_prc_file_data('', 'sync-video #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')


def makeCCDikStepAnim(render, ccdik, animCtrl, jointTrans, jointTransEE, jControlNodes,
                      baseJointParentTransPrev, Pd, Od, IKNEEDED, minZ, utilities,
                      actor, useOrient, task):
    '''Performs, or not, an iteration of the CCD algorithm's
    '''

    # save previous global transform of the base joint's parent for later use
    baseJointParentTransI = baseJointParentTransPrev[0]
    # get the new global transform of the base joint's parent
    baseJointParent = ccdik.jHierarchyTree.root().element()['jBaseParent']
    globalTransF = baseJointParent.get_transform(render)
    globalTransFAnim = globalTransF
    baseJointParentTransPrev[0] = globalTransF
    # we need to compute the joints' global 'difference' transformations and
    # store them in tree's array based representation
    jointsDeltaTrans = []
    # traverse the hierarchy in breadthfirst order
    for p, pT in zip(ccdik.jHierarchyTree.breadthfirst(), jointTrans.breadthfirst()):
        jParamsT = pT.element()
        jParams = p.element()
        # Before making any adjustments via the IK, it is necessary to check
        # the pose that the animation has reached at this time and then save
        # the local transformations related to the joints of the hierarchy.
        # jointTrans.preorder()):
        animChannel = jParams['joints'].get_bound(0)
        mat = LMatrix4f()
        animChannel.get_value(animCtrl.get_frame(), mat)
        jParamsT['local'] = TransformState.make_mat(mat)
        jControlNodesTrans = jParams['jControlNodes'].get_transform()
        # the joint's initial global transform
        if jointTrans.is_root(pT):
            jParamsT['globalI'] = baseJointParentTransI.compose(
                jControlNodesTrans)
            jParamsT['globalFAnim'] = globalTransFAnim.compose(
                jParamsT['local'])
            jParamsT['globalF'] = globalTransF
        else:
            jParamsTP = jointTrans.parent(pT).element()
            jParamsT['globalI'] = jParamsTP['globalI'].compose(
                jControlNodesTrans)
            jParamsT['globalFAnim'] = jParamsTP['globalFAnim'].compose(
                jParamsT['local'])
            jParamsT['globalF'] = jParamsTP['globalF']
        # the joint's final global transform
        if IKNEEDED:
            # IK is being applied: use the current transform
            jParamsT['globalF'] = jParamsT['globalF'].compose(
                jControlNodesTrans)
        else:
            # IK is not being applied: use transform from FK (animation)
            jParamsT['globalF'] = jParamsT['globalFAnim']
            jParams['jControlNodes'].set_transform(jParamsT['local'])
        # compute the joint's delta transform: from the initial to the
        # final ones
        deltaTrans = jParamsT['globalF'].compose(
            jParamsT['globalI'].get_inverse())
        # store the joint delta global transform: the breadthfirst traversal
        # automagically creates the 'array based representation'
        jointsDeltaTrans.append(deltaTrans)
    # apply the the joints' global 'difference' transformations to the joint
    # hierarchy. Note: remember that the nodes in jointsDeltaTrans are ordered
    # as the breadthfirst traversal, i.e. in 'array based representation'
    ccdik.applyJointsTransforms(jointsDeltaTrans)
    # at this point the transformation established by the animation is applied
    # to the whole hierarchy; now it is necessary to check if the position and
    # orientation of each end effector require correction via IK as the foot
    # must be constrained on the ground or this is not necessary
    for pEE in jointTransEE:
        jParamsEE = pEE.element()
        name = jParamsEE['name']
        # get end effector position (stated by the animation)
        eePos = LPoint3f(jParamsEE['globalFAnim'].get_pos())
        # get the distance in height from the ground
        eeCollisionZ = utilities.get_collision_height(eePos, render)
        # check whether the position of the end effector can be considered 'in
        # contact' with the uneven ground or not
        if eePos.get_z() <= minZ[name] + eeCollisionZ[1]:
            # the end effector 'is on' or 'below' the ground: its position may
            # have to be IK corrected and remain fixed
            if not (name in IKNEEDED):
                # the end effector is not among those that in the previous step
                # needed IK correction, so add it to the IKNEEDED set
                IKNEEDED.add(name)
                # this is the first step in which we need IK correction, and as
                # we want it to remain fixed during the next steps (at least
                # until we no longer need correction) we save the position as
                # desired
                Pd[name] = eePos
                # print('Pd[' + name +']: ' + str(Pd[name]))
            # at each step we adjust the height of the desired position (which
            # was stored in this or in a previous step)
            Pd[name].set_z(eeCollisionZ[1] + minZ[name])
        else:
            # end effector can be considered is not 'in contact' with the
            # uneven ground so ...
            if name in IKNEEDED:
                IKNEEDED.remove(name)
            # ... follow the animation position (no IK needed)
            Pd[name] = eePos
    # IK correction is necessary if at least one of the end effectors needs it
    if IKNEEDED:
        if useOrient:
            # 1) option: the desired orientations of the end effectors is the
            # same as those established by the animation
            for pEE in jointTransEE:
                jointName = pEE.element()['name']
                jointControlNode = jControlNodes[jointName]
                eeDir = (render.get_relative_vector(jointControlNode, LVector3f.right()),
                         render.get_relative_vector(
                             jointControlNode, LVector3f.forward()),
                         render.get_relative_vector(jointControlNode, LVector3f.up()))
                Od[jointName] = eeDir
            # performs one iteration of the CCD algorithm's
            ccdik.iterateIK(Pd, Od, tolerance=0.01,
                            relError=0.01, maxIterations=100)
        else:
            # 2) option: ignore the desired orientations
            # performs one iteration of the CCD algorithm's
            ccdik.iterateIK(Pd, None, tolerance=0.01,
                            relError=0.01, maxIterations=100)
        # update joint node transforms
        CCDik.updateJointNodesLocalTransforms(render, ccdik.jHierarchyTree)
        print('iterationCycleCount:' + str(ccdik.iterationCycleCount))
    #
    CCDik._debugDraw(ccdik, app, color=LVecBase4f(0.0, 0.5, 1.0, 1.0))
    # print('makeCCDikStepAnim: ' + ccdik.joints[ccdik.jNum - 1].get_name())
    #
    return task.cont


def buildCCDikData(ccdik):
    '''Helper. Builds data structures needed by makeCCDikStepAnim() task.
    '''

    # we use a tree (jointTrans) structurally equivalent to that of the joint
    # hierarchy, to be used as a supporting data structure for storing various
    # related information on the transformations of the joints themselves, in
    # order to obtain the final pose of the actor; each jointTrans node has
    # the same name as the corresponding node in the hierarchy
    jointTrans = ccdik.jHierarchyTree._make_copy(
        copyElement=lambda e: {'name': e['name']})
    # we need a list of jointTrans' nodes each one corresponding to an end
    # effectors of the  hierarchy
    jointTransEE = []
    # we prepare the Pd and Od dictionaries needed by CCDik.iterateIK()
    Pd = {}
    Od = {}
    # IKNEEDED is the set of end effectors that need IK correction at any
    # given time
    IKNEEDED = set()
    for p, pT in zip(ccdik.jHierarchyTree.breadthfirst(), jointTrans.breadthfirst()):
        if p in ccdik.jEndEffectors:
            jointTransEE.append(pT)
            name = pT.element()['name']
            Pd[name] = None
            Od[name] = None
    # return data
    return (jointTrans, jointTransEE, Pd, Od, IKNEEDED)


def handleActorUpdate(task):
    '''handles actor on every update'''

    global actorDriver, animCtrl, playing, actorHeight, U, app, animRateFactor
    # get current forward velocity size
    currentVelSize = abs(actorDriver.get_current_speeds()[0].get_y())
    # handle vehicle's animation
    if actorDriver.is_forward_enabled() or actorDriver.is_backward_enabled():
        if actorDriver.is_backward_enabled():
            currentVelSize = -currentVelSize
        animCtrl.set_play_rate(currentVelSize * animRateFactor)
        if not playing[0]:
            animCtrl.loop(False)
            playing[0] = True
    else:
        # stop any animation
        if playing[0]:
            currFrame = animCtrl.get_full_fframe()
            endFrame = animCtrl.get_num_frames()
            animCtrl.play(currFrame, endFrame)
            playing[0] = False
        animCtrl.set_play_rate(currentVelSize * animRateFactor)
    # correct position on uneven ground
    pos = actor.get_pos()
    pOrig = pos + LPoint3f(0, 0, actorHeight)
    gotCollisionZ = U.get_collision_height(pOrig, app.render)
    if gotCollisionZ[0]:
        deltaHeight = pOrig.get_z() - gotCollisionZ[1]
        actor.set_z(pos.get_z() + actorHeight - deltaHeight)
    # print('handleActorUpdate')
    #
    return task.cont


def moveActor(data):
    '''actor's movement callback'''

    global actorDriver, forwardMove, leftMove, backwardMove, rightMove
    if not actorDriver:
        return

    action = data
    if action > 0:
        # start movement
        enable = True
    else:
        action = -action
        # stop movement
        enable = False
    #
    if action == forwardMove:
        actorDriver.enable_forward(enable)
    elif action == leftMove:
        actorDriver.enable_head_left(enable)
    elif action == backwardMove:
        actorDriver.enable_backward(enable)
    elif action == rightMove:
        actorDriver.enable_head_right(enable)


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''
    This script will test the walking cycle using IK Cyclic-Coordinate Descent algorithm 
      
    Commands:
        - 'arrow keys': use the keys to move the actor.
    '''))
    # set up arguments
    dataPath = os.path.dirname(os.path.abspath(__file__))
    parser.add_argument('--actor-data', type=str,
                        default=os.path.join(
                            dataPath, 'testRagdollData_john.py'),
                        help='the actor model data path')
    parser.add_argument('-a', '--anim', type=str, default='walk',
                        choices=['walk', 'run'], help='the actor\'s animation')
    parser.add_argument('-u', '--use-orientation', action='store_true',
                        help='when specified IK will use desired orientations, default is not')
    parser.add_argument('-d', '--data-dir', type=str,
                        action='append', help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    actorData = args.actor_data
    actorAnim = args.anim
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    #
    app = ShowBase()

    # ACTOR SETUP
    # read actor data
    exec(open(actorData).read(), globals())
    global data
    useOrient = args.use_orientation

    # get an actor
    actor = loadActor(data.modelName)
    actor.reparent_to(app.render)

    # handle skeleton's animation, and bind it to the skeleton
    animCtrl = actor.get_anim_control(
        actorAnim, 'modelRoot', allowAsyncBind=False)
    # set start frame
    animCtrl.pose(0)

    # create the picker
    PICKABLETAG = 'pickable'
    PICKKEYON = 'a'
    PICKKEYOFF = 'a-up'
    picker = Picker(app, app.render, app.cam, app.mouseWatcher, PICKKEYON, PICKKEYOFF,
                    BitMask32.all_on(), PICKABLETAG)

    # set actor pickable
    actor.set_tag(PICKABLETAG, '')

    # synchronize animation and movements
    actorDriver = None
    forwardMove = 1
    forwardMoveStop = -1
    leftMove = 2
    leftMoveStop = -2
    backwardMove = 3
    backwardMoveStop = -3
    rightMove = 4
    rightMoveStop = -4

    # actor will be driven by arrows keys
    app.accept('arrow_up', moveActor, [forwardMove])
    app.accept('arrow_up-up', moveActor, [forwardMoveStop])
    app.accept('arrow_left', moveActor, [leftMove])
    app.accept('arrow_left-up', moveActor, [leftMoveStop])
    app.accept('arrow_down', moveActor, [backwardMove])
    app.accept('arrow_down-up', moveActor, [backwardMoveStop])
    app.accept('arrow_right', moveActor, [rightMove])
    app.accept('arrow_right-up', moveActor, [rightMoveStop])

    # driver configuration
    actorDriver = Driver(app, actor, 10)
    actorDriver.set_max_angular_speed(200.0)
    actorDriver.set_angular_accel(100.0)
    actorDriver.set_linear_accel(LVecBase3f(0))
    actorDriver.set_linear_friction(100)
    actorDriver.enable()
    #
    linSpeed = data.ctrlData['linSpeed'] / 10.0
    actorDriver.set_max_linear_speed(LVector3f(linSpeed))
    animRateFactor = 1.0 / linSpeed  # max_linear_speed * animRateFactor = 1.0
    playing = [False]

    # scene
    cm = CardMaker('ground')
    cm.setFrame(1000, -1000, 1000, -1000)
    ground = app.render.attach_new_node(cm.generate())
    ground.set_pos(0, 0, 0)
    ground.set_p(-90)
    ground.set_texture(app.loader.load_texture('grass_ground2.jpg'))
    ground.set_tex_scale(TextureStage.default, 10.0, 10.0)

    # utilities
    mask = BitMask32(0x10)
    U = utils(app.render, mask)
    ground.set_collide_mask(mask)
    modelDims = LVecBase3f()
    modelDeltaCenter = LVecBase3f()
    U.get_bounding_dimensions(actor, modelDims, modelDeltaCenter)
    actorHeight = modelDims.get_z()

    # set actor
    actor.set_pos(0, 0, -actorHeight / 2.0)

    # update actor's movement parameters
    app.task_mgr.add(handleActorUpdate, 'handleActorUpdate', sort=5)

    # IK SETUP
    # setup the right CCDik's hierarchy
    ccdikR = CCDik(actor, data.jointHierarchyArrayR, alpha=10.0)
    jointBaseParentR, jControlNodesR = CCDik.initializeJointNodes(
        actor, ccdikR.jHierarchyTree, jointBaseParent=data.jointBaseParentR)
    CCDik.updateJointNodesLocalTransforms(
        app.render, ccdikR.jHierarchyTree)

    # setup the left CCDik's hierarchy
    ccdikL = CCDik(actor, data.jointHierarchyArrayL, alpha=10.0)
    jointBaseParentL, jControlNodesL = CCDik.initializeJointNodes(
        actor, ccdikL.jHierarchyTree, jointBaseParent=data.jointBaseParentL)
    CCDik.updateJointNodesLocalTransforms(app.render, ccdikL.jHierarchyTree)

    # track base's parent global transform
    jBaseParentTransPrevR = [ccdikR.jHierarchyTree.root().element()['jBaseParent'].get_transform(
        app.render)]
    jBaseParentTransPrevL = [ccdikL.jHierarchyTree.root().element()['jBaseParent'].get_transform(
        app.render)]

    # minZ
    # 0.396054387093 RightToeBase
    # 1.11598670483 RightFoot
    # 0.390445828438 LeftToeBase
    # 1.03538942337 LeftFoot

    # build makeCCDikStepAnim()'s right data
    jointTransR, jointTransEER, PdR, OdR, IKNEEDEDR = buildCCDikData(ccdikR)
    # right update task
    app.task_mgr.add(makeCCDikStepAnim, 'makeCCDikStepAnim', appendTask=True, sort=6, extraArgs=[
                     app.render, ccdikR, animCtrl, jointTransR, jointTransEER, jControlNodesR,
                     jBaseParentTransPrevR, PdR, OdR, IKNEEDEDR, data.minZR, U, actor, useOrient])

    # build makeCCDikStepAnim()'s left data
    jointTransL, jointTransEEL, PdL, OdL, IKNEEDEDL = buildCCDikData(ccdikL)
    # left update task
    app.task_mgr.add(makeCCDikStepAnim, 'makeCCDikStepAnim', appendTask=True, sort=6, extraArgs=[
                     app.render, ccdikL, animCtrl, jointTransL, jointTransEEL, jControlNodesL,
                     jBaseParentTransPrevL, PdL, OdL, IKNEEDEDL, data.minZL, U, actor, useOrient])

    # first right/left debug draw
    CCDik._debugDraw(ccdikR, app, color=LVecBase4f(1.0, 0.5, 0.0, 1.0))
    CCDik._debugDraw(ccdikL, app, color=LVecBase4f(1.0, 0.5, 0.0, 1.0))

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 55.0, -7.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
