'''
Created on Sep 20, 2016

@author: consultit
'''

from ely.control import GameControlManager
from panda3d.core import load_prc_file_data, WindowProperties, BitMask32, \
    LVector3f, NodePath, AnimControlCollection, auto_bind, PartGroup, \
    ClockObject, TextNode, LPoint3f, LVecBase3f, LVector4f
from direct.showbase.ShowBase import ShowBase
import argparse
import textwrap
import sys
import os

# # global data declaration
scriptPath = os.path.dirname(os.path.abspath(__file__))
prefD = os.path.join(scriptPath, '..', 'assets')
dataDirs = [
    os.path.join(prefD, 'misc', 'test'),
    os.path.join(prefD, 'models', 'test'),
    os.path.join(prefD, 'textures', 'test'),
    os.path.join(prefD, 'sounds', 'test'),
    os.path.join(prefD, 'scripts', 'test'),
    os.path.join(prefD, 'shaders', 'test'), ]
app = None
mask = BitMask32(0x10)
updateTask = None
# models and animations
modelFile = ['eve.egg', 'ralph.egg', 'sparrow.egg', 'ball.egg', 'red_car.egg']
modelAnimFiles = [['eve-walk.egg', 'eve-run.egg'],
                  ['ralph-walk.egg', 'ralph-run.egg'],
                  ['sparrow-flying.egg', 'sparrow-flying2.egg'],
                  ['', ''],
                  ['red_car-anim.egg', 'red_car-anim2.egg']]
animRateFactor = [0.6, 0.175]
# bame file
bamFileName = 'control.boo'

# # specific data/functions declarations/definitions
sceneNP = None
globalClock = None
# player specifics
playerAnimCtls = []
playerNP = None
playerDriver = None
playerHeightRayCast = LVector3f()
forwardMove = 1
forwardMoveStop = -1
leftMove = 2
leftMoveStop = -2
backwardMove = 3
backwardMoveStop = -3
rightMove = 4
rightMoveStop = -4
# pursuer specifics
pursuerAnimCtls = []
pursuerNP = None
pursuerChaser = None

#


def printCreationParameters():
    '''print creation parameters'''

    controlMgr = GameControlManager.get_global_ptr()
    #
    valueList = controlMgr.get_parameter_name_list(GameControlManager.DRIVER)
    print('\n' + 'P3Driver creation parameters:')
    for name in valueList:
        print('\t' + name + ' = ' +
              controlMgr.get_parameter_value(GameControlManager.DRIVER, name))
    #
    valueList = controlMgr.get_parameter_name_list(GameControlManager.CHASER)
    print('\n' + 'P3Chaser creation parameters:')
    for name in valueList:
        print('\t' + name + ' = ' +
              controlMgr.get_parameter_value(GameControlManager.CHASER, name))


def setParametersBeforeCreation():
    '''set parameters as strings before drivers/chasers creation'''

    controlMgr = GameControlManager.get_global_ptr()
    # set driver's parameters
    controlMgr.set_parameter_value(GameControlManager.DRIVER, 'max_angular_speed',
                                   '100.0')
    controlMgr.set_parameter_value(GameControlManager.DRIVER, 'angular_accel',
                                   '50.0')
    controlMgr.set_parameter_value(GameControlManager.DRIVER, 'max_linear_speed',
                                   '8.0')
    controlMgr.set_parameter_value(GameControlManager.DRIVER, 'linear_accel',
                                   '1.0')
    controlMgr.set_parameter_value(GameControlManager.DRIVER, 'linear_friction',
                                   '0.5')
    controlMgr.set_parameter_value(GameControlManager.DRIVER, 'angular_friction',
                                   '5.0')
    # set chaser's parameters
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'fixed_relative_position',
                                   'false')
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'max_distance',
                                   '25.0')
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'min_distance',
                                   '18.0')
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'max_height',
                                   '18.0')
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'min_height',
                                   '15.0')
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'friction',
                                   '5.0')
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'fixed_look_at',
                                   'true')
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'mouse_head',
                                   'true')
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'mouse_pitch',
                                   'true')
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'look_at_distance',
                                   '5.0')
    controlMgr.set_parameter_value(GameControlManager.CHASER, 'look_at_height',
                                   '12.5')
    #
    printCreationParameters()


def startFramework(msg):
    '''start base framework'''

    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''
    This script will perform ''' + msg + '''.
    '''))
    # set up arguments
    parser.add_argument('bamFile', type=str, nargs='?',
                        help='the bam file storing the saved state')
    parser.add_argument('-r', '--render-pipeline', type=str,
                        help='the RenderPipeline dir')
    parser.add_argument('-f', '--heightfield', type=str,
                        help='the heightfield map')
    parser.add_argument('-d', '--data-dir', type=str,
                        action='append', help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    bamFile = args.bamFile
    renderPipeline = args.render_pipeline
    if args.heightfield:
        heightfield = args.heightfield
    else:
        heightfield = os.path.join(
            prefD, 'textures', 'test', 'heightfield.png')
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)

    # Setup your application
    # Load your application's configuration
    for dataDir in dataDirs:
        load_prc_file_data('', 'model-path ' + dataDir)
    # Load your application's configuration
    load_prc_file_data('', 'model-path ' + dataDir)
    load_prc_file_data('', 'win-size 1024 768')
    load_prc_file_data('', 'show-frame-rate-meter #t')
    load_prc_file_data('', 'sync-video #t')
#     load_prc_file_data('', 'want-directtools #t')
#     load_prc_file_data('', 'want-tk #t')

    app = ShowBase()

    if renderPipeline:
        # 1: RenderPipeline
        sys.path.insert(0, renderPipeline)
        # Import the main render pipeline class
        from rpcore import RenderPipeline
        # Construct and create the pipeline
        render_pipeline = RenderPipeline()
        render_pipeline.pre_showbase_init()
        render_pipeline.create(app)
        render_pipeline.daytime_mgr.time = '7:40'
        render_pipeline.set_effect(app.render, prefD + 'scripts/game/scene-effect.yaml', {},
                                   sort=250)
        app.win.set_clear_color(LVector4f(0, 0, 0, 1))

    props = WindowProperties()
    props.setTitle(msg)
    app.win.requestProperties(props)

    # common callbacks
    #
    return (app, bamFile, renderPipeline, heightfield)


def readFromBamFile(fileName):
    '''read scene from a file'''

    return GameControlManager.get_global_ptr().read_from_bam_file(fileName)


def writeToBamFileAndExit(fileName):
    '''write scene to a file (and exit)'''

    GameControlManager.get_global_ptr().write_to_bam_file(fileName)
    # # this is for testing explicit removal and destruction of all elements
    controlMgr = GameControlManager.get_global_ptr()
    # destroy drivers
    for driverTmp in controlMgr.get_drivers():
        # destroy driverTmp
        controlMgr.destroy_driver(NodePath.any_path(driverTmp))
    # destroy chasers
    for chaserTmp in controlMgr.get_chasers():
        # destroy chaserTmp
        controlMgr.destroy_chaser(NodePath.any_path(chaserTmp))
    #
    sys.exit(0)


def loadTerrainLowPoly(name, widthScale=128, heightScale=64.0,
                       texture='dry-grass.png'):
    '''load terrain low poly stuff'''

    global app
    terrainNP = app.loader.load_model('terrain-low-poly.egg')
    terrainNP.set_name(name)
    terrainNP.set_scale(widthScale, widthScale, heightScale)
    tex = app.loader.load_texture(texture)
    terrainNP.set_texture(tex)
    return terrainNP


def getModelAnims(name, scale, modelFileIdx, modelAnimCtls):
    '''get model and animations'''

    global app, modelFile, modelAnimFiles
    # get some models, with animations
    # get the model
    modelNP = app.loader.load_model(modelFile[modelFileIdx])
    # set the name
    modelNP.set_name(name)
    # set scale
    modelNP.set_scale(scale)
    # associate an anim with a given anim control
    tmpAnims = AnimControlCollection()
    modelAnimNP = [None, None]
    modelAnimCtls.append([None, None])
    if(len(modelAnimFiles[modelFileIdx][0]) != 0) and \
            (len(modelAnimFiles[modelFileIdx][1]) != 0):
        # first anim . modelAnimCtls[i][0]
        modelAnimNP[0] = app.loader.load_model(modelAnimFiles[modelFileIdx][0])
        modelAnimNP[0].reparent_to(modelNP)
        auto_bind(modelNP.node(), tmpAnims,
                  PartGroup.HMF_ok_part_extra |
                  PartGroup.HMF_ok_anim_extra |
                  PartGroup.HMF_ok_wrong_root_name)
        modelAnimCtls[-1][0] = tmpAnims.get_anim(0)
        tmpAnims.clear_anims()
        modelAnimNP[0].detach_node()
        # second anim . modelAnimCtls[i][1]
        modelAnimNP[1] = app.loader.load_model(modelAnimFiles[modelFileIdx][1])
        modelAnimNP[1].reparent_to(modelNP)
        auto_bind(modelNP.node(), tmpAnims,
                  PartGroup.HMF_ok_part_extra |
                  PartGroup.HMF_ok_anim_extra |
                  PartGroup.HMF_ok_wrong_root_name)
        modelAnimCtls[-1][1] = tmpAnims.get_anim(0)
        tmpAnims.clear_anims()
        modelAnimNP[1].detach_node()
        # reparent all node paths
        modelAnimNP[0].reparent_to(modelNP)
        modelAnimNP[1].reparent_to(modelNP)
    #
    return modelNP


def handlePlayerUpdate():
    '''handles player on every update'''

    global playerDriver, playerAnimCtls, playerNP, playerHeightRayCast
    # get current forward velocity size
    currentVelSize = abs(playerDriver.linear_speed.y)
    playerDriverNP = NodePath.any_path(playerDriver)
    # handle vehicle's animation
    for i in range(len(playerAnimCtls)):
        if currentVelSize > 0.0:
            if currentVelSize < 5.0:
                animOnIdx = 0
            else:
                animOnIdx = 1
            animOffIdx = (animOnIdx + 1) % 2
            # Off anim (0:walk, 1:run)
            if playerAnimCtls[i][animOffIdx].is_playing():
                playerAnimCtls[i][animOffIdx].stop()
            # On amin (0:walk, 1:run)
            playerAnimCtls[i][animOnIdx].set_play_rate(currentVelSize *
                                                       animRateFactor[animOnIdx])
            if not playerAnimCtls[i][animOnIdx].is_playing():
                playerAnimCtls[i][animOnIdx].loop(True)
        else:
            # stop any animation
            playerAnimCtls[i][0].stop()
            playerAnimCtls[i][1].stop()
    # make playerNP kinematic (ie stand on floor)
    if currentVelSize > 0.0:
        # get control manager
        controlMgr = GameControlManager.get_global_ptr()
        # correct player's Z: set the collision ray origin wrt collision root
        pOrig = controlMgr.utilities.get_collision_root().get_relative_point(
            controlMgr.get_reference_node_path(), playerDriverNP.get_pos()) + \
            playerHeightRayCast * 2.0
        # get the collision height wrt the reference node path
        gotCollisionZ = controlMgr.utilities.get_collision_height(pOrig,
                                                                  controlMgr.get_reference_node_path())
        if gotCollisionZ.get_first():
            # updatedPos.z needs correction
            playerDriverNP.set_z(gotCollisionZ.get_second())


def handlePursuerUpdate():
    '''handles pursuer on every update'''

    global pursuerChaser, pursuerAnimCtls, pursuerNP
    # get current forward velocity size
    currentVelSize = abs(pursuerChaser.chased_object.node().linear_speed.y)
    # handle vehicle's animation
    for i in range(len(pursuerAnimCtls)):
        if currentVelSize < 5.0:
            animOnIdx = 0
        else:
            animOnIdx = 1
        animOffIdx = (animOnIdx + 1) % 2
        # Off anim (0:walk, 1:run)
        if pursuerAnimCtls[i][animOffIdx].is_playing():
            pursuerAnimCtls[i][animOffIdx].stop()
        # On amin (0:walk, 1:run)
        pursuerAnimCtls[i][animOnIdx].set_play_rate(
            (currentVelSize + 1.0) * 0.5)
        if not pursuerAnimCtls[i][animOnIdx].is_playing():
            pursuerAnimCtls[i][animOnIdx].loop(True)


def updateControls(task):
    '''custom update task for controls'''

    global playerDriver, pursuerChaser
    # call update for controls
    dt = ClockObject.get_global_clock().get_dt()
    playerDriver.update(dt)
    pursuerChaser.update(dt)
    # handle player on update
    handlePlayerUpdate()
    # handle player on update
    handlePursuerUpdate()
    #
    return task.cont


def movePlayer(data):
    '''player's movement callback'''

    global playerDriver, forwardMove, leftMove, backwardMove, rightMove
    if not playerDriver:
        return

    action = data
    if action > 0:
        # start movement
        enable = True
    else:
        action = -action
        # stop movement
        enable = False
    #
    if action == forwardMove:
        playerDriver.set_move_forward(enable)
    elif action == leftMove:
        playerDriver.set_rotate_head_left(enable)
    elif action == backwardMove:
        playerDriver.set_move_backward(enable)
    elif action == rightMove:
        playerDriver.set_rotate_head_right(enable)


def driverCallback(driver):
    '''driver update callback function'''

    global globalClock
    linSpeed = driver.linear_speed
    angSpeeds = driver.angular_speeds
    print(driver, ' ' + str(globalClock.get_real_time()) + ' - ' +
          str(globalClock.get_dt()))
    print('current speeds: ', linSpeed, ' - ',
          angSpeeds[0], ',', angSpeeds[1])
    # handle player on update
    handlePlayerUpdate()


def chaserCallback(chaser):
    '''chaser update callback function'''

    distance = (chaser.get_chased_object().get_pos() -
                NodePath.any_path(chaser).get_pos()).length()
    print(chaser, ' ' + str(globalClock.get_real_time()) + ' - ' +
          str(globalClock.get_dt()))
    print('current distance: ', distance)
    # handle chaser on update
    handlePursuerUpdate()


if __name__ == '__main__':

    msg = 'P3Driver & P3Chaser test'
    app, bamFile, renderPipeline, heightfield = startFramework(msg)

    # # here is room for your own code
    # print some help to screen
    text = TextNode('Help')
    text.set_text(
        msg + '\n\n'
        '- press \'up\'/\'left\'/\'down\'/\'right\' arrows to move the player\n')
    textNodePath = app.aspect2d.attach_new_node(text)
    textNodePath.set_pos(-1.25, 0.0, 0.8)
    textNodePath.set_scale(0.035)
    if renderPipeline:
        textNodePath.set_color(0, 0, 0)

    # create a control manager set root and mask to manage 'kinematic' players
    controlMgr = GameControlManager(app.win, 10, app.render, mask)

    # print creation parameters: defult values
    print('\n' + 'Default creation parameters:')
    printCreationParameters()

    # load or restore all scene stuff: if passed an argument
    # try to read it from bam file
    if (not bamFile) or (not readFromBamFile(bamFile)):
        # no argument or no valid bamFile
        # reparent the reference node to render
        controlMgr.get_reference_node_path().reparent_to(app.render)

        # get a sceneNP, naming it with 'SceneNP' to ease restoring from bam
        # file
        sceneNP = loadTerrainLowPoly('SceneNP')
        # and reparent to the reference node
        sceneNP.reparent_to(controlMgr.get_reference_node_path())

        # set sceneNP's collide mask
        sceneNP.set_collide_mask(mask)

        # set driver's various creation parameters as string
        setParametersBeforeCreation()
        # get a player with anims
        playerNP = getModelAnims('PlayerNP', 1.2, 0, playerAnimCtls)
        # get a pursuer with anims
        pursuerNP = getModelAnims('PursuerNP', 0.01, 2, pursuerAnimCtls)
        pursuerNP.set_h(180)

        # create the driver (attached to the reference node)
        playerDriverNP = controlMgr.create_driver('PlayerDriver')
        # get a reference to the player driver
        playerDriver = playerDriverNP.node()
        # set the position
        playerDriverNP.set_pos(LPoint3f(4.1, -12.0, 1.5))
        # attach some geometry (a model) to control vehicle
        playerNP.reparent_to(playerDriverNP)

        # create the pursuer (attached to the reference node)
        pursuerChaserNP = controlMgr.create_chaser('PursuerChaser')
        # get a reference to the pursuer's chaser
        pursuerChaser = pursuerChaserNP.node()
        # set the chased object: playerDriverNP or playerNP
        pursuerChaser.set_chased_object(playerDriverNP)
        # attach some geometry (a model) to pursuer's chaser
        pursuerNP.reparent_to(pursuerChaserNP)
    else:
        # valid bamFile
        # restore sceneNP: through panda3d
        sceneNP = GameControlManager.get_global_ptr(
        ).get_reference_node_path().find('**/SceneNP')
        # reparent the reference node to render
        GameControlManager.get_global_ptr().get_reference_node_path().reparent_to(app.render)

        # restore the player's reference
        playerNP = GameControlManager.get_global_ptr().get_reference_node_path().find(
            '**/PlayerNP')

        # restore driver: through control manager
        playerDriver = GameControlManager.get_global_ptr().get_driver(0)
        # restore animations
        tmpList = [None for i in range(1)]
        playerAnimCtls.extend(tmpList)
        tmpAnims = AnimControlCollection()
        auto_bind(playerDriver, tmpAnims)
        for i in range(1):
            playerAnimCtls[i] = [None, None]
            # restore animations
            for j in range(tmpAnims.get_num_anims()):
                playerAnimCtls[i][j] = tmpAnims.get_anim(j)

        # restore chaser: through control manager
        pursuerChaser = GameControlManager.get_global_ptr().get_chaser(0)
        # restore animations
        pursuerAnimCtls.extend(tmpList)
        tmpAnims.clear_anims()
        auto_bind(pursuerChaser, tmpAnims)
        pursuerAnimCtls[0] = [None, None]
        for j in range(tmpAnims.get_num_anims()):
            pursuerAnimCtls[0][j] = tmpAnims.get_anim(j)

        # set creation parameters as strings before other drivers creation
        print('\n' + 'Current creation parameters:')
        setParametersBeforeCreation()

    # # first option: start the default update task for all plug-ins
    controlMgr.start_default_update()
    playerDriver.set_update_callback(driverCallback)
    pursuerChaser.set_update_callback(chaserCallback)
    globalClock = ClockObject.get_global_clock()

    # # second option: start the custom update task for all plug-ins
#     app.taskMgr.add(updateControls, 'updateControls', 10, appendTask=True)

    # write to bam file on exit
    app.win.set_close_request_event('close_request_event')
    app.accept('close_request_event', writeToBamFileAndExit, [bamFileName])

    # get player dims for kinematic ray cast
    modelDims = LVecBase3f()
    modelDeltaCenter = LVector3f()
    controlMgr.utilities.get_bounding_dimensions(
        playerNP, modelDims, modelDeltaCenter)
    playerHeightRayCast = LVector3f(0.0, 0.0, modelDims.get_z())

    # player will be driven by arrows keys
    app.accept('arrow_up', movePlayer, [forwardMove])
    app.accept('arrow_up-up', movePlayer, [forwardMoveStop])
    app.accept('arrow_left', movePlayer, [leftMove])
    app.accept('arrow_left-up', movePlayer, [leftMoveStop])
    app.accept('arrow_down', movePlayer, [backwardMove])
    app.accept('arrow_down-up', movePlayer, [backwardMoveStop])
    app.accept('arrow_right', movePlayer, [rightMove])
    app.accept('arrow_right-up', movePlayer, [rightMoveStop])

    # place camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 120.0, 5.0)
    trackball.set_hpr(0.0, 10.0, 0.0)

    # app.run(), equals to do the main loop in C++
    app.run()
