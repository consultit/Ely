'''
Created on Oct 12, 2019

@author: consultit
'''

from ely.gui import GameGUIManager
from ely.libtools import ValueList_string
from panda3d.core import NodePath, TextNode
import re
import sys
import os

# # specific data/functions declarations/definitions


def writeText(text, scale, color, location, app):
    '''common text writing'''
    textNode = NodePath(TextNode('CommonTextNode'))
    textNode.reparent_to(app.render2d)
    textNode.set_bin('fixed', 50)
    textNode.set_depth_write(False)
    textNode.set_depth_test(False)
    textNode.set_billboard_point_eye()
    textNode.node().set_text(text)
    textNode.set_scale(scale)
    textNode.set_color(color)
    textNode.set_pos(location)
    return textNode


def guiSystemInitialization(rmlUiBaseDir):
    # gui main menu
    mGuiMainMenuParam = os.path.join(rmlUiBaseDir, 'ely-main-menu.rml')
    # gui exit menu
    mGuiExitMenuParam = os.path.join(rmlUiBaseDir, 'ely-exit-menu.rml')
    # gui font paths
    fontPaths = [
        os.path.join(rmlUiBaseDir, 'Delicious-Roman.otf'),
        os.path.join(rmlUiBaseDir, 'Delicious-Italic.otf'),
        os.path.join(rmlUiBaseDir, 'Delicious-Bold.otf'),
        os.path.join(rmlUiBaseDir, 'Delicious-BoldItalic.otf'),
    ]
    #
    # gui main menu if any
    if mGuiMainMenuParam:
        GameGUIManager.get_global_ptr().set_gui_main_menu_path(mGuiMainMenuParam)
    # gui exit menu if any
    if mGuiExitMenuParam:
        GameGUIManager.get_global_ptr().set_gui_exit_menu_path(mGuiExitMenuParam)
    pathList = ValueList_string()
    for path in fontPaths:
        # an empty font_path is ignored
        if path:
            pathList.add_value(path)
    GameGUIManager.get_global_ptr().set_gui_font_paths(pathList)


# locals
CALLBACKSNUM = 5


def showMainMenu():
    GameGUIManager.get_global_ptr().show_main_menu()


def showExitMenu():
    GameGUIManager.get_global_ptr().show_exit_menu()

# event handler added to the main one


def rocketEventHandler(event, value):
    if value == 'main.body.load_logo':
        print('main.body.load_logo')


def on_exit_function():
    '''on exit function'''
    GameGUIManager.get_global_ptr().gui_cleanup()
    sys.exit(0)


def elyPreObjects_initialization():
    # register the add element function to gui (Rocket) main menu
    # register the event handler to gui main menu for each event value
    GameGUIManager.get_global_ptr().register_gui_event_handler('main.body.load_logo',
                                                               rocketEventHandler)
    # register the preset function to gui main menu
    # register the commit function to gui main menu
    GameGUIManager.get_global_ptr().set_gui_on_exit_function(on_exit_function)


def elyPostObjects_initialization(app):
    # /Gui (libRocket)
    # add show main menu event handler
    app.accept('m', showMainMenu)
    # handle 'close request' and 'esc' events
    app.win.set_close_request_event('close_request_event')
    app.accept('close_request_event', showExitMenu)
    app.accept('escape', showExitMenu)


def parse_compound_string(srcCompoundString, separator):
    # erase blanks
    compoundString = re.sub('\n|\t| ', '', srcCompoundString)
    # parse
    substrings = compoundString.split(separator)
    #
    return substrings
