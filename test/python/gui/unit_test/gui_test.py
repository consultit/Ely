'''
Created on Sep 24, 2018

@author: consultit
'''

import panda3d.core
import unittest
import ely.libtools
import suite


def setUpModule():
    pass


def tearDownModule():
    pass


class GUITEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        self.assertTrue(True)


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTest(GUITEST('test'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
