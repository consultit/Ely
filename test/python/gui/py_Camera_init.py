'''
Created on Oct 12, 2019

@author: consultit
'''

from ely.gui import GameGUIManager, P3GUIElementPtr, P3GUIDictionary, \
    P3GUIFactory, P3GUIElementFormControlInputPtr, \
    P3GUIElementFormControlSelectPtr
from ely.libtools import ValueList_string
from panda3d.core import NodePath, LVecBase4f, LVector3f
from enum import Enum
from py_Game_init import parse_compound_string, writeText
import os

app = None
rmlUiBaseDir = None

# locals
# common text writing
textNode = NodePath('textNode')


class CameraType(Enum):
    free_view_camera = 0
    chaser_camera = 1
    object_picker = 2
    none = 3


cameraType = CameraType.none
# PT(Object)camera
cameraEnabled = False
cameraDriverParams = {
    'enabled': 'false',
    'max_linear_speed': '40.0',
    'max_angular_speed': '40.0',
    'linear_accel': '30.0',
    'angular_accel': '30.0',
    'linear_friction': '5.0',
    'angular_friction': '5.0',
    'stop_threshold': '0.0',
    'fast_factor': '5.0',
    'sens_x': '0.05',
    'sens_y': '0.05',
    'pitch_limit': 'true@60.0',
    'head_left': 'enabled',
    'head_right': 'enabled',
    'inverted_translation': 'true',
    'inverted_rotation': 'false',
    'mouse_enabled_h': 'true',
    'mouse_enabled_p': 'true',
}
cameraChaserParams = {
    'enabled': 'false',
    'backward': 'false',
    'chased_object': 'NONE',
    'fixed_relative_position': 'false',
    'reference_object': 'render',
    'abs_max_distance': '25.0',
    'abs_min_distance': '18.0',
    'abs_max_height': '8.0',
    'abs_min_height': '5.0',
    'friction': '5.0',
    'fixed_lookat': 'false',
    'mouse_enabled_h': 'true',
    'mouse_enabled_p': 'true',
    'abs_lookat_distance': '5.0',
    'abs_lookat_height': '1.5',
}
chasedObject = 'chasedObject'
pickerOn = False
pickerCsIspherical = True


def rocketAddElements(mainMenu):
    '''add elements (tags) function for main menu'''
    # <button onclick='camera.options'>Camera options</button><br/>
    content = mainMenu.get_element_by_id('content')
    exitT = mainMenu.get_element_by_id('main::button::exit')
    if not content.is_empty():
        # create inputT element
        params = P3GUIDictionary()
        params.set_string('onclick', 'camera::options')
        inputT = P3GUIFactory.instance_element(
            P3GUIElementPtr(), 'button', 'button', params)
        P3GUIFactory.instance_element_text(inputT, 'Camera options')
        # create br element
        params.clear()
        params.set_string('id', 'br')
        br = P3GUIFactory.instance_element(
            P3GUIElementPtr(), 'br', 'br', params)
        # insert elements
        content.insert_before(inputT, exitT)
        content.insert_before(br, exitT)

# helpers


def setElementValue(param, paramsTable, document):
    inputElem = P3GUIElementFormControlInputPtr(
        document.get_element_by_id(param))
    inputElem.set_value(paramsTable[param])


def setElementChecked(param, checked, unchecked, defaultValue, paramsTable, document):
    inputElem = P3GUIElementFormControlInputPtr(
        document.get_element_by_id(param))
    actualChecked = paramsTable[param]
    if actualChecked == checked:
        inputElem.set_attribute_string('checked', 'true')
    elif actualChecked == unchecked:
        inputElem.remove_attribute('checked')
    elif defaultValue == checked:
        inputElem.set_attribute_string('checked', 'true')
    else:
        inputElem.remove_attribute('checked')


def setOptionValue(event, param, paramsTable):
    paramsTable[param] = event.get_parameter_string(param, '0.0')


def setOptionChecked(event, param, checked, unchecked, paramsTable):
    paramValue = event.get_parameter_string(param, '')
    if paramValue == 'true':
        paramsTable[param] = checked
    else:
        paramsTable[param] = unchecked


def rocketEventHandler(event, value):
    '''event handler added to the main one'''
    global rmlUiBaseDir, cameraType, chasedObject, pickerCsIspherical
    if value == 'camera::options':
        # hide main menu
        GameGUIManager.get_global_ptr().get_main_menu().hide()
        #  Load and show the camera options document.
        cameraOptionsMenu = GameGUIManager.get_global_ptr().get_rml_context().load_document(
            os.path.join(rmlUiBaseDir, 'ely-camera-options.rml'))
        if not cameraOptionsMenu.is_empty():
            cameraOptionsMenu.get_element_by_id('title').set_inner_rml(
                cameraOptionsMenu.get_title())
            # #update radio buttons
            # camera type
            if cameraType == CameraType.free_view_camera:
                cameraOptionsMenu.get_element_by_id('free_view_camera').set_attribute_bool(
                    'checked', True)
            elif cameraType == CameraType.chaser_camera:
                cameraOptionsMenu.get_element_by_id('chaser_camera').set_attribute_bool(
                    'checked', True)
            elif cameraType == CameraType.object_picker:
                cameraOptionsMenu.get_element_by_id('object_picker').set_attribute_bool(
                    'checked', True)
            elif cameraType == CameraType.none:
                cameraOptionsMenu.get_element_by_id('none').set_attribute_bool(
                    'checked', True)
            #
            cameraOptionsMenu.show()
    elif value == 'camera::body::load_logo':
        print('camera::body::load_logo')
    elif value == 'camera::free_view_camera::options':
        #  This event is sent from the 'onchange' of the 'free_view_camera'
        # radio button. It shows or hides the related options.
        cameraOptionsMenu = event.get_target_element().get_owner_document()
        if cameraOptionsMenu.is_empty():
            return
        free_view_camera_options = cameraOptionsMenu.get_element_by_id(
            'free_view_camera_options')
        if not free_view_camera_options.is_empty():
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                free_view_camera_options.set_property('display', 'none')
            else:
                free_view_camera_options.set_property('display', 'block')
                # set elements' values from options' values
                # pitch limit and pitch limit value: enabled@limit
                paramValuesStr = parse_compound_string(
                    cameraDriverParams['pitch_limit'], '@')
                # pitch limit value
                inputValueElem = P3GUIElementFormControlInputPtr(
                    cameraOptionsMenu.get_element_by_id('pitch_limit_value'))
                inputValueElem.set_value(paramValuesStr[1])
                # pitch limit
                inputPitchElem = P3GUIElementFormControlInputPtr(
                    cameraOptionsMenu.get_element_by_id('pitch_limit'))
                # checks
                if not inputPitchElem.get_attribute('checked').is_empty():
                    if paramValuesStr[0] == 'true':
                        # enable value input element
                        inputValueElem.set_disabled(False)
                    else:
                        # checked . unchecked: change event thrown
                        inputPitchElem.remove_attribute('checked')
                else:
                    if paramValuesStr[0] != 'true':
                        # disable value input element
                        inputValueElem.set_disabled(True)
                    else:
                        # unchecked . checked: change event thrown
                        inputPitchElem.set_attribute_string('checked', 'true')
                # max linear speed
                setElementValue('max_linear_speed', cameraDriverParams,
                                cameraOptionsMenu)
                # max angular speed
                setElementValue('max_angular_speed', cameraDriverParams,
                                cameraOptionsMenu)
                # linear accel
                setElementValue('linear_accel', cameraDriverParams,
                                cameraOptionsMenu)
                # angular accel
                setElementValue('angular_accel', cameraDriverParams,
                                cameraOptionsMenu)
                # linear friction
                setElementValue('linear_friction', cameraDriverParams,
                                cameraOptionsMenu)
                # angular friction
                setElementValue('angular_friction', cameraDriverParams,
                                cameraOptionsMenu)
                # fast factor
                setElementValue('fast_factor', cameraDriverParams,
                                cameraOptionsMenu)
                # sens_x
                setElementValue('sens_x', cameraDriverParams,
                                cameraOptionsMenu)
                # sens_y
                setElementValue('sens_y', cameraDriverParams,
                                cameraOptionsMenu)
    elif value == 'camera::chaser_camera::options':
        #  This event is sent from the 'onchange' of the 'chaser_camera'
        # radio button. It shows or hides the related options.
        cameraOptionsMenu = event.get_target_element().get_owner_document()
        if cameraOptionsMenu.is_empty():
            return
        chaserCameraOptions = cameraOptionsMenu.get_element_by_id(
            'chaser_camera_options')
        if not chaserCameraOptions.is_empty():
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                chaserCameraOptions.set_property('display', 'none')
            else:
                chaserCameraOptions.set_property('display', 'block')
                # set elements' values from options' values
                # chased object
                objectsSelect = P3GUIElementFormControlSelectPtr(
                    cameraOptionsMenu.get_element_by_id('chased_object'))
                if not objectsSelect.is_empty():
                    # remove all options
                    objectsSelect.remove_all()
                    # set object list
                    createdObjects = ['object1', 'object2',
                                      'object3', 'object4']
                    selectedIdx = objectsSelect.add('', '')
                    for objectT in createdObjects:
                        # add options
                        objectId = objectT
                        i = objectsSelect.add(objectId, objectId)
                        if objectId == chasedObject:
                            selectedIdx = i
                    # set first option as selected
                    objectsSelect.set_selection(selectedIdx)
                # fixed relative position
                setElementChecked('fixed_relative_position', 'true', 'false',
                                  'true', cameraChaserParams, cameraOptionsMenu)
                # backward
                setElementChecked('backward', 'true', 'false', 'true',
                                  cameraChaserParams, cameraOptionsMenu)
                # abs max distance
                setElementValue('abs_max_distance', cameraChaserParams,
                                cameraOptionsMenu)
                # abs min distance
                setElementValue('abs_min_distance', cameraChaserParams,
                                cameraOptionsMenu)
                # abs max height
                setElementValue('abs_max_height', cameraChaserParams,
                                cameraOptionsMenu)
                # abs min height
                setElementValue('abs_min_height', cameraChaserParams,
                                cameraOptionsMenu)
                # abs lookat distance
                setElementValue('abs_lookat_distance', cameraChaserParams,
                                cameraOptionsMenu)
                # abs lookat height
                setElementValue('abs_lookat_height', cameraChaserParams,
                                cameraOptionsMenu)
                # friction
                setElementValue('friction', cameraChaserParams,
                                cameraOptionsMenu)
    elif value == 'camera::object_picker::options':
        #  This event is sent from the 'onchange' of the 'object_picker'
        # radio button. It shows or hides the related options.
        cameraOptionsMenu = event.get_target_element().get_owner_document()
        if cameraOptionsMenu.is_empty():
            return
        objectPickerOptions = cameraOptionsMenu.get_element_by_id(
            'object_picker_options')
        if not objectPickerOptions.is_empty():
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                objectPickerOptions.set_property('display', 'none')
            else:
                objectPickerOptions.set_property('display', 'block')
                # set elements' values from options' values
                # constraint type
                if pickerCsIspherical:
                    cameraOptionsMenu.get_element_by_id(
                        'spherical_constraint').set_attribute_bool(
                        'checked', True)
                else:
                    cameraOptionsMenu.get_element_by_id(
                        'generic_constraint').set_attribute_bool(
                        'checked', True)
    elif value == 'pitch_limit::change':
        cameraOptionsMenu = event.get_target_element().get_owner_document()
        # check
        if not P3GUIElementFormControlInputPtr(
                cameraOptionsMenu.get_element_by_id('pitch_limit')).get_attribute(
                'checked').is_empty():
            P3GUIElementFormControlInputPtr(
                cameraOptionsMenu.get_element_by_id('pitch_limit_value')).set_disabled(
                False)
        else:
            P3GUIElementFormControlInputPtr(
                cameraOptionsMenu.get_element_by_id('pitch_limit_value')).set_disabled(
                True)
    # #Submit
    elif value == 'camera::form::submit_options':
        # check if ok or cancel
        paramValue = event.get_parameter_string('submit', 'cancel')
        if paramValue == 'ok':
            # set new camera type
            paramValue = event.get_parameter_string('camera', 'none')
            if paramValue == 'free_view_camera':
                cameraType = CameraType.free_view_camera
                # set options' values from elements' values
                # pitch limit and value: enabled@limit
                enabled = ('true' if
                           event.get_parameter_string('pitch_limit', '')
                           == 'true' else 'false')
                if enabled == 'true':
                    limit = event.get_parameter_string(
                        'pitch_limit_value', '0.0')
                else:
                    limit = parse_compound_string(
                        cameraDriverParams['pitch_limit'], '@')[1]
                cameraDriverParams['pitch_limit'] = enabled + '@' + limit
                # max linear speed
                setOptionValue(event, 'max_linear_speed', cameraDriverParams)
                # max angular speed
                setOptionValue(event, 'max_angular_speed', cameraDriverParams)
                # linear accel
                setOptionValue(event, 'linear_accel', cameraDriverParams)
                # angular accel
                setOptionValue(event, 'angular_accel', cameraDriverParams)
                # linear friction
                setOptionValue(event, 'linear_friction', cameraDriverParams)
                # angular friction
                setOptionValue(event, 'angular_friction', cameraDriverParams)
                # fast factor
                setOptionValue(event, 'fast_factor', cameraDriverParams)
                # sens_x
                setOptionValue(event, 'sens_x', cameraDriverParams)
                # sens_y
                setOptionValue(event, 'sens_y', cameraDriverParams)

            elif paramValue == 'chaser_camera':
                cameraType = CameraType.chaser_camera
                # set options' values from elements' values
                # chased object
                chasedObject = event.get_parameter_string('chased_object', '')
                if chasedObject:
                    cameraChaserParams['chased_object'] = chasedObject
                # fixed relative position
                setOptionChecked(event, 'fixed_relative_position', 'true',
                                 'false', cameraChaserParams)
                # backward
                setOptionChecked(event, 'backward', 'true', 'false',
                                 cameraChaserParams)
                # abs max distance
                setOptionValue(event, 'abs_max_distance', cameraChaserParams)
                # abs min distance
                setOptionValue(event, 'abs_min_distance', cameraChaserParams)
                # abs max height
                setOptionValue(event, 'abs_max_height', cameraChaserParams)
                # abs min height
                setOptionValue(event, 'abs_min_height', cameraChaserParams)
                # abs lookat distance
                setOptionValue(event, 'abs_lookat_distance',
                               cameraChaserParams)
                # abs lookat height
                setOptionValue(event, 'abs_lookat_height', cameraChaserParams)
                # friction
                setOptionValue(event, 'friction', cameraChaserParams)
            elif paramValue == 'object_picker':
                cameraType = CameraType.object_picker
                # set options' values from elements' values
                # constraint type
                paramValue = event.get_parameter_string('constraint_type', '')
                if paramValue == 'spherical':
                    pickerCsIspherical = True
                elif paramValue == 'generic':
                    pickerCsIspherical = False
            else:
                # default
                cameraType = CameraType.none
        # close (i.e. unload) the camera options menu and set as closed..
        cameraOptionsMenu = event.get_target_element().get_owner_document()
        cameraOptionsMenu.close()
        # return to main menu.
        GameGUIManager.get_global_ptr().get_main_menu().show()

# helper


def setCameraType(newType, actualType):
    global cameraEnabled, textNode, pickerOn, app
    # return if no camera type change is needed
    if newType == actualType:
        return
    # unset actual camera type
    if actualType == CameraType.free_view_camera:
        if cameraEnabled:
            # enabled: then disable it
            # disable
            cameraEnabled = False
            # remove text
            textNode.remove_node()
    elif actualType == CameraType.chaser_camera:
        if cameraEnabled:
            # enabled: then disable it
            # disable
            cameraEnabled = False
            # remove text
            textNode.remove_node()
    elif actualType == CameraType.object_picker:
        if pickerOn:
            if cameraEnabled:
                # enabled: then disable it
                # disable
                cameraEnabled = False
            # picker on: remove
            pickerOn = False
            # remove text
            textNode.remove_node()
    # set new type
    if newType == CameraType.free_view_camera:
        # add a new Driver component to camera and ...
        # ... enable it
        cameraEnabled = True
        # write text
        textNode = writeText('Free View Camera', 0.05,
                             LVecBase4f(1.0, 1.0, 0.0,
                                        1.0), LVector3f(-1.0, 0, -0.9),
                             app)
    elif newType == CameraType.chaser_camera:
        # add a new Chaser component to camera and ...
        # ... enable it
        cameraEnabled = True
        # write text
        textNode = writeText('Camera Chasing \'' + chasedObject + '\'', 0.05,
                             LVecBase4f(1.0, 1.0, 0.0,
                                        1.0), LVector3f(-1.0, 0, -0.9),
                             app)
    elif newType == CameraType.object_picker:
        if not pickerOn:
            # ... enable it
            cameraEnabled = True
            # picker off: add
            pickerOn = True
            # write text
            textNode = writeText('Object Picker Active', 0.05,
                                 LVecBase4f(1.0, 1.0, 0.0,
                                            1.0), LVector3f(-1.0, 0, -0.9),
                                 app)


def rocketPreset():
    '''preset function called from main menu'''
    setCameraType(CameraType.none, cameraType)


def rocketCommit():
    '''commit function called main menu'''
    setCameraType(cameraType, CameraType.none)


def camera_initialization(_app, _rmlUiBaseDir):
    global app, rmlUiBaseDir
    app = _app
    rmlUiBaseDir = _rmlUiBaseDir
    # register the add element function to (RmlUi) main menu
    GameGUIManager.get_global_ptr().register_gui_add_elements_function(
        rocketAddElements)
    # register the event handler to main menu for each event value
    GameGUIManager.get_global_ptr().register_gui_event_handler(
        'camera::options', rocketEventHandler)
    GameGUIManager.get_global_ptr().register_gui_event_handler(
        'camera::body::load_logo', rocketEventHandler)
    GameGUIManager.get_global_ptr().register_gui_event_handler(
        'camera::form::submit_options', rocketEventHandler)
    GameGUIManager.get_global_ptr().register_gui_event_handler(
        'camera::free_view_camera::options', rocketEventHandler)
    GameGUIManager.get_global_ptr().register_gui_event_handler(
        'camera::chaser_camera::options', rocketEventHandler)
    GameGUIManager.get_global_ptr().register_gui_event_handler(
        'camera::object_picker::options', rocketEventHandler)
    GameGUIManager.get_global_ptr().register_gui_event_handler(
        'pitch_limit::change', rocketEventHandler)
    # register the preset function to main menu
    GameGUIManager.get_global_ptr().register_gui_preset_function(
        rocketPreset)
    # register the commit function to main menu
    GameGUIManager.get_global_ptr().register_gui_commit_function(
        rocketCommit)
