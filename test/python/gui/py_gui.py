'''
Created on Oct 12, 2019

@author: consultit
'''

from ely.gui import GameGUIManager
from py_Game_init import guiSystemInitialization, elyPreObjects_initialization, \
    elyPostObjects_initialization
from py_Camera_init import camera_initialization
from panda3d.core import load_prc_file_data, WindowProperties, TextNode, LVector4f
from direct.showbase.ShowBase import ShowBase
import argparse
import textwrap
import sys
import os

# # global data declaration
scriptPath = os.path.dirname(os.path.abspath(__file__))
prefD = os.path.join(scriptPath, '..', 'assets')
dataDirs = [
    os.path.join(prefD, 'misc', 'test'),
    os.path.join(prefD, 'models', 'test'),
    os.path.join(prefD, 'textures', 'test'),
    os.path.join(prefD, 'sounds', 'test'),
    os.path.join(prefD, 'scripts', 'test'),
    os.path.join(prefD, 'shaders', 'test'), ]
app = None
renderPipeline = None
rmlUiBaseDir = None

# start base framework


def startFramework(msg):
    '''start base framework'''

    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''
    This script will perform ''' + msg + '''.
    '''))
    # set up arguments
    parser.add_argument('-r', '--render-pipeline', type=str,
                        help='the RenderPipeline dir')
    parser.add_argument('-d', '--data-dir', type=str,
                        action='append', help='the data dir(s)')
    parser.add_argument('-b', '--rmlui-base-dir', type=str,
                        help='the RmlUi base dir')
    # parse arguments
    args = parser.parse_args()

    # do actions
    renderPipeline = args.render_pipeline
    if args.rmlui_base_dir:
        rmlUiBaseDir = args.rmlui_base_dir
    else:
        rmlUiBaseDir = os.path.join(prefD, 'misc', 'test')
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)

    # Setup your application
    # Load your application's configuration
    for dataDir in dataDirs:
        load_prc_file_data('', 'model-path ' + dataDir)
    # Load your application's configuration
    load_prc_file_data('', 'model-path ' + dataDir)
    load_prc_file_data('', 'win-size 1024 768')
    load_prc_file_data('', 'show-frame-rate-meter #t')
    load_prc_file_data('', 'sync-video #t')
#     load_prc_file_data('', 'want-directtools #t')
#     load_prc_file_data('', 'want-tk #t')

    app = ShowBase()

    if renderPipeline:
        # 1: RenderPipeline
        sys.path.insert(0, renderPipeline)
        # Import the main render pipeline class
        from rpcore import RenderPipeline
        # Construct and create the pipeline
        render_pipeline = RenderPipeline()
        render_pipeline.pre_showbase_init()
        render_pipeline.create(app)
        render_pipeline.daytime_mgr.time = '7:40'
        render_pipeline.set_effect(app.render, prefD + 'scripts/game/scene-effect.yaml', {},
                                   sort=250)
        app.win.set_clear_color(LVector4f(0, 0, 0, 1))

    props = WindowProperties()
    props.setTitle(msg)
    app.win.requestProperties(props)

    # common callbacks
    #
    return (app, rmlUiBaseDir, renderPipeline)


def doExit():
    '''do exit'''
    #
    sys.exit(0)


if __name__ == '__main__':

    msg = '\'P3GUI tests\''
    app, rmlUiBaseDir, renderPipeline = startFramework(msg)

    # # here is room for your own code
    # print some help to screen
    text = TextNode('Help')
    text.set_text(
        msg + '\n\n'
        '- press \'up\'/\'left\'/\'down\'/\'right\' arrows TODO\n')
    textNodePath = app.aspect2d.attach_new_node(text)
    textNodePath.set_pos(-1.25, 0.0, 0.8)
    textNodePath.set_scale(0.035)
    if renderPipeline:
        textNodePath.set_color(0, 0, 0)

    # # create a gui manager
    guiMgr = GameGUIManager(app.win, app.mouseWatcher)
    # # gui mandatory configuration
    guiSystemInitialization(rmlUiBaseDir)
    # # gui system setup
    guiMgr.gui_setup()
    # # gui additional configurations
    elyPreObjects_initialization()
    elyPostObjects_initialization(app)
    camera_initialization(app, rmlUiBaseDir)

    # place camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 120.0, 5.0)
    trackball.set_hpr(0.0, 10.0, 0.0)

    # app.run(), equals to do the main loop in C++
    app.run()
