'''
Created on Jul 27, 2017

@author: consultit
'''

from common.BaseApplication import BaseApplication
from panda3d.core import LVecBase4f, NodePath, LVecBase3f, LVector3f
from ely.physics import GamePhysicsManager
from random import random

# static environment
staticModelData = {
    'left-wall':{'model':'left-wall.bam', 'shape_type':'box', 
                 'child_shape_type':'box',}, 
    'floor':{'model':'floor.bam', 'shape_type':'triangle_mesh',},
    'right-wall':{'model':'right-wall.bam', 'shape_type':'box', 
                 'child_shape_type':'box',},
    'back-wall':{'model':'back-wall.bam', 'shape_type':'triangle_mesh',},
    'ceiling':{'model':'ceiling.bam', 'shape_type':'box',},
    'countertop':{'model':'countertop.bam', 'shape_type':'box',},
    'front-wall':{'model':'front-wall.bam', 'shape_type':'box',},
    }
staticLightData = {
    'static-lights':{'model':'static-lights.bam'},
    }
dynamicModelData = {
    'pin' : {'model':'pin.bam', 'shape_type':'compound', 
                 'child_shape_type':'cylinder',}, 
    'ball' : {'model':'ball.bam', 'shape_type':'sphere', 
                 'child_shape_type':'box',}, 
    }

class BowlingApplication(BaseApplication):
    '''
    BowlingApplication
    '''

    def gamePlay(self, **kwArgs):
        '''
        BowlingApplication GamePlay
        '''
        
        # call BaseApplication' gamePlay
        super(BowlingApplication, self).gamePlay(**kwArgs)

        # load the static environment
        self.loadStaticEnvironment()
        
        # load the static lights
        self.loadStaticLights()
        
        # setup accessories
        self.initialSetup()
       
        # prepare the self.referenceNode's scene graph with RenderPipeline
        if self.useRenderPipeline:
            global PointLight, SpotLight
#             from rpcore import PointLight, SpotLight
            from panda3d._rplight import RPPointLight as PointLight, RPSpotLight as SpotLight
            # prepare root node for render pipeline
            rpDict = self.render_pipeline.prepare_scene(self.referenceNode)
            # save render pipeline's stuff
            self.rpLights = rpDict['lights']
            self.rpEnvprobes = rpDict['envprobes']
            self.rpTransparentObjects = rpDict['transparent_objects']
            # update lights' states
            self.setupLightsParameters()
            # load effects if any
#             self.render_pipeline.set_effect(self.referenceNode, self.gameDataDir + 'scene-effect.yaml', {}, sort=250)

    def loadStaticEnvironment(self):
        '''Load the static environment'''
        
        # create a node for static environment
        self.staticEnvironmentNode = self.referenceNode.attach_new_node('StaticEnvironment')
        
        # create the static rigid bodies
        self.staticRigidBodies = {}
        for key in staticModelData:
            modelNode = self.loader.load_model(staticModelData[key]['model'])
            for subNode in modelNode.find_all_matches('**/-PandaNode'):
                # check if there is a geom node
                if subNode.find('**/-GeomNode').is_empty():
                    continue
                self.physicsMgr.set_parameters_defaults(GamePhysicsManager.RIGIDBODY)
                self.physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 
                                                    'shape_type', staticModelData[key]['shape_type'])
                if 'child_shape_type' in staticModelData[key].keys():
                    self.physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 
                        'child_shape_type', staticModelData[key]['child_shape_type'])
                # create && get a reference to a BT3RigidBody
                self.staticRigidBodies[key] = self.physicsMgr.create_rigid_body(
                                        subNode.get_name() + 'RB').node()
                # set parameters
                self.staticRigidBodies[key].group_mask = self.groupMask
                self.staticRigidBodies[key].collide_mask = self.collideMask
                self.staticRigidBodies[key].mass = 0.0
                self.staticRigidBodies[key].set_owner_object(subNode)
                # setup
                self.staticRigidBodies[key].setup()

    def loadStaticLights(self):
        '''Load the static lights'''
        
        # create a node for static lights
        self.staticLightsNode = self.referenceNode.attach_new_node('StaticLights')
        # load static lights from all model files
        for key in staticLightData:
            self.loader.load_model(staticLightData[key]['model']).reparent_to(self.staticLightsNode)
    
    def setupLightsParameters(self):
        '''Update lights' states'''
        
        # update light parameters
        for light in self.rpLights:
            if light.light_type == PointLight.LT_point_light:
                light.near_plane = 10.0
            elif light.light_type == SpotLight.LT_spot_light:
                light.near_plane = 15.0
        # update lights
        self.add_task(self.updateLights, 'UpdateLights', sort=10)
       
    def updateLights(self, task):
        '''UpdateLights'''
         
        if self.useRenderPipeline:
            for light in self.rpLights:
                light.invalidate_shadows()
        #
        return task.cont
        
    def initialSetup(self):
        '''Initial setup'''
        
        try:
            floorNode = self.loader.load_model(staticModelData['floor']['model'])
            self.setupPinsArrangement(floorNode)
            self.setupBallsArrangement(floorNode)
        except:
            return
        
    def setupPinsArrangement(self, refNode):
        '''Create and arrange pins'''
        
        # read positions in floor model based on empties 'Pin.LL.NN'
        # only lane '00' has all pins, the other lanes only the first one
        self.pinRigiBodies = {}
        pinPos = {}
        for pin in refNode.find_all_matches('**/Pin*'):
            lane, num = pin.get_name().split('.')[1:]
            if not lane in pinPos.keys():
                pinPos[lane] = {}
            pinPos[lane][num] = pin.get_pos()
        #create the pins
        pinNode = self.loader.load_model(dynamicModelData['pin']['model'])
        pinUseShapeOfName = None
        for lane in pinPos.keys():
            # complete pinPos for this lane (if not '00')
            if lane != '00':
                laneDist = pinPos[lane]['00'] - pinPos['00']['00']
                # complete positions or this lane
                for num in pinPos['00']:
                    # exclude first pin (is defined for each lane)
                    if num != '00':
                        pinPos[lane][num] = pinPos['00'][num] + laneDist
            # now we have a complete set of positions for this lane
            for num in pinPos[lane]:
                pinName = 'Pin.' + lane + '.' + num
                pinInst = NodePath(pinName)
                pinBodyName = pinInst.get_name() + 'RB'
                pinNode.instance_to(pinInst)
                # set position
                pos = pinPos[lane][num]
                pinInst.set_pos(pos)
                self.physicsMgr.set_parameters_defaults(GamePhysicsManager.RIGIDBODY)
                self.physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 
                                        'shape_type', dynamicModelData['pin']['shape_type'])
                if 'child_shape_type' in dynamicModelData['pin'].keys():
                    self.physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 
                        'child_shape_type', dynamicModelData['pin']['child_shape_type'])
                if pinUseShapeOfName == None:
                    # set the first pin's collision shape to be shared 
                    pinUseShapeOfName = pinBodyName
                else:
                    # use the first pin's collision shape
                    self.physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 
                                        'use_shape_of', pinUseShapeOfName)
                # create && get a reference to a BT3RigidBody
                self.pinRigiBodies[pinName] = self.physicsMgr.create_rigid_body(
                                        pinBodyName).node()
                # set parameters
                self.pinRigiBodies[pinName].group_mask = self.groupMask
                self.pinRigiBodies[pinName].collide_mask = self.collideMask
                self.pinRigiBodies[pinName].mass = 1.0
                self.pinRigiBodies[pinName].set_owner_object(pinInst)
                # setup
                self.pinRigiBodies[pinName].setup()
 
    def setupBallsArrangement(self, refNode, ballsPerLane=3):
        '''Create and arrange balls'''
        
        # read positions in floor model based on empties 'Ball.LL'
        # only one ball per lane is defined
        self.ballRigiBodies = {}
        ballPos = {}
        for ball in refNode.find_all_matches('**/Ball*'):
            lane = ball.get_name().split('.')[1]
            ballPos[lane] = ball.get_pos()
        #create the balls
        ballNode = self.loader.load_model(dynamicModelData['ball']['model'])
        ballUseShapeOfName = None
        # get ball's radius
        modelDims = LVecBase3f()
        modelDeltaCenter = LVector3f()
        ballRadius = self.physicsMgr.get_bounding_dimensions(ballNode, modelDims,
                                                            modelDeltaCenter)
        for lane in ballPos.keys():
            # get the number of balls to create
            ballsNum = max(1, ballsPerLane)
            for num in range(ballsNum):
                ballName = 'Ball.' + lane + '.' + '{0:02d}'.format(num)
                ballInst = NodePath(ballName)
                ballBodyName = ballInst.get_name() + 'RB'
                ballNode.instance_to(ballInst)
                # set position
                if num == 0:
                    pos = ballPos[lane]
                else:
                    gap = ballRadius * (1.0 + 0.2 * (2.0 * random() - 1.0))
                    deltaPos = LVector3f(0.0, -gap * 2 * num, gap * num)
                    pos = ballPos[lane] + deltaPos
                ballInst.set_pos(pos)
                self.physicsMgr.set_parameters_defaults(GamePhysicsManager.RIGIDBODY)
                self.physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 
                                        'shape_type', dynamicModelData['ball']['shape_type'])
                if 'child_shape_type' in dynamicModelData['ball'].keys():
                    self.physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 
                        'child_shape_type', dynamicModelData['ball']['child_shape_type'])
                if ballUseShapeOfName == None:
                    # set the first ball's collision shape to be shared 
                    ballUseShapeOfName = ballBodyName
                else:
                    # use the first ball's collision shape
                    self.physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 
                                        'use_shape_of', ballUseShapeOfName)
                # create && get a reference to a BT3RigidBody
                self.ballRigiBodies[ballName] = self.physicsMgr.create_rigid_body(
                                        ballBodyName).node()
                # set parameters
                self.ballRigiBodies[ballName].group_mask = self.groupMask
                self.ballRigiBodies[ballName].collide_mask = self.collideMask
                self.ballRigiBodies[ballName].mass = 20.0
                self.ballRigiBodies[ballName].set_owner_object(ballInst)
                # setup
                self.ballRigiBodies[ballName].setup()               

