'''
Created on Jul 27, 2017

@author: consultit
'''

import sys
from direct.showbase.ShowBase import ShowBase
from direct.interval.MetaInterval import Sequence, Parallel
from panda3d.core import LVector3f, LPoint3f,LVecBase4f, load_prc_file_data, \
        LVecBase3f, LVecBase2i, LOrientation, LVecBase2f
from math import pi


class TestApplication(ShowBase):
    '''
    TestApplication
    '''

    def __init__(self, rp, dt, dataDirs=None):
        '''
        TestApplication init
        '''
        
        load_prc_file_data('', 'win-size 1024 768')
        load_prc_file_data('', 'sync-video #t')
        # load_prc_file_data('', 'want-directtools #t')
        # load_prc_file_data('', 'want-tk #t')
        if dataDirs:
            for dataDir in dataDirs:
                load_prc_file_data('', 'model-path ' + dataDir)
        
        self.rp = rp
        if self.rp:
#             sys.path.insert(0, '../../../../..')
#             sys.path.insert(0, '../../../../../RenderPipeline')
            # Import the main render pipeline class
            global PointLight, SpotLight
            from rpcore import RenderPipeline, PointLight, SpotLight
            # Construct and create the pipeline
            self.render_pipeline = RenderPipeline()
            self.render_pipeline.create(self)
            self.render_pipeline.daytime_mgr.time = dt
            # Use a special effect for rendering the scene, this is because the
            # roaming ralph model has no normals or valid materials
#             self.render_pipeline.set_effect(self.render, 'data/scene-effect.yaml', {}, sort=250)
        else:
            global SphereLight, Spotlight, AmbientLight
            from panda3d.core import SphereLight, Spotlight, AmbientLight
            ShowBase.__init__(self)
#         self.win.set_clear_color(LVector4f(0, 0, 0, 1))

        self.rootNP = self.render
        self.sceneNP = self.loader.load_model('test.bam')
        self.sceneNP.reparent_to(self.rootNP)
        
        # cube
        self.setupCube()
      
        # lights
        self.lights = []
        self.lights.extend(self.setupLights())
        
        # final setup
        if self.rp:
            # # prepare root node for render pipeline
            rpDict = self.render_pipeline.prepare_scene(self.rootNP)
            # # get render pipeline's stuff
            self.rpLights = rpDict['lights']
            self.rpEnvprobes = rpDict['envprobes']
            self.rpTransparentObjects = rpDict['transparent_objects']
            # # update lights' states
            self.addTask(self.updateLights, 'UpdateLights', sort=10)
        else:
            alight = AmbientLight('alight')
            alight.set_color(LVecBase4f(0.2, 0.2, 0.2, 1))
            alnp = self.rootNP.attach_new_node(alight)
            self.rootNP.set_light(alnp) 
            # Enable the shader generator for the receiving nodes
            self.rootNP.set_shader_auto()
            
        # place camera
        trackball = self.trackball.node()
        trackball.set_pos(0.0, 150.0, -2.0);
        trackball.set_hpr(0.0, 25.0, 0.0);

    def setupCube(self):
        '''Set cube up'''
        
        DIST = 30
        POSY = 20
        INTTIME = 10
        HPRTIME = INTTIME * 2
        self.cube = self.rootNP.find('**/Cube')
        if not self.cube.is_empty():
            self.cubeBase = self.rootNP.attachNewNode('orbitBase')
            self.cubeOrbit = self.cubeBase.attachNewNode('cubeOrbit')
            self.cubeOrbit.set_pos(0, POSY, 5.0)
            self.cube.reparent_to(self.cubeOrbit)
            self.cube.set_pos((0, -DIST, 0))
            cubeUp = self.cube.posInterval(INTTIME, LPoint3f(0, DIST, 0),
                                               startPos=LPoint3f(0, -DIST, 0),
                                               fluid=1, blendType='noBlend')
            cubeDown = self.cube.posInterval(INTTIME, LPoint3f(0, -DIST, 0),
                                                 startPos=LPoint3f(0, DIST, 0),
                                                 fluid=1, blendType='noBlend')
            cubePace = Sequence(cubeUp, cubeDown, name='cubePace')
            orbitRound = self.cubeOrbit.hprInterval(HPRTIME, LVecBase3f(360, 0, 0),
                                                      startHpr=LVecBase3f(0, 0, 0),
                                                      fluid=1, blendType='noBlend')
            self.cubeEllipsis = Parallel(orbitRound, cubePace, name='cubeEllipsis')
            self.cubeEllipsis.loop()        
            # cube move/scale
            self.accept('arrow_up', self.moveScaleCube, ['move', 'up'])
            self.accept('arrow_down', self.moveScaleCube, ['move', 'down'])
            self.accept('arrow_right', self.moveScaleCube, ['scale', 'up'])
            self.accept('arrow_left', self.moveScaleCube, ['scale', 'down'])
    
    def setupLights(self):
        '''Set lights up'''
        
        # point lights: {color: (1.0,0.896,0.402), radius: 10.0, distance: 100.0,
        # energy: 200.0, shadowResolution: 256, iesProfile:-1)}
        lights = []
        plnps = self.rootNP.find_all_matches('**/Point*')
        for plnp in plnps:
            pos = plnp.get_pos(self.rootNP)
            lights.append(self.createPointLight(plnp.get_name() + '.PL',
                            (1.0, 0.896, 0.402), 10.0, 100.0, 200.0, 512, pos, -1))
        # spot lights: {color: (1.0,0.896,0.402), distance: 100.0, spotSize:75,
        # energy: 200.0, shadowResolution: 256, iesProfile:-1)}
        slnps = self.rootNP.find_all_matches('**/Spot*')
        for slnp in slnps:
            pos = slnp.get_pos(self.rootNP)
            if self.rp:
                rot = slnp.get_mat(self.rootNP).xform_vec((0, 0, -1))
            else:
                rot = slnp.get_hpr(self.rootNP)
            #
            lights.append(self.createSpotLight(slnp.get_name() + '.SL',
                            (1.0, 0.896, 0.402), 90.0, 100.0, 400.0, 512, pos, rot, -1))
        # toggle lights
        self.lightToggle = [1] * len(lights)
        for i in range(len(lights)):
            self.accept(str(i + 1), self.toggleLight, [i])
        #
        return lights
    
    def createPointLight(self, name, color, radius, distance, energy,
                         shadowResolution, pos, iesProfile):
        '''Create point light'''
        
        light = None
        if self.rp:
            light = PointLight()
            light.pos = pos
            light.radius = distance
            light.near_plane = 5.5
            light.energy = 20.0 * energy
            color4 = LVecBase4f(color[0], color[1], color[2], 1.0)
            light.color = color4.xyz
            light.casts_shadows = True
            light.shadow_map_resolution = shadowResolution
            light.inner_radius = 0.4
            light.ies_profile = iesProfile
            self.render_pipeline.add_light(light)
        else:
            light = self.rootNP.attach_new_node(SphereLight(name))
            light.set_pos(pos)
            light.node().set_scene(self.rootNP)
#             light.node().set_shadow_caster(True)
#             light.node().shadow_buffer_size = LVecBase2i(shadowResolution, 
#                                                     shadowResolution)
#             light.node().show_frustum()
#             light.node().attenuation = LVecBase3f(0, 0, 1)
            color4 = LVecBase4f(color[0], color[1], color[2], energy)
            light.node().color = color4
#             light.node().specular_color = color4 ERROR
            light.node().max_distance = distance
            light.node().radius = radius
            self.rootNP.set_light(light)
        return light
    
    def createSpotLight(self, name, color, spotSize, distance, energy,
                         shadowResolution, pos, rot, iesProfile):
        '''Create spot light'''
        
        light = None
        if self.rp:
            light = SpotLight()
            light.direction = rot
            light.pos = pos
            light.radius = distance
            light.energy = 20.0 * energy
            color4 = LVecBase4f(color[0], color[1], color[2], 1.0)
            light.color = color4.xyz
            light.casts_shadows = True
            light.shadow_map_resolution = shadowResolution
            light.fov = spotSize / pi * 180.0
            light.near_plane = 10.0
            light.ies_profile = iesProfile
            self.render_pipeline.add_light(light)
        else:
            light = self.rootNP.attach_new_node(Spotlight(name))
            light.set_pos(pos)
            light.set_hpr(rot)
            right = self.rootNP.get_relative_vector(light, LVector3f.right())
            quat = LOrientation(right, -90)
            light.set_quat(light.get_quat(self.rootNP) * quat)
            light.node().set_scene(self.rootNP)
            light.node().set_shadow_caster(True)
            light.node().shadow_buffer_size = LVecBase2i(shadowResolution,
                                                    shadowResolution)
#             light.node().show_frustum()
            light.node().exponent = spotSize
#             light.node().get_lens().fov = LVecBase2f(spotSize, spotSize)
#             light.node().attenuation = LVecBase3f(0, 0, 1)
            color4 = LVecBase4f(color[0], color[1], color[2], energy)
            light.node().color = color4
#             light.node().specular_color = color4 ERROR
            light.node().max_distance = distance
            self.rootNP.set_light(light)
        #
        return light
        
    def moveScaleCube(self, what, where):
        '''Move/Scale Cube'''
        
        if what == 'move':
            if where == 'up':
                self.cubeBase.set_pos(self.cubeBase.get_pos() + LVecBase3f(0, 0, 1))
            else:
                self.cubeBase.set_pos(self.cubeBase.get_pos() - LVecBase3f(0, 0, 1))
        else:
            # scale
            if where == 'up':
                self.cube.set_scale(self.cube.get_scale() + 0.1)
            else:
                self.cube.set_scale(self.cube.get_scale() - 0.1)
        
    def toggleLight(self, idx):
        '''Toggle Light'''
        
        if self.lightToggle[idx] == 1:
            if self.rp:
                self.render_pipeline.remove_light(self.lights[idx])
            else:
                self.rootNP.clear_light(self.lights[idx])
            self.lightToggle[idx] = 0
        else:
            # self.lightToggle[idx] == 0
            if self.rp:
                self.render_pipeline.add_light(self.lights[idx])
            else:
                self.rootNP.set_light(self.lights[idx])
            self.lightToggle[idx] = 1
        
    def updateLights(self, task):
        '''UpdateLights'''
         
        if self.rp:
            for light in self.lights:
                light.invalidate_shadows()
        #
        return task.cont
                 
    def get_children_type(self, node):
        for child in node.get_children():
            child.ls()
            print(type(child.node()))
            self.get_children(child)
    
    def get_attrs_tags(self, nodeNP):
        for key in dir(nodeNP.node()):
            try:
                if key.startswith('get_') or key.startswith('is_') or key.startswith('has_'):
                    print (key, str(getattr(nodeNP.node(), key)()))
                else:
                    print (key, str(getattr(nodeNP.node(), key)))
            except:
                print(key, 'Unexpected error:', sys.exc_info()[0])
                continue
 
        tags = nodeNP.get_tag_keys()
        for t in tags:
            print(t, nodeNP.get_tag(t))

