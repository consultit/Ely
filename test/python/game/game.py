'''
Created on May 20, 2017

@author: consultit
'''

from bowling.BowlingApplication import BowlingApplication
import argparse, textwrap
from panda3d.core import BitMask32, LVecBase4f, LPoint3f, LVecBase3f

## GAME DATA
groupMask = BitMask32(0x10)
collideMask = BitMask32(0x10)
mask = BitMask32(0x10)
cameraPos=LPoint3f(0.0, -200.0, 20.0)
cameraHpr=LVecBase3f(0.0, 0.0, 0.0)
clearColor = LVecBase4f(0, 0, 0, 1)
debugMode = True
# callbacks' keys
debugKey = 'g'
physicPickingKey = 'h'
toggleCameraDriverKey = 'b'
toggleEnableCameraDriverKey = 'n'
rayTestClosestKey = 'y'
rayTestAllKey = 'u'
selectContactObjectKey = 'o'
contactTestKey = 'p'
cameraDriverKeys = {'move_forward':'mouse3', 'move_backward':'x',
            'rotate_head_left':'a', 'rotate_head_right':'d',
            'rotate_pitch_up':'w', 'rotate_pitch_down':'s',            
            'move_strafe_left':'q', 'move_strafe_right':'e',
            'move_up':'r', 'move_down':'f',}

if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    This is the Bowling game.
    
    Commands:
        - \'''' + debugKey + '''\': to toggle physics' debug drawings.
        - \'''' + physicPickingKey + '''\': to pick-and-drag physic bodies with the mouse hold the key.        
        - \'''' + rayTestClosestKey + '''\': to perform physics' ray test closest. 
        - \'''' + rayTestAllKey + '''\': to perform physics' ray test all. 
        - \'''' + selectContactObjectKey + '''\': to select physics' contact object.
        - \'''' + contactTestKey + '''\': to perform physics' contact test.
        - \'''' + toggleCameraDriverKey + '''\': to toggle camera's driver type: 'Panda3d default' | 'Free style view'.

    Commands for 'Free style view' camera movement: 
        - \'''' + toggleEnableCameraDriverKey + '''\': to enable/disable driver.
        - \'''' + cameraDriverKeys['move_forward'] + '''\': to move forward. 
        - \'''' + cameraDriverKeys['move_backward'] + '''\': to move backward.
        - \'''' + cameraDriverKeys['rotate_head_left'] + '''\': to rotate head left. 
        - \'''' + cameraDriverKeys['rotate_head_right'] + '''\': to rotate head right.
        - \'''' + cameraDriverKeys['rotate_pitch_up'] + '''\': to rotate pitch up.
        - \'''' + cameraDriverKeys['rotate_pitch_down'] + '''\': to rotate pitch down.
        - \'''' + cameraDriverKeys['move_strafe_left'] + '''\': to move strafe left. 
        - \'''' + cameraDriverKeys['move_strafe_right'] + '''\': to move strafe right.
        - \'''' + cameraDriverKeys['move_up'] + '''\': to move up.
        - \'''' + cameraDriverKeys['move_down'] + '''\': to move down.
    '''))    
    # set up arguments
    parser.add_argument('-d','--data-dir', type=str, action='append', help='the data dir(s)')
    rpGroup = parser.add_argument_group('RenderPipeline', 'RenderPipeline related arguments')
    rpGroup.add_argument('-r', '--render-pipeline', action='store_true', help='use RenderPipeline')
    rpGroup.add_argument('-t', '--day-time', type=str, default='20:15', help='the day time to use')
    # parse arguments
    args = parser.parse_args()
    # do actions
    app = BowlingApplication(title='Bowling', useRenderPipeline=args.render_pipeline, 
                             daytime=args.day_time, dataDirs=args.data_dir, mask=mask, 
                             groupMask=groupMask, collideMask=collideMask, debugMode=debugMode)
    app.setup()
    app.gamePlay(cameraPos=cameraPos, cameraHpr=cameraHpr, clearColor=clearColor,
                 toggleCameraDriverKey=toggleCameraDriverKey, 
                 toggleEnableCameraDriverKey=toggleEnableCameraDriverKey,
                 cameraDriverKeys=cameraDriverKeys, debugKey=debugKey,
                 physicPickingKey=physicPickingKey, rayTestClosestKey=rayTestClosestKey, 
                 rayTestAllKey=rayTestAllKey, selectContactObjectKey=selectContactObjectKey,
                 contactTestKey=contactTestKey)
    app.run()
