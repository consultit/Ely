'''
Created on Jul 27, 2017

@author: consultit
'''

from test.TestApplication import TestApplication
import argparse, textwrap

if __name__ == '__main__':    
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    This is the Test application.
    '''))    
    # set up arguments
    parser.add_argument('-d','--data-dir', type=str, action='append', help='the data dir(s)')
    rpGroup = parser.add_argument_group('RenderPipeline', 'RenderPipeline related arguments')
    rpGroup.add_argument('-r', '--render-pipeline', action='store_true', help='use RenderPipeline')
    rpGroup.add_argument('-t', '--day-time', type=str, default='20:15', help='the day time to use')
    # parse arguments
    args = parser.parse_args()
    # do actions
    app = TestApplication(rp=args.render_pipeline, dt=args.day_time,
                          dataDirs=args.data_dir)
    app.run()
