'''
Created on Jul 22, 2017

@author: consultit
'''

from panda3d.core import load_prc_file_data, WindowProperties, BitMask32, \
        NodePath, TransformState, LMatrix4f, LPoint3f, LVecBase4f, LVecBase3f

from direct.showbase.ShowBase import ShowBase

import ely.libtools
from ely.ai import GameAIManager
from ely.audio import GameAudioManager
from ely.control import GameControlManager
from ely.physics import GamePhysicsManager, BT3RayTestResult, BT3ContactTestResult

class BaseApplication(ShowBase, object):
    '''
    Game BaseApplication object
    '''

    def __init__(self, useRenderPipeline = False, title = 'game', daytime = '12:00',
                 dataDirs=None, mask=BitMask32(0x10), groupMask=BitMask32(0x10),
                 collideMask=BitMask32(0x10), debugMode=False):
        '''
        Game BaseApplication object constructor.
        '''
        
        # application parameters
        self.mask = mask
        self.groupMask = groupMask
        self.collideMask = collideMask
        self.debugMode = debugMode
        prefD =  '../../../assets/'
        self.gameDataDirs = [prefD + 'models/game', prefD + 'textures/game', prefD + 'sounds/game', 
            prefD + 'scripts/game', prefD + 'shaders/game']
       
        # Load your application's configuration
        for dataDir in self.gameDataDirs:
            load_prc_file_data('', 'model-path ' + dataDir)
        load_prc_file_data('', 'win-size 1024 768')
        load_prc_file_data('', 'show-frame-rate-meter #t')
        load_prc_file_data('', 'sync-video #t')
#         load_prc_file_data('', 'want-directtools #t')
#         load_prc_file_data('', 'want-tk #t')
        # set additional model dir(s)
        if dataDirs:
            for dataDir in dataDirs:
                load_prc_file_data('', 'model-path ' + dataDir)

        # check RenderPipeline use
        self.useRenderPipeline = useRenderPipeline
        if self.useRenderPipeline:
            # Notice that you must not call ShowBase.__init__ (or super), the
            # render pipeline does that for you. If this is unconvenient for you,
            # have a look at the other initialization possibilities.
    
#             # Insert the pipeline path to the system path, this is required to be
#             # able to import the pipeline classes. In case you placed the render
#             # pipeline in a subfolder of your project, you have to adjust this.
#             import os
#             scriptPath = os.path.dirname(os.path.abspath(__file__))
#             sys.path.insert(0, scriptPath + '/../../../../..')
#             sys.path.insert(0, scriptPath + '/../../../../RenderPipeline')
    
            # Import the main render pipeline class
            from rpcore import RenderPipeline
    
            # Construct and create the pipeline
            self.render_pipeline = RenderPipeline()
            self.render_pipeline.create(self)
            self.render_pipeline.daytime_mgr.time = daytime
        else:
            ShowBase.__init__(self)
        
        # Done! You can start setting up your application stuff as regular now.
        props = WindowProperties()
        props.setTitle(title)
        self.win.requestProperties(props)
        
    def setup(self, updateTask = None, **updateArgs):
        '''
        Sets Game BaseApplication's sub-systems up.
        '''
        
        # create managers
        self.aiMgr = GameAIManager(taskSort=0, root=self.render, mask=self.mask)
        self.audioMgr = GameAudioManager(taskSort=0)
        self.controlMgr = GameControlManager(win=self.win, taskSort=10, 
                                    root=self.render, mask=self.mask)
        self.physicsMgr = GamePhysicsManager(taskSort=10, root=self.render, 
                                    groupMask=self.groupMask, 
                                    collideMask=self.collideMask)
        
        # set a common reference node and reparent it to render
        self.referenceNode = NodePath('ReferenceNode')
        self.aiMgr.set_reference_node_path(self.referenceNode)
        self.audioMgr.set_reference_node_path(self.referenceNode)
        self.controlMgr.set_reference_node_path(self.referenceNode)
        self.physicsMgr.set_reference_node_path(self.referenceNode)
        self.referenceNode.reparent_to(self.render)
        
        if updateTask is not None:
            self.taskMgr.add(updateTask, **updateArgs)
        else:
            # start managers' default updates
            self.aiMgr.start_default_update()
            self.audioMgr.start_default_update()
            self.controlMgr.start_default_update()
            self.physicsMgr.start_default_update()

        ### DEBUG ###
        if self.debugMode:
            self.debugDraw = {}
            self.debugFlag = {}
            self.plugIn = None
            self.navMesh = None

    def gamePlay(self, cameraPos=LPoint3f(0.0, -200.0, 20.0), cameraHpr=LVecBase3f(0.0, 0.0, 0.0),
                 clearColor=LVecBase4f(0, 0, 0, 1), toggleCameraDriverKey='b',
                 toggleEnableCameraDriverKey='n', 
                 cameraDriverKeys = {'move_forward':'mouse3', 'move_backward':'x',
                'rotate_head_left':'a', 'rotate_head_right':'d',
                'rotate_pitch_up':'w', 'rotate_pitch_down':'s',
                'move_strafe_left':'q', 'move_strafe_right':'e',
                'move_up':'r', 'move_down':'f', }, 
                 debugKey='g', physicPickingKey='h', rayTestClosestKey='y', rayTestAllKey='u', 
                 selectContactObjectKey='o', contactTestKey='p'):
        '''
        BaseApplication GamePlay
        '''
        
        # setup
        self.win.set_clear_color(clearColor)
        # setup camera
        trackball = self.trackball.node()
        trackball.set_pos(-cameraPos)
        trackball.set_hpr(-cameraHpr)
        # camera driver stuff
        self.cameraDriverKeys = cameraDriverKeys
        self.accept(toggleCameraDriverKey, self.toggleCameraDriver)
        self.accept(toggleEnableCameraDriverKey, self.toggleEnableCameraDriver)
        
        ### DEBUG ###
        if self.debugMode:
            self.enableDebugDraw('ai', False, plugIn=None, navMesh=None)
            self.enableDebugDraw('audio', False)
            self.enableDebugDraw('control', False)
            self.enableDebugDraw('physics', True)
            # callback
            self.accept(debugKey, self.toggleDebugDraw, [])
            
            ### PHYSIC PICKING ###
            self.enablePhysicPicking(True, physicPickingKey, GamePhysicsManager.DOF6)
            
            ### RAY TESTS FROM CAMERA ###
            self.accept(rayTestClosestKey, self.rayTestFromCamera, [True])
            self.accept(rayTestAllKey, self.rayTestFromCamera, [False])
            
            ### CONTACT TESTS ###
            self.contactData = {'objectA':NodePath(), 'objectB':NodePath(), 'numContacts':16}
            self.accept(selectContactObjectKey, self.selectContactObject, [True])
            self.accept(contactTestKey, self.contactTestProxy)
    
    ### CAMERA ###
    def addCameraDriver(self, driverKeys=dict()):
        '''Adds a driver to the camera, initially disabled.
        
        @param driverKeys: specifies keys for starting the following movements:
            - move_forward, move_backward, rotate_head_left, rotate_head_right, 
                rotate_pitch_up, rotate_pitch_down, move_strafe_left, 
                move_strafe_right, move_up, move_down
            The '-up' keys stop the corresponding movements.
            Keys for disabled movements are specified as empty strings.
        '''

        if not hasattr(self, 'cameraDriver'):
            # disable default camera
            self.cameraParent = self.camera.get_parent()
            mat = LMatrix4f(self.mouseInterfaceNode.get_mat())
            mat.invert_in_place()
            self.disableMouse()
            self.camera.set_transform(TransformState.make_mat(mat))
            # set driver parameters specific for camera
            self.controlMgr.set_parameters_defaults(GameControlManager.DRIVER)
            self.controlMgr.set_parameter_value(GameControlManager.DRIVER, 
                                                'inverted_translation', 'true')
            self.controlMgr.set_parameter_value(GameControlManager.DRIVER, 
                                                'inverted_rotation', 'false')
            self.controlMgr.set_parameter_value(GameControlManager.DRIVER, 
                                                'mouse_head', 'enabled')
            self.controlMgr.set_parameter_value(GameControlManager.DRIVER, 
                                                'mouse_pitch', 'enabled')
            # create the driver (attached to the reference node)
            cameraDriverNP = self.controlMgr.create_driver('CameraDriver')
            # and store a reference to it
            self.cameraDriver = cameraDriverNP.node()
            # hinerit the camera's transform
            self.cameraDriver.set_transform(self.camera.node().get_transform())
            self.camera.set_transform(TransformState.make_identity())
            # attach the camera to the driver
            self.camera.reparent_to(cameraDriverNP)
            # set more parameters of the driver
            self.cameraDriver.max_linear_speed=40.0
            self.cameraDriver.max_angular_speed=40.0
            self.cameraDriver.linear_accel=30.0
            self.cameraDriver.angular_accel=30.0
            self.cameraDriver.linear_friction=5.0
            self.cameraDriver.angular_friction=5.0
            self.cameraDriver.stop_threshold=0.0
            self.cameraDriver.fast_factor=5.0
            self.cameraDriver.sens=[0.05,0.05]
            self.cameraDriver.set_pitch_limit(True, 80.0)
            # setup movements' enabling/disabling callbacks
            self.driverKeyValues = driverKeys.values()
            for movement in driverKeys:
                # movement prefix is 'move_' or 'rotate_'
                bareMove = movement.lstrip('move_')
                bareMove = bareMove.lstrip('rotate_')
                if driverKeys[movement] != '':
                    # enable movement
                    getattr(self.cameraDriver, 'set_enable_' + bareMove)(True)
                    # set the callbacks
                    # enabling
                    self.accept(driverKeys[movement], self.cameraMovementClbk, 
                                [movement, True])
                    # disabling
                    self.accept(driverKeys[movement]+'-up', self.cameraMovementClbk, 
                                [movement, False])
                else:
                    # disable movement
                    getattr(self.cameraDriver, 'set_enable_' + bareMove)(False)
            # disable
            self.enableCameraDriver(False)
            
    def removeCameraDriver(self):
        '''Removes a driver from the camera.'''
        
        if hasattr(self, 'cameraDriver'):
            # reparent the camera to the original parent
            self.camera.reparent_to(self.cameraParent)
            delattr(self, 'cameraParent')
            # hinerit the driver's transform
            self.camera.set_transform(self.cameraDriver.get_transform())
            self.cameraDriver.set_transform(TransformState.make_identity())
            # destroy the driver 
            self.controlMgr.destroy_driver(NodePath.any_path(self.cameraDriver))
            # re-enable default camera
            mat = LMatrix4f(self.camera.get_transform().get_mat())
            mat.invert_in_place()
            self.camera.set_transform(TransformState.make_identity())
            self.mouseInterfaceNode.set_mat(mat)
            self.enableMouse()
            # delete cameraDriver attribute
            delattr(self, 'cameraDriver')
            # ignore movements' enabling/disabling callbacks
            for keyValue in self.driverKeyValues:
                self.ignore(keyValue)
            # delete driverKeyValues attribute
            delattr(self, 'driverKeyValues')
            
    def enableCameraDriver(self, enable):
        '''Temporarily enables/disables camera's driver.'''
        
        if hasattr(self, 'cameraDriver'):
            if enable:
                self.cameraDriver.enable()
            else:
                self.cameraDriver.disable()
            
    def cameraMovementClbk(self, movement, enable):
        '''Callback for camera enabling/disabling movements'''
        
        if hasattr(self, 'cameraDriver'):
            setattr(self.cameraDriver, movement, enable)
            
    def toggleCameraDriver(self):
        '''Adds/removes a camera driver'''
        
        if hasattr(self, 'cameraDriver'):
            self.removeCameraDriver()
        else:
            self.addCameraDriver(self.cameraDriverKeys)
            
    def toggleEnableCameraDriver(self):
        '''Temporarily enables/disables camera driver.'''
        
        if hasattr(self, 'cameraDriver'):
            if self.cameraDriver.is_enabled:
                self.enableCameraDriver(False) 
            else:
                self.enableCameraDriver(True) 
        
    ### DEBUG ###
    def enableDebugDraw(self, system, enable, plugIn=None, navMesh=None):
        '''Enables/disables debug for system'''

        if system in ['ai', 'audio', 'control', 'physics']:
            self.debugDraw[system] = enable
            self.debugFlag[system] = not enable
        # ai specific
        if system == 'ai':
            self.plugIn = plugIn
            self.navMesh = navMesh
            if enable:
                self.aiMgr.get_reference_node_path_debug().reparent_to(self.render)
                self.aiMgr.get_reference_node_path_debug_2d().reparent_to(self.aspect2d)
                if self.plugIn != None:
                    self.plugIn.enable_debug_drawing(self.camera)
                if self.navMesh != None:
                    self.navMesh.enable_debug_drawing(self.camera)
            else:
                self.aiMgr.get_reference_node_path_debug().detach_node()
                self.aiMgr.get_reference_node_path_debug_2d().detach_node()
                if self.plugIn != None:
                    self.plugIn.disable_debug_drawing()
                if self.navMesh != None:
                    self.navMesh.disable_debug_drawing()
        # physics specific
        if system == 'physics':
            if enable:
                self.physicsMgr.get_reference_node_path_debug().reparent_to(self.render)
            else:
                self.physicsMgr.get_reference_node_path_debug().detach_node()
      
    def toggleDebugDraw(self):
        '''Toggles debug draw'''
        
        if self.debugDraw['ai']:
            self.debugFlag['ai'] = not self.debugFlag['ai']
            if self.plugIn != None:
                self.plugIn.toggle_debug_drawing(self.debugFlag['ai'])
            if self.navMesh != None:
                self.navMesh.toggle_debug_drawing(self.debugFlag['ai'])
            
        if self.debugDraw['audio']:
            self.debugFlag['audio'] = not self.debugFlag['audio']
        
        if self.debugDraw['control']:
            self.debugFlag['control'] = not self.debugFlag['control']
        
        if self.debugDraw['physics']:
            self.debugFlag['physics'] = not self.debugFlag['physics']
            self.physicsMgr.debug(self.debugFlag['physics'])
            
    def enablePhysicPicking(self, enable, key, pickingType=GamePhysicsManager.POINT2POINT):
        '''Enables/disables picking of physic objects'''
        
        if enable:
            self.physicsMgr.enable_picking(self.render, NodePath.any_path(self.camNode), 
                                self.mouseWatcher.node(), key, key + '-up')
            self.physicsMgr.set_picking_type(pickingType)
        else:
            self.physicsMgr.disable_picking()

    def rayTestFromCamera(self, closest=True, echoResult=True):
        '''Raycast test from camera'''
        
        if self.mouseWatcherNode.has_mouse():
            # Get to and from pos in camera coordinates
            pMouse = self.mouseWatcherNode.get_mouse()
            pFrom = LPoint3f()
            pTo = LPoint3f()
            if self.camLens.extrude(pMouse, pFrom, pTo):
                #Transform to global coordinates
                rayFromWorld = self.render.get_relative_point(self.camera, pFrom)
                rayToWorld = self.render.get_relative_point(self.camera, pTo)
                #cast a ray to detect a body
                if closest:
                    result = self.physicsMgr.ray_test(rayFromWorld, rayToWorld, 
                                    BT3RayTestResult.CLOSEST)
                else:
                    result = self.physicsMgr.ray_test(rayFromWorld, rayToWorld, 
                                    BT3RayTestResult.ALL)
                # check if hit
                if result.has_hit:
                    if echoResult:
                        print('Hit objects: ' + str(result.num_hit_objects))
                        print('Hit points: ' + str(result.num_hit_points))
                        print('Hit normals: ' + str(result.num_hit_normals))
                        print('Hit fractions: ' + str(result.num_hit_fractions))
                        i = 0
                        for hitObject in result.hit_objects:
                            print('Object: ' + hitObject.get_name())
                            print('\tpoint: ' + str(result.get_hit_point(i)))
                            print('\tnormal: ' + str(result.get_hit_normal(i)))
                            print('\tfraction: ' + str(result.get_hit_fraction(i)))
                            i += 1
                    # return objects
                    return result.hit_objects
        # return empty NodePath
        return (NodePath(),)                        

    def contactTest(self, objectA, objectB, maxContacts=16, echoResult=True):
        '''Contact test for objects'''

        if objectB.is_empty():
            contactType = BT3ContactTestResult.SINGLE
        else:
            contactType = BT3ContactTestResult.PAIR
        # return results
        result = self.physicsMgr.contact_test(objectA, objectB, contactType, maxContacts)
        if echoResult:
            print('Contact Points: ' + str(result.num_contact_points))
            print('Closest Distance Threshold: ' + str(result.closest_distance_threshold))
            print('Point Capacity: ' + str(result.point_capacity))
            i = 0
            for contact in result.contact_points:
                print('Contact point:')
                print('\tpoint on a: ' + str(contact.point_on_a))
                print('\tpoint on b: ' + str(contact.point_on_b))
                print('\tnormal on b: ' + str(contact.normal_on_b))
                print('\tdistance: ' + str(contact.distance))
                if contactType == BT3ContactTestResult.SINGLE:
                    nodeA = result.get_contact_objects(i)[0]
                    nodeB = result.get_contact_objects(i)[1]
                    print('\tobjects: ' + str('"' + nodeA.get_name() + '" - "'
                                              + nodeB.get_name() + '"'))
                i += 1
        # return results
        if contactType == BT3ContactTestResult.SINGLE:
            retValue = {}
            for i in range(result.num_contact_points):
                retValue[result.get_contact_point(i)] = result.get_contact_objects(i)
        else:
            retValue = result.contact_points
        #   
        return retValue
    
    def selectContactObject(self, echoResult=True):
        '''Select object under mouse, cyclically assigning self.contactObjects'''
        
        hitObject = self.rayTestFromCamera(closest=True, echoResult=False)[0]
        if not hitObject.is_empty():
            # cyclically assign self.contactObjects
            if self.contactData['objectA'].is_empty():
                self.contactData['objectA'] = hitObject
                self.contactData['objectB'] = NodePath()
            elif self.contactData['objectB'].is_empty():
                self.contactData['objectB'] = hitObject 
            else:
                self.contactData['objectA'] = hitObject
                self.contactData['objectB'] = NodePath()                              
            # echo selected objcts
            if echoResult:
                print('Contact-Test selected objects: "' + 
                    self.contactData['objectA'].get_name() + '"' +  
                    (' - "' + self.contactData['objectB'].get_name() + '"' 
                                        if not self.contactData['objectB'].is_empty() else '')) 
    
    def contactTestProxy(self):
        '''Proxy to self.contactTest()'''
        
        return self.contactTest(self.contactData['objectA'], 
                                self.contactData['objectB'], 
                                maxContacts=self.contactData['numContacts'], 
                                echoResult=True)
        
