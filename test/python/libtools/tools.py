'''
Created on Jun 05, 2017

@author: consultit
'''

from ely.libtools import Pair_bool_float, ValueList_string, \
    ValueList_Pair_LPoint3f_int, Pair_LPoint3f_int, \
    Pair_LVector3f_ValueList_float, ValueList_float
from panda3d.core import LPoint3f, LVector3f

if __name__ == '__main__':
    print(dir(Pair_bool_float))
    p1 = Pair_bool_float()
    p1
    print(p1)
    print(dir(ValueList_string))
    # use keyword arguments to distinguish between constructors
    v0 = ValueList_string(size=10)
    print(v0)
    v1 = ValueList_string(copy=v0)
    v2 = ValueList_string(['alfa', 'beta', 'gamma', 'delta'])
    v3 = v2
    print(v2 == v3)
    for v in v2:
        print(v)
    print(v3)
    p0 = Pair_LPoint3f_int(LPoint3f(1, 1, 1), 10)
    print(p0)
    v4 = ValueList_Pair_LPoint3f_int([Pair_LPoint3f_int(LPoint3f(1, 1, 1), 10),
                                      Pair_LPoint3f_int(LPoint3f(-1, -1, -1), -10)])
    print(v4[1].second)
    p1 = Pair_LVector3f_ValueList_float(LVector3f(-1, 2, -3),
                                        ValueList_float([-1.0, 2.0, -3.0]))
    print(p1)
    # test sequence
    v5 = ValueList_string(['a', 'b', 'c', 'd'])
    print(v5.values)
    for s in v5.values:
        print(s)
