'''
Created on Sep 15, 2018

@author: consultit
'''

from panda3d.core import LPoint3f, LVector3f, NodePath
from ely.libtools import Pair_bool_float, ValueList_string, \
    ValueList_Pair_LPoint3f_int, Pair_LPoint3f_int, \
    Pair_LVector3f_ValueList_float, ValueList_float, ValueList_NodePath
import unittest
import suite

DIM = suite.DIM
MAX32 = suite.MAX32
MAX64 = suite.MAX64
MAXFLOAT = suite.MAXFLOAT
MAXDOUBLE = suite.MAXDOUBLE
RandInt32 = suite.RandInt32
RandFloat = suite.RandFloat


def setUpModule():
    pass


def tearDownModule():
    pass


class PairsTEST(unittest.TestCase):

    def setUp(self):
        print(dir(Pair_bool_float))

    def tearDown(self):
        pass

    def test(self):
        p0 = Pair_LPoint3f_int(LPoint3f(1, 1, 1), 10)
        self.assertEqual(p0, (LPoint3f(1, 1, 1), 10))
        p1 = Pair_bool_float()
        self.assertEqual(p1, (False, 0.0))
        p2 = Pair_LVector3f_ValueList_float(LVector3f(-1, 2, -3),
                                            ValueList_float([-1.0, 2.0, -3.0]))
        self.assertEqual(p2.first, LVector3f(-1, 2, -3))
        self.assertEqual(p2.second, [-1.0, 2.0, -3.0])


class ValueListsTEST(unittest.TestCase):

    def setUp(self):
        print(dir(ValueList_string))

    def tearDown(self):
        pass

    def test(self):
        # use keyword arguments to distinguish between constructors
        v0 = ValueList_string(size=10)
        self.assertEqual(len(v0), 10)
        v1 = ValueList_string(copy=v0)
        self.assertTrue(id(v1) != id(v0))
        #
        stringList = ['alfa', 'beta', 'gamma', 'delta']
        v2 = ValueList_string(stringList)
        v3 = v2
        self.assertTrue(id(v3) == id(v2))
        v3a = ValueList_string()
        v3a.assign(v2)
        self.assertFalse(id(v3a) == id(v2))
        self.assertEqual(v3a, v2)
        for v, s in zip(v2, stringList):
            self.assertEqual(v, s)
        #
        v4 = ValueList_Pair_LPoint3f_int([Pair_LPoint3f_int(LPoint3f(1, 1, 1), 10),
                                          Pair_LPoint3f_int(LPoint3f(-1, -1, -1), -10)])
        self.assertEqual(v4[1].second, -10)
        # test sequence
        charList = ['a', 'b', 'c', 'd']
        v5 = ValueList_string(charList)
        print(v5)
        for v, s in zip(v5.values, charList):
            self.assertEqual(v, s)
        # test indexing
        nodes = ValueList_NodePath(size=0)
        nodes.add_value(NodePath('1'))
        nodes.add_value(NodePath('2'))
        print(nodes)
        self.assertEqual(nodes[0].get_name(), '1')
        self.assertEqual(nodes[-1].get_name(), '2')
        # membership test
        immutableStr = ValueList_string()
        immutableStr.add_value('a')
        immutableStr.add_value('b')
        print(immutableStr)
        self.assertTrue('a' in immutableStr)
        self.assertTrue('b' in immutableStr)
        self.assertFalse('c' in immutableStr)


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTest(PairsTEST('test'))
    suite.addTest(ValueListsTEST('test'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
