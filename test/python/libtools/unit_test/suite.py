'''
Created on Aug 10, 2018

@author: consultit
'''

import numpy 
import random
random.seed()

DIM = 16
MAX32 = numpy.iinfo(numpy.int8).max
MAX64 = numpy.iinfo(numpy.int8).max
MAXFLOAT = numpy.iinfo(numpy.int8).max
MAXDOUBLE = numpy.iinfo(numpy.int8).max

def RandInt32():
    return numpy.int32(random.randrange(-MAX32, MAX32))

def RandInt64():
    return numpy.int64(random.randrange(-MAX64, MAX64))

def RandFloat():
    return numpy.float((random.random() * 2.0 - 1.0) * MAXFLOAT)

def RandDouble():
    return numpy.double((random.random() * 2.0 - 1.0) * MAXDOUBLE)

