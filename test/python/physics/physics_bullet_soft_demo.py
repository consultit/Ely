'''
Created on May 06, 2017

@author: consultit
'''

from ely.physics import GamePhysicsManager, BT3SoftBody, BT3SoftBodyMaterial, \
    BT3RigidBody, BT3SoftBodyConfig, BT3SoftBodyLJointSpecs, \
    BT3SoftBodyAJointSpecs, BT3SoftBodyAJointControl
from ely.libtools import ValueList_string
import physics_bt3_soft_demo
import common
from panda3d.core import TextureStage, TexturePool, Filename, GeomNode, \
    NodePath, LPoint3f, Geom, LVecBase3f, NurbsCurveEvaluator, RopeNode, \
    LVector3f, GeomVertexArrayFormat, InternalName, GeomVertexFormat, \
    GeomVertexData, GeomVertexRewriter, GeomTriangles, TransformState, \
    LQuaternionf, LVecBase4f, OmniBoundingVolume, ClockObject
from math import pi as M_PI, sqrt
import os
import random
random.seed()

graphicNPs = []
softBodyNP = NodePath()
ropeGeomBody = None


def UnitRand():
    return random.random()


def SignedUnitRand():
    return UnitRand() * 2 - 1


def Vector3Rand():
    p = LVecBase3f(SignedUnitRand(), SignedUnitRand(), SignedUnitRand())
    return p.normalized()


def destroyObjects():
    global graphicNPs, softBodyNP
    physicsMgr = GamePhysicsManager.get_global_ptr()
    # destroy all soft bodies
    for soft_bodyTmp in physicsMgr.get_soft_bodies():
        # destroy soft_bodyTmp
        physicsMgr.destroy_soft_body(NodePath.any_path(soft_bodyTmp))
    softBodyNP.clear()
    # remove rigid bodies but first (== sceneRigidBody)
    while physicsMgr.num_rigid_bodies > 1:
        # destroy the second one on every cycle
        physicsMgr.destroy_rigid_body(
            NodePath.any_path(physicsMgr.get_rigid_bodies()[1]))
    # remove graphic nodes
    for gNP in graphicNPs:
        gNP.remove_node()
    del graphicNPs[:]


def Ctor_RbUpStack(count, org, app, modelFile, modelAnimFiles):
    global graphicNPs
    physicsMgr = GamePhysicsManager.get_global_ptr()
    mass = 10.0
    modelNP = [common.getModelAnims('modelNP1', 1.0, 0,
                                    physics_bt3_soft_demo.playerAnimCtls, app, modelFile, modelAnimFiles),
               common.getModelAnims('modelNP2', 1.0, 1,
                                    physics_bt3_soft_demo.playerAnimCtls, app, modelFile, modelAnimFiles),
               common.getModelAnims('modelNP3', 1.0, 4,
                                    physics_bt3_soft_demo.playerAnimCtls, app, modelFile, modelAnimFiles)]
    shape = ['box', 'capsule', 'cylinder']
    axis = ['z', 'z', 'y']
    dimIdx = [0, 2, 1]
    for i in range(count):
        common.createRigidBodyXYZ(modelNP[i % 3], 'RbUpStack' + str(i),
                                  shape[i % 3], axis[i % 3], org +
                                  LPoint3f(0, 0, 2 + 12 * i),
                                  LVecBase3f(45.0 * i, -30.0 *
                                             (count - i), +60.0 * (i % 3)),
                                  LVecBase3f(8 if dimIdx[i % 3] == 0 else 2,
                                             8 if dimIdx[i % 3] == 1 else 2,
                                             8 if dimIdx[i % 3] == 2 else 2),
                                  False)
        rigidBody = physicsMgr.get_rigid_body(
            physicsMgr.num_rigid_bodies - 1)
        rigidBody.mass = mass
        rigidBody.switch_body_type(BT3RigidBody.DYNAMIC)
        graphicNPs.append(rigidBody.owner_object)


def Ctor_LinearStair(org, dims, count, modelFileIdx, shape, upAxis,
                     app, modelFile, modelAnimFiles):
    global graphicNPs
    physicsMgr = GamePhysicsManager.get_global_ptr()
    name = 'LinearStair'
    modelNP = common.getModelAnims(name, 1.0, modelFileIdx,
                                   physics_bt3_soft_demo.playerAnimCtls, app, modelFile, modelAnimFiles)
    for i in range(count):
        bodyName = 'body' + name + str(i)
        pos = org + LVecBase3f(-dims.get_x() * i, 0, dims.get_z() * i)
        common.createRigidBodyXYZ(modelNP, bodyName,
                                  shape, upAxis, pos, LVecBase3f.zero(), dims, False)
        rigidBody = physicsMgr.get_rigid_body(
            physicsMgr.num_rigid_bodies - 1)
        rigidBody.mass = 0.0
        rigidBody.switch_body_type(BT3RigidBody.STATIC)
        graphicNPs.append(rigidBody.owner_object)
        rigidBody.get_collision_object_attrs().friction = 1


def procedurallyGenerating3DModels(vertices, triangles, ntriangles,
                                   bt3vertices=True, name='gnode'):
    # # Defining your own GeomVertexFormat
    array = GeomVertexArrayFormat()
    array.add_column(InternalName.make('vertex'), 3, Geom.NT_float32,
                     Geom.C_point)
    array.add_column(InternalName.make('normal'), 3, Geom.NT_float32,
                     Geom.C_normal)
    #
    unregistered_format = GeomVertexFormat()
    unregistered_format.add_array(array)
    #
    format1 = GeomVertexFormat.register_format(unregistered_format)
    # # Creating and filling a GeomVertexData
    vdata = GeomVertexData(name + 'VD', format1, Geom.UH_static)
    #
    numVerts = 0
    i = 0
    for i in range(0, ntriangles * 3, 1):
        numVerts = max(triangles[i], numVerts)
    numVerts += 1
    vdata.set_num_rows(numVerts)
    #
    vertexRW = GeomVertexRewriter(vdata, 'vertex')
    normalRW = GeomVertexRewriter(vdata, 'normal')
    #
    if bt3vertices:
        # vertices in bullet space reset normal
        for i in range(0, numVerts * 3, 3):
            bt3vert = LPoint3f(-vertices[i], vertices[i + 2], vertices[i + 1])
            vertexRW.add_data3f(bt3vert)
            normalRW.add_data3f(LVector3f.zero())
    else:
        # vertices in panda3d space reset normal
        for i in range(0, numVerts * 3, 3):
            p3dvert = LPoint3f(vertices[i], vertices[i + 1], vertices[i + 2])
            vertexRW.add_data3f(p3dvert)
            normalRW.add_data3f(LVector3f.zero())
    # # Creating the GeomPrimitive objects and compute normals per vertex
    # # note: for each face the normal is computed and added to each normal of
    # # its vertices normals are not normalized at this stage, so a vertex
    # # normal is more influenced by triangles with wider area. All vertex
    # # normals are normalized all together in a second step.
    prim = GeomTriangles(Geom.UH_static)
    for i in range(0, ntriangles * 3, 3):
        i0 = triangles[i]
        i1 = triangles[i + 1]
        i2 = triangles[i + 2]
        prim.add_vertex(i0)
        prim.add_vertex(i1)
        prim.add_vertex(i2)
        # compute face normal (not normalized): (p1-p0)X(p2-p0)
        vertexRW.set_row(i0)
        p0 = vertexRW.get_data3f()
        vertexRW.set_row(i1)
        p1 = vertexRW.get_data3f()
        vertexRW.set_row(i2)
        p2 = vertexRW.get_data3f()
        faceNorm = (p1 - p0).cross(p2 - p0)
        # add faceNorm to each vertex normal (not normalized)
        normalRW.set_row(i0)
        n0 = normalRW.get_data3f()
        normalRW.set_row(i0)
        normalRW.set_data3f(n0 + faceNorm)
        #
        normalRW.set_row(i1)
        n1 = normalRW.get_data3f()
        normalRW.set_row(i1)
        normalRW.set_data3f(n1 + faceNorm)
        #
        normalRW.set_row(i2)
        n2 = normalRW.get_data3f()
        normalRW.set_row(i2)
        normalRW.set_data3f(n2 + faceNorm)
    prim.close_primitive()
    # normalize all vertex normals
    normalRW.set_row(0)
    for i in range(0, numVerts, 1):
        normalRW.set_row(i)
        n = LVector3f(normalRW.get_data3f())
        n.normalize()
        normalRW.set_row(i)
        normalRW.set_data3f(n)
    # Putting your new geometry in the scene graph
    geom = Geom(vdata)
    geom.add_primitive(prim)
    node = GeomNode(name)
    node.add_geom(geom)
    #
    return NodePath.any_path(node)


def elementwiseProduct(v1, v2):
    return LVecBase3f(v1[0] * v2[0], v1[1] * v2[1], v1[2] * v2[2])


def Ctor_SoftBox(p, s):
    physicsMgr = GamePhysicsManager.get_global_ptr()
    # #Hack: convex hull soft body needs any way a Geom node and take into
    # #account only its vertices.
    h = s * 0.5
    c = [
        p + elementwiseProduct(h, LVector3f(+1, -1, -1)),
        p + elementwiseProduct(h, LVector3f(-1, -1, -1)),
        p + elementwiseProduct(h, LVector3f(+1, -1, +1)),
        p + elementwiseProduct(h, LVector3f(-1, -1, +1)),
        p + elementwiseProduct(h, LVector3f(+1, +1, -1)),
        p + elementwiseProduct(h, LVector3f(-1, +1, -1)),
        p + elementwiseProduct(h, LVector3f(+1, +1, +1)),
        p + elementwiseProduct(h, LVector3f(-1, +1, +1))]
    numVert = len(c)
    vertices = [None] * (numVert * 3)
    v = 0
    for i in range(0, numVert * 3, 3):
        vertices[i] = c[v][0]
        vertices[i + 1] = c[v][1]
        vertices[i + 2] = c[v][2]
        v += 1
    # create fake triangles
    ntriangles = numVert // 3 + (1 if (numVert % 3) != 0 else 0)
    triangles = [None] * (ntriangles * 3)
    for i in range(0, ntriangles * 3, 3):
        triangles[i] = i % numVert
        triangles[i + 1] = (i + 1) % numVert
        triangles[i + 2] = (i + 2) % numVert
    # 1: the graphic object (NodePath)
    softBox = procedurallyGenerating3DModels(vertices, triangles,
                                             ntriangles, False, 'SoftBox')
    graphicNPs.append(softBox)
    graphicNPs[-1].set_pos_hpr_scale(
        LPoint3f(0.0, 0.0, 12.0),  # position
        LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
        LVecBase3f(1.0, 1.0, 1.0))  # scale
    # 2: create the empty soft body
    softBodyNP = physicsMgr.create_soft_body('SoftBoxBody')
    softBoxBody = softBodyNP.node()
    softBodyNP.set_color(LVecBase4f(Vector3Rand(), 1.0))
    softBodyNP.node().set_bounds(OmniBoundingVolume())
    softBodyNP.node().set_final(True)
    # 6: visual state: CONVEX_HULL inherits render state
#     softBodyNP.set_two_sided(True)
#     softBodyNP.show_bounds()
#     softBodyNP.set_render_mode_wireframe()
    # 3: set CONSTRUCTION PARAMETERS
    softBoxBody.group_mask = physics_bt3_soft_demo.groupMask
    softBoxBody.collide_mask = physics_bt3_soft_demo.collideMask
    softBoxBody.body_type = BT3SoftBody.CONVEX_HULL
    # 4: setup the body with graphicNPs[-1] as owner
    softBoxBody.owner_object = graphicNPs[-1]
    softBoxBody.setup()
    # CONVEX_HULL create a node with name = softBodyNP.get_name() + '_convexHull'
    # parented to softBodyNP's parent
    nodeCHNP = softBodyNP.get_parent().find(softBodyNP.get_name() + '_convexHull')
    graphicNPs.append(nodeCHNP)
    # 5: set DYNAMIC PARAMETERS
    softBoxBody.generate_bending_constraints(2)
    # cleanup
    softBox.remove_node()
    #
    return softBoxBody


def Ctor_BigPlate(mass, height, app, modelFile, modelAnimFiles):
    physicsMgr = GamePhysicsManager.get_global_ptr()
    bigPlateNP = common.getModelAnims('BigPlateNP', 1.0, 8,
                                      physics_bt3_soft_demo.playerAnimCtls, app, modelFile,
                                      modelAnimFiles)
    common.createRigidBodyXYZ(bigPlateNP, 'BigPlate', 'box', 'z',
                              LPoint3f(0, 0, height), LVecBase3f(
                                  0, 0, 0), LVecBase3f(10, 10, 2),
                              False)
    rigidBody = physicsMgr.get_rigid_body(
        physicsMgr.get_num_rigid_bodies() - 1)
    rigidBody.mass = mass
    rigidBody.switch_body_type(BT3RigidBody.DYNAMIC)
    rigidBody.collision_object_attrs.friction = 1
    graphicNPs.append(rigidBody.get_owner_object())
    return rigidBody


# callback
softBodyCBCount = 0.0
globalClock = ClockObject.get_global_clock()
countClbk = 0


def softBodyCallback(softBody):
    '''soft body update callback function'''

    global softBodyCBCount, globalClock, countClbk
    softBodyCBCount += globalClock.get_dt()
    if softBodyCBCount <= 1:
        return
    softBodyCBCount = 0
    countClbk += 1
    idx = countClbk % softBody.get_num_nodes()
    vel = softBody.get_node(idx).get_velocity()
    print(str(softBody) + '\'s node ' + str(idx) + '\'s velocity: ' +
          str(vel.length()))


def RopeGeom(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP, ropeGeomBody
    if init:
        physicsMgr = GamePhysicsManager.get_global_ptr()
        sharedTS0 = TextureStage('sharedTS0')
        ropeTex = TexturePool.load_texture(Filename('red.tga'))
        # # Rope Geom
        rope = GeomNode('SoftRopeGeom')
        graphicNPs.append(NodePath(rope))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 40.0),
            LVecBase3f(0.0, 0.0, 0.0),
            LVecBase3f(1.5, 1.5, 1.5))
        graphicNPs[-1].reparent_to(physicsMgr.get_reference_node_path())
        # Rope Geom texturing
        graphicNPs[-1].set_tex_scale(sharedTS0, 1.0, 1.0)
        graphicNPs[-1].set_texture(sharedTS0, ropeTex, 1)
        # set various creation parameters as string for other soft bodies
        common.setParametersBeforeCreation(GamePhysicsManager.SOFTBODY,
                                           'SoftRopeGeom')
        physicsMgr.set_parameter_value(GamePhysicsManager.SOFTBODY,
                                       'body_type', 'rope')
        physicsMgr.set_parameter_value(GamePhysicsManager.SOFTBODY, 'rope_points',
                                       '-15, 0, 0:15, 0, 0')
        physicsMgr.set_parameter_value(GamePhysicsManager.SOFTBODY, 'rope_fixeds',
                                       '1')
        physicsMgr.set_parameter_value(GamePhysicsManager.SOFTBODY, 'rope_res',
                                       '14')  # curve.get_num_vertices() - 2
        # create the rope soft body
        softBodyNP = physicsMgr.create_soft_body('RopeGeomBody')
        ropeGeomBody = softBodyNP.node()
        ropeGeomBody.set_update_callback(softBodyCallback)
#         softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def RopeNURBS(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        physicsMgr = GamePhysicsManager.get_global_ptr()
        sharedTS0 = TextureStage('sharedTS0')
        ropeTex = TexturePool.load_texture(Filename('iron.jpg'))
        # # Rope NURBS
        curve = NurbsCurveEvaluator()
        curve.set_order(3)
        curve.reset(16)
        print('vertices: ' + str(curve.get_num_vertices()) + '\n'
              + 'segments: ' + str(curve.get_num_segments()) + '\n'
                + 'knots: ' + str(curve.get_num_knots()))
        ropeNURBS = RopeNode('SoftRopeNURBS')
        ropeNURBS.set_curve(curve)
        ropeNURBS.set_render_mode(RopeNode.RM_tube)
        ropeNURBS.set_uv_mode(RopeNode.UV_parametric)
        ropeNURBS.set_thickness(1.0)
        graphicNPs.append(NodePath(ropeNURBS))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 40.0),
            LVecBase3f(0.0, 0.0, 0.0),
            LVecBase3f(1.5, 1.5, 1.5))
        graphicNPs[-1].reparent_to(physicsMgr.get_reference_node_path())
        # Rope NURBS texturing
        graphicNPs[-1].set_tex_scale(sharedTS0, 1.0, 1.0)
        graphicNPs[-1].set_texture(sharedTS0, ropeTex, 1)
        # set various creation parameters as string for other soft bodies
        common.setParametersBeforeCreation(GamePhysicsManager.SOFTBODY, '')
        # create the soft body (default rope)
        softBodyNP = physicsMgr.create_soft_body('RopeNURBSBody')
        ropeNURBSBody = softBodyNP.node()
        # test before setup parameters' settings
        ropeNURBSBody.body_type = BT3SoftBody.TETGEN_MESH  # error
        ropeNURBSBody.rope_points = [LPoint3f(-15, 0, 0), LPoint3f(15, 0, 0)]
# ropeNURBSBody.set_rope_res(curve.get_num_vertices() - 2) setup
# automatically
        ropeNURBSBody.set_rope_fixeds(0x0 | 0x2)
        ropeNURBSBody.owner_object = graphicNPs[0]
        ropeNURBSBody.setup()
#         softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def Init_Cloth(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_Cloth
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        cloth = GeomNode('Cloth')
        graphicNPs.append(NodePath(cloth))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 20.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        graphicNPs[-1].set_texture(
            TexturePool.load_texture(Filename(common.getFilePath('terrain.png'))))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('ClothBody')
        clothBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        clothBody.group_mask = physics_bt3_soft_demo.groupMask
        clothBody.collide_mask = physics_bt3_soft_demo.collideMask
        clothBody.body_type = BT3SoftBody.PATCH
        s = 8
        clothBody.patch_points = [
            LPoint3f(s, -s, 0),
            LPoint3f(-s, -s, 0),
            LPoint3f(s, s, 0),
            LPoint3f(-s, s, 0)]
        clothBody.patch_resxy = [31, 31]
        clothBody.patch_fixeds = 0x1 | 0x2 | 0x4 | 0x8
        clothBody.patch_gendiags = True
        # 4: setup the body with clothNP as owner
        clothBody.owner_object = graphicNPs[0]
        clothBody.setup()
        # 5: set DYNAMIC PARAMETERS
        clothBody.collision_object_attrs.shape_margin = 0.5
        material = clothBody.append_material()
        material.linear_stiffness_coefficient = 0.4
        material.flags = BT3SoftBodyMaterial.DebugDraw
        clothBody.generate_bending_constraints(2, material)
        clothBody.set_total_mass(150)
        # 6: visual state
        softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()

        # rigid bodies
        Ctor_RbUpStack(10, LPoint3f(0.0, 0.0, 20.0), app, modelFile,
                       modelAnimFiles)
    else:
        destroyObjects()


def Init_Pressure(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_Pressure
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        pressure = GeomNode('Pressure')
        graphicNPs.append(NodePath(pressure))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 0.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('PressureBody')
        pressureBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        pressureBody.group_mask = physics_bt3_soft_demo.groupMask
        pressureBody.collide_mask = physics_bt3_soft_demo.collideMask
        pressureBody.body_type = BT3SoftBody.ELLIPSOID
        pressureBody.ellipsoid_center = LPoint3f(-35, 0, 25)
        pressureBody.ellipsoid_radius = LVecBase3f(1, 1, 1) * 3
        pressureBody.ellipsoid_res = 512
        # 4: setup the body with graphicNPs[0] as owner
        pressureBody.owner_object = graphicNPs[0]
        pressureBody.setup()
        # 5: set DYNAMIC PARAMETERS
        pressureBody.get_materials()[0].linear_stiffness_coefficient = 0.1
        pressureBody.config.dynamic_friction_coefficient = 1
        pressureBody.config.damping_coefficient = 0.001
        pressureBody.config.pressure_coefficient = 2500
        pressureBody.set_total_mass(30, True)
        # 6: visual state
#        softBodyNP.set_two_sided(True)
#        softBodyNP.show_bounds()
#        softBodyNP.set_render_mode_wireframe()

        # rigid bodies
        Ctor_BigPlate(15, 10, app, modelFile, modelAnimFiles)
        Ctor_LinearStair(LPoint3f(0, 0, 3.6), LVecBase3f(4, 10, 2), 10,
                         8, 'box', 'z', app, modelFile, modelAnimFiles)
    else:
        destroyObjects()


def Init_Volume(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Volume
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        volume = GeomNode('Volume')
        graphicNPs.append(NodePath(volume))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 0.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('VolumeBody')
        volumeBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        volumeBody.group_mask = physics_bt3_soft_demo.groupMask
        volumeBody.collide_mask = physics_bt3_soft_demo.collideMask
        volumeBody.body_type = BT3SoftBody.ELLIPSOID
        volumeBody.ellipsoid_center = LPoint3f(-35, 0, 25)
        volumeBody.ellipsoid_radius = LVecBase3f(1, 1, 1) * 3
        volumeBody.ellipsoid_res = 512
        # 4: setup the body with graphicNPs[0] as owner
        volumeBody.owner_object = graphicNPs[0]
        volumeBody.setup()
        # 5: set DYNAMIC PARAMETERS
        volumeBody.get_materials()[0].linear_stiffness_coefficient = 0.45
        volumeBody.config.volume_conversation_coefficient = 20
        volumeBody.set_total_mass(50, True)
        volumeBody.set_pose(True, False)
        # 6: visual state
#        softBodyNP.set_two_sided(True)
#        softBodyNP.show_bounds()
#        softBodyNP.set_render_mode_wireframe()

        # rigid bodies
        Ctor_BigPlate(15, 10, app, modelFile, modelAnimFiles)
        Ctor_LinearStair(LPoint3f(0, 0, 3.6), LVecBase3f(4, 10, 2), 10,
                         8, 'box', 'z', app, modelFile, modelAnimFiles)
    else:
        destroyObjects()


def Init_Ropes(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Ropes
        physicsMgr = GamePhysicsManager.get_global_ptr()
        sharedTS0 = TextureStage('sharedTS0')
        ropeTex = TexturePool.load_texture(Filename('red.tga'))
        n = 15
        for i in range(n):
            # 1: the graphic object (NodePath)
            rope = GeomNode('Rope' + str(i))
            graphicNPs.append(NodePath(rope))
            graphicNPs[-1].set_pos_hpr_scale(
                LPoint3f(0.0, 0.0, 20.0),  # position
                LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
                LVecBase3f(1.0, 1.0, 1.0))  # scale
            # Rope texturing
            graphicNPs[-1].set_tex_scale(sharedTS0, 1.0, 1.0)
            graphicNPs[-1].set_texture(sharedTS0, ropeTex, 1)
            # 2: create the empty soft body
            softBodyNP = physicsMgr.create_soft_body('RopeBody' + str(i))
            ropeBody = softBodyNP.node()
            # 3: set CONSTRUCTION PARAMETERS
            ropeBody.set_group_mask(physics_bt3_soft_demo.groupMask)
            ropeBody.set_collide_mask(physics_bt3_soft_demo.collideMask)
            ropeBody.set_body_type(BT3SoftBody.ROPE)
            ropeBody.set_rope_points(
                [LPoint3f(10, i * 0.25, 0), LPoint3f(-10, i * 0.25, 0)])
            ropeBody.set_rope_res(16)
            ropeBody.set_rope_fixeds(1 | 2)
            # 4: setup the body with graphicNPs[-1] as owner
            ropeBody.owner_object = graphicNPs[-1]
            ropeBody.setup()
            # 5: set DYNAMIC PARAMETERS
            ropeBody.get_config().set_positions_solver_iterations(4)
            ropeBody.get_materials()[0].set_linear_stiffness_coefficient(
                0.1 + (i / (float)(n - 1)) * 0.9)
            ropeBody.set_total_mass(20)
            # 6: visual state
#            softBodyNP.set_two_sided(True)
#            softBodyNP.show_bounds()
#            softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def Init_RopeAttach(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_RopeAttach
        physicsMgr = GamePhysicsManager.get_global_ptr()
        sharedTS0 = TextureStage('sharedTS0')
        ropeTex = TexturePool.load_texture(Filename('yellow.tga'))

#        pdemo.m_softBodyWorldInfo.m_sparsesdf.RemoveReferences(0) TODO ?
        class Functors:

            @staticmethod
            def CtorRope(p):
                # 1: the graphic object (NodePath)
                rope = GeomNode('Rope' + str(p.get_x()))
                graphicNPs.append(NodePath(rope))
                graphicNPs[-1].set_pos_hpr_scale(
                    LPoint3f(0.0, 0.0, 0.0),  # position
                    LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
                    LVecBase3f(1.0, 1.0, 1.0))  # scale
                # Rope texturing
                graphicNPs[-1].set_tex_scale(sharedTS0, 1.0, 1.0)
                graphicNPs[-1].set_texture(sharedTS0, ropeTex, 1)
                # 2: create the empty soft body
                softBodyNP = physicsMgr.create_soft_body('RopeBody' +
                                                         str(p.get_x()))
                ropeBody = softBodyNP.node()
                # 3: set CONSTRUCTION PARAMETERS
                ropeBody.group_mask = physics_bt3_soft_demo.groupMask
                ropeBody.collide_mask = physics_bt3_soft_demo.collideMask
                ropeBody.body_type = BT3SoftBody.ROPE
                ropeBody.rope_points = [p, LPoint3f(p + LVector3f(-10, 0, 0))]
                ropeBody.rope_res = 8
                ropeBody.rope_fixeds = 1
                # 4: setup the body with graphicNPs[-1] as owner
                ropeBody.owner_object = graphicNPs[-1]
                ropeBody.setup()
                # 5: set DYNAMIC PARAMETERS
                ropeBody.set_total_mass(50)
                # 6: visual state
#                 softBodyNP.set_two_sided(True)
#                 softBodyNP.show_bounds()
#                 softBodyNP.set_render_mode_wireframe()
                return ropeBody

        # rigid bodies
        modelNP1 = common.getModelAnims('modelNP1', 1.0, 8,
                                        physics_bt3_soft_demo.playerAnimCtls,
                                        app, modelFile, modelAnimFiles)
        common.createRigidBodyXYZ(modelNP1, 'boxZ', 'box', 'z',
                                  LPoint3f(-12, 0, 8 +
                                           20), LVecBase3f(0.0, 0.0, 0.0),
                                  LVecBase3f(4, 4, 12), False)
        rigidBody = physicsMgr.get_rigid_body(
            physicsMgr.num_rigid_bodies - 1)
        # hack: change mass
        rigidBody.mass = 50
        rigidBody.switch_body_type(BT3RigidBody.DYNAMIC)
        graphicNPs.append(rigidBody.owner_object)
        #
        psb0 = Functors.CtorRope(LPoint3f(0, -1, 8 + 20))
        psb1 = Functors.CtorRope(LPoint3f(0, 1, 8 + 20))
        psb0.append_anchor(psb0.get_num_nodes() - 1, rigidBody)
        psb1.append_anchor(psb1.get_num_nodes() - 1, rigidBody)
    else:
        destroyObjects()


def Init_ClothAttach(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_ClothAttach
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        clothAttach = GeomNode('ClothAttach')
        graphicNPs.append(NodePath(clothAttach))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 0.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        graphicNPs[-1].set_texture(
            TexturePool.load_texture(Filename(common.getFilePath('terrain.png'))))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('ClothAttachBody')
        clothAttachBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        clothAttachBody.group_mask = physics_bt3_soft_demo.groupMask
        clothAttachBody.collide_mask = physics_bt3_soft_demo.collideMask
        clothAttachBody.body_type = BT3SoftBody.PATCH
        s = 4
        h = 6 + 40
        r = 9
        clothAttachBody.patch_points = [
            LPoint3f(s, -s, h),
            LPoint3f(-s, -s, h),
            LPoint3f(s, s, h),
            LPoint3f(-s, s, h),
        ]
        clothAttachBody.patch_resxy = [r, r]
        clothAttachBody.patch_fixeds = 0x4 | 0x8
        clothAttachBody.patch_gendiags = True
        # 4: setup the body with graphicNPs[0] as owner
        clothAttachBody.owner_object = graphicNPs[0]
        clothAttachBody.setup()
        # 5: set DYNAMIC PARAMETERS
        # 6: visual state
        softBodyNP.set_two_sided(True)
#        softBodyNP.show_bounds()
#        softBodyNP.set_render_mode_wireframe()

        # rigid bodies
        modelNP1 = common.getModelAnims('modelNP1', 1.0, 4,
                                        physics_bt3_soft_demo.playerAnimCtls, app, modelFile,
                                        modelAnimFiles)
        common.createRigidBodyXYZ(modelNP1, 'boxZ', 'box', 'z',
                                  LPoint3f(0, -(s + 3.5), h -
                                           1), LVecBase3f(0.0, 0.0, 0.0),
                                  LVecBase3f(2 * s, 6, 2), False)
        rigidBody = physicsMgr.get_rigid_body(physicsMgr.num_rigid_bodies - 1)
        # hack: change mass
        rigidBody.mass = 20
        rigidBody.switch_body_type(BT3RigidBody.DYNAMIC)
        graphicNPs.append(rigidBody.owner_object)
        #
        clothAttachBody.append_anchor(0, rigidBody)
        clothAttachBody.append_anchor(r - 1, rigidBody)

    else:
        destroyObjects()


def Init_Sticks(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Sticks
        physicsMgr = GamePhysicsManager.get_global_ptr()
        sharedTS0 = TextureStage('sharedTS0')
        ropeTex = TexturePool.load_texture(Filename('red.tga'))
        n = 16
        sg = 4
        sz = 5
        hg = 4
        in0 = 1 / (float)(n - 1)
        for y in range(n):
            for x in range(n):
                org = LPoint3f(-(-sz + sz * 2 * x * in0),
                               -sz + sz * 2 * y * in0, 0)
                # 1: the graphic object (NodePath)
                rope = GeomNode('Rope' + str(y) + str(x))
                graphicNPs.append(NodePath(rope))
                graphicNPs[-1].set_pos_hpr_scale(
                    LPoint3f(0.0, 0.0, 0.0),  # position
                    LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
                    LVecBase3f(1.0, 1.0, 1.0))  # scale
                # Rope texturing
                graphicNPs[-1].set_tex_scale(sharedTS0, 1.0, 1.0)
                graphicNPs[-1].set_texture(sharedTS0, ropeTex, 1)
                # 2: create the empty soft body
                softBodyNP = physicsMgr.create_soft_body('RopeBody' + str(y) +
                                                         str(x))
                ropeBody = softBodyNP.node()
                # 3: set CONSTRUCTION PARAMETERS
                ropeBody.group_mask = physics_bt3_soft_demo.groupMask
                ropeBody.collide_mask = physics_bt3_soft_demo.collideMask
                ropeBody.body_type = BT3SoftBody.ROPE
                ropeBody.rope_points = [
                    org, LPoint3f(org + LVector3f(-(hg * 0.001), 0, hg))]
                ropeBody.rope_res = sg
                ropeBody.rope_fixeds = 1
                # 4: setup the body with graphicNPs[-1] as owner
                ropeBody.owner_object = graphicNPs[-1]
                ropeBody.setup()
                # 5: set DYNAMIC PARAMETERS
                ropeBody.get_config().damping_coefficient = 0.005
                ropeBody.get_config().rigid_contacts_hardness = 0.1
                for i in range(3):
                    ropeBody.generate_bending_constraints(2 + i)
                ropeBody.set_mass(1, 0)
                ropeBody.set_total_mass(0.01)
                # 6: visual state
#                softBodyNP.set_two_sided(True)
#                softBodyNP.show_bounds()
#                softBodyNP.set_render_mode_wireframe()
        # rigid bodies: Ctor_BigBall
        modelNP1 = common.getModelAnims('modelNP1', 1.0, 3,
                                        physics_bt3_soft_demo.playerAnimCtls, app, modelFile,
                                        modelAnimFiles)
        common.createRigidBodyXYZ(modelNP1, 'sphereZ', 'sphere', 'z',
                                  LPoint3f(0, 0, 13), LVecBase3f(
                                      0.0, 0.0, 0.0),
                                  LVecBase3f(3, 3, 3), False)
        rigidBody = physicsMgr.get_rigid_body(
            physicsMgr.num_rigid_bodies - 1)
        # hack: change mass
        rigidBody.mass = 10
        rigidBody.switch_body_type(BT3RigidBody.DYNAMIC)
        graphicNPs.append(rigidBody.owner_object)
    else:
        destroyObjects()


def Init_CapsuleCollision(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_CapsuleCollision
        physicsMgr = GamePhysicsManager.get_global_ptr()
        sharedTS0 = TextureStage('sharedTS0')
        patchTex = TexturePool.load_texture(Filename('heightfield.png'))
        s = 4
        h = 6
        r = 20
        fixed = 0  # 4|8
        # 1: the graphic object (NodePath)
        cloth = GeomNode('Cloth')
        graphicNPs.append(NodePath(cloth))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 10.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        # Rope texturing
        graphicNPs[-1].set_tex_scale(sharedTS0, 2.0, 2.0)
        graphicNPs[-1].set_texture(sharedTS0, patchTex, 1)
        graphicNPs[-1].set_texture(
            TexturePool.load_texture(Filename(common.getFilePath('terrain.png'))))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('ClothBody')
        clothBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        clothBody.group_mask = physics_bt3_soft_demo.groupMask
        clothBody.collide_mask = physics_bt3_soft_demo.collideMask
        clothBody.body_type = BT3SoftBody.PATCH
        clothBody.patch_points = [
            LPoint3f(s, -s, h),
            LPoint3f(-s, -s, h),
            LPoint3f(s, s, h),
            LPoint3f(-s, s, h), ]
        clothBody.patch_resxy = [r, r]
        clothBody.patch_fixeds = fixed
        clothBody.patch_gendiags = True
        # 4: setup the body with graphicNPs[0] as owner
        clothBody.owner_object = graphicNPs[0]
        clothBody.setup()
        # 5: set DYNAMIC PARAMETERS
        clothBody.set_total_mass(0.1)
        clothBody.config.positions_solver_iterations = 10
        clothBody.config.cluster_solver_iterations = 10
        clothBody.config.drift_solver_iterations = 10
#        clothBody.config.velocities_solver_iterations = 10
        # 6: visual state
        softBodyNP.set_two_sided(True)
#        softBodyNP.show_bounds()
#        softBodyNP.set_render_mode_wireframe()

        # rigid bodies
        modelNP1 = common.getModelAnims('modelNP1', 1.0, 4,
                                        physics_bt3_soft_demo.playerAnimCtls,
                                        app, modelFile, modelAnimFiles)
        common.createRigidBodyXYZ(modelNP1, 'capsuleX', 'capsule', 'x',
                                  LPoint3f(0, 0, h - 2 +
                                           10), LVecBase3f(0.0, 0.0, 0.0),
                                  LVecBase3f(5 + 2, 2, 2), False)  # total height == height+2*radius
        rigidBody = physicsMgr.get_rigid_body(
            physicsMgr.num_rigid_bodies - 1)
        # hack: change mass
        rigidBody.mass = 0
        rigidBody.switch_body_type(BT3RigidBody.STATIC)
        graphicNPs.append(rigidBody.owner_object)
        rigidBody.collision_object_attrs.shape_margin = 0.5
        rigidBody.collision_object_attrs.friction = 0.8
#        clothBody.append_anchor(0,rigidBody)
#        clothBody.append_anchor(r-1 ,rigidBody)
    else:
        destroyObjects()


from TorusMesh import gVertices, gIndices, NUM_TRIANGLES


def Init_Collide(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Collide
        physicsMgr = GamePhysicsManager.get_global_ptr()

        class Functor:

            @staticmethod
            def Create(x, a, s=LVecBase3f(1, 1, 1)):
                # 1: the graphic object (NodePath)
                nameSuffix = str(random.random())
                triMesh = procedurallyGenerating3DModels(gVertices,
                                                         gIndices, NUM_TRIANGLES, True, 'TriMesh' + nameSuffix)
                graphicNPs.append(triMesh)
#                 graphicNPs[-1].set_pos_hpr_scale(
#                         x,#position
#                         a,#rotation (h,p,r)
#                         s)#scale
                # model coloring
                graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
                # 2: create the empty soft body
                softBodyNP = physicsMgr.create_soft_body('TriMeshBody' +
                                                         nameSuffix)
                triMeshBody = softBodyNP.node()
                # 3: set CONSTRUCTION PARAMETERS
                triMeshBody.group_mask = physics_bt3_soft_demo.groupMask
                triMeshBody.collide_mask = physics_bt3_soft_demo.collideMask
                triMeshBody.body_type = BT3SoftBody.TRIANGLE_MESH
                # 4: setup the body with graphicNPs[-1] as owner
                triMeshBody.owner_object = graphicNPs[-1]
                triMeshBody.setup()
                # 5: set DYNAMIC PARAMETERS
                triMeshBody.generate_bending_constraints(2)
                triMeshBody.config.positions_solver_iterations = 2
                flags = triMeshBody.config.collisions_flags
                triMeshBody.config.collisions_flags = flags | \
                    BT3SoftBodyConfig.Vertex_Face_Soft_Soft
                triMeshBody.randomize_constraints()
                m = TransformState.make_pos_hpr(x, a)
                triMeshBody.transform(m)
                triMeshBody.scale(s)
                triMeshBody.set_total_mass(50, True)
                # 6: visual state
#                softBodyNP.set_two_sided(True)
#                softBodyNP.show_bounds()
#                softBodyNP.set_render_mode_wireframe()
                return triMeshBody

        #
        for i in range(3):
            Functor.Create(LPoint3f(-2.9 + 2.9 * i, 0, 8),
                           LVecBase3f(0, 90 - i * 90, 0), 2)
    else:
        destroyObjects()


from BunnyMesh import gVerticesBunny, gIndicesBunny, BUNNY_NUM_TRIANGLES


def Init_Collide2(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Collide2
        physicsMgr = GamePhysicsManager.get_global_ptr()

        class Functor:

            @staticmethod
            def Create(x, a, s=LVecBase3f(1, 1, 1)):
                # 1: the graphic object (NodePath)
                nameSuffix = str(random.random())
                triMesh = procedurallyGenerating3DModels(gVerticesBunny,
                                                         gIndicesBunny, BUNNY_NUM_TRIANGLES, True, 'TriMesh' +
                                                         nameSuffix)
                graphicNPs.append(triMesh)
#                graphicNPs[-1].set_pos_hpr_scale(
#                        x,#position
#                        a,#rotation (h,p,r)
#                        s)#scale
                # model coloring
                graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
                # 2: create the empty soft body
                softBodyNP = physicsMgr.create_soft_body('TriMeshBody' +
                                                         nameSuffix)
                triMeshBody = softBodyNP.node()
                # 3: set CONSTRUCTION PARAMETERS
                triMeshBody.group_mask = physics_bt3_soft_demo.groupMask
                triMeshBody.collide_mask = physics_bt3_soft_demo.collideMask
                triMeshBody.body_type = BT3SoftBody.TRIANGLE_MESH
                # 4: setup the body with graphicNPs[-1] as owner
                triMeshBody.owner_object = graphicNPs[-1]
                triMeshBody.setup()
                # 5: set DYNAMIC PARAMETERS
                pm = triMeshBody.append_material()
                pm.linear_stiffness_coefficient = 0.5
                pm.flags = pm.flags - BT3SoftBodyMaterial.DebugDraw
                triMeshBody.generate_bending_constraints(2, pm)
                triMeshBody.config.positions_solver_iterations = 2
                triMeshBody.config.dynamic_friction_coefficient = 0.5
                flags = triMeshBody.config.collisions_flags
                triMeshBody.config.collisions_flags = flags | \
                    BT3SoftBodyConfig.Vertex_Face_Soft_Soft
                triMeshBody.randomize_constraints()
                m = TransformState.make_pos_hpr(x, a)
                triMeshBody.transform(m)
                triMeshBody.scale(s)
                triMeshBody.set_total_mass(100, True)
                # 6: visual state
#                softBodyNP.set_two_sided(True)
#                softBodyNP.show_bounds()
#                softBodyNP.set_render_mode_wireframe()
                return triMeshBody

        #
        for i in range(3):
            Functor.Create(LPoint3f(0, 0, -1 + 5 * i + 2.0),
                           LVecBase3f(45 - i * 45, 0, 0), 6)
    else:
        destroyObjects()


def Init_Collide3(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Collide3
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        clothAttach = GeomNode('ClothCollide31')
        graphicNPs.append(NodePath(clothAttach))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 15.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        graphicNPs[-1].set_texture(TexturePool.load_texture(Filename(
            common.getFilePath('heightfield.png'))))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('clothCollideBody31')
        clothCollideBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        clothCollideBody.group_mask = physics_bt3_soft_demo.groupMask
        clothCollideBody.collide_mask = physics_bt3_soft_demo.collideMask
        clothCollideBody.body_type = BT3SoftBody.PATCH
        s = 8
        clothCollideBody.patch_points = [
            LPoint3f(s, -s, 0),
            LPoint3f(-s, -s, 0),
            LPoint3f(s, s, 0),
            LPoint3f(-s, s, 0), ]
        clothCollideBody.patch_resxy = [15, 15]
        clothCollideBody.patch_fixeds = 0x1 | 0x2 | 0x4 | 0x8
        clothCollideBody.patch_gendiags = True
        # 4: setup the body with graphicNPs[-1] as owner
        clothCollideBody.owner_object = graphicNPs[-1]
        clothCollideBody.setup()
        # 5: set DYNAMIC PARAMETERS
        clothCollideBody.get_materials(
        )[0].set_linear_stiffness_coefficient(0.4)
        flags = clothCollideBody.config.collisions_flags
        clothCollideBody.config.collisions_flags = flags | \
            BT3SoftBodyConfig.Vertex_Face_Soft_Soft
        clothCollideBody.set_total_mass(150)
        # 6: visual state
        softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()
        #
        # 1: the graphic object (NodePath)
        clothAttach = GeomNode('ClothCollide32')
        graphicNPs.append(NodePath(clothAttach))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 15.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        graphicNPs[-1].set_texture(TexturePool.load_texture(Filename(
            common.getFilePath('terrain.png'))))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('clothCollideBody32')
        clothCollideBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        clothCollideBody.group_mask = physics_bt3_soft_demo.groupMask
        clothCollideBody.collide_mask = physics_bt3_soft_demo.collideMask
        clothCollideBody.body_type = BT3SoftBody.PATCH
        s = 4
        o = LVector3f(-5, 0, 10)
        clothCollideBody.patch_points = [
            LPoint3f(s, -s, 0) + o,
            LPoint3f(-s, -s, 0) + o,
            LPoint3f(s, s, 0) + o,
            LPoint3f(-s, s, 0) + o, ]
        clothCollideBody.patch_resxy = [7, 7]
        clothCollideBody.patch_fixeds = 0x0
        clothCollideBody.patch_gendiags = True
        # 4: setup the body with graphicNPs[-1] as owner
        clothCollideBody.owner_object = graphicNPs[-1]
        clothCollideBody.setup()
        # 5: set DYNAMIC PARAMETERS
        pm = clothCollideBody.append_material()
        pm.linear_stiffness_coefficient = 0.1
        pm.flags = pm.get_flags() - BT3SoftBodyMaterial.DebugDraw
        clothCollideBody.generate_bending_constraints(2, pm)
        clothCollideBody.get_materials()[0].linear_stiffness_coefficient = 0.5
        flags = clothCollideBody.config.collisions_flags
        clothCollideBody.get_config().collisions_flags = flags | \
            BT3SoftBodyConfig.Vertex_Face_Soft_Soft
        clothCollideBody.set_total_mass(150)
        # 6: visual state
        softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def Init_Impact(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Impact
        physicsMgr = GamePhysicsManager.get_global_ptr()
        sharedTS0 = TextureStage('sharedTS0')
        ropeTex = TexturePool.load_texture(Filename('red.tga'))
        # 1: the graphic object (NodePath)
        rope = GeomNode('RopeImpact')
        graphicNPs.append(NodePath(rope))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 10.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        # Rope texturing
        graphicNPs[-1].set_tex_scale(sharedTS0, 1.0, 1.0)
        graphicNPs[-1].set_texture(sharedTS0, ropeTex, 1)
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('RopeBodyImpact')
        ropeBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        ropeBody.group_mask = physics_bt3_soft_demo.groupMask
        ropeBody.collide_mask = physics_bt3_soft_demo.collideMask
        ropeBody.body_type = BT3SoftBody.ROPE
        ropeBody.rope_points = [LPoint3f(0, 0, 0), LPoint3f(0, 0, -1)]
        ropeBody.rope_res = 0
        ropeBody.rope_fixeds = 1
        # 4: setup the body with graphicNPs[-1] as owner
        ropeBody.owner_object = graphicNPs[-1]
        ropeBody.setup()
        # 5: set DYNAMIC PARAMETERS
        ropeBody.config.rigid_contacts_hardness = 0.5
        # 6: visual state
#         softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()

        # rigid bodies
        modelNP1 = common.getModelAnims('modelNP1', 1.0, 8,
                                        physics_bt3_soft_demo.playerAnimCtls,
                                        app, modelFile, modelAnimFiles)
        common.createRigidBodyXYZ(modelNP1, 'boxZ', 'box', 'z',
                                  LPoint3f(
                                      0, 0, 20 + 10), LVecBase3f(0.0, 0.0, 0.0),
                                  LVecBase3f(4, 4, 4), False)
        rigidBody = physicsMgr.get_rigid_body(
            physicsMgr.num_rigid_bodies - 1)
        # hack: change mass
        rigidBody.mass = 10
        rigidBody.switch_body_type(BT3RigidBody.DYNAMIC)
        graphicNPs.append(rigidBody.owner_object)
    else:
        destroyObjects()


def Init_Aero(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Aero
        physicsMgr = GamePhysicsManager.get_global_ptr()
        s = 2
        h = 10
        segments = 6
        count = 50
        for i in range(count):
            # 1: the graphic object (NodePath)
            clothAero = GeomNode('ClothAero' + str(i))
            graphicNPs.append(NodePath(clothAero))
            graphicNPs[-1].set_pos_hpr_scale(
                LPoint3f(0.0, 0.0, 0.0),  # position
                LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
                LVecBase3f(1.0, 1.0, 1.0))  # scale
            graphicNPs[-1].set_texture(TexturePool.load_texture(Filename(
                common.getFilePath('heightfield.png'))))
            # 2: create the empty soft body
            softBodyNP = physicsMgr.create_soft_body('ClothAeroBody' + str(i))
            clothAeroBody = softBodyNP.node()
            # 3: set CONSTRUCTION PARAMETERS
            clothAeroBody.group_mask = physics_bt3_soft_demo.groupMask
            clothAeroBody.collide_mask = physics_bt3_soft_demo.collideMask
            clothAeroBody.body_type = BT3SoftBody.PATCH
            clothAeroBody.patch_points = [
                LPoint3f(s, -s, h),
                LPoint3f(-s, -s, h),
                LPoint3f(s, s, h),
                LPoint3f(-s, s, h), ]
            clothAeroBody.patch_resxy = [segments, segments]
            clothAeroBody.patch_fixeds = 0x0
            clothAeroBody.patch_gendiags = True
            # 4: setup the body with graphicNPs[-1] as owner
            clothAeroBody.owner_object = graphicNPs[-1]
            clothAeroBody.setup()
            # 5: set DYNAMIC PARAMETERS
            pm = clothAeroBody.append_material()
            pm.flags = pm.get_flags() - BT3SoftBodyMaterial.DebugDraw
            clothAeroBody.generate_bending_constraints(2, pm)
            clothAeroBody.config.lift_coefficient = 0.004
            clothAeroBody.config.drag_coefficient = 0.0003
            clothAeroBody.config.aerodynamic_model = BT3SoftBodyConfig.Vertex_TwoSided
            rot = LQuaternionf()
            ra = Vector3Rand() * 0.1
            rp = Vector3Rand() * 15 + LVecBase3f(0, 80, 20)
            rot.set_hpr(LVecBase3f(-(M_PI / 8 + ra.get_x()) * (180.0 / M_PI),
                                   ra.get_y() * (180.0 / M_PI),
                                   (-M_PI / 7 + ra.get_z()) * (180.0 / M_PI)))
            trs = TransformState.make_pos_quat_scale(rp, rot, 1)
            clothAeroBody.transform(trs)
            clothAeroBody.set_total_mass(0.1)
            clothAeroBody.add_force(LVector3f(0, 0, 2))
            # 6: visual state
            softBodyNP.set_two_sided(True)
#             softBodyNP.show_bounds()
#             softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def Init_Aero2(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Aero2
        physicsMgr = GamePhysicsManager.get_global_ptr()
        s = 5
        segments = 10
        count = 5
        pos = LVector3f(s * segments, 0, 0) + LVector3f(0, 0, 20)
        gap = 0.5
        for i in range(count):
            # 1: the graphic object (NodePath)
            clothAero = GeomNode('ClothAero2' + str(i))
            graphicNPs.append(NodePath(clothAero))
            graphicNPs[-1].set_pos_hpr_scale(
                LPoint3f(0.0, 0.0, 0.0),  # position
                LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
                LVecBase3f(1.0, 1.0, 1.0))  # scale
            graphicNPs[-1].set_texture(TexturePool.load_texture(Filename(
                common.getFilePath('flag_oga.png'))))
            # 2: create the empty soft body
            softBodyNP = physicsMgr.create_soft_body('ClothAero2Body' + str(i))
            clothAeroBody = softBodyNP.node()
            # 3: set CONSTRUCTION PARAMETERS
            clothAeroBody.group_mask = physics_bt3_soft_demo.groupMask
            clothAeroBody.collide_mask = physics_bt3_soft_demo.collideMask
            clothAeroBody.body_type = BT3SoftBody.PATCH
            clothAeroBody.patch_points = [
                LPoint3f(s, -s * 3, 0),
                LPoint3f(-s, -s * 3, 0),
                LPoint3f(s, s, 0),
                LPoint3f(-s, s, 0), ]
            clothAeroBody.patch_resxy = [segments, segments * 3]
            clothAeroBody.patch_fixeds = 0x1 | 0x2
            clothAeroBody.patch_gendiags = True
            # 4: setup the body with graphicNPs[-1] as owner
            clothAeroBody.owner_object = graphicNPs[-1]
            clothAeroBody.setup()
            # 5: set DYNAMIC PARAMETERS
            clothAeroBody.collision_object_attrs.shape_margin = 0.5
            pm = clothAeroBody.append_material()
            pm.linear_stiffness_coefficient = 0.0004
            pm.flags = pm.get_flags() - BT3SoftBodyMaterial.DebugDraw
            clothAeroBody.generate_bending_constraints(2, pm)
            clothAeroBody.config.lift_coefficient = 0.05
            clothAeroBody.config.drag_coefficient = 0.01
            clothAeroBody.config.positions_solver_iterations = 2
            clothAeroBody.config.aerodynamic_model = BT3SoftBodyConfig.Vertex_TwoSidedLiftDrag
            clothAeroBody.wind_velocity = LVector3f(-4, -25.0, -12.0)
            rot = LQuaternionf()
            pos += LVector3f(-(s * 2 + gap), 0, 0)
            rot.set_from_axis_angle_rad(M_PI / 2, LVector3f(-1, 0, 0))
            trs = TransformState.make_pos_quat_scale(pos, rot, 1)
            clothAeroBody.transform(trs)
            clothAeroBody.set_total_mass(2.0)
            clothAeroBody.reoptimize_link_order()
            # 6: visual state
            softBodyNP.set_two_sided(True)
#             softBodyNP.show_bounds()
#             softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def Init_Friction(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Friction
        bs = float(2)
        ts = bs + bs / 4.0
        ni = 20
        for i in range(ni):
            p = LPoint3f(-(-ni * ts / 2 + i * ts), 40 + 50, -10 + bs)
            psb = Ctor_SoftBox(p, LVector3f(-bs, bs, bs))
            psb.config.dynamic_friction_coefficient = 0.1 * \
                ((i + 1) / float(ni))
            psb.add_velocity(LVecBase3f(0, -10, 0))
    else:
        destroyObjects()


def Init_Torus(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Torus
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        triMesh = procedurallyGenerating3DModels(gVertices,
                                                 gIndices, NUM_TRIANGLES, True, 'Torus')
        graphicNPs.append(triMesh)
#         graphicNPs[-1].set_pos_hpr_scale(
#                 LPoint3f(0.0, 0.0, 0.0),#position
#                 LVecBase3f(0.0, 0.0, 0.0),#rotation (h,p,r)
#                 LVecBase3f(1.0, 1.0, 1.0))#scale
        # model coloring
        graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('TorusBody')
        triMeshBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        triMeshBody.group_mask = physics_bt3_soft_demo.groupMask
        triMeshBody.collide_mask = physics_bt3_soft_demo.collideMask
        triMeshBody.body_type = BT3SoftBody.TRIANGLE_MESH
        # 4: setup the body with graphicNPs[-1] as owner
        triMeshBody.owner_object = graphicNPs[-1]
        triMeshBody.setup()
        # 5: set DYNAMIC PARAMETERS
        triMeshBody.generate_bending_constraints(2)
        triMeshBody.config.positions_solver_iterations = 2
        triMeshBody.randomize_constraints()
        hpr = LVecBase3f(0, (-M_PI / 2.0) * (180.0 / M_PI), 0)
        m = TransformState.make_pos_hpr(LPoint3f(0, 0, 4 + 10), hpr)
        triMeshBody.transform(m)
        triMeshBody.scale(2)
        triMeshBody.set_total_mass(50, True)
        # 6: visual state
#         softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def Init_TorusMatch(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_TorusMatch
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        triMesh = procedurallyGenerating3DModels(gVertices,
                                                 gIndices, NUM_TRIANGLES, True, 'TorusMatch')
        graphicNPs.append(triMesh)
#         graphicNPs[-1].set_pos_hpr_scale(
#                 LPoint3f(0.0, 0.0, 0.0),#position
#                 LVecBase3f(0.0, 0.0, 0.0),#rotation (h,p,r)
#                 LVecBase3f(1.0, 1.0, 1.0))#scale
        # model coloring
        graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('TorusMatchBody')
        triMeshBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        triMeshBody.group_mask = physics_bt3_soft_demo.groupMask
        triMeshBody.collide_mask = physics_bt3_soft_demo.collideMask
        triMeshBody.body_type = BT3SoftBody.TRIANGLE_MESH
        # 4: setup the body with graphicNPs[-1] as owner
        triMeshBody.owner_object = graphicNPs[-1]
        triMeshBody.setup()
        # 5: set DYNAMIC PARAMETERS
        triMeshBody.get_materials()[0].linear_stiffness_coefficient = 0.1
        triMeshBody.config.pose_matching_coefficient = 0.05
        triMeshBody.randomize_constraints()
        hpr = LVecBase3f(0, -M_PI / 2.0 * (180.0 / M_PI), 0)
        m = TransformState.make_pos_hpr(LPoint3f(0, 0, 4 + 10), hpr)
        triMeshBody.transform(m)
        triMeshBody.scale(2)
        triMeshBody.set_total_mass(50, True)
        triMeshBody.set_pose(False, True)
        # 6: visual state
#         softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def Init_Bunny(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_Bunny
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        triMesh = procedurallyGenerating3DModels(gVerticesBunny,
                                                 gIndicesBunny, BUNNY_NUM_TRIANGLES, True, 'Bunny')
        graphicNPs.append(triMesh)
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 3.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        # model coloring
        graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('BunnyBody')
        triMeshBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        triMeshBody.group_mask = physics_bt3_soft_demo.groupMask
        triMeshBody.collide_mask = physics_bt3_soft_demo.collideMask
        triMeshBody.body_type = BT3SoftBody.TRIANGLE_MESH
        # 4: setup the body with graphicNPs[-1] as owner
        triMeshBody.owner_object = graphicNPs[-1]
        triMeshBody.setup()
        # 5: set DYNAMIC PARAMETERS
        pm = triMeshBody.append_material()
        pm.linear_stiffness_coefficient = 0.5
        pm.flags = pm.get_flags() - BT3SoftBodyMaterial.DebugDraw
        triMeshBody.generate_bending_constraints(2, pm)
        triMeshBody.config.positions_solver_iterations = 2
        triMeshBody.config.dynamic_friction_coefficient = 0.5
        triMeshBody.randomize_constraints()
        triMeshBody.scale(6)
        triMeshBody.set_total_mass(100, True)
        # 6: visual state
#         softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def Init_BunnyMatch(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_BunnyMatch
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        triMesh = procedurallyGenerating3DModels(gVerticesBunny,
                                                 gIndicesBunny, BUNNY_NUM_TRIANGLES, True, 'BunnyMatch')
        graphicNPs.append(triMesh)
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 3.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        # model coloring
        graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('BunnyMatchBody')
        triMeshBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        triMeshBody.group_mask = physics_bt3_soft_demo.groupMask
        triMeshBody.collide_mask = physics_bt3_soft_demo.collideMask
        triMeshBody.body_type = BT3SoftBody.TRIANGLE_MESH
        # 4: setup the body with graphicNPs[-1] as owner
        triMeshBody.owner_object = graphicNPs[-1]
        triMeshBody.setup()
        # 5: set DYNAMIC PARAMETERS
        triMeshBody.config.dynamic_friction_coefficient = 0.5
        triMeshBody.config.pose_matching_coefficient = 0.05
        triMeshBody.config.positions_solver_iterations = 5
        triMeshBody.randomize_constraints()
        triMeshBody.scale(6)
        triMeshBody.set_total_mass(100, True)
        triMeshBody.set_pose(False, True)
        # 6: visual state
#         softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def Init_Cutting1(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_Cutting1
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        cloth = GeomNode('Cutting1')
        graphicNPs.append(NodePath(cloth))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 20.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        graphicNPs[-1].set_texture(TexturePool.load_texture(Filename(
            common.getFilePath('terrain.png'))))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('Cutting1Body')
        clothBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        clothBody.group_mask = physics_bt3_soft_demo.groupMask
        clothBody.collide_mask = physics_bt3_soft_demo.collideMask
        clothBody.body_type = BT3SoftBody.PATCH
        s = 6
        h = 2
        r = 16
        p = [
            LPoint3f(-s, -s, h),
            LPoint3f(+s, -s, h),
            LPoint3f(-s, +s, h),
            LPoint3f(+s, +s, h)]
        clothBody.patch_points = [p[0], p[1], p[2], p[3]]
        clothBody.patch_resxy = [r, r]
        clothBody.patch_fixeds = 0x1 | 0x2 | 0x4 | 0x8
        clothBody.patch_gendiags = True
        # 4: setup the body with clothNP as owner
        clothBody.owner_object = graphicNPs[0]
        clothBody.setup()
        # 5: set DYNAMIC PARAMETERS
        clothBody.config.positions_solver_iterations = 1
        # 6: visual state
        softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


def Ctor_ClusterTorus(x, a, s=LVecBase3f(2, 2, 2)):
    physicsMgr = GamePhysicsManager.get_global_ptr()
    # 1: the graphic object (NodePath)
    triMesh = procedurallyGenerating3DModels(gVertices,
                                             gIndices, NUM_TRIANGLES, True, 'Torus')
    graphicNPs.append(triMesh)
#     graphicNPs[-1].set_pos_hpr_scale(
#             LPoint3f(0.0, 0.0, 0.0),#position
#             LVecBase3f(0.0, 0.0, 0.0),#rotation (h,p,r)
#             LVecBase3f(1.0, 1.0, 1.0))#scale
    # model coloring
    graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
    # 2: create the empty soft body
    softBodyNP = physicsMgr.create_soft_body('TorusBody')
    triMeshBody = softBodyNP.node()
    # 3: set CONSTRUCTION PARAMETERS
    triMeshBody.group_mask = physics_bt3_soft_demo.groupMask
    triMeshBody.collide_mask = physics_bt3_soft_demo.collideMask
    triMeshBody.body_type = BT3SoftBody.TRIANGLE_MESH
    # 4: setup the body with graphicNPs[-1] as owner
    triMeshBody.owner_object = graphicNPs[-1]
    triMeshBody.setup()
    # 5: set DYNAMIC PARAMETERS
    pm = triMeshBody.append_material()
    pm.set_linear_stiffness_coefficient(1)
    pm.flags = pm.flags - BT3SoftBodyMaterial.DebugDraw
    triMeshBody.generate_bending_constraints(2, pm)
    triMeshBody.config.positions_solver_iterations = 2
    triMeshBody.config.collisions_flags = BT3SoftBodyConfig.Cluster_Soft_Soft | \
        BT3SoftBodyConfig.Cluster_Rigid_Soft
    triMeshBody.randomize_constraints()
    triMeshBody.scale(s)
    q = LQuaternionf()
    q.set_hpr(LVecBase3f(-a[0], a[2], a[1]))
    triMeshBody.rotate(q)
    triMeshBody.translate(x)
    triMeshBody.set_total_mass(50, True)
    triMeshBody.generate_clusters(64)
    # 6: visual state
#     softBodyNP.set_two_sided(True)
#     softBodyNP.show_bounds()
#     softBodyNP.set_render_mode_wireframe()
    return triMeshBody


def Init_ClusterDeform(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_ClusterDeform
        torus = Ctor_ClusterTorus(LPoint3f(0, 0, 10),
                                  LVecBase3f(-M_PI / 2.0 * (180.0 / M_PI),
                                             M_PI / 2.0 * (180.0 / M_PI),
                                             0))
        torus.generate_clusters(8)
        torus.config.dynamic_friction_coefficient = 1
    else:
        destroyObjects()


def Init_ClusterCollide1(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_ClusterCollide1
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        cloth = GeomNode('ClusterCollide1')
        graphicNPs.append(NodePath(cloth))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 20.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        graphicNPs[-1].set_texture(TexturePool.load_texture(Filename(
            common.getFilePath('terrain.png'))))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('ClusterCollide1Body')
        clothBody = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        clothBody.group_mask = physics_bt3_soft_demo.groupMask
        clothBody.collide_mask = physics_bt3_soft_demo.collideMask
        clothBody.body_type = BT3SoftBody.PATCH
        s = 8
        clothBody.patch_points = [
            LPoint3f(s, -s, 0),
            LPoint3f(-s, -s, 0),
            LPoint3f(s, s, 0),
            LPoint3f(-s, s, 0)]
        clothBody.patch_resxy = [17, 17]
        clothBody.patch_fixeds = 0x1 | 0x2 | 0x4 | 0x8
        clothBody.patch_gendiags = True
        # 4: setup the body with clothNP as owner
        clothBody.owner_object = graphicNPs[0]
        clothBody.setup()
        # 5: set DYNAMIC PARAMETERS
        pm = clothBody.append_material()
        pm.linear_stiffness_coefficient = 0.4
        pm.flags = pm.flags - BT3SoftBodyMaterial.DebugDraw
        clothBody.config.dynamic_friction_coefficient = 1
        clothBody.config.soft_rigid_hardness = 1
        clothBody.config.soft_rigid_impulse_split = 0
        clothBody.config.collisions_flags = BT3SoftBodyConfig.Cluster_Soft_Soft | \
            BT3SoftBodyConfig.Cluster_Rigid_Soft
        clothBody.generate_bending_constraints(2, pm)
        clothBody.collision_object_attrs.shape_margin = 0.05
        clothBody.set_total_mass(50)
        # #pass zero in generateClusters to create  cluster for each tetrahedron or triangle
        clothBody.generate_clusters(0)
        # 6: visual state
        softBodyNP.set_two_sided(True)
#         softBodyNP.show_bounds()
#         softBodyNP.set_render_mode_wireframe()

        # rigid bodies
        Ctor_RbUpStack(10, LPoint3f(0.0, 0.0, 20.0), app, modelFile,
                       modelAnimFiles)
    else:
        destroyObjects()


def Init_ClusterCollide2(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_ClusterCollide2
        physicsMgr = GamePhysicsManager.get_global_ptr()

        class Functor:

            @staticmethod
            def Create(x,  # pos
                       a):  # PHR
                # 1: the graphic object (NodePath)
                nameSuffix = str(random.random())
                triMesh = procedurallyGenerating3DModels(gVertices,
                                                         gIndices, NUM_TRIANGLES, True, 'TriMesh' + nameSuffix)
                graphicNPs.append(triMesh)
#                 graphicNPs[-1].set_pos_hpr_scale(
#                         x,#position
#                         a,#rotation (h,p,r)
#                         s)#scale
                # model coloring
                graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
                # 2: create the empty soft body
                softBodyNP = physicsMgr.create_soft_body('TriMeshBody' +
                                                         nameSuffix)
                triMeshBody = softBodyNP.node()
                # 3: set CONSTRUCTION PARAMETERS
                triMeshBody.group_mask = physics_bt3_soft_demo.groupMask
                triMeshBody.collide_mask = physics_bt3_soft_demo.collideMask
                triMeshBody.body_type = BT3SoftBody.TRIANGLE_MESH
                # 4: setup the body with graphicNPs[-1] as owner
                triMeshBody.owner_object = graphicNPs[-1]
                triMeshBody.setup()
                # 5: set DYNAMIC PARAMETERS
                pm = triMeshBody.append_material()
                pm.flags = pm.flags - BT3SoftBodyMaterial.DebugDraw
                triMeshBody.generate_bending_constraints(2, pm)
                triMeshBody.config.positions_solver_iterations = 2
                triMeshBody.config.dynamic_friction_coefficient = 1
                triMeshBody.config.soft_soft_hardness = 1
                triMeshBody.config.soft_rigid_impulse_split = 0
                triMeshBody.config.soft_kinetic_hardness = 0.1
                triMeshBody.config.soft_rigid_impulse_split = 1
                triMeshBody.config.collisions_flags = BT3SoftBodyConfig.Cluster_Soft_Soft | \
                    BT3SoftBodyConfig.Cluster_Rigid_Soft
                triMeshBody.randomize_constraints()
                # compose: P.H.R
                tsP = TransformState.make_pos_hpr(
                    LPoint3f.zero(), LVecBase3f(0, a.get_x(), 0))
                tsH = TransformState.make_pos_hpr(
                    LPoint3f.zero(), LVecBase3f(a.get_y(), 0, 0))
                tsR = TransformState.make_pos_hpr(
                    LPoint3f.zero(), LVecBase3f(0, 0, a.get_z()))
                m = tsP.compose(tsH.compose(tsR))
                triMeshBody.transform(
                    TransformState.make_pos_hpr(x, m.get_hpr()))
                triMeshBody.scale(2)
                triMeshBody.set_total_mass(50, True)
                triMeshBody.generate_clusters(16)
                # 6: visual state
#                softBodyNP.set_two_sided(True)
#                softBodyNP.show_bounds()
#                softBodyNP.set_render_mode_wireframe()
                return triMeshBody

        #
        for i in range(3):
            Functor.Create(LPoint3f(-3 * i, 0, 2 + 10),
                           LVecBase3f(-M_PI / 2 * (1 - (i & 1)) * (180.0 / M_PI),
                                      M_PI / 2 * (i & 1) * (180.0 / M_PI), 0))
    else:
        destroyObjects()


def Init_ClusterSocket(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_ClusterSocket
        psb = Ctor_ClusterTorus(LPoint3f(0, 0, 10),
                                LVecBase3f(-M_PI / 2.0 * (180.0 / M_PI),
                                           M_PI / 2.0 * (180.0 / M_PI),
                                           0))
        prb = Ctor_BigPlate(50, 10 + 8, app, modelFile, modelAnimFiles)
        lj = BT3SoftBodyLJointSpecs()
        lj.position = LPoint3f(0, 0, 5 + 10)
        psb.append_linear_joint(lj, NodePath.any_path(prb))
    else:
        destroyObjects()


def Init_ClusterHinge(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_ClusterHinge
        psb = Ctor_ClusterTorus(LPoint3f(0, 0, 10),
                                LVecBase3f(-M_PI / 2.0 * (180.0 / M_PI),
                                           M_PI / 2.0 * (180.0 / M_PI),
                                           0))
        prb = Ctor_BigPlate(50, 10 + 8, app, modelFile, modelAnimFiles)
        psb.config.dynamic_friction_coefficient = 1
        aj = BT3SoftBodyAJointSpecs()
        aj.axis = LVector3f(0, 1, 0)
        psb.append_angular_joint(aj, NodePath.any_path(prb))
    else:
        destroyObjects()


motorcontrol = BT3SoftBodyAJointControl(BT3SoftBodyAJointControl.MOTOR)
steercontrol_f = BT3SoftBodyAJointControl(BT3SoftBodyAJointControl.STEER, +1)
steercontrol_r = BT3SoftBodyAJointControl(BT3SoftBodyAJointControl.STEER, -1)


def Init_ClusterCombine(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_ClusterCombine
        sz = LVecBase3f(2, 2, 4)
        psb0 = Ctor_ClusterTorus(LPoint3f(0, 0, 10 + 8),
                                 LVecBase3f(-M_PI / 2.0 * (180.0 / M_PI),
                                            M_PI / 2.0 * (180.0 / M_PI),
                                            0),
                                 sz)
        psb1 = Ctor_ClusterTorus(LPoint3f(0, 10, 10 + 8),
                                 LVecBase3f(-M_PI / 2.0 * (180.0 / M_PI),
                                            M_PI / 2.0 * (180.0 / M_PI),
                                            0),
                                 sz)
        psbs = [psb0, psb1]
        for j in range(2):
            psbs[j].config.dynamic_friction_coefficient = 1
            psbs[j].config.damping_coefficient = 0
            psbs[j].config.positions_solver_iterations = 1
            psbs[j].set_cluster_matching(0, 0.05)
            psbs[j].set_cluster_node_damping(0, 0.05)
        aj = BT3SoftBodyAJointSpecs()
        aj.set_axis(LVector3f(0, 1, 0))
        aj.set_icontrol(motorcontrol)
        psb0.append_angular_joint(aj, NodePath.any_path(psb1))

        lj = BT3SoftBodyLJointSpecs()
        lj.set_position(LVector3f(0, 5, 10 + 8))
        psb0.append_linear_joint(lj, NodePath.any_path(psb1))
    else:
        destroyObjects()


def Ctor_ClusterBunny(x, a):
    physicsMgr = GamePhysicsManager.get_global_ptr()
    # 1: the graphic object (NodePath)
    nameSuffix = str(random.random())
    triMesh = procedurallyGenerating3DModels(gVerticesBunny,
                                             gIndicesBunny, BUNNY_NUM_TRIANGLES, True, 'ClusterBunny' +
                                             nameSuffix)
    graphicNPs.append(triMesh)
    # model coloring
    graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
    # 2: create the empty soft body
    softBodyNP = physicsMgr.create_soft_body('ClusterBunnyBody' + nameSuffix)
    psb = softBodyNP.node()
    # 3: set CONSTRUCTION PARAMETERS
    psb.group_mask = physics_bt3_soft_demo.groupMask
    psb.collide_mask = physics_bt3_soft_demo.collideMask
    psb.body_type = BT3SoftBody.TRIANGLE_MESH
    # 4: setup the body with graphicNPs[-1] as owner
    psb.owner_object = graphicNPs[-1]
    psb.setup()
    # 5: set DYNAMIC PARAMETERS
    pm = psb.append_material()
    pm.linear_stiffness_coefficient = 1
    pm.flags = pm.get_flags() - BT3SoftBodyMaterial.DebugDraw
    psb.generate_bending_constraints(2, pm)
    psb.config.positions_solver_iterations = 2
    psb.config.dynamic_friction_coefficient = 1
    psb.config.collisions_flags = BT3SoftBodyConfig.Cluster_Soft_Soft | \
        BT3SoftBodyConfig.Cluster_Rigid_Soft
    psb.randomize_constraints()
    m = TransformState.make_pos_hpr(x, a)
    psb.transform(m)
    psb.scale(LVecBase3f(4, 4, 4))
    psb.set_total_mass(75, True)
    psb.generate_clusters(1)
    # 6: visual state
#     softBodyNP.set_two_sided(True)
#     softBodyNP.show_bounds()
#     softBodyNP.set_render_mode_wireframe()
    return psb


def Init_ClusterCar(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_ClusterCar
        origin = LPoint3f(-50, 0, 40 + 10)
        orientation = LQuaternionf()
        orientation.set_hpr(LVecBase3f((-M_PI / 2) * (180.0 / M_PI), 0, 0))
        widthf = 4.0
        widthr = 4.5
        length = 4.0
        height = 2.0
        wheels = [
            LPoint3f(-widthf, +length, -height),  # Front left
            LPoint3f(+widthf, +length, -height),  # Front right
            LPoint3f(-widthr, -length, -height),  # Rear left
            LPoint3f(+widthr, -length, -height),  # Rear right
        ]
        pa = Ctor_ClusterBunny(LPoint3f(0, 0, 0),
                               LVecBase3f(0, 0, 0))
        pfl = Ctor_ClusterTorus(wheels[0],
                                LVecBase3f(0, M_PI / 2.0 * (180.0 / M_PI), 0),
                                LVecBase3f(1, 1, 2))
        pfr = Ctor_ClusterTorus(wheels[1],
                                LVecBase3f(0, M_PI / 2.0 * (180.0 / M_PI), 0),
                                LVecBase3f(1, 1, 2))
        prl = Ctor_ClusterTorus(wheels[2],
                                LVecBase3f(0, M_PI / 2.0 * (180.0 / M_PI), 0),
                                LVecBase3f(1, 1, 2.5))
        prr = Ctor_ClusterTorus(wheels[3],
                                LVecBase3f(0, M_PI / 2.0 * (180.0 / M_PI), 0),
                                LVecBase3f(1, 1, 2.5))

        pfl.config.dynamic_friction_coefficient = 1
        pfr.config.dynamic_friction_coefficient = 1
        prl.config.dynamic_friction_coefficient = 1
        prr.config.dynamic_friction_coefficient = 1

        lspecs = BT3SoftBodyLJointSpecs()
        lspecs.cfm = 1
        lspecs.erp = 1
        lspecs.position = LPoint3f(0, 0, 0)

        lspecs.position = wheels[0]
        pa.append_linear_joint(lspecs, NodePath.any_path(pfl))
        lspecs.position = wheels[1]
        pa.append_linear_joint(lspecs, NodePath.any_path(pfr))
        lspecs.position = wheels[2]
        pa.append_linear_joint(lspecs, NodePath.any_path(prl))
        lspecs.position = wheels[3]
        pa.append_linear_joint(lspecs, NodePath.any_path(prr))

        aspecs = BT3SoftBodyAJointSpecs()
        aspecs.cfm = 1
        aspecs.erp = 1
        aspecs.axis = LVector3f(-1, 0, 0)

        aspecs.icontrol = steercontrol_f
        pa.append_angular_joint(aspecs, NodePath.any_path(pfl))
        pa.append_angular_joint(aspecs, NodePath.any_path(pfr))

        aspecs.icontrol = motorcontrol
        pa.append_angular_joint(aspecs, NodePath.any_path(prl))
        pa.append_angular_joint(aspecs, NodePath.any_path(prr))

        pa.rotate(orientation)
        pfl.rotate(orientation)
        pfr.rotate(orientation)
        prl.rotate(orientation)
        prr.rotate(orientation)
        pa.translate(origin)
        pfl.translate(origin)
        pfr.translate(origin)
        prl.translate(origin)
        prr.translate(origin)
        pfl.config.positions_solver_iterations = 1
        pfr.config.positions_solver_iterations = 1
        prl.config.positions_solver_iterations = 1
        prr.config.positions_solver_iterations = 1
        pfl.set_cluster_matching(0, 0.05)
        pfr.set_cluster_matching(0, 0.05)
        prl.set_cluster_matching(0, 0.05)
        prr.set_cluster_matching(0, 0.05)
        pfl.set_cluster_node_damping(0, 0.05)
        pfr.set_cluster_node_damping(0, 0.05)
        prl.set_cluster_node_damping(0, 0.05)
        prr.set_cluster_node_damping(0, 0.05)

        Ctor_LinearStair(LPoint3f(0, 0, -8 + 10), LVecBase3f(3, 40, 2), 20,
                         8, 'box', 'z', app, modelFile, modelAnimFiles)
        Ctor_RbUpStack(50, LPoint3f(0.0, 0.0, 20.0), app, modelFile,
                       modelAnimFiles)
    else:
        destroyObjects()


def Init_ClusterRobot(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # #bullet3.examples.SoftDemo.Init_ClusterRobot
        physicsMgr = GamePhysicsManager.get_global_ptr()

        class Functor:

            @staticmethod
            def CreateBall(pos):
                # 1: the graphic object (NodePath)
                ball = GeomNode('Ball')
                graphicNPs.append(NodePath(ball))
                graphicNPs[-1].set_pos_hpr_scale(
                    LPoint3f(0.0, 0.0, 0.0),  # position
                    LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
                    LVecBase3f(1.0, 1.0, 1.0))  # scale
                graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
                # 2: create the empty soft body
                softBodyNP = physicsMgr.create_soft_body('BallBody')
                ballBody = softBodyNP.node()
                # 3: set CONSTRUCTION PARAMETERS
                ballBody.group_mask = physics_bt3_soft_demo.groupMask
                ballBody.collide_mask = physics_bt3_soft_demo.collideMask
                ballBody.body_type = BT3SoftBody.ELLIPSOID
                ballBody.ellipsoid_center = pos
                ballBody.ellipsoid_radius = LVecBase3f(1, 1, 1) * 3
                ballBody.ellipsoid_res = 512
                # 4: setup the body with graphicNPs[-1] as owner
                ballBody.owner_object = graphicNPs[-1]
                ballBody.setup()
                # 5: set DYNAMIC PARAMETERS
                ballBody.get_materials()[0].linear_stiffness_coefficient = 0.45
                ballBody.config.volume_conversation_coefficient = 20
                ballBody.set_total_mass(50, True)
                ballBody.set_pose(True, False)
                ballBody.generate_clusters(1)
                # 6: visual state
#                softBodyNP.set_two_sided(True)
#                softBodyNP.show_bounds()
#                softBodyNP.set_render_mode_wireframe()
                return ballBody

        #
        base = LPoint3f(0, 8, 25 + 10)
        psb0 = Functor.CreateBall(base + LVector3f(8, 0, 0))
        psb1 = Functor.CreateBall(base + LVector3f(-8, 0, 0))
        psb2 = Functor.CreateBall(base + LVector3f(0, +8 * sqrt(2), 0))
        ctr = (psb0.get_cluster_com(0) + psb1.get_cluster_com(0) +
               psb2.get_cluster_com(0)) / 3.0

        prbNP = common.getModelAnims('prbNP', 1.0, 3,
                                     physics_bt3_soft_demo.playerAnimCtls, app, modelFile, modelAnimFiles)
        prb = common.createRigidBodyXYZ(prbNP,
                                        'prbZ', 'cylinder', 'z', ctr +
                                        LVector3f(0, 0, 5),
                                        LVecBase3f(0.0, 0.0, 0.0), LVecBase3f(16, 16, 1), False).node()
        prb.mass = 50.0
        prb.switch_body_type(BT3RigidBody.DYNAMIC)
        graphicNPs.append(prb.get_owner_object())

        ls = BT3SoftBodyLJointSpecs()
        ls.erp = 0.5
        ls.position = psb0.get_cluster_com(0)
        psb0.append_linear_joint(ls, NodePath.any_path(prb))
        ls.position = psb1.get_cluster_com(0)
        psb1.append_linear_joint(ls, NodePath.any_path(prb))
        ls.position = psb2.get_cluster_com(0)
        psb2.append_linear_joint(ls, NodePath.any_path(prb))

        pgrnNP = common.getModelAnims('pgrnNP', 1.0, 8,
                                      physics_bt3_soft_demo.playerAnimCtls, app, modelFile, modelAnimFiles)
        pgrn = common.createRigidBodyXYZ(pgrnNP,
                                         'pgrnZ', 'box', 'z', LPoint3f(
                                             0, 0, 10),
                                         LVecBase3f(0, M_PI / 4.0 *
                                                    (180.0 / M_PI), 0),
                                         LVecBase3f(40, 80, 2), False).node()
        pgrn.mass = 0.0
        pgrn.switch_body_type(BT3RigidBody.STATIC)
        graphicNPs.append(pgrn.owner_object)
    else:
        destroyObjects()


def Init_ClusterStackSoft(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_ClusterStackSoft
        for i in range(10):
            psb = Ctor_ClusterTorus(LPoint3f(0, 0, -9 + 8.25 * i + 20),
                                    LVecBase3f(0, 0, 0))
            psb.config.dynamic_friction_coefficient = 1
    else:
        destroyObjects()


def Init_ClusterStackMixed(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_ClusterStackSoft
        for i in range(10):
            if (i + 1) & 1:
                Ctor_BigPlate(50, -9 + 4.25 * i + 20, app, modelFile,
                              modelAnimFiles)
            else:
                psb = Ctor_ClusterTorus(LPoint3f(0, 0, -9 + 4.25 * i + 20),
                                        LVecBase3f(0, 0, 0))
                psb.config.dynamic_friction_coefficient = 1
    else:
        destroyObjects()


from TetraModels import TetraCube


def Init_TetraCube(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_TetraCube
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        tetraCube = GeomNode('TetraCube')
        graphicNPs.append(NodePath(tetraCube))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 0.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('TetraCubeBody')
        psb = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        psb.group_mask = physics_bt3_soft_demo.groupMask
        psb.collide_mask = physics_bt3_soft_demo.collideMask
        psb.body_type = BT3SoftBody.TETGEN_MESH
        eleFaceNode = ValueList_string([
            TetraCube.getElements(),
            '',
            TetraCube.getNodes()
        ])
        psb.set_tetgen_ele_face_node(eleFaceNode, False)
        psb.tetgen_facelinks = False
        psb.tetgen_tetralinks = True
        psb.tetgen_facesfromtetras = True
        # 4: setup the body with graphicNPs[-1] as owner
        psb.owner_object = graphicNPs[-1]
        psb.setup()
        # 5: set DYNAMIC PARAMETERS
        psb.scale(LVecBase3f(4, 4, 4))
        psb.translate(LPoint3f(0, 0, 5 + 10))
        psb.set_volume_mass(300)
        psb.config.positions_solver_iterations = 1
        psb.generate_clusters(16)
        psb.get_collision_object_attrs().set_shape_margin(0.01)
        psb.config.collisions_flags = BT3SoftBodyConfig.Cluster_Soft_Soft | \
            BT3SoftBodyConfig.Cluster_Rigid_Soft
        psb.get_material(0).linear_stiffness_coefficient = 0.8
        # 6: visual state
#        softBodyNP.set_two_sided(True)
#        softBodyNP.show_bounds()
#        softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()


from TetraModels import TetraBunny


def Init_TetraBunny(init, app, modelFile, modelAnimFiles):
    global graphicNPs, softBodyNP
    if init:
        # # bullet3.examples.SoftDemo.Init_TetraBunny
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # 1: the graphic object (NodePath)
        tetraBunny = GeomNode('TetraBunny')
        graphicNPs.append(NodePath(tetraBunny))
        graphicNPs[-1].set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 0.0),  # position
            LVecBase3f(0.0, 0.0, 0.0),  # rotation (h,p,r)
            LVecBase3f(1.0, 1.0, 1.0))  # scale
        graphicNPs[-1].set_color(LVecBase4f(Vector3Rand(), 1.0))
        # 2: create the empty soft body
        softBodyNP = physicsMgr.create_soft_body('TetraBunnyBody')
        psb = softBodyNP.node()
        # 3: set CONSTRUCTION PARAMETERS
        psb.group_mask = physics_bt3_soft_demo.groupMask
        psb.collide_mask = physics_bt3_soft_demo.collideMask
        psb.body_type = BT3SoftBody.TETGEN_MESH
        eleFaceNode = ValueList_string([
            TetraBunny.getElements(),
            '',
            TetraBunny.getNodes()
        ])
        psb.set_tetgen_ele_face_node(eleFaceNode, False)
        psb.tetgen_facelinks = False
        psb.tetgen_tetralinks = True
        psb.tetgen_facesfromtetras = True
        # 4: setup the body with graphicNPs[-1] as owner
        psb.owner_object = graphicNPs[-1]
        psb.setup()
        # 5: set DYNAMIC PARAMETERS
        quat = LQuaternionf()
        quat.set_hpr(LVecBase3f(M_PI / 2.0 * (180.0 / M_PI), 0, 0))
        psb.rotate(quat)
        psb.translate(LPoint3f(0, 0, 20))
        psb.set_volume_mass(150)
        psb.config.positions_solver_iterations = 2
        psb.config.collisions_flags = BT3SoftBodyConfig.Cluster_Soft_Soft | \
            BT3SoftBodyConfig.Cluster_Rigid_Soft
        # #pass zero in generateClusters to create  cluster for each tetrahedron or triangle
        psb.generate_clusters(0)
        psb.config.dynamic_friction_coefficient = 10
        # 6: visual state
#        softBodyNP.set_two_sided(True)
#        softBodyNP.show_bounds()
#        softBodyNP.set_render_mode_wireframe()
    else:
        destroyObjects()
