'''
Created on Oct 09, 2016

@author: consultit
'''

from ely.physics import GamePhysicsManager, BT3Constraint, BT3RigidBody, \
    BT3PhysicsInfo
from common import startFramework, printCreationParameters, modelFile, \
    readFromBamFile, writeToBamFileAndExit, loadPlane, getModelAnims, \
    toggleDebugDraw, createRigidBodyXYZ, createConstraint, modelAnimFiles, \
    loadTerrainLowPoly, loadTerrain, app, groupMask, collideMask, userData, \
    bamFileName, toggleDebugFlag, sceneNP, globalClock, playerAnimCtls
from panda3d.core import NodePath, LRotationf, ClockObject, TextNode, \
    LPoint3f, LVecBase3f, LVector3f
from math import radians

#
if __name__ == '__main__':

    msg = 'BT3Constraint test'
    app, bamFile, renderPipeline, heightfield, physicsMT = startFramework(msg)

    # # here is room for your own code
    # print some help to screen
    text = TextNode('Help')
    text.set_text(
        msg + '\n\n'
        '- press \'d\' to toggle debug drawing\n'
        '- press \'up\'/\'left\'/\'down\'/\'right\' arrows to move the player\n'
        '- press \'a\' to pick up the object under the mouse\n')
    textNodePath = app.aspect2d.attach_new_node(text)
    textNodePath.set_pos(-1.25, 0.0, 0.8)
    textNodePath.set_scale(0.035)
    if renderPipeline:
        textNodePath.set_color(0, 0, 0)

    # create a physics manager
    physicsMgr = None
    if physicsMT:
        # Multi-threaded physics
        physicsInfo = BT3PhysicsInfo()
        physicsInfo.set_world_type(BT3PhysicsInfo.DISCRETE_MT_WORLD)
        physicsInfo.set_broad_phase_type(BT3PhysicsInfo.DBVT_BROADPHASE)
        physicsInfo.set_solver_type(
            BT3PhysicsInfo.SEQUENTIAL_IMPULSE_CSOLVER_MT)
        physicsMgr = GamePhysicsManager(10, app.render, groupMask, collideMask,
                                        physicsInfo)
    else:
        physicsMgr = GamePhysicsManager(10, app.render, groupMask, collideMask)

    # enable object picking
    physicsMgr.enable_picking(app.render, NodePath.any_path(app.camNode),
                              app.mouseWatcher.node(), 'a', 'a-up')

    # print creation parameters: defult values
    print('\n' + 'Default creation parameters:')
    printCreationParameters()

    # reparent reference node to render
    physicsMgr.get_reference_node_path().reparent_to(app.render)

    # # Static environment
    # get a sceneNP, naming it with 'SceneNP' to ease restoring from bam file
    pos = LVector3f(0.0, 0.0, 0.0)
    hpr = LVecBase3f(45.0, 0.0, 0.0)
    # # plane
    planeUpAxis = GamePhysicsManager.Z_up  # Z_up X_up Y_up
    sceneNP = loadPlane('SceneNP', app, 128.0, 128.0, planeUpAxis)

    # set sceneNP transform
    sceneNP.set_pos_hpr(pos, hpr)

    # create scene's rigid_body (attached to the reference node)
    sceneRigidBodyNP = physicsMgr.create_rigid_body('SceneRigidBody')
    # get a reference to the scene's rigid_body
    sceneRigidBody = sceneRigidBodyNP.node()

    # set some parameters before setup
    # # plane
    sceneRigidBody.set_up_axis(planeUpAxis)
    sceneRigidBody.set_shape_type(GamePhysicsManager.PLANE)

    # set mass=0 before setup() for static (and kinematic) bodies
    sceneRigidBody.set_mass(0.0)
    # set the object for rigid body
    sceneRigidBody.set_owner_object(sceneNP)

    # setup
    sceneRigidBody.setup()
    # change some attrs
    print(sceneRigidBody.collision_object_attrs.friction)
    sceneRigidBody.collision_object_attrs.friction = 0.3
    print(sceneRigidBody.collision_object_attrs.friction)

    # # Constraints
    modelNP = getModelAnims('modelNP', 1.0, 1, playerAnimCtls, app, modelFile,
                            modelAnimFiles)
    rbANP = NodePath()
    rbBNP = NodePath()
    # # constraint with world reference
    rbANP = createRigidBodyXYZ(modelNP, 'rbA1', 'cylinder', 'z',
                               LPoint3f(-10.0, 0.0,
                                        40.0), LVecBase3f(0.0, 0.0, 0.0),
                               LVecBase3f(0.5))
    rbANP.node().set_mass(0.0)
    rbANP.node().switch_body_type(BT3RigidBody.KINEMATIC)
    rbBNP = createRigidBodyXYZ(modelNP, 'rbB1', 'capsule', 'z',
                               LPoint3f(-10.0, 0.0,
                                        25.0), LVecBase3f(0.0, 0.0, 0.0),
                               LVecBase3f(2.0, 2.0, 2.0))

    # event handling
    def setAccept(keyEv, handlerEv, argsEv):
        global app
        argsAdd = argsEv[:2] + [+argsEv[-1]]
        argsSub = argsEv[:2] + [-argsEv[-1]]
        app.accept(keyEv, handlerEv, argsAdd)
        app.accept(keyEv + '-repeat', handlerEv, argsAdd)
        app.accept('shift-' + keyEv, handlerEv, argsSub)
        app.accept('shift-' + keyEv + '-repeat', handlerEv, argsSub)

    diffH = 1

    def rotateH(rbA, rbB, diff):
        rbB.node().collision_object_attrs.set_activate()
        rbA.set_h(rbA.get_h() + diff)

    setAccept('h', rotateH, [rbANP, rbBNP, diffH])

    diffP = 1

    def rotateP(rbA, rbB, diff):
        rbB.node().collision_object_attrs.set_activate()
        rbA.set_p(rbA.get_p() + diff)

    setAccept('p', rotateP, [rbANP, rbBNP, diffP])

    diffR = 1

    def rotateR(rbA, rbB, diff):
        rbB.node().collision_object_attrs.set_activate()
        rbA.set_r(rbA.get_r() + diff)

    setAccept('r', rotateR, [rbANP, rbBNP, diffR])

    csInitialize = True
    csType = (
        #         BT3Constraint.POINT2POINT, 'point2point'
        #         BT3Constraint.CONETWIST, 'cone_twist'
        BT3Constraint.DOF6, 'dof6'
    )

    csWR = None
    if not csInitialize:
        csWR = createConstraint(rbANP, rbBNP, 'constraint',
                                csType[0], True,
                                LPoint3f(-10.0, 0.0, 39.0), LPoint3f.zero(),
                                LVector3f.zero(), LVector3f.zero(),
                                LVecBase3f.zero(), LVecBase3f.zero(),
                                LVector3f.zero(), LVector3f.zero(),
                                LPoint3f.zero(), 0.0, BT3Constraint.RO_XYZ,
                                False,
                                initialize=csInitialize)
        csWR.set_rotation_references([LVecBase3f(0, 0, 0)])
        csWR.setup()
    else:
        csWR = createConstraint(rbANP.get_name(), rbBNP.get_name(), 'constraint',
                                csType[1], 'true',
                                '-10.0,0.0,39.0', '0.0,0.0,0.0',
                                '0.0,0.0,0.0', '0.0,0.0,0.0',
                                '0.0,0.0,0.0', '0.0,0.0,0.0',
                                '0.0,0.0,0.0', '0.0,0.0,0.0',
                                '0.0,0.0,0.0', '0.0', 'xyz', 'false',
                                initialize=csInitialize)
    #
    csWR.constraints_attrs.debug_draw_size = 1.0
    if csType[0] == BT3Constraint.CONETWIST:
        csAttrs = csWR.constraints_attrs
        params = [csAttrs.swing_span1, csAttrs.swing_span2, csAttrs.twist_span,
                  csAttrs.limit_softness, csAttrs.bias_factor,
                  csAttrs.relaxation_factor]
        params[:6] = [radians(45), radians(45), radians(45), 1.0, 0.3, 1.0]
        csAttrs.set_limit(*params)
        csAttrs.angular_only = False
        csAttrs.damping = 0.01
        csAttrs.fix_thresh = 0.05
        csAttrs.enable_motor = False
        target = LRotationf(LVector3f(0.0, 0.0, 1.0), 0)
        csAttrs.set_motor_target(target)
        csAttrs.max_motor_impulse = 2.0
        csAttrs.max_motor_impulse_normalized = True

    # # body A attached to world through constraint
    physicsMgr.set_parameters_defaults(GamePhysicsManager.CONSTRAINT)
    rbWANP = createRigidBodyXYZ(modelNP, 'rbWA1', 'capsule', 'z',
                                LPoint3f(10.0, 0.0, 40.0), LVecBase3f(
                                    0.0, 0.0, 0.0),
                                LVecBase3f(1.0))
    rbWBNP = NodePath()
    csWType = BT3Constraint.POINT2POINT
    csWWR = createConstraint(rbWANP, rbWBNP, 'constraintW',
                             csWType, True,
                             LPoint3f(10.0, 0.0, 50.0), LPoint3f.zero(),
                             LVector3f.zero(), LVector3f.zero(),
                             LVecBase3f.zero(), LVecBase3f.zero(),
                             LVector3f.zero(), LVector3f.zero(),
                             LPoint3f.zero(), 0.0,
                             BT3Constraint.RO_XYZ, False,
                             initialize=False)
    csWWR.set_rotation_references([LVecBase3f(0, 0, 0)])
    csWWR.setup()
    csWWR.constraints_attrs.debug_draw_size = 1.0

    # DEBUG DRAWING: make the debug reference node paths sibling of the
    # reference node
    physicsMgr.get_reference_node_path_debug().reparent_to(app.render)
    app.accept('d', toggleDebugDraw, [toggleDebugFlag])

    # # first option: start the default update task for all physics components
    physicsMgr.start_default_update()
    globalClock = ClockObject.get_global_clock()

    # write to bam file on exit
    app.win.set_close_request_event('close_request_event')
    app.accept('close_request_event', writeToBamFileAndExit, [bamFileName])

    # place camera
    trackball = app.trackball.node()
    trackball.set_pos(10.0, 80.0, -35.0)
    trackball.set_hpr(0.0, 10.0, 0.0)

    # app.run(), equals to call framework.main_loop() in C++
    app.run()
