'''
Created on May 06, 2017

@author: consultit
'''

from ely.physics import GamePhysicsManager
from common import startFramework, printCreationParameters, loadPlane, \
    loadTerrainLowPoly, toggleDebugDraw, modelFile, modelAnimFiles, app, \
    groupMask, sceneNP, collideMask, bamFileName, toggleDebugFlag, \
    playerAnimCtls
import physics_bullet_soft_demo
from panda3d.core import NodePath, ClockObject, DatagramInputFile, \
    DatagramIterator, TextNode, LPoint3f, LVecBase3f, Filename, LVector3f, \
    DatagramOutputFile, Datagram
from panda3d.direct import CIntervalManager
from direct.task import Task
import sys
#

# # soft demos' declarations


class Sample:
    def __init__(self, sample, descr):
        self.sample = sample
        self.descr = descr


sampleList = []


def createSampleList():
    global sampleList
    sampleList.append(Sample(physics_bullet_soft_demo.RopeGeom, 'RopeGeom'))
    sampleList.append(Sample(physics_bullet_soft_demo.RopeNURBS, 'RopeNURBS'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Cloth, 'Init_Cloth'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Pressure, 'Init_Pressure'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Volume, 'Init_Volume'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Ropes, 'Init_Ropes'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_RopeAttach, 'Init_RopeAttach'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_ClothAttach, 'Init_ClothAttach'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Sticks, 'Init_Sticks'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_CapsuleCollision, 'Init_CapsuleCollision'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Collide, 'Init_Collide'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Collide2, 'Init_Collide2'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Collide3, 'Init_Collide3'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Impact, 'Init_Impact'))
    sampleList.append(Sample(physics_bullet_soft_demo.Init_Aero, 'Init_Aero'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Aero2, 'Init_Aero2'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Friction, 'Init_Friction'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Torus, 'Init_Torus'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_TorusMatch, 'Init_TorusMatch'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Bunny, 'Init_Bunny'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_BunnyMatch, 'Init_BunnyMatch'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_Cutting1, 'Init_Cutting1'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_ClusterDeform, 'Init_ClusterDeform'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_ClusterCollide1, 'Init_ClusterCollide1'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_ClusterCollide2, 'Init_ClusterCollide2'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_ClusterSocket, 'Init_ClusterSocket'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_ClusterHinge, 'Init_ClusterHinge'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_ClusterCombine, 'Init_ClusterCombine'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_ClusterCar, 'Init_ClusterCar'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_ClusterRobot, 'Init_ClusterRobot'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_ClusterStackSoft, 'Init_ClusterStackSoft'))
    sampleList.append(Sample(
        physics_bullet_soft_demo.Init_ClusterStackMixed, 'Init_ClusterStackMixed'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_TetraCube, 'Init_TetraCube'))
    sampleList.append(
        Sample(physics_bullet_soft_demo.Init_TetraBunny, 'Init_TetraBunny'))


sampleIdx = -1
fromBamFile = False
sampleText = TextNode('')


def iterateSamples():
    '''iterate over samples'''

    global app, modelFile, modelAnimFiles, sampleIdx, sampleList, sampleText
    if sampleIdx >= 0:
        # stop old sample
        sampleList[sampleIdx].sample(False, app, modelFile, modelAnimFiles)
    # start new sample
    sampleIdx += 1
    if sampleIdx >= len(sampleList):
        sampleIdx = 0
    sampleList[sampleIdx].sample(True, app, modelFile, modelAnimFiles)
    sampleText.set_text(sampleList[sampleIdx].descr)

#


def readFromBamFile(fileName):
    '''read scene from a file'''

    global sampleIdx
    # restore sampleIdx
    sampleIdxF = DatagramInputFile()
    sampleIdxF.open(Filename('sampleIdx.boo'))
    sampleIdxDG = Datagram()
    sampleIdxF.get_datagram(sampleIdxDG)
    sampleIdxDGScan = DatagramIterator(sampleIdxDG)
    sampleIdx = sampleIdxDGScan.get_int32()
    sampleIdxF.close()
    #
    return GamePhysicsManager.get_global_ptr().read_from_bam_file(fileName)


def writeToBamFileAndExit(fileName):
    '''write scene to a file (and exit)'''

    global sampleIdx
    # save sampleIdx
    sampleIdxF = DatagramOutputFile()
    sampleIdxF.open(Filename('sampleIdx.boo'))
    sampleIdxDG = Datagram()
    sampleIdxDG.add_int32(sampleIdx)
    sampleIdxF.put_datagram(sampleIdxDG)
    sampleIdxF.close()
    #
    GamePhysicsManager.get_global_ptr().write_to_bam_file(fileName)
    # # this is for testing explicit removal and destruction of all elements
    physicsMgr = GamePhysicsManager.get_global_ptr()
    # destroy rigid bodies
    for rigid_bodyTmp in physicsMgr.get_rigid_bodies():
        # destroy rigid_bodyTmp
        physicsMgr.destroy_rigid_body(NodePath.any_path(rigid_bodyTmp))
    # destroy soft bodies
    for soft_bodyTmp in physicsMgr.get_soft_bodies():
        # destroy soft_bodyTmp
        physicsMgr.destroy_soft_body(NodePath.any_path(soft_bodyTmp))
    #
    sys.exit(0)


def updateControls(task):
    '''custom update task for controls'''

    # call update for controls
    dt = ClockObject.get_global_clock().get_dt()
    if sampleIdx == 0:
        physics_bullet_soft_demo.ropeGeomBody.update(dt)
    #
    return task.cont


def genericEventNotify(name, object0, object1):
    '''collision notify'''

    print('got \'' + name + '\' between \'' + object0.get_name() +
          '\' and \'' + object1.get_name() + '\'')


if __name__ == '__main__':

    msg = 'BTSoftBody test'
    app, bamFile, renderPipeline, heightfield, physicsMT = startFramework(msg)

    # # here is room for your own code
    # print some help to screen
    text = TextNode('Help')
    text.set_text(
        msg + '\n\n'
        '- press \'n\' to cycle samples\n'
        '- press \'d\' to toggle debug drawing\n'
        '- press \'a\' to pick up the object under the mouse\n')
    textNodePath = app.aspect2d.attach_new_node(text)
    textNodePath.set_pos(-1.25, 0.0, 0.8)
    textNodePath.set_scale(0.035)
    if renderPipeline:
        textNodePath.set_color(0, 0, 0)

    # create a physics manager
    physicsMgr = GamePhysicsManager(10, app.render, groupMask, collideMask)

    # enable object picking
    physicsMgr.enable_picking(app.render, NodePath.any_path(app.camNode),
                              app.mouseWatcher.node(), 'a', 'a-up')

    # print creation parameters: defult values
    print('\n' + 'Default creation parameters:')
    printCreationParameters()

    # # set soft world parameters
    physicsMgr.air_density = 1.2
    physicsMgr.water_density = 0
    physicsMgr.water_offset = 0
    physicsMgr.water_normal = LVector3f(0, 0, 0)

    # load or restore all scene stuff: if passed an argument
    # try to read it from bam file
    if (not bamFile) or (not readFromBamFile(bamFile)):
        # no argument or no valid bamFile
        fromBamFile = False
        # reparent reference node to render
        physicsMgr.get_reference_node_path().reparent_to(app.render)

        # # Static environment
        # get a sceneNP, naming it with 'SceneNP' to ease restoring from bam
        # file
        pos = LVector3f(0.0, 0.0, 0.0)
        hpr = LVecBase3f(45.0, 0.0, 0.0)
        # # plane
#         planeUpAxis = GamePhysicsManager.Z_up  # Z_up X_up Y_up
#         sceneNP = loadPlane('SceneNP', 128.0, 128.0, planeUpAxis)
        # # triangle mesh
#         sceneNP = loadTerrainLowPoly(app, 'SceneNP', 128, 128.0)
        # # parallelepiped
        sceneNP = app.loader.load_model('box')
        pos = LPoint3f(-10, -105.0, -2.5)
        hpr = LVecBase3f(45.0, 0, 0)
        sceneNP.set_scale(LVecBase3f(200.0, 200.0, 5.0))

        # set sceneNP transform
        sceneNP.set_pos_hpr(pos, hpr)

        # create scene's rigid_body (attached to the reference node)
        sceneRigidBodyNP = physicsMgr.create_rigid_body('SceneRigidBody')
        # get a reference to the scene's rigid_body
        sceneRigidBody = sceneRigidBodyNP.node()

        # set some parameters
        # # plane
#         sceneRigidBody.set_up_axis(planeUpAxis)
#         sceneRigidBody.set_shape_type(GamePhysicsManager.PLANE)
        # # triangle mesh
#         sceneRigidBody.set_shape_type(GamePhysicsManager.TRIANGLE_MESH)
        # # parallelepiped
        sceneRigidBody.set_shape_type(GamePhysicsManager.BOX)

        # set mass=0 before setup() for static (and kinematic) bodies
        sceneRigidBody.mass = 0.0
        # set the object for rigid body
        sceneRigidBody.owner_object = sceneNP

        # setup
        sceneRigidBody.setup()
        # change to kinematic only after setup (as long as mass=0)
#         sceneRigidBody.switch_body_type(BT3RigidBody.STATIC)
    else:
        # valid bamFile
        fromBamFile = True
        # reparent reference node to render
        physicsMgr.get_reference_node_path().reparent_to(app.render)

        # restore sceneNP: through panda3d
        sceneNP = physicsMgr.get_reference_node_path().find('**/SceneNP')

        if sampleIdx == 0:
            # restore ropeGeomBody: through physics manager
            for softBody in physicsMgr.get_soft_bodies():
                if softBody.get_name() == 'RopeGeomBody':
                    physics_bullet_soft_demo.ropeGeomBody = softBody
                    break

    # DEBUG DRAWING: make the debug reference node paths sibling of the
    # reference node
    physicsMgr.get_reference_node_path_debug().reparent_to(app.render)
    app.accept('d', toggleDebugDraw, [toggleDebugFlag])

    # enable collision notify event: BTRigidBody_BTRigidBody_Collision
    physicsMgr.enable_throw_event(GamePhysicsManager.COLLISIONNOTIFY, True, 0.1,
                                  'COLLISION')
    app.accept('COLLISION', genericEventNotify, ['COLLISION'])
    app.accept('COLLISIONOff', genericEventNotify, ['COLLISIONOff'])

    # ghost overlap notify events: OVERLAP
    app.accept('OVERLAP', genericEventNotify, ['OVERLAP'])
    app.accept('OVERLAPOff', genericEventNotify, ['OVERLAPOff'])

    # # Soft Bodies: iterate samples
    sampleText = TextNode('Samples')
    sampleTextNP = app.aspect2d.attach_new_node(sampleText)
    sampleTextNP.set_pos(-0.1, 0.0, 0.9)
    sampleTextNP.set_scale(0.05)
    sampleTextNP.set_color(0.9, 0.5, 0.1, 1.0)
    createSampleList()
    app.accept('n', iterateSamples)

    # # first option: start the default update task for all plug-ins
    physicsMgr.start_default_update()

    # # second option: start the custom update task for all plug-ins
#     app.taskMgr.add(updateControls, 'updateControls', 10, appendTask=True)

    # write to bam file on exit
    app.win.set_close_request_event('close_request_event')
    app.accept('close_request_event', writeToBamFileAndExit, [bamFileName])

    # place camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 130.0, -25.0)
    trackball.set_hpr(-20.0, 10.0, 0.0)

    # intervals task
    def intervalTask(task):
        CIntervalManager.get_global_ptr().step()
        return Task.cont
    app.taskMgr.add(intervalTask, 'intervalTask')
    # app.run(), equals to do the main loop in C++
    app.run()
