'''
Created on Feb 24, 2018

@author: consultit
'''

from ely.physics import GamePhysicsManager
from ely.libtools import ValueList_NodePath, ValueList_LPoint3f, \
    ValueList_LVector3f, ValueList_LVecBase3f
from panda3d.core import load_prc_file_data, WindowProperties, LVector4f, \
    NodePath, Filename, GeomVertexArrayFormat, InternalName, Geom, \
    GeomVertexFormat, GeomVertexData, GeomVertexWriter, LVector3f, LPoint3f, \
    GeomTriangles, GeomNode, TransformState, GeoMipTerrain, PNMImage, \
    TextureStage, TexturePool, AnimControlCollection, auto_bind, PartGroup, \
    LVecBase3f, BitMask32, ConfigVariableList
from direct.showbase.ShowBase import ShowBase
import argparse
import textwrap
import sys
import os

# # global data definitions
scriptPath = os.path.dirname(os.path.abspath(__file__))
prefD = os.path.join(scriptPath, '..', 'assets')
dataDirs = [
    os.path.join(prefD, 'models', 'test'),
    os.path.join(prefD, 'textures', 'test'),
    os.path.join(prefD, 'sounds', 'test'),
    os.path.join(prefD, 'scripts', 'test'),
    os.path.join(prefD, 'shaders', 'test'), ]
app = None
groupMask = BitMask32(0x10)
collideMask = BitMask32(0x10)
updateTask = None
userData = None
# bame file
bamFileName = 'physics.boo'
sceneNP = None
globalClock = None
playerAnimCtls = []

# models and animations
modelFile = ['eve.egg', 'ralph.egg', 'sparrow.egg', 'ball.egg', 'red_car.egg',
             'two-pills.egg', 'vehicle.egg', 'wheel.egg', 'misc/rgbCube']
modelAnimFiles = [['eve-walk.egg', 'eve-run.egg'],
                  ['ralph-walk.egg', 'ralph-run.egg'],
                  ['sparrow-flying.egg', 'sparrow-flying2.egg'],
                  ['', ''],
                  ['red_car-anim.egg', 'red_car-anim2.egg'],
                  ['', ''], ['', ''], ['', ''], ['', '']]
animRateFactor = [0.6, 0.175]


def getFilePath(fileName):
    for dataDir in ConfigVariableList('model-path'):
        filePath = os.path.join(dataDir, fileName)
        if os.path.exists(filePath):
            return filePath


def startFramework(msg):
    '''start base framework'''

    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter, description=textwrap.dedent('''
    This script will perform ''' + msg + '''.
    '''))
    # set up arguments
    parser.add_argument('bamFile', type=str, nargs='?',
                        help='the bam file storing the saved state')
    parser.add_argument('-r', '--render-pipeline', type=str,
                        help='the RenderPipeline dir')
    parser.add_argument('-f', '--heightfield', type=str,
                        help='the heightfield map')
    parser.add_argument('-p', '--physics-mt', action='store_true',
                        help='use multi-threaded physics engine')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    bamFile = args.bamFile
    renderPipeline = args.render_pipeline
    if args.heightfield:
        heightfield = args.heightfield
    else:
        heightfield = os.path.join(
            prefD, 'textures', 'test', 'heightfield.png')
    physicsMT = args.physics_mt
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)

    # Setup your application
    # Load your application's configuration
    for dataDir in dataDirs:
        load_prc_file_data('', 'model-path ' + dataDir)
    # Load your application's configuration
    load_prc_file_data('', 'model-path ' + dataDir)
    load_prc_file_data('', 'win-size 1024 768')
    load_prc_file_data('', 'show-frame-rate-meter #t')
    load_prc_file_data('', 'sync-video #t')
#     load_prc_file_data('', 'want-directtools #t')
#     load_prc_file_data('', 'want-tk #t')

    app = ShowBase()

    if renderPipeline:
        # 1: RenderPipeline
        sys.path.insert(0, renderPipeline)
        # Import the main render pipeline class
        from rpcore import RenderPipeline
        # Construct and create the pipeline
        render_pipeline = RenderPipeline()
        render_pipeline.pre_showbase_init()
        render_pipeline.create(app)
        render_pipeline.daytime_mgr.time = '7:40'
        render_pipeline.set_effect(app.render,
                                   os.path.join(prefD, 'scripts', 'game',
                                                'scene-effect.yaml'), {},
                                   sort=250)
        app.win.set_clear_color(LVector4f(0, 0, 0, 1))

    props = WindowProperties()
    props.setTitle(msg)
    app.win.requestProperties(props)

    # common callbacks
    #
    return (app, bamFile, renderPipeline, heightfield, physicsMT)


def printCreationParameters():
    '''print creation parameters'''

    physicsMgr = GamePhysicsManager.get_global_ptr()
    #
    valueList = physicsMgr.get_parameter_name_list(
        GamePhysicsManager.RIGIDBODY)
    print('\n' + 'BTRigidBody creation parameters:')
    for name in valueList:
        print('\t' + name + ' = ' +
              physicsMgr.get_parameter_value(GamePhysicsManager.RIGIDBODY, name))
    #
    valueList = physicsMgr.get_parameter_name_list(GamePhysicsManager.SOFTBODY)
    print('\n' + 'BTSoftBody creation parameters:')
    for name in valueList:
        print('\t' + name + ' = ' +
              physicsMgr.get_parameter_value(
                  GamePhysicsManager.SOFTBODY, name))
    #
    valueList = physicsMgr.get_parameter_name_list(GamePhysicsManager.GHOST)
    print('\n' + 'BT3Ghost creation parameters:')
    for name in valueList:
        print('\t' + name + ' = ' +
              physicsMgr.get_parameter_value(GamePhysicsManager.GHOST, name))
    #
    valueList = physicsMgr.get_parameter_name_list(
        GamePhysicsManager.CONSTRAINT)
    print('\n' + 'BT3Constraint creation parameters:')
    for name in valueList:
        print('\t' + name + ' = ' +
              physicsMgr.get_parameter_value(
                  GamePhysicsManager.CONSTRAINT, name))
    #
    valueList = physicsMgr.get_parameter_name_list(GamePhysicsManager.VEHICLE)
    print('\n' + 'BT3Vehicle creation parameters:')
    for name in valueList:
        print('\t' + name + ' = ' +
              physicsMgr.get_parameter_value(GamePhysicsManager.VEHICLE, name))
    #
    valueList = physicsMgr.get_parameter_name_list(
        GamePhysicsManager.CHARACTERCONTROLLER)
    print('\n' + 'BT3CharacterController creation parameters:')
    for name in valueList:
        print('\t' + name + ' = ' +
              physicsMgr.get_parameter_value(
                  GamePhysicsManager.CHARACTERCONTROLLER, name))


def setParametersBeforeCreation(typeIn, objectName, upAxis='z'):
    '''set parameters as strings before rigid_bodies/soft_bodies creation'''

    physicsMgr = GamePhysicsManager.get_global_ptr()
    if typeIn == GamePhysicsManager.RIGIDBODY:
        # set rigid body's parameters
        physicsMgr.set_parameter_value(typeIn, 'object', objectName)
        physicsMgr.set_parameter_value(typeIn, 'mass', '10.0')
        physicsMgr.set_parameter_value(typeIn, 'group_mask', '0x10')
        physicsMgr.set_parameter_value(typeIn, 'collide_mask', '0x10')
        physicsMgr.set_parameter_value(typeIn, 'up_axis', upAxis)

    # set soft body's parameters
    if typeIn == GamePhysicsManager.SOFTBODY:
        physicsMgr.set_parameter_value(typeIn, 'object', objectName)
        physicsMgr.set_parameter_value(typeIn, 'group_mask', '0x10')
        physicsMgr.set_parameter_value(typeIn, 'collide_mask', '0x10')
        physicsMgr.set_parameter_value(typeIn, 'total_mass', '20.0')
        physicsMgr.set_parameter_value(typeIn, 'from_faces', 'false')

    # set ghost's parameters
    if typeIn == GamePhysicsManager.GHOST:
        physicsMgr.set_parameter_value(typeIn, 'object', objectName)
        physicsMgr.set_parameter_value(typeIn, 'group_mask', '0x10')
        physicsMgr.set_parameter_value(typeIn, 'collide_mask', '0x10')
        physicsMgr.set_parameter_value(typeIn, 'up_axis', upAxis)

    # set constraint's parameters
    if typeIn == GamePhysicsManager.CONSTRAINT:
        physicsMgr.set_parameter_value(typeIn, '', '')

    # set vehicle's parameters
    if typeIn == GamePhysicsManager.VEHICLE:
        physicsMgr.set_parameter_value(typeIn, '', '')

    # set character controller's parameters
    if typeIn == GamePhysicsManager.CHARACTERCONTROLLER:
        physicsMgr.set_parameter_value(typeIn, '', '')

    #
    printCreationParameters()


def readFromBamFile(fileName):
    '''read scene from a file'''

    return GamePhysicsManager.get_global_ptr().read_from_bam_file(fileName)


def writeToBamFileAndExit(fileName):
    '''write scene to a file (and exit)'''

    GamePhysicsManager.get_global_ptr().write_to_bam_file(fileName)
    # # this is for testing explicit removal and destruction of all elements
    physicsMgr = GamePhysicsManager.get_global_ptr()
    # destroy vehicles
    for vehicleTmp in physicsMgr.get_vehicles():
        # destroy vehicleTmp
        physicsMgr.destroy_vehicle(NodePath.any_path(vehicleTmp))
    # destroy characterControllers
    for characterControllerTmp in physicsMgr.get_character_controllers():
        # destroy characterControllers
        physicsMgr.destroy_character_controller(NodePath.any_path(
            characterControllerTmp))
    # destroy constraints
    for constraintTmp in physicsMgr.get_constraints():
        # destroy constraintTmp
        physicsMgr.destroy_constraint(NodePath.any_path(constraintTmp))
    # destroy rigid bodies
    for rigid_bodyTmp in physicsMgr.get_rigid_bodies():
        # destroy rigid_bodyTmp
        physicsMgr.destroy_rigid_body(NodePath.any_path(rigid_bodyTmp))
    # destroy ghosts
    for ghostTmp in physicsMgr.get_ghosts():
        # destroy ghostTmp
        physicsMgr.destroy_ghost(NodePath.any_path(ghostTmp))
    #
    sys.exit(0)


def loadPlane(name, app, width=30.0, depth=30.0, upAxis=GamePhysicsManager.Z_up,
              texture='dry-grass.png'):
    '''load plane stuff centered at (0,0,0)'''

    # Vertex Format
    arrayFormat = GeomVertexArrayFormat()
    arrayFormat.add_column(InternalName.make('vertex'), 3,
                           Geom.NT_float32, Geom.C_point)
    arrayFormat.add_column(InternalName.make('normal'), 3,
                           Geom.NT_float32, Geom.C_vector)
    arrayFormat.add_column(InternalName.make('texcoord'), 2,
                           Geom.NT_float32, Geom.C_texcoord)
    vertexFormat = GeomVertexFormat()
    vertexFormat.add_array(arrayFormat)
    # Pre-defined vertex formats
#         vertexFormatAdded = GeomVertexFormat.get_v3n3t2()
    vertexFormatAdded = GeomVertexFormat.register_format(vertexFormat)
    # Vertex Data
    vertexData = GeomVertexData('plane', vertexFormatAdded, Geom.UH_static)
    vertex = GeomVertexWriter(vertexData, 'vertex')
    normal = GeomVertexWriter(vertexData, 'normal')
    texcoord = GeomVertexWriter(vertexData, 'texcoord')
    # compute coords and normal according to up axis
    # 3------2      ^y           ^z           ^x
    # |      |      |            |            |
    # |      | +d   | Z_up   OR  | X_up   OR  | Y_up
    # |      |      |            |            |
    # 0------1      -----.x     -----.y     -----.z
    #    +w
    w = abs(width) / 0.5
    d = abs(depth) / 0.5
    # default: Z_up
    n = LVector3f(0.0, 0.0, 1.0)
    v0 = LPoint3f(-w, -d, 0.0)
    v1 = LPoint3f(w, -d, 0.0)
    v2 = LPoint3f(w, d, 0.0)
    v3 = LPoint3f(-w, d, 0.0)
    if upAxis == GamePhysicsManager.X_up:
        n = LVector3f(1.0, 0.0, 0.0)
        v0 = LPoint3f(0.0, -w, -d)
        v1 = LPoint3f(0.0, w, -d)
        v2 = LPoint3f(0.0, w, d)
        v3 = LPoint3f(0.0, -w, d)
    elif upAxis == GamePhysicsManager.Y_up:
        n = LVector3f(0.0, 1.0, 0.0)
        v0 = LPoint3f(-d, 0.0, -w)
        v1 = LPoint3f(d, 0.0, -w)
        v2 = LPoint3f(d, 0.0, w)
        v3 = LPoint3f(-d, 0.0, w)
    # normalize
    n.normalize()
    # fill-up vertex data (plane)
    # vertex 0
    vertex.add_data3f(v0)
    normal.add_data3f(n)
    texcoord.add_data2f(0.0, 0.0)
    # vertex 1
    vertex.add_data3f(v1)
    normal.add_data3f(n)
    texcoord.add_data2f(1.0, 0.0)
    # vertex 2
    vertex.add_data3f(v2)
    normal.add_data3f(n)
    texcoord.add_data2f(1.0, 1.0)
    # vertex 3
    vertex.add_data3f(v3)
    normal.add_data3f(n)
    texcoord.add_data2f(0.0, 1.0)
    # Creating the GeomPrimitive objects for plane
    planeTriangles = GeomTriangles(Geom.UH_static)
    # lower triangle
    planeTriangles.add_vertices(0, 1, 3)
    # higher triangle
    planeTriangles.add_vertices(2, 3, 1)
    # Putting your new geometry in the scene graph
    planeGeom = Geom(vertexData)
    planeGeom.add_primitive(planeTriangles)
    planeNode = GeomNode(name + 'Node')
    planeNode.add_geom(planeGeom)
    planeNP = NodePath(planeNode)
    # apply texture
    tex = app.loader.load_texture(texture)
    planeNP.set_texture(tex)
    #
    return planeNP


def loadTerrainLowPoly(app, name, widthScale=128, heightScale=64.0,
                       texture='dry-grass.png'):
    '''load terrain low poly stuff'''

    terrainNP = app.loader.load_model('terrain-low-poly.egg')
    terrainNP.set_name(name)
    terrainNP.set_transform(TransformState.make_identity())
    terrainNP.set_scale(widthScale, widthScale, heightScale)
    tex = app.loader.load_texture(texture)
    terrainNP.set_texture(tex)
    return terrainNP


terrainRootNetPos = None


def terrainUpdate(app, task):
    '''terrain update'''

    global terrain, terrainRootNetPos
    # set focal point
    # see https:#www.panda3d.org/forums/viewtopic.php?t=5384
    focalPointNetPos = app.camera.get_net_transform().get_pos()
    terrain.set_focal_point(focalPointNetPos - terrainRootNetPos)
    # update every frame
    terrain.update()
    #
    return task.cont


def loadTerrain(app, name, widthScale=0.5, heightScale=10.0):
    '''load terrain stuff'''

    global terrainRootNetPos

    terrain = GeoMipTerrain('terrain')
    heightField = PNMImage(Filename(dataDirs[1] + '/heightfield.png'))
    terrain.set_heightfield(heightField)
    terrain.get_root().set_transform(TransformState.make_identity())
    # sizing
    environmentWidthX = (heightField.get_x_size() - 1) * widthScale
    environmentWidthY = (heightField.get_y_size() - 1) * widthScale
    environmentWidth = (environmentWidthX + environmentWidthY) / 2.0
    terrain.get_root().set_sx(widthScale)
    terrain.get_root().set_sy(widthScale)
    terrain.get_root().set_sz(heightScale)
    # set other terrain's properties
    blockSize, minimumLevel = (64, 0)
    nearPercent, farPercent = (0.1, 0.7)
    terrainLODmin = min(minimumLevel, terrain.get_max_level())
    flattenMode = GeoMipTerrain.AFM_off
    terrain.set_block_size(blockSize)
    terrain.set_near(nearPercent * environmentWidth)
    terrain.set_far(farPercent * environmentWidth)
    terrain.set_min_level(terrainLODmin)
    terrain.set_auto_flatten(flattenMode)
    # terrain texturing
    textureStage0 = TextureStage('TextureStage0')
    textureImage = TexturePool.load_texture(Filename('terrain.png'))
    terrain.get_root().set_tex_scale(textureStage0, 1.0, 1.0)
    terrain.get_root().set_texture(textureStage0, textureImage, 1)
    # reparent this Terrain node path to the object node path
#     terrain.get_root().set_collide_mask(mask) fixme
    terrain.get_root().set_name(name)
    # brute force generation
    bruteForce = True
    terrain.set_bruteforce(bruteForce)
    # Generate the terrain
    terrain.generate()
    # check if terrain needs update or not
    if not bruteForce:
        # save the net pos of terrain root
        terrainRootNetPos = terrain.get_root().get_net_transform().get_pos()
        # Add a task to keep updating the terrain
        app.taskMgr.add(terrainUpdate, 'terrainUpdate',
                        extraArgs=[app], appendTask=True)
    #
    return terrain


def getModelAnims(name, scale, modelFileIdx, modelAnimCtls, app, modelFile, modelAnimFiles):
    '''get model and animations'''

    # get some models, with animations
    # get the model
    modelNP = app.loader.load_model(modelFile[modelFileIdx])
    # set the name
    modelNP.set_name(name)
    # set scale
    modelNP.set_scale(scale)
    # get animations if requested
    if modelAnimCtls:
        # associate an anim with a given anim control
        tmpAnims = AnimControlCollection()
        modelAnimNP = [None, None]
        modelAnimCtls.append([None, None])
        if(len(modelAnimFiles[modelFileIdx][0]) != 0) and \
                (len(modelAnimFiles[modelFileIdx][1]) != 0):
            # first anim . modelAnimCtls[i][0]
            modelAnimNP[0] = app.loader.load_model(
                modelAnimFiles[modelFileIdx][0])
            modelAnimNP[0].reparent_to(modelNP)
            auto_bind(modelNP.node(), tmpAnims,
                      PartGroup.HMF_ok_part_extra |
                      PartGroup.HMF_ok_anim_extra |
                      PartGroup.HMF_ok_wrong_root_name)
            modelAnimCtls[-1][0] = tmpAnims.get_anim(0)
            tmpAnims.clear_anims()
            modelAnimNP[0].detach_node()
            # second anim . modelAnimCtls[i][1]
            modelAnimNP[1] = app.loader.load_model(
                modelAnimFiles[modelFileIdx][1])
            modelAnimNP[1].reparent_to(modelNP)
            auto_bind(modelNP.node(), tmpAnims,
                      PartGroup.HMF_ok_part_extra |
                      PartGroup.HMF_ok_anim_extra |
                      PartGroup.HMF_ok_wrong_root_name)
            modelAnimCtls[-1][1] = tmpAnims.get_anim(0)
            tmpAnims.clear_anims()
            modelAnimNP[1].detach_node()
            # reparent all node paths
            modelAnimNP[0].reparent_to(modelNP)
            modelAnimNP[1].reparent_to(modelNP)
    #
    return modelNP


# debug flag
toggleDebugFlag = [False]


def toggleDebugDraw(toggleDebugFlag):
    '''toggle debug draw'''

    toggleDebugFlag[0] = not toggleDebugFlag[0]
    GamePhysicsManager.get_global_ptr().debug(toggleDebugFlag[0])


def createRigidBodyXYZ(modelNP, name, shapeType, upAxis, pos, hpr,
                       scaleOrDims=LVecBase3f(1, 1, 1), isScale=True):
    '''create a rigid body oriented along X,Y,Z axis'''

    np = GamePhysicsManager.get_global_ptr().reference_node_path.attach_new_node(
        name)
    modelNP.instance_to(np)
    scale = LVecBase3f()
    if isScale:
        scale = scaleOrDims
    else:
        sizes2 = LVecBase3f()
        modelDeltaCenter = LVector3f()
        GamePhysicsManager.get_global_ptr().get_bounding_dimensions(
            modelNP, sizes2, modelDeltaCenter)
        scale = LVecBase3f(abs(scaleOrDims.get_x() / sizes2.get_x()),
                           abs(scaleOrDims.get_y() / sizes2.get_y()),
                           abs(scaleOrDims.get_z() / sizes2.get_z()))
    np.set_pos_hpr_scale(pos, hpr, scale)
    setParametersBeforeCreation(GamePhysicsManager.RIGIDBODY, name, upAxis)
    GamePhysicsManager.get_global_ptr().set_parameter_value(
        GamePhysicsManager.RIGIDBODY, 'shape_type', shapeType)
    return GamePhysicsManager.get_global_ptr().create_rigid_body(
        str('RigidBody') + name)


def createConstraint(objectA, objectB, name,
                     contraint_type, use_world_reference,
                     pivot_references1, pivot_references2,
                     axis_references1, axis_references2,
                     rotation_references1, rotation_references2,
                     world_axes1, world_axes2,
                     world_anchor, ratio, rotation_order, use_reference_frame_a,
                     initialize=False):
    '''create a constraint'''

    physicsMgr = GamePhysicsManager.get_global_ptr()
    if not initialize:
        # create the constraint and get a reference
        cs = physicsMgr.create_constraint(name).node()
        # set the constraint type
        cs.set_constraint_type(contraint_type)
        # set the objects
        objects = ValueList_NodePath()
        objects.add_value(objectA)
        objects.add_value(objectB)
        cs.set_owner_objects(objects)
        # # set the parameters
        # world/local references
        cs.set_use_world_reference(use_world_reference)
        # references
        points = ValueList_LPoint3f()
        points.add_value(pivot_references1)
        points.add_value(pivot_references2)
        cs.set_pivot_references(points)
        vectors = ValueList_LVector3f()
        vectors.add_value(axis_references1)
        vectors.add_value(axis_references2)
        cs.set_axis_references(vectors)
        hpr = ValueList_LVecBase3f()
        hpr.add_value(rotation_references1)
        hpr.add_value(rotation_references2)
        cs.set_rotation_references(hpr)
        # world axes
        wectors = ValueList_LVector3f()
        wectors.add_value(world_axes1)
        wectors.add_value(world_axes2)
        cs.set_world_axes(wectors)
        # ratio, rotation rotation_order, world world_anchor, use reference
        # frame a
        cs.set_ratio(ratio)
        cs.set_rotation_order(rotation_order)
        cs.set_world_anchor(world_anchor)
        cs.set_use_reference_frame_a(use_reference_frame_a)
    else:
        # arguments must be strings
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'objectA', objectA)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'objectB', objectB)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'contraint_type', contraint_type)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'use_world_reference', use_world_reference)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'pivot_references',
                                       pivot_references1 + ':' + pivot_references2)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'axis_references',
                                       axis_references1 + ':' + axis_references2)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'rotation_references',
                                       rotation_references1 + ':' +
                                       rotation_references2)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'world_axes', world_axes1 + ':' +
                                       world_axes2)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'world_anchor', world_anchor)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'ratio', ratio)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'rotation_order', rotation_order)
        physicsMgr.set_parameter_value(GamePhysicsManager.CONSTRAINT,
                                       'use_reference_frame_a',
                                       use_reference_frame_a)
        # create the constraint and get a reference
        cs = physicsMgr.create_constraint(name).node()
    #
    return cs
