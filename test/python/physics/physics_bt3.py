'''
Created on Oct 09, 2016

@author: consultit
'''

from ely.physics import GamePhysicsManager, BT3Constraint, BT3RigidBody, \
    BT3PhysicsInfo
from common import startFramework, printCreationParameters, loadPlane, \
    setParametersBeforeCreation, readFromBamFile, writeToBamFileAndExit, app, \
    loadTerrainLowPoly, loadTerrain, getModelAnims, toggleDebugDraw, \
    createRigidBodyXYZ, createConstraint, modelFile, modelAnimFiles, sceneNP, \
    groupMask, collideMask, updateTask, userData, bamFileName, globalClock, \
    toggleDebugFlag, playerAnimCtls
from panda3d.core import NodePath, LVector3f, ClockObject, TextNode, LPoint3f, \
    LVecBase3f
from panda3d.direct import CLerpNodePathInterval, CMetaInterval, \
    CIntervalManager
from panda3d.direct import CLerpInterval
from direct.task import Task

# # local data definitions
# player specifics
playerRigidBody = None
# ghost
canGhost = None
# vehicle
vehicle = None
moveEvent = None
steadyEvent = None
# character controller
characterController = None


#
def updateControls(task):
    '''custom update task for controls'''

    global playerRigidBody
    # call update for controls
    dt = ClockObject.get_global_clock().get_dt()
    playerRigidBody.update(dt)
    #
    return task.cont


delay = 0


def rigidBodyCallback(rigidBody):
    '''rigid body update callback function'''

    global playerRigidBody, delay
    if rigidBody != playerRigidBody:
        return
    if delay >= 60:
        currentVelSize = rigidBody.get_linear_velocity().length()
        print('box velocity: ' + str(currentVelSize))
        delay = 0
    else:
        delay += 1


def genericEventNotify(name, object0, object1):
    '''generic event notify'''

    print('got \'' + name + '\' between \'' + object0.get_name() +
          '\' and \'' + object1.get_name() + '\'')


def vehicleEventNotify(name, object0):
    '''vehicle event notify'''

    print('got \'' + name + '\' for \'' + object0.get_name() + '\' vehicle.')


def characterControllerEventNotify(name, object0):
    '''character controller event notify'''

    print('got \'' + name + '\' for \'' +
          object0.get_name() + '\' character controller.')


def getGhostOverlappingObjects(ghost):
    '''get ghost's overlapping objects'''

    res = 'Overlapping objects: '
    for overlapObj in ghost.overlapping_objects:
        res += overlapObj.get_name() + ', '
    print(res + '\n')


targetPosInterval1 = []
targetPosInterval2 = []
targetHprInterval1 = []
targetHprInterval2 = []
targetPace = []


def letMoveBackForth(target, duration, startPos, endPos, startHpr, endHpr):
    '''let target move back and forth'''

    global targetPosInterval1, targetPosInterval2, targetHprInterval1, \
        targetHprInterval2, targetPace
    # Create the lerp intervals needed to walk back and forth
    walkTime = duration * 0.9 / 2.0
    turnTime = duration * 0.1 / 2.0
    # walk
    targetPosInterval1.append(CLerpNodePathInterval('targetPosInterval1' +
                                                    str(len(targetPosInterval1)),
                                                    walkTime, CLerpInterval.BT_no_blend,
                                                    True, False, target, NodePath()))
    targetPosInterval1[-1].set_start_pos(startPos)
    targetPosInterval1[-1].set_end_pos(endPos)

    targetPosInterval2.append(CLerpNodePathInterval('targetPosInterval2' +
                                                    str(len(targetPosInterval2)),
                                                    walkTime, CLerpInterval.BT_no_blend,
                                                    True, False, target, NodePath()))
    targetPosInterval2[-1].set_start_pos(endPos)
    targetPosInterval2[-1].set_end_pos(startPos)

    # turn
    targetHprInterval1.append(CLerpNodePathInterval('targetHprInterval1' +
                                                    str(len(
                                                        targetHprInterval1)), turnTime,
                                                    CLerpInterval.BT_no_blend,
                                                    True, False, target, NodePath()))
    targetHprInterval1[-1].set_start_hpr(startHpr)
    targetHprInterval1[-1].set_end_hpr(endHpr)

    targetHprInterval2.append(CLerpNodePathInterval('targetHprInterval2' +
                                                    str(len(
                                                        targetHprInterval2)), turnTime,
                                                    CLerpInterval.BT_no_blend,
                                                    True, False, target, NodePath()))
    targetHprInterval2[-1].set_start_hpr(endHpr)
    targetHprInterval2[-1].set_end_hpr(startHpr)

    # Create and play the sequence that coordinates the intervals
    targetPace.append(CMetaInterval('targetPace' + str(len(targetPace))))
    targetPace[-1].add_c_interval(targetPosInterval1[-1], 0,
                                  CMetaInterval.RS_previous_end)
    targetPace[-1].add_c_interval(targetHprInterval1[-1], 0,
                                  CMetaInterval.RS_previous_end)
    targetPace[-1].add_c_interval(targetPosInterval2[-1], 0,
                                  CMetaInterval.RS_previous_end)
    targetPace[-1].add_c_interval(targetHprInterval2[-1], 0,
                                  CMetaInterval.RS_previous_end)
    targetPace[-1].loop()


# [f,f-stop,b,b-stop,l,l-stop,r,r-stop,b,b-stop]
vehicleKeys = ['i', 'i-up', 'k', 'k-up', 'j', 'j-up', 'l', 'l-up', 'b', 'b-up']


def vehicleController(e, data):
    '''vehicle controller'''

    name = e
    vehicle = data
    if name == vehicleKeys[0]:
        vehicle.set_move_forward(True)
    elif name == vehicleKeys[1]:
        vehicle.set_move_forward(False)
    elif name == vehicleKeys[2]:
        vehicle.set_move_backward(True)
    elif name == vehicleKeys[3]:
        vehicle.set_move_backward(False)
    elif name == vehicleKeys[4]:
        vehicle.set_rotate_head_left(True)
    elif name == vehicleKeys[5]:
        vehicle.set_rotate_head_left(False)
    elif name == vehicleKeys[6]:
        vehicle.set_rotate_head_right(True)
    elif name == vehicleKeys[7]:
        vehicle.set_rotate_head_right(False)
    elif name == vehicleKeys[8]:
        vehicle.set_brake(True)
    elif name == vehicleKeys[9]:
        vehicle.set_brake(False)


# [f,f-stop,b,b-stop,l,l-stop,r,r-stop,j,j-stop]
characterKeys = ['arrow_up', 'arrow_up-up', 'arrow_down', 'arrow_down-up',
                 'arrow_left', 'arrow_left-up', 'arrow_right', 'arrow_right-up', 'm',
                 'm-up']


def characterControllerClbk(e, data):
    '''character controller'''

    name = e
    characterController = data
    if name == characterKeys[0]:
        characterController.set_move_forward(True)
    elif name == characterKeys[1]:
        characterController.set_move_forward(False)
    elif name == characterKeys[2]:
        characterController.set_move_backward(True)
    elif name == characterKeys[3]:
        characterController.set_move_backward(False)
    elif name == characterKeys[4]:
        characterController.set_rotate_head_left(True)
    elif name == characterKeys[5]:
        characterController.set_rotate_head_left(False)
    elif name == characterKeys[6]:
        characterController.set_rotate_head_right(True)
    elif name == characterKeys[7]:
        characterController.set_rotate_head_right(False)
    elif name == characterKeys[8]:
        characterController.set_jump(True)
    elif name == characterKeys[9]:
        characterController.set_jump(False)


if __name__ == '__main__':

    msg = 'BT3RigidBody & BT3Ghost & BT3Constraint & BT3Vehicle & BT3CharacterController test'
    app, bamFile, renderPipeline, heightfield, physicsMT = startFramework(msg)

    # # here is room for your own code
    # print some help to screen
    text = TextNode('Help')
    text.set_text(
        msg + '\n\n'
        '- press \'d\' to toggle debug drawing\n'
        '- press \'up\'/\'left\'/\'down\'/\'right\' arrows to move the player\n'
        '- press \'a\' to pick up the object under the mouse\n')
    textNodePath = app.aspect2d.attach_new_node(text)
    textNodePath.set_pos(-1.25, 0.0, 0.8)
    textNodePath.set_scale(0.035)
    if renderPipeline:
        textNodePath.set_color(0, 0, 0)

    # create a physics manager
    physicsMgr = None
    if physicsMT:
        # Multi-threaded physics
        physicsInfo = BT3PhysicsInfo()
        physicsInfo.set_world_type(BT3PhysicsInfo.DISCRETE_MT_WORLD)
        physicsInfo.set_broad_phase_type(BT3PhysicsInfo.DBVT_BROADPHASE)
        physicsInfo.set_solver_type(
            BT3PhysicsInfo.SEQUENTIAL_IMPULSE_CSOLVER_MT)
        physicsMgr = GamePhysicsManager(10, app.render, groupMask, collideMask,
                                        physicsInfo)
    else:
        physicsMgr = GamePhysicsManager(10, app.render, groupMask, collideMask)

    # enable object picking
    physicsMgr.enable_picking(app.render, NodePath.any_path(app.camNode),
                              app.mouseWatcher.node(), 'a', 'a-up')

    # print creation parameters: defult values
    print('\n' + 'Default creation parameters:')
    printCreationParameters()

    # load or restore all scene stuff: if passed an argument
    # try to read it from bam file
    if (not bamFile) or (not readFromBamFile(bamFile)):
        # no argument or no valid bamFile
        # reparent reference node to render
        physicsMgr.get_reference_node_path().reparent_to(app.render)

        # # Static environment
        # get a sceneNP, naming it with 'SceneNP' to ease restoring from bam
        # file
        pos = LVector3f(0.0, 0.0, 0.0)
        hpr = LVecBase3f(45.0, 0.0, 0.0)
        # # plane
#         planeUpAxis = GamePhysicsManager.Z_up # Z_up X_up Y_up
#         sceneNP = loadPlane('SceneNP', app, 128.0, 128.0, planeUpAxis)
        # # triangle mesh
        sceneNP = loadTerrainLowPoly(app, 'SceneNP', 128, 128.0)
        # # terrain
#         userData = loadTerrain(app, 'SceneNP', 1.0, 60.0)
#         sceneNP = userData.get_root()
#         pos = LVector3f(-256.0, -256.0, 0.0)
#         hpr = LVecBase3f(0.0, 0.0, 0.0)

        # set sceneNP transform
        sceneNP.set_pos_hpr(pos, hpr)

        # create scene's rigid_body (attached to the reference node)
        sceneRigidBodyNP = physicsMgr.create_rigid_body('SceneRigidBody')
        # get a reference to the scene's rigid_body
        sceneRigidBody = sceneRigidBodyNP.node()

        # set some parameters
        # # plane
#         sceneRigidBody.set_up_axis(planeUpAxis)
#         sceneRigidBody.set_shape_type(GamePhysicsManager.PLANE)
        # # triangle mesh
        sceneRigidBody.set_shape_type(GamePhysicsManager.TRIANGLE_MESH)
        # # terrain
#         sceneRigidBody.set_shape_type(GamePhysicsManager.TERRAIN)
#         sceneRigidBody.set_user_data(userData)

        # set mass=0 before setup() for static (and kinematic) bodies
        sceneRigidBody.set_mass(0.0)
        # set the object for rigid body
        sceneRigidBody.set_owner_object(sceneNP)

        # setup
        sceneRigidBody.setup()
        # change to kinematic only after setup (as long as mass=0)
#         sceneRigidBody.switch_body_type(BT3RigidBody.STATIC)
        # change some attrs
        print(sceneRigidBody.collision_object_attrs.friction)
        sceneRigidBody.collision_object_attrs.friction = 0.3
        print(sceneRigidBody.collision_object_attrs.friction)

        # # Rigid Bodies
        modelNP = getModelAnims('modelNP', 1.0, 4, playerAnimCtls, app,
                                modelFile, modelAnimFiles)
        # # box
        playerNP = physicsMgr.get_reference_node_path().attach_new_node('playerNP')
        modelNP.instance_to(playerNP)
        # set shape type
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY,
                                       'shape_type', 'box')
        # set various creation parameters as string for other rigid bodies
        setParametersBeforeCreation(GamePhysicsManager.RIGIDBODY, 'playerNP',
                                    'no_up')
        # set transform
        playerNP.set_pos_hpr(LPoint3f(4.1, 0.0, 100.1),
                             LVecBase3f(-75.0, 145.0, -235.0))
        # create player's rigid_body (attached to the reference node)
        playerRigidBodyNP = physicsMgr.create_rigid_body(
            'PlayerRigidBody')
        # get a reference to the player's rigid_body
        playerRigidBody = playerRigidBodyNP.node()

        # # sphere
        sphereNP = NodePath('SphereNP')
        modelNP.instance_to(sphereNP)
        sphereNP.set_pos_hpr(LPoint3f(4.1, 0.0, 130.1),
                             LVecBase3f(145.0, -235.0, -75.0))
        setParametersBeforeCreation(GamePhysicsManager.RIGIDBODY, '',
                                    'no_up')
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY,
                                       'shape_type', 'sphere')
        # create && get a reference to a BT3RigidBody
        sphereRigidBody = physicsMgr.create_rigid_body(
            'RigidBodySphere').node()
        # setup
        sphereRigidBody.set_owner_object(sphereNP)
        sphereRigidBody.setup()
        # change some attrs
        print(sphereRigidBody.collision_object_attrs.friction)
        sphereRigidBody.collision_object_attrs.friction = 0.9
        print(sphereRigidBody.collision_object_attrs.friction)

        # # cylinders
        # z
        createRigidBodyXYZ(modelNP, 'cylinderZ', 'cylinder', 'z',
                           LPoint3f(4.1, 0.0, 160.1), LVecBase3f(
                               0.0, 0.0, 0.0),
                           LVecBase3f(1.0, 1.0, 3.0))
        # x
        createRigidBodyXYZ(modelNP, 'cylinderX', 'cylinder', 'x',
                           LPoint3f(4.1 - 15.1, 0.0,
                                    160.1), LVecBase3f(145.0, -75.0, -235.0),
                           LVecBase3f(3.0, 1.0, 1.0))
        # y
        createRigidBodyXYZ(modelNP, 'cylinderY', 'cylinder', 'y',
                           LPoint3f(4.1 + 15.1, 0.0,
                                    160.1), LVecBase3f(145.0, -75.0, -235.0),
                           LVecBase3f(1.0, 3.0, 1.0))

        # # capsules
        # z
        createRigidBodyXYZ(modelNP, 'capsuleZ', 'capsule', 'z',
                           LPoint3f(4.1, 0.0, 190.1), LVecBase3f(
                               0.0, 0.0, 0.0),
                           LVecBase3f(1.0, 1.0, 6.0))
        # x
        createRigidBodyXYZ(modelNP, 'capsuleX', 'capsule', 'x',
                           LPoint3f(4.1 - 15.1, 0.0,
                                    190.1), LVecBase3f(145.0, -75.0, -235.0),
                           LVecBase3f(6.0, 1.0, 1.0))
        # y
        createRigidBodyXYZ(modelNP, 'capsuleY', 'capsule', 'y',
                           LPoint3f(4.1 + 15.1, 0.0,
                                    190.1), LVecBase3f(145.0, -75.0, -235.0),
                           LVecBase3f(1.0, 6.0, 1.0))

        # # cones
        # z
        createRigidBodyXYZ(modelNP, 'coneZ', 'cone', 'z',
                           LPoint3f(4.1, 0.0, 210.1), LVecBase3f(
                               0.0, 0.0, 0.0),
                           LVecBase3f(1.0, 1.0, 3.0))
        # x
        createRigidBodyXYZ(modelNP, 'coneX', 'cone', 'x',
                           LPoint3f(4.1 - 15.1, 0.0,
                                    210.1), LVecBase3f(145.0, -75.0, -235.0),
                           LVecBase3f(3.0, 1.0, 1.0))
        # y
        createRigidBodyXYZ(modelNP, 'coneY', 'cone', 'y',
                           LPoint3f(4.1 + 15.1, 0.0,
                                    210.1), LVecBase3f(145.0, -75.0, -235.0),
                           LVecBase3f(1.0, 3.0, 1.0))

        # # Constraints
        modelNP = getModelAnims('modelNP', 1.0, 5, playerAnimCtls, app,
                                modelFile, modelAnimFiles)
        rbANP = NodePath()
        rbBNP = NodePath()
        # # point2point (use local reference)
        rbANP = createRigidBodyXYZ(modelNP, 'rbA1', 'cylinder', 'z',
                                   LPoint3f(-60.0, 0.0,
                                            20.0), LVecBase3f(0.0, 0.0, 0.0),
                                   LVecBase3f(2.0, 2.0, 2.0))
        rbBNP = createRigidBodyXYZ(modelNP, 'rbB1', 'capsule', 'z',
                                   LPoint3f(-50.0, 0.0,
                                            20.0), LVecBase3f(0.0, 0.0, 0.0),
                                   LVecBase3f(2.0, 2.0, 2.0))
        p2p = createConstraint(rbANP, rbBNP, 'point2point',
                               BT3Constraint.POINT2POINT, False,
                               LPoint3f(
                                   5.0, 0.0, 0.0), LPoint3f(-5.0, 0.0, 0.0),
                               LVector3f.zero(), LVector3f.zero(),
                               LVecBase3f.zero(), LVecBase3f.zero(),
                               LVector3f.zero(), LVector3f.zero(),
                               LPoint3f.zero(), 0.0, BT3Constraint.RO_XYZ, False)
        p2p.setup()
        # # hinge (use global reference)
        rbANP = createRigidBodyXYZ(modelNP, 'rbA2', 'cylinder', 'z',
                                   LPoint3f(50.0, 0.0, 20.0), LVecBase3f(
                                       45.0, 0.0, 0.0),
                                   LVecBase3f(2.0, 2.0, 2.0))
        rbBNP = createRigidBodyXYZ(modelNP, 'rbB2', 'capsule', 'z',
                                   LPoint3f(70.0, 0.0, 20.0), LVecBase3f(
                                       0.0, 0.0, 0.0),
                                   LVecBase3f(2.0, 2.0, 2.0))
        hinge = createConstraint(rbANP, rbBNP, 'hinge',
                                 BT3Constraint.HINGE, True,
                                 LPoint3f(60.0, 0.0, 20.0), LPoint3f.zero(),
                                 LVector3f(0.0, 0.0, 1.0), LVector3f.zero(),
                                 LVecBase3f.zero(), LVecBase3f.zero(),
                                 LVector3f(0.0, 0.0, 1.0), LVector3f.zero(),
                                 LPoint3f.zero(), 0.0, BT3Constraint.RO_XYZ, False)
        hinge.setup()
        # change some attrs
        print(hinge.constraints_attrs.angular_only)
        hinge.constraints_attrs.angular_only = True
        print(hinge.constraints_attrs.angular_only)

        # # hinge_accumulated,hinge2,cone_twist,dof6,dof6_spring,dof6_spring2,fixed,universal,slider,contact,gear

        # # Ghosts
        modelNP = getModelAnims('modelNP', 1.0, 5, playerAnimCtls, app,
                                modelFile, modelAnimFiles)
        # # ghost
        canNP = physicsMgr.get_reference_node_path().attach_new_node('canNP')
        modelNP.instance_to(canNP)
        # set shape type and events
        physicsMgr.set_parameter_value(GamePhysicsManager.GHOST,
                                       'shape_type', 'cylinder')
        physicsMgr.set_parameter_value(GamePhysicsManager.GHOST,
                                       'thrown_events', 'overlap@OVERLAP@0.1')
        # set various creation parameters as string for other ghosts
        setParametersBeforeCreation(GamePhysicsManager.GHOST, 'canNP', 'x')
        # set scale, transform
        canNP.set_pos_hpr_scale(LPoint3f(0.0, 0.0, 6.0),
                                LVecBase3f(0.0, 0.0, 90.0), LVecBase3f(1.0, 5.0, 5.0))
        # create player's rigid_body (attached to the reference node)
        canGhostNP = physicsMgr.create_ghost('CanGhost')
        # get a reference to the player's rigid_body
        canGhost = canGhostNP.node()
        # set visible for debugging
        canGhost.set_visible(True)

        # #Vehicle
        # create the chassis
        chassisModel = getModelAnims('chassisModel', 4.0, 6, playerAnimCtls,
                                     app, modelFile, modelAnimFiles)
        chassisNP = createRigidBodyXYZ(chassisModel, 'chassis', 'box', 'z',
                                       LPoint3f(
                                           20.0, -20.0, 10.0), LVecBase3f(0.0, 0.0, 0.0),
                                       LVecBase3f(1.0, 1.0, 1.0))
        chassis = chassisNP.node()
        chassis.set_mass(1.0)
        chassis.switch_body_type(BT3RigidBody.DYNAMIC)
        # set events (default names)
        moveEvent = chassisNP.get_name() + '_Move'
        steadyEvent = chassisNP.get_name() + '_Steady'
        physicsMgr.set_parameter_value(GamePhysicsManager.VEHICLE,
                                       'thrown_events',
                                       'move@' + moveEvent + '@0.1:steady@' + steadyEvent
                                       + '@0.1')
        # create the vehicle
        vehicle = physicsMgr.create_vehicle('Vehicle').node()
        # set the vehicle's chassis (i.e. owner object)
        vehicle.set_owner_object(chassisNP)
        # set vehicle's wheels' models
        wheelNP = getModelAnims('wheelNP', 4.0, 7, playerAnimCtls, app,
                                modelFile, modelAnimFiles)
        vehicle.set_wheel_model(wheelNP, True)
        vehicle.set_wheel_model(wheelNP, False)
        # setup the vehicle
        vehicle.setup()

        # #Character controller
        charModelNP = getModelAnims('charModelNP', 1.0, 1, playerAnimCtls, app,
                                    modelFile, modelAnimFiles)
        # set scale, transform
        charModelNP.set_pos_hpr_scale(LPoint3f(0.0, -30.0, 0.1),
                                      LVecBase3f(0.0, 0.0, 0.0), LVecBase3f(2.0, 2.0, 2.0))
        # set shape type and events
        physicsMgr.set_parameters_defaults(GamePhysicsManager.GHOST)
        physicsMgr.set_parameter_value(GamePhysicsManager.GHOST,
                                       'shape_type', 'capsule')
        # create character's ghost (attached to the reference node)
        characterNP = physicsMgr.create_ghost('CharacterGhost')
        # get a reference to the player's rigid_body
        characterGhost = characterNP.node()
        # set owner object and setup
        characterGhost.set_owner_object(charModelNP)
        characterGhost.setup()
        # create the character controller
        physicsMgr.set_parameters_defaults(
            GamePhysicsManager.CHARACTERCONTROLLER)
        physicsMgr.set_parameter_value(GamePhysicsManager.CHARACTERCONTROLLER,
                                       'thrown_events', 'on_ground@@0.1:in_air@@0.1m')
        characterController = physicsMgr.create_character_controller(
            'CharacterController').node()
        # set the character controller's character (i.e. owner object)
        characterController.set_owner_object(characterNP)
        # setup the character controller
        characterController.setup()
    else:
        # valid bamFile
        # reparent reference node to render
        physicsMgr.get_reference_node_path().reparent_to(app.render)

        # restore sceneNP: through panda3d
        sceneNP = physicsMgr.get_reference_node_path().find('**/SceneNP')

        # restore playerRigidBody: through physics manager
        for rigidBody in physicsMgr.get_rigid_bodies():
            if rigidBody.get_name() == 'PlayerRigidBody':
                playerRigidBody = rigidBody
                break
        # restore canGhost: through physics manager
        for ghost in physicsMgr.get_ghosts():
            if ghost.get_name() == 'CanGhost':
                canGhost = ghost
                break
        # restore vehicle: through physics manager
        for vehicleA in physicsMgr.get_vehicles():
            if vehicleA.get_name() == 'Vehicle':
                vehicle = vehicleA
                break
        moveEvent = vehicle.get_owner_object().get_name() + '_Move'
        steadyEvent = vehicle.get_owner_object().get_name() + '_Steady'
        # restore characterController: through physics manager
        for characterControllerA in physicsMgr.get_character_controllers():
            if characterControllerA.get_name() == 'CharacterController':
                characterController = characterControllerA
                break

    # DEBUG DRAWING: make the debug reference node paths sibling of the
    # reference node
    physicsMgr.get_reference_node_path_debug().reparent_to(app.render)
    app.accept('d', toggleDebugDraw, [toggleDebugFlag])

    # enable collision notify event: BTRigidBody_BTRigidBody_Collision
    physicsMgr.enable_throw_event(GamePhysicsManager.COLLISIONNOTIFY, True, 0.1,
                                  'COLLISION')
    app.accept('COLLISION', genericEventNotify, ['COLLISION'])
    app.accept('COLLISIONOff', genericEventNotify, ['COLLISIONOff'])

    # ghost overlap notify events: OVERLAP
    app.accept('OVERLAP', genericEventNotify, ['OVERLAP'])
    app.accept('OVERLAPOff', genericEventNotify, ['OVERLAPOff'])

    # let ghost moving endlessly back and forth
    letMoveBackForth(NodePath.any_path(canGhost), 20.0,
                     LPoint3f(-20.0, 0.0, 6.0), LPoint3f(20.0, 0.0, 6.0),
                     LVecBase3f(0.0, 0.0, 90.0), LVecBase3f(180.0, 0.0, 90.0))

    # # first option: start the default update task for all plug-ins
    physicsMgr.start_default_update()
    playerRigidBody.set_update_callback(rigidBodyCallback)
    globalClock = ClockObject.get_global_clock()

    # # second option: start the custom update task for all plug-ins
#     app.taskMgr.add(updateControls, 'updateControls', 10, appendTask=True)

    # set ijkl + b (brake) to control the vehicle's movement
    app.accept(vehicleKeys[0], vehicleController, [vehicleKeys[0], vehicle])
    app.accept(vehicleKeys[1], vehicleController, [vehicleKeys[1], vehicle])
    app.accept(vehicleKeys[2], vehicleController, [vehicleKeys[2], vehicle])
    app.accept(vehicleKeys[3], vehicleController, [vehicleKeys[3], vehicle])
    app.accept(vehicleKeys[4], vehicleController, [vehicleKeys[4], vehicle])
    app.accept(vehicleKeys[5], vehicleController, [vehicleKeys[5], vehicle])
    app.accept(vehicleKeys[6], vehicleController, [vehicleKeys[6], vehicle])
    app.accept(vehicleKeys[7], vehicleController, [vehicleKeys[7], vehicle])
    app.accept(vehicleKeys[8], vehicleController, [vehicleKeys[8], vehicle])
    app.accept(vehicleKeys[9], vehicleController, [vehicleKeys[9], vehicle])
    # vehicle notify events: MOVE/STEADY
    app.accept(moveEvent, vehicleEventNotify, [moveEvent])
    app.accept(steadyEvent, vehicleEventNotify, [steadyEvent])

    # tweak some character controller's parameters
    characterController.linear_accel = LVector3f(3.0)
    characterController.max_linear_speed = LVector3f(6.0)
    characterController.linear_friction = 2.0
    characterController.angular_accel = 0.8
    characterController.max_angular_speed = 1.6
    characterController.angular_friction = 2.0
    characterController.jump_speed = 20.0
    # set keys to control the character controller's movement:
    app.accept(characterKeys[0], characterControllerClbk, [characterKeys[0],
                                                           characterController])
    app.accept(characterKeys[1], characterControllerClbk, [characterKeys[1],
                                                           characterController])
    app.accept(characterKeys[2], characterControllerClbk, [characterKeys[2],
                                                           characterController])
    app.accept(characterKeys[3], characterControllerClbk, [characterKeys[3],
                                                           characterController])
    app.accept(characterKeys[4], characterControllerClbk, [characterKeys[4],
                                                           characterController])
    app.accept(characterKeys[5], characterControllerClbk, [characterKeys[5],
                                                           characterController])
    app.accept(characterKeys[6], characterControllerClbk, [characterKeys[6],
                                                           characterController])
    app.accept(characterKeys[7], characterControllerClbk, [characterKeys[7],
                                                           characterController])
    app.accept(characterKeys[8], characterControllerClbk, [characterKeys[8],
                                                           characterController])
    app.accept(characterKeys[9], characterControllerClbk, [characterKeys[9],
                                                           characterController])
    # vehicle notify events: ONGROUND/INAIR
    app.accept('CharacterController_OnGround', characterControllerEventNotify,
               ['CharacterController_OnGround'])
    app.accept('CharacterController_InAir', characterControllerEventNotify,
               ['CharacterController_InAir'])

    # get ghost's overlapping objects
    app.accept('f4', getGhostOverlappingObjects, [canGhost])

    # write to bam file on exit
    app.win.set_close_request_event('close_request_event')
    app.accept('close_request_event', writeToBamFileAndExit, [bamFileName])

    # place camera
    trackball = app.trackball.node()
    trackball.set_pos(10.0, 200.0, -30.0)
    trackball.set_hpr(0.0, 10.0, 0.0)

    # intervals task
    def intervalTask(task):
        CIntervalManager.get_global_ptr().step()
        return Task.cont

    app.taskMgr.add(intervalTask, 'intervalTask')
    # app.run(), equals to do the main loop in C++
    app.run()
