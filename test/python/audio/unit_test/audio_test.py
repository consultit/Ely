'''
Created on Sep 24, 2018

@author: consultit
'''

import panda3d.core
import unittest
import ely.libtools
import suite
from ely.audio import GameAudioManager


def setUpModule():
    pass


def tearDownModule():
    pass


class CallbackTEST(unittest.TestCase):

    def setUp(self):
        self.audioMgr = GameAudioManager()

    def tearDown(self):
        pass

    def soundClbk(self, sound):
        print('soundClbk')
        self.assertEqual(sound.name, 'sound')

    def listenerClbk(self, listener):
        print('listenerClbk')
        self.assertEqual(listener.name, 'listener')

    def test(self):
        #
        sound = self.audioMgr.create_sound3d('sound')
        sound.node().set_update_callback(self.soundClbk)
        sound.node().update(0.016666667)
        sound.node().set_update_callback(None)
        sound.node().set_update_callback(self.soundClbk)
        sound.node().update(0.016666667)
        #
        listener = self.audioMgr.create_listener('listener')
        listener.node().set_update_callback(self.listenerClbk)
        listener.node().update(0.016666667)
        #
        self.audioMgr.destroy_sound3d(sound)
        self.audioMgr.destroy_listener(listener)


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTest(CallbackTEST('test'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
