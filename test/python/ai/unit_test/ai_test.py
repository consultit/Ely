'''
Created on May 01, 2020

@author: consultit
'''

import panda3d.core
import unittest
import ely.libtools
import suite


def setUpModule():
    pass


def tearDownModule():
    pass


class AITEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        self.assertTrue(True)


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTest(AITEST('test'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
