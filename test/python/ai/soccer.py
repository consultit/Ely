'''
Created on Jun 26, 2016

@author: consultit
'''

from ely.ai import GameAIManager, OSSteerPlugIn, OSSteerVehicle
from ely.libtools import ValueList_string
from panda3d.core import TextNode, ClockObject, AnimControlCollection, \
    auto_bind, LPoint3f
import sys
#
from common import startFramework, toggleDebugFlag, toggleDebugDraw, mask, \
    HandleVehicleData, printCreationParameters, handleVehicleEvent, \
    animRateFactor, readFromBamFile, bamFileName, writeToBamFileAndExit, \
    loadPlane, handleVehicles, mask

# # specific data/functions declarations/definitions
sceneNP = None
vehicleAnimCtls = []
steerPlugIn = None
steerVehicles = []

#


def setParametersBeforeCreation():
    '''set parameters as strings before plug-ins/vehicles creation'''

    steerMgr = GameAIManager.get_global_ptr()
    valueList = ValueList_string()
    # set plug-in type
    steerMgr.set_parameter_value(GameAIManager.STEERPLUGIN, 'plugin_type',
                                 'soccer')

    # set vehicle throwing events
    valueList.clear()
    valueList.add_value('avoid_neighbor@avoid_neighbor@30.0')
    steerMgr.set_parameter_values(GameAIManager.STEERVEHICLE,
                                  'thrown_events', valueList)
    #
    printCreationParameters()


def updatePlugIn(steerPlugIn, task):
    '''custom update task for plug-ins'''

    global steerVehicles, vehicleAnimCtls
    # call update for plug-in
    dt = ClockObject.get_global_clock().get_dt()
    steerPlugIn.update(dt)
    # handle vehicle's animation
    for i in range(len(vehicleAnimCtls)):
        if (vehicleAnimCtls[i][0] != None) and \
                (vehicleAnimCtls[i][1] != None):
            # get current velocity size
            currentVelSize = steerVehicles[i].settings.speed
            if currentVelSize > 0.0:
                if currentVelSize < 4.0:
                    animOnIdx = 0
                else:
                    animOnIdx = 1
                animOffIdx = (animOnIdx + 1) % 2
                # Off anim (0:walk, 1:run)
                if vehicleAnimCtls[i][animOffIdx].is_playing():
                    vehicleAnimCtls[i][animOffIdx].stop()
                # On amin (0:walk, 1:run)
                vehicleAnimCtls[i][animOnIdx].set_play_rate(
                    currentVelSize / animRateFactor[animOnIdx])
                if not vehicleAnimCtls[i][animOnIdx].is_playing():
                    vehicleAnimCtls[i][animOnIdx].loop(True)
            else:
                # stop any animation
                vehicleAnimCtls[i][0].stop()
                vehicleAnimCtls[i][1].stop()
    #
    return task.cont


def createSoccerVehicle(app, data, vehicleType):
    '''creates a generic vehicle for soccer plug-in'''

    global steerVehicles
    # set vehicle's type == player
    typeStr = 'player'
    maxForce = 3000.7
    maxSpeed = 10.0
    speed = 0.0
    if vehicleType == OSSteerVehicle.BALL:
        typeStr = 'ball'
        maxForce = 9.0
        maxSpeed = 9.0
        speed = 1.0
    GameAIManager.get_global_ptr().set_parameter_value(
        GameAIManager.STEERVEHICLE, 'vehicle_type', typeStr)

    oldPlayerNum = len(steerVehicles)
    # handle vehicle
    handleVehicles(app, data)
    if len(steerVehicles) > oldPlayerNum:
        # set vehicle's parameters
        steerVehicles[-1].settings.max_force = maxForce
        steerVehicles[-1].settings.max_speed = maxSpeed
        steerVehicles[-1].settings.speed = speed
        steerVehicles[-1].set_up_axis_fixed(True)
        return True
    return False


def addPlayerA(app, data=None):
    '''adds last created player to teamA'''

    global steerVehicles, steerPlugIn
    if data == None:
        return

    if createSoccerVehicle(app, data, OSSteerVehicle.PLAYER):
        # add to teamA
        steerPlugIn.add_player_to_team(steerVehicles[-1], OSSteerPlugIn.TEAM_A)


def addPlayerB(app, data=None):
    '''adds last created player to teamB'''

    global steerVehicles, steerPlugIn
    if data == None:
        return

    if createSoccerVehicle(app, data, OSSteerVehicle.PLAYER):
        # add to teamB
        steerPlugIn.add_player_to_team(steerVehicles[-1], OSSteerPlugIn.TEAM_B)


def addBall(app, data=None):
    '''adds a ball'''

    global steerVehicles, steerPlugIn
    if data == None:
        return

    createSoccerVehicle(app, data, OSSteerVehicle.BALL)


if __name__ == '__main__':

    msg = 'soccer test'
    app, bamFile, renderPipeline, heightfield = startFramework(msg)

    # # here is room for your own code
    # print some help to screen
    text = TextNode('Help')
    text.set_text(
        msg + '\n\n'
        '- press \'d\' to toggle debug drawing\n'
        '- press \'a\'/\'b\' to add a player to teamA/teamB\n'
        '- press \'p\' to add a ball\n')
    textNodePath = app.aspect2d.attach_new_node(text)
    textNodePath.set_pos(-1.25, 0.0, 0.90)
    textNodePath.set_scale(0.035)
    if renderPipeline:
        textNodePath.set_color(0, 0, 0)

    # create a steer manager; set root and mask to manage 'kinematic' vehicles
    steerMgr = GameAIManager(0, app.render, mask)

    # print creation parameters: defult values
    print('\n' + 'Default creation parameters:')
    printCreationParameters()

    # load or restore all scene stuff: if passed an argument
    # try to read it from bam file
    if (not bamFile) or (not readFromBamFile(bamFile)):
        # no argument or no valid bamFile
        # reparent the reference node to render
        steerMgr.get_reference_node_path().reparent_to(app.render)

        # get a sceneNP, naming it with 'SceneNP' to ease restoring from bam
        # file
        sceneNP = loadPlane('SceneNP', 128, 128)
        tex = app.loader.load_texture('soccer-field.png')
        sceneNP.set_texture(tex)
        # and reparent to the reference node
        sceneNP.reparent_to(steerMgr.get_reference_node_path())

        # set sceneNP's collide mask
        sceneNP.set_collide_mask(mask)

        # set creation parameters as strings before plug-in/vehicles creation
        print('\n' + 'Current creation parameters:')
        setParametersBeforeCreation()

        # create the plug-in (attached to the reference node)
        plugInNP = steerMgr.create_steer_plug_in('soccer plug-in')
        steerPlugIn = plugInNP.node()

        # set playing field
        steerPlugIn.set_playing_field(LPoint3f(-45.5, -35.5, 0.1),
                                      LPoint3f(45.5, 35.5, 0.1), 0.279)
    else:
        # valid bamFile
        # restore plug-in: through steer manager
        steerPlugIn = GameAIManager.get_global_ptr().get_steer_plug_in(0)
        # restore sceneNP: through panda3d
        sceneNP = GameAIManager.get_global_ptr().get_reference_node_path().find('**/SceneNP')
        # reparent the reference node to render
        GameAIManager.get_global_ptr().get_reference_node_path().reparent_to(app.render)

        # restore steer vehicles
        NUMVEHICLES = GameAIManager.get_global_ptr().get_num_steer_vehicles()
        tmpList = [None for i in range(NUMVEHICLES)]
        steerVehicles.extend(tmpList)
        vehicleAnimCtls.extend(tmpList)
        for i in range(NUMVEHICLES):
            # restore the steer vehicle: through steer manager
            steerVehicles[i] = GameAIManager.get_global_ptr(
            ).get_steer_vehicle(i)
            # restore animations
            tmpAnims = AnimControlCollection()
            auto_bind(steerVehicles[i], tmpAnims)
            vehicleAnimCtls[i] = [None, None]
            for j in range(tmpAnims.get_num_anims()):
                vehicleAnimCtls[i][j] = tmpAnims.get_anim(j)

        # set creation parameters as strings before other plug-ins/vehicles
        # creation
        print('\n' + 'Current creation parameters:')
        setParametersBeforeCreation()

    # # first option: start the default update task for all plug-ins
#     steerMgr.start_default_update()

    # # second option: start the custom update task for all plug-ins
    app.taskMgr.add(updatePlugIn, 'updatePlugIn', extraArgs=[steerPlugIn],
                    appendTask=True)

    # # set events' callbacks
    # toggle debug draw
    toggleDebugFlag = False
    app.accept('d', toggleDebugDraw, [steerPlugIn, app])

    # handle addition steer vehicles, models and animations
    playerAData = HandleVehicleData(0.7, 0, 'kinematic', sceneNP,
                                    steerPlugIn, steerVehicles, vehicleAnimCtls)
    app.accept('a', addPlayerA, [app, playerAData])
    playerBData = HandleVehicleData(0.7, 1, 'kinematic', sceneNP,
                                    steerPlugIn, steerVehicles, vehicleAnimCtls)
    app.accept('b', addPlayerB, [app, playerBData])
    ballData = HandleVehicleData(0.7, 3, 'kinematic', sceneNP,
                                 steerPlugIn, steerVehicles, vehicleAnimCtls)
    app.accept('p', addBall, [app, ballData])

    # handle OSSteerVehicle(s)' events
    app.accept('avoid_neighbor', handleVehicleEvent, ['avoid_neighbor'])

    # write to bam file on exit
    app.win.set_close_request_event('close_request_event')
    app.accept('close_request_event', writeToBamFileAndExit, [bamFileName])

    # place camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 180.0, -15.0)
    trackball.set_hpr(0.0, 15.0, 0.0)

    # app.run(), equals to do the main loop in C++
    app.run()
