/**
 * \file Game_init.h
 *
 * \date 2019-10-04
 * \author consultit
 */

#ifndef GAME_INIT_H_
#define GAME_INIT_H_

#include <string>
#include <nodePath.h>
#include <pandaFramework.h>

//(RmlUi) externs
extern std::string rmlUiBaseDir;

extern PandaFramework framework;

//utilities
void writeText(NodePath &textNode, const std::string &text, float scale,
		const LVecBase4 &color, const LVector3f &location,
		PandaFramework *framework);

struct Object
{
	Object(const std::string &objId) :
			objectId(objId)
	{
	}
	std::string objectId;
};
struct SteerPlugIn
{
	SteerPlugIn(const std::string &objId) :
			objectId(objId)
	{
	}
	std::string objectId;
};

void guiSystemInitialization();
void elyPreObjects_initialization();
void elyPostObjects_initialization(PandaFramework *framework);
void camera_initialization();

#endif /* GAME_INIT_H_ */
