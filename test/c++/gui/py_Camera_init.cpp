/**
 * \file Camera_init.cpp
 *
 * \date 2019-10-04
 * \author consultit
 */

#include <RmlUi/Core.h>
#include "py_Game_init.h"
#include "gameGUIManager.h"
#include "p3guiDataStructures.h"
#include "p3guiObjects.h"

//locals
namespace
{
//common text writing
NodePath textNode("textNode");
enum CameraType
{
	free_view_camera, chaser_camera, object_picker, none
} cameraType = none;
//PT(Object)camera;
bool cameraEnabled = false;
ParameterTable cameraDriverParams
{
{ "enabled", "false" },
{ "max_linear_speed", "40.0" },
{ "max_angular_speed", "40.0" },
{ "linear_accel", "30.0" },
{ "angular_accel", "30.0" },
{ "linear_friction", "5.0" },
{ "angular_friction", "5.0" },
{ "stop_threshold", "0.0" },
{ "fast_factor", "5.0" },
{ "sens_x", "0.05" },
{ "sens_y", "0.05" },
{ "pitch_limit", "true@60.0" },
{ "head_left", "enabled" },
{ "head_right", "enabled" },
{ "inverted_translation", "true" },
{ "inverted_rotation", "false" },
{ "mouse_enabled_h", "true" },
{ "mouse_enabled_p", "true" } };
ParameterTable cameraChaserParams
{
{ "enabled", "false" },
{ "backward", "false" },
{ "chased_object", "NONE" },
{ "fixed_relative_position", "false" },
{ "reference_object", "render" },
{ "abs_max_distance", "25.0" },
{ "abs_min_distance", "18.0" },
{ "abs_max_height", "8.0" },
{ "abs_min_height", "5.0" },
{ "friction", "5.0" },
{ "fixed_lookat", "false" },
{ "mouse_enabled_h", "true" },
{ "mouse_enabled_p", "true" },
{ "abs_lookat_distance", "5.0" },
{ "abs_lookat_height", "1.5" }, };
string chasedObject
{ "chasedObject" };
bool pickerOn = false;
bool pickerCsIspherical = true;

//add elements (tags) function for main menu
void rocketAddElements(P3GUIElementDocumentPtr mainMenu)
{
	//<button onclick="camera::options">Camera options</button><br/>
	P3GUIElementPtr content = mainMenu.get_element_by_id("content");
	P3GUIElementPtr exit = mainMenu.get_element_by_id("main::button::exit");
	if (!content.is_empty())
	{
		//create input element
		P3GUIDictionary params;
		params.set_string("onclick", "camera::options");
		P3GUIElementOwnerPtr input = P3GUIFactory::instance_element(
				P3GUIElementPtr(), "button", "button", params);
		P3GUIFactory::instance_element_text(P3GUIElementPtr(input), "Camera options");
		//create br element
		params.clear();
		params.set_string("id", "br");
		P3GUIElementOwnerPtr br = P3GUIFactory::instance_element(
				P3GUIElementPtr(), "br", "br", params);
		//insert elements
		content.insert_before(input, exit);
		content.insert_before(br, exit);
	}
}

//helpers
inline void setElementValue(const string &param, ParameterTable &paramsTable,
		P3GUIElementDocumentPtr document)
{
	P3GUIElementFormControlInputPtr inputElem = P3GUIElementFormControlInputPtr(
			document.get_element_by_id(param));
	inputElem.set_value((*paramsTable.find(param)).second);
}

inline void setElementChecked(const string &param, const string &checked,
		const string &unchecked, const string &defaultValue,
		ParameterTable &paramsTable, P3GUIElementDocumentPtr document)
{
	P3GUIElementFormControlInputPtr inputElem = P3GUIElementFormControlInputPtr(
			document.get_element_by_id(param));
	string actualChecked = (*paramsTable.find(param)).second;
	if (actualChecked == checked)
	{
		inputElem.set_attribute_string("checked", "true");
	}
	else if (actualChecked == unchecked)
	{
		inputElem.remove_attribute("checked");
	}
	else
	{
		defaultValue == checked ?
				inputElem.set_attribute_string("checked", "true") :
				inputElem.remove_attribute("checked");
	}
}

inline void setOptionValue(P3GUIEventPtr event, const string &param,
		ParameterTable &paramsTable)
{
	(*paramsTable.find(param)).second = event.get_parameter_string(param,
			"0.0");
}

inline void setOptionChecked(P3GUIEventPtr event, const string &param,
		const string &checked, const string &unchecked,
		ParameterTable &paramsTable)
{
	string paramValue = event.get_parameter_string(param, "");
	paramValue == "true" ?
			(*paramsTable.find(param)).second = checked :
			(*paramsTable.find(param)).second = unchecked;
}

//event handler added to the main one
void rocketEventHandler(P3GUIEventPtr event, const string &value)
{
	if (value == "camera::options")
	{
		//hide main menu
		GameGUIManager::get_global_ptr()->get_main_menu().hide();
		// Load and show the camera options document.
		P3GUIElementDocumentPtr cameraOptionsMenu =
				GameGUIManager::get_global_ptr()->get_rml_context().load_document(
						rmlUiBaseDir + "/ely-camera-options.rml");
		if (!cameraOptionsMenu.is_empty())
		{
			cameraOptionsMenu.get_element_by_id("title").set_inner_rml(
					cameraOptionsMenu.get_title());
			///update radio buttons
			//camera type
			switch (cameraType)
			{
			case free_view_camera:
				cameraOptionsMenu.get_element_by_id("free_view_camera").set_attribute_bool(
						"checked", true);
				break;
			case chaser_camera:
				cameraOptionsMenu.get_element_by_id("chaser_camera").set_attribute_bool(
						"checked", true);
				break;
			case object_picker:
				cameraOptionsMenu.get_element_by_id("object_picker").set_attribute_bool(
						"checked", true);
				break;
			case none:
				cameraOptionsMenu.get_element_by_id("none").set_attribute_bool(
						"checked", true);
				break;
			default:
				break;
			}
			//
			cameraOptionsMenu.show();
		}
	}
	else if (value == "camera::body::load_logo")
	{
		PRINT_DEBUG("camera::body::load_logo");
	}
	else if (value == "camera::free_view_camera::options")
	{
		// This event is sent from the "onchange" of the "free_view_camera"
		//radio button. It shows or hides the related options.
		P3GUIElementDocumentPtr cameraOptionsMenu =
				event.get_target_element().get_owner_document();
		if (cameraOptionsMenu.is_empty())
			return;

		P3GUIElementPtr free_view_camera_options =
				cameraOptionsMenu.get_element_by_id("free_view_camera_options");
		if (!free_view_camera_options.is_empty())
		{
			//The "value" parameter of an "onchange" event is set to
			//the value the control would send if it was submitted;
			//so, the empty string if it is clear or to the "value"
			//attribute of the control if it is set.
			if (event.get_parameter_string("value", "").empty())
			{
				free_view_camera_options.set_property("display", "none");
			}
			else
			{
				free_view_camera_options.set_property("display", "block");
				//set elements' values from options' values
				//pitch limit and pitch limit value: enabled@limit
				pvector<string> paramValuesStr = parseCompoundString(
						(*cameraDriverParams.find("pitch_limit")).second, '@');
				P3GUIElementFormControlInputPtr inputValueElem, inputPitchElem;
				//pitch limit value
				inputValueElem = P3GUIElementFormControlInputPtr(
						cameraOptionsMenu.get_element_by_id(
								"pitch_limit_value"));
				inputValueElem.set_value(paramValuesStr[1]);
				//pitch limit
				inputPitchElem = P3GUIElementFormControlInputPtr(
						cameraOptionsMenu.get_element_by_id("pitch_limit"));
				//checks
				if (inputPitchElem.get_attribute("checked").is_empty())
				{
					if (paramValuesStr[0] == "true")
					{
						//enable value input element
						inputValueElem.set_disabled(false);
					}
					else
					{
						//checked -> unchecked: change event thrown
						inputPitchElem.remove_attribute("checked");
					}
				}
				else
				{
					if (paramValuesStr[0] != "true")
					{
						//disable value input element
						inputValueElem.set_disabled(true);
					}
					else
					{
						//unchecked -> checked: change event thrown
						inputPitchElem.set_attribute_string("checked", "true");
					}
				}
				//max linear speed
				setElementValue("max_linear_speed", cameraDriverParams,
						cameraOptionsMenu);
				//max angular speed
				setElementValue("max_angular_speed", cameraDriverParams,
						cameraOptionsMenu);
				//linear accel
				setElementValue("linear_accel", cameraDriverParams,
						cameraOptionsMenu);
				//angular accel
				setElementValue("angular_accel", cameraDriverParams,
						cameraOptionsMenu);
				//linear friction
				setElementValue("linear_friction", cameraDriverParams,
						cameraOptionsMenu);
				//angular friction
				setElementValue("angular_friction", cameraDriverParams,
						cameraOptionsMenu);
				//fast factor
				setElementValue("fast_factor", cameraDriverParams,
						cameraOptionsMenu);
				//sens_x
				setElementValue("sens_x", cameraDriverParams,
						cameraOptionsMenu);
				//sens_y
				setElementValue("sens_y", cameraDriverParams,
						cameraOptionsMenu);
			}
		}
	}
	else if (value == "camera::chaser_camera::options")
	{
		// This event is sent from the "onchange" of the "chaser_camera"
		//radio button. It shows or hides the related options.
		P3GUIElementDocumentPtr cameraOptionsMenu =
				event.get_target_element().get_owner_document();
		if (cameraOptionsMenu.is_empty())
			return;

		P3GUIElementPtr chaser_camera_options =
				cameraOptionsMenu.get_element_by_id("chaser_camera_options");
		if (!chaser_camera_options.is_empty())
		{
			//The "value" parameter of an "onchange" event is set to
			//the value the control would send if it was submitted;
			//so, the empty string if it is clear or to the "value"
			//attribute of the control if it is set.
			if (event.get_parameter_string("value", "").empty())
			{
				chaser_camera_options.set_property("display", "none");
			}
			else
			{
				chaser_camera_options.set_property("display", "block");
				//set elements' values from options' values
				//chased object
				P3GUIElementFormControlSelectPtr objectsSelect =
						P3GUIElementFormControlSelectPtr(
								cameraOptionsMenu.get_element_by_id(
										"chased_object"));
				if (!objectsSelect.is_empty())
				{
					//remove all options
					objectsSelect.remove_all();
					//set object list
					list<string> createdObjects = { "object1", "object2",
							"object3", "object4" };
					int selectedIdx = objectsSelect.add("", "");
					for (auto object : createdObjects)
					{
						//add options
						string objectId = object;
						int i = objectsSelect.add(objectId, objectId);
						if (objectId == chasedObject)
						{
							selectedIdx = i;
						}
					}
					//set first option as selected
					objectsSelect.set_selection(selectedIdx);
				}
				//fixed relative position
				setElementChecked("fixed_relative_position", "true", "false",
						"true", cameraChaserParams, cameraOptionsMenu);
				//backward
				setElementChecked("backward", "true", "false", "true",
						cameraChaserParams, cameraOptionsMenu);
				//abs max distance
				setElementValue("abs_max_distance", cameraChaserParams,
						cameraOptionsMenu);
				//abs min distance
				setElementValue("abs_min_distance", cameraChaserParams,
						cameraOptionsMenu);
				//abs max height
				setElementValue("abs_max_height", cameraChaserParams,
						cameraOptionsMenu);
				//abs min height
				setElementValue("abs_min_height", cameraChaserParams,
						cameraOptionsMenu);
				//abs lookat distance
				setElementValue("abs_lookat_distance", cameraChaserParams,
						cameraOptionsMenu);
				//abs lookat height
				setElementValue("abs_lookat_height", cameraChaserParams,
						cameraOptionsMenu);
				//friction
				setElementValue("friction", cameraChaserParams,
						cameraOptionsMenu);
			}
		}
	}
	else if (value == "camera::object_picker::options")
	{
		// This event is sent from the "onchange" of the "object_picker"
		//radio button. It shows or hides the related options.
		P3GUIElementDocumentPtr cameraOptionsMenu =
				event.get_target_element().get_owner_document();
		if (cameraOptionsMenu.is_empty())
			return;

		P3GUIElementPtr object_picker_options =
				cameraOptionsMenu.get_element_by_id("object_picker_options");
		if (!object_picker_options.is_empty())
		{
			//The "value" parameter of an "onchange" event is set to
			//the value the control would send if it was submitted;
			//so, the empty string if it is clear or to the "value"
			//attribute of the control if it is set.
			if (event.get_parameter_string("value", "").empty())
			{
				object_picker_options.set_property("display", "none");
			}
			else
			{
				object_picker_options.set_property("display", "block");
				//set elements' values from options' values
				//constraint type
				pickerCsIspherical ?
						cameraOptionsMenu.get_element_by_id(
								"spherical_constraint").set_attribute_bool(
								"checked", true) :
						cameraOptionsMenu.get_element_by_id(
								"generic_constraint").set_attribute_bool(
								"checked", true);
			}
		}
	}
	else if (value == "pitch_limit::change")
	{
		P3GUIElementDocumentPtr cameraOptionsMenu =
				event.get_target_element().get_owner_document();
		//check
		if (!P3GUIElementFormControlInputPtr(
				cameraOptionsMenu.get_element_by_id("pitch_limit")).get_attribute(
				"checked").is_empty())
		{
			P3GUIElementFormControlInputPtr(
					cameraOptionsMenu.get_element_by_id("pitch_limit_value")).set_disabled(
					false);
		}
		else
		{
			P3GUIElementFormControlInputPtr(
					cameraOptionsMenu.get_element_by_id("pitch_limit_value")).set_disabled(
					true);
		}
	}
	///Submit
	else if (value == "camera::form::submit_options")
	{
		string paramValue;
		//check if ok or cancel
		paramValue = event.get_parameter_string("submit", "cancel");
		if (paramValue == "ok")
		{
			//set new camera type
			paramValue = event.get_parameter_string("camera", "none");
			if (paramValue == "free_view_camera")
			{
				cameraType = free_view_camera;
				//set options' values from elements' values
				//pitch limit and value: enabled@limit
				string enabled = (
						event.get_parameter_string("pitch_limit", "")
								== string("true") ? "true" : "false");
				string limit;
				if (enabled == "true")
				{
					limit = event.get_parameter_string("pitch_limit_value",
							"0.0");
				}
				else
				{
					limit = parseCompoundString(
							(*cameraDriverParams.find("pitch_limit")).second,
							'@')[1];
				}
				(*cameraDriverParams.find("pitch_limit")).second = enabled
						+ string("@") + limit;
				//max linear speed
				setOptionValue(event, "max_linear_speed", cameraDriverParams);
				//max angular speed
				setOptionValue(event, "max_angular_speed", cameraDriverParams);
				//linear accel
				setOptionValue(event, "linear_accel", cameraDriverParams);
				//angular accel
				setOptionValue(event, "angular_accel", cameraDriverParams);
				//linear friction
				setOptionValue(event, "linear_friction", cameraDriverParams);
				//angular friction
				setOptionValue(event, "angular_friction", cameraDriverParams);
				//fast factor
				setOptionValue(event, "fast_factor", cameraDriverParams);
				//sens_x
				setOptionValue(event, "sens_x", cameraDriverParams);
				//sens_y
				setOptionValue(event, "sens_y", cameraDriverParams);
			}
			else if (paramValue == "chaser_camera")
			{
				cameraType = chaser_camera;
				//set options' values from elements' values
				//chased object
				chasedObject = event.get_parameter_string("chased_object", "");
				if (not chasedObject.empty())
				{
					(*cameraChaserParams.find("chased_object")).second =
							chasedObject;
				}
				//fixed relative position
				setOptionChecked(event, "fixed_relative_position", "true",
						"false", cameraChaserParams);
				//backward
				setOptionChecked(event, "backward", "true", "false",
						cameraChaserParams);
				//abs max distance
				setOptionValue(event, "abs_max_distance", cameraChaserParams);
				//abs min distance
				setOptionValue(event, "abs_min_distance", cameraChaserParams);
				//abs max height
				setOptionValue(event, "abs_max_height", cameraChaserParams);
				//abs min height
				setOptionValue(event, "abs_min_height", cameraChaserParams);
				//abs lookat distance
				setOptionValue(event, "abs_lookat_distance",
						cameraChaserParams);
				//abs lookat height
				setOptionValue(event, "abs_lookat_height", cameraChaserParams);
				//friction
				setOptionValue(event, "friction", cameraChaserParams);
			}
			else if (paramValue == "object_picker")
			{
				cameraType = object_picker;
				//set options' values from elements' values
				//constraint type
				paramValue = event.get_parameter_string("constraint_type", "");
				if (paramValue == "spherical")
				{
					pickerCsIspherical = true;
				}
				else if (paramValue == "generic")
				{
					pickerCsIspherical = false;
				}
			}
			else
			{
				//default
				cameraType = none;
			}
		}
		//close (i.e. unload) the camera options menu and set as closed..
		P3GUIElementDocumentPtr cameraOptionsMenu =
				event.get_target_element().get_owner_document();
		cameraOptionsMenu.close();
		//return to main menu.
		GameGUIManager::get_global_ptr()->get_main_menu().show();
	}
}

//helper
inline void setCameraType(const CameraType &newType,
		const CameraType &actualType)
{

	//return if no camera type change is needed
	RETURN_ON_COND(newType == actualType,)

	//unset actual camera type
	if (actualType == free_view_camera)
	{
		if (cameraEnabled)
		{
			//enabled: then disable it
			//disable
			cameraEnabled = false;
			//remove text
			textNode.remove_node();
		}
	}
	else if (actualType == chaser_camera)
	{
		if (cameraEnabled)
		{
			//enabled: then disable it
			//disable
			cameraEnabled = false;
			//remove text
			textNode.remove_node();
		}
	}
	else if (actualType == object_picker)
	{
		if (pickerOn)
		{
			if (cameraEnabled)
			{
				//enabled: then disable it
				//disable
				cameraEnabled = false;
			}

			//picker on: remove
			pickerOn = false;
			//remove text
			textNode.remove_node();
		}
	}
	//set new type
	if (newType == free_view_camera)
	{
		//add a new Driver component to camera and ...
		//... enable it
		cameraEnabled = true;
		//write text
		writeText(textNode, "Free View Camera", 0.05,
				LVecBase4(1.0, 1.0, 0.0, 1.0), LVector3f(-1.0, 0, -0.9),
				&framework);
	}
	else if (newType == chaser_camera)
	{
		//add a new Chaser component to camera and ...
		//... enable it
		cameraEnabled = true;
		//write text
		writeText(textNode, "Camera Chasing '" + chasedObject + "'", 0.05,
				LVecBase4(1.0, 1.0, 0.0, 1.0), LVector3f(-1.0, 0, -0.9),
				&framework);

	}
	else if (newType == object_picker)
	{
		if (not pickerOn)
		{
			//... enable it
			cameraEnabled = true;
			//picker off: add
			pickerOn = true;
			//write text
			writeText(textNode, "Object Picker Active", 0.05,
					LVecBase4(1.0, 1.0, 0.0, 1.0), LVector3f(-1.0, 0, -0.9),
					&framework);
		}
	}

}

//preset function called from main menu
void rocketPreset()
{
	setCameraType(none, cameraType);
}

//commit function called main menu
void rocketCommit()
{
	setCameraType(cameraType, none);
}

} // namespace

void camera_initialization()
{
	//register the add element function to (RmlUi) main menu
	GameGUIManager::get_global_ptr()->register_gui_add_elements_function(
			&rocketAddElements);
	//register the event handler to main menu for each event value
	GameGUIManager::get_global_ptr()->register_gui_event_handler(
			"camera::options", &rocketEventHandler);
	GameGUIManager::get_global_ptr()->register_gui_event_handler(
			"camera::body::load_logo", &rocketEventHandler);
	GameGUIManager::get_global_ptr()->register_gui_event_handler(
			"camera::form::submit_options", &rocketEventHandler);
	GameGUIManager::get_global_ptr()->register_gui_event_handler(
			"camera::free_view_camera::options", &rocketEventHandler);
	GameGUIManager::get_global_ptr()->register_gui_event_handler(
			"camera::chaser_camera::options", &rocketEventHandler);
	GameGUIManager::get_global_ptr()->register_gui_event_handler(
			"camera::object_picker::options", &rocketEventHandler);
	GameGUIManager::get_global_ptr()->register_gui_event_handler(
			"pitch_limit::change", &rocketEventHandler);
	//register the preset function to main menu
	GameGUIManager::get_global_ptr()->register_gui_preset_function(
			&rocketPreset);
	//register the commit function to main menu
	GameGUIManager::get_global_ptr()->register_gui_commit_function(
			&rocketCommit);
}
