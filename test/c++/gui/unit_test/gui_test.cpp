/**
 * \file audio.cpp
 *
 * \date 2018-06-28
 * \author consultit
 */

#include "suite.h"

struct GUITestCaseFixture
{
	GUITestCaseFixture()
	{
		BOOST_TEST_MESSAGE("ctor GUITestCaseFixture");
	}
	~GUITestCaseFixture()
	{
		BOOST_TEST_MESSAGE("dtor GUITestCaseFixture");
	}
};

/// GUI suite
BOOST_FIXTURE_TEST_SUITE(GUI, GUISuiteFixture);

/// Test cases
BOOST_AUTO_TEST_CASE(GUITest)
{
	BOOST_TEST_MESSAGE("GUITest");
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END(); // GUISuiteFixture
