/**
 * \file suite.h
 *
 * \date 2018-06-28
 * \author consultit
 */
#ifndef AUDIO_SUITE_H_
#define AUDIO_SUITE_H_

#include <boost/test/unit_test.hpp>

struct GUISuiteFixture
{
	GUISuiteFixture()
	{
		BOOST_TEST_MESSAGE("ctor GUISuiteFixture");
	}
	~GUISuiteFixture()
	{
		BOOST_TEST_MESSAGE("dtor GUISuiteFixture");
	}
};

#endif /* AUDIO_SUITE_H_ */
