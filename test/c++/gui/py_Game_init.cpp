/**
 * \file Game_init.cpp
 *
 * \date 2019-10-04
 * \author consultit
 */

#include "py_Game_init.h"
#include "gameGUIManager.h"

//globals
string rmlUiBaseDir;
//common text writing
void writeText(NodePath &textNode, const std::string &text, float scale,
		const LVecBase4 &color, const LVector3f &location,
		PandaFramework *framework)
{
	textNode = NodePath(new TextNode("CommonTextNode"));
	textNode.reparent_to(framework->get_window(0)->get_render_2d());
	textNode.set_bin("fixed", 50);
	textNode.set_depth_write(false);
	textNode.set_depth_test(false);
	textNode.set_billboard_point_eye();
	DCAST(TextNode, textNode.node())->set_text(text);
	textNode.set_scale(scale);
	textNode.set_color(color);
	textNode.set_pos(location);
}

void guiSystemInitialization()
{
	//gui main menu
	string mGuiMainMenuParam{ rmlUiBaseDir + "/ely-main-menu.rml" };
	//gui exit menu
	string mGuiExitMenuParam{ rmlUiBaseDir + "/ely-exit-menu.rml" };
	//gui font paths
	list<string> fontPaths
	{
		rmlUiBaseDir + "/Delicious-Roman.otf",
		rmlUiBaseDir + "/Delicious-Italic.otf",
		rmlUiBaseDir + "/Delicious-Bold.otf",
		rmlUiBaseDir + "/Delicious-BoldItalic.otf"
	};
	//
	//gui main menu if any
	if (not mGuiMainMenuParam.empty())
	{
		GameGUIManager::get_global_ptr()->set_gui_main_menu_path(mGuiMainMenuParam);
	}
	//gui exit menu if any
	if (not mGuiExitMenuParam.empty())
	{
		GameGUIManager::get_global_ptr()->set_gui_exit_menu_path(mGuiExitMenuParam);
	}
	ValueList_string pathList;
	for (auto path : fontPaths)
	{
		//an empty font_path is ignored
		if (not path.empty())
		{
			pathList.add_value(path);
		}
	}
	GameGUIManager::get_global_ptr()->set_gui_font_paths(pathList);
}

//locals
namespace
{
const int CALLBACKSNUM = 5;

void showMainMenu(const Event *e)
{
	GameGUIManager::get_global_ptr()->show_main_menu();
}

void showExitMenu(const Event *e)
{
	GameGUIManager::get_global_ptr()->show_exit_menu();
}

//event handler added to the main one
void rocketEventHandler(P3GUIEventPtr event, const string &value)
{
	if (value == "main::body::load_logo")
	{
		PRINT_DEBUG("main::body::load_logo");
	}
}

}  // namespace

void on_exit_function()
{
	//on exit function
	GameGUIManager::get_global_ptr()->gui_cleanup();
	framework.set_exit_flag();
}

void elyPreObjects_initialization()
{
	//register the add element function to gui (Rocket) main menu
	//register the event handler to gui main menu for each event value
	GameGUIManager::get_global_ptr()->register_gui_event_handler("main::body::load_logo",
			&rocketEventHandler);
	//register the preset function to gui main menu
	//register the commit function to gui main menu
	//set the on exit function
	GameGUIManager::get_global_ptr()->set_gui_on_exit_function(on_exit_function);
}

void elyPostObjects_initialization(PandaFramework *framework)
{
	///Gui (libRocket)
	//add show main menu event handler
	EventHandler::get_global_event_handler()->add_hook("m", &showMainMenu);
	//handle "close request" and "esc" events
	framework->get_window(0)->get_graphics_window()->set_close_request_event(
			"close_request_event");
	EventHandler::get_global_event_handler()->add_hook("close_request_event",
			&showExitMenu);
	EventHandler::get_global_event_handler()->add_hook("escape", &showExitMenu);
}
