/**
 * \file audio.cpp
 *
 * \date 2016-09-20
 * \author consultit
 */

#include <string>
#include <pandaFramework.h>
#include <load_prc_file.h>
#include <default_font.h>

#include <p3guiInputHandler.h>
#include <p3guiRegion.h>
#include <p3guiFileInterface.h>
#include <p3guiSystemInterface.h>

#include "gameGUIManager.h"
#include "py_Game_init.h"

using namespace std;

/// global data declaration
extern string dataDir;
PandaFramework framework;
WindowFramework *window = nullptr;

/// specific data/functions declarations/definitions
// libp3gui typed objects init
void libp3guiTypedObjectInit();
// libp3tools typed objects init
void libp3toolsTypedObjectInit();
// start base framework
string startFramework(int argc, char *argv[], const string &msg);
// do exit
void doExit(const Event*, void *data);

int main(int argc, char *argv[])
{
	string msg("'P3GUI tests'");
	string bamFile = startFramework(argc, argv, msg);

	/// here is room for your own code
	// print some help to screen
	PT(TextNode) text;
	text = new TextNode("Help");
	text->set_text(msg + "\n\n"
			"- press \"up\"/\"left\"/\"down\"/\"right\" arrows TODO\n");
	NodePath textNodePath = window->get_aspect_2d().attach_new_node(text);
	textNodePath.set_pos(-1.25, 0.0, 0.8);
	textNodePath.set_scale(0.035);

	/// create a gui manager
	GameGUIManager *guiMgr = new GameGUIManager(window->get_graphics_window(),
			window->get_mouse());
	/// gui mandatory configuration
	guiSystemInitialization();
	/// gui system setup
	guiMgr->gui_setup();
	/// gui additional configurations
	elyPreObjects_initialization();
	elyPostObjects_initialization(&framework);
	camera_initialization();

	// place camera trackball (local coordinate)
	PT(Trackball) trackball = DCAST(Trackball,
			window->get_mouse().find("**/+Trackball").node());
	trackball->set_pos(0.0, 120.0, 5.0);
	trackball->set_hpr(0.0, 10.0, 0.0);

	// do the main loop, equals to call app.run() in python
	framework.main_loop();

	return (0);
}

///functions' definitions
// libp3tools typed objects init
void libp3guiTypedObjectInit()
{
	P3GUIInputHandler::init_type();
	P3GUIRegion::init_type();
	GameGUIManager::init_type();

	if (p3gui_cat->is_debug())
	{
		p3gui_cat->debug() << "Initializing RmlUi library.\n";
	}
}
// libp3tools typed objects init
void libp3toolsTypedObjectInit()
{
	Pair_bool_float::init_type();
	Pair_LPoint3f_int::init_type();
	Pair_LVector3f_ValueList_float::init_type();
	ValueList_string::init_type();
	ValueList_NodePath::init_type();
	ValueList_int::init_type();
	ValueList_float::init_type();
	ValueList_LVecBase3f::init_type();
	ValueList_LVector3f::init_type();
	ValueList_LPoint3f::init_type();
	ValueList_Pair_LPoint3f_int::init_type();
}

// start base framework
string startFramework(int argc, char *argv[], const string &msg)
{
	// parse arguments
	vector<string> dirs;
	string bamFile;
	{
		// parse arguments
		int c;
		while ((c = getopt(argc, argv, "d:b:")) != -1)
		{
			switch (c)
			{
			case 'd':
			{
				dirs.emplace_back(optarg);
			}
				break;
			case 'b':
			{
				rmlUiBaseDir = optarg;
			}
				break;
			case '?':
				if ((optopt == 'd') || (optopt == 'b'))
					fprintf(stderr, "Option -%c requires an argument.\n",
							optopt);
				else if (isprint(optopt))
					fprintf(stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf(stderr, "Unknown option character `\\x%x'.\n",
							optopt);
				break;
			default:
				abort();
			}
		}
		for (c = optind; c < argc; c++)
		{
			// bamfile is the first non-option argument
			bamFile = string(argv[c]);
			break;
		}
	}

	// Load your application's configuration
	load_prc_file_data("", "model-path " + dataDir);
	for (auto dir : dirs)
	{
		load_prc_file_data("", "model-path " + dir);
	}
	load_prc_file_data("", "win-size 1024 768");
	load_prc_file_data("", "show-frame-rate-meter #t");
	load_prc_file_data("", "sync-video #t");
	// Setup your application
	framework.open_framework(argc, argv);
	framework.set_window_title("p3audio: " + msg);
	window = framework.open_window();
	if (window != (WindowFramework*) nullptr)
	{
		cout << "Opened the window successfully!\n";
		window->enable_keyboard();
		window->setup_trackball();
	}

	/// typed object init; not needed if you build inside panda source tree
	libp3toolsTypedObjectInit();
	libp3guiTypedObjectInit();
	///

	//common callbacks
	return bamFile;
}

// do exit
void doExit(const Event*, void *data)
{
	// close the window framework
	framework.close_framework();
	//
	exit(0);
}
