/**
 * \file GameGUIManager.h
 *
 * \date 2019-10-04
 * \author consultit
 */

#ifndef GAMEGUIMANAGER_H_
#define GAMEGUIMANAGER_H_

#include "p3gui_includes.h"
#include "RmlUi/Include/RmlUi/Core.h"
#include "pandaFramework.h"

/**
 * \brief Abstract class interface for GUI (RmlUi) event listeners.
 *
 * A concrete derived class must implement:
 * \code
 * virtual void ProcessEvent(Rml::Event& event);
 * \endcode
 */
class EventListener: public Rml::EventListener
{
public:
	EventListener(const string &value) :
			mValue(value)
	{
		PRINT_DEBUG(
				"Creating EventListener: " << mValue << " " << this);
	}
	virtual ~EventListener()
	{
		PRINT_DEBUG(
				"Destroying EventListener: " << mValue << " " << this);
	}

	virtual void OnAttach(Rml::Element*RMLUI_UNUSED(element))
	{
		PRINT_DEBUG(
				"Attaching EventListener: " << mValue << " " << this);
	}

	virtual void OnDetach(Rml::Element*RMLUI_UNUSED(element))
	{
		PRINT_DEBUG(
				"Detaching EventListener: " << mValue << " " << this);
	}

protected:
	virtual void OnReferenceDeactivate()
	{
		delete this;
	}

	string mValue;
};

/**
 * \brief Abstract class interface for RmlUi event listener instancers.
 *
 * A concrete derived class must implement:
 * \code
 * virtual Rml::EventListener* InstanceEventListener(
 const string& value, Rml::Element* element);
 * \endcode
 * This interface is designed so that method would create a ely::EventListener
 * derived class.\n
 */
class EventListenerInstancer: public Rml::EventListenerInstancer
{
public:
	EventListenerInstancer(PandaFramework *framework)
	{
		PRINT_DEBUG("Creating EventListenerInstancer " << " " << this);
	}
	virtual ~EventListenerInstancer()
	{
		PRINT_DEBUG("Destroying EventListenerInstancer " << " " << this);
	}

	virtual void Release()
	{
		delete this;
	}

};

/**
 * \brief Gui (RmlUi) event framework class.
 */
class MainEventListener: public EventListener
{
public:
	MainEventListener(const string &value);

	virtual ~MainEventListener();

	virtual void ProcessEvent(Rml::Event &event);

};

class MainEventListenerInstancer: public EventListenerInstancer
{
public:
	MainEventListenerInstancer(PandaFramework *framework) :
			EventListenerInstancer(framework)
	{
	}
	virtual ~MainEventListenerInstancer()
	{
	}
	virtual Rml::EventListener* InstanceEventListener(
			const string &value, Rml::Element *element)
	{
		return new MainEventListener(value);
	}
};

/**
 * \brief GameGUIManager.
 *
 * To use this RmlUi based framework, these documents must be
 * supplied:
 * - "main_menu" that responds, at least, to "MAIN::ENTER_GAME",
 * "MAIN::EXIT_GAME" events
 * - "exit_menu" that responds, at least,  to "EXIT::SUBMIT_EXIT" events
 *
 * Currently these parameters (along with font paths) are passed through a
 * GameConfig component.
 *
 */
class GameGUIManager: public Singleton<GameGUIManager>
{
public:
	/**
	 * \brief Constructor. Initializes the GUI (RmlUi) environment.
	 */
	GameGUIManager(PandaFramework *framework);

	/**
	 * \brief Destructor
	 */
	virtual ~GameGUIManager();

	/**
	 * \brief Put here game setup code.
	 */
	virtual void guiSetup();

	/**
	 * \brief GUI (RmlUi) global related variables and functions.
	 */
	///@{
	Rml::Context *gGuiRmlUiContext;
	Rml::ElementDocument *gGuiMainMenu;
	Rml::ElementDocument *gGuiExitMenu;
	std::string gGuiMainMenuPath;
	std::string gGuiExitMenuPath;
	//registered by subsystems to add their element (tags) to main menu
	std::vector<void (*)(Rml::ElementDocument*)> gGuiAddElementsFunctions;
	//registered by subsystems to handle their events
	std::map<string,
			void (*)(const string&, Rml::Event&)> gGuiEventHandlers;
	//registered by some subsystems that need to preset themselves before main/exit menus are shown
	std::vector<void (*)()> gGuiPresetFunctions;
	//registered by some subsystems that need to commit their changes after main/exit menus are closed
	std::vector<void (*)()> gGuiCommitFunctions;
	std::vector<std::string> gGuiFontPaths;
	bool gGuiMainPresetsCommits;
	//functions
	void showMainMenu();
	void showExitMenu();
	///@}

	/**
	 * \name SINGLETON
	 */
	///@{
	INLINE static GameGUIManager* get_global_ptr();
	///@}

	inline PandaFramework* get_framework() const;

protected:

	PandaFramework *mFramework;

	/**
	 * \brief Gui (RmlUi) related variables and functions.
	 */
	///@{
	MainEventListenerInstancer *mEventListenerInstancer;
	///@}

//Hack
public:
	static void init_type()
	{
	}
};

///inline definitions
inline PandaFramework* GameGUIManager::get_framework() const
{
	return mFramework;
}
/**
 * Returns the singleton pointer.
 */
INLINE GameGUIManager* GameGUIManager::get_global_ptr()
{
	return Singleton<GameGUIManager>::GetSingletonPtr();
}

#endif /* GAMEGUIMANAGER_H_ */
