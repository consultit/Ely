/**
 * \file Camera_init.cpp
 *
 * \date 2019-10-04
 * \author consultit
 */

#include <RmlUi/Core.h>
//#include <RmlUi/Controls.h> xxx
#include "Game_init.h"
#include "GameGUIManager.h"

//locals
namespace
{
//common text writing
NodePath textNode;
enum CameraType
{
	free_view_camera, chaser_camera, object_picker, none
} cameraType = none;
//PT(Object)camera;
bool cameraEnabled = false;
ParameterTable cameraDriverParams{
	{"enabled", "false"},
	{"max_linear_speed", "40.0"},
	{"max_angular_speed", "40.0"},
	{"linear_accel", "30.0"},
	{"angular_accel", "30.0"},
	{"linear_friction", "5.0"},
	{"angular_friction", "5.0"},
	{"stop_threshold", "0.0"},
	{"fast_factor", "5.0"},
	{"sens_x", "0.05"},
	{"sens_y", "0.05"},
	{"pitch_limit", "true@60.0"},
	{"head_left", "enabled"},
	{"head_right", "enabled"},
	{"inverted_translation", "true"},
	{"inverted_rotation", "false"},
	{"mouse_enabled_h", "true"},
	{"mouse_enabled_p", "true"}
};
ParameterTable cameraChaserParams{
	{"enabled", "false"},
	{"backward", "false"},
	{"chased_object", "NONE"},
	{"fixed_relative_position", "false"},
	{"reference_object", "render"},
	{"abs_max_distance", "25.0"},
	{"abs_min_distance", "18.0"},
	{"abs_max_height", "8.0"},
	{"abs_min_height", "5.0"},
	{"friction", "5.0"},
	{"fixed_lookat", "false"},
	{"mouse_enabled_h", "true"},
	{"mouse_enabled_p", "true"},
	{"abs_lookat_distance", "5.0"},
	{"abs_lookat_height", "1.5"},
};
string chasedObject
{ "chasedObject" };
bool pickerOn = false;
bool pickerCsIspherical = true;

//add elements (tags) function for main menu
void rocketAddElements(Rml::ElementDocument *mainMenu)
{
	//<button onclick="camera::options">Camera options</button><br/>
	Rml::Element *content = mainMenu->GetElementById("content");
	Rml::Element *exit = mainMenu->GetElementById("main::button::exit");
	if (content)
	{
		//create input element
		Rml::Dictionary params;
		params["onclick"] = "camera::options";
		Rml::ElementPtr input = Rml::Factory::InstanceElement(nullptr,
				"button", "button", params);
		Rml::Factory::InstanceElementText(input.get(), "Camera options");
		//create br element
		params.clear();
		params["id"] = "br";
		Rml::ElementPtr br = Rml::Factory::InstanceElement(nullptr,
				"br", "br", params);
		//inster elements
		content->InsertBefore(move(input), exit);
		content->InsertBefore(move(br), exit);
	}
}

//helpers
inline void setElementValue(const string &param, ParameterTable &paramsTable,
		Rml::ElementDocument *document)
{
	Rml::ElementFormControlInput *inputElem =
			static_cast<Rml::ElementFormControlInput*>(document->GetElementById(
					param));
	inputElem->SetValue((*paramsTable.find(param)).second);
}

inline void setElementChecked(const string &param, const string &checked,
		const string &unchecked, const string &defaultValue,
		ParameterTable &paramsTable, Rml::ElementDocument *document)
{
	Rml::ElementFormControlInput *inputElem =
			static_cast<Rml::ElementFormControlInput*>(document->GetElementById(
					param));
	string actualChecked = (*paramsTable.find(param)).second;
	if (actualChecked == checked)
	{
		inputElem->SetAttribute<string>("checked", "true");
	}
	else if (actualChecked == unchecked)
	{
		inputElem->RemoveAttribute("checked");
	}
	else
	{
		defaultValue == checked ?
				inputElem->SetAttribute<string>("checked", "true") :
				inputElem->RemoveAttribute("checked");
	}
}

inline void setOptionValue(Rml::Event &event, const string &param,
		ParameterTable &paramsTable)
{
	(*paramsTable.find(param)).second = event.GetParameter<string>(
			param, "0.0");
}

inline void setOptionChecked(Rml::Event &event, const string &param,
		const string &checked, const string &unchecked,
		ParameterTable &paramsTable)
{
	string paramValue =
			event.GetParameter<string>(param, "");
	paramValue == "true" ?
			(*paramsTable.find(param)).second = checked :
			(*paramsTable.find(param)).second = unchecked;
}

//event handler added to the main one
void rocketEventHandler(const string &value, Rml::Event &event)
{
	if (value == "camera::options")
	{
		//hide main menu
		GameGUIManager::get_global_ptr()->gGuiMainMenu->Hide();
		// Load and show the camera options document.
		Rml::ElementDocument *cameraOptionsMenu =
				GameGUIManager::get_global_ptr()->gGuiRmlUiContext->LoadDocument(
						(rmlUiBaseDir + "/ely-camera-options.rml"));
		if (cameraOptionsMenu != nullptr)
		{
			cameraOptionsMenu->GetElementById("title")->SetInnerRML(
					cameraOptionsMenu->GetTitle());
			///update radio buttons
			//camera type
			switch (cameraType)
			{
			case free_view_camera:
				cameraOptionsMenu->GetElementById("free_view_camera")->SetAttribute(
						"checked", true);
				break;
			case chaser_camera:
				cameraOptionsMenu->GetElementById("chaser_camera")->SetAttribute(
						"checked", true);
				break;
			case object_picker:
				cameraOptionsMenu->GetElementById("object_picker")->SetAttribute(
						"checked", true);
				break;
			case none:
				cameraOptionsMenu->GetElementById("none")->SetAttribute(
						"checked", true);
				break;
			default:
				break;
			}
			//
			cameraOptionsMenu->Show();
		}
	}
	else if (value == "camera::body::load_logo")
	{
		PRINT_DEBUG("camera::body::load_logo");
	}
	else if (value == "camera::free_view_camera::options")
	{
		// This event is sent from the "onchange" of the "free_view_camera"
		//radio button. It shows or hides the related options.
		Rml::ElementDocument *cameraOptionsMenu =
				event.GetTargetElement()->GetOwnerDocument();
		if (cameraOptionsMenu == nullptr)
			return;

		Rml::Element *free_view_camera_options =
				cameraOptionsMenu->GetElementById("free_view_camera_options");
		if (free_view_camera_options)
		{
			//The "value" parameter of an "onchange" event is set to
			//the value the control would send if it was submitted;
			//so, the empty string if it is clear or to the "value"
			//attribute of the control if it is set.
			if (event.GetParameter<string>("value", "").empty())
			{
				free_view_camera_options->SetProperty("display", "none");
			}
			else
			{
				free_view_camera_options->SetProperty("display", "block");
				//set elements' values from options' values
				//pitch limit and pitch limit value: enabled@limit
				pvector<string> paramValuesStr = parseCompoundString(
						(*cameraDriverParams.find("pitch_limit")).second, '@');
				Rml::ElementFormControlInput *inputValueElem,
						*inputPitchElem;
				//pitch limit value
				inputValueElem =
						static_cast<Rml::ElementFormControlInput*>(cameraOptionsMenu->GetElementById(
								"pitch_limit_value"));
				inputValueElem->SetValue(paramValuesStr[1]);
				//pitch limit
				inputPitchElem =
						static_cast<Rml::ElementFormControlInput*>(cameraOptionsMenu->GetElementById(
								"pitch_limit"));
				//checks
				if (inputPitchElem->GetAttribute("checked"))
				{
					if (paramValuesStr[0] == "true")
					{
						//enable value input element
						inputValueElem->SetDisabled(false);
					}
					else
					{
						//checked -> unchecked: change event thrown
						inputPitchElem->RemoveAttribute("checked");
					}
				}
				else
				{
					if (paramValuesStr[0] != "true")
					{
						//disable value input element
						inputValueElem->SetDisabled(true);
					}
					else
					{
						//unchecked -> checked: change event thrown
						inputPitchElem->SetAttribute<string>(
								"checked", "true");
					}
				}
				//max linear speed
				setElementValue("max_linear_speed", cameraDriverParams,
						cameraOptionsMenu);
				//max angular speed
				setElementValue("max_angular_speed", cameraDriverParams,
						cameraOptionsMenu);
				//linear accel
				setElementValue("linear_accel", cameraDriverParams,
						cameraOptionsMenu);
				//angular accel
				setElementValue("angular_accel", cameraDriverParams,
						cameraOptionsMenu);
				//linear friction
				setElementValue("linear_friction", cameraDriverParams,
						cameraOptionsMenu);
				//angular friction
				setElementValue("angular_friction", cameraDriverParams,
						cameraOptionsMenu);
				//fast factor
				setElementValue("fast_factor", cameraDriverParams,
						cameraOptionsMenu);
				//sens_x
				setElementValue("sens_x", cameraDriverParams,
						cameraOptionsMenu);
				//sens_y
				setElementValue("sens_y", cameraDriverParams,
						cameraOptionsMenu);
			}
		}
	}
	else if (value == "camera::chaser_camera::options")
	{
		// This event is sent from the "onchange" of the "chaser_camera"
		//radio button. It shows or hides the related options.
		Rml::ElementDocument *cameraOptionsMenu =
				event.GetTargetElement()->GetOwnerDocument();
		if (cameraOptionsMenu == nullptr)
			return;

		Rml::Element *chaser_camera_options =
				cameraOptionsMenu->GetElementById("chaser_camera_options");
		if (chaser_camera_options)
		{
			//The "value" parameter of an "onchange" event is set to
			//the value the control would send if it was submitted;
			//so, the empty string if it is clear or to the "value"
			//attribute of the control if it is set.
			if (event.GetParameter<string>("value", "").empty())
			{
				chaser_camera_options->SetProperty("display", "none");
			}
			else
			{
				chaser_camera_options->SetProperty("display", "block");
				//set elements' values from options' values
				//chased object
				Rml::ElementFormControlSelect *objectsSelect =
						static_cast<Rml::ElementFormControlSelect*>(cameraOptionsMenu->GetElementById(
								"chased_object"));
				if (objectsSelect)
				{
					//remove all options
					objectsSelect->RemoveAll();
					//set object list
					list<shared_ptr<Object>>::iterator objectIter;
					list<shared_ptr<Object>> createdObjects =
					{ };
					int selectedIdx = objectsSelect->Add("", "");
					for (objectIter = createdObjects.begin();
							objectIter != createdObjects.end(); ++objectIter)
					{
						//add options
						string objectId = (*objectIter)->objectId;
						int i = objectsSelect->Add(objectId,
								objectId);
						if (objectId == chasedObject)
						{
							selectedIdx = i;
						}
					}
					//set first option as selected
					objectsSelect->SetSelection(selectedIdx);
				}
				//fixed relative position
				setElementChecked("fixed_relative_position", "true", "false",
						"true", cameraChaserParams, cameraOptionsMenu);
				//backward
				setElementChecked("backward", "true", "false", "true",
						cameraChaserParams, cameraOptionsMenu);
				//abs max distance
				setElementValue("abs_max_distance", cameraChaserParams,
						cameraOptionsMenu);
				//abs min distance
				setElementValue("abs_min_distance", cameraChaserParams,
						cameraOptionsMenu);
				//abs max height
				setElementValue("abs_max_height", cameraChaserParams,
						cameraOptionsMenu);
				//abs min height
				setElementValue("abs_min_height", cameraChaserParams,
						cameraOptionsMenu);
				//abs lookat distance
				setElementValue("abs_lookat_distance", cameraChaserParams,
						cameraOptionsMenu);
				//abs lookat height
				setElementValue("abs_lookat_height", cameraChaserParams,
						cameraOptionsMenu);
				//friction
				setElementValue("friction", cameraChaserParams,
						cameraOptionsMenu);
			}
		}
	}
	else if (value == "camera::object_picker::options")
	{
		// This event is sent from the "onchange" of the "object_picker"
		//radio button. It shows or hides the related options.
		Rml::ElementDocument *cameraOptionsMenu =
				event.GetTargetElement()->GetOwnerDocument();
		if (cameraOptionsMenu == nullptr)
			return;

		Rml::Element *object_picker_options =
				cameraOptionsMenu->GetElementById("object_picker_options");
		if (object_picker_options)
		{
			//The "value" parameter of an "onchange" event is set to
			//the value the control would send if it was submitted;
			//so, the empty string if it is clear or to the "value"
			//attribute of the control if it is set.
			if (event.GetParameter<string>("value", "").empty())
			{
				object_picker_options->SetProperty("display", "none");
			}
			else
			{
				object_picker_options->SetProperty("display", "block");
				//set elements' values from options' values
				//constraint type
				pickerCsIspherical ?
						cameraOptionsMenu->GetElementById(
								"spherical_constraint")->SetAttribute("checked",
								true) :
						cameraOptionsMenu->GetElementById("generic_constraint")->SetAttribute(
								"checked", true);
			}
		}
	}
	else if (value == "pitch_limit::change")
	{
		Rml::ElementDocument *cameraOptionsMenu =
				event.GetTargetElement()->GetOwnerDocument();
		//check
		if (static_cast<Rml::ElementFormControlInput*>(cameraOptionsMenu->GetElementById(
				"pitch_limit"))->GetAttribute("checked"))
		{
			static_cast<Rml::ElementFormControlInput*>(cameraOptionsMenu->GetElementById(
					"pitch_limit_value"))->SetDisabled(false);
		}
		else
		{
			static_cast<Rml::ElementFormControlInput*>(cameraOptionsMenu->GetElementById(
					"pitch_limit_value"))->SetDisabled(true);
		}
	}
	///Submit
	else if (value == "camera::form::submit_options")
	{
		string paramValue;
		//check if ok or cancel
		paramValue = event.GetParameter<string>("submit", "cancel");
		if (paramValue == "ok")
		{
			//set new camera type
			paramValue = event.GetParameter<string>("camera",
					"none");
			if (paramValue == "free_view_camera")
			{
				cameraType = free_view_camera;
				//set options' values from elements' values
				//pitch limit and value: enabled@limit
				string enabled = (
						event.GetParameter<string>("pitch_limit", "")
								== string("true") ? "true" : "false");
				string limit;
				if (enabled == "true")
				{
					limit = event.GetParameter<string>(
							"pitch_limit_value", "0.0");
				}
				else
				{
					limit = parseCompoundString(
							(*cameraDriverParams.find("pitch_limit")).second,
							'@')[1];
				}
				(*cameraDriverParams.find("pitch_limit")).second = enabled
						+ string("@") + limit;
				//max linear speed
				setOptionValue(event, "max_linear_speed", cameraDriverParams);
				//max angular speed
				setOptionValue(event, "max_angular_speed", cameraDriverParams);
				//linear accel
				setOptionValue(event, "linear_accel", cameraDriverParams);
				//angular accel
				setOptionValue(event, "angular_accel", cameraDriverParams);
				//linear friction
				setOptionValue(event, "linear_friction", cameraDriverParams);
				//angular friction
				setOptionValue(event, "angular_friction", cameraDriverParams);
				//fast factor
				setOptionValue(event, "fast_factor", cameraDriverParams);
				//sens_x
				setOptionValue(event, "sens_x", cameraDriverParams);
				//sens_y
				setOptionValue(event, "sens_y", cameraDriverParams);
			}
			else if (paramValue == "chaser_camera")
			{
				cameraType = chaser_camera;
				//set options' values from elements' values
				//chased object
				chasedObject = event.GetParameter<string>(
						"chased_object", "");
				if (not chasedObject.empty())
				{
					(*cameraChaserParams.find("chased_object")).second =
							chasedObject;
				}
				//fixed relative position
				setOptionChecked(event, "fixed_relative_position", "true",
						"false", cameraChaserParams);
				//backward
				setOptionChecked(event, "backward", "true", "false",
						cameraChaserParams);
				//abs max distance
				setOptionValue(event, "abs_max_distance", cameraChaserParams);
				//abs min distance
				setOptionValue(event, "abs_min_distance", cameraChaserParams);
				//abs max height
				setOptionValue(event, "abs_max_height", cameraChaserParams);
				//abs min height
				setOptionValue(event, "abs_min_height", cameraChaserParams);
				//abs lookat distance
				setOptionValue(event, "abs_lookat_distance",
						cameraChaserParams);
				//abs lookat height
				setOptionValue(event, "abs_lookat_height", cameraChaserParams);
				//friction
				setOptionValue(event, "friction", cameraChaserParams);
			}
			else if (paramValue == "object_picker")
			{
				cameraType = object_picker;
				//set options' values from elements' values
				//constraint type
				paramValue = event.GetParameter<string>(
						"constraint_type", "");
				if (paramValue == "spherical")
				{
					pickerCsIspherical = true;
				}
				else if (paramValue == "generic")
				{
					pickerCsIspherical = false;
				}
			}
			else
			{
				//default
				cameraType = none;
			}
		}
		//close (i.e. unload) the camera options menu and set as closed..
		Rml::ElementDocument *cameraOptionsMenu =
				event.GetTargetElement()->GetOwnerDocument();
		cameraOptionsMenu->Close();
		//return to main menu.
		GameGUIManager::get_global_ptr()->gGuiMainMenu->Show();
	}
}

//helper
inline void setCameraType(const CameraType &newType,
		const CameraType &actualType)
{

	//return if no camera type change is needed
	RETURN_ON_COND(newType == actualType,)

	//unset actual camera type
	if (actualType == free_view_camera)
	{
		if (cameraEnabled)
		{
			//enabled: then disable it
			//disable
			cameraEnabled = false;
			//remove text
			textNode.remove_node();
		}
	}
	else if (actualType == chaser_camera)
	{
		if (cameraEnabled)
		{
			//enabled: then disable it
			//disable
			cameraEnabled = false;
			//remove text
			textNode.remove_node();
		}
	}
	else if (actualType == object_picker)
	{
		if (pickerOn)
		{
			if (cameraEnabled)
			{
				//enabled: then disable it
				//disable
				cameraEnabled = false;
			}

			//picker on: remove
			pickerOn = false;
			//remove text
			textNode.remove_node();
		}
	}
	//set new type
	if (newType == free_view_camera)
	{
		//add a new Driver component to camera and ...
		//... enable it
		cameraEnabled = true;
		//write text
		writeText(textNode, "Free View Camera", 0.05,
				LVecBase4(1.0, 1.0, 0.0, 1.0), LVector3f(-1.0, 0, -0.9),
				GameGUIManager::get_global_ptr()->get_framework());
	}
	else if (newType == chaser_camera)
	{
		//add a new Chaser component to camera and ...
		//... enable it
		cameraEnabled = true;
		//write text
		writeText(textNode, "Camera Chasing '" + chasedObject + "'", 0.05,
				LVecBase4(1.0, 1.0, 0.0, 1.0), LVector3f(-1.0, 0, -0.9),
				GameGUIManager::get_global_ptr()->get_framework());

	}
	else if (newType == object_picker)
	{
		if (not pickerOn)
		{
			//add a new Driver component to camera without mouse movement control and ...
			ParameterTable cameraDriverParamsNoMouse = cameraDriverParams;
			//... enable it
			cameraEnabled = true;
			//picker off: add
			pickerOn = true;
			//write text
			writeText(textNode, "Object Picker Active", 0.05,
					LVecBase4(1.0, 1.0, 0.0, 1.0), LVector3f(-1.0, 0, -0.9),
					GameGUIManager::get_global_ptr()->get_framework());
		}
	}

}

//preset function called from main menu
void rocketPreset()
{
	setCameraType(none, cameraType);
}

//commit function called main menu
void rocketCommit()
{
	setCameraType(cameraType, none);
}

} // namespace

void camera_initialization()
{
	//register the add element function to (RmlUi) main menu
	GameGUIManager::get_global_ptr()->gGuiAddElementsFunctions.push_back(
			&rocketAddElements);
	//register the event handler to main menu for each event value
	GameGUIManager::get_global_ptr()->gGuiEventHandlers["camera::options"] =
			&rocketEventHandler;
	GameGUIManager::get_global_ptr()->gGuiEventHandlers["camera::body::load_logo"] =
			&rocketEventHandler;
	GameGUIManager::get_global_ptr()->gGuiEventHandlers["camera::form::submit_options"] =
			&rocketEventHandler;
	GameGUIManager::get_global_ptr()->gGuiEventHandlers["camera::free_view_camera::options"] =
			&rocketEventHandler;
	GameGUIManager::get_global_ptr()->gGuiEventHandlers["camera::chaser_camera::options"] =
			&rocketEventHandler;
	GameGUIManager::get_global_ptr()->gGuiEventHandlers["camera::object_picker::options"] =
			&rocketEventHandler;
	GameGUIManager::get_global_ptr()->gGuiEventHandlers["pitch_limit::change"] =
			&rocketEventHandler;
	//register the preset function to main menu
	GameGUIManager::get_global_ptr()->gGuiPresetFunctions.push_back(
			&rocketPreset);
	//register the commit function to main menu
	GameGUIManager::get_global_ptr()->gGuiCommitFunctions.push_back(
			&rocketCommit);
}
