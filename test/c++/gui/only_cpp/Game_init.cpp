/**
 * \file Game_init.cpp
 *
 * \date 2019-10-04
 * \author consultit
 */

#include "Game_init.h"
#include "GameGUIManager.h"

//globals
string rmlUiBaseDir;
//common text writing
void writeText(NodePath &textNode, const std::string &text, float scale,
		const LVecBase4 &color, const LVector3f &location,
		PandaFramework *framework)
{
	textNode = NodePath(new TextNode("CommonTextNode"));
	textNode.reparent_to(framework->get_window(0)->get_render_2d());
	textNode.set_bin("fixed", 50);
	textNode.set_depth_write(false);
	textNode.set_depth_test(false);
	textNode.set_billboard_point_eye();
	DCAST(TextNode, textNode.node())->set_text(text);
	textNode.set_scale(scale);
	textNode.set_color(color);
	textNode.set_pos(location);
}

void guiSystemInitialization()
{
	//gui main menu
	string mGuiMainMenuParam{ rmlUiBaseDir + "/ely-main-menu.rml" };
	//gui exit menu
	string mGuiExitMenuParam{ rmlUiBaseDir + "/ely-exit-menu.rml" };
	//gui font paths
	list<string> fontPaths
	{
		rmlUiBaseDir + "/Delicious-Roman.otf",
		rmlUiBaseDir + "/Delicious-Italic.otf",
		rmlUiBaseDir + "/Delicious-Bold.otf",
		rmlUiBaseDir + "/Delicious-BoldItalic.otf"
	};
	//
	//gui main menu if any
	if (not mGuiMainMenuParam.empty())
	{
		GameGUIManager::get_global_ptr()->gGuiMainMenuPath = mGuiMainMenuParam;
	}
	//gui exit menu if any
	if (not mGuiExitMenuParam.empty())
	{
		GameGUIManager::get_global_ptr()->gGuiExitMenuPath = mGuiExitMenuParam;
	}
	for (auto path : fontPaths)
	{
		//an empty font_path is ignored
		if (not path.empty())
		{
			GameGUIManager::get_global_ptr()->gGuiFontPaths.push_back(path);
		}
	}
}

//locals
namespace
{
const int CALLBACKSNUM = 5;

void showMainMenu(const Event *e)
{
	GameGUIManager::get_global_ptr()->showMainMenu();
}

void showExitMenu(const Event *e)
{
	GameGUIManager::get_global_ptr()->showExitMenu();
}

//event handler added to the main one
void rocketEventHandler(const string &value, Rml::Event &event)
{
	if (value == "main::body::load_logo")
	{
		PRINT_DEBUG("main::body::load_logo");
	}
}

}  // namespace

void elyPreObjects_initialization()
{
	//register the add element function to gui (Rocket) main menu
	//register the event handler to gui main menu for each event value
	GameGUIManager::get_global_ptr()->gGuiEventHandlers["main::body::load_logo"] =
			&rocketEventHandler;
	//register the preset function to gui main menu
	//register the commit function to gui main menu
}

void elyPostObjects_initialization(PandaFramework *framework)
{
	///Gui (libRocket)
	//add show main menu event handler
	EventHandler::get_global_event_handler()->add_hook("m", &showMainMenu);
	//handle "close request" and "esc" events
	framework->get_window(0)->get_graphics_window()->set_close_request_event(
			"close_request_event");
	EventHandler::get_global_event_handler()->add_hook("close_request_event",
			&showExitMenu);
	EventHandler::get_global_event_handler()->add_hook("escape", &showExitMenu);
}
