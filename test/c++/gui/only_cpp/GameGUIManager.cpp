/**
 * \file GameGUIManager.cpp
 *
 * \date 2019-10-04
 * \author consultit
 */

#include "GameGUIManager.h"
#include "p3guiRegion.h"
#include "RmlUi/Include/RmlUi/Debugger.h"
#include "RmlUi/Include/RmlUi/Core/Factory.h"

GameGUIManager::GameGUIManager(PandaFramework *framework) :
		mFramework(framework)
{
	//gui (RmlUi) variables initialization
	gGuiRmlUiContext = nullptr;
	gGuiMainMenu = nullptr;
	gGuiExitMenu = nullptr;
	gGuiMainMenuPath.clear();
	gGuiExitMenuPath.clear();
	//registered by subsystems to add their element (tags) to main menu
	gGuiAddElementsFunctions.clear();
	//registered by subsystems to handle their events
	gGuiEventHandlers.clear();
	//registered by some subsystems that need to preset themselves before main/exit menus are shown
	gGuiPresetFunctions.clear();
	//registered by some subsystems that need to commit their changes after main/exit menus are closed
	gGuiCommitFunctions.clear();
	gGuiFontPaths.clear();
	gGuiMainPresetsCommits = false;
	mEventListenerInstancer = nullptr;
}

GameGUIManager::~GameGUIManager()
{
}

void GameGUIManager::guiSetup()
{
	RETURN_ON_COND(gGuiMainMenuPath.empty() or gGuiExitMenuPath.empty(),)

	///RmlUi initialization
	//load fonts
	std::vector<std::string>::const_iterator iterPath;
	for (iterPath = gGuiFontPaths.begin(); iterPath != gGuiFontPaths.end();
			++iterPath)
	{
		Rml::LoadFontFace(string(iterPath->c_str()));
	}

	//initialize one region for all documents
	PT(P3GUIRegion) region = P3GUIRegion::make("elyRocket",
			mFramework->get_window(0)->get_graphics_window());
	region->set_active(true);

	//set input handler
	PT(P3GUIInputHandler) inputHandler = new P3GUIInputHandler();
	mFramework->get_window(0)->get_mouse().attach_new_node(inputHandler);
	region->set_input_handler(inputHandler);

	//set global context variable
	gGuiRmlUiContext = region->get_context();

	/* xxx
	 //initialize controls
	 Rml::Initialise();
	 */

	//register the main EventListenerInstancer: used by all the application documents
	mEventListenerInstancer = new MainEventListenerInstancer(mFramework);
	Rml::Factory::RegisterEventListenerInstancer(mEventListenerInstancer);

#ifdef ELY_DEBUG
	Rml::Debugger::Initialise(gGuiRmlUiContext);
	Rml::Debugger::SetVisible(true);
#endif
}

void GameGUIManager::showMainMenu()
{
	RETURN_ON_COND(gGuiMainMenuPath.empty() or gGuiExitMenuPath.empty(),)
	//return if already shown or we are asking to exit
	RETURN_ON_COND(
			gGuiRmlUiContext->GetDocument("main_menu")
					or gGuiRmlUiContext->GetDocument("exit_menu"),)

	//call all registered preset functions
	std::vector<void (*)()>::iterator iter;
	for (iter = gGuiPresetFunctions.begin(); iter != gGuiPresetFunctions.end();
			++iter)
	{
		(*iter)();
	}
	//presets & commits are executing by main menu
	gGuiMainPresetsCommits = true;

	// Load and show the main document.
	gGuiMainMenu = gGuiRmlUiContext->LoadDocument(gGuiMainMenuPath);
	if (gGuiMainMenu != nullptr)
	{
		gGuiMainMenu->GetElementById("title")->SetInnerRML(
				gGuiMainMenu->GetTitle());
		//call all registered add elements functions
		std::vector<void (*)(Rml::ElementDocument*)>::iterator iter;
		for (iter = gGuiAddElementsFunctions.begin();
				iter != gGuiAddElementsFunctions.end(); ++iter)
		{
			(*iter)(gGuiMainMenu);
		}
		gGuiMainMenu->Show();
	}
}

void GameGUIManager::showExitMenu()
{
	RETURN_ON_COND(gGuiMainMenuPath.empty() or gGuiExitMenuPath.empty(),)
	//return if we are already asking to exit
	RETURN_ON_COND(gGuiRmlUiContext->GetDocument("exit_menu"),)

	//if presets & commits are not being executed by main menu
	if (not gGuiMainPresetsCommits)
	{
		//call all registered preset functions
		std::vector<void (*)()>::iterator iter;
		for (iter = gGuiPresetFunctions.begin();
				iter != gGuiPresetFunctions.end(); ++iter)
		{
			(*iter)();
		}
	}

	// Load and show the exit menu modal document.
	gGuiExitMenu = gGuiRmlUiContext->LoadDocument(gGuiExitMenuPath);
	if (gGuiExitMenu != nullptr)
	{
		gGuiExitMenu->GetElementById("title")->SetInnerRML(
				gGuiExitMenu->GetTitle());
		//
		gGuiExitMenu->Show(Rml::ModalFlag::Modal);
	}
}

///MainEventListener stuff
MainEventListener::MainEventListener(const string &value) :
		EventListener(value)
{
}

MainEventListener::~MainEventListener()
{
}

void MainEventListener::ProcessEvent(Rml::Event &event)
{
#ifdef ELY_DEBUG
	Rml::EventPhase phase = event.GetPhase();
	string phaseStr;
	switch (phase)
	{
	case Rml::EventPhase::Bubble:
		phaseStr = "PHASE_BUBBLE";
		break;
	case Rml::EventPhase::Capture:
		phaseStr = "PHASE_CAPTURE";
		break;
	case Rml::EventPhase::Target:
		phaseStr = "PHASE_TARGET";
		break;
	case Rml::EventPhase::None:
	default:
		phaseStr = "PHASE_None";
		break;
	}
	PRINT_DEBUG("Event type: " << event.GetType());
	PRINT_DEBUG("Event value: " << mValue);
	PRINT_DEBUG("Event phase: " << phaseStr);
	PRINT_DEBUG(
			"Event target element tag: " << event.GetTargetElement()->GetTagName());
	PRINT_DEBUG(
			"Event current element tag: " << event.GetTargetElement()->GetTagName());
	int pos = 0;
	string key, valueAsStr;
	for (auto item : event.GetParameters())
	{
		auto key = item.first;
		auto value = item.second;
		valueAsStr = value.Get<string>();
		PRINT_DEBUG(
				"Event parameter: pos = " << pos++ << " key = \"" << key << "\"" << " value = " << valueAsStr);
	}
	PRINT_DEBUG("");
#endif

	if (mValue == "MAIN::ENTER_GAME")
	{
		//call all registered commit functions
		std::vector<void (*)()>::iterator iter;
		for (iter =
				GameGUIManager::get_global_ptr()->gGuiCommitFunctions.begin();
				iter
						!= GameGUIManager::get_global_ptr()->gGuiCommitFunctions.end();
				++iter)
		{
			(*iter)();
		}
		//presets & commits are not being executed by main menu any more
		GameGUIManager::get_global_ptr()->gGuiMainPresetsCommits = false;
		//close (i.e. unload) the main document and set as closed..
		GameGUIManager::get_global_ptr()->gGuiMainMenu->Close();
	}
	else if (mValue == "MAIN::EXIT_GAME")
	{
		GameGUIManager::get_global_ptr()->showExitMenu();
	}
	else if (mValue == "EXIT::SUBMIT_EXIT")
	{
		string paramValue;
		//check if ok or cancel
		paramValue = event.GetParameter<string>("submit", "cancel");
		//close (i.e. unload) the exit menu and set as closed..
		GameGUIManager::get_global_ptr()->gGuiExitMenu->Close();
		if (paramValue == "ok")
		{
			//user wants to exit: unload all documents
			GameGUIManager::get_global_ptr()->gGuiRmlUiContext->UnloadAllDocuments();
			//set PandaFramework exit flag
			GameGUIManager::get_global_ptr()->get_framework()->set_exit_flag();
		}
		//if presets & commits are not being executed by main menu
		if (not GameGUIManager::get_global_ptr()->gGuiMainPresetsCommits)
		{
			//call all registered commit functions
			std::vector<void (*)()>::iterator iter;
			for (iter =
					GameGUIManager::get_global_ptr()->gGuiCommitFunctions.begin();
					iter
							!= GameGUIManager::get_global_ptr()->gGuiCommitFunctions.end();
					++iter)
			{
				(*iter)();
			}
		}
	}
	else
	{
		//check if it is a registered event name
		if (GameGUIManager::get_global_ptr()->gGuiEventHandlers.find(mValue)
				!= GameGUIManager::get_global_ptr()->gGuiEventHandlers.end())
		{
			//call the registered event handler
			GameGUIManager::get_global_ptr()->gGuiEventHandlers[mValue](mValue,
					event);
		}
	}
}
