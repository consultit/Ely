/**
 * \file libtools_test.cpp
 *
 * \date 2018-06-28
 * \author consultit
 */

#include "suite.h"

struct LibtoolsTestCaseFixture
{
	LibtoolsTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("ctor LibtoolsTestCaseFixture");
	}
	~LibtoolsTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("dtor LibtoolsTestCaseFixture");
	}
};

/// Libtools suite
BOOST_FIXTURE_TEST_SUITE(Libtools, LibtoolsSuiteFixture);

/// Test cases
BOOST_AUTO_TEST_CASE(LibtoolsTest)
{
	BOOST_TEST_MESSAGE("LibtoolsTest");
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END(); // LibtoolsSuiteFixture
