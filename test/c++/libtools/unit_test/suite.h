/**
 * \file suite.h
 *
 * \date 2018-06-28
 * \author consultit
 */
#ifndef LIBTOOLS_SUITE_H_
#define LIBTOOLS_SUITE_H_

#include <boost/test/unit_test.hpp>

struct LibtoolsSuiteFixture
{
	LibtoolsSuiteFixture()
	{
		BOOST_TEST_MESSAGE("ctor LibtoolsSuiteFixture");
	}
	~LibtoolsSuiteFixture()
	{
		BOOST_TEST_MESSAGE("dtor LibtoolsSuiteFixture");
	}
};

#endif /* LIBTOOLS_SUITE_H_ */
