/**
 * \file suite.h
 *
 * \date 2018-06-28
 * \author consultit
 */
#ifndef AI_SUITE_H_
#define AI_SUITE_H_

#include <boost/test/unit_test.hpp>

struct AISuiteFixture
{
	AISuiteFixture()
	{
		BOOST_TEST_MESSAGE("ctor AISuiteFixture");
	}
	~AISuiteFixture()
	{
		BOOST_TEST_MESSAGE("dtor AISuiteFixture");
	}
};

#endif /* AI_SUITE_H_ */
