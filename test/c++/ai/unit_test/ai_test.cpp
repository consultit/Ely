/**
 * \file ai.cpp
 *
 * \date 2018-06-28
 * \author consultit
 */

#include "suite.h"

struct AITestCaseFixture
{
	AITestCaseFixture()
	{
		BOOST_TEST_MESSAGE("ctor AITestCaseFixture");
	}
	~AITestCaseFixture()
	{
		BOOST_TEST_MESSAGE("dtor AITestCaseFixture");
	}
};

/// AI suite
BOOST_FIXTURE_TEST_SUITE(AI, AISuiteFixture);

/// Test cases
BOOST_AUTO_TEST_CASE(AITest)
{
	BOOST_TEST_MESSAGE("AITest");
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END(); // AISuiteFixture
