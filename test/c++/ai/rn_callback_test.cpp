/**
 * \file main.cpp
 *
 * \date 2016-09-12
 * \author consultit
 */

#include "common.h"
#include <rnCrowdAgent.h>

///global data
PT(RNCrowdAgent)crowdAgent;

ClockObject* globalClock = NULL;
void crowdAgentCallback(PT(RNCrowdAgent));
void navMeshCallback(PT(RNNavMesh));

int main(int argc, char *argv[])
{
	startFramework(argc, argv, "'recastnavigation basic callback test'");

	/// here is room for your own code

	cout << "create a nav mesh manager" << endl;
	GameAIManager* navMesMgr = new GameAIManager();

	cout << "reparent the reference node to render" << endl;
	navMesMgr->get_reference_node_path().reparent_to(window->get_render());

	cout << "get a sceneNP as owner model" << endl;
	NodePath sceneNP = window->load_model(framework.get_models(),
			"nav_test.egg");

	cout << "create a nav mesh (it is attached to the reference node)" << endl;
	NodePath navMeshNP = navMesMgr->create_nav_mesh("callback nav-mesh");
	PT(RNNavMesh)navMesh = DCAST(RNNavMesh, navMeshNP.node());
	navMesh->set_update_callback(navMeshCallback);

	cout << "mandatory: set sceneNP as owner of navMesh" << endl;
	navMesh->set_owner_object(sceneNP);

	cout << "setup the navMesh with sceneNP as its owner object" << endl;
	navMesh->setup();

	cout << "reparent sceneNP to the reference node" << endl;
	sceneNP.reparent_to(navMesMgr->get_reference_node_path());

	cout << "get the agent model" << endl;
	NodePath agentNP = window->load_model(framework.get_models(), "eve.egg");
	agentNP.set_scale(0.40);

	cout << "create the crowd agent (it is attached to the reference node) and set its position" << endl;
	NodePath crowdAgentNP = navMesMgr->create_crowd_agent("crowdAgent");
	crowdAgent = DCAST(RNCrowdAgent, crowdAgentNP.node());
	crowdAgentNP.set_pos(24.0, -20.4, -2.37);
	crowdAgent->set_update_callback(crowdAgentCallback);
    globalClock = ClockObject::get_global_clock();

	cout << "attach the agent model to crowdAgent" << endl;
	agentNP.reparent_to(crowdAgentNP);

	cout << "attach the crowd agent to the nav mesh" << endl;
	navMesh->add_crowd_agent(crowdAgentNP);
	cout << *crowdAgent << " added to: " << *crowdAgent->get_nav_mesh() << endl;

	cout << "start the path finding default update task" << endl;
	navMesMgr->start_default_update();

	cout << "DEBUG DRAWING: make the debug reference node path sibling of the reference node" << endl;
	navMesMgr->get_reference_node_path_debug().reparent_to(
			window->get_render());
	cout << "enable debug drawing" << endl;
	navMesh->enable_debug_drawing(window->get_camera_group());

	cout << "toggle debug draw" << endl;
	navMesh->toggle_debug_drawing(true);

	cout << "set crowd agent move target on scene surface" << endl;
	crowdAgent->set_move_target(LPoint3f(-20.5, 5.2, -2.36));

	// place camera trackball (local coordinate)
	PT(Trackball)trackball = DCAST(Trackball, window->get_mouse().find("**/+Trackball").node());
	trackball->set_pos(-10.0, 90.0, -2.0);
	trackball->set_hpr(0.0, 15.0, 0.0);

	// do the main loop, equals to call app.run() in python
	framework.main_loop();

	return (0);
}

// crowd agent update callback function
void crowdAgentCallback(PT(RNCrowdAgent)crowdAgent)
{
    cout << crowdAgent->get_name() + " params:" << endl;
    cout << crowdAgent->get_params() << endl;
}

// nav mesh update callback function
void navMeshCallback(PT(RNNavMesh)navMesh)
{
	cout << navMesh->get_name() + " real time and dt: " +
			str(globalClock->get_real_time()) + str(globalClock->get_dt()) << endl;
}
