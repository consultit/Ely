/**
 * \file audio.cpp
 *
 * \date 2018-06-28
 * \author consultit
 */

#include "suite.h"

struct AudioTestCaseFixture
{
	AudioTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("ctor AudioTestCaseFixture");
	}
	~AudioTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("dtor AudioTestCaseFixture");
	}
};

/// Audio suite
BOOST_FIXTURE_TEST_SUITE(Audio, AudioSuiteFixture);

/// Test cases
BOOST_AUTO_TEST_CASE(AudioTest)
{
	BOOST_TEST_MESSAGE("AudioTest");
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END(); // AudioSuiteFixture
