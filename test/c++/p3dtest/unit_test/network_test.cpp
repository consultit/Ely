/**
 * \file network_test.cpp
 *
 * \date 2018-08-25
 * \author consultit
 */

#include <unit_test/suite.h>
#include <networkObject.h>

string get_class_id_str(uint32_t classId);
uint32_t get_class_id_int(const string& classIdStr);

struct NetTestCaseFixture
{
	NetTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("ctor NetTestCaseFixture");
	}
	~NetTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("dtor NetTestCaseFixture");
	}
};

/// NetTestSuite
BOOST_FIXTURE_TEST_SUITE(NetTestSuite, P3dTestSuiteFixture);

/// Test cases
BOOST_AUTO_TEST_CASE(ClassIdTEST)
{
	BOOST_TEST_MESSAGE("ClassIdTEST");
	uint32_t classId = get_class_id_int("MOBJ");
	BOOST_CHECK_EQUAL("MOBJ", get_class_id_str(classId));
}

BOOST_AUTO_TEST_SUITE_END(); // NetTestSuite
