/**
 * \file suite.h
 *
 * \date 2018-08-25
 * \author consultit
 */

#ifndef P3D_SUITE_H_
#define P3D_SUITE_H_

#include <boost/test/unit_test.hpp>

struct P3dTestSuiteFixture
{
	P3dTestSuiteFixture()
	{
		BOOST_TEST_MESSAGE("ctor P3dTestSuiteFixture");
	}
	~P3dTestSuiteFixture()
	{
		BOOST_TEST_MESSAGE("dtor P3dTestSuiteFixture");
	}
};

#endif /* P3D_SUITE_H_ */
