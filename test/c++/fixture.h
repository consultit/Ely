/**
 * \file fixture.h
 *
 * \date 2018-06-28
 * \author consultit
 */
#ifndef FIXTURE_H_
#define FIXTURE_H_

#include <boost/test/unit_test.hpp>

struct ElyGlobalFixture
{
	ElyGlobalFixture()
	{
		BOOST_TEST_MESSAGE("ctor ElyGlobalFixture");
	}
	~ElyGlobalFixture()
	{
		BOOST_TEST_MESSAGE("dtor ElyGlobalFixture");
	}
};

#endif /* FIXTURE_H_ */
