/**
 * \file runner.cpp
 *
 * \date 2018-06-27
 * \author consultit
 */

// This is the Boost runner common to all unit-tests

#define BOOST_TEST_MODULE "Ely Tests"
#include <boost/test/unit_test.hpp>
#include "fixture.h"

BOOST_GLOBAL_FIXTURE( ElyGlobalFixture );
