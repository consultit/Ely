/**
 * \file control_test.cpp
 *
 * \date 2018-06-28
 * \author consultit
 */

#include "suite.h"

struct ControlTestCaseFixture
{
	ControlTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("ctor ControlTestCaseFixture");
	}
	~ControlTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("dtor ControlTestCaseFixture");
	}
};

/// Control suite
BOOST_FIXTURE_TEST_SUITE(Control, ControlSuiteFixture);

/// Test cases
BOOST_AUTO_TEST_CASE(ControlTest)
{
	BOOST_TEST_MESSAGE("ControlTest");
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END(); // ControlSuiteFixture
