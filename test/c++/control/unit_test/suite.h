/**
 * \file suite.h
 *
 * \date 2018-06-28
 * \author consultit
 */
#ifndef CONTROL_SUITE_H_
#define CONTROL_SUITE_H_

#include <boost/test/unit_test.hpp>

struct ControlSuiteFixture
{
	ControlSuiteFixture()
	{
		BOOST_TEST_MESSAGE("ctor ControlSuiteFixture");
	}
	~ControlSuiteFixture()
	{
		BOOST_TEST_MESSAGE("dtor ControlSuiteFixture");
	}
};

#endif /* CONTROL_SUITE_H_ */
