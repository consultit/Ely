#include "MouseClient.h"

MouseClient::MouseClient() :
		Mouse()
{
	// add r/w members
	// set r/w members' dirty states
	// callbacks
#ifdef ELY_DEBUG
	set_update_callback(&GameObject::update_clbk);
#endif //ELY_DEBUG
//	set_all_state_mask_callback(clbk);
//	set_handle_dying_callback(clbk);
//	set_pre_write_callback(clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
//	set_post_read_callback(clbk);
}

TYPED_OBJECT_API_DEF(MouseClient)
