#include "Mouse.h"
#include <utilities.h>

Mouse::Mouse() :
		GameObject(get_class_id_int("MOUS"))
{
	set_owner_object(Utilities::load_model("eve"));
	set_scale(0.2);
	LVecBase3f modelDims;
	LVector3f modelDeltaCenter;
	Utilities::get_bounding_dimensions(get_owner_object(), modelDims, modelDeltaCenter);
	set_collision_radius(modelDims.get_y() / 2.0);
	set_rotation(0.0f);
	set_color(Colors::White);
	// add r/w members
	DataVariables& members = get_members();
	// set r/w members' dirty states
	members.set_dirty_state("posX", EMRS_Pose);
	members.set_dirty_state("posY", EMRS_Pose);
	members.set_dirty_state("posZ", 0);
	members.set_dirty_state("rotH", EMRS_Pose);
	members.set_dirty_state("rotP", 0);
	members.set_dirty_state("rotR", 0);
	members.set_dirty_state("color", EMRS_Color);
	// callbacks
//	set_update_callback (clbk);
	set_all_state_mask_callback(&Mouse::get_all_state_mask_clbk);
//	set_handle_dying_callback(clbk);
	set_pre_write_callback(&Mouse::pre_write_clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
	set_post_read_callback(&Mouse::post_read_clbk);
}

bool Mouse::HandleCollisionWithCat(PT(RoboCat) inCat)
{
	(void) inCat;
	return false;
}

/*fixme left only for reference
uint32_t Mouse::write(OutputMemoryBitStream& inOutputStream,
		uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & EMRS_Pose)
	{
		inOutputStream.write((bool) true);

		LPoint3f location = get_location();
		inOutputStream.write(location.get_x());
		inOutputStream.write(location.get_y());

		inOutputStream.write(get_rotation());

		writtenState |= EMRS_Pose;
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	if (inDirtyState & EMRS_Color)
	{
		inOutputStream.write((bool) true);

		inOutputStream.write(get_color());

		writtenState |= EMRS_Color;
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	return writtenState;
}
*/

void Mouse::pre_write_clbk(PT(NetworkObject) THIS,
		const BitMask32& writeState)
{
	// owner object -> r/w members synchronization
	PT(Mouse) downTHIS = DCAST(Mouse, THIS);
	if (writeState.get_word() & EMRS_Pose)
	{
		downTHIS->SetPosVar(downTHIS->get_location());
		downTHIS->SetRotVar(downTHIS->get_rotation());
	}
	if (writeState.get_word() & EMRS_Color)
	{
		downTHIS->SetColVar(downTHIS->get_color());
	}
}

/*fixme left only for reference
void Mouse::read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	inInputStream.read(stateBit);
	if (stateBit)
	{
		LPoint3f location(0.f, 0.f, 0.f);
		inInputStream.read(location[0]);
		inInputStream.read(location[1]);
		set_location(location);

		LVecBase3f rotation;
		inInputStream.read(rotation);
		set_rotation(rotation);
	}

	inInputStream.read(stateBit);
	if (stateBit)
	{
		LVecBase4f color;
		inInputStream.read(color);
		set_color(color);
	}
}
*/

void Mouse::post_read_clbk(PT(NetworkObject)THIS, const BitMask32& readState)
{
	// r/w members -> owner object synchronization
	PT(Mouse) downTHIS = DCAST(Mouse, THIS);
	if (readState.get_word() & EMRS_Pose)
	{
		downTHIS->set_location(downTHIS->GetPosVar());
		downTHIS->set_rotation(downTHIS->GetRotVar());
	}
	if (readState.get_word() & EMRS_Color)
	{
		downTHIS->set_color(downTHIS->GetColVar());
	}
}

TYPED_OBJECT_API_DEF(Mouse)
