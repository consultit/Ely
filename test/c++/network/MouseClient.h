#ifndef MouseClient_h
#define MouseClient_h

#include "Mouse.h"

class MouseClient: public Mouse
{
public:
	static PT(NetworkObject) StaticCreate()
	{
		NetworkObject* go = new MouseClient();
		go->get_owner_object().reparent_to(render);
		return PT(NetworkObject)(go);
	}

protected:
	MouseClient();

TYPED_OBJECT_API_DECL(MouseClient, Mouse)
};

#endif //MouseClient_h
