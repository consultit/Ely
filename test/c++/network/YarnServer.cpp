#include "YarnServer.h"

YarnServer::YarnServer() :
		Yarn()
{
/*fixme
	get_owner_object().remove_node();
	// add a collision node to render
	CollisionSphere* collSolid = new CollisionSphere(0, 0, 0, get_collision_radius());
	CollisionNode* collNode = new CollisionNode("collNode");
	collNode->add_solid(collSolid);
	NodePath collNodeNP = render.attach_new_node(collNode);
	set_owner_object(collNodeNP);
*/
	//yarn lives a second...
	float ttl = mMuzzleSpeed > 0 ? HALF_WORLD_WIDTH / mMuzzleSpeed : 1.f;
	mTimeToDie = Timing::get_global_ptr()->get_frame_start_time() + ttl;
	// add r/w members
	// set r/w members' dirty states
	// callbacks
	set_update_callback(&YarnServer::update_clbk);
//	set_all_state_mask_callback (clbk);
	set_handle_dying_callback(&YarnServer::handle_dying_clbk);
//	set_pre_write_callback(clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
//	set_post_read_callback(clbk);
}

void YarnServer::handle_dying_clbk(PT(NetworkObject)THIS)
{
	GameNetworkManagerServer::get_global_ptr()->unregister_network_object(THIS);
}

void YarnServer::update_clbk(PT(NetworkObject)THIS)
{
	Yarn::update_clbk(THIS);

	PT(YarnServer) downTHIS = DCAST(YarnServer, THIS);
	if (Timing::get_global_ptr()->get_frame_start_time() > downTHIS->mTimeToDie)
	{
		THIS->set_does_want_to_die(true);
	}

}

bool YarnServer::HandleCollisionWithCat(PT(RoboCat) inCat)
{
	if (inCat->GetPlayerId() != (uint32_t) GetPlayerId())
	{
		//kill yourself!
		set_does_want_to_die(true);

		DCAST(RoboCatServer, inCat)->TakeDamage(GetPlayerId());

	}

	return false;
}

TYPED_OBJECT_API_DEF(YarnServer)
