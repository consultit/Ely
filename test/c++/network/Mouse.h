#ifndef Mouse_h
#define Mouse_h

#include "common.h"

class RoboCat;

class Mouse: public GameObject
{
public:
	static constexpr uint32_t MOUS = constUInt32('M', 'O', 'U', 'S');

	enum EMouseReplicationState
	{
		EMRS_Pose = 1 << 0, EMRS_Color = 1 << 1,

		EMRS_AllState = EMRS_Pose | EMRS_Color
	};

	static uint32_t get_all_state_mask_clbk(PT(NetworkObject))
	{
		return EMRS_AllState;
	}

	static void pre_write_clbk(PT(NetworkObject) THIS, const BitMask32& writeState);
	static void post_read_clbk(PT(NetworkObject) THIS, const BitMask32& readState);

	virtual bool HandleCollisionWithCat(PT(RoboCat) inCat);

protected:
	Mouse();

TYPED_OBJECT_API_DECL(Mouse, GameObject)
};

#endif //Mouse_h
