/**
 * \file Hud.cpp
 *
 * \date 2018-06-03
 * \author consultit
 */

#include "HUD.h"

#include "ScoreBoardManager.h"
#include <windowFramework.h>
#include <fontPool.h>
#include <support/NetworkMath.h>
#include <support/StringUtils.h>
#include "../../../ely/network/source/gameNetworkManagerClient.h"
extern WindowFramework *window;

using std::ostringstream;

unique_ptr<HUD> HUD::sInstance;

HUD::HUD() :
		mScoreBoardOrigin(50.f, 60.f, 0.0f), mBandwidthOrigin(50.f, 10.f, 0.0f), mRoundTripTimeOrigin(
				50.f, 10.f, 0.0f), mScoreOffset(0.f, 50.f, 0.0f), mHealthOffset(
				1000, 10.f, 0.0f), mHealth(0), m_textNodeIdx(0), m_prevTextNodeIdx(
				-1), m_textNodesSize(0), m_textScale(0.05)
{
	Initialize();
}

void HUD::StaticInit()
{
	sInstance.reset(new HUD());
	GraphicsWindow *win = window->get_graphics_window();
	float ratio = win->get_sbs_left_x_size() / win->get_sbs_left_y_size();
	float x0 = -ratio * 1.1;
	sInstance->mBandwidthOrigin = LPoint3f(x0, 0.0, 1.0 * 0.9);
	sInstance->mRoundTripTimeOrigin = LPoint3f(x0, 0.0, 1.0 * 0.8);
	sInstance->mScoreBoardOrigin = LPoint3f(x0, 0.0, 1.0 * 0.7);
	sInstance->mScoreOffset = LVector3f(0.0, 0.0, -0.1);
	sInstance->mHealthOffset = LVector3f(ratio * 0.9, 0.0, 1.0 * 0.9);
	sInstance->m_font = FontPool::load_font("Carlito-Regular.ttf");
}

void HUD::Render()
{
	Initialize();
	RenderBandWidth();
	RenderRoundTripTime();
	RenderScoreBoard();
	RenderHealth();
}

void HUD::RenderHealth()
{
	if (mHealth > 0)
	{
		string healthString = StringUtils::Sprintf("Health %d", mHealth);
		RenderText(healthString, mHealthOffset, Colors::Red);
	}
}

void HUD::RenderBandWidth()
{
	string bandwidth =
			StringUtils::Sprintf("In %d  Bps, Out %d Bps",
					static_cast<int>(GameNetworkManagerClient::get_global_ptr()->get_bytes_received_per_second().get_value()),
					static_cast<int>(GameNetworkManagerClient::get_global_ptr()->get_bytes_sent_per_second().get_value()));
	RenderText(bandwidth, mBandwidthOrigin, Colors::White);
}

void HUD::RenderRoundTripTime()
{
	float rttMS =
			GameNetworkManagerClient::get_global_ptr()->get_avg_round_trip_time().get_value()
					* 1000.f;

	string roundTripTime = StringUtils::Sprintf("RTT %d ms", (int) rttMS);
	RenderText(roundTripTime, mRoundTripTimeOrigin, Colors::White);
}

void HUD::RenderScoreBoard()
{
	const vector<ScoreBoardManager::Entry>& entries =
			ScoreBoardManager::sInstance->GetEntries();
	LPoint3f offset = mScoreBoardOrigin;

	for (const auto& entry : entries)
	{
		RenderText(entry.GetFormattedNameScore(), offset, entry.GetColor());
		offset[0] += mScoreOffset[0];
		offset[2] += mScoreOffset[2];
	}

}

void HUD::RenderText(const string& inStr, const LPoint3f& origin,
		const LVecBase4f& inColor)
{
	//dynamically allocate TextNodes if necessary
	if (m_textNodeIdx >= m_textNodesSize)
	{
		//allocate a new TextNode
		string textNum =
				static_cast<ostringstream&>(ostringstream().operator <<(
						m_textNodeIdx)).str();
		m_textNodes.emplace_back(new TextNode("TextNode-" + textNum));
		//common TextNodes setup
		m_textNodes.back().reparent_to(window->get_aspect_2d());
		m_textNodes.back().set_scale(m_textScale);
		m_textNodes.back().set_bin("fixed", 50);
		m_textNodes.back().set_depth_write(false);
		m_textNodes.back().set_depth_test(false);
		m_textNodes.back().set_billboard_point_eye();
		DCAST(TextNode, m_textNodes.back().node())->set_font(m_font);
		//update number of TextNodes
		m_textNodesSize = m_textNodes.size();
	}
	//setup current TextNode
	DCAST(TextNode, m_textNodes[m_textNodeIdx].node())->set_text(inStr);
	m_textNodes[m_textNodeIdx].set_pos(origin);
	m_textNodes[m_textNodeIdx].set_color(inColor);
	//increase index
	++m_textNodeIdx;
}

HUD::~HUD()
{
	m_textNodeIdx = 0;
	m_prevTextNodeIdx = -1;
	m_textNodesSize = 0;
	vector<NodePath>::iterator iter;
	for (iter = m_textNodes.begin(); iter != m_textNodes.end(); ++iter)
	{
		m_textNodes[iter - m_textNodes.begin()].remove_node();
	}
	m_textNodes.clear();
}

void HUD::Initialize()
{
	//clear text on not used text nodes
	for (int idx = m_textNodeIdx; idx <= m_prevTextNodeIdx; ++idx)
	{
		DCAST(TextNode, m_textNodes[idx].node())->clear_text();
	}
	//reset current and previous TextNode indexes
	m_prevTextNodeIdx = m_textNodeIdx - 1;
	m_textNodeIdx = 0;
}
