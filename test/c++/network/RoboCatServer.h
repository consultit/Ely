#ifndef RoboCatServer_h
#define RoboCatServer_h

#include "../../../ely/network/source/gameNetworkManagerServer.h"
#include "RoboCat.h"

enum ECatControlType
{
	ESCT_Human, ESCT_AI
};

class RoboCatServer: public RoboCat
{
public:
	static PT(NetworkObject) StaticCreate()
	{
		NetworkObject* go = new RoboCatServer();
		go->get_owner_object().reparent_to(render);
		return GameNetworkManagerServer::get_global_ptr()->register_and_return(go);
	}
	static void handle_dying_clbk(PT(NetworkObject) THIS);

	static void update_clbk(PT(NetworkObject) THIS);

	void SetCatControlType(ECatControlType inCatControlType)
	{
		mCatControlType = inCatControlType;
	}

	void TakeDamage(int inDamagingPlayerId);

protected:
	RoboCatServer();

private:

	void HandleShooting();

	ECatControlType mCatControlType;

	float mTimeOfNextShot;
	float mTimeBetweenShots;

TYPED_OBJECT_API_DECL(RoboCatServer, RoboCat)
};

#endif //RoboCatServer_h
