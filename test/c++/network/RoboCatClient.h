#ifndef RoboCatClient_h
#define RoboCatClient_h

#include "RoboCat.h"

class RoboCatClient: public RoboCat
{
public:
	static PT(NetworkObject) StaticCreate()
	{
		NetworkObject* go = new RoboCatClient();
		go->get_owner_object().reparent_to(render);
		return PT(NetworkObject)(go);
	}

	static void update_clbk(PT(NetworkObject) THIS);
	static void handle_dying_clbk(PT(NetworkObject) THIS);

	static void pre_read_clbk(PT(NetworkObject) THIS);
	static void post_read_clbk(PT(NetworkObject) THIS, const BitMask32& readState);

	void DoClientSidePredictionAfterReplicationForLocalCat(
			uint32_t inReadState);
	void DoClientSidePredictionAfterReplicationForRemoteCat(
			uint32_t inReadState);

protected:
	RoboCatClient();

private:

	void InterpolateClientSidePrediction(const LVecBase3f& inOldRotation,
			const LPoint3f& inOldLocation, const LVector3f& inOldVelocity,
			bool inIsForRemoteCat);

	float mTimeLocationBecameOutOfSync;
	float mTimeVelocityBecameOutOfSync;

	LVecBase3f mOldRotation;
	LPoint3f mOldLocation;
	LVector3f mOldVelocity;

TYPED_OBJECT_API_DECL(RoboCatClient, RoboCat)
};

#endif //RoboCatClient_h
