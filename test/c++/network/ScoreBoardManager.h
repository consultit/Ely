#ifndef ScoreBoardManager_h
#define ScoreBoardManager_h

#include <lvecBase4.h>
#include <memoryBitStream.h>

using std::unique_ptr;
using std::vector;

class ScoreBoardManager
{
public:

	static void StaticInit();
	static unique_ptr<ScoreBoardManager> sInstance;

	class Entry
	{
	public:
		Entry() :
				mScore(0), mPlayerId(0)
		{
		}

		Entry(uint32_t inPlayerID, const string& inPlayerName,
				const LVecBase4f& inColor);

		const LVecBase4f& GetColor() const
		{
			return mColor;
		}
		uint32_t GetPlayerId() const
		{
			return mPlayerId;
		}
		const string& GetPlayerName() const
		{
			return mPlayerName;
		}
		const string& GetFormattedNameScore() const
		{
			return mFormattedNameScore;
		}
		int GetScore() const
		{
			return mScore;
		}

		void SetScore(int inScore);

		bool Write(OutputMemoryBitStream& inOutputStream) const;
		bool Read(InputMemoryBitStream& inInputStream);

	private:
		LVecBase4f mColor;

		uint32_t mPlayerId;
		string mPlayerName;

		int mScore;

		string mFormattedNameScore;
	};

	Entry* GetEntry(uint32_t inPlayerId);
	bool RemoveEntry(uint32_t inPlayerId);
	void AddEntry(uint32_t inPlayerId, const string& inPlayerName);
	void IncScore(uint32_t inPlayerId, int inAmount);

	bool Write(OutputMemoryBitStream& inOutputStream) const;
	bool Read(InputMemoryBitStream& inInputStream);

	const vector<Entry>& GetEntries() const
	{
		return mEntries;
	}

private:

	ScoreBoardManager();

	vector<Entry> mEntries;

	vector<LVecBase4f> mDefaultColors;

};

#endif //ScoreBoardManager_h
