/**
 * \file Panda.cpp
 *
 * \date 2018-06-09
 * \author consultit
 */

#include "Panda.h"

#include <gameControlManager.h>
#include <support/NetworkMath.h>
#include <utilities.h>

Panda::Panda() :
		GameObject(get_class_id_int("PAND")), mPlayerId(0)
{
	set_owner_object(Utilities::load_model("panda"));
	set_scale(0.25);
	LVecBase3f modelDims;
	LVector3f modelDeltaCenter;
	Utilities::get_bounding_dimensions(get_owner_object(), modelDims, modelDeltaCenter);
	set_collision_radius(modelDims.get_y() / 2.0);
	set_rotation(LVecBase3f::zero());
	set_color(Colors::White);
	driver.clear();
	// add r/w members
	DataVariables& members = get_members();
	members.add_variable_int8_t("mPlayerId", &this->mPlayerId);
	members.add_variable_float("driverVelX", &this->driverVelX);
	members.add_variable_float("driverVelY", &this->driverVelY);
	members.add_variable_float("driverVelH", &this->driverVelH);
	// set r/w members' dirty states
	members.set_dirty_state("posX", under_type(EReplicationState::Pose));
	members.set_dirty_state("posY", under_type(EReplicationState::Pose));
	members.set_dirty_state("posZ", 0);
	members.set_dirty_state("rotH", under_type(EReplicationState::Pose));
	members.set_dirty_state("rotP", 0);
	members.set_dirty_state("rotR", 0);
	members.set_dirty_state("color", under_type(EReplicationState::Color));
	members.set_dirty_state("mPlayerId", under_type(EReplicationState::PlayerId));
	members.set_dirty_state("driverVelX", under_type(EReplicationState::Pose));
	members.set_dirty_state("driverVelY", under_type(EReplicationState::Pose));
	members.set_dirty_state("driverVelH", under_type(EReplicationState::Pose));
	// callbacks
//	set_update_callback (clbk);
	set_all_state_mask_callback(&Panda::get_all_state_mask_clbk);
//	set_handle_dying_callback(clbk);
	set_pre_write_callback(&Panda::pre_write_clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
	set_post_read_callback(&Panda::post_read_clbk);
}

void Panda::pre_write_clbk(PT(NetworkObject) THIS, const BitMask32& writeState)
{
	// owner object -> r/w members synchronization
	PT(Panda) downTHIS = DCAST(Panda, THIS);
	if (writeState.get_word() & under_type(EReplicationState::Pose))
	{
		LVector3f vel = downTHIS->driver->get_linear_speed();
		downTHIS->driverVelX = vel.get_x();
		downTHIS->driverVelY = vel.get_y();
		downTHIS->SetPosVar(downTHIS->get_location());
		downTHIS->driverVelH = downTHIS->driver->get_angular_speeds()[0];
		downTHIS->SetRotVar(downTHIS->get_rotation());
	}
	if (writeState.get_word() & under_type(EReplicationState::Color))
	{
		downTHIS->SetColVar(downTHIS->get_color());
	}
}

void Panda::post_read_clbk(PT(NetworkObject)THIS, const BitMask32& readState)
{
	// r/w members -> owner object synchronization
	PT(Panda) downTHIS = DCAST(Panda, THIS);
	if (readState.get_word() & under_type(EReplicationState::Pose))
	{
		downTHIS->driver->set_linear_speed(LVector3f(downTHIS->driverVelX,
				downTHIS->driverVelY, 0.0));
		downTHIS->set_location(downTHIS->GetPosVar());
		downTHIS->driver->set_angular_speeds({downTHIS->driverVelH, 0.f});
		downTHIS->set_rotation(downTHIS->GetRotVar());
	}
	if (readState.get_word() & under_type(EReplicationState::Color))
	{
		downTHIS->set_color(downTHIS->GetColVar());
	}
}

void Panda::ProcessInput(float inDeltaTime, InputState& inInputState)
{
	//process our input....

	RoboInputState* state = RoboInputState::get_global_ptr();
	driver->set_move_forward(state->get_move_forward(inInputState));
	driver->set_move_backward(state->get_move_backward(inInputState));
	driver->set_rotate_head_left(state->get_rotate_head_left(inInputState));
	driver->set_rotate_head_right(state->get_rotate_head_right(inInputState));
}

void Panda::SimulateMovement(float inDeltaTime)
{
	//simulate us...
	driver->update(inDeltaTime);
}

/*fixme left only for reference
uint32_t Panda::write(OutputMemoryBitStream& inOutputStream,
		uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & under_type(EReplicationState::PlayerId))
	{
		inOutputStream.write((bool) true);
		inOutputStream.write(GetPlayerId());

		writtenState |= under_type(EReplicationState::PlayerId);
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	if (inDirtyState & under_type(EReplicationState::Pose))
	{
		inOutputStream.write((bool) true);

		LVector3f velocity = driver->get_linear_speed();
		inOutputStream.write(velocity.get_x());
		inOutputStream.write(velocity.get_y());

		LPoint3f location = get_location();
		inOutputStream.write(location.get_x());
		inOutputStream.write(location.get_y());

		inOutputStream.write(driver->get_angular_speeds()[0]);

		inOutputStream.write(get_rotation());

		writtenState |= under_type(EReplicationState::Pose);
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	if (inDirtyState & under_type(EReplicationState::Color))
	{
		inOutputStream.write((bool) true);
		inOutputStream.write(get_color());

		writtenState |= under_type(EReplicationState::Color);
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	return writtenState;
}
*/

// set parameters as strings before drivers/chasers creation
void Panda::setDriverParametersBeforeCreation()
{
	GameControlManager* ctrlMgr = GameControlManager::get_global_ptr();
	// set driver's parameters
	ctrlMgr->set_parameter_value(GameControlManager::DRIVER,
			"max_angular_speed", "100.0");
	ctrlMgr->set_parameter_value(GameControlManager::DRIVER, "angular_accel",
			"50.0");
	ctrlMgr->set_parameter_value(GameControlManager::DRIVER, "max_linear_speed",
			"8.0");
	ctrlMgr->set_parameter_value(GameControlManager::DRIVER, "linear_accel",
			"1.0");
	ctrlMgr->set_parameter_value(GameControlManager::DRIVER, "linear_friction",
			"0.5");
	ctrlMgr->set_parameter_value(GameControlManager::DRIVER, "angular_friction",
			"5.0");
}

NodePath Panda::setupDriver()
{
	// setup the driver
	setDriverParametersBeforeCreation();
	// create the driver (attached to the reference node)
	driver =
			DCAST(P3Driver,
					GameControlManager::get_global_ptr()->create_driver(
							"PandaDriver").node());
	return NodePath::any_path(driver);
}

TYPED_OBJECT_API_DEF(Panda)
