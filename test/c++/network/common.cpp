/**
 * \file common.cpp
 *
 * \date 2018-08-04
 * \author consultit
 */

#include "common.h"
#include <dataVariables.h>
#include <iostream>
#include <pandaFramework.h>
#include <texturePool.h>
#include <loaderFileTypeRegistry.h>
#include <geomTristrips.h>

GameObject::GameObject(uint32_t inCode) :
		NetworkObject(inCode), mCollisionRadius(0.5f)
{
	// add r/w members
	DataVariables& members = get_members();
	members.add_variable_float("posX", &this->posX);
	members.add_variable_float("posY", &this->posY);
	members.add_variable_float("posZ", &this->posZ);
	members.add_variable_float("rotH", &this->rotH);
	members.add_variable_float("rotP", &this->rotP);
	members.add_variable_float("rotR", &this->rotR);
	members.add_variable_lvecbase4f("color", &this->color);
	// set r/w members' dirty states
	// callbacks
//	set_update_callback(clbk);
//	set_all_state_mask_callback (clbk);
//	set_handle_dying_callback(clbk);
//	set_pre_write_callback(clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
//	set_post_read_callback(clbk);
#ifdef ELY_DEBUG
	textNode = new TextNode("TextNode");
	text = render.attach_new_node(textNode);
	//text.set_p(-90.0);
	text.set_color(0.0, 0.0, 1.0, 1.0);// blue
	text.set_scale(0.1);
	text.set_two_sided(true);
	text.set_billboard_point_eye();
#endif //ELY_DEBUG
}

#ifdef ELY_DEBUG
GameObject::~GameObject()
{
	text.remove_node();
}

void GameObject::update_clbk(PT(NetworkObject) THIS)
{
	PT(GameObject) downTHIS = DCAST(GameObject, THIS);
	LPoint3f loc = downTHIS->get_location();
	downTHIS->text.set_pos(loc.get_x(), loc.get_y(), 4.5 * downTHIS->get_collision_radius());
	string locStr = "(" + to_string(loc.get_x()) + "," + to_string(loc.get_y()) + ")";
	downTHIS->textNode->set_text(locStr);
}
#endif //ELY_DEBUG

void GameObject::set_rotation(const LVecBase3f& inRotation)
{
	//should we normalize using fmodf?
	NodePath owner_object = get_owner_object();
	owner_object.set_hpr(-inRotation.get_x(), inRotation.get_y(),
			inRotation.get_z());
}

LVecBase3f GameObject::get_rotation() const
{
	NodePath owner_object = get_owner_object();
	return LVecBase3f(-owner_object.get_h(), owner_object.get_p(),
			owner_object.get_r());
}

void GameObject::set_location(const LPoint3f& inLocation)
{
	NodePath owner_object = get_owner_object();
	owner_object.set_pos(inLocation);
}

LPoint3f GameObject::get_location() const
{
	NodePath owner_object = get_owner_object();
	return owner_object.get_pos();
}

void GameObject::set_scale(const LVecBase3f& inScale)
{
	NodePath owner_object = get_owner_object();
	owner_object.set_scale(inScale);
}

LVecBase3f GameObject::get_scale() const
{
	NodePath owner_object = get_owner_object();
	return owner_object.get_scale();
}

void GameObject::set_color(const LVecBase4f& inColor)
{
	NodePath owner_object = get_owner_object();
	owner_object.set_color(inColor);
}

LVecBase4f GameObject::get_color() const
{
	NodePath owner_object = get_owner_object();
	return owner_object.get_color();
}

void GameObject::set_collision_radius(float inRadius)
{
	mCollisionRadius = inRadius;
}

float GameObject::get_collision_radius() const
{
	return mCollisionRadius;
}

void GameObject::SetPosVar(const LPoint3f& pos)
{
	posX = pos.get_x();
	posY = pos.get_y();
	posZ = pos.get_z();
}

LPoint3f GameObject::GetPosVar() const
{
	return LPoint3f(static_cast<float&>(const_cast<Float&>(posX)),
			static_cast<float&>(const_cast<Float&>(posY)),
			static_cast<float&>(const_cast<Float&>(posZ)));
}

void GameObject::SetRotVar(const LVecBase3f& rot)
{
	rotH = rot.get_x();
	rotP = rot.get_y();
	rotR = rot.get_z();
}

LVecBase3f GameObject::GetRotVar() const
{
	return LVecBase3f(static_cast<float&>(const_cast<Float&>(rotH)),
			static_cast<float&>(const_cast<Float&>(rotP)),
			static_cast<float&>(const_cast<Float&>(rotR)));
}

void GameObject::SetColVar(const LVecBase4f& col)
{
	color = col;
}

LVecBase4f GameObject::GetColVar() const
{
	return color;
}

TYPED_OBJECT_API_DEF(GameObject)
