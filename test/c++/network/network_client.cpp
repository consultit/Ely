/**
 * \file network_client.cpp
 *
 * \date 2018-05-25
 * \author consultit
 */

#include "HUD.h"
#include "RoboCatClient.h"
#include "MouseClient.h"
#include "ScoreBoardManager.h"
#include "YarnClient.h"
#include "PandaClient.h"
#include "RoboInputState.h"
#include <networkWorld.h>
#include <networkObjectRegistry.h>
#include <inputManager.h>
#include <socketAddress.h>
#include <socketUtil.h>
#include <support/NetworkMath.h>
#include <support/StringUtils.h>
#include <load_prc_file.h>
#include <buttonThrower.h>
#include <pandaFramework.h>
#include <gameControlManager.h>
#include <p3Driver.h>
#include <p3Chaser.h>
#include <lineSegs.h>

#include "../../../ely/network/source/gameNetworkManagerClient.h"

using std::unique_ptr;
using std::string;

PandaFramework framework;
WindowFramework *window = nullptr;
NodePath render;
extern string dataDir;

/// static global declarations/definitions
namespace
{
AsyncTask* doFrameTask = nullptr;
int doFrameSort = 10;
string address = "127.0.0.1:45000";
string player = "player";
string msg = "Network Application - Client";
bool windowOn = false;
vector<string> dirs;
float timeBetweenInputs = 0.03;
float delayBeforeAck = 0.5;
float timeBetweenHellos = 1.0;
float timeBetweenInputPackets = 0.033;

void parseArguments(int, char**);
void loadPandaFramework(int, char**, const string&);
void setupPandaFramework();
void loadAndSetupNetworkFramework();
void loadAndSetupGameFramework();
void atExitCleanup();
void handleEventClient(const Event*, void*);
AsyncTask::DoneStatus doFrameClient(GenericAsyncTask*, void*);
void libp3toolsTypedObjectInit();
//callbacks
void HandleScoreBoardState(InputMemoryBitStream&);
void drawField(NodePath render);
}

int main(int argc, char** argv)
{
	///INTIALIZATION
	parseArguments(argc, argv);
	msg = "ClientMain - address: '" + address + "' - name: '" + player + "'";
	///SETUP
	loadPandaFramework(argc, argv, msg);
	setupPandaFramework();
	loadAndSetupNetworkFramework();
	loadAndSetupGameFramework();
	///RUN
	{
		// do the main loop, equals to call app.run() in python
		framework.main_loop();
	}
	///ATEXIT
	atExitCleanup();
	//
	return (0);
}

///SOURCES

namespace
{
void parseArguments(int argc, char** argv)
{
	int c;
	while ((c = getopt(argc, argv, "a:u:wd:i:k:l:p:")) != -1)
	{
		switch (c)
		{
		case 'a':
		{
			address = optarg;
		}
			break;
		case 'u':
		{
			player = optarg;
		}
			break;
		case 'w':
		{
			windowOn = true;
		}
			break;
		case 'd':
		{
			dirs.emplace_back(optarg);
		}
			break;
		case 'i':
		{
			timeBetweenInputs = atof(optarg);
		}
			break;
		case 'k':
		{
			delayBeforeAck = atof(optarg);
		}
			break;
		case 'l':
		{
			timeBetweenHellos = atof(optarg);
		}
			break;
		case 'p':
		{
			timeBetweenInputPackets = atof(optarg);
		}
			break;
		case '?':
			if (optopt == 'a' || optopt == 'u' || optopt == 'd' || optopt == 'i'
					|| optopt == 'k' || optopt == 'l' || optopt == 'p')
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			else if (isprint(optopt))
				fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
			break;
		default:
			abort();
		}
	}
	for (c = optind; c < argc; c++)
	{
		fprintf(stderr, "Unused argument `%s'.\n", argv[c]);
	}
}

void loadPandaFramework(int argc, char** argv, const string& msg)
{
	// Load your application's configuration
	load_prc_file_data("", "model-path " + dataDir);
	for (auto dir : dirs)
	{
		load_prc_file_data("", "model-path " + dir);
	}
	if (windowOn)
	{
		load_prc_file_data("", "win-size 1024 768");
		load_prc_file_data("", "show-frame-rate-meter #t");
		load_prc_file_data("", "sync-video #t");
	}
	else
	{
		load_prc_file_data("", "window-type	none");
	}
	// Setup your application
	framework.open_framework(argc, argv);
	if (windowOn)
	{
		framework.set_window_title(msg);
		window = framework.open_window();
		if (window != (WindowFramework *) nullptr)
		{
			cout << "Opened the window successfully!\n";
			window->enable_keyboard();
			window->setup_trackball();
		}
		render = window->get_render();
		// place camera trackball (local coordinate)
		PT(Trackball)trackball = DCAST(Trackball, window->get_mouse().find("**/+Trackball").node());
		trackball->set_pos(0.0, 60.0, -5.0);
		trackball->set_hpr(0.0, 20.0, 0.0);
	}
	else
	{
		render = NodePath("render");
		cout << msg << endl;
	}
}

static map<string, string> keyDefs
{
	pair<string, string>("forward", "w"), pair<string, string>("forwardStop", "w-up"),
	pair<string, string>("backward", "s"), pair<string, string>("backwardStop", "s-up"),
	pair<string, string>("left", "a"), pair<string,	string>("leftStop", "a-up"),
	pair<string, string>("right", "d"), pair<string, string>("rightStop", "d-up"),
	pair<string, string>("shoot", "k"), pair<string, string>("shootStop", "k-up"),
	// P3Driver
	pair<string, string>("drv_forward", "arrow_up"), pair<string, string>("drv_forwardStop", "arrow_up-up"),
	pair<string, string>("drv_backward", "arrow_down"), pair<string, string>("drv_backwardStop", "arrow_down-up"),
	pair<string, string>("drv_left", "arrow_left"), pair<string,	string>("drv_leftStop", "arrow_left-up"),
	pair<string, string>("drv_right", "arrow_right"), pair<string, string>("drv_rightStop", "arrow_right-up"),
#ifdef ELY_DEBUG
	pair<string, string>("latencyInc", "t"), pair<string, string>("latencyDec", "shift-t"),
	pair<string, string>("dropPacketInc", "y"), pair<string, string>("dropPacketDec", "shift-y"),
	pair<string, string>("jitterInc", "u"), pair<string, string>("jitterDec", "shift-u"),
#endif //ELY_DEBUG
};

void setupPandaFramework()
{
	/// typed object init; not needed if you build inside panda source tree
	P3Driver::init_type();
	P3Chaser::init_type();
	GameControlManager::init_type();
	P3Driver::register_with_read_factory();
	P3Chaser::register_with_read_factory();
	libp3toolsTypedObjectInit();
	// network
	ValueList_ClientProxyPtr::init_type();
	ValueList_NetworkObjectPtr::init_type();
	ValueList_TCPSocketPtr::init_type();
	NetworkObject::init_type();
	GameNetworkManager::init_type();
	GameNetworkManagerClient::init_type();
	NetworkWorld::init_type();
	InputManager::init_type();
	NetworkObjectRegistry::init_type();
	SocketAddress::init_type();
	TransmissionData::init_type();
	UDPSocket::init_type();
	TCPSocket::init_type();
	NetBitMask::init_type();
	GameObject::init_type();
	Mouse::init_type();
	MouseClient::init_type();
	Panda::init_type();
	PandaClient::init_type();
	RoboCat::init_type();
	RoboCatClient::init_type();
	Yarn::init_type();
	YarnClient::init_type();

	// create a control manager
	GameControlManager* controlMgr = new GameControlManager(
			GameControlManager::Output(window->get_graphics_window()),
			5, render, BitMask32::all_on());
	// reparent the reference node to render
	controlMgr->get_reference_node_path().reparent_to(render);
/* fixme manual update of driver
	// start the default update task for all drivers
	controlMgr->start_default_update();
*/

	// add event handling
	for (auto keyDef : keyDefs)
	{
		framework.define_key(keyDef.second, keyDef.second + "action",
				&handleEventClient, nullptr);
	}
	// create the task for DoFrameClient
	doFrameTask = new GenericAsyncTask(string("doFrameClient"), &doFrameClient,
			nullptr);
	doFrameTask->set_sort(doFrameSort);
	//Adds mDoFrameTask to the active queue.
	AsyncTaskManager::get_global_ptr()->add(doFrameTask);
	// draw field
	drawField(render);
}

void loadAndSetupNetworkFramework()
{
	new Timing();
	SocketUtil::static_init();
	srand(static_cast<uint32_t>(time(nullptr)));
	PT(SocketAddress) serverAddress =
			SocketAddressFactory::create_ipv4_from_string(address);
	(new GameNetworkManagerClient())->init(*serverAddress, player);
	new NetworkWorld();
	// manage custom InputState
	new InputStateFactory();
	new RoboInputState(*InputStateFactory::get_global_ptr()->get_input_state());
	new InputManager();
	InputManager::get_global_ptr()->set_time_between_input_samples(timeBetweenInputs);
	new NetworkObjectRegistry();
}

void loadAndSetupGameFramework()
{
	ScoreBoardManager::StaticInit();
	HUD::StaticInit();
	//
	NetworkObjectRegistry::get_global_ptr()->register_creation_function(NetworkObject::get_class_id_int("PAND"),
			PandaClient::StaticCreate);
	NetworkObjectRegistry::get_global_ptr()->register_creation_function(NetworkObject::get_class_id_int("RCAT"),
			RoboCatClient::StaticCreate);
	NetworkObjectRegistry::get_global_ptr()->register_creation_function(NetworkObject::get_class_id_int("MOUS"),
			MouseClient::StaticCreate);
	NetworkObjectRegistry::get_global_ptr()->register_creation_function(NetworkObject::get_class_id_int("YARN"),
			YarnClient::StaticCreate);
	//
	GameNetworkManagerClient::get_global_ptr()->set_handle_state_packet_callback(&HandleScoreBoardState);
	GameNetworkManagerClient::get_global_ptr()->set_delay_before_ack_timeout(delayBeforeAck);
	GameNetworkManagerClient::get_global_ptr()->set_time_between_hellos(timeBetweenHellos);
	GameNetworkManagerClient::get_global_ptr()->set_time_between_input_packets(timeBetweenInputPackets);
}

void atExitCleanup()
{
	if (doFrameTask)
	{
		AsyncTaskManager::get_global_ptr()->remove(doFrameTask);
	}

	delete NetworkObjectRegistry::get_global_ptr();
	delete InputManager::get_global_ptr();
	delete RoboInputState::get_global_ptr();
	delete InputStateFactory::get_global_ptr();
	delete NetworkWorld::get_global_ptr();
	delete GameNetworkManagerClient::get_global_ptr();
	SocketUtil::clean_up();
	delete Timing::get_global_ptr();
}

///
inline bool UpdateDesireVariableFromKey(bool down)
{
	bool ioVariable;
	if (down)
	{
		ioVariable = true;
	}
	else
	{
		ioVariable = false;
	}
	return ioVariable;
}

inline float UpdateDesireFloatFromKey(bool down)
{
	float ioVariable;
	if (down)
	{
		ioVariable = 1.f;
	}
	else
	{
		ioVariable = 0.f;
	}
	return ioVariable;
}

void handleEventClient(const Event* inEvent, void*)
{
	string name = inEvent->get_name();
	RoboInputState* state = RoboInputState::get_global_ptr();
	InputState& inInputState = *InputManager::get_global_ptr()->get_input_state();
	if ((name == keyDefs["forward"]) || (name == keyDefs["forwardStop"]))
	{
		state->SetDesiredForwardAmount(inInputState,
				UpdateDesireFloatFromKey(name == keyDefs["forward"]));
	}
	if ((name == keyDefs["backward"]) || (name == keyDefs["backwardStop"]))
	{
		state->SetDesiredBackAmount(inInputState,
				UpdateDesireFloatFromKey(name == keyDefs["backward"]));
	}
	if ((name == keyDefs["left"]) || (name == keyDefs["leftStop"]))
	{
		state->SetDesiredLeftAmount(inInputState,
				UpdateDesireFloatFromKey(name == keyDefs["left"]));
	}
	if ((name == keyDefs["right"]) || (name == keyDefs["rightStop"]))
	{
		state->SetDesiredRightAmount(inInputState,
				UpdateDesireFloatFromKey(name == keyDefs["right"]));
	}
	if ((name == keyDefs["shoot"]) || (name == keyDefs["shootStop"]))
	{
		state->SetIsShooting(inInputState,
				UpdateDesireVariableFromKey(name == keyDefs["shoot"]));
	}
	// P3Driver
	if ((name == keyDefs["drv_forward"]) || (name == keyDefs["drv_forwardStop"]))
	{
		state->set_move_forward(inInputState, name == keyDefs["drv_forward"] ? true : false);
	}
	if ((name == keyDefs["drv_backward"]) || (name == keyDefs["drv_backwardStop"]))
	{
		state->set_move_backward(inInputState, name == keyDefs["drv_backward"] ? true : false);
	}
	if ((name == keyDefs["drv_left"]) || (name == keyDefs["drv_leftStop"]))
	{
		state->set_rotate_head_left(inInputState, name == keyDefs["drv_left"] ? true : false);
	}
	if ((name == keyDefs["drv_right"]) || (name == keyDefs["drv_rightStop"]))
	{
		state->set_rotate_head_right(inInputState, name == keyDefs["drv_right"] ? true : false);
	}
#ifdef ELY_DEBUG
	else
	{
		// latency, packet drop, jitter simulation
		float latency =
				GameNetworkManagerClient::get_global_ptr()->get_simulated_latency();
		float chance =
				GameNetworkManagerClient::get_global_ptr()->get_drop_packet_chance();
		float jitter =
				GameNetworkManagerClient::get_global_ptr()->get_simulated_max_jitter();
		if ((name == keyDefs["latencyInc"]) || (name == keyDefs["latencyDec"]))
		{
			if (name == keyDefs["latencyInc"])
			{
				latency += 0.1f;
				if (latency > 0.5f)
				{
					latency = 0.5f;
				}
			}
			else
			{
				latency -= 0.1f;
				if (latency < 0.0f)
				{
					latency = 0.0f;
				}
			}
			GameNetworkManagerClient::get_global_ptr()->set_simulated_latency(latency);
			LOG("Simulated Latency: %f", latency)
		}
		else if ((name == keyDefs["dropPacketInc"]) || (name == keyDefs["dropPacketDec"]))
		{
			if (name == keyDefs["dropPacketInc"])
			{
				chance += 0.05f;
				if (chance > 1.0f)
				{
					chance = 1.0f;
				}
			}
			else
			{
				chance -= 0.05f;
				if (chance < 0.0f)
				{
					chance = 0.0f;
				}
			}
			GameNetworkManagerClient::get_global_ptr()->set_drop_packet_chance(chance);
			LOG("Drop Packet Chance: %f", chance)
		}
		else if ((name == keyDefs["jitterInc"]) || (name == keyDefs["jitterDec"]))
		{
			if (name == keyDefs["jitterInc"])
			{
				jitter += 0.1f;
				if (jitter > 0.5f)
				{
					jitter = 0.5f;
				}
			}
			else
			{
				jitter -= 0.1f;
				if (jitter < 0.0f)
				{
					jitter = 0.0f;
				}
			}
			GameNetworkManagerClient::get_global_ptr()->set_simulated_max_jitter(jitter);
			LOG("Simulated Max Jitter: %f", jitter)
		}
	}
#endif //ELY_DEBUG
}

AsyncTask::DoneStatus doFrameClient(GenericAsyncTask* task, void*)
{
	Timing::get_global_ptr()->update();
	InputManager::get_global_ptr()->update();
	NetworkWorld::get_global_ptr()->update();
	// the pending move has been processed by all objects: clear
	InputManager::get_global_ptr()->clear_pending_move();
	GameNetworkManagerClient::get_global_ptr()->process_incoming_packets();
	HUD::sInstance->Render();
	GameNetworkManagerClient::get_global_ptr()->send_outgoing_packets();
	return AsyncTask::DS_cont;
}

// libp3tools typed objects init
void libp3toolsTypedObjectInit()
{
	Pair_bool_float::init_type();
	Pair_LPoint3f_int::init_type();
	Pair_LVector3f_ValueList_float::init_type();
	ValueList_string::init_type();
	ValueList_NodePath::init_type();
	ValueList_int::init_type();
	ValueList_float::init_type();
	ValueList_LVecBase3f::init_type();
	ValueList_LVector3f::init_type();
	ValueList_LPoint3f::init_type();
	ValueList_Pair_LPoint3f_int::init_type();
}

void HandleScoreBoardState(InputMemoryBitStream& inInputStream)
{
	ScoreBoardManager::sInstance->Read(inInputStream);
}

void drawField(NodePath render)
{
	LineSegs field("field");
	field.set_color(1.0, 1.0, 0.0, 1.0); //yellow
	field.set_thickness(1.0);
	LPoint3f
	A(-HALF_WORLD_WIDTH, -HALF_WORLD_HEIGHT, 0.0),
	B(HALF_WORLD_WIDTH,	-HALF_WORLD_HEIGHT, 0.0),
	C(HALF_WORLD_WIDTH, HALF_WORLD_HEIGHT, 0.0),
	D(-HALF_WORLD_WIDTH, HALF_WORLD_HEIGHT, 0.0);
	//lower edge
	field.move_to(A);
	field.draw_to(B);
	//right edge
	field.move_to(B);
	field.draw_to(C);
	//upper edge
	field.move_to(C);
	field.draw_to(D);
	//left edge
	field.move_to(D);
	field.draw_to(A);
	// draw grid
	// columns
	for (float x = -HALF_WORLD_WIDTH; x < HALF_WORLD_WIDTH; x += WORLD_STEP)
	{
		if (abs(x) < WORLD_STEP * 0.01)
		{
			field.set_color(0.0, 1.0, 0.0, 1.0); //green
		}
		else if (abs(x - rint(x)) < WORLD_STEP * 0.01)
		{
			field.set_color(0.5, 0.25, 0.0, 1.0); //brown
		}
		else
		{
			field.set_color(1.0, 0.5, 0.0, 1.0); //orange
		}
		field.move_to(x, HALF_WORLD_HEIGHT, 0);
		field.draw_to(x, -HALF_WORLD_HEIGHT, 0);
	}
	// rows
	for (float y = HALF_WORLD_HEIGHT; y > -HALF_WORLD_HEIGHT; y -= WORLD_STEP)
	{
		if (abs(y) < WORLD_STEP * 0.01)
		{
			field.set_color(1.0, 0.0, 0.0, 1.0); //red
		}
		else if (abs(y - rint(y)) < WORLD_STEP * 0.01)
		{
			field.set_color(0.5, 0.25, 0.0, 1.0); //brown
		}
		else
		{
			field.set_color(1.0, 0.5, 0.0, 1.0); //orange
		}
		field.move_to(-HALF_WORLD_WIDTH, y, 0);
		field.draw_to(HALF_WORLD_WIDTH, y, 0);
	}
	//add to render
	render.attach_new_node(field.create());
}
}
