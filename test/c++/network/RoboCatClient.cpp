#include "RoboCatClient.h"

#include "HUD.h"
#include <inputManager.h>
#include <moveList.h>
#include <support/NetworkMath.h>
#include <support/StringUtils.h>
#include "../../../ely/network/source/gameNetworkManagerClient.h"

RoboCatClient::RoboCatClient() : RoboCat(),
		mTimeLocationBecameOutOfSync(0.f), mTimeVelocityBecameOutOfSync(0.f)
{
	// add r/w members
	// set r/w members' dirty states
	// callbacks
	set_update_callback(&RoboCatClient::update_clbk);
//	set_all_state_mask_callback(clbk);
	set_handle_dying_callback(&RoboCatClient::handle_dying_clbk);
//	set_pre_write_callback(clbk);
//	set_post_write_callback(clbk);
	set_pre_read_callback(&RoboCatClient::pre_read_clbk);
	set_post_read_callback(&RoboCatClient::post_read_clbk);
}

void RoboCatClient::handle_dying_clbk(PT(NetworkObject)THIS)
{
	
	PT(RoboCatClient) downTHIS = DCAST(RoboCatClient, THIS);
	//and if we're local, tell the hud so our health goes away!
	if (downTHIS->GetPlayerId() ==
			GameNetworkManagerClient::get_global_ptr()->get_player_id())
	{
		HUD::sInstance->SetPlayerHealth(0);
	}
}

void RoboCatClient::update_clbk(PT(NetworkObject)THIS)
{
	PT(RoboCatClient) downTHIS = DCAST(RoboCatClient, THIS);
	//is this the cat owned by us?
	if (downTHIS->GetPlayerId() ==
			GameNetworkManagerClient::get_global_ptr()->get_player_id())
	{
		Move* pendingMove =
				InputManager::get_global_ptr()->get_pending_move();
		//in theory, only do this if we want to sample input this frame / if there's a new move ( since we have to keep in sync with server )
		if (pendingMove) //is it time to sample a new move...
		{
			float deltaTime = pendingMove->get_delta_time();

			//apply that input

			downTHIS->ProcessInput(deltaTime, pendingMove->get_input_state());

			//and simulate!

			downTHIS->SimulateMovement(deltaTime);

			//LOG( "Client Move Time: %3.4f deltaTime: %3.4f left rot at %3.4f", latestMove.GetTimestamp(), deltaTime, get_rotation() );
		}
	}
	else
	{
		downTHIS->SimulateMovement(Timing::get_global_ptr()->get_delta_time());

		if (Math::Is2DVectorEqual(downTHIS->GetVelocity(), LVector3f::zero()))
		{
			//we're in sync if our velocity is 0
			downTHIS->mTimeLocationBecameOutOfSync = 0.f;
		}
	}
#ifdef ELY_DEBUG
	GameObject::update_clbk(THIS);
#endif //ELY_DEBUG
}

/*fixme left only for reference
void RoboCatClient::read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	uint32_t readState = 0;

	inInputStream.read(stateBit);
	if (stateBit)
	{
		uint32_t playerId;
		inInputStream.read(playerId);
		SetPlayerId(playerId);
		readState |= ECRS_PlayerId;
	}

	LVecBase3f oldRotation = get_rotation();
	LPoint3f oldLocation = get_location();
	LVector3f oldVelocity = GetVelocity();

	LVecBase3f replicatedRotation;
	LPoint3f replicatedLocation(0.f, 0.f, 0.f);
	LVector3f replicatedVelocity(0.f, 0.f, 0.f);

	inInputStream.read(stateBit);
	if (stateBit)
	{
		inInputStream.read(replicatedVelocity[0]);
		inInputStream.read(replicatedVelocity[1]);

		SetVelocity(replicatedVelocity);

		inInputStream.read(replicatedLocation[0]);
		inInputStream.read(replicatedLocation[1]);

		set_location(replicatedLocation);

		inInputStream.read(replicatedRotation);
		set_rotation(replicatedRotation);

		readState |= ECRS_Pose;
	}

	inInputStream.read(stateBit);
	if (stateBit)
	{
		inInputStream.read(stateBit);
		mThrustDir = stateBit ? 1.f : -1.f;
	}
	else
	{
		mThrustDir = 0.f;
	}

	inInputStream.read(stateBit);
	if (stateBit)
	{
		LVecBase4f color;
		inInputStream.read(color);
		set_color(color);
		readState |= ECRS_Color;
	}

	inInputStream.read(stateBit);
	if (stateBit)
	{
		mHealth = 0;
		inInputStream.read(mHealth, 4);
		readState |= ECRS_Health;
	}

	if (GetPlayerId()
			== (uint32_t) GameNetworkManagerClient::get_global_ptr()->get_player_id())
	{
		//did we get health? if so, tell the hud!
		if ((readState & ECRS_Health) != 0)
		{
			HUD::sInstance->SetPlayerHealth(mHealth);
		}

		DoClientSidePredictionAfterReplicationForLocalCat(readState);

		//if this is a create packet, don't interpolate
		if ((readState & ECRS_PlayerId) == 0)
		{
			InterpolateClientSidePrediction(oldRotation, oldLocation,
					oldVelocity, false);
		}
	}
	else
	{
		DoClientSidePredictionAfterReplicationForRemoteCat(readState);

		//will this smooth us out too? it'll interpolate us just 10% of the way there...
		if ((readState & ECRS_PlayerId) == 0)
		{
			InterpolateClientSidePrediction(oldRotation, oldLocation,
					oldVelocity, true);
		}

	}

}
*/

void RoboCatClient::pre_read_clbk(PT(NetworkObject)THIS)
{
	PT(RoboCatClient) downTHIS = DCAST(RoboCatClient, THIS);
	downTHIS->mOldRotation = downTHIS->get_rotation();
	downTHIS->mOldLocation = downTHIS->get_location();
	downTHIS->mOldVelocity = downTHIS->GetVelocity();
}

void RoboCatClient::post_read_clbk(PT(NetworkObject)THIS,
		const BitMask32& readState)
{
	// r/w members -> owner object synchronization
	RoboCat::post_read_clbk(THIS, readState);
	PT(RoboCatClient) downTHIS = DCAST(RoboCatClient, THIS);
	if (downTHIS->GetPlayerId() ==
			GameNetworkManagerClient::get_global_ptr()->get_player_id())
	{
		//did we get health? if so, tell the hud!
		if ((readState & ECRS_Health) != 0)
		{
			HUD::sInstance->SetPlayerHealth(downTHIS->GetHealth());
		}
		downTHIS->DoClientSidePredictionAfterReplicationForLocalCat(
				readState.get_word());
		//if this is a create packet, don't interpolate
		if ((readState & ECRS_PlayerId) == 0)
		{
			downTHIS->InterpolateClientSidePrediction(downTHIS->mOldRotation,
					downTHIS->mOldLocation, downTHIS->mOldVelocity, false);
		}
	}
	else
	{
		downTHIS->DoClientSidePredictionAfterReplicationForRemoteCat(
				readState.get_word());
		//will this smooth us out too? it'll interpolate us just 10% of the way there...
		if ((readState & ECRS_PlayerId) == 0)
		{
			downTHIS->InterpolateClientSidePrediction(downTHIS->mOldRotation,
					downTHIS->mOldLocation, downTHIS->mOldVelocity, true);
		}
	}
}

void RoboCatClient::DoClientSidePredictionAfterReplicationForLocalCat(
		uint32_t inReadState)
{
	if ((inReadState & ECRS_Pose) != 0)
	{
		//simulate pose only if we received new pose- might have just gotten thrustDir
		//in which case we don't need to replay moves because we haven't warped backwards

		//all processed moves have been removed, so all that are left are unprocessed moves
		//so we must apply them...
		MoveList& moveList = *InputManager::get_global_ptr()->get_move_list();

		for (Move& move : moveList)
		{
			float deltaTime = move.get_delta_time();
			ProcessInput(deltaTime, move.get_input_state());

			SimulateMovement(deltaTime);
		}
	}

}

void RoboCatClient::InterpolateClientSidePrediction(
		const LVecBase3f& inOldRotation, const LPoint3f& inOldLocation,
		const LVector3f& inOldVelocity, bool inIsForRemoteCat)
{
	if (inOldRotation != get_rotation() && !inIsForRemoteCat)
	{
		LOG("ERROR! Move replay ended with incorrect rotation!", 0);
	}

	float roundTripTime = GameNetworkManagerClient::get_global_ptr()->get_round_trip_time();

	if (!Math::Is2DVectorEqual(inOldLocation, get_location()))
	{
		//LOG( "ERROR! Move replay ended with incorrect location!", 0 );

		//have we been out of sync, or did we just become out of sync?
		float time = Timing::get_global_ptr()->get_frame_start_time();
		if (mTimeLocationBecameOutOfSync == 0.f)
		{
			mTimeLocationBecameOutOfSync = time;
		}

		float durationOutOfSync = time - mTimeLocationBecameOutOfSync;
		if (durationOutOfSync < roundTripTime)
		{
			set_location(
					Lerp(inOldLocation, get_location(),
							inIsForRemoteCat ?
									(durationOutOfSync / roundTripTime) :
									0.1f));
		}
	}
	else
	{
		//we're in sync
		mTimeLocationBecameOutOfSync = 0.f;
	}

	if (!Math::Is2DVectorEqual(inOldVelocity, GetVelocity()))
	{
		//LOG( "ERROR! Move replay ended with incorrect velocity!", 0 );

		//have we been out of sync, or did we just become out of sync?
		float time = Timing::get_global_ptr()->get_frame_start_time();
		if (mTimeVelocityBecameOutOfSync == 0.f)
		{
			mTimeVelocityBecameOutOfSync = time;
		}

		//now interpolate to the correct value...
		float durationOutOfSync = time - mTimeVelocityBecameOutOfSync;
		if (durationOutOfSync < roundTripTime)
		{
			SetVelocity(
					Lerp(inOldVelocity, GetVelocity(),
							inIsForRemoteCat ?
									(durationOutOfSync / roundTripTime) :
									0.1f));
		}
		//otherwise, fine...

	}
	else
	{
		//we're in sync
		mTimeVelocityBecameOutOfSync = 0.f;
	}

}

//so what do we want to do here? need to do some kind of interpolation...

void RoboCatClient::DoClientSidePredictionAfterReplicationForRemoteCat(
		uint32_t inReadState)
{
	if ((inReadState & ECRS_Pose) != 0)
	{

		//simulate movement for an additional RTT
		float rtt = GameNetworkManagerClient::get_global_ptr()->get_round_trip_time();
		//LOG( "Other cat came in, simulating for an extra %f", rtt );

		//let's break into framerate sized chunks though so that we don't run through walls and do crazy things...
		float deltaTime = 1.f / 30.f;

		while (true)
		{
			if (rtt < deltaTime)
			{
				SimulateMovement(rtt);
				break;
			}
			else
			{
				SimulateMovement(deltaTime);
				rtt -= deltaTime;
			}
		}
	}

}

TYPED_OBJECT_API_DEF(RoboCatClient)
