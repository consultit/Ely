#ifndef Yarn_h
#define Yarn_h

#include "common.h"

class RoboCat;

class Yarn: public GameObject
{
public:

	static constexpr uint32_t YARN = constUInt32('Y', 'A', 'R', 'N');

	enum EYarnReplicationState
	{
		EYRS_Pose = 1 << 0, EYRS_Color = 1 << 1, EYRS_PlayerId = 1 << 2,

		EYRS_AllState = EYRS_Pose | EYRS_Color | EYRS_PlayerId
	};

	static uint32_t get_all_state_mask_clbk(PT(NetworkObject))
	{
		return EYRS_AllState;
	}

	static void update_clbk(PT(NetworkObject) THIS);

	static void pre_write_clbk(PT(NetworkObject) THIS, const BitMask32& writeState);
	static void post_read_clbk(PT(NetworkObject) THIS, const BitMask32& readState);

	void SetVelocity(const LVecBase3f& inVelocity)
	{
		mVelocity = inVelocity;
	}
	LVector3f GetVelocity() const
	{
		return mVelocity;
	}

	void SetPlayerId(int inPlayerId)
	{
		mPlayerId = inPlayerId;
	}
	int GetPlayerId() const
	{
		return static_cast<Int8>(mPlayerId);
	}

	void InitFromShooter(PT(RoboCat) inShooter);

	virtual bool HandleCollisionWithCat(PT(RoboCat) inCat);

protected:
	Yarn();

	LVector3f mVelocity;

	float mMuzzleSpeed;
	Int8 mPlayerId;

TYPED_OBJECT_API_DECL(Yarn, GameObject)
};

#endif //Yarn_h
