#ifndef YarnClient_h
#define YarnClient_h

#include "Yarn.h"

class YarnClient: public Yarn
{
public:
	static PT(NetworkObject) StaticCreate()
	{
		NetworkObject* go = new YarnClient();
		go->get_owner_object().reparent_to(render);
		return PT(NetworkObject)(go);
	}

#ifdef ELY_DEBUG
	static void update_clbk(PT(NetworkObject) THIS);
#endif //ELY_DEBUG

	static void post_read_clbk(PT(NetworkObject) THIS, const BitMask32& stateBit);
	virtual bool HandleCollisionWithCat(PT(RoboCat) inCat) override;

protected:
	YarnClient();

TYPED_OBJECT_API_DECL(YarnClient, Yarn)
};

#endif //YarnClient_h
