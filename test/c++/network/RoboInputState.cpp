#include "RoboInputState.h"

RoboInputState::RoboInputState(InputState& inputState)
{
	inputState.get_members().add_variable("mDesiredRightAmount",
			DataVariables::EPT_Float);
	inputState.get_members().add_variable("mDesiredLeftAmount",
			DataVariables::EPT_Float);
	inputState.get_members().add_variable("mDesiredForwardAmount",
			DataVariables::EPT_Float);
	inputState.get_members().add_variable("mDesiredBackAmount",
			DataVariables::EPT_Float);
	inputState.get_members().add_variable("mIsShooting",
			DataVariables::EPT_Bool);
	inputState.get_members().add_variable("move_forward",
			DataVariables::EPT_Bool);
	inputState.get_members().add_variable("move_backward",
			DataVariables::EPT_Bool);
	inputState.get_members().add_variable("rotate_head_left",
			DataVariables::EPT_Bool);
	inputState.get_members().add_variable("rotate_head_right",
			DataVariables::EPT_Bool);
}

