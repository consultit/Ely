#include "RoboCatServer.h"

#include "ScoreBoardManager.h"
#include "Yarn.h"
#include <networkObjectRegistry.h>
#include <support/NetworkMath.h>

RoboCatServer::RoboCatServer() : RoboCat(),
		mCatControlType(ESCT_Human), mTimeOfNextShot(0.f), mTimeBetweenShots(
				0.2f)
{
/*fixme
	 get_owner_object().remove_node();
	 // add a collision node to render
	 CollisionSphere* collSolid = new CollisionSphere(0,0,0,get_collision_radius());
	 CollisionNode* collNode = new CollisionNode("collNode");
	 collNode->add_solid(collSolid);
	 NodePath collNodeNP = render.attach_new_node(collNode);
	 set_owner_object(collNodeNP);
*/
	// add r/w members
	// set r/w members' dirty states
	// callbacks
	set_update_callback(&RoboCatServer::update_clbk);
//	set_all_state_mask_callback(clbk);
	set_handle_dying_callback(&RoboCatServer::handle_dying_clbk);
//	set_pre_write_callback(clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
//	set_post_read_callback(clbk);
}

void RoboCatServer::handle_dying_clbk(PT(NetworkObject) THIS)
{
	GameNetworkManagerServer::get_global_ptr()->unregister_network_object(THIS);
}

void RoboCatServer::update_clbk(PT(NetworkObject) THIS)
{
	PT(RoboCatServer) downTHIS = DCAST(RoboCatServer, THIS);
	LPoint3f oldLocation = downTHIS->get_location();
	LVector3f oldVelocity = downTHIS->GetVelocity();
	LVecBase3f oldRotation = downTHIS->get_rotation();
	//are you controlled by a player?
	//if so, is there a move we haven't processed yet?
	if (downTHIS->mCatControlType == ESCT_Human)
	{
		PT(ClientProxy) client = GameNetworkManagerServer::get_global_ptr()->get_client_proxy(
				downTHIS->GetPlayerId());
		if (client)
		{
			MoveList& moveList = client->get_unprocessed_move_list();
			for (Move& unprocessedMove : moveList)
			{
				InputState& currentState =
						unprocessedMove.get_input_state();

				float deltaTime = unprocessedMove.get_delta_time();

				downTHIS->ProcessInput(deltaTime, currentState);
				downTHIS->SimulateMovement(deltaTime);

				//LOG( "Server Move Time: %3.4f deltaTime: %3.4f left rot at %3.4f", unprocessedMove.GetTimestamp(), deltaTime, get_rotation() );

			}

		}
	}
	else
	{
		//do some AI stuff
		downTHIS->SimulateMovement(Timing::get_global_ptr()->get_delta_time());
	}

	downTHIS->HandleShooting();

	if (!Math::Is2DVectorEqual(oldLocation, downTHIS->get_location())
			|| !Math::Is2DVectorEqual(oldVelocity, downTHIS->GetVelocity())
			|| oldRotation != downTHIS->get_rotation())
	{
		GameNetworkManagerServer::get_global_ptr()->set_state_dirty(THIS->get_network_id(),
				ECRS_Pose);
	}
}

void RoboCatServer::HandleShooting()
{
	float time = Timing::get_global_ptr()->get_frame_start_time();
	if (mIsShooting && Timing::get_global_ptr()->get_frame_start_time() > mTimeOfNextShot)
	{
		//not exact, but okay
		mTimeOfNextShot = time + mTimeBetweenShots;

		//fire!
		PT(Yarn) yarn = DCAST(Yarn,
				NetworkObjectRegistry::get_global_ptr()->create_network_object(NetworkObject::get_class_id_int("YARN")));
		yarn->InitFromShooter(this);
	}
}

void RoboCatServer::TakeDamage(int inDamagingPlayerId)
{
	mHealth--;
	if (mHealth <= 0.f)
	{
		//score one for damaging player...
		ScoreBoardManager::sInstance->IncScore(inDamagingPlayerId, 1);

		//and you want to die
		set_does_want_to_die(true);

		//tell the client proxy to make you a new cat
		PT(ClientProxy) clientProxy =
				GameNetworkManagerServer::get_global_ptr()->get_client_proxy(GetPlayerId());
		if (clientProxy)
		{
			clientProxy->compute_time_to_respawn();
		}
	}

	//tell the world our health dropped
	GameNetworkManagerServer::get_global_ptr()->set_state_dirty(get_network_id(), ECRS_Health);
}

TYPED_OBJECT_API_DEF(RoboCatServer)
