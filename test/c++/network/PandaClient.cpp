/**
 * \file PandaClient.cpp
 *
 * \date 2018-06-09
 * \author consultit
 */

#include "PandaClient.h"

#include <inputManager.h>
#include <moveList.h>
#include "../../../ely/network/source/gameNetworkManagerClient.h"

PandaClient::PandaClient() : Panda(),
		mTimeLocationBecameOutOfSync(0.f), mTimeVelocityBecameOutOfSync(0.f),
		mTimeRotationBecameOutOfSync(0.f), mTimeAngularVelocityBecameOutOfSync(0.f),
		mOldAngularVelocity(0.f)
{
	// add r/w members
	// set r/w members' dirty states
	// callbacks
	set_update_callback(&PandaClient::update_clbk);
//	set_all_state_mask_callback(clbk);
//	set_handle_dying_callback(clbk);
//	set_pre_write_callback(clbk);
//	set_post_write_callback(clbk);
	set_pre_read_callback(&PandaClient::pre_read_clbk);
	set_post_read_callback(&PandaClient::post_read_clbk);
}

void PandaClient::update_clbk(PT(NetworkObject) THIS)
{
	PT(PandaClient) downTHIS = DCAST(PandaClient, THIS);
	//is this the panda owned by us?
	if (downTHIS->GetPlayerId()
			== (uint32_t) GameNetworkManagerClient::get_global_ptr()->get_player_id())
	{
		Move* pendingMove =
				InputManager::get_global_ptr()->get_pending_move();
		//in theory, only do this if we want to sample input this frame / if there's a new move ( since we have to keep in sync with server )
		if (pendingMove) //is it time to sample a new move...
		{
			float deltaTime = pendingMove->get_delta_time();

			//apply that input

			downTHIS->ProcessInput(deltaTime, pendingMove->get_input_state());

			//and simulate!

			downTHIS->SimulateMovement(deltaTime);

			//LOG( "Client Move Time: %3.4f deltaTime: %3.4f left rot at %3.4f", latestMove.GetTimestamp(), deltaTime, get_rotation() );
		}
	}
	else
	{
		downTHIS->SimulateMovement(Timing::get_global_ptr()->get_delta_time());

/* fixme or xxx ?
		if (Math::Is2DVectorEqual(downTHIS->driver->get_linear_speed(), LVector3f::zero()))
		{
			//we're in sync if our velocity is 0
			downTHIS->mTimeLocationBecameOutOfSync = 0.f;
		}
		if (driver->get_angular_speeds()[0] == 0.f)
		{
			//we're in sync if our angular velocity is 0
			downTHIS->mTimeRotationBecameOutOfSync = 0.f;
		}
*/
	}
#ifdef ELY_DEBUG
	GameObject::update_clbk(THIS);
#endif //ELY_DEBUG
}

/*fixme left only for reference
void PandaClient::read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	uint32_t readState = 0;

	inInputStream.read(stateBit);
	if (stateBit)
	{
		uint32_t playerId;
		inInputStream.read(playerId);
		SetPlayerId(playerId);
		readState |= under_type(EReplicationState::PlayerId);
	}

	LVecBase3f oldRotation = get_rotation();
	LPoint3f oldLocation = get_location();
	LVector3f oldVelocity = driver->get_linear_speed();
	float oldAngularVelocity = driver->get_angular_speeds()[0];

	LVecBase3f replicatedRotation(0.f, 0.f, 0.f);
	LPoint3f replicatedLocation(0.f, 0.f, 0.f);
	LVector3f replicatedVelocity(0.f, 0.f, 0.f);
	float replicatedAngularVelocity = 0.f;

	inInputStream.read(stateBit);
	if (stateBit)
	{
		inInputStream.read(replicatedVelocity[0]);
		inInputStream.read(replicatedVelocity[1]);

		driver->set_linear_speed(replicatedVelocity);

		inInputStream.read(replicatedLocation[0]);
		inInputStream.read(replicatedLocation[1]);

		set_location(replicatedLocation);

		inInputStream.read(replicatedAngularVelocity);

		driver->set_angular_speeds({replicatedAngularVelocity, 0.f});

		inInputStream.read(replicatedRotation);
		set_rotation(replicatedRotation);

		readState |= under_type(EReplicationState::Pose);
	}

	inInputStream.read(stateBit);
	if (stateBit)
	{
		LVecBase4f color;
		inInputStream.read(color);
		set_color(color);
		readState |= under_type(EReplicationState::Color);
	}

	if (GetPlayerId()
			== (uint32_t) GameNetworkManagerClient::get_global_ptr()->get_player_id())
	{

		DoClientSidePredictionAfterReplicationForLocalPanda(readState);

		//if this is a create packet, don't interpolate
		if ((readState & under_type(EReplicationState::PlayerId)) == 0)
		{
			InterpolateClientSidePrediction(oldRotation, oldLocation,
					oldVelocity, oldAngularVelocity, false);
		}
	}
	else
	{
		DoClientSidePredictionAfterReplicationForRemotePanda(readState);

		//will this smooth us out too? it'll interpolate us just 10% of the way there...
		if ((readState & under_type(EReplicationState::PlayerId)) == 0)
		{
			InterpolateClientSidePrediction(oldRotation, oldLocation,
					oldVelocity, oldAngularVelocity, true);
		}

	}

}
*/

void PandaClient::pre_read_clbk(PT(NetworkObject)THIS)
{
	PT(PandaClient) downTHIS = DCAST(PandaClient, THIS);
	downTHIS->mOldRotation = downTHIS->get_rotation();
	downTHIS->mOldLocation = downTHIS->get_location();
	downTHIS->mOldVelocity = downTHIS->driver->get_linear_speed();
	downTHIS->mOldAngularVelocity = downTHIS->driver->get_angular_speeds()[0];
}

void PandaClient::post_read_clbk(PT(NetworkObject)THIS,
		const BitMask32& readState)
{
	// r/w members -> owner object synchronization
	Panda::post_read_clbk(THIS, readState);
	PT(PandaClient) downTHIS = DCAST(PandaClient, THIS);
	if (downTHIS->GetPlayerId() ==
			GameNetworkManagerClient::get_global_ptr()->get_player_id())
	{
		downTHIS->DoClientSidePredictionAfterReplicationForLocalPanda(
				readState.get_word());
		//if this is a create packet, don't interpolate
		if ((readState.get_word() & under_type(EReplicationState::PlayerId)) == 0)
		{
			downTHIS->InterpolateClientSidePrediction(downTHIS->mOldRotation,
					downTHIS->mOldLocation, downTHIS->mOldVelocity,
					downTHIS->mOldAngularVelocity, false);
		}
	}
	else
	{
		downTHIS->DoClientSidePredictionAfterReplicationForRemotePanda(
				readState.get_word());
		//will this smooth us out too? it'll interpolate us just 10% of the way there...
		if ((readState.get_word() & under_type(EReplicationState::PlayerId)) == 0)
		{
			downTHIS->InterpolateClientSidePrediction(downTHIS->mOldRotation,
					downTHIS->mOldLocation, downTHIS->mOldVelocity,
					downTHIS->mOldAngularVelocity, true);
		}
	}
}

void PandaClient::DoClientSidePredictionAfterReplicationForLocalPanda(
		uint32_t inReadState)
{
	if ((inReadState & under_type(EReplicationState::Pose)) != 0)
	{
		//simulate pose only if we received new pose- might have just gotten thrustDir
		//in which case we don't need to replay moves because we haven't warped backwards

		//all processed moves have been removed, so all that are left are unprocessed moves
		//so we must apply them...
		MoveList& moveList = *InputManager::get_global_ptr()->get_move_list();

		for (Move& move : moveList)
		{
			float deltaTime = move.get_delta_time();
			ProcessInput(deltaTime, move.get_input_state());

			SimulateMovement(deltaTime);
		}
	}

}

//so what do we want to do here? need to do some kind of interpolation...
void PandaClient::InterpolateClientSidePrediction(
		const LVecBase3f& inOldRotation, const LPoint3f& inOldLocation,
		const LVector3f& inOldVelocity, float inOldAngularVelocity, bool inIsForRemotePanda)
{
	if (inOldRotation != get_rotation() && !inIsForRemotePanda)
	{
		LOG("ERROR! Move replay ended with incorrect rotation!", 0);
	}

	float roundTripTime = GameNetworkManagerClient::get_global_ptr()->get_round_trip_time();

	//location
	if (!Math::Is2DVectorEqual(inOldLocation, get_location()))
	{
		//LOG( "ERROR! Move replay ended with incorrect location!", 0 );

		//have we been out of sync, or did we just become out of sync?
		float time = Timing::get_global_ptr()->get_frame_start_time();
		if (mTimeLocationBecameOutOfSync == 0.f)
		{
			mTimeLocationBecameOutOfSync = time;
		}

		float durationOutOfSync = time - mTimeLocationBecameOutOfSync;
		if (durationOutOfSync < roundTripTime)
		{
			set_location(
					Lerp(inOldLocation, get_location(),
							inIsForRemotePanda ?
									(durationOutOfSync / roundTripTime) :
									0.1f));
		}
	}
	else
	{
		//we're in sync
		mTimeLocationBecameOutOfSync = 0.f;
	}

	//velocity
	if (!Math::Is2DVectorEqual(inOldVelocity, driver->get_linear_speed()))
	{
		//LOG( "ERROR! Move replay ended with incorrect velocity!", 0 );

		//have we been out of sync, or did we just become out of sync?
		float time = Timing::get_global_ptr()->get_frame_start_time();
		if (mTimeVelocityBecameOutOfSync == 0.f)
		{
			mTimeVelocityBecameOutOfSync = time;
		}

		//now interpolate to the correct value...
		float durationOutOfSync = time - mTimeVelocityBecameOutOfSync;
		if (durationOutOfSync < roundTripTime)
		{
			driver->set_linear_speed(
					Lerp(inOldVelocity, driver->get_linear_speed(),
							inIsForRemotePanda ?
									(durationOutOfSync / roundTripTime) :
									0.1f));
		}
		//otherwise, fine...

	}
	else
	{
		//we're in sync
		mTimeVelocityBecameOutOfSync = 0.f;
	}

	//rotation
	if (!Math::Is2DVectorEqual(inOldRotation, get_rotation()))
	{
		//LOG( "ERROR! Move replay ended with incorrect rotation!", 0 );

		//have we been out of sync, or did we just become out of sync?
		float time = Timing::get_global_ptr()->get_frame_start_time();
		if (mTimeRotationBecameOutOfSync == 0.f)
		{
			mTimeRotationBecameOutOfSync = time;
		}

		float durationOutOfSync = time - mTimeRotationBecameOutOfSync;
		if (durationOutOfSync < roundTripTime)
		{
			set_rotation(
					Lerp(inOldRotation, get_rotation(),
							inIsForRemotePanda ?
									(durationOutOfSync / roundTripTime) :
									0.1f));
		}
	}
	else
	{
		//we're in sync
		mTimeRotationBecameOutOfSync = 0.f;
	}

	//angular velocity
	if (inOldAngularVelocity != driver->get_angular_speeds()[0])
	{
		//LOG( "ERROR! Move replay ended with incorrect velocity!", 0 );

		//have we been out of sync, or did we just become out of sync?
		float time = Timing::get_global_ptr()->get_frame_start_time();
		if (mTimeAngularVelocityBecameOutOfSync == 0.f)
		{
			mTimeAngularVelocityBecameOutOfSync = time;
		}

		//now interpolate to the correct value...
		float durationOutOfSync = time - mTimeAngularVelocityBecameOutOfSync;
		if (durationOutOfSync < roundTripTime)
		{
			float outAngularVelocity = Lerp(inOldAngularVelocity,
					driver->get_angular_speeds()[0],
					inIsForRemotePanda ?
							(durationOutOfSync / roundTripTime) : 0.1f);
			driver->set_angular_speeds({ outAngularVelocity, 0.f });
		}
		//otherwise, fine...

	}
	else
	{
		//we're in sync
		mTimeAngularVelocityBecameOutOfSync = 0.f;
	}

}

void PandaClient::DoClientSidePredictionAfterReplicationForRemotePanda(
		uint32_t inReadState)
{
	if ((inReadState & under_type(EReplicationState::Pose)) != 0)
	{

		//simulate movement for an additional RTT
		float rtt = GameNetworkManagerClient::get_global_ptr()->get_round_trip_time();
		//LOG( "Other panda came in, simulating for an extra %f", rtt );

		//let's break into framerate sized chunks though so that we don't run through walls and do crazy things...
		float deltaTime = 1.f / 30.f;

		while (true)
		{
			if (rtt < deltaTime)
			{
				SimulateMovement(rtt);
				break;
			}
			else
			{
				SimulateMovement(deltaTime);
				rtt -= deltaTime;
			}
		}
	}

}

// set parameters as strings before drivers/chasers creation
void PandaClient::setDriverParametersBeforeCreation()
{
	Panda::setDriverParametersBeforeCreation();

/*fixme no mouse rotation
	GameControlManager* ctrlMgr = GameControlManager::get_global_ptr();
	// set driver's parameters
	ctrlMgr->set_parameter_value(GameControlManager::DRIVER, "mouse_head", "enabled");
	ctrlMgr->set_parameter_value(GameControlManager::DRIVER, "mouse_pitch", "enabled");
*/
}

TYPED_OBJECT_API_DEF(PandaClient)
