/**
 * \file shared_test.cpp
 *
 * \date 2018-06-27
 * \author consultit
 */

#include <memoryBitStream.h>
#include <inFlightPacket.h>
#include <networkTools.h>
#include <networkObject.h>
#include <random>

template<typename T> T Max()
{
	return std::numeric_limits<T>::max();
}

constexpr int DIM = 16;
const int32_t MAX_INT32 = Max<int8_t>();
const int64_t MAX_INT64 = Max<int8_t>();
const float MAX_FLOAT = Max<int8_t>();
const double MAX_DOUBLE = Max<int8_t>();

struct NetworkTestSuiteFixture
{
	NetworkTestSuiteFixture()
	{
		generator = new std::default_random_engine((std::random_device())());
		int32Dist = std::uniform_int_distribution<int32_t>(-MAX_INT32, MAX_INT32);
		int64Dist = std::uniform_int_distribution<int64_t>(-MAX_INT64, MAX_INT64);
		floatDist = std::uniform_real_distribution<float>(-MAX_FLOAT, MAX_FLOAT);
		doubleDist = std::uniform_real_distribution<double>(-MAX_DOUBLE, MAX_DOUBLE);
	}
	~NetworkTestSuiteFixture()
	{
		delete generator;
	}
	static int32_t RandInt32()
	{
		return int32Dist(*generator);
	}
	static int64_t RandInt64()
	{
		return int64Dist(*generator);
	}
	static float RandFloat()
	{
		return floatDist(*generator);
	}
	static double RandDouble()
	{
		return doubleDist(*generator);
	}
private:
	static std::default_random_engine *generator;
	static std::uniform_int_distribution<int32_t> int32Dist;
	static std::uniform_int_distribution<int64_t> int64Dist;
	static std::uniform_real_distribution<float> floatDist;
	static std::uniform_real_distribution<double> doubleDist;
};
std::default_random_engine *NetworkTestSuiteFixture::generator;
std::uniform_int_distribution<int32_t> NetworkTestSuiteFixture::int32Dist;
std::uniform_int_distribution<int64_t> NetworkTestSuiteFixture::int64Dist;
std::uniform_real_distribution<float> NetworkTestSuiteFixture::floatDist;
std::uniform_real_distribution<double> NetworkTestSuiteFixture::doubleDist;

template<typename T> bool BOOST_CHECK_EQUAL(T a, T b)
{
	return a == b;
}

template<typename T> bool BOOST_CHECK_CLOSE(T a, T b, float tolerance)
{
	return abs(a - b) < abs(a) * tolerance / 100.0f;
}

#define FILLDATAORDERED(type) \
	dataVarsS.add_variable("m"#type, DataVariables::EPT_##type);\
	dataVarsE.add_variable("m"#type, DataVariables::EPT_##type);
#define MAKEINTERNALCHECK(type, typesuffix)	\
	type##E = type();\
	dataVarsS.set_value_##typesuffix("m"#type, type##S);\
	dataVarsS.get_value_##typesuffix("m"#type, type##E);\
	cout << BOOST_CHECK_EQUAL(type##E.get_value(), type##S.get_value()) << endl;
#define MAKEINTERNALCHECK1(type, typesuffix)	\
	type##E = type();\
	dataVarsS.set_value_##typesuffix("m"#type, type##S);\
	dataVarsS.get_value_##typesuffix("m"#type, type##E);\
	cout << BOOST_CHECK_EQUAL(type##E, type##S) << endl;
#define MAKESTREAMCHECK(type, typesuffix) \
	type##E = type();\
	dataVarsE.get_value_##typesuffix("m"#type, type##E);\
	cout << BOOST_CHECK_EQUAL(type##E.get_value(), type##S.get_value()) << endl;
#define MAKESTREAMCHECK1(type, typesuffix) \
	type##E = type();\
	dataVarsE.get_value_##typesuffix("m"#type, type##E);\
	cout << BOOST_CHECK_EQUAL(type##E, type##S) << endl;

int main(int,char**)
{
	cout.setf(std::ios::boolalpha);
	NetworkTestSuiteFixture fixture;
	Int32 Int32S, Int32E;
	String StringS, StringE;
	Float FloatS, FloatE;
	Bool BoolS, BoolE;
	LVecBase3f LVecBase3fS, LVecBase3fE;
	LPoint3f LPoint3fS, LPoint3fE;
	LVector3f LVector3fS, LVector3fE;
	LVecBase4f LVecBase4fS, LVecBase4fE;
	LQuaternionf LQuaternionfS, LQuaternionfE;
	DataVariables dataVarsS, dataVarsE;
	OutputMemoryBitStream outStream;
	InputMemoryBitStream inStream(outStream.get_buffer_ptr(), 0);
	Int32S = NetworkTestSuiteFixture::RandInt32();
	StringS = ("Hello World");
	FloatS = NetworkTestSuiteFixture::RandFloat();
	BoolS = true;
	LVecBase3fS = LVecBase3f(NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat());
	LPoint3fS = LPoint3f(NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat());
	LVector3fS = LVector3f(NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat());
	LVecBase4fS = LVecBase4f(NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat());
	LQuaternionfS = LQuaternionf(NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat(),
			NetworkTestSuiteFixture::RandFloat());
	LQuaternionfS.normalize();
	//FILLDATAORDERED(Int)
	dataVarsS.add_variable("m" "Int32", DataVariables::EPT_Int32);
	dataVarsE.add_variable("m" "Int32", DataVariables::EPT_Int32);
	//
	FILLDATAORDERED(String)
	FILLDATAORDERED(Float)
	FILLDATAORDERED(Bool)
	FILLDATAORDERED(LVecBase3f)
	FILLDATAORDERED(LPoint3f)
	FILLDATAORDERED(LVector3f)
	FILLDATAORDERED(LVecBase4f)
	FILLDATAORDERED(LQuaternionf)
	cout << "DataVariables INTERNAL CHECK" << endl;
	//MAKEINTERNALCHECK(Int, int)
	Int32E = Int32();
	dataVarsS.set_value_int32_t("m""Int32", Int32S);
	dataVarsS.get_value_int32_t("m""Int32", Int32E);
	cout << BOOST_CHECK_EQUAL(Int32E.get_value(), Int32S.get_value()) << endl;
	//
	MAKEINTERNALCHECK(String, string)
	MAKEINTERNALCHECK(Float, float)
	MAKEINTERNALCHECK(Bool, bool)
	MAKEINTERNALCHECK1(LVecBase3f, lvecbase3f)
	MAKEINTERNALCHECK1(LPoint3f, lpoint3f)
	MAKEINTERNALCHECK1(LVector3f, lvector3f)
	MAKEINTERNALCHECK1(LVecBase4f, lvecbase4f)
	MAKEINTERNALCHECK1(LQuaternionf, lquaternionf)
	cout << "DataVariables STREAM CHECK" << endl;
	dataVarsS.write(outStream);
	dataVarsE.read(inStream);
	//MAKESTREAMCHECK(Int, int)
	Int32E = Int32();
	dataVarsE.get_value_int32_t("m""Int32", Int32E);
	cout << BOOST_CHECK_EQUAL(Int32E.get_value(), Int32S.get_value()) << endl;
	//
	MAKESTREAMCHECK(String, string)
	MAKESTREAMCHECK(Float, float)
	MAKESTREAMCHECK(Bool, bool)
	MAKESTREAMCHECK1(LVecBase3f, lvecbase3f)
	MAKESTREAMCHECK1(LPoint3f, lpoint3f)
	MAKESTREAMCHECK1(LVector3f, lvector3f)
	MAKESTREAMCHECK1(LVecBase4f, lvecbase4f)
	///streams' R/W for LQuaternionf use 'fixed point' approximation
	MAKESTREAMCHECK1(LQuaternionf, lquaternionf)
	//Expected Failure
	float tolerance = MAX_FLOAT * (2.f / 65535.f) * 100;
	for (int i = 0; i < 4; i++)
	{
		cout << BOOST_CHECK_CLOSE(LQuaternionfE[i], LQuaternionfS[i], tolerance)  << endl;
	}
	//NetworkObject
	NetworkObject* netObj = new NetworkObject('TST0');
	PT(NetworkObject) netObjPT;
	cout << netObj->get_ref_count() << endl;
	netObjPT = PT(NetworkObject)(netObj);
	cout << netObj->get_ref_count() << endl;
	//
	PT(NetworkObject) netObjPT1 = new NetworkObject('TST1');
	cout << netObjPT1.p()->get_ref_count() << endl;
	//
	PT(NetworkObject) netObjPT2;
	NetworkObject* netObj2 = new NetworkObject('TST2');
	netObjPT2 = PT(NetworkObject)(netObj2);
	cout << netObjPT2.p()->get_ref_count() << endl;
	cout << (netObjPT2.p() == netObj2) << endl;
	//
	cout.unsetf(std::ios::boolalpha);
	return 0;
}
