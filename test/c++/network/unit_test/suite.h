/**
 * \file fixture.h
 *
 * \date 2018-06-27
 * \author consultit
 */

#ifndef NETWORK_FIXTURE_H_
#define NETWORK_FIXTURE_H_

#include <boost/test/unit_test.hpp>
#include <random>

template<typename T> T Max()
{
	return std::numeric_limits<T>::max();
}

constexpr int DIM = 16;
const int32_t MAX_INT32 = Max<int8_t>();
const int64_t MAX_INT64 = Max<int8_t>();
const float MAX_FLOAT = Max<int8_t>();
const double MAX_DOUBLE = Max<int8_t>();

struct NetworkTestSuiteFixture
{
	NetworkTestSuiteFixture()
	{
		BOOST_TEST_MESSAGE("ctor NetworkTestSuiteFixture");
		generator = new std::default_random_engine((std::random_device())());
		int32Dist = std::uniform_int_distribution<int32_t>(-MAX_INT32, MAX_INT32);
		int64Dist = std::uniform_int_distribution<int64_t>(-MAX_INT64, MAX_INT64);
		floatDist = std::uniform_real_distribution<float>(-MAX_FLOAT, MAX_FLOAT);
		doubleDist = std::uniform_real_distribution<double>(-MAX_DOUBLE, MAX_DOUBLE);
	}
	~NetworkTestSuiteFixture()
	{
		BOOST_TEST_MESSAGE("dtor NetworkTestSuiteFixture");
		delete generator;
	}
	static int32_t RandInt32()
	{
		return int32Dist(*generator);
	}
	static int64_t RandInt64()
	{
		return int64Dist(*generator);
	}
	static float RandFloat()
	{
		return floatDist(*generator);
	}
	static double RandDouble()
	{
		return doubleDist(*generator);
	}
private:
	static std::default_random_engine *generator;
	static std::uniform_int_distribution<int32_t> int32Dist;
	static std::uniform_int_distribution<int64_t> int64Dist;
	static std::uniform_real_distribution<float> floatDist;
	static std::uniform_real_distribution<double> doubleDist;
};

#endif /* NETWORK_FIXTURE_H_ */
