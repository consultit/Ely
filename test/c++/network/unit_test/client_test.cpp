/**
 * \file client_test.cpp
 *
 * \date 2018-06-27
 * \author consultit
 */

#include "suite.h"

struct ClientTestCaseFixture
{
	ClientTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("ctor ClientTestCaseFixture");
	}
	~ClientTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("dtor ClientTestCaseFixture");
	}
};

/// NetworkTestSuite
BOOST_FIXTURE_TEST_SUITE(NetworkTestSuite, NetworkTestSuiteFixture);

/// Test cases
BOOST_AUTO_TEST_CASE(ClientTEST)
{
	BOOST_TEST_MESSAGE("ClientTEST");
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END(); // NetworkTestSuite
