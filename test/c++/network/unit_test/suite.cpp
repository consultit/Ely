/**
 * \file dummy_defs.cpp
 *
 * \date 2018-06-27
 * \author consultit
 */

/// some dummy definitions
#include "suite.h"

std::default_random_engine *NetworkTestSuiteFixture::generator;
std::uniform_int_distribution<int32_t> NetworkTestSuiteFixture::int32Dist;
std::uniform_int_distribution<int64_t> NetworkTestSuiteFixture::int64Dist;
std::uniform_real_distribution<float> NetworkTestSuiteFixture::floatDist;
std::uniform_real_distribution<double> NetworkTestSuiteFixture::doubleDist;

void spawnCatForPlayer(int)
{
}
