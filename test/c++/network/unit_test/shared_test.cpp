/**
 * \file shared_test.cpp
 *
 * \date 2018-06-27
 * \author consultit
 */

#include "suite.h"
#include <support/ByteSwap.h>
#include <memoryBitStream.h>
#include <ackRange.h>
#include <inFlightPacket.h>
#include <replicationManagerTransmissionData.h>
#include <networkTools.h>

struct SharedTestCaseFixture
{
	SharedTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("ctor SharedTestCaseFixture");
	}
	~SharedTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("dtor SharedTestCaseFixture");
	}
};

/// NetworkTestSuite
BOOST_FIXTURE_TEST_SUITE(NetworkTestSuite, NetworkTestSuiteFixture);

/// Test cases
BOOST_AUTO_TEST_CASE(ByteSwapTEST)
{
	int32_t int32 = 0x12345678, int32Swap = 0x78563412;
	int64_t int64 = 0x1234567812345678, int64Swap = 0x7856341278563412;
	float float32 = 1.2345678e9f;
	double double64 = 1.234567e+89;
	//int32
	BOOST_CHECK_EQUAL(ByteSwap(int32), int32Swap);
	BOOST_CHECK_EQUAL(int32 + int32Swap, ByteSwap(ByteSwap(int32 + int32Swap)));
	int32_t int32Rand = RandInt32();
	BOOST_CHECK_EQUAL(int32Rand, ByteSwap(ByteSwap(int32Rand)));
	//int64
	BOOST_CHECK_EQUAL(ByteSwap(int64), int64Swap);
	BOOST_CHECK_EQUAL(int64 + int64Swap, ByteSwap(ByteSwap(int64 + int64Swap)));
	int64_t int64Rand = RandInt64();
	BOOST_CHECK_EQUAL(int64Rand, ByteSwap(ByteSwap(int64Rand)));
	//float
	int32_t repr32 = TypeAliaser<float, int32_t>(float32).Get();
	int32_t repr32Swap = TypeAliaser<float, int32_t>(ByteSwap(float32)).Get();
	BOOST_TEST_MESSAGE("byte representation of " << float32 << " is " << std::hex << repr32 <<
			" and its swapped one is " << repr32Swap);
	float float32Rand = RandFloat();
	BOOST_CHECK_EQUAL(float32Rand, ByteSwap(ByteSwap(float32Rand)));
	//double
	int64_t repr64 = TypeAliaser<double, int64_t>(double64).Get();
	int64_t repr64Swap = TypeAliaser<double, int64_t>(ByteSwap(double64)).Get();
	BOOST_TEST_MESSAGE("byte representation of " << double64 << " is " << std::hex << repr64 <<
			" and its swapped one is " << repr64Swap);
	double double64Rand = RandDouble();
	BOOST_CHECK_EQUAL(double64Rand, ByteSwap(ByteSwap(double64Rand)));
}

BOOST_AUTO_TEST_CASE(MemoryStreamTEST)
{
	OutputMemoryBitStream outStream;
	InputMemoryBitStream inStream(outStream.get_buffer_ptr(), 0);
	pvector<LPoint3f> pStart(DIM), pEnd(DIM);
	vector<String> vStrings = {string("Hello"), string("World"), string("!")};
	for (int i = 0; i < DIM ; i++)
	{
		pStart[i] = LPoint3f(RandFloat(), RandFloat(), RandFloat());
		outStream.write(pStart[i]);
	}
	for(auto s: vStrings)
	{
		outStream.write(s);
	}
	for (int i = 0; i < DIM ; i++)
	{
		inStream.read(pEnd[i]);
		BOOST_CHECK_EQUAL(pStart[i], pEnd[i]);
	}
	for(auto s: vStrings)
	{
		String strS;
		inStream.read(strS);
		BOOST_TEST_MESSAGE(strS.get_value());
		BOOST_CHECK_EQUAL(strS.get_value(), s.get_value());
	}
}

class TransmissionDataTest : public TransmissionData
{
public:
	int i;
	void handle_delivery_failure(DeliveryNotificationManager*) const{}
	void handle_delivery_success(DeliveryNotificationManager*) const{}
	bool operator==(const TransmissionDataTest& other)
	{return i == other.i;}
};

BOOST_AUTO_TEST_CASE(PacketsTEST)
{
	BOOST_TEST_MESSAGE("AckRange CHECK");
	OutputMemoryBitStream outStream;
	AckRange ackRStart(DIM);
	ackRStart.extend_if_should(DIM + 1);
	ackRStart.write(outStream);
	InputMemoryBitStream inStream(outStream.get_buffer_ptr(), outStream.get_bit_length());
	AckRange ackREnd;
	ackREnd.read(inStream);
	BOOST_CHECK_EQUAL(ackRStart.get_start(), ackREnd.get_start());
	BOOST_CHECK_EQUAL(ackRStart.get_count(), ackREnd.get_count());
	BOOST_TEST_MESSAGE("InFlightPacket CHECK");
	InFlightPacket flightPacket(DIM);
	PT(TransmissionDataTest) txData = new TransmissionDataTest();
	txData->i = RandInt32();
	int inKey = (int)abs(RandInt32());
	flightPacket.set_transmission_data(inKey, txData);
	bool transmissionDataEqual =
			(*DCAST(TransmissionDataTest, flightPacket.get_transmission_data(inKey))
					== *txData);
	BOOST_CHECK(transmissionDataEqual);
}

struct DataVariablesFixture
{
	DataVariablesFixture() :
			outStream(), inStream(outStream.get_buffer_ptr(), 0)
	{
		BOOST_TEST_MESSAGE("ctor DataVariablesFixture");
		Int32S = NetworkTestSuiteFixture::RandInt32();
		StringS = ("Hello World");
		FloatS = NetworkTestSuiteFixture::RandFloat();
		BoolS = true;
		LVecBase3fS = LVecBase3f(NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat());
		LPoint3fS = LPoint3f(NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat());
		LVector3fS = LVector3f(NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat());
		LVecBase4fS = LVecBase4f(NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat());
		LQuaternionfS = LQuaternionf(NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat(),
				NetworkTestSuiteFixture::RandFloat());
		LQuaternionfS.normalize();
	}
	~DataVariablesFixture()
	{
		BOOST_TEST_MESSAGE("dtor DataVariablesFixture");
	}
protected:
	Int32 Int32S, Int32E;
	String StringS, StringE;
	Float FloatS, FloatE;
	Bool BoolS, BoolE;
	LVecBase3f LVecBase3fS, LVecBase3fE;
	LPoint3f LPoint3fS, LPoint3fE;
	LVector3f LVector3fS, LVector3fE;
	LVecBase4f LVecBase4fS, LVecBase4fE;
	LQuaternionf LQuaternionfS, LQuaternionfE;
	DataVariables dataVarsS, dataVarsE;
	OutputMemoryBitStream outStream;
	InputMemoryBitStream inStream;
};

#define FILLDATAORDERED(type) \
	dataVarsS.add_variable("m"#type, DataVariables::EPT_##type);\
	dataVarsE.add_variable("m"#type, DataVariables::EPT_##type);
#define MAKEINTERNALCHECK(type, typesuffix)	\
	type##E = type();\
	dataVarsS.set_value_##typesuffix("m"#type, type##S);\
	dataVarsS.get_value_##typesuffix("m"#type, type##E);\
	BOOST_CHECK_EQUAL(type##E.get_value(), type##S.get_value());
#define MAKEINTERNALCHECK1(type, typesuffix)	\
	type##E = type();\
	dataVarsS.set_value_##typesuffix("m"#type, type##S);\
	dataVarsS.get_value_##typesuffix("m"#type, type##E);\
	BOOST_CHECK_EQUAL(type##E, type##S);
#define MAKESTREAMCHECK(type, typesuffix) \
	type##E = type();\
	dataVarsE.get_value_##typesuffix("m"#type, type##E);\
	BOOST_CHECK_EQUAL(type##E.get_value(), type##S.get_value());
#define MAKESTREAMCHECK1(type, typesuffix) \
	type##E = type();\
	dataVarsE.get_value_##typesuffix("m"#type, type##E);\
	BOOST_CHECK_EQUAL(type##E, type##S);

BOOST_FIXTURE_TEST_CASE(DataVariablesTEST, DataVariablesFixture,
		* boost::unit_test::expected_failures(1))
{
	FILLDATAORDERED(Int32)
	FILLDATAORDERED(String)
	FILLDATAORDERED(Float)
	FILLDATAORDERED(Bool)
	FILLDATAORDERED(LVecBase3f)
	FILLDATAORDERED(LPoint3f)
	FILLDATAORDERED(LVector3f)
	FILLDATAORDERED(LVecBase4f)
	FILLDATAORDERED(LQuaternionf)
	BOOST_TEST_MESSAGE("DataVariables INTERNAL CHECK");
	MAKEINTERNALCHECK(Int32, int32_t)
	MAKEINTERNALCHECK(String, string)
	MAKEINTERNALCHECK(Float, float)
	MAKEINTERNALCHECK(Bool, bool)
	MAKEINTERNALCHECK1(LVecBase3f, lvecbase3f)
	MAKEINTERNALCHECK1(LPoint3f, lpoint3f)
	MAKEINTERNALCHECK1(LVector3f, lvector3f)
	MAKEINTERNALCHECK1(LVecBase4f, lvecbase4f)
	MAKEINTERNALCHECK1(LQuaternionf, lquaternionf)
	BOOST_TEST_MESSAGE("DataVariables STREAM CHECK");
	dataVarsS.write(outStream);
	dataVarsE.read(inStream);
	MAKESTREAMCHECK(Int32, int32_t)
	MAKESTREAMCHECK(String, string)
	MAKESTREAMCHECK(Float, float)
	MAKESTREAMCHECK(Bool, bool)
	MAKESTREAMCHECK1(LVecBase3f, lvecbase3f)
	MAKESTREAMCHECK1(LPoint3f, lpoint3f)
	MAKESTREAMCHECK1(LVector3f, lvector3f)
	MAKESTREAMCHECK1(LVecBase4f, lvecbase4f)
	///streams' R/W for LQuaternionf use 'fixed point' approximation
	MAKESTREAMCHECK1(LQuaternionf, lquaternionf) //Expected Failure
	float tolerance = MAX_FLOAT * (2.f / 65535.f) * 100;
	for (int i = 0; i < 4; i++)
	{
		BOOST_CHECK_CLOSE(LQuaternionfE[i], LQuaternionfS[i], tolerance);
	}
}

BOOST_AUTO_TEST_SUITE_END(); // NetworkTestSuite
