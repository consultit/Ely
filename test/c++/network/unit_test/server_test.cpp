/**
 * \file server_test.cpp
 *
 * \date 2018-06-27
 * \author consultit
 */

#include "suite.h"

struct ServerTestCaseFixture
{
	ServerTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("ctor ServerTestCaseFixture");
	}
	~ServerTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("dtor ServerTestCaseFixture");
	}
};

/// NetworkTestSuite
BOOST_FIXTURE_TEST_SUITE(NetworkTestSuite, NetworkTestSuiteFixture);

/// Test cases
BOOST_AUTO_TEST_CASE(ServerTEST)
{
	BOOST_TEST_MESSAGE("ServerTEST");
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END(); // NetworkTestSuite
