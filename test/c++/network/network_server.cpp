/**
 * \file network_server.cpp
 *
 * \date 2018-05-25
 * \author consultit
 */

#include "MouseServer.h"
#include "YarnServer.h"
#include "RoboCatServer.h"
#include "PandaServer.h"
#include "RoboInputState.h"
#include "ScoreBoardManager.h"
#include <networkWorld.h>
#include <networkObjectRegistry.h>
#include <socketUtil.h>
#include <clientProxy.h>
#include <replicationManagerTransmissionData.h>
#include <support/NetworkMath.h>
#include <load_prc_file.h>
#include <pandaFramework.h>
#include <gameControlManager.h>
#include <p3Driver.h>
#include <p3Chaser.h>
#include "../../../ely/network/source/gameNetworkManagerServer.h"

/// global declarations/definitions
PandaFramework framework;
WindowFramework *window = nullptr;
NodePath render;
extern string dataDir;

void spawnCatForPlayer(int);

/// static global declarations/definitions
namespace
{
AsyncTask* doFrameTask = nullptr;
int doFrameSort = 10;
int ctrlMgrUpdateSort = 15;
string portString = "45000";
string msg = "Network Application - Server";
bool windowOn = false;
vector<string> dirs;
bool noWorld = false;
float disconnectTimeout = 5.0;
float respawnDelay = 3.0;
float delayBeforeAck = 0.5;

void parseArguments(int, char**);
void loadPandaFramework(int, char**, const string&);
void setupPandaFramework();
void loadAndSetupNetworkFramework();
void loadAndSetupGameFramework();
void atExitCleanup();
AsyncTask::DoneStatus doFrameServer(GenericAsyncTask*, void*);
void setupWorld();
PT(RoboCat) getCatForPlayer(int);
void libp3toolsTypedObjectInit();
//callbacks
void handleNewClient(PT(ClientProxy));
void handleLostClient(PT(ClientProxy));
void setShouldKeepRunning(bool);
void AddScoreBoardStateToPacket(OutputMemoryBitStream&);
}

int main(int argc, char** argv)
{
	///INITIALIZATION
	parseArguments(argc, argv);
	msg = "ServerMain - listening port: " + portString;
	///SETUP
	loadPandaFramework(argc, argv, msg);
	setupPandaFramework();
	loadAndSetupNetworkFramework();
	loadAndSetupGameFramework();
	///RUN
	{
		// do the main loop, equals to call app.run() in python
		framework.main_loop();
	}
	///ATEXIT
	atExitCleanup();
	//
	return (0);
}

///SOURCES

namespace
{

void parseArguments(int argc, char** argv)
{
	int c;
	while ((c = getopt(argc, argv, "p:wd:nt:r:k:")) != -1)
	{
		switch (c)
		{
		case 'p':
		{
			portString = optarg;
		}
			break;
		case 'w':
		{
			windowOn = true;
		}
			break;
		case 'd':
		{
			dirs.emplace_back(optarg);
		}
			break;
		case 'n':
		{
			noWorld = true;
		}
			break;
		case 't':
		{
			disconnectTimeout = atof(optarg);
		}
			break;
		case 'r':
		{
			respawnDelay = atof(optarg);
		}
			break;
		case 'k':
		{
			delayBeforeAck = atof(optarg);
		}
			break;
		case '?':
			if (optopt == 'p' || optopt == 'd' || optopt == 't'
					|| optopt == 'r' || optopt == 'k')
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			else if (isprint(optopt))
				fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
			break;
		default:
			abort();
		}
	}
	for (c = optind; c < argc; c++)
	{
		fprintf(stderr, "Unused argument `%s'.\n", argv[c]);
	}
}

void loadPandaFramework(int argc, char** argv, const string& msg)
{
	// Load your application's configuration
	load_prc_file_data("", "model-path " + dataDir);
	for (auto dir : dirs)
	{
		load_prc_file_data("", "model-path " + dir);
	}
	if (windowOn)
	{
		load_prc_file_data("", "win-size 1024 768");
		load_prc_file_data("", "show-frame-rate-meter #t");
		load_prc_file_data("", "sync-video #t");
	}
	else
	{
		load_prc_file_data("", "window-type	none");
	}
	// Setup your application
	framework.open_framework(argc, argv);
	setShouldKeepRunning(true);
	if (windowOn)
	{
		framework.set_window_title(msg);
		window = framework.open_window();
		if (window != (WindowFramework *) nullptr)
		{
			cout << "Opened the window successfully!\n";
			window->enable_keyboard();
			window->setup_trackball();
		}
		render = window->get_render();
	}
	else
	{
		render = NodePath("render");
		cout << msg << endl;
	}
}

void setupPandaFramework()
{
	/// typed object init; not needed if you build inside panda source tree
	P3Driver::init_type();
	P3Chaser::init_type();
	GameControlManager::init_type();
	P3Driver::register_with_read_factory();
	P3Chaser::register_with_read_factory();
	libp3toolsTypedObjectInit();
	// network
	ValueList_ClientProxyPtr::init_type();
	ValueList_NetworkObjectPtr::init_type();
	ValueList_TCPSocketPtr::init_type();
	NetworkObject::init_type();
	GameNetworkManager::init_type();
	GameNetworkManagerServer::init_type();
	NetworkWorld::init_type();
	NetworkObjectRegistry::init_type();
	SocketAddress::init_type();
	ClientProxy::init_type();
	TransmissionData::init_type();
	ReplicationManagerTransmissionData::init_type();
	UDPSocket::init_type();
	TCPSocket::init_type();
	NetBitMask::init_type();
	GameObject::init_type();
	Mouse::init_type();
	MouseServer::init_type();
	Yarn::init_type();
	YarnServer::init_type();
	RoboCat::init_type();
	RoboCatServer::init_type();
	Panda::init_type();
	PandaServer::init_type();
	// create a control manager
	GameControlManager* controlMgr = new GameControlManager(
			GameControlManager::Output(),
			ctrlMgrUpdateSort, render, BitMask32::all_on());
	// reparent the reference node to render
	controlMgr->get_reference_node_path().reparent_to(render);
/* fixme manual update of driver
	// start the default update task for all drivers
	controlMgr->start_default_update();
*/

	// create the task for DoFrameServer
	doFrameTask = new GenericAsyncTask(string("DoFrameServer"), &doFrameServer,
			nullptr);
	doFrameTask->set_sort(doFrameSort);
	//Adds mDoFrameServerTask to the active queue.
	AsyncTaskManager::get_global_ptr()->add(doFrameTask);
}

void loadAndSetupNetworkFramework()
{
	new Timing();
	SocketUtil::static_init();
	srand(static_cast<uint32_t>(time(nullptr)));
	uint16_t port = stoi(portString);
	if (!(new GameNetworkManagerServer())->init(port))
	{
		exit(1);
	}
	new NetworkWorld();
	// manage custom InputState
	new InputStateFactory();
	new RoboInputState(*InputStateFactory::get_global_ptr()->get_input_state());
	new NetworkObjectRegistry();
}

void loadAndSetupGameFramework()
{
	ScoreBoardManager::StaticInit();
	//
	NetworkObjectRegistry::get_global_ptr()->register_creation_function(NetworkObject::get_class_id_int("PAND"),
			PandaServer::StaticCreate);
	NetworkObjectRegistry::get_global_ptr()->register_creation_function(NetworkObject::get_class_id_int("RCAT"),
			RoboCatServer::StaticCreate);
	NetworkObjectRegistry::get_global_ptr()->register_creation_function(NetworkObject::get_class_id_int("MOUS"), 
			MouseServer::StaticCreate);
	NetworkObjectRegistry::get_global_ptr()->register_creation_function(NetworkObject::get_class_id_int("YARN"),
			YarnServer::StaticCreate);
	//
	setupWorld();
	//
	GameNetworkManagerServer::get_global_ptr()->set_client_disconnect_timeout(disconnectTimeout);
	GameNetworkManagerServer::get_global_ptr()->set_send_state_packet_to_client_callback(&AddScoreBoardStateToPacket);
	GameNetworkManagerServer::get_global_ptr()->set_handle_packet_from_new_client_callback(&handleNewClient);
	GameNetworkManagerServer::get_global_ptr()->set_handle_client_disconnected_callback(&handleLostClient);
	GameNetworkManagerServer::get_global_ptr()->set_delay_before_ack_timeout(delayBeforeAck);
	GameNetworkManagerServer::get_global_ptr()->set_respawn_client_objects_delay(respawnDelay);
	GameNetworkManagerServer::get_global_ptr()->set_respawn_client_objects_callback(&spawnCatForPlayer);
}

void atExitCleanup()
{
	if (doFrameTask)
	{
		AsyncTaskManager::get_global_ptr()->remove(doFrameTask);
	}

	delete NetworkObjectRegistry::get_global_ptr();
	delete RoboInputState::get_global_ptr();
	delete InputStateFactory::get_global_ptr();
	delete NetworkWorld::get_global_ptr();
	delete GameNetworkManagerServer::get_global_ptr();
	SocketUtil::clean_up();
	delete Timing::get_global_ptr();
}

AsyncTask::DoneStatus doFrameServer(GenericAsyncTask* task, void*)
{
	Timing::get_global_ptr()->update();
	GameNetworkManagerServer::get_global_ptr()->process_incoming_packets();
	GameNetworkManagerServer::get_global_ptr()->check_for_disconnects();
	GameNetworkManagerServer::get_global_ptr()->respawn_objects_for_clients();
	NetworkWorld::get_global_ptr()->update();
	// the pending moves of all clients have been processed: clear all
	GameNetworkManagerServer::get_global_ptr()->clear_all_client_moves();
	GameNetworkManagerServer::get_global_ptr()->send_outgoing_packets();
	return AsyncTask::DS_cont;
}

void CreateRandomMice(int inMouseCount)
{
	LPoint3f mouseMin(WORLD_MIN * 0.9);
	LPoint3f mouseMax(WORLD_MAX * 0.9);
	PT(GameObject) go;

	//make a mouse somewhere- where will these come from?
	for (int i = 0; i < inMouseCount; ++i)
	{
		go = DCAST(GameObject, NetworkObjectRegistry::get_global_ptr()->create_network_object(NetworkObject::get_class_id_int("MOUS")));
		LPoint3f mouseLocation = Math::GetRandomVector(mouseMin,
				mouseMax);
		go->set_location(mouseLocation);
	}
}

void setupWorld()
{
	if (!noWorld)
	{
		//spawn some random mice
		CreateRandomMice(10);

		//spawn more random mice!
		CreateRandomMice(10);
	}
}

PT(RoboCat) getCatForPlayer(int inPlayerId)
{
	//run through the objects till we find the cat...
	//it would be nice if we kept a pointer to the cat on the clientproxy
	//but then we'd have to clean it up when the cat died, etc.
	//this will work for now until it's a perf issue
	const auto& networkObjects = NetworkWorld::get_global_ptr()->get_network_objects();
	for (int i = 0, c = networkObjects.size(); i < c; ++i)
	{
		PT(NetworkObject) go = networkObjects[i];
		if ((go->get_class_id() == NetworkObject::get_class_id_int("RCAT"))
				&& (DCAST(RoboCat,go)->GetPlayerId()
						== (uint32_t) inPlayerId))
		{
			return DCAST(RoboCat,go);
		}
	}

	return nullptr;

}

PT(Panda) getPandaForPlayer(int inPlayerId)
{
	//run through the objects till we find the panda...
	//it would be nice if we kept a pointer to the panda on the clientproxy
	//but then we'd have to clean it up when the panda died, etc.
	//this will work for now until it's a perf issue
	const auto& networkObjects = NetworkWorld::get_global_ptr()->get_network_objects();
	for (int i = 0, c = networkObjects.size(); i < c; ++i)
	{
		PT(NetworkObject) go = networkObjects[i];
		if ((go->get_class_id() == NetworkObject::get_class_id_int("PAND"))
				&& (DCAST(Panda, go)->GetPlayerId()
						== (uint32_t) inPlayerId))
		{
			return DCAST(Panda, go);
		}
	}

	return nullptr;

}

// libp3tools typed objects init
void libp3toolsTypedObjectInit()
{
	Pair_bool_float::init_type();
	Pair_LPoint3f_int::init_type();
	Pair_LVector3f_ValueList_float::init_type();
	ValueList_string::init_type();
	ValueList_NodePath::init_type();
	ValueList_int::init_type();
	ValueList_float::init_type();
	ValueList_LVecBase3f::init_type();
	ValueList_LVector3f::init_type();
	ValueList_LPoint3f::init_type();
	ValueList_Pair_LPoint3f_int::init_type();
}

void handleNewClient(PT(ClientProxy) inClientProxy)
{

	int playerId = inClientProxy->get_player_id();

	ScoreBoardManager::sInstance->AddEntry(playerId, inClientProxy->get_name());
	spawnCatForPlayer(playerId);

	//create a driven panda
	{
		PT(Panda) panda = DCAST(Panda,
				NetworkObjectRegistry::get_global_ptr()->create_network_object(NetworkObject::get_class_id_int("PAND")));
		panda->set_color(
				ScoreBoardManager::sInstance->GetEntry(playerId)->GetColor());
		panda->SetPlayerId(playerId);
		//gotta pick a better spawn location than this...
		panda->set_location(LPoint3f(10.f - static_cast<float>(playerId), 0.f, 0.f));
	}
}

void handleLostClient(PT(ClientProxy) inClientProxy)
{
	//kill client's cat
	//remove client from scoreboard
	int playerId = inClientProxy->get_player_id();
	ScoreBoardManager::sInstance->RemoveEntry(playerId);
	PT(RoboCat) cat = getCatForPlayer(playerId);
	if (cat)
	{
		cat->set_does_want_to_die(true);
	}
	// driven panda
	PT(Panda) panda = getPandaForPlayer(playerId);
	if (panda)
	{
		panda->set_does_want_to_die(true);
	}
	//was that the last client? if so, bye!
	if(GameNetworkManagerServer::get_global_ptr()->get_clients_num() == 0)
	{
		setShouldKeepRunning(false);
	}
}

void setShouldKeepRunning(bool value)
{
	value ? framework.clear_exit_flag() : framework.set_exit_flag();
}

void AddScoreBoardStateToPacket(OutputMemoryBitStream& inOutputStream)
{
	ScoreBoardManager::sInstance->Write(inOutputStream);
}
}

///
void spawnCatForPlayer(int inPlayerId)
{
	PT(RoboCat) cat = DCAST(RoboCat,
			NetworkObjectRegistry::get_global_ptr()->create_network_object(
					NetworkObject::get_class_id_int("RCAT")));
	cat->set_color(
			ScoreBoardManager::sInstance->GetEntry(inPlayerId)->GetColor());
	cat->SetPlayerId(inPlayerId);
	//gotta pick a better spawn location than this...
	cat->set_location(LPoint3f(1.f - static_cast<float>(inPlayerId), 0.f, 0.f));

}
