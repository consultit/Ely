#ifndef RoboInputState_h
#define RoboInputState_h

#include <inputState.h>

class RoboInputState: public Singleton<RoboInputState>
{
public:

	explicit RoboInputState(InputState& inputState);

	static RoboInputState* get_global_ptr()
	{
		return Singleton<RoboInputState>::GetSingletonPtr();
	}

	float GetDesiredHorizontalDelta(InputState& inputState) const
	{
		Float mDesiredRightAmount, mDesiredLeftAmount;
		inputState.get_members().get_value_float("mDesiredRightAmount",
				mDesiredRightAmount);
		inputState.get_members().get_value_float("mDesiredLeftAmount",
				mDesiredLeftAmount);
		return mDesiredRightAmount - mDesiredLeftAmount;
	}
	float GetDesiredVerticalDelta(InputState& inputState) const
	{
		Float mDesiredForwardAmount, mDesiredBackAmount;
		inputState.get_members().get_value_float("mDesiredForwardAmount",
				mDesiredForwardAmount);
		inputState.get_members().get_value_float("mDesiredBackAmount",
				mDesiredBackAmount);
		return mDesiredForwardAmount - mDesiredBackAmount;
	}
	bool IsShooting(InputState& inputState) const
	{
		Bool mIsShooting;
		inputState.get_members().get_value_bool("mIsShooting", mIsShooting);
		return mIsShooting;
	}

	void SetDesiredRightAmount(InputState& inputState, float value)
	{
		inputState.get_members().set_value_float("mDesiredRightAmount",
				value);
	}
	void SetDesiredLeftAmount(InputState& inputState, float value)
	{
		inputState.get_members().set_value_float("mDesiredLeftAmount", value);
	}
	void SetDesiredForwardAmount(InputState& inputState, float value)
	{
		inputState.get_members().set_value_float("mDesiredForwardAmount",
				value);
	}
	void SetDesiredBackAmount(InputState& inputState, float value)
	{
		inputState.get_members().set_value_float("mDesiredBackAmount", value);
	}
	void SetIsShooting(InputState& inputState, bool value)
	{
		inputState.get_members().set_value_bool("mIsShooting", value);
	}

	// P3Driver
	void set_move_forward(InputState& inputState, bool on)
	{
		inputState.get_members().set_value_bool("move_forward", on);
	}
	bool get_move_forward(InputState& inputState) const
	{
		Bool result;
		inputState.get_members().get_value_bool("move_forward", result);
		return result;
	}
	void set_move_backward(InputState& inputState, bool on)
	{
		inputState.get_members().set_value_bool("move_backward", on);
	}
	bool get_move_backward(InputState& inputState) const
	{
		Bool result;
		inputState.get_members().get_value_bool("move_backward", result);
		return result;
	}
	void set_rotate_head_left(InputState& inputState, bool on)
	{
		inputState.get_members().set_value_bool("rotate_head_left", on);
	}
	bool get_rotate_head_left(InputState& inputState) const
	{
		Bool result;
		inputState.get_members().get_value_bool("rotate_head_left", result);
		return result;
	}
	void set_rotate_head_right(InputState& inputState, bool on)
	{
		inputState.get_members().set_value_bool("rotate_head_right", on);
	}
	bool get_rotate_head_right(InputState& inputState) const
	{
		Bool result;
		inputState.get_members().get_value_bool("rotate_head_right", result);
		return result;
	}
};

#endif //RoboInputState_h
