/**
 * \file PandaClient.h
 *
 * \date 2018-06-09
 * \author consultit
 */

#ifndef PANDACLIENT_H_
#define PANDACLIENT_H_

#include "Panda.h"

class PandaClient: public Panda
{
public:
	static PT(NetworkObject) StaticCreate()
	{
		NetworkObject* go = new PandaClient();
		NodePath driverNP = DCAST(PandaClient, go)->setupDriver();
		go->get_owner_object().reparent_to(driverNP);
		go->set_owner_object(driverNP);
		return PT(NetworkObject)(go);
	}

	static void update_clbk(PT(NetworkObject) THIS);

	static void pre_read_clbk(PT(NetworkObject) THIS);
	static void post_read_clbk(PT(NetworkObject) THIS, const BitMask32& readState);

	void DoClientSidePredictionAfterReplicationForLocalPanda(
			uint32_t inReadState);
	void DoClientSidePredictionAfterReplicationForRemotePanda(
			uint32_t inReadState);

protected:
	PandaClient();
	virtual void setDriverParametersBeforeCreation();

private:

	void InterpolateClientSidePrediction(const LVecBase3f& inOldRotation,
			const LPoint3f& inOldLocation, const LVector3f& inOldVelocity,
			float inOldAngularVelocity,	bool inIsForRemotePanda);

	float mTimeLocationBecameOutOfSync;
	float mTimeVelocityBecameOutOfSync;
	float mTimeRotationBecameOutOfSync;
	float mTimeAngularVelocityBecameOutOfSync;

	LVecBase3f mOldRotation;
	LPoint3f mOldLocation;
	LVector3f mOldVelocity;
	float mOldAngularVelocity;

TYPED_OBJECT_API_DECL(PandaClient, Panda)
};

#endif // PANDACLIENT_H_
