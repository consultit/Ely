#include "YarnClient.h"

#include "../../../ely/network/source/gameNetworkManagerClient.h"
#include "RoboCat.h"

YarnClient::YarnClient() :
		Yarn()
{
	// add r/w members
	// set r/w members' dirty states
	// callbacks
#ifdef ELY_DEBUG
	set_update_callback(&YarnClient::update_clbk);
#endif //ELY_DEBUG
//	set_all_state_mask_callback(clbk);
//	set_handle_dying_callback(clbk);
//	set_pre_write_callback(clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
	set_post_read_callback(&YarnClient::post_read_clbk);
}

#ifdef ELY_DEBUG
void YarnClient::update_clbk(PT(NetworkObject) THIS)
{
	Yarn::update_clbk(THIS);
	GameObject::update_clbk(THIS);
}
#endif //ELY_DEBUG

/*fixme left only for reference
void YarnClient::read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	inInputStream.read(stateBit);
	if (stateBit)
	{
		LPoint3f location(0.f, 0.f, 0.f);
		inInputStream.read(location[0]);
		inInputStream.read(location[1]);

		LVector3f velocity(0.f, 0.f, 0.f);
		inInputStream.read(velocity[0]);
		inInputStream.read(velocity[1]);
		SetVelocity(velocity);

		//dead reckon ahead by rtt, since this was spawned a while ago!
		set_location(
				location
						+ velocity
								* GameNetworkManagerClient::get_global_ptr()->get_round_trip_time());

		LVecBase3f rotation;
		inInputStream.read(rotation);
		set_rotation(rotation);
	}

	inInputStream.read(stateBit);
	if (stateBit)
	{
		LVecBase4f color;
		inInputStream.read(color);
		set_color(color);
	}

	inInputStream.read(stateBit);
	if (stateBit)
	{
		inInputStream.read(mPlayerId, 8);
	}

}
*/

void YarnClient::post_read_clbk(PT(NetworkObject) THIS, const BitMask32& stateBit)
{
	// r/w members -> owner object synchronization
	///due to correction (dead reckon) we don't use directly Yarn::post_read_clbk(THIS, stateBit);
	PT(Yarn) downTHIS = DCAST(Yarn, THIS);
	if (stateBit.get_word() & EYRS_Pose)
	{
		// first apply corrections (dead reckon) using r/w members: (posX, posY, posZ) and mVelocity
		LPoint3f location = downTHIS->GetPosVar();
		LVector3f velocity = downTHIS->GetVelocity();

		///dead reckon ahead by rtt, since this was spawned a while ago!
		downTHIS->set_location(
				location
						+ velocity
								* GameNetworkManagerClient::get_global_ptr()->get_round_trip_time());

		// lastly update rotation
		downTHIS->set_rotation(downTHIS->GetRotVar());
	}
	if (stateBit.get_word() & EYRS_Color)
	{
		downTHIS->set_color(downTHIS->GetColVar());
	}
}

//you look like you hit a cat on the client, so disappear ( whether server registered or not
bool YarnClient::HandleCollisionWithCat(PT(RoboCat) inCat)
{
	if ((uint32_t) GetPlayerId() != inCat->GetPlayerId())
	{
		get_owner_object().hide();
	}
	return false;
}

TYPED_OBJECT_API_DEF(YarnClient)
