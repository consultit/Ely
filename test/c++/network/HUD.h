/**
 * \file Hud.h
 *
 * \date 2018-06-03
 * \author consultit
 */
#ifndef HUD_H_
#define HUD_H_

#include <string>
#include <memory.h>
#include <nodePath.h>
#include <textNode.h>

using std::unique_ptr;
using std::string;
using std::vector;

class HUD
{
public:
	virtual ~HUD();

	static void StaticInit();
	static unique_ptr<HUD> sInstance;

	void Render();

	void SetPlayerHealth(int inHealth)
	{
		mHealth = inHealth;
	}

private:

	HUD();

	void RenderBandWidth();
	void RenderRoundTripTime();
	void RenderScoreBoard();
	void RenderHealth();
	void RenderText(const string& inStr, const LPoint3f& origin,
			const LVecBase4f& inColor);

	LPoint3f mBandwidthOrigin;
	LPoint3f mRoundTripTimeOrigin;
	LPoint3f mScoreBoardOrigin;
	LVecBase3f mScoreOffset;
	LVecBase3f mHealthOffset;
	int mHealth;
	vector<NodePath> m_textNodes;
	int m_textNodeIdx, m_prevTextNodeIdx;
	int m_textNodesSize;
	float m_textScale;PT(TextFont)m_font;
	void Initialize();
};

#endif /* HUD_H_ */
