#include "MouseServer.h"
#include "RoboCat.h"
#include "ScoreBoardManager.h"

MouseServer::MouseServer() :
		Mouse()
{
/* fixme
	 get_owner_object().remove_node();
	 // add a collision node to render
	 CollisionSphere* collSolid = new CollisionSphere(0,0,0,get_collision_radius());
	 CollisionNode* collNode = new CollisionNode("collNode");
	 collNode->add_solid(collSolid);
	 NodePath collNodeNP = render.attach_new_node(collNode);
	 set_owner_object(collNodeNP);
 */
	// add r/w members
	// set r/w members' dirty states
	// callbacks
//	set_update_callback (clbk);
//	set_all_state_mask_callback(clbk);
	set_handle_dying_callback(&MouseServer::handle_dying_clbk);
//	set_pre_write_callback(clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
//	set_post_read_callback(clbk);
}

void MouseServer::handle_dying_clbk(PT(NetworkObject) THIS)
{
	GameNetworkManagerServer::get_global_ptr()->unregister_network_object(THIS);
}

bool MouseServer::HandleCollisionWithCat(PT(RoboCat) inCat)
{
	//kill yourself!
	set_does_want_to_die(true);

	ScoreBoardManager::sInstance->IncScore(inCat->GetPlayerId(), 1);

	return false;
}

TYPED_OBJECT_API_DEF(MouseServer)
