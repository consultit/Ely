/**
 * \file PandaServer.cpp
 *
 * \date 2018-06-09
 * \author consultit
 */

#include "PandaServer.h"

#include <gameControlManager.h>
#include <support/NetworkMath.h>

PandaServer::PandaServer() : Panda(),
		mControlType(EControlType::Human)
{
	// add r/w members
	// set r/w members' dirty states
	// callbacks
	set_update_callback(&PandaServer::update_clbk);
//	set_all_state_mask_callback(clbk);
	set_handle_dying_callback(&PandaServer::handle_dying_clbk);
//	set_pre_write_callback(clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
//	set_post_read_callback(clbk);
}

void PandaServer::handle_dying_clbk(PT(NetworkObject) THIS)
{
	GameNetworkManagerServer::get_global_ptr()->unregister_network_object(THIS);
}

void PandaServer::update_clbk(PT(NetworkObject) THIS)
{
	PT(PandaServer) downTHIS = DCAST(PandaServer, THIS);
	LPoint3f oldLocation = downTHIS->get_location();
	LVector3f oldVelocity = downTHIS->driver->get_linear_speed();
	LVecBase3f oldRotation = downTHIS->get_rotation();
	float oldAngularVelocity = downTHIS->driver->get_angular_speeds()[0];

	//are you controlled by a player?
	//if so, is there a move we haven't processed yet?
	if (downTHIS->mControlType == EControlType::Human)
	{
		PT(ClientProxy) client = GameNetworkManagerServer::get_global_ptr()->get_client_proxy(
				downTHIS->GetPlayerId());
		if (client)
		{
			MoveList& moveList = client->get_unprocessed_move_list();
			for (Move& unprocessedMove : moveList)
			{
				InputState& currentState =
						unprocessedMove.get_input_state();

				float deltaTime = unprocessedMove.get_delta_time();

				downTHIS->ProcessInput(deltaTime, currentState);
				downTHIS->SimulateMovement(deltaTime);

				//LOG( "Server Move Time: %3.4f deltaTime: %3.4f left rot at %3.4f", unprocessedMove.GetTimestamp(), deltaTime, get_rotation() );

			}

		}
	}
	else
	{
		//do some AI stuff
		downTHIS->SimulateMovement(Timing::get_global_ptr()->get_delta_time());
	}

	if (!Math::Is2DVectorEqual(oldLocation, downTHIS->get_location())
			|| !Math::Is2DVectorEqual(oldVelocity, downTHIS->driver->get_linear_speed())
			|| oldRotation != downTHIS->get_rotation()
			|| oldAngularVelocity != downTHIS->driver->get_angular_speeds()[0])
	{
		GameNetworkManagerServer::get_global_ptr()->set_state_dirty(THIS->get_network_id(),
				under_type(EReplicationState::Pose));
	}
}

// set parameters as strings before drivers/chasers creation
void PandaServer::setParametersBeforeCreation()
{
	Panda::setDriverParametersBeforeCreation();

	GameControlManager* ctrlMgr = GameControlManager::get_global_ptr();
	// set driver's parameters
	ctrlMgr->set_parameter_value(GameControlManager::DRIVER, "mouse_head",
			"disabled");
	ctrlMgr->set_parameter_value(GameControlManager::DRIVER, "mouse_pitch",
			"disabled");
}

TYPED_OBJECT_API_DEF(PandaServer)
