/**
 * \file Panda.h
 *
 * \date 2018-06-09
 * \author consultit
 */

#ifndef PANDA_H_
#define PANDA_H_
#include "RoboInputState.h"
#include "common.h"
#include <p3Driver.h>

template<typename E> constexpr typename std::underlying_type<E>::type under_type(E e) noexcept
{
	return static_cast<typename std::underlying_type<E>::type>(e);
}

class Panda: public GameObject
{
public:
	static constexpr uint32_t PAND = constUInt32('P', 'A', 'N', 'D');

	virtual ~Panda(){}

	enum class EReplicationState : uint32_t
	{
		Pose = 1 << 0,
		PlayerId = 1 << 1,
		Color = 1 << 2,
		AllState = Pose | PlayerId | Color,
	};

	NodePath setupDriver();

	static uint32_t get_all_state_mask_clbk(PT(NetworkObject))
	{
		return under_type(EReplicationState::AllState);
	}

	static void pre_write_clbk(PT(NetworkObject) THIS, const BitMask32& writeState);
	static void post_read_clbk(PT(NetworkObject) THIS, const BitMask32& readState);

	void ProcessInput(float inDeltaTime, InputState& inInputState);
	void SimulateMovement(float inDeltaTime);

	void SetPlayerId(int inPlayerId)
	{
		mPlayerId = inPlayerId;
	}
	int GetPlayerId() const
	{
		return static_cast<Int8>(mPlayerId);
	}

protected:
	Panda();
	// set driver's various creation parameters as string
	virtual void setDriverParametersBeforeCreation();
	PT(P3Driver)driver;

private:
	Int8 mPlayerId;
	Float driverVelX, driverVelY;
	Float driverVelH;

TYPED_OBJECT_API_DECL(Panda, GameObject)
};

#endif // PANDA_H_
