#include "Yarn.h"
#include "RoboCat.h"
#include <timing.h>
#include <utilities.h>

Yarn::Yarn() :
		GameObject(get_class_id_int("YARN")), mMuzzleSpeed(6.f), mVelocity(
				LVecBase3f::zero()), mPlayerId(0)
{
	set_owner_object(Utilities::load_model("ball"));
	set_scale(0.5);
	LVecBase3f modelDims;
	LVector3f modelDeltaCenter;
	Utilities::get_bounding_dimensions(get_owner_object(), modelDims, modelDeltaCenter);
	set_collision_radius(modelDims.get_y() / 2.0);
	set_rotation(0.0f);
	set_color(Colors::White);
	// add r/w members
	DataVariables& members = get_members();
	members.add_variable_int8_t("mPlayerId", &this->mPlayerId);
	members.add_variable_lvector3f("mVelocity", &this->mVelocity);
	// set r/w members' dirty states
	members.set_dirty_state("posX", EYRS_Pose);
	members.set_dirty_state("posY", EYRS_Pose);
	members.set_dirty_state("posZ", 0);
	members.set_dirty_state("rotH", EYRS_Pose);
	members.set_dirty_state("rotP", 0);
	members.set_dirty_state("rotR", 0);
	members.set_dirty_state("color", EYRS_Color);
	members.set_dirty_state("mPlayerId", EYRS_PlayerId);
	members.set_dirty_state("mVelocity", EYRS_Pose);
	// callbacks
	set_update_callback(&Yarn::update_clbk);
	set_all_state_mask_callback(&Yarn::get_all_state_mask_clbk);
//	set_handle_dying_callback (clbk);
	set_pre_write_callback(&Yarn::pre_write_clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
	set_post_read_callback(&Yarn::post_read_clbk);
}

/*fixme left only for reference
uint32_t Yarn::write(OutputMemoryBitStream& inOutputStream,
		uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & EYRS_Pose)
	{
		inOutputStream.write((bool) true);

		LPoint3f location = get_location();
		inOutputStream.write(location.get_x());
		inOutputStream.write(location.get_y());

		LVector3f velocity = GetVelocity();
		inOutputStream.write(velocity.get_x());
		inOutputStream.write(velocity.get_y());

		inOutputStream.write(get_rotation());

		writtenState |= EYRS_Pose;
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	if (inDirtyState & EYRS_Color)
	{
		inOutputStream.write((bool) true);

		inOutputStream.write(get_color());

		writtenState |= EYRS_Color;
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	if (inDirtyState & EYRS_PlayerId)
	{
		inOutputStream.write((bool) true);

		inOutputStream.write(mPlayerId, 8);

		writtenState |= EYRS_PlayerId;
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	return writtenState;
}
*/

bool Yarn::HandleCollisionWithCat(PT(RoboCat) inCat)
{
	(void) inCat;

	//you hit a cat, so look like you hit a cat

	return false;
}

void Yarn::InitFromShooter(PT(RoboCat) inShooter)
{
	set_color(inShooter->get_color());
	SetPlayerId(inShooter->GetPlayerId());

	LVector3f forward = inShooter->GetForwardVector();
	SetVelocity(inShooter->GetVelocity() + forward * mMuzzleSpeed);
	set_location(inShooter->get_location() /* + forward * 0.55f */);

	set_rotation(inShooter->get_rotation());
}

void Yarn::update_clbk(PT(NetworkObject) THIS)
{

	PT(Yarn) downTHIS = DCAST(Yarn, THIS);
	float deltaTime = Timing::get_global_ptr()->get_delta_time();

	downTHIS->set_location(downTHIS->get_location() + downTHIS->mVelocity * deltaTime);

	//we'll let the cats handle the collisions
}

void Yarn::pre_write_clbk(PT(NetworkObject) THIS, const BitMask32& writeState)
{
	// owner object -> r/w members synchronization
	PT(Yarn) downTHIS = DCAST(Yarn, THIS);
	if (writeState.get_word() & EYRS_Pose)
	{
		downTHIS->SetPosVar(downTHIS->get_location());
		downTHIS->SetRotVar(downTHIS->get_rotation());
	}
	if (writeState.get_word() & EYRS_Color)
	{
		downTHIS->SetColVar(downTHIS->get_color());
	}
}

void Yarn::post_read_clbk(PT(NetworkObject)THIS, const BitMask32& readState)
{
	// r/w members -> owner object synchronization
	PT(Yarn) downTHIS = DCAST(Yarn, THIS);
	if (readState.get_word() & EYRS_Pose)
	{
		downTHIS->set_location(downTHIS->GetPosVar());
		downTHIS->set_rotation(downTHIS->GetRotVar());
	}
	if (readState.get_word() & EYRS_Color)
	{
		downTHIS->set_color(downTHIS->GetColVar());
	}
}

TYPED_OBJECT_API_DEF(Yarn)
