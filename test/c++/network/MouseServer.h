#ifndef MouseServer_h
#define MouseServer_h

#include "../../../ely/network/source/gameNetworkManagerServer.h"
#include "Mouse.h"

class MouseServer: public Mouse
{
public:
	static PT(NetworkObject) StaticCreate()
	{
		NetworkObject* go = new MouseServer();
		go->get_owner_object().reparent_to(render);
		return GameNetworkManagerServer::get_global_ptr()->register_and_return(go);
	}
	static void handle_dying_clbk(PT(NetworkObject) THIS);
	virtual bool HandleCollisionWithCat(PT(RoboCat) inCat) override;

protected:
	MouseServer();

TYPED_OBJECT_API_DECL(MouseServer, Mouse)
};

#endif //MouseServer_h
