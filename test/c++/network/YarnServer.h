#ifndef YarnServer_h
#define YarnServer_h

#include "../../../ely/network/source/gameNetworkManagerServer.h"
#include "Yarn.h"
#include "RoboCatServer.h"

class YarnServer: public Yarn
{
public:
	static PT(NetworkObject) StaticCreate()
	{
		NetworkObject* go = new YarnServer();
		go->get_owner_object().reparent_to(render);
		return GameNetworkManagerServer::get_global_ptr()->register_and_return(go);
	}

	static void update_clbk(PT(NetworkObject) THIS);

	static void handle_dying_clbk(PT(NetworkObject) THIS);

	virtual bool HandleCollisionWithCat(PT(RoboCat) inCat) override;

protected:
	YarnServer();

private:
	float mTimeToDie;

TYPED_OBJECT_API_DECL(YarnServer, Yarn)
};

#endif //YarnServer_h
