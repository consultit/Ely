#ifndef RoboCat_h
#define RoboCat_h

#include "common.h"
#include "RoboInputState.h"

class RoboCat: public GameObject
{
public:
	static constexpr uint32_t RCAT = constUInt32('R', 'C', 'A', 'T');

	enum ECatReplicationState
	{
		ECRS_Pose = 1 << 0,
		ECRS_Color = 1 << 1,
		ECRS_PlayerId = 1 << 2,
		ECRS_Health = 1 << 3,

		ECRS_AllState = ECRS_Pose | ECRS_Color | ECRS_PlayerId | ECRS_Health
	};

	static uint32_t get_all_state_mask_clbk(PT(NetworkObject))
	{
		return ECRS_AllState;
	}

	static void pre_write_clbk(PT(NetworkObject) THIS, const BitMask32& writeState);
	static void post_read_clbk(PT(NetworkObject) THIS, const BitMask32& readState);

	void ProcessInput(float inDeltaTime, InputState& inInputState);
	void SimulateMovement(float inDeltaTime);

	void ProcessCollisions();
	void ProcessCollisionsWithScreenWalls();

	void SetPlayerId(int inPlayerId)
	{
		mPlayerId = inPlayerId;
	}
	int GetPlayerId() const
	{
		return static_cast<Int8>(mPlayerId);
	}

	void SetVelocity(const LVector3f& inVelocity)
	{
		mVelocity = inVelocity;
	}
	LVector3f GetVelocity() const
	{
		return mVelocity;
	}

	LVector3f GetForwardVector() const
	{
		//should we cache this when you turn?
		return get_owner_object().get_parent().get_relative_vector(get_owner_object(),
				-LVector3f::forward());
	}

	void SetHealth(int value)
	{
		mHealth = value;
	}
	int GetHealth() const
	{
		return static_cast<Int8>(mHealth);
	}

/*fixme optimization
void SetThrustDir(float value)
{
	mThrustDir = value;
}

float GetThrustDir() const
{
	return mThrustDir;
}

void SetThrustDirPos(bool value)
{
	mThrustDirPos = value;
}

bool GetThrustDirPos() const
{
	return static_cast<Bool>(mThrustDirPos);
}

void SetThrustDirNeg(bool value)
{
	mThrustDirNeg = value;
}

bool GetThrustDirNeg() const
{
	return static_cast<Bool>(mThrustDirNeg);
}
*/

protected:
	RoboCat();

private:

	void AdjustVelocityByThrust(float inDeltaTime);

	LVector3f mVelocity;

	float mMaxLinearSpeed;
	float mMaxRotationSpeed;

	//bounce fraction when hitting various things
	float mWallRestitution;
	float mCatRestitution;

	Int8 mPlayerId;

protected:

	///move down here for padding reasons...

	float mLastMoveTimestamp;

	Float mThrustDir;
/*fixme optimization
	float mThrustDir;
	// (!mThrustDirPos && !mThrustDirNeg) == true -> mThrustDir == 0.0
	Bool mThrustDirPos, mThrustDirNeg;
*/

	Int8 mHealth;
	bool mIsShooting;

TYPED_OBJECT_API_DECL(RoboCat, GameObject)
};

#endif //RoboCat_h
