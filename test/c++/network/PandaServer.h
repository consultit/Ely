/**
 * \file PandaServer.h
 *
 * \date 2018-06-09
 * \author consultit
 */

#ifndef PANDASERVER_H_
#define PANDASERVER_H_

#include "../../../ely/network/source/gameNetworkManagerServer.h"
#include "Panda.h"

enum class EControlType
{
	Human, AI
};

class PandaServer: public Panda
{
public:

	static PT(NetworkObject) StaticCreate()
	{
		NetworkObject* go = new PandaServer();
		NodePath driverNP = DCAST(PandaServer, go)->setupDriver();
		go->get_owner_object().reparent_to(driverNP);
		go->set_owner_object(driverNP);
		return GameNetworkManagerServer::get_global_ptr()->register_and_return(go);
	}
	static void handle_dying_clbk(PT(NetworkObject) THIS);

	static void update_clbk(PT(NetworkObject) THIS);

	void SetCatControlType(EControlType inControlType)
	{
		mControlType = inControlType;
	}

protected:
	PandaServer();
	virtual void setParametersBeforeCreation();

private:

	EControlType mControlType;

TYPED_OBJECT_API_DECL(PandaServer, Panda)
};

#endif // PANDASERVER_H_
