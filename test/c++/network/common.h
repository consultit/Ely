/**
 * \file common.h
 *
 * \date 2018-06-26
 * \author consultit
 */
#ifndef common_h
#define common_h

#include <nodePath.h>
#include <networkObject.h>
#include <commonMacros.h>
#include "../../../ely/network/source/gameNetworkManager.h"
#ifdef ELY_DEBUG
#include <textNode.h>
#endif //ELY_DEBUG

extern NodePath render;

using namespace std;

//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom
constexpr float HALF_WORLD_HEIGHT = 12.0;
constexpr float HALF_WORLD_WIDTH = 15.0;
constexpr float WORLD_STEP = 1.0;
const LPoint3f WORLD_MIN(-HALF_WORLD_WIDTH, -HALF_WORLD_HEIGHT, 0.0);
const LPoint3f WORLD_MAX(HALF_WORLD_WIDTH, HALF_WORLD_HEIGHT, 0.0);

/**
 * GameObject class.
 */
class GameObject: public NetworkObject
{
public:

	GameObject(uint32_t inCode);
#ifdef ELY_DEBUG
	~GameObject();
	static void update_clbk(PT(NetworkObject) THIS);
#endif //ELY_DEBUG
	void set_rotation(const LVecBase3f& inRotation);
	LVecBase3f get_rotation() const;
	void set_location(const LPoint3f& inLocation);
	LPoint3f get_location() const;
	void set_scale(const LVecBase3f& inScale);
	LVecBase3f get_scale() const;
	void set_color(const LVecBase4f& inColor);
	LVecBase4f get_color() const;
	void set_collision_radius(float inRadius);
	float get_collision_radius() const;
	void SetPosVar(const LPoint3f& pos);
	LPoint3f GetPosVar() const;
	void SetRotVar(const LVecBase3f& pos);
	LVecBase3f GetRotVar() const;
	void SetColVar(const LVecBase4f& pos);
	LVecBase4f GetColVar() const;

protected:
	Float posX, posY, posZ;
	Float rotH, rotP, rotR;
	LVecBase4f color;
	float mCollisionRadius;
#ifdef ELY_DEBUG
	PT(TextNode) textNode;
	NodePath text;
#endif //ELY_DEBUG

TYPED_OBJECT_API_DECL(GameObject, NetworkObject)
};

#endif /* common_h */
