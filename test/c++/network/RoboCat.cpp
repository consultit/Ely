#include "RoboCat.h"

#include "Mouse.h"
#include "Yarn.h"
#include <networkWorld.h>
#include <utilities.h>

RoboCat::RoboCat() :
		GameObject(get_class_id_int("RCAT")), mMaxRotationSpeed(25.f), mMaxLinearSpeed(
				100.f), mVelocity(LVector3f::zero()), mWallRestitution(0.1f), mCatRestitution(
				0.1f), mThrustDir(0.f), mPlayerId(0), mIsShooting(false), mHealth(
				10), mLastMoveTimestamp(0.f)/*, mThrustDirPos(false), mThrustDirNeg(
				false) fixme optimization*/
{
	set_owner_object(Utilities::load_model("ralph"));
	set_scale(0.5);
	LVecBase3f modelDims;
	LVector3f modelDeltaCenter;
	Utilities::get_bounding_dimensions(get_owner_object(), modelDims, modelDeltaCenter);
	set_collision_radius(modelDims.get_y() / 2.0);
	set_rotation(LVecBase3f::zero());
	set_color(Colors::White);
	// add r/w members
	DataVariables& members = get_members();
	members.add_variable_int8_t("mPlayerId", &this->mPlayerId);
	members.add_variable_lvector3f("mVelocity", &this->mVelocity);
	members.add_variable_float("mThrustDir", &this->mThrustDir);
	members.add_variable_int8_t("mHealth", &this->mHealth);
/*fixme optimization
    members.add_variable_bool("mThrustDirPos", &this->mThrustDirPos);
    members.add_variable_bool("mThrustDirNeg", &this->mThrustDirNeg);
*/
	// set r/w members' dirty states
	members.set_dirty_state("posX", ECRS_Pose);
	members.set_dirty_state("posY", ECRS_Pose);
	members.set_dirty_state("posZ", 0);
	members.set_dirty_state("rotH", ECRS_Pose);
	members.set_dirty_state("rotP", 0);
	members.set_dirty_state("rotR", 0);
	members.set_dirty_state("color", ECRS_Color);
	members.set_dirty_state("mPlayerId", ECRS_PlayerId);
	members.set_dirty_state("mVelocity", ECRS_Pose);
	members.set_dirty_state("mThrustDir", 0xFFFFFFFF); //already constructed with BitMsk32::all_on()
/*fixme optimization
	members.set_dirty_state("mThrustDirPos", 0xFFFFFFFF); //already constructed with BitMsk32::all_on()
	members.set_dirty_state("mThrustDirNeg", 0xFFFFFFFF); //already constructed with BitMsk32::all_on()
*/
	members.set_dirty_state("mHealth", ECRS_Health);
	// callbacks
//	set_update_callback (clbk);
	set_all_state_mask_callback(&RoboCat::get_all_state_mask_clbk);
//	set_handle_dying_callback(clbk);
	set_pre_write_callback(&RoboCat::pre_write_clbk);
//	set_post_write_callback(clbk);
//	set_pre_read_callback(clbk);
	set_post_read_callback(&RoboCat::post_read_clbk);
}

void RoboCat::pre_write_clbk(PT(NetworkObject) THIS, const BitMask32& writeState)
{
	// owner object -> r/w members synchronization
	PT(RoboCat) downTHIS = DCAST(RoboCat, THIS);
	if (writeState.get_word() & ECRS_Pose)
	{
		downTHIS->SetPosVar(downTHIS->get_location());
		downTHIS->SetRotVar(downTHIS->get_rotation());
	}
	if (writeState.get_word() & ECRS_Color)
	{
		downTHIS->SetColVar(downTHIS->get_color());
	}
/*fixme optimization
	// always write mThrustDir- it's just two bits
	// never happens that (mThrustDirPos && mThrustDirNeg) == true
	PT(RoboCat) downTHIS = DCAST(RoboCat, THIS);
	if (downTHIS->GetThrustDir() > 0.0)
	{
		downTHIS->SetThrustDirPos(true);
		downTHIS->SetThrustDirNeg(false);
	}
	else if (downTHIS->GetThrustDir() > 0.0)
	{
		downTHIS->SetThrustDirPos(false);
		downTHIS->SetThrustDirNeg(true);
	}
	else
	{
		downTHIS->SetThrustDirPos(false);
		downTHIS->SetThrustDirNeg(false);
	}
*/
}

void RoboCat::post_read_clbk(PT(NetworkObject)THIS, const BitMask32& readState)
{
	// r/w members -> owner object synchronization
	PT(RoboCat) downTHIS = DCAST(RoboCat, THIS);
	if (readState.get_word() & ECRS_Pose)
	{
		downTHIS->set_location(downTHIS->GetPosVar());
		downTHIS->set_rotation(downTHIS->GetRotVar());
	}
	if (readState.get_word() & ECRS_Color)
	{
		downTHIS->set_color(downTHIS->GetColVar());
	}
/*fixme optimization
	// always adjust mThrustDir
	// never happens that (mThrustDirPos && mThrustDirNeg) == true
	if (downTHIS->GetThrustDirPos())
	{
		downTHIS->SetThrustDir(1.0);
	}
	else if (downTHIS->GetThrustDirNeg())
	{
		downTHIS->SetThrustDir(-1.0);
	}
	else
	{
		downTHIS->SetThrustDir(0.0);
	}
*/
}

void RoboCat::ProcessInput(float inDeltaTime, InputState& inInputState)
{
	//process our input....

	RoboInputState* state = RoboInputState::get_global_ptr();
	//turning...
	float newRotation = get_rotation().get_x()
			+ state->GetDesiredHorizontalDelta(inInputState) * mMaxRotationSpeed
					* inDeltaTime;
	set_rotation(
			LVecBase3f(newRotation, get_rotation().get_y(),
					get_rotation().get_z()));

	//moving...
	float inputForwardDelta = state->GetDesiredVerticalDelta(inInputState);
	mThrustDir = inputForwardDelta;

	mIsShooting = state->IsShooting(inInputState);

}

void RoboCat::AdjustVelocityByThrust(float inDeltaTime)
{
	//just set the velocity based on the thrust direction -- no thrust will lead to 0 velocity
	//simulating acceleration makes the client prediction a bit more complex
	LVector3f forwardVector = GetForwardVector();
	mVelocity = forwardVector * (mThrustDir * inDeltaTime * mMaxLinearSpeed);
}

void RoboCat::SimulateMovement(float inDeltaTime)
{
	//simulate us...
	AdjustVelocityByThrust(inDeltaTime);

	set_location(get_location() + mVelocity * inDeltaTime);

	ProcessCollisions();
}

void RoboCat::ProcessCollisions()
{
	//right now just bounce off the sides..
	ProcessCollisionsWithScreenWalls();

	float sourceRadius = get_collision_radius();
	LVector3f sourceLocation = get_location();

	//now let's iterate through the world and see what we hit...
	//note: since there's a small number of objects in our game, this is fine.
	//but in a real game, brute-force checking collisions against every other object is not efficient.
	//it would be preferable to use a quad tree or some other structure to minimize the
	//number of collisions that need to be tested.
	pvector<PT(NetworkObject)>&& netObjs =
			NetworkWorld::get_global_ptr()->get_network_objects();
	for (auto goIt = netObjs.begin(), end =	netObjs.end(); goIt != end; ++goIt)
	{
		PT(GameObject) target = DCAST(GameObject, *goIt);
		if (target != this && !target->get_does_want_to_die())
		{
			//simple collision test for spheres- are the radii summed less than the distance?
			LPoint3f targetLocation = target->get_location();
			float targetRadius = target->get_collision_radius();

			LVector3f delta = targetLocation - sourceLocation;
			float distSq = delta.length_squared();
			float collisionDist = (sourceRadius + targetRadius);
			if (distSq < (collisionDist * collisionDist))
			{
				//first, tell the other guy there was a collision with a cat, so it can do something...

				bool collision;
				switch (target->get_class_id())
				{
				case Mouse::MOUS:
					collision =
							DCAST(Mouse, target)->HandleCollisionWithCat(
									this);
					break;
				case Yarn::YARN:
					collision =
							DCAST(Yarn, target)->HandleCollisionWithCat(
									this);
					break;
				default:
					break;
				}
				if (collision)
				{
					//okay, you hit something!
					//so, project your location far enough that you're not colliding
					LVector3f dirToTarget = delta;
					dirToTarget.normalize();
					LVector3f acceptableDeltaFromSourceToTarget = dirToTarget
							* collisionDist;
					//important note- we only move this cat. the other cat can take care of moving itself
					set_location(
							targetLocation - acceptableDeltaFromSourceToTarget);

					LVector3f relVel = mVelocity;

					//if other object is a cat, it might have velocity, so there might be relative velocity...
					if (target->get_class_id() == RCAT)
					{
						relVel -= DCAST(RoboCat, target)->mVelocity;
					}

					//got vel with dir between objects to figure out if they're moving towards each other
					//and if so, the magnitude of the impulse ( since they're both just balls )
					float relVelDotDir = relVel.dot(dirToTarget);

					if (relVelDotDir > 0.f)
					{
						LVector3f impulse = relVelDotDir * dirToTarget;

						if (target->get_class_id() == RCAT)
						{
							mVelocity -= impulse;
							mVelocity *= mCatRestitution;
						}
						else
						{
							mVelocity -= impulse * 2.f;
							mVelocity *= mWallRestitution;
						}

					}
				}
			}
		}
	}

}

void RoboCat::ProcessCollisionsWithScreenWalls()
{
	LVector3f location = get_location();
	float x = location.get_x();
	float y = location.get_y();

	float vx = mVelocity.get_x();
	float vy = mVelocity.get_y();

	float radius = get_collision_radius();

	//if the cat collides against a wall, the quick solution is to push it off
	if ((y + radius) >= HALF_WORLD_HEIGHT && vy > 0)
	{
		mVelocity[1] = -vy * mWallRestitution;
		location[1] = HALF_WORLD_HEIGHT - radius;
		set_location(location);
	}
	else if (y <= (-HALF_WORLD_HEIGHT - radius) && vy < 0)
	{
		mVelocity[1] = -vy * mWallRestitution;
		location[1] = -HALF_WORLD_HEIGHT - radius;
		set_location(location);
	}

	if ((x + radius) >= HALF_WORLD_WIDTH && vx > 0)
	{
		mVelocity[0] = -vx * mWallRestitution;
		location[0] = HALF_WORLD_WIDTH - radius;
		set_location(location);
	}
	else if (x <= (-HALF_WORLD_WIDTH - radius) && vx < 0)
	{
		mVelocity[0] = -vx * mWallRestitution;
		location[0] = -HALF_WORLD_WIDTH - radius;
		set_location(location);
	}
}

/*fixme left only for reference
uint32_t RoboCat::write(OutputMemoryBitStream& inOutputStream,
		uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & ECRS_PlayerId)
	{
		inOutputStream.write((bool) true);
		inOutputStream.write(GetPlayerId());

		writtenState |= ECRS_PlayerId;
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	if (inDirtyState & ECRS_Pose)
	{
		inOutputStream.write((bool) true);

		LVector3f velocity = mVelocity;
		inOutputStream.write(velocity.get_x());
		inOutputStream.write(velocity.get_y());

		LPoint3f location = get_location();
		inOutputStream.write(location.get_x());
		inOutputStream.write(location.get_y());

		inOutputStream.write(get_rotation());

		writtenState |= ECRS_Pose;
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	//always write mThrustDir- it's just two bits
	if (mThrustDir != 0.f)
	{
		inOutputStream.write(true);
		inOutputStream.write(mThrustDir > 0.f);
	}
	else
	{
		inOutputStream.write(false);
	}

	if (inDirtyState & ECRS_Color)
	{
		inOutputStream.write((bool) true);
		inOutputStream.write(get_color());

		writtenState |= ECRS_Color;
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	if (inDirtyState & ECRS_Health)
	{
		inOutputStream.write((bool) true);
		inOutputStream.write(mHealth, 4);

		writtenState |= ECRS_Health;
	}
	else
	{
		inOutputStream.write((bool) false);
	}

	return writtenState;

}
*/

TYPED_OBJECT_API_DEF(RoboCat)
