/**
 * \file common_physics.cpp
 *
 * \date 2018-02-25
 * \author consultit
 */

#include "common_physics.h"
#include <load_prc_file.h>
#include <texturePool.h>
#include <geoMipTerrain.h>
#include <texturePool.h>
#include <cardMaker.h>
#include <geomVertexArrayFormat.h>
#include <geomVertexFormat.h>
#include <geomVertexWriter.h>
#include <geomTriangles.h>
#include <auto_bind.h>

/// global data definitions
extern string dataDir;
PandaFramework framework;
WindowFramework *window = NULL;
CollideMask groupMask = BitMask32(0x10);
CollideMask collideMask = BitMask32(0x10);
AsyncTask *updateTask = NULL;
NodePath sceneNP;
GamePhysicsManager *physicsMgr;
ClockObject *globalClock = nullptr;
// bam file
string bamFileName("physics.boo");
// rope
PT(BT3SoftBody) ropeGeomBody = nullptr;
// player
vector<vector<PT(AnimControl)> > playerAnimCtls;
// physics multi-threaded
bool physicsMT = false;

/// static data definitions
// models and animations
static string modelFile[] =
{ "eve.egg", "ralph.egg", "sparrow.egg", "ball.egg", "red_car.egg",
		"two-pills.egg", "vehicle.egg", "wheel.egg", "misc/rgbCube" };
static string modelAnimFiles[][2] =
{
{ "eve-walk.egg", "eve-run.egg" },
{ "ralph-walk.egg", "ralph-run.egg" },
{ "sparrow-flying.egg", "sparrow-flying2.egg" },
{ "", "" },
{ "red_car-anim.egg", "red_car-anim2.egg" },
{ "", "" },
{ "", "" },
{ "", "" },
{ "", "" }, };
static const float animRateFactor[2] =
{ 0.6, 0.175 };
// debug flag
static bool toggleDebugFlag = false;

// print creation parameters
void printCreationParameters()
{
	GamePhysicsManager *physicsMgr = GamePhysicsManager::get_global_ptr();
	//
	ValueList_string valueList = physicsMgr->get_parameter_name_list(
			GamePhysicsManager::RIGIDBODY);
	cout << endl << "BT3RigidBody creation parameters:" << endl;
	for (int i = 0; i < valueList.get_num_values(); ++i)
	{
		cout << "\t" << valueList[i] << " = "
				<< physicsMgr->get_parameter_value(
						GamePhysicsManager::RIGIDBODY, valueList[i]) << endl;
	}
	//
	valueList = physicsMgr->get_parameter_name_list(
			GamePhysicsManager::SOFTBODY);
	cout << endl << "BT3SoftBody creation parameters:" << endl;
	for (int i = 0; i < valueList.get_num_values(); ++i)
	{
		cout << "\t" << valueList[i] << " = "
				<< physicsMgr->get_parameter_value(GamePhysicsManager::SOFTBODY,
						valueList[i]) << endl;
	}
	//
	valueList = physicsMgr->get_parameter_name_list(GamePhysicsManager::GHOST);
	cout << endl << "BT3Ghost creation parameters:" << endl;
	for (int i = 0; i < valueList.get_num_values(); ++i)
	{
		cout << "\t" << valueList[i] << " = "
				<< physicsMgr->get_parameter_value(GamePhysicsManager::GHOST,
						valueList[i]) << endl;
	}
	//
	valueList = physicsMgr->get_parameter_name_list(
			GamePhysicsManager::CONSTRAINT);
	cout << endl << "BT3Constraint creation parameters:" << endl;
	for (int i = 0; i < valueList.get_num_values(); ++i)
	{
		cout << "\t" << valueList[i] << " = "
				<< physicsMgr->get_parameter_value(
						GamePhysicsManager::CONSTRAINT, valueList[i]) << endl;
	}
	//
	valueList = physicsMgr->get_parameter_name_list(
			GamePhysicsManager::VEHICLE);
	cout << endl << "BT3Vehicle creation parameters:" << endl;
	for (int i = 0; i < valueList.get_num_values(); ++i)
	{
		cout << "\t" << valueList[i] << " = "
				<< physicsMgr->get_parameter_value(GamePhysicsManager::VEHICLE,
						valueList[i]) << endl;
	}
	//
	valueList = physicsMgr->get_parameter_name_list(
			GamePhysicsManager::CHARACTERCONTROLLER);
	cout << endl << "BT3CharacterController creation parameters:" << endl;
	for (int i = 0; i < valueList.get_num_values(); ++i)
	{
		cout << "\t" << valueList[i] << " = "
				<< physicsMgr->get_parameter_value(
						GamePhysicsManager::CHARACTERCONTROLLER, valueList[i])
				<< endl;
	}
}

// set parameters as strings before objects creation
void setParametersBeforeCreation(GamePhysicsManager::BT3PhysicType type,
		const string &objectName, const string &upAxis)
{
	GamePhysicsManager *physicsMgr = GamePhysicsManager::get_global_ptr();
	if (type == GamePhysicsManager::RIGIDBODY)
	{
		// set rigid_body's parameters
		physicsMgr->set_parameter_value(type, "object", objectName);
		physicsMgr->set_parameter_value(type, "mass", "10.0");
		physicsMgr->set_parameter_value(type, "group_mask", "0x10");
		physicsMgr->set_parameter_value(type, "collide_mask", "0x10");
		physicsMgr->set_parameter_value(type, "up_axis", upAxis);
	}

	// set soft_body's parameters
	if (type == GamePhysicsManager::SOFTBODY)
	{
		physicsMgr->set_parameter_value(type, "object", objectName);
		physicsMgr->set_parameter_value(type, "group_mask", "0x10");
		physicsMgr->set_parameter_value(type, "collide_mask", "0x10");
		physicsMgr->set_parameter_value(type, "total_mass", "20.0");
		physicsMgr->set_parameter_value(type, "from_faces", "false");
	}

	// set ghost's parameters
	if (type == GamePhysicsManager::GHOST)
	{
		physicsMgr->set_parameter_value(type, "object", objectName);
		physicsMgr->set_parameter_value(type, "group_mask", "0x10");
		physicsMgr->set_parameter_value(type, "collide_mask", "0x10");
		physicsMgr->set_parameter_value(type, "up_axis", upAxis);
	}
	// set constraint's parameters
	if (type == GamePhysicsManager::CONSTRAINT)
	{
		physicsMgr->set_parameter_value(type, "", "");
	}
	// set vehicle parameters
	if (type == GamePhysicsManager::VEHICLE)
	{
		physicsMgr->set_parameter_value(type, "", "");
	}
	// set character controller's parameters
	if (type == GamePhysicsManager::CHARACTERCONTROLLER)
	{
		physicsMgr->set_parameter_value(type, "", "");
	}
	//
	printCreationParameters();
}

// libp3tools typed objects init
void libp3toolsTypedObjectInit()
{
	Pair_bool_float::init_type();
	Pair_LPoint3f_int::init_type();
	Pair_LVector3f_ValueList_float::init_type();
	ValueList_string::init_type();
	ValueList_NodePath::init_type();
	ValueList_int::init_type();
	ValueList_float::init_type();
	ValueList_LVecBase3f::init_type();
	ValueList_LVector3f::init_type();
	ValueList_LPoint3f::init_type();
	ValueList_Pair_LPoint3f_int::init_type();
}

// start base framework
string startFramework(int argc, char *argv[], const string &msg)
{
	// parse arguments
	vector<string> dirs;
	string bamFile;
	{
		// parse arguments
		int c;
		while ((c = getopt(argc, argv, "pd:")) != -1)
		{
			switch (c)
			{
			case 'p':
			{
				physicsMT = true;
			}
				break;
			case 'd':
			{
				dirs.emplace_back(optarg);
			}
				break;
			case '?':
				if (optopt == 'd')
					fprintf(stderr, "Option -%c requires an argument.\n",
							optopt);
				else if (isprint(optopt))
					fprintf(stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf(stderr, "Unknown option character `\\x%x'.\n",
							optopt);
				break;
			default:
				abort();
			}
		}
		for (c = optind; c < argc; c++)
		{
			// bamfile is the first non-option argument
			bamFile = string(argv[c]);
			break;
		}
	}

	// Load your application's configuration
	load_prc_file_data("", "model-path " + dataDir);
	for (auto dir : dirs)
	{
		load_prc_file_data("", "model-path " + dir);
	}
	load_prc_file_data("", "win-size 1024 768");
	load_prc_file_data("", "show-frame-rate-meter #t");
	load_prc_file_data("", "sync-video #t");
	// Setup your application
	framework.open_framework(argc, argv);
	framework.set_window_title("p3physics: " + msg);
	window = framework.open_window();
	if (window != (WindowFramework*) nullptr)
	{
		cout << "Opened the window successfully!\n";
		window->enable_keyboard();
		window->setup_trackball();
	}

	/// typed object init; not needed if you build inside panda source tree
	BT3RigidBody::init_type();
	BT3SoftBody::init_type();
	BT3Ghost::init_type();
	BT3Constraint::init_type();
	BT3Vehicle::init_type();
	BT3CharacterController::init_type();
	GamePhysicsManager::init_type();
	BT3RigidBody::register_with_read_factory();
	BT3SoftBody::register_with_read_factory();
	BT3Ghost::register_with_read_factory();
	BT3Constraint::register_with_read_factory();
	BT3Vehicle::register_with_read_factory();
	BT3CharacterController::register_with_read_factory();
	libp3toolsTypedObjectInit();
	///

	//common callbacks
	return bamFile;
}

// load plane stuff centered at (0,0,0)
NodePath loadPlane(const string &name, float width, float depth,
		GamePhysicsManager::BT3UpAxis upAxis, const string &texture)
{
	// Vertex Format
	PT(GeomVertexArrayFormat) arrayFormat = new GeomVertexArrayFormat();
	arrayFormat->add_column(InternalName::make("vertex"), 3, Geom::NT_float32,
			Geom::C_point);
	arrayFormat->add_column(InternalName::make("normal"), 3, Geom::NT_float32,
			Geom::C_vector);
	arrayFormat->add_column(InternalName::make("texcoord"), 2, Geom::NT_float32,
			Geom::C_texcoord);
	PT(GeomVertexFormat) vertexFormat = new GeomVertexFormat();
	vertexFormat->add_array(arrayFormat);
	// Pre-defined vertex formats
//    CPT(GeomVertexFormat) vertexFormatAdded = GeomVertexFormat::get_v3n3t2();
	CPT(GeomVertexFormat) vertexFormatAdded =
			GeomVertexFormat::register_format(vertexFormat);
	// Vertex Data
	PT(GeomVertexData) vertexData = new GeomVertexData("plane",
			vertexFormatAdded, Geom::UH_static);
	GeomVertexWriter vertex(vertexData, "vertex");
	GeomVertexWriter normal(vertexData, "normal");
	GeomVertexWriter texcoord(vertexData, "texcoord");
	// compute coords and normal according to up axis
	// 3------2      ^y           ^z           ^x
	// |      |      |            |            |
	// |      | +d   | Z_up   OR  | X_up   OR  | Y_up
	// |      |      |            |            |
	// 0------1      ------>x     ------>y     ------>z
	//    +w
	float w = abs(width) / 0.5;
	float d = abs(depth) / 0.5;
	// default: Z_up
	LVector3f n(0.0, 0.0, 1.0);
	LPoint3f v0(-w, -d, 0.0);
	LPoint3f v1(w, -d, 0.0);
	LPoint3f v2(w, d, 0.0);
	LPoint3f v3(-w, d, 0.0);
	if (upAxis == GamePhysicsManager::X_up)
	{
		n = LVector3f(1.0, 0.0, 0.0);
		v0 = LPoint3f(0.0, -w, -d);
		v1 = LPoint3f(0.0, w, -d);
		v2 = LPoint3f(0.0, w, d);
		v3 = LPoint3f(0.0, -w, d);
	}
	else if (upAxis == GamePhysicsManager::Y_up)
	{
		n = LVector3f(0.0, 1.0, 0.0);
		v0 = LPoint3f(-d, 0.0, -w);
		v1 = LPoint3f(d, 0.0, -w);
		v2 = LPoint3f(d, 0.0, w);
		v3 = LPoint3f(-d, 0.0, w);
	}
	// normalize
	n.normalize();
	// fill-up vertex data (plane)
	// vertex 0
	vertex.add_data3f(v0);
	normal.add_data3f(n);
	texcoord.add_data2f(0.0, 0.0);
	// vertex 1
	vertex.add_data3f(v1);
	normal.add_data3f(n);
	texcoord.add_data2f(1.0, 0.0);
	// vertex 2
	vertex.add_data3f(v2);
	normal.add_data3f(n);
	texcoord.add_data2f(1.0, 1.0);
	// vertex 3
	vertex.add_data3f(v3);
	normal.add_data3f(n);
	texcoord.add_data2f(0.0, 1.0);
	// Creating the GeomPrimitive objects for plane
	PT(GeomTriangles) planeTriangles = new GeomTriangles(Geom::UH_static);
	// lower triangle
	planeTriangles->add_vertices(0, 1, 3);
	// higher triangle
	planeTriangles->add_vertices(2, 3, 1);
	// Putting your new geometry in the scene graph
	PT(Geom) planeGeom = new Geom(vertexData);
	planeGeom->add_primitive(planeTriangles);
	PT(GeomNode) planeNode = new GeomNode(name + "Node");
	planeNode->add_geom(planeGeom);
	NodePath planeNP(planeNode);
	// apply texture
	PT(Texture) tex = TexturePool::load_texture(Filename(texture));
	planeNP.set_texture(tex);
	//
	return planeNP;
}

// load terrain low poly stuff
NodePath loadTerrainLowPoly(const string &name, float widthScale,
		float heightScale, const string &texture)
{
	NodePath terrainNP = window->load_model(framework.get_models(),
			"terrain-low-poly.egg");
	terrainNP.set_name(name);
	terrainNP.set_transform(TransformState::make_identity());
	terrainNP.set_scale(widthScale, widthScale, heightScale);
	PT(Texture) tex = TexturePool::load_texture(Filename(texture));
	terrainNP.set_texture(tex);
	return terrainNP;
}

static LPoint3f terrainRootNetPos;
// terrain update
static AsyncTask::DoneStatus terrainUpdate(GenericAsyncTask *task, void *data)
{
	GeoMipTerrain *terrain = reinterpret_cast<GeoMipTerrain*>(data);
	//set focal point
	//see https://www.panda3d.org/forums/viewtopic.php?t=5384
	LPoint3f focalPointNetPos =
			window->get_camera_group().get_net_transform()->get_pos();
	terrain->set_focal_point(focalPointNetPos - terrainRootNetPos);
	//update every frame
	terrain->update();
	//
	return AsyncTask::DS_cont;
}

// load terrain stuff
GeoMipTerrain* loadTerrain(const string &name, float widthScale,
		float heightScale)
{
	GeoMipTerrain *terrain = new GeoMipTerrain("terrain");
	PNMImage heightField(Filename(dataDir + string("/heightfield.png")));
	terrain->set_heightfield(heightField);
	terrain->get_root().set_transform(TransformState::make_identity());
	//sizing
	float environmentWidthX = (heightField.get_x_size() - 1) * widthScale;
	float environmentWidthY = (heightField.get_y_size() - 1) * widthScale;
	float environmentWidth = (environmentWidthX + environmentWidthY) / 2.0;
	terrain->get_root().set_sx(widthScale);
	terrain->get_root().set_sy(widthScale);
	terrain->get_root().set_sz(heightScale);
	//set other terrain's properties
	unsigned short blockSize = 64, minimumLevel = 0;
	float nearPercent = 0.1, farPercent = 0.7;
	float terrainLODmin = min<float>(minimumLevel, terrain->get_max_level());
	GeoMipTerrain::AutoFlattenMode flattenMode = GeoMipTerrain::AFM_off;
	terrain->set_block_size(blockSize);
	terrain->set_near(nearPercent * environmentWidth);
	terrain->set_far(farPercent * environmentWidth);
	terrain->set_min_level(terrainLODmin);
	terrain->set_auto_flatten(flattenMode);
	//terrain texturing
	PT(TextureStage) textureStage0 = new TextureStage("TextureStage0");
	PT(Texture) textureImage = TexturePool::load_texture(
			Filename(string("terrain.png")));
	terrain->get_root().set_tex_scale(textureStage0, 1.0, 1.0);
	terrain->get_root().set_texture(textureStage0, textureImage, 1);
//	terrain->get_root().set_collide_mask(mask); fixme
	terrain->get_root().set_name(name);
	//brute force generation
	bool bruteForce = true;
	terrain->set_bruteforce(bruteForce);
	//Generate the terrain
	terrain->generate();
	//check if terrain needs update or not
	if (not bruteForce)
	{
		//save the net pos of terrain root
		terrainRootNetPos = terrain->get_root().get_net_transform()->get_pos();
		// Add a task to keep updating the terrain
		framework.get_task_mgr().add(
				new GenericAsyncTask("terrainUpdate", &terrainUpdate,
						(void*) terrain));
	}
	//
	return terrain;
}

// get model and animations
NodePath getModelAnims(const string &name, float scale, int modelFileIdx,
		vector<vector<PT(AnimControl)> > *modelAnimCtls)
{
	// get some models, with animations
	// get the model
	NodePath modelNP = window->load_model(framework.get_models(),
			modelFile[modelFileIdx]);
	// set the name
	modelNP.set_name(name);
	// set scale
	modelNP.set_scale(scale);
	// get animations if requested
	if (modelAnimCtls)
	{
		// associate an anim with a given anim control
		AnimControlCollection tmpAnims;
		NodePath modelAnimNP[2];
		modelAnimCtls->push_back(vector<PT(AnimControl)>(2));
		if ((!modelAnimFiles[modelFileIdx][0].empty())
				&& (!modelAnimFiles[modelFileIdx][1].empty()))
		{
			// first anim -> modelAnimCtls[i][0]
			modelAnimNP[0] = window->load_model(modelNP,
					modelAnimFiles[modelFileIdx][0]);
			auto_bind(modelNP.node(), tmpAnims,
					PartGroup::HMF_ok_part_extra | PartGroup::HMF_ok_anim_extra
							| PartGroup::HMF_ok_wrong_root_name);
			modelAnimCtls->back()[0] = tmpAnims.get_anim(0);
			tmpAnims.clear_anims();
			modelAnimNP[0].detach_node();
			// second anim -> modelAnimCtls[i][1]
			modelAnimNP[1] = window->load_model(modelNP,
					modelAnimFiles[modelFileIdx][1]);
			auto_bind(modelNP.node(), tmpAnims,
					PartGroup::HMF_ok_part_extra | PartGroup::HMF_ok_anim_extra
							| PartGroup::HMF_ok_wrong_root_name);
			modelAnimCtls->back()[1] = tmpAnims.get_anim(0);
			tmpAnims.clear_anims();
			modelAnimNP[1].detach_node();
			// reparent all node paths
			modelAnimNP[0].reparent_to(modelNP);
			modelAnimNP[1].reparent_to(modelNP);
		}
	}
	//
	return modelNP;
}

// toggle debug draw
void toggleDebugDraw(const Event *e, void *data)
{
	toggleDebugFlag = not toggleDebugFlag;
	GamePhysicsManager::get_global_ptr()->debug(toggleDebugFlag);
}

// generic event notify
void genericEventNotify(const Event *e, void *data)
{
	PT(PandaNode) object0 = DCAST(PandaNode,
			e->get_parameter(0).get_ptr());
	PT(PandaNode) object1 = DCAST(PandaNode,
			e->get_parameter(1).get_ptr());

	cout
			<< string("got '") + e->get_name() + string("' between '")
					+ object0->get_name() + string("' and '")
					+ object1->get_name() + string("'") << endl;
}

// create a rigid body oriented along X,Y,Z axis
NodePath createRigidBodyXYZ(const NodePath &modelNP, const string &name,
		const string &shapeType, const string &upAxis, const LPoint3f &pos,
		const LVecBase3f &hpr, const LVecBase3f &scaleOrDims, bool isScale)
{
	NodePath np =
			GamePhysicsManager::get_global_ptr()->get_reference_node_path().attach_new_node(
					name);
	modelNP.instance_to(np);
	LVecBase3f scale;
	if (isScale)
	{
		scale = scaleOrDims;
	}
	else
	{
		LVecBase3f sizes2;
		LVector3f modelDeltaCenter;
		GamePhysicsManager::get_global_ptr()->get_bounding_dimensions(modelNP,
				sizes2, modelDeltaCenter);
		scale = LVecBase3f(abs(scaleOrDims.get_x() / sizes2.get_x()),
				abs(scaleOrDims.get_y() / sizes2.get_y()),
				abs(scaleOrDims.get_z() / sizes2.get_z()));
	}
	np.set_pos_hpr_scale(pos, hpr, scale);
	setParametersBeforeCreation(GamePhysicsManager::RIGIDBODY, name, upAxis);
	GamePhysicsManager::get_global_ptr()->set_parameter_value(
			GamePhysicsManager::RIGIDBODY, "shape_type", shapeType);
	return GamePhysicsManager::get_global_ptr()->create_rigid_body(
			string("RigidBody") + name);
}

// create a constraint
PT(BT3Constraint) createConstraint(const NodePath &objectA,
		const NodePath &objectB, const string &name,
		BT3Constraint::BT3ConstraintType contraint_type,
		bool use_world_reference, const LPoint3f &pivot_references1,
		const LPoint3f &pivot_references2, const LVector3f &axis_references1,
		const LVector3f &axis_references2,
		const LVecBase3f &rotation_references1,
		const LVector3f &rotation_references2, const LVector3f &world_axes1,
		const LVector3f &world_axes2, const LPoint3f &world_anchor, float ratio,
		BT3Constraint::BT3RotateOrder rotation_order,
		bool use_reference_frame_a)
{
	// create the constraint and get a reference
	PT(BT3Constraint) cs =
			DCAST(BT3Constraint,
					GamePhysicsManager::get_global_ptr()->create_constraint(
							name).node());
	// set the constraint type
	cs->set_constraint_type(contraint_type);
	// set the objects
	ValueList_NodePath objects{ objectA, objectB };
	cs->set_owner_objects(objects);
	/// set the parameters
	// world/local references
	cs->set_use_world_reference(use_world_reference);
	// references
	ValueList_LPoint3f points{ pivot_references1, pivot_references2 };
	cs->set_pivot_references(points);
	ValueList_LVector3f vectors{ axis_references1, axis_references2 };
	cs->set_axis_references(vectors);
	ValueList_LVecBase3f hpr{ rotation_references1, rotation_references2 };
	cs->set_rotation_references(hpr);
	// world axes
	ValueList_LVector3f wectors{ world_axes1, world_axes2 };
	cs->set_world_axes(wectors);
	// ratio, rotation order, world anchor, use reference frame a
	cs->set_ratio(ratio);
	cs->set_rotation_order(rotation_order);
	cs->set_world_anchor(world_anchor);
	cs->set_use_reference_frame_a(use_reference_frame_a);
	//
	return cs;
}

PT(BT3Constraint) createConstraint(const string &objectA,
		const string &objectB, const string &name, const string &contraint_type,
		const string &use_world_reference, const string &pivot_references1,
		const string &pivot_references2, const string &axis_references1,
		const string &axis_references2, const string &rotation_references1,
		const string &rotation_references2, const string &world_axes1,
		const string &world_axes2, const string &world_anchor,
		const string &ratio, const string &rotation_order,
		const string &use_reference_frame_a)
{
	auto physicsMgr = GamePhysicsManager::get_global_ptr();
	// arguments must be strings
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT, "objectA",
			objectA);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT, "objectB",
			objectB);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT,
			"contraint_type", contraint_type);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT,
			"use_world_reference", use_world_reference);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT,
			"pivot_references", pivot_references1 + ":" + pivot_references2);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT,
			"axis_references", axis_references1 + ":" + axis_references2);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT,
			"rotation_references",
			rotation_references1 + ":" + rotation_references2);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT,
			"world_axes", world_axes1 + ":" + world_axes2);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT,
			"world_anchor", world_anchor);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT, "ratio",
			ratio);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT,
			"rotation_order", rotation_order);
	physicsMgr->set_parameter_value(GamePhysicsManager::CONSTRAINT,
			"use_reference_frame_a", use_reference_frame_a);
	// create the constraint and get a reference
	PT(BT3Constraint) cs =
			DCAST(BT3Constraint,
					GamePhysicsManager::get_global_ptr()->create_constraint(
							name).node());
	//
	return cs;
}

