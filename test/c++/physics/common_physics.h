/**
 * \file common_physics.h
 *
 * \date 2018-02-25
 * \author consultit
 */

#ifndef PHYSICS_C___COMMON_H_
#define PHYSICS_C___COMMON_H_

#include <pandaFramework.h>
#include <gamePhysicsManager.h>
#include <bt3RigidBody.h>
#include <bt3Ghost.h>
#include <bt3Constraint.h>
#include <bt3ConstraintAttrs.h>
#include <bt3SoftBody.h>
#include <bt3Vehicle.h>
#include <bt3CharacterController.h>

/// global data declarations
extern string dataDir;
extern PandaFramework framework;
extern WindowFramework *window;
extern CollideMask groupMask;
extern CollideMask collideMask;
extern AsyncTask* updateTask;
extern NodePath sceneNP;
extern ClockObject* globalClock;
extern GamePhysicsManager* physicsMgr;
// bam file
extern string bamFileName;
// rope
extern PT(BT3SoftBody)ropeGeomBody;
// player
extern vector<vector<PT(AnimControl)> > playerAnimCtls;
// physics multi-threaded
extern bool physicsMT;

// print creation parameters
void printCreationParameters();
// set parameters as strings before objects creation
void setParametersBeforeCreation(GamePhysicsManager::BT3PhysicType type,
		const string& objectName, const string& upAxis = "z");
// libp3tools typed objects init
void libp3toolsTypedObjectInit();
// start base framework
string startFramework(int argc, char *argv[], const string& msg);
// load plane stuff centered at (0,0,0)
NodePath loadPlane(const string& name, float width = 30.0, float depth = 30.0,
		GamePhysicsManager::BT3UpAxis upAxis = GamePhysicsManager::Z_up,
		const string& texture = "dry-grass.png");
// load terrain low poly stuff
NodePath loadTerrainLowPoly(const string& name, float widthScale = 128,
		float heightScale = 64.0, const string& texture = "dry-grass.png");
// load terrain stuff
GeoMipTerrain* loadTerrain(const string& name, float widthScale = 0.5,
		float heightScale = 10.0);
// get model and animations
NodePath getModelAnims(const string& name, float scale, int modelFileIdx,
		vector<vector<PT(AnimControl)> >* modelAnimCtls);
// toggle debug draw
void toggleDebugDraw(const Event* e, void* data);
// generic event notify
void genericEventNotify(const Event* e, void* data);
// create a rigid body oriented along X,Y,Z axis
NodePath createRigidBodyXYZ(const NodePath& modelNP, const string& name,
		const string& shapeType, const string& upAxis, const LPoint3f& pos,
		const LVecBase3f& hpr, const LVecBase3f& scaleOrDims = LVecBase3f(1, 1, 1),
		bool isScale = true);
// create a constraint
PT(BT3Constraint) createConstraint(const NodePath &objectA,
		const NodePath &objectB, const string &name,
		BT3Constraint::BT3ConstraintType contraint_type,
		bool use_world_reference, const LPoint3f &pivot_references1,
		const LPoint3f &pivot_references2, const LVector3f &axis_references1,
		const LVector3f &axis_references2,
		const LVecBase3f &rotation_references1,
		const LVector3f &rotation_references2, const LVector3f &world_axes1,
		const LVector3f &world_axes2, const LPoint3f &world_anchor, float ratio,
		BT3Constraint::BT3RotateOrder rotation_order,
		bool use_reference_frame_a);
PT(BT3Constraint) createConstraint(const string &objectA,
		const string &objectB, const string &name, const string &contraint_type,
		const string &use_world_reference, const string &pivot_references1,
		const string &pivot_references2, const string &axis_references1,
		const string &axis_references2, const string &rotation_references1,
		const string &rotation_references2, const string &world_axes1,
		const string &world_axes2, const string &world_anchor,
		const string &ratio, const string &rotation_order,
		const string &use_reference_frame_a);

#endif /* PHYSICS_C___COMMON_H_ */
