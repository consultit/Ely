/**
 * \file physics_bt3_soft_demo.cpp
 *
 * \date 2017-05-06
 * \author consultit
 */

#include "common_physics.h"
#include <cIntervalManager.h>

/// soft demos' declarations
void RopeGeom(bool);
void RopeNURBS(bool);
void Init_Cloth(bool);
void Init_Pressure(bool);
void Init_Volume(bool);
void Init_Ropes(bool);
void Init_RopeAttach(bool);
void Init_ClothAttach(bool);
void Init_Sticks(bool);
void Init_CapsuleCollision(bool);
void Init_Collide(bool);
void Init_Collide2(bool);
void Init_Collide3(bool);
void Init_Impact(bool);
void Init_Aero(bool);
void Init_Aero2(bool);
void Init_Friction(bool);
void Init_Torus(bool);
void Init_TorusMatch(bool);
void Init_Bunny(bool);
void Init_BunnyMatch(bool);
void Init_Cutting1(bool);
void Init_ClusterDeform(bool);
void Init_ClusterCollide1(bool);
void Init_ClusterCollide2(bool);
void Init_ClusterSocket(bool);
void Init_ClusterHinge(bool);
void Init_ClusterCombine(bool);
void Init_ClusterCar(bool);
void Init_ClusterRobot(bool);
void Init_ClusterStackSoft(bool);
void Init_ClusterStackMixed(bool);
void Init_TetraCube(bool);
void Init_TetraBunny(bool);

struct Sample
{
	void (*sample)(bool);
	string descr;
};
vector<Sample> sampleList;
void createSampleList();
int sampleIdx = -1;
bool fromBamFile = false;
PT(TextNode)sampleText;
// iterate over samples
static void iterateSamples(const Event* e, void* data);
// read scene from a file
static bool readFromBamFile(string fileName);
// write scene to a file (and exit)
static void writeToBamFileAndExit(const Event*, void* data);
// custom update task for controls
static AsyncTask::DoneStatus updateControls(GenericAsyncTask*, void* data);
// soft body update callback function
static float softBodyCBCount = 0.0;
static int countClbk = 0;
static void softBodyCallback(PT(BT3SoftBody)softBody);

int main(int argc, char *argv[])
{
	string msg("'BT3RigidBody & BT3SoftBody & BTGhost'");
	string bamFile = startFramework(argc, argv, msg);

	/// here is room for your own code
	// print some help to screen
	PT(TextNode)text;
	text = new TextNode("Help");
	text->set_text(
			msg + "\n\n"
			"- press \"n\" to cycle samples\n"
			"- press \"d\" to toggle debug drawing\n"
			"- press \"a\" to pick up the object under the mouse\n");
	NodePath textNodePath = window->get_aspect_2d().attach_new_node(text);
	textNodePath.set_pos(-1.25, 0.0, 0.8);
	textNodePath.set_scale(0.035);

	// create a physics manager
	physicsMgr = new GamePhysicsManager(10,
			window->get_render(), groupMask, collideMask);

	// enable object picking
	physicsMgr->enable_picking(window->get_render(),
			window->get_camera_group().find(string("**/+Camera")),
			DCAST(MouseWatcher, window->get_mouse().node()), "a", "a-up");

	// print creation parameters: default values
	cout << endl << "Default creation parameters:";
	printCreationParameters();

	/// set soft world parameters
	physicsMgr->set_air_density(1.2);
	physicsMgr->set_water_density(0);
	physicsMgr->set_water_offset(0);
	physicsMgr->set_water_normal(LVector3f(0,0,0));

	// load or restore all scene stuff: if passed an argument
	// try to read it from bam file
	if ( bamFile.empty() or (not readFromBamFile(bamFile)))
	{
		// no argument or no valid bamFile
		fromBamFile = false;
		// reparent reference node to render
		physicsMgr->get_reference_node_path().reparent_to(window->get_render());

		///Static environment
		{
			// get a sceneNP, naming it with "SceneNP" to ease restoring from bam file
			LVector3f pos(0.0, 0.0, 0.0);
			LVecBase3f hpr(45.0, 0.0, 0.0);
			/// plane
//			GamePhysicsManager::BT3UpAxis planeUpAxis = GamePhysicsManager::Z_up; // Z_up X_up Y_up
//			sceneNP = loadPlane("SceneNP", 128.0, 128.0, planeUpAxis);
			/// triangle mesh
//			sceneNP = loadTerrainLowPoly("SceneNP", 128.0, 128.0);
			/// parallelepiped
			sceneNP = window->load_model(framework.get_models(), "box");
	        pos = LPoint3f(-10, -105.0, -2.5);
	        hpr = LVecBase3f(45.0, 0, 0);
	        sceneNP.set_scale(LVecBase3f(200.0, 200.0, 5.0));

			// set sceneNP transform
			sceneNP.set_pos_hpr(pos, hpr);

			// create scene's rigid_body (attached to the reference node)
			NodePath sceneRigidBodyNP = physicsMgr->create_rigid_body(
					"SceneRigidBody");
			// get a reference to the scene's rigid_body
			PT(BT3RigidBody)sceneRigidBody =
					DCAST(BT3RigidBody, sceneRigidBodyNP.node());

			// set some parameters before setup
			/// plane
//			sceneRigidBody->set_up_axis(planeUpAxis);
//			sceneRigidBody->set_shape_type(GamePhysicsManager::PLANE);
			/// triangle mesh
//			sceneRigidBody->set_shape_type(GamePhysicsManager::TRIANGLE_MESH);
			/// parallelepiped
			sceneRigidBody->set_shape_type(GamePhysicsManager::BOX);

			// set mass=0 before setup() for static (and kinematic) bodies
			sceneRigidBody->set_mass(0.0);
			// set the object for rigid body
			sceneRigidBody->set_owner_object(sceneNP);

			// setup
			sceneRigidBody->setup();
			// change to kinematic only after setup (as long as mass=0)
//			sceneRigidBody->switch_body_type(BT3RigidBody::STATIC);
		}
	}
	else
	{
		// valid bamFile
		fromBamFile = true;
		// reparent reference node to render
		physicsMgr->get_reference_node_path().reparent_to(window->get_render());

		// restore sceneNP: through panda3d
		sceneNP = physicsMgr->get_reference_node_path().find("**/SceneNP");

		if (sampleIdx == 0)
		{
			// restore ropeGeomBody: through physics manager
			for (int i = 0; i < physicsMgr->get_num_soft_bodies(); ++i)
			{
				PT(BT3SoftBody)softBody = GamePhysicsManager::get_global_ptr()->get_soft_body(i);
				if (softBody->get_name() == "RopeGeomBody")
				{
					ropeGeomBody = softBody;
					break;
				}
			}
		}
	}

	// DEBUG DRAWING: make the debug reference node paths sibling of the reference node
	physicsMgr->get_reference_node_path_debug().reparent_to(window->get_render());
	framework.define_key("d", "toggleDebugDraw", &toggleDebugDraw, nullptr);

	// enable collision notify event: BTRigidBody_BTRigidBody_Collision
	physicsMgr->enable_throw_event(GamePhysicsManager::COLLISIONNOTIFY, true,
			0.1, "COLLISION");
	framework.define_key("COLLISION", "genricEventNotify", &genericEventNotify,
			nullptr);
	framework.define_key("COLLISIONOff", "collisionNotifyOff",
			&genericEventNotify, nullptr);

	// ghost overlap notify events: OVERLAP
	framework.define_key("OVERLAP", "overlapNotify", &genericEventNotify, nullptr);
	framework.define_key("OVERLAPOff", "overlapNotifyOff", &genericEventNotify,
			nullptr);

	/// Soft Bodies: iterate samples
	sampleText = new TextNode("Samples");
	NodePath sampleTextNP = window->get_aspect_2d().attach_new_node(sampleText);
	sampleTextNP.set_pos(-0.1, 0.0, 0.9);
	sampleTextNP.set_scale(0.05);
	sampleTextNP.set_color(0.9, 0.5, 0.1, 1.0);
	createSampleList();
	if (!fromBamFile)
	{
		iterateSamples(nullptr, nullptr);
	}
	framework.define_key("n", "iterate samples", &iterateSamples, nullptr);

	/// first option: start the default update task for all drivers
	physicsMgr->start_default_update();
	if (sampleIdx == 0)
	{
		ropeGeomBody->set_update_callback(softBodyCallback);
	}
	globalClock = ClockObject::get_global_clock();

	/// second option: start the custom update task for the drivers
//	updateTask = new GenericAsyncTask("updateControls", &updateControls,
//			nullptr);
//	framework.get_task_mgr().add(updateTask);
//	updateTask->set_sort(10);

	// write to bam file on exit
	window->get_graphics_window()->set_close_request_event(
			"close_request_event");
	framework.define_key("close_request_event", "writeToBamFile",
			&writeToBamFileAndExit, (void*) &bamFileName);

	// place camera trackball (local coordinate)
	PT(Trackball)trackball = DCAST(Trackball, window->get_mouse().find("**/+Trackball").node());
	trackball->set_pos(0.0, 130.0, -25.0);
	trackball->set_hpr(-20.0, 10.0, 0.0);

	// do the main loop, equals to call app.run() in python
///	framework.main_loop();
	// This is a simpler way to do stuff every frame,
	// if you're too lazy to create a task.
	Thread *current_thread = Thread::get_current_thread();
	while (framework.do_frame(current_thread))
	{
		// Step the interval manager
		CIntervalManager::get_global_ptr()->step();
	}

	return (0);
}

///functions' definitions
void createSampleList()
{
	sampleList =
	{
		{&RopeGeom, "RopeGeom"},
		{&RopeNURBS, "RopeNURBS"},
		{&Init_Cloth, "Init_Cloth"},
		{&Init_Pressure, "Init_Pressure"},
		{&Init_Volume, "Init_Volume"},
		{&Init_Ropes, "Init_Ropes"},
		{&Init_RopeAttach, "Init_RopeAttach"},
		{&Init_ClothAttach, "Init_ClothAttach"},
		{&Init_Sticks, "Init_Sticks"},
		{&Init_CapsuleCollision, "Init_CapsuleCollision"},
		{&Init_Collide, "Init_Collide"},
		{&Init_Collide2, "Init_Collide2"},
		{&Init_Collide3, "Init_Collide3"},
		{&Init_Impact, "Init_Impact"},
		{&Init_Aero, "Init_Aero"},
		{&Init_Aero2, "Init_Aero2"},
		{&Init_Friction, "Init_Friction"},
		{&Init_Torus, "Init_Torus"},
		{&Init_TorusMatch, "Init_TorusMatch"},
		{&Init_Bunny, "Init_Bunny"},
		{&Init_BunnyMatch, "Init_BunnyMatch"},
		{&Init_Cutting1, "Init_Cutting1"},
		{&Init_ClusterDeform, "Init_ClusterDeform"},
		{&Init_ClusterCollide1, "Init_ClusterCollide1"},
		{&Init_ClusterCollide2, "Init_ClusterCollide2"},
		{&Init_ClusterSocket, "Init_ClusterSocket"},
		{&Init_ClusterHinge, "Init_ClusterHinge"},
		{&Init_ClusterCombine, "Init_ClusterCombine"},
		{&Init_ClusterCar, "Init_ClusterCar"},
		{&Init_ClusterRobot, "Init_ClusterRobot"},
		{&Init_ClusterStackSoft, "Init_ClusterStackSoft"},
		{&Init_ClusterStackMixed, "Init_ClusterStackMixed"},
		{&Init_TetraCube, "Init_TetraCube"},
		{&Init_TetraBunny, "Init_TetraBunny"},
	};
}

// iterate over samples
static void iterateSamples(const Event* e, void* data)
{
	if (sampleIdx >= 0)
	{
		// stop old sample
		sampleList[sampleIdx].sample(false);
	}
	// start new sample
	sampleIdx++;
	if ((size_t)sampleIdx >= sampleList.size())
	{
		sampleIdx = 0;
	}
	sampleList[sampleIdx].sample(true);
	sampleText->set_text(sampleList[sampleIdx].descr);
}

// read scene from a file
static bool readFromBamFile(string fileName)
{
	// restore sampleIdx
	DatagramInputFile sampleIdxF;
	sampleIdxF.open(Filename("sampleIdx.boo"));
	Datagram sampleIdxDG;
	sampleIdxF.get_datagram(sampleIdxDG);
	DatagramIterator sampleIdxDGScan(sampleIdxDG);
	sampleIdx = sampleIdxDGScan.get_int32();
	sampleIdxF.close();
	//
	return GamePhysicsManager::get_global_ptr()->read_from_bam_file(fileName);
}

// write scene to a file (and exit)
static void writeToBamFileAndExit(const Event*, void* data)
{
	// save sampleIdx
	DatagramOutputFile sampleIdxF;
	sampleIdxF.open(Filename("sampleIdx.boo"));
	Datagram sampleIdxDG;
	sampleIdxDG.add_int32(sampleIdx);
	sampleIdxF.put_datagram(sampleIdxDG);
	sampleIdxF.close();
	//
	string fileName = *reinterpret_cast<string*>(data);
	GamePhysicsManager::get_global_ptr()->write_to_bam_file(fileName);
	/// second option: remove custom update updateTask
//	framework.get_task_mgr().remove(updateTask);

/// this is for testing explicit removal and destruction of all elements
	// destroy rigid bodies
	while (physicsMgr->get_num_rigid_bodies() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_rigid_body(
				NodePath::any_path(physicsMgr->get_rigid_body(0)));
///		delete DCAST(BT3RigidBody, physicsMgr->get_rigid_body(0).node()); //ERROR
	}
	// destroy soft bodies
	while (physicsMgr->get_num_soft_bodies() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_soft_body(
				NodePath::any_path(physicsMgr->get_soft_body(0)));
///		delete DCAST(BT3SoftBody, physicsMgr->get_soft_body(0).node()); //ERROR
	}
	// delete managers
	delete GamePhysicsManager::get_global_ptr();
	// close the window framework
	framework.close_framework();
	//
	exit(0);
}

// custom update task for controls
static AsyncTask::DoneStatus updateControls(GenericAsyncTask*, void* data)
{
	// call update for controls
	double dt = ClockObject::get_global_clock()->get_dt();
	if (sampleIdx == 0)
	{
		ropeGeomBody->update(dt);
	}
	//
	return AsyncTask::DS_cont;
}

// soft body update callback function
static void softBodyCallback(PT(BT3SoftBody)softBody)
{
	softBodyCBCount += globalClock->get_dt();
	if (softBodyCBCount <= 1)
	{
		return;
	}
	softBodyCBCount = 0;
	int idx = countClbk++ % softBody->get_num_nodes();
	LVector3f vel = softBody->get_node(idx).get_velocity();
	cout << *softBody << "'s node " << idx << "'s velocity: " << vel.length() << endl;
}

