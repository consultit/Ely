/**
 * \file physics_test.cpp
 *
 * \date 2018-06-28
 * \author consultit
 */

#include "suite.h"

struct PhysicsTestCaseFixture
{
	PhysicsTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("ctor PhysicsTestCaseFixture");
	}
	~PhysicsTestCaseFixture()
	{
		BOOST_TEST_MESSAGE("dtor PhysicsTestCaseFixture");
	}
};

/// Physics suite
BOOST_FIXTURE_TEST_SUITE(Physics, PhysicsSuiteFixture);

/// Test cases
BOOST_AUTO_TEST_CASE(PhysicsTest)
{
	BOOST_TEST_MESSAGE("PhysicsTest");
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END(); // PhysicsSuiteFixture
