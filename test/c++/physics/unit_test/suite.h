/**
 * \file suite.h
 *
 * \date 2018-06-28
 * \author consultit
 */
#ifndef PHYSICS_SUITE_H_
#define PHYSICS_SUITE_H_

#include <boost/test/unit_test.hpp>

struct PhysicsSuiteFixture
{
	PhysicsSuiteFixture()
	{
		BOOST_TEST_MESSAGE("ctor PhysicsSuiteFixture");
	}
	~PhysicsSuiteFixture()
	{
		BOOST_TEST_MESSAGE("dtor PhysicsSuiteFixture");
	}
};

#endif /* PHYSICS_SUITE_H_ */
