/**
 * \file physics_bt3_constraints.cpp
 *
 * \date 2019-09-21
 * \author consultit
 */

#include "common_physics.h"
#include <cmath>

// event handling
using ArgsEv =  tuple<NodePath, NodePath, float> ;

static void setAccept(const string &keyEv,
		void (*handlerEv)(const Event*, void*), ArgsEv &argsAdd,
		ArgsEv &argsSub)
{
	framework.define_key(keyEv, keyEv + "_handler", handlerEv,
			(void*) &argsAdd);
	framework.define_key(keyEv + "-repeat", keyEv + "-repeat" + "_handler",
			handlerEv, (void*) &argsAdd);
	framework.define_key("shift-" + keyEv, "shift-" + keyEv + "_handler",
			handlerEv, (void*) &argsSub);
	framework.define_key("shift-" + keyEv + "-repeat",
			"shift-" + keyEv + "-repeat" + "_handler", handlerEv,
			(void*) &argsSub);
}

static void rotateH(const Event*, void *data)
{
	ArgsEv& args = *static_cast<ArgsEv*>(data);
	DCAST(BT3RigidBody, get<1>(args).node())->get_collision_object_attrs()->set_activate();
	get<0>(args).set_h(get<0>(args).get_h() + get<2>(args));
}

static void rotateP(const Event*, void *data)
{
	ArgsEv& args = *static_cast<ArgsEv*>(data);
	DCAST(BT3RigidBody, get<1>(args).node())->get_collision_object_attrs()->set_activate();
	get<0>(args).set_p(get<0>(args).get_p() + get<2>(args));
}

static void rotateR(const Event*, void *data)
{
	ArgsEv& args = *static_cast<ArgsEv*>(data);
	DCAST(BT3RigidBody, get<1>(args).node())->get_collision_object_attrs()->set_activate();
	get<0>(args).set_r(get<0>(args).get_r() + get<2>(args));
}

// write scene to a file (and exit)
static void writeToBamFileAndExit(const Event*, void *data);

int main(int argc, char *argv[])
{
	string msg("'BT3Constraint test'");
	string bamFile = startFramework(argc, argv, msg);

	/// here is room for your own code
	// print some help to screen
	PT(TextNode) text;
	text = new TextNode("Help");
	text->set_text(
			msg + "\n\n"
			"- press \"d\" to toggle debug drawing\n"
			"- press \"up\"/\"left\"/\"down\"/\"right\" arrows to move the player\n"
			"- press \"a\" to pick up the object under the mouse\n");
	NodePath textNodePath = window->get_aspect_2d().attach_new_node(text);
	textNodePath.set_pos(-1.25, 0.0, 0.8);
	textNodePath.set_scale(0.035);

	// create a physics manager
	GamePhysicsManager *physicsMgr;
	if (physicsMT)
	{
		// Multi-threaded physics
		BT3PhysicsInfo physicsInfo;
		physicsInfo.set_world_type(BT3PhysicsInfo::DISCRETE_MT_WORLD);
		physicsInfo.set_broad_phase_type(BT3PhysicsInfo::DBVT_BROADPHASE);
		physicsInfo.set_solver_type(
				BT3PhysicsInfo::SEQUENTIAL_IMPULSE_CSOLVER_MT);
		physicsMgr = new GamePhysicsManager(10, window->get_render(), groupMask,
				collideMask, physicsInfo);
	}
	else
	{
		physicsMgr = new GamePhysicsManager(10, window->get_render(), groupMask,
				collideMask);
	}

	// enable object picking
	physicsMgr->enable_picking(window->get_render(),
			window->get_camera_group().find(string("**/+Camera")),
			DCAST(MouseWatcher, window->get_mouse().node()), "a", "a-up");

	// print creation parameters: default values
	cout << endl << "Default creation parameters:";
	printCreationParameters();

	// reparent reference node to render
	physicsMgr->get_reference_node_path().reparent_to(window->get_render());

	///Static environment
	// get a sceneNP, naming it with "SceneNP" to ease restoring from bam file
	LVector3f pos(0.0, 0.0, 0.0);
	LVecBase3f hpr(45.0, 0.0, 0.0);
	/// plane
	GamePhysicsManager::BT3UpAxis planeUpAxis = GamePhysicsManager::Z_up; // Z_up X_up Y_up
	sceneNP = loadPlane("SceneNP", 128.0, 128.0, planeUpAxis);

	// set sceneNP transform
	sceneNP.set_pos_hpr(pos, hpr);

	// create scene's rigid_body (attached to the reference node)
	NodePath sceneRigidBodyNP = physicsMgr->create_rigid_body("SceneRigidBody");
	// get a reference to the scene's rigid_body
	PT(BT3RigidBody) sceneRigidBody = DCAST(BT3RigidBody,
			sceneRigidBodyNP.node());

	// set some parameters before setup
	/// plane
	sceneRigidBody->set_up_axis(planeUpAxis);
	sceneRigidBody->set_shape_type(GamePhysicsManager::PLANE);

	// set mass=0 before setup() for static (and kinematic) bodies
	sceneRigidBody->set_mass(0.0);
	// set the object for rigid body
	sceneRigidBody->set_owner_object(sceneNP);

	// setup
	sceneRigidBody->setup();
	// change some attrs
	cout << sceneRigidBody->get_collision_object_attrs()->get_friction()
			<< endl;
	sceneRigidBody->get_collision_object_attrs()->set_friction(0.3);
	cout << sceneRigidBody->get_collision_object_attrs()->get_friction()
			<< endl;

	/// Constraints
	NodePath modelNP = getModelAnims("modelNP", 1.0, 1, &playerAnimCtls);
	NodePath rbANP, rbBNP;
	/// constraint with world reference
	rbANP = createRigidBodyXYZ(modelNP, "rbA1", "cylinder", "z",
			LPoint3f(-10.0, 0.0, 40.0), LVecBase3f(0.0, 0.0, 0.0),
			LVecBase3f(0.5));
	DCAST(BT3RigidBody, rbANP.node())->set_mass(0.0);
	DCAST(BT3RigidBody, rbANP.node())->switch_body_type(
			BT3RigidBody::KINEMATIC);
	rbBNP = createRigidBodyXYZ(modelNP, "rbB1", "capsule", "z",
			LPoint3f(-10.0, 0.0, 25.0), LVecBase3f(0.0, 0.0, 0.0),
			LVecBase3f(2.0, 2.0, 2.0));

	// event handling
	constexpr float diffH = 1.0;
	ArgsEv diffHAdd(rbANP, rbBNP, diffH);
	ArgsEv diffHSub(rbANP, rbBNP, -diffH);

	setAccept("h", rotateH, diffHAdd, diffHSub);

	constexpr float diffP = 1.0;
	ArgsEv diffPAdd(rbANP, rbBNP, diffP);
	ArgsEv diffPSub(rbANP, rbBNP, -diffP);

	setAccept("p", rotateP, diffPAdd, diffPSub);

	constexpr float diffR = 1.0;
	ArgsEv diffRAdd(rbANP, rbBNP, diffR);
	ArgsEv diffRSub(rbANP, rbBNP, -diffR);

	setAccept("r", rotateR, diffRAdd, diffRSub);

    bool csInitialize = true;
    auto csType = make_tuple(
//    		BT3Constraint::POINT2POINT, "point2point"
//    		BT3Constraint::CONETWIST,"cone_twist"
    		BT3Constraint::DOF6, "dof6"
    );

    PT(BT3Constraint) csWR = nullptr;
    if (not csInitialize)
    {
        csWR = createConstraint(rbANP, rbBNP, "constraint",
                                get<0>(csType), true,
                                LPoint3f(-10.0, 0.0, 39.0), LPoint3f::zero(),
                                LVector3f::zero(), LVector3f::zero(),
                                LVecBase3f::zero(), LVecBase3f::zero(),
                                LVector3f::zero(), LVector3f::zero(),
                                LPoint3f::zero(), 0.0, BT3Constraint::RO_XYZ, false);
        csWR->set_rotation_references({LVecBase3f(0, 0, 0)});
        csWR->setup();
    }
    else
    {
        csWR = createConstraint(rbANP.get_name(), rbBNP.get_name(), "constraint",
                                get<1>(csType), "true",
                                "-10.0,0.0,39.0", "0.0,0.0,0.0",
                                "0.0,0.0,0.0", "0.0,0.0,0.0",
                                "0.0,0.0,0.0", "0.0,0.0,0.0",
                                "0.0,0.0,0.0", "0.0,0.0,0.0",
                                "0.0,0.0,0.0", "0.0", "xyz", "false");
    }
    //
    csWR->get_constraints_attrs()->set_debug_draw_size(1.0);
    if (get<0>(csType) == BT3Constraint::CONETWIST)
	{
		ConeTwistAttrs *csAttrs =
				static_cast<ConeTwistAttrs*>(csWR->get_constraints_attrs());
		auto params = make_tuple(csAttrs->get_swing_span1(),
				csAttrs->get_swing_span2(), csAttrs->get_twist_span(),
				csAttrs->get_limit_softness(), csAttrs->get_bias_factor(),
				csAttrs->get_relaxation_factor());
		auto paramsCT = make_tuple(M_PI_4, M_PI_4, M_PI_4, 1.0, 0.3, 1.0);
		csAttrs->set_limit(M_PI_4, M_PI_4, M_PI_4, 1.0, 0.3, 1.0);
		csAttrs->set_angular_only(false);
		csAttrs->set_damping(0.01);
		csAttrs->set_fix_thresh(0.05);
		csAttrs->set_enable_motor(false);
		LRotationf target(LVector3f(0.0, 0.0, 1.0), 0);
		csAttrs->set_motor_target(target);
		csAttrs->set_max_motor_impulse(2.0);
		csAttrs->set_max_motor_impulse_normalized(true);
	}

	/// body A attached to world through constraint
    physicsMgr->set_parameters_defaults(GamePhysicsManager::CONSTRAINT);
    NodePath rbWANP, rbWBNP;
	rbWANP = createRigidBodyXYZ(modelNP, "rbWA1", "capsule", "z",
			LPoint3f(10.0, 0.0, 40.0), LVecBase3f(0.0, 0.0, 0.0),
			LVecBase3f(1.0));
	rbWBNP = NodePath();
	auto csWType = BT3Constraint::POINT2POINT;
	PT(BT3Constraint) csWWR = nullptr;
	csWWR = createConstraint(rbWANP, rbWBNP, "constraintW",
							csWType, true,
							LPoint3f(10.0, 0.0, 50.0), LPoint3f::zero(),
							LVector3f::zero(), LVector3f::zero(),
							LVecBase3f::zero(), LVecBase3f::zero(),
							LVector3f::zero(), LVector3f::zero(),
							LPoint3f::zero(), 0.0,
							BT3Constraint::RO_XYZ, false);
	csWWR->set_rotation_references({ LVecBase3f(0, 0, 0) });
	csWWR->setup();
	csWWR->get_constraints_attrs()->set_debug_draw_size(1.0);

	// DEBUG DRAWING: make the debug reference node paths sibling of the reference node
	physicsMgr->get_reference_node_path_debug().reparent_to(
			window->get_render());
	framework.define_key("d", "toggleDebugDraw", &toggleDebugDraw, nullptr);

	/// first option: start the default update task for all physics components
	physicsMgr->start_default_update();
	globalClock = ClockObject::get_global_clock();

	// write to bam file on exit
	window->get_graphics_window()->set_close_request_event(
			"close_request_event");
	framework.define_key("close_request_event", "writeToBamFile",
			&writeToBamFileAndExit, (void*) &bamFileName);

	// place camera trackball (local coordinate)
	PT(Trackball) trackball = DCAST(Trackball,
			window->get_mouse().find("**/+Trackball").node());
	trackball->set_pos(10.0, 80.0, -35.0);
	trackball->set_hpr(0.0, 10.0, 0.0);

	// do the main loop, equals to call app.run() in python
	framework.main_loop();

	return (0);
}

// write scene to a file (and exit)
static void writeToBamFileAndExit(const Event*, void *data)
{
	string fileName = *reinterpret_cast<string*>(data);
	GamePhysicsManager::get_global_ptr()->write_to_bam_file(fileName);
	/// second option: remove custom update updateTask
//	framework.get_task_mgr().remove(updateTask);

/// this is for testing explicit removal and destruction of all elements
	GamePhysicsManager *physicsMgr = GamePhysicsManager::get_global_ptr();
	// destroy vehicles
	while (physicsMgr->get_num_vehicles() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_vehicle(
				NodePath::any_path(physicsMgr->get_vehicle(0)));
///		delete DCAST(BT3Vehicle, physicsMgr->get_vehicle(0).node()); //ERROR
	}
	// destroy characterControllers
	while (physicsMgr->get_num_character_controllers() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_character_controller(
				NodePath::any_path(physicsMgr->get_character_controller(0)));
///		delete DCAST(BT3CharacterControllers, physicsMgr->get_character_controller(0).node()); //ERROR
	}
	// destroy constraints
	while (physicsMgr->get_num_constraints() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_constraint(
				NodePath::any_path(physicsMgr->get_constraint(0)));
///		delete DCAST(BT3Constraint, physicsMgr->get_constraint(0).node()); //ERROR
	}
	// destroy rigid bodies
	while (physicsMgr->get_num_rigid_bodies() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_rigid_body(
				NodePath::any_path(physicsMgr->get_rigid_body(0)));
///		delete DCAST(BT3RigidBody, physicsMgr->get_rigid_body(0).node()); //ERROR
	}
	// destroy ghosts
	while (physicsMgr->get_num_ghosts() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_ghost(NodePath::any_path(physicsMgr->get_ghost(0)));
///		delete DCAST(BT3Ghost, physicsMgr->get_ghost(0).node()); //ERROR
	}
	// delete managers
	delete GamePhysicsManager::get_global_ptr();
	// close the window framework
	framework.close_framework();
	//
	exit(0);
}
