/**
 * \file physics_bt3.cpp
 *
 * \date 2016-10-09
 * \author consultit
 */

#include "common_physics.h"
#include <cLerpNodePathInterval.h>
#include <cMetaInterval.h>
#include <cIntervalManager.h>

/// static data definitions
static TypedObject *userData = nullptr;
//// player specifics
static PT(BT3RigidBody) playerRigidBody = nullptr;
// ghost
static PT(BT3Ghost) canGhost = nullptr;
// vehicle
static PT(BT3Vehicle) vehicle = nullptr;
static string moveEvent;
static string steadyEvent;
// character controller
static PT(BT3CharacterController) characterController = nullptr;

// read scene from a file
static bool readFromBamFile(string fileName);
// write scene to a file (and exit)
static void writeToBamFileAndExit(const Event*, void *data);
// custom update task for controls
static AsyncTask::DoneStatus updateControls(GenericAsyncTask*, void *data);
// rigid body update callback function
static void rigidBodyCallback(PT(BT3RigidBody) rigidBody);
// vehicle event notify
void vehicleEventNotify(const Event *e, void *data);
// character controller event notify
void characterControllerEventNotify(const Event *e, void *data);
// let target move back and forth
void letMoveBackForth(const NodePath &target, float duration,
		const LPoint3f &startPos, const LPoint3f &endPos,
		const LVecBase3f &startHpr, const LVecBase3f &endHpr);
// vehicle controller
//[f,f-stop,b,b-stop,l,l-stop,r,r-stop,b,b-stop]
string vehicleKeys[] =
{ "i", "i-up", "k", "k-up", "j", "j-up", "l", "l-up", "b", "b-up" };
void vehicleController(const Event *e, void *data);
// character controller
//[f,f-stop,b,b-stop,l,l-stop,r,r-stop,j,j-stop]
string characterKeys[] =
{ "arrow_up", "arrow_up-up", "arrow_down", "arrow_down-up", "arrow_left",
		"arrow_left-up", "arrow_right", "arrow_right-up", "m", "m-up" };
void characterControllerClbk(const Event *e, void *data);
// get ghost's overlapping objects
void getGhostOverlappingObjects(const Event *e, void *data);

int main(int argc, char *argv[])
{
	string msg("'BT3RigidBody & BT3SoftBody & BTGhost'");
	string bamFile = startFramework(argc, argv, msg);

	/// here is room for your own code
	// print some help to screen
	PT(TextNode) text;
	text = new TextNode("Help");
	text->set_text(
			msg
					+ "\n\n"
							"- press \"d\" to toggle debug drawing\n"
							"- press \"up\"/\"left\"/\"down\"/\"right\" arrows to move the player\n"
							"- press \"a\" to pick up the object under the mouse\n");
	NodePath textNodePath = window->get_aspect_2d().attach_new_node(text);
	textNodePath.set_pos(-1.25, 0.0, 0.8);
	textNodePath.set_scale(0.035);

	// create a physics manager
	GamePhysicsManager *physicsMgr;
	if (physicsMT)
	{
		// Multi-threaded physics
		BT3PhysicsInfo physicsInfo;
		physicsInfo.set_world_type(BT3PhysicsInfo::DISCRETE_MT_WORLD);
		physicsInfo.set_broad_phase_type(BT3PhysicsInfo::DBVT_BROADPHASE);
		physicsInfo.set_solver_type(
				BT3PhysicsInfo::SEQUENTIAL_IMPULSE_CSOLVER_MT);
		physicsMgr = new GamePhysicsManager(10, window->get_render(), groupMask,
				collideMask, physicsInfo);
	}
	else
	{
		physicsMgr = new GamePhysicsManager(10, window->get_render(), groupMask,
				collideMask);
	}

	// enable object picking
	physicsMgr->enable_picking(window->get_render(),
			window->get_camera_group().find(string("**/+Camera")),
			DCAST(MouseWatcher, window->get_mouse().node()), "a", "a-up");

	// print creation parameters: default values
	cout << endl << "Default creation parameters:";
	printCreationParameters();

	// load or restore all scene stuff: if passed an argument
	// try to read it from bam file
	if (bamFile.empty() or (not readFromBamFile(bamFile)))
	{
		// no argument or no valid bamFile
		// reparent reference node to render
		physicsMgr->get_reference_node_path().reparent_to(window->get_render());

		///Static environment
		{
			// get a sceneNP, naming it with "SceneNP" to ease restoring from bam file
			LVector3f pos(0.0, 0.0, 0.0);
			LVecBase3f hpr(45.0, 0.0, 0.0);
			/// plane
			GamePhysicsManager::BT3UpAxis planeUpAxis = GamePhysicsManager::Z_up; // Z_up X_up Y_up
			sceneNP = loadPlane("SceneNP", 128.0, 128.0, planeUpAxis);
			/// triangle mesh
//			sceneNP = loadTerrainLowPoly("SceneNP", 128.0, 128.0);
			/// terrain
//			userData = loadTerrain("SceneNP", 1.0, 60.0);
//			sceneNP = DCAST(GeoMipTerrain, userData)->get_root();
//			pos = LVector3f(-256.0, -256.0, 0.0);
//			hpr = LVecBase3f(0.0, 0.0, 0.0);

			// set sceneNP transform
			sceneNP.set_pos_hpr(pos, hpr);

			// create scene's rigid_body (attached to the reference node)
			NodePath sceneRigidBodyNP = physicsMgr->create_rigid_body(
					"SceneRigidBody");
			// get a reference to the scene's rigid_body
			PT(BT3RigidBody) sceneRigidBody = DCAST(BT3RigidBody,
					sceneRigidBodyNP.node());

			// set some parameters before setup
			/// plane
			sceneRigidBody->set_up_axis(planeUpAxis);
			sceneRigidBody->set_shape_type(GamePhysicsManager::PLANE);
			/// triangle mesh
//			sceneRigidBody->set_shape_type(GamePhysicsManager::TRIANGLE_MESH);
			/// terrain
//			sceneRigidBody->set_shape_type(GamePhysicsManager::TERRAIN);
//			sceneRigidBody->set_user_data(userData);

			// set mass=0 before setup() for static (and kinematic) bodies
			sceneRigidBody->set_mass(0.0);
			// set the object for rigid body
			sceneRigidBody->set_owner_object(sceneNP);

			// setup
			sceneRigidBody->setup();
			// change to kinematic only after setup (as long as mass=0)
//			sceneRigidBody->switch_body_type(BT3RigidBody::KINEMATIC);
			// change some attrs
			cout << sceneRigidBody->get_collision_object_attrs()->get_friction()
					<< endl;
			sceneRigidBody->get_collision_object_attrs()->set_friction(0.3);
			cout << sceneRigidBody->get_collision_object_attrs()->get_friction()
					<< endl;
		}

		/// Rigid Bodies
		{
			NodePath modelNP = getModelAnims("modelNP", 1.0, 4,
					&playerAnimCtls);
			/// box
			NodePath playerNP =
					physicsMgr->get_reference_node_path().attach_new_node(
							"playerNP");
			modelNP.instance_to(playerNP);
			// set shape type
			physicsMgr->set_parameter_value(GamePhysicsManager::RIGIDBODY,
					"shape_type", "box");
			// set various creation parameters as string for other rigid bodies
			setParametersBeforeCreation(GamePhysicsManager::RIGIDBODY,
					"playerNP", "no_up");
			// set transform
			playerNP.set_pos_hpr(LPoint3f(4.1, 0.0, 100.1),
					LVecBase3f(-75.0, 145.0, -235.0));
			// create player's rigid_body (attached to the reference node)
			NodePath playerRigidBodyNP = physicsMgr->create_rigid_body(
					"PlayerRigidBody");
			// get a reference to the player's rigid_body
			playerRigidBody = DCAST(BT3RigidBody, playerRigidBodyNP.node());

			/// sphere
			NodePath sphereNP("SphereNP");
			modelNP.instance_to(sphereNP);
			sphereNP.set_pos_hpr(LPoint3f(4.1, 0.0, 130.1),
					LVecBase3f(145.0, -235.0, -75.0));
			setParametersBeforeCreation(GamePhysicsManager::RIGIDBODY, "",
					"no_up");
			physicsMgr->set_parameter_value(GamePhysicsManager::RIGIDBODY,
					"shape_type", "sphere");
			// create && get a reference to a BT3RigidBody
			PT(BT3RigidBody) sphereRigidBody = DCAST(BT3RigidBody,
					physicsMgr->create_rigid_body("RigidBodySphere").node());
			// setup
			sphereRigidBody->set_owner_object(sphereNP);
			sphereRigidBody->setup();
			// change some attrs
			cout
					<< sphereRigidBody->get_collision_object_attrs()->get_friction()
					<< endl;
			sphereRigidBody->get_collision_object_attrs()->set_friction(0.9);
			cout
					<< sphereRigidBody->get_collision_object_attrs()->get_friction()
					<< endl;

			/// cylinders
			//z
			createRigidBodyXYZ(modelNP, "cylinderZ", "cylinder", "z",
					LPoint3f(4.1, 0.0, 160.1), LVecBase3f(0.0, 0.0, 0.0),
					LVecBase3f(1.0, 1.0, 3.0));
			//x
			createRigidBodyXYZ(modelNP, "cylinderX", "cylinder", "x",
					LPoint3f(4.1 - 15.1, 0.0, 160.1),
					LVecBase3f(145.0, -75.0, -235.0),
					LVecBase3f(3.0, 1.0, 1.0));
			//y
			createRigidBodyXYZ(modelNP, "cylinderY", "cylinder", "y",
					LPoint3f(4.1 + 15.1, 0.0, 160.1),
					LVecBase3f(145.0, -75.0, -235.0),
					LVecBase3f(1.0, 3.0, 1.0));

			/// capsules
			//z
			createRigidBodyXYZ(modelNP, "capsuleZ", "capsule", "z",
					LPoint3f(4.1, 0.0, 190.1), LVecBase3f(0.0, 0.0, 0.0),
					LVecBase3f(1.0, 1.0, 6.0));
			//x
			createRigidBodyXYZ(modelNP, "capsuleX", "capsule", "x",
					LPoint3f(4.1 - 15.1, 0.0, 190.1),
					LVecBase3f(145.0, -75.0, -235.0),
					LVecBase3f(6.0, 1.0, 1.0));
			//y
			createRigidBodyXYZ(modelNP, "capsuleY", "capsule", "y",
					LPoint3f(4.1 + 15.1, 0.0, 190.1),
					LVecBase3f(145.0, -75.0, -235.0),
					LVecBase3f(1.0, 6.0, 1.0));

			/// cones
			//z
			createRigidBodyXYZ(modelNP, "coneZ", "cone", "z",
					LPoint3f(4.1, 0.0, 210.1), LVecBase3f(0.0, 0.0, 0.0),
					LVecBase3f(1.0, 1.0, 3.0));
			//x
			createRigidBodyXYZ(modelNP, "coneX", "cone", "x",
					LPoint3f(4.1 - 15.1, 0.0, 210.1),
					LVecBase3f(145.0, -75.0, -235.0),
					LVecBase3f(3.0, 1.0, 1.0));
			//y
			createRigidBodyXYZ(modelNP, "coneY", "cone", "y",
					LPoint3f(4.1 + 15.1, 0.0, 210.1),
					LVecBase3f(145.0, -75.0, -235.0),
					LVecBase3f(1.0, 3.0, 1.0));
		}

		/// Constraints
		{
			NodePath modelNP = getModelAnims("modelNP", 1.0, 5,
					&playerAnimCtls);
			NodePath rbANP, rbBNP;
			/// point2point (use local reference)
			rbANP = createRigidBodyXYZ(modelNP, "rbA1", "cylinder", "z",
					LPoint3f(-60.0, 0.0, 20.0), LVecBase3f(0.0, 0.0, 0.0),
					LVecBase3f(2.0, 2.0, 2.0));
			rbBNP = createRigidBodyXYZ(modelNP, "rbB1", "capsule", "z",
					LPoint3f(-50.0, 0.0, 20.0), LVecBase3f(0.0, 0.0, 0.0),
					LVecBase3f(2.0, 2.0, 2.0));
			PT(BT3Constraint) p2p = createConstraint(rbANP, rbBNP,
					"point2point", BT3Constraint::POINT2POINT, false,
					LPoint3f(5.0, 0.0, 0.0), LPoint3f(-5.0, 0.0, 0.0),
					LVector3f::zero(), LVector3f::zero(), LVecBase3f::zero(),
					LVecBase3f::zero(), LVector3f::zero(), LVector3f::zero(),
					LPoint3f::zero(), 0.0, BT3Constraint::RO_XYZ, false);
			p2p->setup();
			/// hinge (use global reference)
			rbANP = createRigidBodyXYZ(modelNP, "rbA2", "cylinder", "z",
					LPoint3f(50.0, 0.0, 20.0), LVecBase3f(45.0, 0.0, 0.0),
					LVecBase3f(2.0, 2.0, 2.0));
			rbBNP = createRigidBodyXYZ(modelNP, "rbB2", "capsule", "z",
					LPoint3f(70.0, 0.0, 20.0), LVecBase3f(0.0, 0.0, 0.0),
					LVecBase3f(2.0, 2.0, 2.0));
			PT(BT3Constraint) hinge = createConstraint(rbANP, rbBNP,
					"hinge", BT3Constraint::HINGE, true,
					LPoint3f(60.0, 0.0, 20.0), LPoint3f::zero(),
					LVector3f(0.0, 0.0, 1.0), LVector3f::zero(),
					LVecBase3f::zero(), LVecBase3f::zero(),
					LVector3f(0.0, 0.0, 1.0), LVector3f::zero(),
					LPoint3f::zero(), 0.0, BT3Constraint::RO_XYZ, false);
			hinge->setup();
			// change some attrs
			HingeAttrs *constraintsAttrs =
					static_cast<HingeAttrs*>(hinge->get_constraints_attrs());
			cout << constraintsAttrs->get_angular_only() << endl;
			constraintsAttrs->set_angular_only(true);
			cout << constraintsAttrs->get_angular_only() << endl;

			/// hinge_accumulated,hinge2,cone_twist,dof6,dof6_spring,dof6_spring2,fixed,universal,slider,contact,gear
		}

		/// Ghosts
		{
			NodePath modelNP = getModelAnims("modelNP", 1.0, 5,
					&playerAnimCtls);
			/// ghost
			NodePath canNP =
					physicsMgr->get_reference_node_path().attach_new_node(
							"canNP");
			modelNP.instance_to(canNP);
			// set shape type and events
			physicsMgr->set_parameter_value(GamePhysicsManager::GHOST,
					"shape_type", "cylinder");
			physicsMgr->set_parameter_value(GamePhysicsManager::GHOST,
					"thrown_events", "overlap@OVERLAP@0.1");
			// set various creation parameters as string for other ghosts
			setParametersBeforeCreation(GamePhysicsManager::GHOST, "canNP",
					"x");
			// set scale, transform
			canNP.set_pos_hpr_scale(LPoint3f(0.0, 0.0, 6.0),
					LVecBase3f(0.0, 0.0, 90.0), LVecBase3f(1.0, 5.0, 5.0));
			// create player's rigid_body (attached to the reference node)
			NodePath canGhostNP = physicsMgr->create_ghost("CanGhost");
			// get a reference to the player's rigid_body
			canGhost = DCAST(BT3Ghost, canGhostNP.node());
			// set visible for debugging
			canGhost->set_visible(true);
		}

		///Vehicle
		{
			// create the chassis
			NodePath chassisModel = getModelAnims("chassisModel", 4.0, 6,
					&playerAnimCtls);
			NodePath chassisNP = createRigidBodyXYZ(chassisModel, "chassis",
					"box", "z", LPoint3f(20.0, -20.0, 10.0),
					LVecBase3f(0.0, 0.0, 0.0), LVecBase3f(1.0, 1.0, 1.0));
			PT(BT3RigidBody) chassis = DCAST(BT3RigidBody,
					chassisNP.node());
			chassis->set_mass(1.0);
			chassis->switch_body_type(BT3RigidBody::DYNAMIC);
			// set events (default names)
			moveEvent = chassisNP.get_name() + "_Move";
			steadyEvent = chassisNP.get_name() + "_Steady";
			physicsMgr->set_parameter_value(GamePhysicsManager::VEHICLE,
					"thrown_events",
					"move@" + moveEvent + "@0.1:steady@" + steadyEvent
							+ "@0.1");
			// create the vehicle
			vehicle = DCAST(BT3Vehicle,
					physicsMgr->create_vehicle("Vehicle").node());
			// set the vehicle's chassis (i.e. owner object)
			vehicle->set_owner_object(chassisNP);
			// set vehicle's wheels' models
			NodePath wheelNP = getModelAnims("wheelNP", 4.0, 7,
					&playerAnimCtls);
			vehicle->set_wheel_model(wheelNP, true);
			vehicle->set_wheel_model(wheelNP, false);
			// setup the vehicle
			vehicle->setup();
		}

		///Character controller
		{
			NodePath charModelNP = getModelAnims("charModelNP", 1.0, 1,
					&playerAnimCtls);
			// set scale, transform
			charModelNP.set_pos_hpr_scale(LPoint3f(0.0, -30.0, 0.1),
					LVecBase3f(0.0, 0.0, 0.0), LVecBase3f(2.0, 2.0, 2.0));
			// set shape type and events
			physicsMgr->set_parameters_defaults(GamePhysicsManager::GHOST);
			physicsMgr->set_parameter_value(GamePhysicsManager::GHOST,
					"shape_type", "capsule");
			physicsMgr->set_parameter_value(GamePhysicsManager::GHOST,
					"thrown_events", "overlap@OVERLAP@0.1");
			// create character's ghost (attached to the reference node)
			NodePath characterNP = physicsMgr->create_ghost("CharacterGhost");
			// get a reference to the player's rigid_body
			PT(BT3Ghost) characterGhost = DCAST(BT3Ghost,
					characterNP.node());
			// set owner object and setup
			characterGhost->set_owner_object(charModelNP);
			characterGhost->setup();
			// create the character controller
			physicsMgr->set_parameters_defaults(
					GamePhysicsManager::CHARACTERCONTROLLER);
			physicsMgr->set_parameter_value(
					GamePhysicsManager::CHARACTERCONTROLLER, "thrown_events",
					"on_ground@@0.1:in_air@@0.1");
			characterController = DCAST(BT3CharacterController,
					physicsMgr->create_character_controller(
							"CharacterController").node());
			// set the character controller's character (i.e. owner object)
			characterController->set_owner_object(characterNP);
			// setup the character controller
			characterController->setup();
		}
	}
	else
	{
		// valid bamFile
		// reparent reference node to render
		physicsMgr->get_reference_node_path().reparent_to(window->get_render());

		// restore sceneNP: through panda3d
		sceneNP = physicsMgr->get_reference_node_path().find("**/SceneNP");

		// restore playerRigidBody: through physics manager
		for (int i = 0; i < physicsMgr->get_num_rigid_bodies(); ++i)
		{
			PT(BT3RigidBody) rigidBody =
					GamePhysicsManager::get_global_ptr()->get_rigid_body(i);
			if (rigidBody->get_name() == "PlayerRigidBody")
			{
				playerRigidBody = rigidBody;
				break;
			}
		}
		// restore canGhost: through physics manager
		for (int i = 0; i < physicsMgr->get_num_ghosts(); ++i)
		{
			PT(BT3Ghost) ghost =
					GamePhysicsManager::get_global_ptr()->get_ghost(i);
			if (ghost->get_name() == "CanGhost")
			{
				canGhost = ghost;
				break;
			}
		}
		// restore vehicle: through physics manager
		for (int i = 0; i < physicsMgr->get_num_vehicles(); ++i)
		{
			PT(BT3Vehicle) vehicleA =
					GamePhysicsManager::get_global_ptr()->get_vehicle(i);
			if (vehicleA->get_name() == "Vehicle")
			{
				vehicle = vehicleA;
				break;
			}
		}
		moveEvent = vehicle->get_owner_object().get_name() + "_Move";
		steadyEvent = vehicle->get_owner_object().get_name() + "_Steady";
		// restore characterController: through physics manager
		for (int i = 0; i < physicsMgr->get_num_character_controllers(); ++i)
		{
			PT(BT3CharacterController) characterControllerA =
					GamePhysicsManager::get_global_ptr()->get_character_controller(
							i);
			if (characterControllerA->get_name() == "CharacterController")
			{
				characterController = characterControllerA;
				break;
			}
		}
	}

	// DEBUG DRAWING: make the debug reference node paths sibling of the reference node
	physicsMgr->get_reference_node_path_debug().reparent_to(
			window->get_render());
	framework.define_key("d", "toggleDebugDraw", &toggleDebugDraw, nullptr);

	// enable collision notify event: BTRigidBody_BTRigidBody_Collision
	physicsMgr->enable_throw_event(GamePhysicsManager::COLLISIONNOTIFY, true,
			0.1, "COLLISION");
	framework.define_key("COLLISION", "genricEventNotify", &genericEventNotify,
			nullptr);
	framework.define_key("COLLISIONOff", "collisionNotifyOff",
			&genericEventNotify, nullptr);

	// ghost overlap notify events: OVERLAP
	framework.define_key("OVERLAP", "overlapNotify", &genericEventNotify,
			nullptr);
	framework.define_key("OVERLAPOff", "overlapNotifyOff", &genericEventNotify,
			nullptr);

	// let ghost moving endlessly back and forth
	letMoveBackForth(NodePath::any_path(canGhost), 20.0,
			LPoint3f(-20.0, 0.0, 6.0), LPoint3f(20.0, 0.0, 6.0),
			LVecBase3f(0.0, 0.0, 90.0), LVecBase3f(180.0, 0.0, 90.0));

	/// first option: start the default update task for all drivers
	physicsMgr->start_default_update();
	playerRigidBody->set_update_callback(rigidBodyCallback);
	globalClock = ClockObject::get_global_clock();

	/// second option: start the custom update task for the drivers
//	updateTask = new GenericAsyncTask("updateControls", &updateControls,
//			nullptr);
//	framework.get_task_mgr().add(updateTask);
//	updateTask->set_sort(10);

	// set keys to control the vehicle's movement:
	framework.define_key(vehicleKeys[0], "forward", &vehicleController,
			(void*) vehicle.p());
	framework.define_key(vehicleKeys[1], "forward-stop", &vehicleController,
			(void*) vehicle.p());
	framework.define_key(vehicleKeys[2], "backward", &vehicleController,
			(void*) vehicle.p());
	framework.define_key(vehicleKeys[3], "backward-stop", &vehicleController,
			(void*) vehicle.p());
	framework.define_key(vehicleKeys[4], "left", &vehicleController,
			(void*) vehicle.p());
	framework.define_key(vehicleKeys[5], "left-stop", &vehicleController,
			(void*) vehicle.p());
	framework.define_key(vehicleKeys[6], "right", &vehicleController,
			(void*) vehicle.p());
	framework.define_key(vehicleKeys[7], "right-stop", &vehicleController,
			(void*) vehicle.p());
	framework.define_key(vehicleKeys[8], "brake", &vehicleController,
			(void*) vehicle.p());
	framework.define_key(vehicleKeys[9], "brake-stop", &vehicleController,
			(void*) vehicle.p());
	// vehicle notify events: MOVE/STEADY
	framework.define_key(moveEvent, "moveNotify", &vehicleEventNotify, nullptr);
	framework.define_key(steadyEvent, "steadyNotify", &vehicleEventNotify,
			nullptr);

	// tweak some character controller's parameters
	characterController->set_linear_accel(LVector3f(3.0));
	characterController->set_max_linear_speed(LVector3f(6.0));
	characterController->set_linear_friction(2.0);
	characterController->set_angular_accel(0.8);
	characterController->set_max_angular_speed(1.6);
	characterController->set_angular_friction(2.0);
	characterController->set_jump_speed(20.0);
	// set keys to control the character controller's movement:
	framework.define_key(characterKeys[0], "forward", &characterControllerClbk,
			(void*) characterController.p());
	framework.define_key(characterKeys[1], "forward-stop",
			&characterControllerClbk, (void*) characterController.p());
	framework.define_key(characterKeys[2], "backward", &characterControllerClbk,
			(void*) characterController.p());
	framework.define_key(characterKeys[3], "backward-stop",
			&characterControllerClbk, (void*) characterController.p());
	framework.define_key(characterKeys[4], "left", &characterControllerClbk,
			(void*) characterController.p());
	framework.define_key(characterKeys[5], "left-stop",
			&characterControllerClbk, (void*) characterController.p());
	framework.define_key(characterKeys[6], "right", &characterControllerClbk,
			(void*) characterController.p());
	framework.define_key(characterKeys[7], "right-stop",
			&characterControllerClbk, (void*) characterController.p());
	framework.define_key(characterKeys[8], "jump", &characterControllerClbk,
			(void*) characterController.p());
	framework.define_key(characterKeys[9], "jump-stop",
			&characterControllerClbk, (void*) characterController.p());
	// vehicle notify events: ONGROUND/INAIR
	framework.define_key("CharacterController_OnGround", "onGroundNotify",
			&characterControllerEventNotify, nullptr);
	framework.define_key("CharacterController_InAir", "inAirNotify",
			&characterControllerEventNotify, nullptr);

	// get ghost's overlapping objects
	framework.define_key("f4", "getGhostOverlappingObjects",
			&getGhostOverlappingObjects, (void*) canGhost);

	// write to bam file on exit
	window->get_graphics_window()->set_close_request_event(
			"close_request_event");
	framework.define_key("close_request_event", "writeToBamFile",
			&writeToBamFileAndExit, (void*) &bamFileName);

	// place camera trackball (local coordinate)
	PT(Trackball) trackball = DCAST(Trackball,
			window->get_mouse().find("**/+Trackball").node());
	trackball->set_pos(10.0, 200.0, -30.0);
	trackball->set_hpr(0.0, 10.0, 0.0);

	// do the main loop, equals to call app.run() in python
///	framework.main_loop();
	// This is a simpler way to do stuff every frame,
	// if you're too lazy to create a task.
	Thread *current_thread = Thread::get_current_thread();
	while (framework.do_frame(current_thread))
	{
		// Step the interval manager
		CIntervalManager::get_global_ptr()->step();
	}

	return (0);
}

///functions' definitions
// read scene from a file
static bool readFromBamFile(string fileName)
{
	return GamePhysicsManager::get_global_ptr()->read_from_bam_file(fileName);
}

// write scene to a file (and exit)
static void writeToBamFileAndExit(const Event*, void *data)
{
	string fileName = *reinterpret_cast<string*>(data);
	GamePhysicsManager::get_global_ptr()->write_to_bam_file(fileName);
	/// second option: remove custom update updateTask
//	framework.get_task_mgr().remove(updateTask);

/// this is for testing explicit removal and destruction of all elements
	GamePhysicsManager *physicsMgr = GamePhysicsManager::get_global_ptr();
	// destroy vehicles
	while (physicsMgr->get_num_vehicles() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_vehicle(
				NodePath::any_path(physicsMgr->get_vehicle(0)));
///		delete DCAST(BT3Vehicle, physicsMgr->get_vehicle(0).node()); //ERROR
	}
	// destroy characterControllers
	while (physicsMgr->get_num_character_controllers() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_character_controller(
				NodePath::any_path(physicsMgr->get_character_controller(0)));
///		delete DCAST(BT3CharacterControllers, physicsMgr->get_character_controller(0).node()); //ERROR
	}
	// destroy constraints
	while (physicsMgr->get_num_constraints() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_constraint(
				NodePath::any_path(physicsMgr->get_constraint(0)));
///		delete DCAST(BT3Constraint, physicsMgr->get_constraint(0).node()); //ERROR
	}
	// destroy rigid bodies
	while (physicsMgr->get_num_rigid_bodies() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_rigid_body(
				NodePath::any_path(physicsMgr->get_rigid_body(0)));
///		delete DCAST(BT3RigidBody, physicsMgr->get_rigid_body(0).node()); //ERROR
	}
	// destroy ghosts
	while (physicsMgr->get_num_ghosts() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_ghost(NodePath::any_path(physicsMgr->get_ghost(0)));
///		delete DCAST(BT3Ghost, physicsMgr->get_ghost(0).node()); //ERROR
	}
	// delete managers
	delete GamePhysicsManager::get_global_ptr();
	// close the window framework
	framework.close_framework();
	//
	exit(0);
}

// custom update task for controls
static AsyncTask::DoneStatus updateControls(GenericAsyncTask*, void *data)
{
	// call update for controls
	double dt = ClockObject::get_global_clock()->get_dt();
	playerRigidBody->update(dt);
	//
	return AsyncTask::DS_cont;
}

// rigid body update callback function
static char delay = 0;
static void rigidBodyCallback(PT(BT3RigidBody) rigidBody)
{
	if (rigidBody != playerRigidBody)
	{
		return;
	}
	if (delay >= 60)
	{
		float currentVelSize = rigidBody->get_linear_velocity().length();
		cout << "box velocity: " << currentVelSize << endl;
		delay = 0;
	}
	else
	{
		delay++;
	}
}

// vehicle event notify
void vehicleEventNotify(const Event *e, void *data)
{
	PT(PandaNode) object0 = DCAST(PandaNode,
			e->get_parameter(0).get_ptr());

	cout
			<< string("got '") + e->get_name() + string("' for '")
					+ object0->get_name() + string("' character controller.")
			<< endl;
}

// character controller event notify
void characterControllerEventNotify(const Event *e, void *data)
{
	PT(PandaNode) object0 = DCAST(PandaNode,
			e->get_parameter(0).get_ptr());

	cout
			<< string("got '") + e->get_name() + string("' for '")
					+ object0->get_name() + string("' vehicle.") << endl;
}

static plist<PT(CLerpNodePathInterval)> targetPosInterval1;
static plist<PT(CLerpNodePathInterval)> targetPosInterval2;
static plist<PT(CLerpNodePathInterval)> targetHprInterval1;
static plist<PT(CLerpNodePathInterval)> targetHprInterval2;
static plist<PT(CMetaInterval)> targetPace;
// let target move back and forth
void letMoveBackForth(const NodePath &target, float duration,
		const LPoint3f &startPos, const LPoint3f &endPos,
		const LVecBase3f &startHpr, const LVecBase3f &endHpr)
{
	// Create the lerp intervals needed to walk back and forth
	float walkTime = duration * 0.9 / 2.0;
	float turnTime = duration * 0.1 / 2.0;
	// walk
	targetPosInterval1.push_back(
			new CLerpNodePathInterval(
					string("targetPosInterval1")
							+ str(targetPosInterval1.size()), walkTime,
					CLerpInterval::BT_no_blend, true, false, target,
					NodePath()));
	targetPosInterval1.back()->set_start_pos(startPos);
	targetPosInterval1.back()->set_end_pos(endPos);

	targetPosInterval2.push_back(
			new CLerpNodePathInterval(
					string("targetPosInterval2")
							+ str(targetPosInterval2.size()), walkTime,
					CLerpInterval::BT_no_blend, true, false, target,
					NodePath()));
	targetPosInterval2.back()->set_start_pos(endPos);
	targetPosInterval2.back()->set_end_pos(startPos);

	// turn
	targetHprInterval1.push_back(
			new CLerpNodePathInterval(
					string("targetHprInterval1")
							+ str(targetHprInterval1.size()), turnTime,
					CLerpInterval::BT_no_blend, true, false, target,
					NodePath()));
	targetHprInterval1.back()->set_start_hpr(startHpr);
	targetHprInterval1.back()->set_end_hpr(endHpr);

	targetHprInterval2.push_back(
			new CLerpNodePathInterval(
					string("targetHprInterval2")
							+ str(targetHprInterval2.size()), turnTime,
					CLerpInterval::BT_no_blend, true, false, target,
					NodePath()));
	targetHprInterval2.back()->set_start_hpr(endHpr);
	targetHprInterval2.back()->set_end_hpr(startHpr);

	// Create and play the sequence that coordinates the intervals
	targetPace.push_back(
			new CMetaInterval(string("targetPace") + str(targetPace.size())));
	targetPace.back()->add_c_interval(targetPosInterval1.back(), 0,
			CMetaInterval::RS_previous_end);
	targetPace.back()->add_c_interval(targetHprInterval1.back(), 0,
			CMetaInterval::RS_previous_end);
	targetPace.back()->add_c_interval(targetPosInterval2.back(), 0,
			CMetaInterval::RS_previous_end);
	targetPace.back()->add_c_interval(targetHprInterval2.back(), 0,
			CMetaInterval::RS_previous_end);
	targetPace.back()->loop();
}

// vehicle controller
void vehicleController(const Event *e, void *data)
{
	string name = e->get_name();
	PT(BT3Vehicle) vehicle = reinterpret_cast<BT3Vehicle*>(data);
	if (name == vehicleKeys[0])
	{
		vehicle->set_move_forward(true);
	}
	else if (name == vehicleKeys[1])
	{
		vehicle->set_move_forward(false);
	}
	else if (name == vehicleKeys[2])
	{
		vehicle->set_move_backward(true);
	}
	else if (name == vehicleKeys[3])
	{
		vehicle->set_move_backward(false);
	}
	else if (name == vehicleKeys[4])
	{
		vehicle->set_rotate_head_left(true);
	}
	else if (name == vehicleKeys[5])
	{
		vehicle->set_rotate_head_left(false);
	}
	else if (name == vehicleKeys[6])
	{
		vehicle->set_rotate_head_right(true);
	}
	else if (name == vehicleKeys[7])
	{
		vehicle->set_rotate_head_right(false);
	}
	else if (name == vehicleKeys[8])
	{
		vehicle->set_brake(true);
	}
	else if (name == vehicleKeys[9])
	{
		vehicle->set_brake(false);
	}
}

// character controller
void characterControllerClbk(const Event *e, void *data)
{
	string name = e->get_name();
	PT(BT3CharacterController) characterController =
			reinterpret_cast<BT3CharacterController*>(data);
	if (name == characterKeys[0])
	{
		characterController->set_move_forward(true);
	}
	else if (name == characterKeys[1])
	{
		characterController->set_move_forward(false);
	}
	else if (name == characterKeys[2])
	{
		characterController->set_move_backward(true);
	}
	else if (name == characterKeys[3])
	{
		characterController->set_move_backward(false);
	}
	else if (name == characterKeys[4])
	{
		characterController->set_rotate_head_left(true);
	}
	else if (name == characterKeys[5])
	{
		characterController->set_rotate_head_left(false);
	}
	else if (name == characterKeys[6])
	{
		characterController->set_rotate_head_right(true);
	}
	else if (name == characterKeys[7])
	{
		characterController->set_rotate_head_right(false);
	}
	else if (name == characterKeys[8])
	{
		characterController->set_jump(true);
	}
	else if (name == characterKeys[9])
	{
		characterController->set_jump(false);
	}
}

// get ghost's overlapping objects
void getGhostOverlappingObjects(const Event *e, void *data)
{
	PT(BT3Ghost) ghost = reinterpret_cast<BT3Ghost*>(data);
	cout << "Overlapping objects: ";
	for (int i = 0; i < ghost->get_num_overlapping_objects(); ++i)
	{
		cout << ghost->get_overlapping_object(i).get_name() << ", ";
	}
	cout << endl;
}

