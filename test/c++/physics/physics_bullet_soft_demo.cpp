/**
 * \file physics_bt3_soft_demo.cpp
 *
 * \date 2017-05-06
 * \author consultit
 */

#include "common_physics.h"
#include <texturePool.h>
#include <ropeNode.h>
#include <geomTriangles.h>
#include <omniBoundingVolume.h>

static vector<NodePath> graphicNPs;
static NodePath softBodyNP;

static inline float	UnitRand()
{
	return(rand()/(float)RAND_MAX);
}

static inline float	SignedUnitRand()
{
	return(UnitRand()*2-1);
}

static inline LVecBase3f Vector3Rand()
{
	const LVecBase3f p=LVecBase3f(SignedUnitRand(),SignedUnitRand(),SignedUnitRand());
	return(p.normalized());
}

static void destroyObjects()
{
	GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
	// destroy all soft bodies
	while (physicsMgr->get_num_soft_bodies() > 0)
	{
		// destroy the first one on every cycle
		physicsMgr->destroy_soft_body(
				NodePath::any_path(physicsMgr->get_soft_body(0)));
	}
	softBodyNP.clear();
	//remove rigid bodies but first (== sceneRigidBody)
	while (physicsMgr->get_num_rigid_bodies() > 1)
	{
		// destroy the second one on every cycle
		physicsMgr->destroy_rigid_body(
				NodePath::any_path(physicsMgr->get_rigid_body(1)));
	}
	//remove graphic nodes
	for (auto gNP: graphicNPs)
	{
		gNP.remove_node();
	}
	graphicNPs.clear();
}

static void Ctor_RbUpStack(int count, const LPoint3f& org)
{
	GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
	float mass = 10;
	static NodePath modelNP[] =
	{	getModelAnims("modelNP1", 1.0, 0, &playerAnimCtls), getModelAnims(
				"modelNP2", 1.0, 1, &playerAnimCtls), getModelAnims("modelNP3", 1.0,
				4, &playerAnimCtls)};
	static string shape[] =
	{	"box", "capsule", "cylinder"};
	static string axis[] =
	{	"z", "z", "y"};
	static int dimIdx[] =
	{	0, 2, 1};

	for (int i = 0; i < count; ++i)
	{
		createRigidBodyXYZ(modelNP[i % 3], "RbUpStack" + str(i),
				shape[i % 3], axis[i % 3], org + LPoint3f(0, 0, 2 + 12 * i),
				LVecBase3f(45.0 * i, -30.0 * (count - i), + 60.0 * (i % 3)),
				LVecBase3f(dimIdx[i % 3] == 0 ? 8 : 2,
						dimIdx[i % 3] == 1 ? 8 : 2,
						dimIdx[i % 3] == 2 ? 8 : 2),
				false);
		PT(BT3RigidBody)rigidBody = physicsMgr->get_rigid_body(
				physicsMgr->get_num_rigid_bodies() - 1);
		rigidBody->set_mass(mass);
		rigidBody->switch_body_type(BT3RigidBody::DYNAMIC);
		graphicNPs.push_back(rigidBody->get_owner_object());
	}
}

static void Ctor_LinearStair(const LPoint3f& org, const LVecBase3f& dims,
		int count, int modelFileIdx, string shape, string upAxis)
{
	GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
	string name = string("LinearStair");
	NodePath modelNP = getModelAnims(name, 1.0, modelFileIdx, &playerAnimCtls);
	for (int i = 0; i < count; ++i)
	{
		string bodyName = "body" + name + str(i);
		LPoint3f pos = org
		+ LVecBase3f(-dims.get_x() * i, 0, dims.get_z() * i);
		createRigidBodyXYZ(modelNP, bodyName, shape, upAxis, pos,
				LVecBase3f::zero(), dims, false);
		PT(BT3RigidBody)rigidBody = physicsMgr->get_rigid_body(
				physicsMgr->get_num_rigid_bodies() - 1);
		rigidBody->set_mass(0.0);
		rigidBody->switch_body_type(BT3RigidBody::STATIC);
		graphicNPs.push_back(rigidBody->get_owner_object());
		rigidBody->get_collision_object_attrs()->set_friction(1);
	}
}

NodePath procedurallyGenerating3DModels(const float* vertices, const int* triangles,
		int ntriangles, bool bt3vertices = true, const string& name = "gnode")
{
	/// Defining your own GeomVertexFormat
	PT(GeomVertexArrayFormat)array = new GeomVertexArrayFormat();
	array->add_column(InternalName::make("vertex"), 3, Geom::NT_float32,
			Geom::C_point);
	array->add_column(InternalName::make("normal"), 3, Geom::NT_float32,
			Geom::C_normal);
	//
	PT(GeomVertexFormat) unregistered_format = new GeomVertexFormat();
	unregistered_format->add_array(array);
	//
	CPT(GeomVertexFormat) format1 = GeomVertexFormat::register_format(unregistered_format);
	/// Creating and filling a GeomVertexData
	PT(GeomVertexData) vdata = new GeomVertexData(name + "VD", format1, Geom::UH_static);
	//
	int numVerts = 0;
	int i;
	for (i = 0; i < ntriangles * 3; ++i)
	{
		numVerts = max(triangles[i], numVerts);
	}
	++numVerts;
	vdata->set_num_rows(numVerts);
	//
	GeomVertexRewriter vertexRW = GeomVertexRewriter(vdata, "vertex");
	GeomVertexRewriter normalRW = GeomVertexRewriter(vdata, "normal");
	//
	if (bt3vertices)
	{
		// vertices in bullet space; reset normal
		for (i = 0; i < numVerts * 3; i += 3)
		{
			btVector3 bt3vert(vertices[i], vertices[i + 1], vertices[i + 2]);
			vertexRW.add_data3f(bt3::btVector3_to_LVecBase3(bt3vert));
			normalRW.add_data3f(LVector3f::zero());
		}
	}
	else
	{
		// vertices in panda3d space; reset normal
		for (i = 0; i < numVerts * 3; i += 3)
		{
			LPoint3f p3dvert(vertices[i], vertices[i + 1], vertices[i + 2]);
			vertexRW.add_data3f(p3dvert);
			normalRW.add_data3f(LVector3f::zero());
		}
	}
	/// Creating the GeomPrimitive objects and compute normals per vertex
	/// note: for each face the normal is computed and added to each normal of
	/// its vertices; normals are not normalized at this stage, so a vertex
	/// normal is more influenced by triangles with wider area. All vertex
	/// normals are normalized all together in a second step.
	PT(GeomTriangles) prim = new GeomTriangles(Geom::UH_static);
	for (i = 0; i < ntriangles * 3; i += 3)
	{
		int i0 = triangles[i], i1 = triangles[i + 1], i2 = triangles[i + 2];
		prim->add_vertex(i0);
		prim->add_vertex(i1);
		prim->add_vertex(i2);
		// compute face normal (not normalized): (p1-p0)X(p2-p0)
		vertexRW.set_row(i0);
		LPoint3f p0 = vertexRW.get_data3f();
		vertexRW.set_row(i1);
		LPoint3f p1 = vertexRW.get_data3f();
		vertexRW.set_row(i2);
		LPoint3f p2 = vertexRW.get_data3f();
		LVector3f faceNorm = (p1 - p0).cross(p2 - p0);
		// add faceNorm to each vertex normal (not normalized)
		normalRW.set_row(i0);
		LVector3f n0 = normalRW.get_data3f();
		normalRW.set_row(i0);
		normalRW.set_data3f(n0 + faceNorm);
		//
		normalRW.set_row(i1);
		LVector3f n1 = normalRW.get_data3f();
		normalRW.set_row(i1);
		normalRW.set_data3f(n1 + faceNorm);
		//
		normalRW.set_row(i2);
		LVector3f n2 = normalRW.get_data3f();
		normalRW.set_row(i2);
		normalRW.set_data3f(n2 + faceNorm);
	}
	prim->close_primitive();
	// normalize all vertex normals
	for (i = 0, normalRW.set_row(0); i < numVerts; i++ )
	{
		normalRW.set_row(i);
		LVector3f n = normalRW.get_data3f();
		n.normalize();
		normalRW.set_row(i);
		normalRW.set_data3f(n);
	}
	// Putting your new geometry in the scene graph
	PT(Geom) geom = new Geom(vdata);
	geom->add_primitive(prim);
	PT(GeomNode) node = new GeomNode(name);
	node->add_geom(geom);
	//
	return NodePath::any_path(node);
}

LVecBase3f operator*(const LVecBase3f& v1, const LVecBase3f& v2)
{
	return LVecBase3f(v1[0] * v2[0], v1[1] * v2[1], v1[2] * v2[2]);
}

static PT(BT3SoftBody) Ctor_SoftBox(const LPoint3f& p,const LVector3f& s)
{
	GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
	///Hack: convex hull soft body needs any way a Geom node and take into
	///account only its vertices.
	const LVector3f h=s*0.5;
	const LPoint3f c[]=
	{	p+h*LVector3f(+1,-1,-1),
		p+h*LVector3f(-1,-1,-1),
		p+h*LVector3f(+1,-1,+1),
		p+h*LVector3f(-1,-1,+1),
		p+h*LVector3f(+1,+1,-1),
		p+h*LVector3f(-1,+1,-1),
		p+h*LVector3f(+1,+1,+1),
		p+h*LVector3f(-1,+1,+1)};
	int numVert = sizeof(c)/sizeof(LPoint3f);
	float* vertices = new float[numVert * 3];
	for (int i = 0, v = 0; i < numVert * 3; i += 3, v++)
	{
		vertices[i] = c[v][0];
		vertices[i + 1] = c[v][1];
		vertices[i + 2] = c[v][2];
	}
	//create fake triangles
	int ntriangles = numVert / 3 + ((numVert % 3) != 0 ? 1: 0);
	int* triangles = new int[ntriangles * 3];
	for(int i = 0; i < ntriangles * 3; i += 3)
	{
		triangles[i] = i % numVert;
		triangles[i + 1] = (i + 1) % numVert;
		triangles[i + 2] = (i + 2) % numVert;
	}
	//1: the graphic object (NodePath)
	NodePath softBox = procedurallyGenerating3DModels(vertices, triangles,
			ntriangles, false, "SoftBox");
	graphicNPs.push_back(softBox);
	graphicNPs.back().set_pos_hpr_scale(
			LPoint3f(0.0, 0.0, 12.0),//position
			LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
			LVecBase3f(1.0, 1.0, 1.0));//scale
	//2: create the empty soft body
	softBodyNP = physicsMgr->create_soft_body("SoftBoxBody");
	PT(BT3SoftBody) softBoxBody = DCAST(BT3SoftBody, softBodyNP.node());
	softBodyNP.set_color(LVecBase4f(Vector3Rand(), 1.0));
	softBodyNP.node()->set_bounds(new OmniBoundingVolume());
	softBodyNP.node()->set_final(true);
	//6: visual state: CONVEX_HULL creates a new node that inherits render state
//	softBodyNP.set_two_sided(true);
//	softBodyNP.show_bounds();
//	softBodyNP.set_render_mode_wireframe();
	//3: set CONSTRUCTION PARAMETERS
	softBoxBody->set_group_mask(groupMask);
	softBoxBody->set_collide_mask(collideMask);
	softBoxBody->set_body_type(BT3SoftBody::CONVEX_HULL);
	//4: setup the body with graphicNPs.back() as owner
	softBoxBody->set_owner_object(graphicNPs.back());
	softBoxBody->setup();
	//CONVEX_HULL create a node with name = softBodyNP.get_name() + "_convexHull"
	//parented to softBodyNP's parent
	NodePath nodeCHNP = softBodyNP.get_parent().find(softBodyNP.get_name() + "_convexHull");
	graphicNPs.push_back(nodeCHNP);
	//5: set DYNAMIC PARAMETERS
	softBoxBody->generate_bending_constraints(2);
	//cleanup
	softBox.remove_node();
	delete[] vertices;
	delete[] triangles;
	//
	return softBoxBody;
}

static PT(BT3RigidBody)	Ctor_BigPlate(float mass,float height)
{
	GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
	NodePath bigPlateNP = getModelAnims("BigPlateNP", 1.0, 8, &playerAnimCtls);
	createRigidBodyXYZ(bigPlateNP, "BigPlate", "box", "z", LPoint3f(0, 0, height),
			LVecBase3f(0, 0, 0), LVecBase3f(10,10,2), false);
	PT(BT3RigidBody)rigidBody = physicsMgr->get_rigid_body(
			physicsMgr->get_num_rigid_bodies() - 1);
	rigidBody->set_mass(mass);
	rigidBody->switch_body_type(BT3RigidBody::DYNAMIC);
	rigidBody->get_collision_object_attrs()->set_friction(1);
	graphicNPs.push_back(rigidBody->get_owner_object());
	return rigidBody;
}


void RopeGeom(bool init)
{
	if (init)
	{
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//create
		PT(TextureStage)sharedTS0 = new TextureStage("sharedTS0");
		PT(Texture)ropeTex = TexturePool::load_texture(Filename("red.tga"));
		/// Rope Geom
		PT(GeomNode)rope = new GeomNode("SoftRopeGeom");
		graphicNPs.push_back(NodePath(rope));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 40.0),
				LVecBase3f(0.0, 0.0, 0.0),
				LVecBase3f(1.5, 1.5, 1.5));
		graphicNPs.back().reparent_to(physicsMgr->get_reference_node_path());
		// Rope Geom texturing
		graphicNPs.back().set_tex_scale(sharedTS0, 1.0, 1.0);
		graphicNPs.back().set_texture(sharedTS0, ropeTex, 1);
		// set various creation parameters as string for other soft bodies
		setParametersBeforeCreation(GamePhysicsManager::SOFTBODY, "SoftRopeGeom");
		physicsMgr->set_parameter_value(GamePhysicsManager::SOFTBODY,
				"body_type", "rope");
		physicsMgr->set_parameter_value(GamePhysicsManager::SOFTBODY, "rope_points",
				"-15, 0, 0:15, 0, 0");
		physicsMgr->set_parameter_value(GamePhysicsManager::SOFTBODY, "rope_fixeds",
				"1");
		physicsMgr->set_parameter_value(GamePhysicsManager::SOFTBODY, "rope_res",
				"14");//curve->get_num_vertices() - 2;
		// create the rope soft body
		softBodyNP = physicsMgr->create_soft_body("RopeGeomBody");
		ropeGeomBody = DCAST(BT3SoftBody, softBodyNP.node());
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();
	}
	else
	{
		destroyObjects();
	}
}

void RopeNURBS(bool init)
{
	if (init)
	{
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		PT(TextureStage)sharedTS0 = new TextureStage("sharedTS0");
		PT(Texture)ropeTex = TexturePool::load_texture(Filename("iron.jpg"));
		/// Rope NURBS
		PT(NurbsCurveEvaluator)curve = new NurbsCurveEvaluator();
		curve->set_order(3);
		curve->reset(16);
		PRINT_DEBUG("vertices: " << curve->get_num_vertices() << endl
				<< "segments: " << curve->get_num_segments() << endl
				<< "knots: " << curve->get_num_knots());
		PT(RopeNode) ropeNURBS = new RopeNode("SoftRopeNURBS");
		ropeNURBS->set_curve(curve);
		ropeNURBS->set_render_mode(RopeNode::RM_tube);
		ropeNURBS->set_uv_mode(RopeNode::UV_parametric);
		ropeNURBS->set_thickness(1.0);
		graphicNPs.push_back(NodePath(ropeNURBS));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 40.0),
				LVecBase3f(0.0, 0.0, 0.0),
				LVecBase3f(1.5, 1.5, 1.5));
		graphicNPs.back().reparent_to(physicsMgr->get_reference_node_path());
		// Rope NURBS texturing
		graphicNPs.back().set_tex_scale(sharedTS0, 1.0, 1.0);
		graphicNPs.back().set_texture(sharedTS0, ropeTex, 1);
		// set various creation parameters as string for other soft bodies
		setParametersBeforeCreation(GamePhysicsManager::SOFTBODY, "");
		// create the soft body (default rope)
		softBodyNP = physicsMgr->create_soft_body("RopeNURBSBody");
		PT(BT3SoftBody)ropeNURBSBody = DCAST(BT3SoftBody, softBodyNP.node());
		// test before setup parameters' settings
		ropeNURBSBody->set_body_type(BT3SoftBody::TETGEN_MESH);//error
		ropeNURBSBody->set_rope_points(
				{	LPoint3f(-15, 0, 0), LPoint3f(15, 0, 0)});
//		ropeNURBSBody->set_rope_res(curve->get_num_vertices() - 2); setup automatically
		ropeNURBSBody->set_rope_fixeds(0x0 | 0x2);
		ropeNURBSBody->set_owner_object(graphicNPs[0]);
		ropeNURBSBody->setup();
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();
	}
	else
	{
		destroyObjects();
	}
}

void Init_Cloth(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Cloth
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		PT(GeomNode)cloth = new GeomNode("Cloth");
		graphicNPs.push_back(NodePath(cloth));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 20.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		graphicNPs.back().set_texture(TexturePool::load_texture(Filename("terrain.png")));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body("ClothBody");
		PT(BT3SoftBody) clothBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		clothBody->set_group_mask(groupMask);
		clothBody->set_collide_mask(collideMask);
		clothBody->set_body_type(BT3SoftBody::PATCH);
		const float s = 8;
		clothBody->set_patch_points(
				{	LPoint3f(s, -s, 0),
					LPoint3f(-s, -s, 0),
					LPoint3f(s, s, 0),
					LPoint3f(-s, s, 0),});
		clothBody->set_patch_resxy(
				{	31, 31});
		clothBody->set_patch_fixeds(0x1 | 0x2 | 0x4 | 0x8);
		clothBody->set_patch_gendiags(true);
		//4: setup the body with graphicNPs.back() as owner
		clothBody->set_owner_object(graphicNPs.back());
		clothBody->setup();
		//5: set DYNAMIC PARAMETERS
		clothBody->get_collision_object_attrs()->set_shape_margin(0.5);
		BT3SoftBodyMaterial material = clothBody->append_material();
		material.set_linear_stiffness_coefficient(0.4);
		material.set_flags(BT3SoftBodyMaterial::DebugDraw);
		clothBody->generate_bending_constraints(2, material);
		clothBody->set_total_mass(150);
		//6: visual state
		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();

		// rigid bodies
		Ctor_RbUpStack(10, LPoint3f(0.0, 0.0, 20.0));
	}
	else
	{
		destroyObjects();
	}
}

void Init_Pressure(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Pressure
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		PT(GeomNode)pressure = new GeomNode("Pressure");
		graphicNPs.push_back(NodePath(pressure));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 0.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body("PressureBody");
		PT(BT3SoftBody) pressureBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		pressureBody->set_group_mask(groupMask);
		pressureBody->set_collide_mask(collideMask);
		pressureBody->set_body_type(BT3SoftBody::ELLIPSOID);
		pressureBody->set_ellipsoid_center(LPoint3f(-35, 0, 25));
		pressureBody->set_ellipsoid_radius(LVecBase3f(1, 1, 1) * 3);
		pressureBody->set_ellipsoid_res(512);
		//4: setup the body with graphicNPs.back() as owner
		pressureBody->set_owner_object(graphicNPs.back());
		pressureBody->setup();
		//5: set DYNAMIC PARAMETERS
		pressureBody->get_material(0).set_linear_stiffness_coefficient(0.1);
		pressureBody->get_config().set_dynamic_friction_coefficient(1);
		pressureBody->get_config().set_damping_coefficient(0.001);
		pressureBody->get_config().set_pressure_coefficient(2500);
		pressureBody->set_total_mass(30,true);
		//6: visual state
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();

		// rigid bodies
		Ctor_BigPlate(15, 10);
		Ctor_LinearStair(LPoint3f(0, 0, 3.6), LVecBase3f(4, 10, 2), 10,
				8, "box", "z");
	}
	else
	{
		destroyObjects();
	}
}

void Init_Volume(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Volume
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		PT(GeomNode)volume = new GeomNode("Volume");
		graphicNPs.push_back(NodePath(volume));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 0.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body("VolumeBody");
		PT(BT3SoftBody) volumeBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		volumeBody->set_group_mask(groupMask);
		volumeBody->set_collide_mask(collideMask);
		volumeBody->set_body_type(BT3SoftBody::ELLIPSOID);
		volumeBody->set_ellipsoid_center(LPoint3f(-35, 0, 25));
		volumeBody->set_ellipsoid_radius(LVecBase3f(1, 1, 1) * 3);
		volumeBody->set_ellipsoid_res(512);
		//4: setup the body with graphicNPs.back() as owner
		volumeBody->set_owner_object(graphicNPs.back());
		volumeBody->setup();
		//5: set DYNAMIC PARAMETERS
		volumeBody->get_material(0).set_linear_stiffness_coefficient(0.45);
		volumeBody->get_config().set_volume_conversation_coefficient(20);
		volumeBody->set_total_mass(50,true);
		volumeBody->set_pose(true, false);
		//6: visual state
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();

		// rigid bodies
		Ctor_BigPlate(15, 10);
		Ctor_LinearStair(LPoint3f(0, 0, 3.6), LVecBase3f(4, 10, 2), 10,
				8, "box", "z");
	}
	else
	{
		destroyObjects();
	}
}

void Init_Ropes(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Ropes
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		PT(TextureStage)sharedTS0 = new TextureStage("sharedTS0");
		PT(Texture)ropeTex = TexturePool::load_texture(Filename("red.tga"));
		const int n = 15;
		for(int i = 0; i < n; ++i)
		{
			//1: the graphic object (NodePath)
			PT(GeomNode)rope = new GeomNode("Rope" + str(i));
			graphicNPs.push_back(NodePath(rope));
			graphicNPs.back().set_pos_hpr_scale(
					LPoint3f(0.0, 0.0, 20.0),//position
					LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
					LVecBase3f(1.0, 1.0, 1.0));//scale
			//Rope texturing
			graphicNPs.back().set_tex_scale(sharedTS0, 1.0, 1.0);
			graphicNPs.back().set_texture(sharedTS0, ropeTex, 1);
			//2: create the empty soft body
			softBodyNP = physicsMgr->create_soft_body("RopeBody" + str(i));
			PT(BT3SoftBody) ropeBody = DCAST(BT3SoftBody, softBodyNP.node());
			//3: set CONSTRUCTION PARAMETERS
			ropeBody->set_group_mask(groupMask);
			ropeBody->set_collide_mask(collideMask);
			ropeBody->set_body_type(BT3SoftBody::ROPE);
			ropeBody->set_rope_points(
					{	LPoint3f(10, i * 0.25, 0), LPoint3f(-10, i * 0.25, 0)});
			ropeBody->set_rope_res(16);
			ropeBody->set_rope_fixeds(1 | 2);
			//4: setup the body with graphicNPs.back() as owner
			ropeBody->set_owner_object(graphicNPs.back());
			ropeBody->setup();
			//5: set DYNAMIC PARAMETERS
			ropeBody->get_config().set_positions_solver_iterations(4);
			ropeBody->get_material(0).set_linear_stiffness_coefficient(
					0.1 + (i / (float)(n-1)) * 0.9);
			ropeBody->set_total_mass(20);
			//6: visual state
//			softBodyNP.set_two_sided(true);
//			softBodyNP.show_bounds();
//			softBodyNP.set_render_mode_wireframe();
		}
	}
	else
	{
		destroyObjects();
	}
}

void Init_RopeAttach(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_RopeAttach
		static GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		static PT(TextureStage)sharedTS0 = new TextureStage("sharedTS0");
		static PT(Texture)ropeTex = TexturePool::load_texture(Filename("yellow.tga"));
//		pdemo->m_softBodyWorldInfo.m_sparsesdf.RemoveReferences(0); TODO ?
		struct Functors
		{
			static PT(BT3SoftBody) CtorRope(const LPoint3f& p)
			{
				//1: the graphic object (NodePath)
				PT(GeomNode)rope = new GeomNode("Rope" + str(p.get_x()));
				graphicNPs.push_back(NodePath(rope));
				graphicNPs.back().set_pos_hpr_scale(
						LPoint3f(0.0, 0.0, 0.0),//position
						LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
						LVecBase3f(1.0, 1.0, 1.0));//scale
				//Rope texturing
				graphicNPs.back().set_tex_scale(sharedTS0, 1.0, 1.0);
				graphicNPs.back().set_texture(sharedTS0, ropeTex, 1);
				//2: create the empty soft body
				softBodyNP = physicsMgr->create_soft_body("RopeBody" + str(p.get_x()));
				PT(BT3SoftBody) ropeBody = DCAST(BT3SoftBody, softBodyNP.node());
				//3: set CONSTRUCTION PARAMETERS
				ropeBody->set_group_mask(groupMask);
				ropeBody->set_collide_mask(collideMask);
				ropeBody->set_body_type(BT3SoftBody::ROPE);
				ropeBody->set_rope_points(
						{	p, LPoint3f(p + LVector3f(-10, 0, 0))});
				ropeBody->set_rope_res(8);
				ropeBody->set_rope_fixeds(1);
				//4: setup the body with graphicNPs.back() as owner
				ropeBody->set_owner_object(graphicNPs.back());
				ropeBody->setup();
				//5: set DYNAMIC PARAMETERS
				ropeBody->set_total_mass(50);
				//6: visual state
//				softBodyNP.set_two_sided(true);
//				softBodyNP.show_bounds();
//				softBodyNP.set_render_mode_wireframe();
				return ropeBody;
			}
		};
		// rigid bodies
		NodePath modelNP1 = getModelAnims("modelNP1", 1.0, 8, &playerAnimCtls);
		createRigidBodyXYZ(modelNP1, "boxZ", "box", "z",
				LPoint3f(-12, 0, 8 + 20), LVecBase3f(0.0, 0.0, 0.0),
				LVecBase3f(4, 4, 12), false);
		PT(BT3RigidBody)rigidBody = physicsMgr->get_rigid_body(
				physicsMgr->get_num_rigid_bodies() - 1);
		//hack: change mass
		rigidBody->set_mass(50);
		rigidBody->switch_body_type(BT3RigidBody::DYNAMIC);
		graphicNPs.push_back(rigidBody->get_owner_object());
		//
		PT(BT3SoftBody) psb0 = Functors::CtorRope(LPoint3f(0, -1, 8 + 20));
		PT(BT3SoftBody) psb1 = Functors::CtorRope(LPoint3f(0, 1, 8 + 20));
		psb0->append_anchor(psb0->get_num_nodes() -1, rigidBody);
		psb1->append_anchor(psb1->get_num_nodes() -1, rigidBody);
	}
	else
	{
		destroyObjects();
	}
}

void Init_ClothAttach(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClothAttach
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		PT(GeomNode)clothAttach = new GeomNode("ClothAttach");
		graphicNPs.push_back(NodePath(clothAttach));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 0.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		graphicNPs.back().set_texture(TexturePool::load_texture(Filename("heightfield.png")));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body("ClothAttachBody");
		PT(BT3SoftBody) clothAttachBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		clothAttachBody->set_group_mask(groupMask);
		clothAttachBody->set_collide_mask(collideMask);
		clothAttachBody->set_body_type(BT3SoftBody::PATCH);
		const float s = 4;
		const float h = 6 + 40;
		const int r = 9;
		clothAttachBody->set_patch_points(
				{
					LPoint3f(s, -s, h),
					LPoint3f(-s, -s, h),
					LPoint3f(s, s, h),
					LPoint3f(-s, s, h),
				});
		clothAttachBody->set_patch_resxy(
				{	r, r});
		clothAttachBody->set_patch_fixeds(0x4 | 0x8);
		clothAttachBody->set_patch_gendiags(true);
		//4: setup the body with graphicNPs.back() as owner
		clothAttachBody->set_owner_object(graphicNPs.back());
		clothAttachBody->setup();
		//5: set DYNAMIC PARAMETERS
		//6: visual state
		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();

		// rigid bodies
		NodePath modelNP1 = getModelAnims("modelNP1", 1.0, 4, &playerAnimCtls);
		createRigidBodyXYZ(modelNP1, "boxZ", "box", "z",
				LPoint3f(0, -(s + 3.5), h - 1), LVecBase3f(0.0, 0.0, 0.0),
				LVecBase3f(2 * s, 6, 2), false);
		PT(BT3RigidBody)rigidBody = physicsMgr->get_rigid_body(
				physicsMgr->get_num_rigid_bodies() - 1);
		//hack: change mass
		rigidBody->set_mass(20);
		rigidBody->switch_body_type(BT3RigidBody::DYNAMIC);
		graphicNPs.push_back(rigidBody->get_owner_object());
		//
		clothAttachBody->append_anchor(0, rigidBody);
		clothAttachBody->append_anchor(r -1, rigidBody);
	}
	else
	{
		destroyObjects();
	}
}

void Init_Sticks(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Sticks
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		PT(TextureStage)sharedTS0 = new TextureStage("sharedTS0");
		PT(Texture)ropeTex = TexturePool::load_texture(Filename("red.tga"));
		const int n=16;
		const int sg=4;
		const float sz=5;
		const float hg=4;
		const float in0=1/(float)(n-1);
		for(int y=0;y<n;++y)
		{
			for(int x=0;x<n;++x)
			{
				const LPoint3f org(-(-sz+sz*2*x*in0), -sz+sz*2*y*in0, 0);
				//1: the graphic object (NodePath)
				PT(GeomNode)rope = new GeomNode("Rope" + str(y) + str(x));
				graphicNPs.push_back(NodePath(rope));
				graphicNPs.back().set_pos_hpr_scale(
						LPoint3f(0.0, 0.0, 0.0),//position
						LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
						LVecBase3f(1.0, 1.0, 1.0));//scale
				//Rope texturing
				graphicNPs.back().set_tex_scale(sharedTS0, 1.0, 1.0);
				graphicNPs.back().set_texture(sharedTS0, ropeTex, 1);
				//2: create the empty soft body
				softBodyNP = physicsMgr->create_soft_body("RopeBody" + str(y) + str(x));
				PT(BT3SoftBody) ropeBody = DCAST(BT3SoftBody, softBodyNP.node());
				//3: set CONSTRUCTION PARAMETERS
				ropeBody->set_group_mask(groupMask);
				ropeBody->set_collide_mask(collideMask);
				ropeBody->set_body_type(BT3SoftBody::ROPE);
				ropeBody->set_rope_points(
						{	org, LPoint3f(org + LVector3f(-(hg*0.001),0,hg))});
				ropeBody->set_rope_res(sg);
				ropeBody->set_rope_fixeds(1);
				//4: setup the body with graphicNPs.back() as owner
				ropeBody->set_owner_object(graphicNPs.back());
				ropeBody->setup();
				//5: set DYNAMIC PARAMETERS
				ropeBody->get_config().set_damping_coefficient(0.005);
				ropeBody->get_config().set_rigid_contacts_hardness(0.1);
				for(int i=0;i<3;++i)
				{
					ropeBody->generate_bending_constraints(2+i);
				}
				ropeBody->set_mass(1,0);
				ropeBody->set_total_mass(0.01);
				//6: visual state
//				softBodyNP.set_two_sided(true);
//				softBodyNP.show_bounds();
//				softBodyNP.set_render_mode_wireframe();
			}
		}
		// rigid bodies: Ctor_BigBall
		NodePath modelNP1 = getModelAnims("modelNP1", 1.0, 3, &playerAnimCtls);
		createRigidBodyXYZ(modelNP1, "sphereZ", "sphere", "z",
				LPoint3f(0, 0, 13), LVecBase3f(0.0, 0.0, 0.0),
				LVecBase3f(3, 3, 3), false);
		PT(BT3RigidBody)rigidBody = physicsMgr->get_rigid_body(
				physicsMgr->get_num_rigid_bodies() - 1);
		//hack: change mass
		rigidBody->set_mass(10);
		rigidBody->switch_body_type(BT3RigidBody::DYNAMIC);
		graphicNPs.push_back(rigidBody->get_owner_object());
	}
	else
	{
		destroyObjects();
	}

}

void Init_CapsuleCollision(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_CapsuleCollision
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		PT(TextureStage)sharedTS0 = new TextureStage("sharedTS0");
		PT(Texture)patchTex = TexturePool::load_texture(Filename("heightfield.png"));
		const float s = 4;
		const float h = 6;
		const int r = 20;
		int fixed = 0;				//4|8;
		//1: the graphic object (NodePath)
		PT(GeomNode)cloth = new GeomNode("Cloth");
		graphicNPs.push_back(NodePath(cloth));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 10.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		//Rope texturing
		graphicNPs.back().set_tex_scale(sharedTS0, 2.0, 2.0);
		graphicNPs.back().set_texture(sharedTS0, patchTex, 1);
		graphicNPs.back().set_texture(TexturePool::load_texture(Filename("terrain.png")));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body("ClothBody");
		PT(BT3SoftBody) clothBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		clothBody->set_group_mask(groupMask);
		clothBody->set_collide_mask(collideMask);
		clothBody->set_body_type(BT3SoftBody::PATCH);
		clothBody->set_patch_points(
				{	LPoint3f(s, -s, h),
					LPoint3f(-s, -s, h),
					LPoint3f(s, s, h),
					LPoint3f(-s, s, h),});
		clothBody->set_patch_resxy(
				{	r, r});
		clothBody->set_patch_fixeds(fixed);
		clothBody->set_patch_gendiags(true);
		//4: setup the body with graphicNPs[0] as owner
		clothBody->set_owner_object(graphicNPs[0]);
		clothBody->setup();
		//5: set DYNAMIC PARAMETERS
		clothBody->set_total_mass(0.1);
		clothBody->get_config().set_positions_solver_iterations(10);
		clothBody->get_config().set_cluster_solver_iterations(10);
		clothBody->get_config().set_drift_solver_iterations(10);
//		clothBody->get_config().set_velocities_solver_iterations(10);
		//6: visual state
		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();

		// rigid bodies
		NodePath modelNP1 = getModelAnims("modelNP1", 1.0, 4, &playerAnimCtls);
		createRigidBodyXYZ(modelNP1, "capsuleX", "capsule", "x",
				LPoint3f(0, 0, h - 2 + 10), LVecBase3f(0.0, 0.0, 0.0),
				LVecBase3f(5 + 2, 2, 2), false);//total height == height+2*radius
		PT(BT3RigidBody)rigidBody = physicsMgr->get_rigid_body(
				physicsMgr->get_num_rigid_bodies() - 1);
		//hack: change mass
		rigidBody->set_mass(0);
		rigidBody->switch_body_type(BT3RigidBody::STATIC);
		graphicNPs.push_back(rigidBody->get_owner_object());
		rigidBody->get_collision_object_attrs()->set_shape_margin(0.5);
		rigidBody->get_collision_object_attrs()->set_friction(0.8);
//		clothBody->append_anchor(0,rigidBody);
//		clothBody->append_anchor(r-1 ,rigidBody);
	}
	else
	{
		destroyObjects();
	}
}

#include "bullet3/examples/SoftDemo/TorusMesh.h"
void Init_Collide(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Collide
		static GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		struct Functor
		{
			static PT(BT3SoftBody) Create(const LPoint3f& x, const LVecBase3f& a,
					const LVecBase3f& s = LVecBase3f(1, 1, 1))
			{
				//1: the graphic object (NodePath)
				string nameSuffix = str((rand()/(float)RAND_MAX));
				NodePath triMesh = procedurallyGenerating3DModels(gVertices,
						&gIndices[0][0], NUM_TRIANGLES, true, "TriMesh" + nameSuffix);
				graphicNPs.push_back(triMesh);
//				graphicNPs.back().set_pos_hpr_scale(
//						x,//position
//						a,//rotation (h,p,r)
//						s);//scale
				//model coloring
				graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
				//2: create the empty soft body
				softBodyNP = physicsMgr->create_soft_body("TriMeshBody" + nameSuffix);
				PT(BT3SoftBody) triMeshBody = DCAST(BT3SoftBody, softBodyNP.node());
				//3: set CONSTRUCTION PARAMETERS
				triMeshBody->set_group_mask(groupMask);
				triMeshBody->set_collide_mask(collideMask);
				triMeshBody->set_body_type(BT3SoftBody::TRIANGLE_MESH);
				//4: setup the body with graphicNPs.back() as owner
				triMeshBody->set_owner_object(graphicNPs.back());
				triMeshBody->setup();
				//5: set DYNAMIC PARAMETERS
				triMeshBody->generate_bending_constraints(2);
				triMeshBody->get_config().set_positions_solver_iterations(2);
				int flags = triMeshBody->get_config().get_collisions_flags();
				triMeshBody->get_config().set_collisions_flags(flags |
						BT3SoftBodyConfig::Vertex_Face_Soft_Soft);
				triMeshBody->randomize_constraints();
				CPT(TransformState) m = TransformState::make_pos_hpr(x, a);
				triMeshBody->transform(m);
				triMeshBody->scale(s);
				triMeshBody->set_total_mass(50, true);
				//6: visual state
//				softBodyNP.set_two_sided(true);
//				softBodyNP.show_bounds();
//				softBodyNP.set_render_mode_wireframe();
				return triMeshBody;
			}
		};
		//
		for(int i = 0; i < 3; ++i)
		{
			Functor::Create(LPoint3f(-2.9 + 2.9 * i, 0, 8),
					LVecBase3f(0, 90 - i * 90, 0), 2);
		}
	}
	else
	{
		destroyObjects();
	}
}

#include "bullet3/examples/SoftDemo/BunnyMesh.h"
void Init_Collide2(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Collide2
		static GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		struct Functor
		{
			static PT(BT3SoftBody) Create(const LPoint3f& x, const LVecBase3f& a,
					const LVecBase3f& s = LVecBase3f(1, 1, 1))
			{
				//1: the graphic object (NodePath)
				string nameSuffix = str((rand()/(float)RAND_MAX));
				NodePath triMesh = procedurallyGenerating3DModels(gVerticesBunny,
						&gIndicesBunny[0][0], BUNNY_NUM_TRIANGLES, true, "TriMesh" + nameSuffix);
				graphicNPs.push_back(triMesh);
//				graphicNPs.back().set_pos_hpr_scale(
//						x,//position
//						a,//rotation (h,p,r)
//						s);//scale
				//model coloring
				graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
				//2: create the empty soft body
				softBodyNP = physicsMgr->create_soft_body(string("TriMeshBody" + nameSuffix));
				PT(BT3SoftBody) triMeshBody = DCAST(BT3SoftBody, softBodyNP.node());
				//3: set CONSTRUCTION PARAMETERS
				triMeshBody->set_group_mask(groupMask);
				triMeshBody->set_collide_mask(collideMask);
				triMeshBody->set_body_type(BT3SoftBody::TRIANGLE_MESH);
				//4: setup the body with graphicNPs.back() as owner
				triMeshBody->set_owner_object(graphicNPs.back());
				triMeshBody->setup();
				//5: set DYNAMIC PARAMETERS
				BT3SoftBodyMaterial pm = triMeshBody->append_material();
				pm.set_linear_stiffness_coefficient(0.5);
				pm.set_flags(pm.get_flags() - BT3SoftBodyMaterial::DebugDraw);
				triMeshBody->generate_bending_constraints(2, pm);
				triMeshBody->get_config().set_positions_solver_iterations(2);
				triMeshBody->get_config().set_dynamic_friction_coefficient(0.5);
				int flags = triMeshBody->get_config().get_collisions_flags();
				triMeshBody->get_config().set_collisions_flags(flags |
						BT3SoftBodyConfig::Vertex_Face_Soft_Soft);
				triMeshBody->randomize_constraints();
				CPT(TransformState) m = TransformState::make_pos_hpr(x, a);
				triMeshBody->transform(m);
				triMeshBody->scale(s);
				triMeshBody->set_total_mass(100, true);
				//6: visual state
//				softBodyNP.set_two_sided(true);
//				softBodyNP.show_bounds();
//				softBodyNP.set_render_mode_wireframe();
				return triMeshBody;
			}
		};
		//
		for(int i = 0; i < 3; ++i)
		{
			Functor::Create(LPoint3f(0, 0, -1 + 5 * i + 2.0),
					LVecBase3f(45 - i * 45, 0, 0), 6);
		}
	}
	else
	{
		destroyObjects();
	}
}

void Init_Collide3(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Collide3
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		{
			//1: the graphic object (NodePath)
			PT(GeomNode)clothAttach = new GeomNode("ClothCollide31");
			graphicNPs.push_back(NodePath(clothAttach));
			graphicNPs.back().set_pos_hpr_scale(
					LPoint3f(0.0, 0.0, 15.0),				//position
					LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
					LVecBase3f(1.0, 1.0, 1.0));//scale
			graphicNPs.back().set_texture(TexturePool::load_texture(Filename("heightfield.png")));
			//2: create the empty soft body
			softBodyNP = physicsMgr->create_soft_body("clothCollideBody31");
			PT(BT3SoftBody) clothCollideBody = DCAST(BT3SoftBody, softBodyNP.node());
			//3: set CONSTRUCTION PARAMETERS
			clothCollideBody->set_group_mask(groupMask);
			clothCollideBody->set_collide_mask(collideMask);
			clothCollideBody->set_body_type(BT3SoftBody::PATCH);
			const float s=8;
			clothCollideBody->set_patch_points(
					{
						LPoint3f(s, -s, 0),
						LPoint3f(-s, -s, 0),
						LPoint3f(s, s, 0),
						LPoint3f(-s, s, 0),
					});
			clothCollideBody->set_patch_resxy(
					{	15, 15});
			clothCollideBody->set_patch_fixeds(0x1 | 0x2 | 0x4 | 0x8);
			clothCollideBody->set_patch_gendiags(true);
			//4: setup the body with graphicNPs.back() as owner
			clothCollideBody->set_owner_object(graphicNPs.back());
			clothCollideBody->setup();
			//5: set DYNAMIC PARAMETERS
			clothCollideBody->get_material(0).set_linear_stiffness_coefficient(0.4);
			int flags = clothCollideBody->get_config().get_collisions_flags();
			clothCollideBody->get_config().set_collisions_flags(flags |
					BT3SoftBodyConfig::Vertex_Face_Soft_Soft);
			clothCollideBody->set_total_mass(150);
			//6: visual state
			softBodyNP.set_two_sided(true);
//			softBodyNP.show_bounds();
//			softBodyNP.set_render_mode_wireframe();
		}
		{
			//1: the graphic object (NodePath)
			PT(GeomNode)clothAttach = new GeomNode("ClothCollide32");
			graphicNPs.push_back(NodePath(clothAttach));
			graphicNPs.back().set_pos_hpr_scale(
					LPoint3f(0.0, 0.0, 15.0),//position
					LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
					LVecBase3f(1.0, 1.0, 1.0));//scale
			graphicNPs.back().set_texture(TexturePool::load_texture(Filename("terrain.png")));
			//2: create the empty soft body
			softBodyNP = physicsMgr->create_soft_body("clothCollideBody32");
			PT(BT3SoftBody) clothCollideBody = DCAST(BT3SoftBody, softBodyNP.node());
			//3: set CONSTRUCTION PARAMETERS
			clothCollideBody->set_group_mask(groupMask);
			clothCollideBody->set_collide_mask(collideMask);
			clothCollideBody->set_body_type(BT3SoftBody::PATCH);
			const float	s=4;
			const LVector3f	o(-5,0,10);
			clothCollideBody->set_patch_points(
					{
						LPoint3f(s, -s, 0)+o,
						LPoint3f(-s, -s, 0)+o,
						LPoint3f(s, s, 0)+o,
						LPoint3f(-s, s, 0)+o,
					});
			clothCollideBody->set_patch_resxy(
					{	7, 7});
			clothCollideBody->set_patch_fixeds(0x0);
			clothCollideBody->set_patch_gendiags(true);
			//4: setup the body with graphicNPs.back() as owner
			clothCollideBody->set_owner_object(graphicNPs.back());
			clothCollideBody->setup();
			//5: set DYNAMIC PARAMETERS
			BT3SoftBodyMaterial pm = clothCollideBody->append_material();
			pm.set_linear_stiffness_coefficient(0.1);
			pm.set_flags(pm.get_flags() - BT3SoftBodyMaterial::DebugDraw);
			clothCollideBody->generate_bending_constraints(2, pm);
			clothCollideBody->get_material(0).set_linear_stiffness_coefficient(0.5);
			int flags = clothCollideBody->get_config().get_collisions_flags();
			clothCollideBody->get_config().set_collisions_flags(flags |
					BT3SoftBodyConfig::Vertex_Face_Soft_Soft);
			clothCollideBody->set_total_mass(150);
			//6: visual state
			softBodyNP.set_two_sided(true);
//			softBodyNP.show_bounds();
//			softBodyNP.set_render_mode_wireframe();
		}
	}
	else
	{
		destroyObjects();
	}
}

void Init_Impact(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Impact
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		PT(TextureStage)sharedTS0 = new TextureStage("sharedTS0");
		PT(Texture)ropeTex = TexturePool::load_texture(Filename("red.tga"));
		//1: the graphic object (NodePath)
		PT(GeomNode)rope = new GeomNode(string("RopeImpact"));
		graphicNPs.push_back(NodePath(rope));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 10.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		//Rope texturing
		graphicNPs.back().set_tex_scale(sharedTS0, 1.0, 1.0);
		graphicNPs.back().set_texture(sharedTS0, ropeTex, 1);
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body(string("RopeBodyImpact"));
		PT(BT3SoftBody) ropeBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		ropeBody->set_group_mask(groupMask);
		ropeBody->set_collide_mask(collideMask);
		ropeBody->set_body_type(BT3SoftBody::ROPE);
		ropeBody->set_rope_points(
				{	LPoint3f(0, 0, 0), LPoint3f(0, 0, -1)});
		ropeBody->set_rope_res(0);
		ropeBody->set_rope_fixeds(1);
		//4: setup the body with graphicNPs.back() as owner
		ropeBody->set_owner_object(graphicNPs.back());
		ropeBody->setup();
		//5: set DYNAMIC PARAMETERS
		ropeBody->get_config().set_rigid_contacts_hardness(0.5);
		//6: visual state
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();

		// rigid bodies
		NodePath modelNP1 = getModelAnims("modelNP1", 1.0, 8, &playerAnimCtls);
		createRigidBodyXYZ(modelNP1, "boxZ", "box", "z",
				LPoint3f(0, 0, 10 + 20), LVecBase3f(0.0, 0.0, 0.0),
				LVecBase3f(4, 4, 4), false);
		PT(BT3RigidBody)rigidBody = physicsMgr->get_rigid_body(
				physicsMgr->get_num_rigid_bodies() - 1);
		//hack: change mass
		rigidBody->set_mass(10);
		rigidBody->switch_body_type(BT3RigidBody::DYNAMIC);
		graphicNPs.push_back(rigidBody->get_owner_object());
	}
	else
	{
		destroyObjects();
	}
}

void Init_Aero(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Aero
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		const float s=2;
		const float h=10;
		const int segments=6;
		const int count=50;
		for(int i=0;i<count;++i)
		{
			//1: the graphic object (NodePath)
			PT(GeomNode)clothAero = new GeomNode("ClothAero" + str(i));
			graphicNPs.push_back(NodePath(clothAero));
			graphicNPs.back().set_pos_hpr_scale(
					LPoint3f(0.0, 0.0, 0.0),//position
					LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
					LVecBase3f(1.0, 1.0, 1.0));//scale
			graphicNPs.back().set_texture(TexturePool::load_texture(Filename("heightfield.png")));
			//2: create the empty soft body
			softBodyNP = physicsMgr->create_soft_body("ClothAeroBody" + str(i));
			PT(BT3SoftBody) clothAeroBody = DCAST(BT3SoftBody, softBodyNP.node());
			//3: set CONSTRUCTION PARAMETERS
			clothAeroBody->set_group_mask(groupMask);
			clothAeroBody->set_collide_mask(collideMask);
			clothAeroBody->set_body_type(BT3SoftBody::PATCH);
			clothAeroBody->set_patch_points(
					{
						LPoint3f(s, -s, h),
						LPoint3f(-s, -s, h),
						LPoint3f(s, s, h),
						LPoint3f(-s, s, h),});
			clothAeroBody->set_patch_resxy(
					{	segments, segments});
			clothAeroBody->set_patch_fixeds(0x0);
			clothAeroBody->set_patch_gendiags(true);
			//4: setup the body with graphicNPs.back() as owner
			clothAeroBody->set_owner_object(graphicNPs.back());
			clothAeroBody->setup();
			//5: set DYNAMIC PARAMETERS
			BT3SoftBodyMaterial pm = clothAeroBody->append_material();
			pm.set_flags(pm.get_flags() - BT3SoftBodyMaterial::DebugDraw);
			clothAeroBody->generate_bending_constraints(2,pm);
			clothAeroBody->get_config().set_lift_coefficient(0.004);
			clothAeroBody->get_config().set_drag_coefficient(0.0003);
			clothAeroBody->get_config().set_aerodynamic_model(BT3SoftBodyConfig::Vertex_TwoSided);
			LQuaternionf rot;
			LVecBase3f ra=Vector3Rand()*0.1;
			LVecBase3f rp=Vector3Rand()*15+LVecBase3f(0,80,20);
			rot.set_hpr(LVecBase3f((-M_PI/8+ra.get_x()) * (180.0 / M_PI),
							ra.get_z() * (180.0 / M_PI),
							(-M_PI/7+ra.get_y()) * (180.0 / M_PI)));
			CPT(TransformState) trs = TransformState::make_pos_quat_scale(rp, rot, 1);
			clothAeroBody->transform(trs);
			clothAeroBody->set_total_mass(0.1);
			clothAeroBody->add_force(LVector3f(0,0,2));
			//6: visual state
			softBodyNP.set_two_sided(true);
//			softBodyNP.show_bounds();
//			softBodyNP.set_render_mode_wireframe();
		}
	}
	else
	{
		destroyObjects();
	}
}

void Init_Aero2(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Aero2
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		const float s = 5;
		const int segments = 10;
		const int count = 5;
		LVector3f pos = LVector3f(s * segments, 0, 0) + LVector3f(0, 0, 20);
		float gap = 0.5;
		for(int i=0;i<count;++i)
		{
			//1: the graphic object (NodePath)
			PT(GeomNode)clothAero = new GeomNode("ClothAero2" + str(i));
			graphicNPs.push_back(NodePath(clothAero));
			graphicNPs.back().set_pos_hpr_scale(
					LPoint3f(0.0, 0.0, 0.0),//position
					LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
					LVecBase3f(1.0, 1.0, 1.0));//scale
			graphicNPs.back().set_texture(TexturePool::load_texture(Filename("flag_oga.png")));
			//2: create the empty soft body
			softBodyNP = physicsMgr->create_soft_body("ClothAero2Body" + str(i));
			PT(BT3SoftBody) clothAeroBody = DCAST(BT3SoftBody, softBodyNP.node());
			//3: set CONSTRUCTION PARAMETERS
			clothAeroBody->set_group_mask(groupMask);
			clothAeroBody->set_collide_mask(collideMask);
			clothAeroBody->set_body_type(BT3SoftBody::PATCH);
			clothAeroBody->set_patch_points({
						LPoint3f(s, -s * 3, 0),
						LPoint3f(-s, -s * 3, 0),
						LPoint3f(s, s, 0),
						LPoint3f(-s, s, 0),});
			clothAeroBody->set_patch_resxy(
					{	segments, segments * 3});
			clothAeroBody->set_patch_fixeds(0x1 | 0x2);
			clothAeroBody->set_patch_gendiags(true);
			//4: setup the body with graphicNPs.back() as owner
			clothAeroBody->set_owner_object(graphicNPs.back());
			clothAeroBody->setup();
			//5: set DYNAMIC PARAMETERS
			clothAeroBody->get_collision_object_attrs()->set_shape_margin(0.5);
			BT3SoftBodyMaterial pm = clothAeroBody->append_material();
			pm.set_linear_stiffness_coefficient(0.0004);
			pm.set_flags(pm.get_flags() - BT3SoftBodyMaterial::DebugDraw);
			clothAeroBody->generate_bending_constraints(2,pm);
			clothAeroBody->get_config().set_lift_coefficient(0.05);
			clothAeroBody->get_config().set_drag_coefficient(0.01);
			clothAeroBody->get_config().set_positions_solver_iterations(2);
			clothAeroBody->get_config().set_aerodynamic_model(BT3SoftBodyConfig::Vertex_TwoSidedLiftDrag);
			clothAeroBody->set_wind_velocity(LVector3f(-4, -25.0, -12.0));
			LQuaternionf rot;
			pos += LVector3f(-(s * 2 + gap), 0, 0);
			rot.set_from_axis_angle_rad(M_PI/2, LVector3f(-1, 0, 0));
			CPT(TransformState) trs = TransformState::make_pos_quat_scale(pos, rot, 1);
			clothAeroBody->transform(trs);
			clothAeroBody->set_total_mass(2.0);
			clothAeroBody->reoptimize_link_order();
			//6: visual state
			softBodyNP.set_two_sided(true);
//			softBodyNP.show_bounds();
//			softBodyNP.set_render_mode_wireframe();
		}
	}
	else
	{
		destroyObjects();
	}
}

void Init_Friction(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Friction
		constexpr float bs = 2;
		constexpr float ts = bs + bs / 4;
		for (int i = 0, ni = 20; i < ni; ++i)
		{
			const LPoint3f p(-(-ni * ts / 2 + i * ts), 40 + 50, -10 + bs);
			PT(BT3SoftBody)psb = Ctor_SoftBox( p, LVector3f(-bs, bs, bs));
			psb->get_config().set_dynamic_friction_coefficient(
					0.1 * ((i + 1) / (float) ni));
			psb->add_velocity(LVecBase3f(0, -10, 0));
		}
	}
	else
	{
		destroyObjects();
	}
}

void Init_Torus(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Torus
		static GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		NodePath triMesh = procedurallyGenerating3DModels(gVertices,
				&gIndices[0][0], NUM_TRIANGLES, true, "Torus");
		graphicNPs.push_back(triMesh);
//		graphicNPs.back().set_pos_hpr_scale(
//				LPoint3f(0.0, 0.0, 0.0),//position
//				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
//				LVecBase3f(1.0, 1.0, 1.0));//scale
		//model coloring
		graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body(string("TorusBody"));
		PT(BT3SoftBody) triMeshBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		triMeshBody->set_group_mask(groupMask);
		triMeshBody->set_collide_mask(collideMask);
		triMeshBody->set_body_type(BT3SoftBody::TRIANGLE_MESH);
		//4: setup the body with graphicNPs.back() as owner
		triMeshBody->set_owner_object(graphicNPs.back());
		triMeshBody->setup();
		//5: set DYNAMIC PARAMETERS
		triMeshBody->generate_bending_constraints(2);
		triMeshBody->get_config().set_positions_solver_iterations(2);
		triMeshBody->randomize_constraints();
		LVecBase3f hpr(0, (-M_PI/2.0) * (180.0 / M_PI), 0);
		CPT(TransformState) m = TransformState::make_pos_hpr(LPoint3f(0,0,4 + 10), hpr);
		triMeshBody->transform(m);
		triMeshBody->scale(2);
		triMeshBody->set_total_mass(50, true);
		//6: visual state
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();
	}
	else
	{
		destroyObjects();
	}
}

void Init_TorusMatch(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_TorusMatch
		static GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		NodePath triMesh = procedurallyGenerating3DModels(gVertices,
				&gIndices[0][0], NUM_TRIANGLES, true, "TorusMatch");
		graphicNPs.push_back(triMesh);
//		graphicNPs.back().set_pos_hpr_scale(
//				LPoint3f(0.0, 0.0, 0.0),//position
//				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
//				LVecBase3f(1.0, 1.0, 1.0));//scale
		//model coloring
		graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body(string("TorusMatchBody"));
		PT(BT3SoftBody) triMeshBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		triMeshBody->set_group_mask(groupMask);
		triMeshBody->set_collide_mask(collideMask);
		triMeshBody->set_body_type(BT3SoftBody::TRIANGLE_MESH);
		//4: setup the body with graphicNPs.back() as owner
		triMeshBody->set_owner_object(graphicNPs.back());
		triMeshBody->setup();
		//5: set DYNAMIC PARAMETERS
		triMeshBody->get_material(0).set_linear_stiffness_coefficient(0.1);
		triMeshBody->get_config().set_pose_matching_coefficient(0.05);
		triMeshBody->randomize_constraints();
		LVecBase3f hpr(0, -M_PI/2.0 * (180.0 / M_PI), 0);
		CPT(TransformState) m = TransformState::make_pos_hpr(LPoint3f(0,0,4 + 10), hpr);
		triMeshBody->transform(m);
		triMeshBody->scale(2);
		triMeshBody->set_total_mass(50, true);
		triMeshBody->set_pose(false, true);
		//6: visual state
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();
	}
	else
	{
		destroyObjects();
	}
}

void Init_Bunny(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Bunny
		static GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		NodePath triMesh = procedurallyGenerating3DModels(gVerticesBunny,
				&gIndicesBunny[0][0], BUNNY_NUM_TRIANGLES, true, "Bunny");
		graphicNPs.push_back(triMesh);
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 3.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		//model coloring
		graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body(string("BunnyBody"));
		PT(BT3SoftBody) triMeshBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		triMeshBody->set_group_mask(groupMask);
		triMeshBody->set_collide_mask(collideMask);
		triMeshBody->set_body_type(BT3SoftBody::TRIANGLE_MESH);
		//4: setup the body with graphicNPs.back() as owner
		triMeshBody->set_owner_object(graphicNPs.back());
		triMeshBody->setup();
		//5: set DYNAMIC PARAMETERS
		BT3SoftBodyMaterial pm = triMeshBody->append_material();
		pm.set_linear_stiffness_coefficient(0.5);
		pm.set_flags(pm.get_flags() - BT3SoftBodyMaterial::DebugDraw);
		triMeshBody->generate_bending_constraints(2,pm);
		triMeshBody->get_config().set_positions_solver_iterations(2);
		triMeshBody->get_config().set_dynamic_friction_coefficient(0.5);
		triMeshBody->randomize_constraints();
		triMeshBody->scale(6);
		triMeshBody->set_total_mass(100, true);
		//6: visual state
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();
	}
	else
	{
		destroyObjects();
	}
}

void Init_BunnyMatch(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_BunnyMatch
		static GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		NodePath triMesh = procedurallyGenerating3DModels(gVerticesBunny,
				&gIndicesBunny[0][0], BUNNY_NUM_TRIANGLES, true, "BunnyMatch");
		graphicNPs.push_back(triMesh);
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 3.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		//model coloring
		graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body(string("BunnyMatchBody"));
		PT(BT3SoftBody) triMeshBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		triMeshBody->set_group_mask(groupMask);
		triMeshBody->set_collide_mask(collideMask);
		triMeshBody->set_body_type(BT3SoftBody::TRIANGLE_MESH);
		//4: setup the body with graphicNPs.back() as owner
		triMeshBody->set_owner_object(graphicNPs.back());
		triMeshBody->setup();
		//5: set DYNAMIC PARAMETERS
		triMeshBody->get_config().set_dynamic_friction_coefficient(0.5);
		triMeshBody->get_config().set_pose_matching_coefficient(0.05);
		triMeshBody->get_config().set_positions_solver_iterations(5);
		triMeshBody->randomize_constraints();
		triMeshBody->scale(6);
		triMeshBody->set_total_mass(100, true);
		triMeshBody->set_pose(false,true);
		//6: visual state
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();
	}
	else
	{
		destroyObjects();
	}
}

void Init_Cutting1(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_Cutting1
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		PT(GeomNode)cloth = new GeomNode("Cutting1");
		graphicNPs.push_back(NodePath(cloth));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 20.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		graphicNPs.back().set_texture(TexturePool::load_texture(Filename("terrain.png")));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body("Cutting1Body");
		PT(BT3SoftBody) clothBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		clothBody->set_group_mask(groupMask);
		clothBody->set_collide_mask(collideMask);
		clothBody->set_body_type(BT3SoftBody::PATCH);
		const float s=6;
		const float h=2;
		const int r=16;
		const LPoint3f	p[]={
				LPoint3f(-s,-s,h),
				LPoint3f(+s,-s,h),
				LPoint3f(-s,+s,h),
				LPoint3f(+s,+s,h)};
		clothBody->set_patch_points({p[0],p[1],p[2],p[3]});
		clothBody->set_patch_resxy({r,r});
		clothBody->set_patch_fixeds(0x1 | 0x2 | 0x4 | 0x8);
		clothBody->set_patch_gendiags(true);
		//4: setup the body with graphicNPs.back() as owner
		clothBody->set_owner_object(graphicNPs.back());
		clothBody->setup();
		//5: set DYNAMIC PARAMETERS
		clothBody->get_config().set_positions_solver_iterations(1);
		//6: visual state
		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();
	}
	else
	{
		destroyObjects();
	}
}

static PT(BT3SoftBody)Ctor_ClusterTorus(const LPoint3f& x,
		const LVecBase3f& a,const LVecBase3f& s=LVecBase3f(2,2,2))
{
	static GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
	//1: the graphic object (NodePath)
	NodePath triMesh = procedurallyGenerating3DModels(gVertices,
			&gIndices[0][0], NUM_TRIANGLES, true, "Torus");
	graphicNPs.push_back(triMesh);
//		graphicNPs.back().set_pos_hpr_scale(
//				LPoint3f(0.0, 0.0, 0.0),//position
//				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
//				LVecBase3f(1.0, 1.0, 1.0));//scale
	//model coloring
	graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
	//2: create the empty soft body
	softBodyNP = physicsMgr->create_soft_body(string("TorusBody"));
	PT(BT3SoftBody) triMeshBody = DCAST(BT3SoftBody, softBodyNP.node());
	//3: set CONSTRUCTION PARAMETERS
	triMeshBody->set_group_mask(groupMask);
	triMeshBody->set_collide_mask(collideMask);
	triMeshBody->set_body_type(BT3SoftBody::TRIANGLE_MESH);
	//4: setup the body with graphicNPs.back() as owner
	triMeshBody->set_owner_object(graphicNPs.back());
	triMeshBody->setup();
	//5: set DYNAMIC PARAMETERS
	BT3SoftBodyMaterial pm = triMeshBody->append_material();
	pm.set_linear_stiffness_coefficient(1);
	pm.set_flags(pm.get_flags() - BT3SoftBodyMaterial::DebugDraw);
	triMeshBody->generate_bending_constraints(2,pm);
	triMeshBody->get_config().set_positions_solver_iterations(2);
	triMeshBody->get_config().set_collisions_flags(BT3SoftBodyConfig::Cluster_Soft_Soft |
			BT3SoftBodyConfig::Cluster_Rigid_Soft);
	triMeshBody->randomize_constraints();
	triMeshBody->scale(s);
	LQuaternionf q;
	q.set_hpr(LVecBase3f(-a[0],a[2],a[1]));
	triMeshBody->rotate(q);
	triMeshBody->translate(x);
	triMeshBody->set_total_mass(50, true);
	triMeshBody->generate_clusters(64);
	//6: visual state
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();
	//
	return triMeshBody;
}

void Init_ClusterDeform(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClusterDeform
		PT(BT3SoftBody)	torus=Ctor_ClusterTorus(LPoint3f(0,0,10),
				LVecBase3f(-M_PI/2.0 * (180.0 / M_PI),M_PI/2.0 * (180.0 / M_PI),0));
		torus->generate_clusters(8);
		torus->get_config().set_dynamic_friction_coefficient(1);
	}
	else
	{
		destroyObjects();
	}
}

void Init_ClusterCollide1(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClusterCollide1
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		PT(GeomNode)cloth = new GeomNode("ClusterCollide1");
		graphicNPs.push_back(NodePath(cloth));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 20.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		graphicNPs.back().set_texture(TexturePool::load_texture(Filename("terrain.png")));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body("ClusterCollide1Body");
		PT(BT3SoftBody) clothBody = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		clothBody->set_group_mask(groupMask);
		clothBody->set_collide_mask(collideMask);
		clothBody->set_body_type(BT3SoftBody::PATCH);
		const float s = 8;
		clothBody->set_patch_points(
				{	LPoint3f(s, -s, 0),
					LPoint3f(-s, -s, 0),
					LPoint3f(s, s, 0),
					LPoint3f(-s, s, 0),});
		clothBody->set_patch_resxy({17, 17});
		clothBody->set_patch_fixeds(0x1 | 0x2 | 0x4 | 0x8);
		clothBody->set_patch_gendiags(true);
		//4: setup the body with graphicNPs.back() as owner
		clothBody->set_owner_object(graphicNPs.back());
		clothBody->setup();
		//5: set DYNAMIC PARAMETERS
		BT3SoftBodyMaterial pm = clothBody->append_material();
		pm.set_linear_stiffness_coefficient(0.4);
		pm.set_flags(pm.get_flags() - BT3SoftBodyMaterial::DebugDraw);
		clothBody->get_config().set_dynamic_friction_coefficient(1);
		clothBody->get_config().set_soft_rigid_hardness(1);
		clothBody->get_config().set_soft_rigid_impulse_split(0);
		clothBody->get_config().set_collisions_flags(BT3SoftBodyConfig::Cluster_Soft_Soft |
				BT3SoftBodyConfig::Cluster_Rigid_Soft);
		clothBody->generate_bending_constraints(2,pm);
		clothBody->get_collision_object_attrs()->set_shape_margin(0.05);
		clothBody->set_total_mass(50);
		///pass zero in generateClusters to create  cluster for each tetrahedron or triangle
		clothBody->generate_clusters(0);
		//6: visual state
		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();

		// rigid bodies
		Ctor_RbUpStack(10, LPoint3f(0.0, 0.0, 20.0));
	}
	else
	{
		destroyObjects();
	}
}

void Init_ClusterCollide2(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClusterCollide2
		static GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		struct Functor
		{
			static PT(BT3SoftBody) Create(const LPoint3f& x,//pos
					const LVecBase3f& a)//PHR
			{
				//1: the graphic object (NodePath)
				string nameSuffix = str((rand()/(float)RAND_MAX));
				NodePath triMesh = procedurallyGenerating3DModels(gVertices,
						&gIndices[0][0], NUM_TRIANGLES, true, "TriMesh" + nameSuffix);
				graphicNPs.push_back(triMesh);
//				graphicNPs.back().set_pos_hpr_scale(
//						x,//position
//						a,//rotation (h,p,r)
//						s);//scale
				//model coloring
				graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
				//2: create the empty soft body
				softBodyNP = physicsMgr->create_soft_body("TriMeshBody" + nameSuffix);
				PT(BT3SoftBody) triMeshBody = DCAST(BT3SoftBody, softBodyNP.node());
				//3: set CONSTRUCTION PARAMETERS
				triMeshBody->set_group_mask(groupMask);
				triMeshBody->set_collide_mask(collideMask);
				triMeshBody->set_body_type(BT3SoftBody::TRIANGLE_MESH);
				//4: setup the body with graphicNPs.back() as owner
				triMeshBody->set_owner_object(graphicNPs.back());
				triMeshBody->setup();
				//5: set DYNAMIC PARAMETERS
				BT3SoftBodyMaterial pm = triMeshBody->append_material();
				pm.set_flags(pm.get_flags() - BT3SoftBodyMaterial::DebugDraw);
				triMeshBody->generate_bending_constraints(2,pm);
				triMeshBody->get_config().set_positions_solver_iterations(2);
				triMeshBody->get_config().set_dynamic_friction_coefficient(1);
				triMeshBody->get_config().set_soft_soft_hardness(1);
				triMeshBody->get_config().set_soft_rigid_impulse_split(0);
				triMeshBody->get_config().set_soft_kinetic_hardness(0.1);
				triMeshBody->get_config().set_soft_rigid_impulse_split(1);
				triMeshBody->get_config().set_collisions_flags(BT3SoftBodyConfig::Cluster_Soft_Soft |
						BT3SoftBodyConfig::Cluster_Rigid_Soft);
				triMeshBody->randomize_constraints();
				//compose: P->H->R
				CPT(TransformState) tsP = TransformState::make_pos_hpr(
						LPoint3f::zero(), LVecBase3f(0, a.get_x(), 0));
				CPT(TransformState) tsH = TransformState::make_pos_hpr(
						LPoint3f::zero(), LVecBase3f(a.get_y(), 0, 0));
				CPT(TransformState) tsR = TransformState::make_pos_hpr(
						LPoint3f::zero(), LVecBase3f(0, 0, a.get_z()));
				CPT(TransformState) m = tsP->compose(tsH->compose(tsR));
				triMeshBody->transform(TransformState::make_pos_hpr(x, m->get_hpr()));
				triMeshBody->scale(2);
				triMeshBody->set_total_mass(50, true);
				triMeshBody->generate_clusters(16);
				//6: visual state
//				softBodyNP.set_two_sided(true);
//				softBodyNP.show_bounds();
//				softBodyNP.set_render_mode_wireframe();
				return triMeshBody;
			}
		};
		//
		for(int i=0;i<3;++i)
		{
			Functor::Create(LPoint3f(-3*i,0,2 + 10),
					LVecBase3f(-M_PI/2*(1-(i&1)) * (180.0 / M_PI),
							M_PI/2*(i&1) * (180.0 / M_PI),0));
		}
	}
	else
	{
		destroyObjects();
	}
}

void Init_ClusterSocket(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClusterSocket
		PT(BT3SoftBody)	psb=Ctor_ClusterTorus(LPoint3f(0,0,10),
				LVecBase3f(-M_PI/2.0 * (180.0 / M_PI),M_PI/2.0 * (180.0 / M_PI),0));
		PT(BT3RigidBody) prb=Ctor_BigPlate(50, 10 + 8);
		psb->get_config().set_dynamic_friction_coefficient(1);
		BT3SoftBodyLJointSpecs	lj;
		lj.set_position(LPoint3f(0,0,5 + 10));
		psb->append_linear_joint(lj, NodePath::any_path(prb));
	}
	else
	{
		destroyObjects();
	}
}

void Init_ClusterHinge(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClusterHinge
		PT(BT3SoftBody)	psb=Ctor_ClusterTorus(LPoint3f(0,0,10),
				LVecBase3f(-M_PI/2.0 * (180.0 / M_PI),M_PI/2.0 * (180.0 / M_PI),0));
		PT(BT3RigidBody) prb=Ctor_BigPlate(50, 10 + 8);
		psb->get_config().set_dynamic_friction_coefficient(1);
		BT3SoftBodyAJointSpecs	aj;
		aj.set_axis(LVector3f(0,1,0));
		psb->append_angular_joint(aj,NodePath::any_path(prb));
	}
	else
	{
		destroyObjects();
	}
}


static BT3SoftBodyAJointControl motorcontrol(BT3SoftBodyAJointControl::MOTOR);
static BT3SoftBodyAJointControl steercontrol_f(BT3SoftBodyAJointControl::STEER,
		+1);
static BT3SoftBodyAJointControl steercontrol_r(BT3SoftBodyAJointControl::STEER,
		-1);

void Init_ClusterCombine(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClusterCombine
		const LVecBase3f sz(2,2,4);
		PT(BT3SoftBody)	psb0=Ctor_ClusterTorus(LPoint3f(0,0,10 + 8),
				LVecBase3f(-M_PI/2.0 * (180.0 / M_PI),M_PI/2.0 * (180.0 / M_PI),0), sz);
		PT(BT3SoftBody)	psb1=Ctor_ClusterTorus(LPoint3f(0,10,10 + 8),
				LVecBase3f(-M_PI/2.0 * (180.0 / M_PI),M_PI/2.0 * (180.0 / M_PI),0), sz);
		PT(BT3SoftBody)	psbs[]={psb0,psb1};
		for(int j=0;j<2;++j)
		{
			psbs[j]->get_config().set_dynamic_friction_coefficient(1);
			psbs[j]->get_config().set_damping_coefficient(0);
			psbs[j]->get_config().set_positions_solver_iterations(1);
			psbs[j]->set_cluster_matching(0, 0.05);
			psbs[j]->set_cluster_node_damping(0, 0.05);
		}
		BT3SoftBodyAJointSpecs	aj;
		aj.set_axis(LVector3f(0,1,0));
		aj.set_icontrol(&motorcontrol);
		psb0->append_angular_joint(aj,NodePath::any_path(psb1));

		BT3SoftBodyLJointSpecs	lj;
		lj.set_position(LVector3f(0,5,10 + 8));
		psb0->append_linear_joint(lj,NodePath::any_path(psb1));
	}
	else
	{
		destroyObjects();
	}
}

static PT(BT3SoftBody) Ctor_ClusterBunny(const LPoint3f& x,const LVecBase3f& a)
{
	GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
	//1: the graphic object (NodePath)
	string nameSuffix = str((rand()/(float)RAND_MAX));
	NodePath triMesh = procedurallyGenerating3DModels(gVerticesBunny,
			&gIndicesBunny[0][0], BUNNY_NUM_TRIANGLES, true, "ClusterBunny" + nameSuffix);
	graphicNPs.push_back(triMesh);
	//model coloring
	graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
	//2: create the empty soft body
	softBodyNP = physicsMgr->create_soft_body(string("ClusterBunny" + nameSuffix));
	PT(BT3SoftBody) psb = DCAST(BT3SoftBody, softBodyNP.node());
	//3: set CONSTRUCTION PARAMETERS
	psb->set_group_mask(groupMask);
	psb->set_collide_mask(collideMask);
	psb->set_body_type(BT3SoftBody::TRIANGLE_MESH);
	//4: setup the body with graphicNPs.back() as owner
	psb->set_owner_object(graphicNPs.back());
	psb->setup();
	//5: set DYNAMIC PARAMETERS
	BT3SoftBodyMaterial pm = psb->append_material();
	pm.set_linear_stiffness_coefficient(1);
	pm.set_flags(pm.get_flags() - BT3SoftBodyMaterial::DebugDraw);
	psb->generate_bending_constraints(2, pm);
	psb->get_config().set_positions_solver_iterations(2);
	psb->get_config().set_dynamic_friction_coefficient(1);
	psb->get_config().set_collisions_flags(BT3SoftBodyConfig::Cluster_Soft_Soft |
			BT3SoftBodyConfig::Cluster_Rigid_Soft);
	psb->randomize_constraints();
	CPT(TransformState) m = TransformState::make_pos_hpr(x, a);
	psb->transform(m);
	psb->scale(LVecBase3f(4,4,4));
	psb->set_total_mass(75, true);
	psb->generate_clusters(1);
	//6: visual state
//				softBodyNP.set_two_sided(true);
//				softBodyNP.show_bounds();
//				softBodyNP.set_render_mode_wireframe();
	return psb;
}

void Init_ClusterCar(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClusterCar
		const LPoint3f origin(-50, 0, 40 + 10);
		LQuaternionf orientation;
		orientation.set_hpr(LVecBase3f((-M_PI / 2) * (180.0 / M_PI), 0, 0));
		const float widthf = 4.0;
		const float widthr = 4.5;
		const float length = 4.0;
		const float height = 2.0;
		const LPoint3f wheels[] =
		{ LPoint3f(-widthf, +length, -height),	// Front left
		LPoint3f(+widthf, +length, -height),	// Front right
		LPoint3f(-widthr, -length, -height),	// Rear left
		LPoint3f(+widthr, -length, -height),	// Rear right
				};
		PT(BT3SoftBody) pa = Ctor_ClusterBunny(LPoint3f(0, 0, 0),
				LVecBase3f(0, 0, 0));
		PT(BT3SoftBody) pfl = Ctor_ClusterTorus(wheels[0],
				LVecBase3f(0, M_PI/2.0 * (180.0 / M_PI), 0), LVecBase3f(1, 1, 2));
		PT(BT3SoftBody) pfr = Ctor_ClusterTorus(wheels[1],
				LVecBase3f(0, M_PI/2.0 * (180.0 / M_PI), 0), LVecBase3f(1, 1, 2));
		PT(BT3SoftBody) prl = Ctor_ClusterTorus(wheels[2],
				LVecBase3f(0, M_PI/2.0 * (180.0 / M_PI), 0), LVecBase3f(1, 1, 2.5));
		PT(BT3SoftBody) prr = Ctor_ClusterTorus(wheels[3],
				LVecBase3f(0, M_PI/2.0 * (180.0 / M_PI), 0), LVecBase3f(1, 1, 2.5));

		pfl->get_config().set_dynamic_friction_coefficient(1);
		pfr->get_config().set_dynamic_friction_coefficient(1);
		prl->get_config().set_dynamic_friction_coefficient(1);
		prr->get_config().set_dynamic_friction_coefficient(1);

		BT3SoftBodyLJointSpecs lspecs;
		lspecs.set_cfm(1);
		lspecs.set_erp(1);
		lspecs.set_position(LPoint3f(0, 0, 0));

		lspecs.set_position(wheels[0]);
		pa->append_linear_joint(lspecs, NodePath::any_path(pfl));
		lspecs.set_position(wheels[1]);
		pa->append_linear_joint(lspecs, NodePath::any_path(pfr));
		lspecs.set_position(wheels[2]);
		pa->append_linear_joint(lspecs, NodePath::any_path(prl));
		lspecs.set_position(wheels[3]);
		pa->append_linear_joint(lspecs, NodePath::any_path(prr));

		BT3SoftBodyAJointSpecs aspecs;
		aspecs.set_cfm(1);
		aspecs.set_erp(1);
		aspecs.set_axis(LVector3f(-1, 0, 0));

		aspecs.set_icontrol(&steercontrol_f);
		pa->append_angular_joint(aspecs, NodePath::any_path(pfl));
		pa->append_angular_joint(aspecs, NodePath::any_path(pfr));

		aspecs.set_icontrol(&motorcontrol);
		pa->append_angular_joint(aspecs, NodePath::any_path(prl));
		pa->append_angular_joint(aspecs, NodePath::any_path(prr));

		pa->rotate(orientation);
		pfl->rotate(orientation);
		pfr->rotate(orientation);
		prl->rotate(orientation);
		prr->rotate(orientation);
		pa->translate(origin);
		pfl->translate(origin);
		pfr->translate(origin);
		prl->translate(origin);
		prr->translate(origin);
		pfl->get_config().set_positions_solver_iterations(1);
		pfr->get_config().set_positions_solver_iterations(1);
		prl->get_config().set_positions_solver_iterations(1);
		prr->get_config().set_positions_solver_iterations(1);
		pfl->set_cluster_matching(0, 0.05);
		pfr->set_cluster_matching(0, 0.05);
		prl->set_cluster_matching(0, 0.05);
		prr->set_cluster_matching(0, 0.05);
		pfl->set_cluster_node_damping(0, 0.05);
		pfr->set_cluster_node_damping(0, 0.05);
		prl->set_cluster_node_damping(0, 0.05);
		prr->set_cluster_node_damping(0, 0.05);

		Ctor_LinearStair(LPoint3f(0, 0, -8 + 10), LVecBase3f(3, 40, 2), 20,
				8, "box", "z");
		Ctor_RbUpStack(50, LPoint3f(0.0, 0.0, 20.0));
	}
	else
	{
		destroyObjects();
	}
}

void Init_ClusterRobot(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClusterRobot
		static GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		struct Functor
		{
			static PT(BT3SoftBody) CreateBall(const LPoint3f& pos)
			{
				//1: the graphic object (NodePath)
				PT(GeomNode)ball = new GeomNode("Ball");
				graphicNPs.push_back(NodePath(ball));
				graphicNPs.back().set_pos_hpr_scale(
						LPoint3f(0.0, 0.0, 0.0),//position
						LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
						LVecBase3f(1.0, 1.0, 1.0));//scale
				graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
				//2: create the empty soft body
				softBodyNP = physicsMgr->create_soft_body("BallBody");
				PT(BT3SoftBody) ballBody = DCAST(BT3SoftBody, softBodyNP.node());
				//3: set CONSTRUCTION PARAMETERS
				ballBody->set_group_mask(groupMask);
				ballBody->set_collide_mask(collideMask);
				ballBody->set_body_type(BT3SoftBody::ELLIPSOID);
				ballBody->set_ellipsoid_center(pos);
				ballBody->set_ellipsoid_radius(LVecBase3f(1, 1, 1) * 3);
				ballBody->set_ellipsoid_res(512);
				//4: setup the body with graphicNPs.back() as owner
				ballBody->set_owner_object(graphicNPs.back());
				ballBody->setup();
				//5: set DYNAMIC PARAMETERS
				ballBody->get_material(0).set_linear_stiffness_coefficient(0.45);
				ballBody->get_config().set_volume_conversation_coefficient(20);
				ballBody->set_total_mass(50,true);
				ballBody->set_pose(true, false);
				ballBody->generate_clusters(1);
				//6: visual state
//				softBodyNP.set_two_sided(true);
//				softBodyNP.show_bounds();
//				softBodyNP.set_render_mode_wireframe();
				return ballBody;
			}
		};
		//
		const LPoint3f base(0,8,25+10);
		PT(BT3SoftBody) psb0=Functor::CreateBall(base+LVector3f(8,0,0));
		PT(BT3SoftBody) psb1=Functor::CreateBall(base+LVector3f(-8,0,0));
		PT(BT3SoftBody) psb2=Functor::CreateBall(base+LVector3f(0,+8*sqrt(2),0));
		const LPoint3f ctr=(psb0->get_cluster_com(0)+psb1->get_cluster_com(0)+psb2->get_cluster_com(0))/3.0;

		NodePath prbNP = getModelAnims("prbNP", 1.0, 3,
				&playerAnimCtls);
		PT(BT3RigidBody)prb = DCAST(BT3RigidBody, createRigidBodyXYZ(prbNP,
				"prbZ", "cylinder", "z", ctr + LVector3f(0, 0, 5),
				LVecBase3f(0.0, 0.0, 0.0), LVecBase3f(16, 16, 1), false).node());
		prb->set_mass(50.0);
		prb->switch_body_type(BT3RigidBody::DYNAMIC);
		graphicNPs.push_back(prb->get_owner_object());

		BT3SoftBodyLJointSpecs ls;
		ls.set_erp(0.5);
		ls.set_position(psb0->get_cluster_com(0));
		psb0->append_linear_joint(ls,NodePath::any_path(prb));
		ls.set_position(psb1->get_cluster_com(0));
		psb1->append_linear_joint(ls,NodePath::any_path(prb));
		ls.set_position(psb2->get_cluster_com(0));
		psb2->append_linear_joint(ls,NodePath::any_path(prb));

		NodePath pgrnNP = getModelAnims("pgrnNP", 1.0, 8,
				&playerAnimCtls);
		PT(BT3RigidBody)pgrn = DCAST(BT3RigidBody, createRigidBodyXYZ(pgrnNP,
				"pgrnZ", "box", "z", LPoint3f(0, 0, 10),
				LVecBase3f(0,M_PI/4.0 * (180.0 / M_PI),0), LVecBase3f(40, 80, 2), false).node());
		pgrn->set_mass(0.0);
		pgrn->switch_body_type(BT3RigidBody::STATIC);
		graphicNPs.push_back(pgrn->get_owner_object());
	}
	else
	{
		destroyObjects();
	}
}

void Init_ClusterStackSoft(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClusterStackSoft
		for (int i = 0; i < 10; ++i)
		{
			PT(BT3SoftBody)psb = Ctor_ClusterTorus(LPoint3f(0, 0, -9 + 8.25 * i + 20),
					LVecBase3f(0, 0, 0));
			psb->get_config().set_dynamic_friction_coefficient(1);
		}
	}
	else
	{
		destroyObjects();
	}
}

void Init_ClusterStackMixed(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_ClusterStackSoft
		for (int i = 0; i < 10; ++i)
		{
			if ((i + 1) & 1)
			{
				Ctor_BigPlate(50, -9 + 4.25 * i + 20);
			}
			else
			{
				PT(BT3SoftBody)psb = Ctor_ClusterTorus(LPoint3f(0, 0, -9+4.25*i + 20),
						LVecBase3f(0, 0, 0));
				psb->get_config().set_dynamic_friction_coefficient(1);
			}
		}
	}
	else
	{
		destroyObjects();
	}
}

struct TetraCube
{
#include "../../physics/source/bullet3/examples/SoftDemo/cube.inl"
};
void Init_TetraCube(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_TetraCube
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		PT(GeomNode)tetraCube = new GeomNode("TetraCube");
		graphicNPs.push_back(NodePath(tetraCube));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 0.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body("TetraCubeBody");
		PT(BT3SoftBody) psb = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		psb->set_group_mask(groupMask);
		psb->set_collide_mask(collideMask);
		psb->set_body_type(BT3SoftBody::TETGEN_MESH);
		ValueList_string eleFaceNode{
				string(TetraCube::getElements()),
				string(),
				string(TetraCube::getNodes())
		};
		psb->set_tetgen_ele_face_node(eleFaceNode, false);
		psb->set_tetgen_facelinks(false);
		psb->set_tetgen_tetralinks(true);
		psb->set_tetgen_facesfromtetras(true);
		//4: setup the body with graphicNPs.back() as owner
		psb->set_owner_object(graphicNPs.back());
		psb->setup();
		//5: set DYNAMIC PARAMETERS
		psb->scale(LVecBase3f(4,4,4));
		psb->translate(LPoint3f(0,0,5 + 10));
		psb->set_volume_mass(300);
		psb->get_config().set_positions_solver_iterations(1);
		psb->generate_clusters(16);
		psb->get_collision_object_attrs()->set_shape_margin(0.01);
		psb->get_config().set_collisions_flags(BT3SoftBodyConfig::Cluster_Soft_Soft |
				BT3SoftBodyConfig::Cluster_Rigid_Soft);
		psb->get_material(0).set_linear_stiffness_coefficient(0.8);
		//6: visual state
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();
	}
	else
	{
		destroyObjects();
	}
}

struct TetraBunny
{
#include "../../physics/source/bullet3/examples/SoftDemo/bunny.inl"
};
void Init_TetraBunny(bool init)
{
	if (init)
	{
		///bullet3::examples::SoftDemo::Init_TetraBunny
		GamePhysicsManager* physicsMgr = GamePhysicsManager::get_global_ptr();
		//1: the graphic object (NodePath)
		PT(GeomNode)tetraBunny = new GeomNode("TetraBunny");
		graphicNPs.push_back(NodePath(tetraBunny));
		graphicNPs.back().set_pos_hpr_scale(
				LPoint3f(0.0, 0.0, 0.0),//position
				LVecBase3f(0.0, 0.0, 0.0),//rotation (h,p,r)
				LVecBase3f(1.0, 1.0, 1.0));//scale
		graphicNPs.back().set_color(LVecBase4f(Vector3Rand(), 1.0));
		//2: create the empty soft body
		softBodyNP = physicsMgr->create_soft_body("TetraBunnyBody");
		PT(BT3SoftBody) psb = DCAST(BT3SoftBody, softBodyNP.node());
		//3: set CONSTRUCTION PARAMETERS
		psb->set_group_mask(groupMask);
		psb->set_collide_mask(collideMask);
		psb->set_body_type(BT3SoftBody::TETGEN_MESH);
		ValueList_string eleFaceNode{
				string(TetraBunny::getElements()),
				string(),
				string(TetraBunny::getNodes())
		};
		psb->set_tetgen_ele_face_node(eleFaceNode, false);
		psb->set_tetgen_facelinks(false);
		psb->set_tetgen_tetralinks(true);
		psb->set_tetgen_facesfromtetras(true);
		//4: setup the body with graphicNPs.back() as owner
		psb->set_owner_object(graphicNPs.back());
		psb->setup();
		//5: set DYNAMIC PARAMETERS
		LQuaternionf quat;
		quat.set_hpr(LVecBase3f(M_PI/2.0 * (180.0 / M_PI),0,0));
		psb->rotate(quat);
		psb->translate(LPoint3f(0,0,20));
		psb->set_volume_mass(150);
		psb->get_config().set_positions_solver_iterations(2);
		psb->get_config().set_collisions_flags(BT3SoftBodyConfig::Cluster_Soft_Soft |
				BT3SoftBodyConfig::Cluster_Rigid_Soft);
		///pass zero in generateClusters to create  cluster for each tetrahedron or triangle
		psb->generate_clusters(0);
		psb->get_config().set_dynamic_friction_coefficient(10);
		//6: visual state
//		softBodyNP.set_two_sided(true);
//		softBodyNP.show_bounds();
//		softBodyNP.set_render_mode_wireframe();
	}
	else
	{
		destroyObjects();
	}
}

