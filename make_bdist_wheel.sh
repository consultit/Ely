#!/bin/bash

workDir="$(pwd)" && echo ${workDir}
pythonSetup="$(which python) setup.py" && echo ${pythonSetup}
sdistTempDir="$(${pythonSetup} --fullname)/" && echo ${sdistTempDir}
pkgName="$(${pythonSetup} --name)" && echo ${pkgName}

# check if we are in Ely dir
if ! [ -f "ely_setup.py" ] ; then
	echo "humm ... it looks like we're not in the 'Ely' directory..." 
	exit -1 
fi

# cleanup
rm ${pkgName}/*.so
rm -r dist build ${sdistTempDir} ${pkgName}.*/ ${pkgName}-*/ *.tar.gz *.whl 

# create source dist
${pythonSetup} sdist --keep-temp
mv dist/*.tar.gz ${workDir}

# create wheel dist
cd ${sdistTempDir}
${pythonSetup} bdist_wheel
mv dist/*.whl ${workDir}
