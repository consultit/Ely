#!/usr/bin/env python

'''
Created on Jan 6, 2018

@author: consultit
'''

import os
from subprocess import run, PIPE, STDOUT  # , call 
import argparse, textwrap
import sys

# ## NOTE: currently this script works only on GNU/Linux
currdir = os.getcwd()
setupdir = os.path.dirname(os.path.realpath(__file__))
builddir = os.path.join(setupdir, 'ely')
elydir = 'ely'
pythoncmd = sys.executable
if not pythoncmd:
    pythoncmd = 'python'
lpref = ''
mpref = ''
if sys.platform.startswith('linux'):
    PLATFORM = 'LINUX'
    libsuff = '.so'
elif sys.platform.startswith('win'):
    PLATFORM = 'WIN'
    libsuff = '.dll'
# ##

def installSrcModule(modSrcDir, instDir):

    def printRet(retObj):
        if retObj.returncode != 0:
            if retObj.stdout: print(retObj.stdout, sep='')
            if retObj.stderr: print(retObj.stderr, sep='')

    if PLATFORM == 'LINUX':
        ret = run(['/bin/mkdir', '-p', instDir], stdout=PIPE, stderr=PIPE)
        printRet(ret)
        ret = run(['/bin/cp', '-r', modSrcDir, instDir], stdout=PIPE, stderr=PIPE)
        printRet(ret)
    elif PLATFORM == 'WIN':  # fixme
        run(['@echo', 'off'], stdout=PIPE, stderr=PIPE)
        run(['setlocal', 'enableextensions'], stdout=PIPE, stderr=PIPE)
        ret = run(['md', instDir], stdout=PIPE, stderr=PIPE)
        run(['endlocal'], stdout=PIPE, stderr=PIPE)
        printRet(ret)
        ret = run(['xcopy', modSrcDir, instDir, '/s', '/e'], stdout=PIPE, stderr=PIPE)
        printRet(ret)

tools = 'libtools'

def getModuleDirs(path, tools, check):
    '''Gets all modules'''
    
    modules = []
    for d in os.listdir(path):
        if (os.path.isdir(d) and 
            os.path.isfile(os.path.join(d, check))
            and (not d in tools)):
            modules.append(d)
            
    return sorted(modules, key=str.lower)
    
if __name__ == '__main__':
    # cwd
    os.chdir(builddir)
    
    modulesList = '\n - ' + tools + '\n - '
    modulesList += '\n - '.join(getModuleDirs(builddir, tools, 'config.ini'))
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.indent(
                    'This script will build and install the \'Ely\' modules:\n' + 
                                            modulesList, '\t', lambda line: not '\'Ely\'' in line))
    # set up arguments
    parser.add_argument('-m', '--module', type=str, action='append',
                        help='the name of the module to build or the word \'all\' for all modules')
    parser.add_argument('-n', '--no-libtools', action='store_true',
                        help='when specified \'libtools\' is not built')
    parser.add_argument('-i', '--install-dir', type=str,
                        help='the module(s) install directory; relative paths are wrt Ely/ely directory')
    # parse arguments
    argsParse = parser.parse_args()

    # go on
    print('changed build dir', builddir, sep=': ')
    
    # do actions
    installdir = os.path.join('.')
    if argsParse.install_dir:
        installdir = os.path.join(argsParse.install_dir, 'ely')
        if PLATFORM == 'LINUX':
            run(['/bin/mkdir', '-p', installdir])
            run(['/bin/cp', '__init__.py', os.path.join(installdir, '..', '__init__.py')])
            run(['/bin/cp', '__init__.py', os.path.join(installdir, '__init__.py')])
        elif PLATFORM == 'WIN':  # fixme
            run(['@echo', 'off'], stdout=PIPE, stderr=PIPE)
            run(['setlocal', 'enableextensions'], stdout=PIPE, stderr=PIPE)
            run(['md', installdir], stdout=PIPE, stderr=PIPE)
            run(['endlocal'], stdout=PIPE, stderr=PIPE)
            run(['copy', '__init__.py', os.path.join(installdir, '..', '__init__.py')])
            run(['copy', '__init__.py', os.path.join(installdir, '__init__.py')])
    
    # set modules
    modules = []
    if argsParse.module:
        for m in argsParse.module:
            modules.append(m)
    if 'all' in modules:
        # get all modules
        modules = getModuleDirs(builddir, tools, 'config.ini')
        
    # build libtools
    libtools = lpref + tools + libsuff 
    libtoolssrc = tools
    toolssrcdir = os.path.join('..', elydir, tools)
    if not argsParse.no_libtools:
        print('building "', libtoolssrc, '" module ...', sep='')
        args = ['build.py', '--dir', toolssrcdir, '--clean']
        # call([pythoncmd] + args)
        retVal = run([pythoncmd] + args)
        if retVal.returncode == 0:
            # Set rpath so libs can be found in the same directory as the deployed game
            if PLATFORM == 'LINUX':
                run(['/usr/bin/patchelf', '--set-rpath', '$ORIGIN', libtools])
            elif PLATFORM == 'WIN':  # fixme
                pass
            if installdir != os.path.join('.'):
                print('installing "', libtoolssrc, '" module ...', sep='')
                try:
                    if PLATFORM == 'LINUX':
                        # call(['/usr/bin/install', '-D', '-t', installdir, libtools])
                        ret = run(['/usr/bin/install', '-D', '-t', installdir, libtools],
                                  stdout=PIPE, stderr=STDOUT)
                    elif PLATFORM == 'WIN':  # fixme
                        run(['@echo', 'off'], stdout=PIPE, stderr=PIPE)
                        run(['setlocal', 'enableextensions'], stdout=PIPE, stderr=PIPE)
                        run(['md', installdir], stdout=PIPE, stderr=PIPE)
                        run(['endlocal'], stdout=PIPE, stderr=PIPE)
                        ret = run(['copy', '/B', libtools, installdir], stdout=PIPE, stderr=STDOUT)
                except:
                    print('Cannot install ', libtoolssrc, ' module into ', installdir, sep='')
                if ret.returncode != 0:
                    # try as source folder
                    installSrcModule(libtoolssrc, installdir)
        else:
            print('Error building ', libtoolssrc, sep='')
    # build modules
    # libtoolsmodule = os.path.join(installdir, libtools)
    libtoolsmodule = os.path.join(libtools)
    for module in modules:
        if module == libtoolssrc:
            continue
        modulelib = mpref + module + libsuff
        modulesrc = module
        print('building "', modulesrc, '" module ...', sep='')
        moduledir = os.path.join('..', elydir, module)
        args = ['build.py', '--dir', moduledir, '--libs', libtoolsmodule, '--libs_src',
                toolssrcdir, '--clean']
        # call([pythoncmd] + args)
        retVal = run([pythoncmd] + args)
        # cp modules into outpudir if any
        if retVal.returncode == 0:
            # Set rpath so libs can be found in the same directory as the deployed game
            if os.path.exists(modulelib):
                if PLATFORM == 'LINUX':
                    run(['/usr/bin/patchelf', '--set-rpath', '$ORIGIN', modulelib])
                elif PLATFORM == 'WIN':  # fixme
                    pass
            if installdir != os.path.join('.'):
                # we are in builddir
                print('installing "', modulesrc, '" module ...', sep='')
                try:
                    if PLATFORM == 'LINUX':
                        # call(['/usr/bin/install'] + ['-D', '-t', installdir, modulelib])
                        ret = run(['/usr/bin/install'] + ['-D', '-t', installdir, modulelib],
                                  stdout=PIPE, stderr=STDOUT)
                    elif PLATFORM == 'WIN':  # fixme
                        run(['@echo', 'off'], stdout=PIPE, stderr=PIPE)
                        run(['setlocal', 'enableextensions'], stdout=PIPE, stderr=PIPE)
                        run(['md', installdir], stdout=PIPE, stderr=PIPE)
                        run(['endlocal'], stdout=PIPE, stderr=PIPE)
                        ret = run(['copy', '/B', modulelib, installdir], stdout=PIPE, stderr=STDOUT)
                except:
                    print('Cannot install ', modulesrc, ' module into ', installdir, sep='')
                if ret.returncode != 0:
                    # try as source folder
                    installSrcModule(modulesrc, installdir)
        else:
            print('Error building ', modulesrc, sep='')
