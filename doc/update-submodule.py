#!/usr/bin/env python3

import argparse
import textwrap
import os
import pathlib
import sys
import configparser

currDir = os.getcwd()

def updateSubModule(subMod, subModules):
    cmd = 'git submodule update --recursive --remote ' + '"' + subModules[subMod] + '"'
    #cmd = 'git submodule update --init --recursive ' + '"' + subModules[subMod] + '"'
    print('{:30} {}'.format('updating '+ subMod, '-> ' + cmd + ' ...'))
    os.system(cmd)

if __name__ == '__main__':
    
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    This script will perform submodule(s) update.
    Specify 'all' to update all modules.
    '''))
    # set up arguments
    parser.add_argument('gitDir', type=str, nargs='?', default='.',
                        help='the git directory, wrt current working directory when relative' + 
                        '(default=\'.\')')
    parser.add_argument('-s', '--submodule', type=str, nargs='?', action='append',
                        help='the submodule(s) to update')
    parser.add_argument('-l', '--list', action='store_true', help='list all available submodules')
    # parse arguments
    args = parser.parse_args()
    
    # do actions
    workDir = None
    gitDir = args.gitDir
    isAbs = os.path.isabs(gitDir)
    if isAbs:
        workDir = gitDir
    else:
        workDir = os.path.join(currDir, gitDir)
    os.chdir(workDir)
    gitmodules = pathlib.Path('.gitmodules')
    if not gitmodules.is_file():
        print('\'' + workDir + '\' doesn\'t seem to contain submodules.')
        sys.exit(-1)
    # parse gitmodules
    subModules = {}
    configIni = configparser.ConfigParser()
    configIni.read(gitmodules)
    for section in configIni.sections():
        pathStr = configIni[section]['path']
        
#         _, pathStr = section.split(' ')
#         pathStr = pathStr.strip('"')
        subModules[os.path.basename(pathStr)] = pathStr
    # list modules if requested
    subModList = sorted(subModules.keys(), key=str.casefold)
    if args.list:
        print('Available submodules:')
        for subMod in subModList:
            print('\t-', subMod)
    # update submodules
    subModUpdateList = args.submodule
    if not subModUpdateList:
        sys.exit(0)
    isAll = 'all' in subModUpdateList
    if isAll:
        for subMod in subModList:
            updateSubModule(subMod, subModules)
    else:
        for subMod in subModUpdateList:
            if subMod in subModList:
                updateSubModule(subMod, subModules)
            else:
                print(subMod, 'is not a submodule.')
    #
    print('\n... Submodule(s) update complete.')
