/**
 * \file typedClass.h
 *
 * \date 2018-08-12
 * \author consultit
 */

#ifndef TYPEDCLASS_H
#define TYPEDCLASS_H

#include "pandabase.h"
#include "typedReferenceCount.h"

/**
 * TypedClass class.
 */
class EXPORT_CLASS TypedClass: public TypedReferenceCount
{
PUBLISHED:
	INLINE TypedClass();
	INLINE virtual ~TypedClass();

	INLINE virtual void set_value(int n);
	INLINE int get_value();

	virtual void set_value_alt(int n);
	int get_value_alt();

private:
	int _value;

public:
	static TypeHandle get_class_type()
	{
		return _type_handle;
	}
	static void init_type()
	{
		TypedObject::init_type();
		register_type(_type_handle, "TypedClass", TypedObject::get_class_type());
	}
	virtual TypeHandle get_type() const
	{
		return get_class_type();
	}
	virtual TypeHandle force_init_type()
	{
		init_type();
		return get_class_type();
	}

private:

	static TypeHandle _type_handle;

};

#include "typedClass.I"

#endif
