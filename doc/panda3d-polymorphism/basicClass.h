/**
 * \file basicClass.h
 *
 * \date 2018-08-12
 * \author consultit
 */

#ifndef BASICCLASS_H
#define BASICCLASS_H

#include "pandabase.h"
#include "typedClass.h"
#include "pointerTo.h"

/**
 * BasicClass class.
 */
class EXPORT_CLASS BasicClass
{
PUBLISHED:
	INLINE BasicClass();
	INLINE virtual ~BasicClass();

	INLINE virtual void set_value(int n);
	INLINE int get_value();

	virtual void set_value_alt(int n);
	int get_value_alt();

	// argument substitution
	void set_typed_class(TypedClass& cl);//1 attempt
	void set_typed_class(TypedClass* cl);//2 attempt
	void set_typed_class(const TypedClass& cl);//3 attempt
	void set_typed_class(PT(TypedClass) cl);//4 attempt

private:
	int _value;
};

#include "basicClass.I"

#endif
