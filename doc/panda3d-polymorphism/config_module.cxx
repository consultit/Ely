#include "config_module.h"
#include "dconfig.h"

#include "typedClass.h"

Configure(config_polymorphism);
NotifyCategoryDef(polymorphism, "");

ConfigureFn( config_polymorphism )
{
	init_libpolymorphism();
}

void init_libpolymorphism()
{
	static bool initialized = false;
	if (initialized)
	{
		return;
	}
	initialized = true;

	// Init your dynamic types here, e.g.:
	TypedClass::init_type();

	return;
}

