#!/bin/bash

#get (ordered) input
CMD=$1
BUILDMODE=$2
THREADS=$3
WORKDIR=$4
MYWORKSPACE=$5
#some colors
RED='\033[0;31m'
LRED='\033[1;31m'
GREEN='\033[0;32m'
LGREEN='\033[1;32m'
BLUE='\033[0;34m'
LBLUE='\033[1;34m'
NC='\033[0m' # No Color

#static and default values
PYTHONCMD=/usr/bin/python3
PYEXT=".py3"
CMDS="install update uninstall clean distclean"
BUILDMODES="release debug"
DISTPKGS="autoconf automake build-essential checkinstall freeglut3-dev gdebi git \
 gnulib libboost-python-dev libeigen3-dev libfreetype6-dev libgl1-mesa-dev libglu1-mesa-dev \
 libode-dev libopenal-dev libopencv-dev libsdl1.2-dev libssl-dev libswscale-dev libtool \
 libvorbis-dev libx11-dev libxxf86dga-dev libxxf86vm-dev nvidia-cg-dev pkg-config python-dev"
CONFARGS=
PANDA3D="panda3d"
PANDA3DPKG=${PANDA3D}"1.11"
PANDA3DOUTPUTDIR="built.release""${PYEXT}"
#PANDAEIGEN="--no-eigen"
PANDAEIGEN=
PANDA3DALLEXT="-all"
PANDA3DINSTCMD="sudo gdebi --n "

#distclean
distcleanElyDeps () {
	echo "distclean Panda3d"
	sudo apt-get --purge --yes remove ${PANDA3DPKG} 
	rm -rfv ${WORKDIR}/panda3d/${PANDA3DOUTPUTDIR}
	#
	cd ${WORKDIR}
}

#clean
cleanElyDeps () {
	echo "clean Panda3d"
	sudo apt-get --purge --yes remove ${PANDA3DPKG} 
	#
	cd ${WORKDIR}
}

#install sources
installElyDeps () {
	#install distribution packages
	sudo apt-get install --yes ${DISTPKGS}
	#install sources
	mkdir -p ${WORKDIR}/${MYWORKSPACE}
	#
	cd ${WORKDIR}
	echo "install Panda3d source"
	WD=${WORKDIR}/panda3d
	! ( [ -d ${WD} ] || [ -h ${WD} ] ) && git clone https://github.com/panda3d/panda3d.git
}

#uninstall
uninstallElyDeps () {
	cleanElyDeps
	#remove all working directories
	echo "distclean Panda3d"
	rm -rfv ${WORKDIR}/panda3d
	#uninstall distribution packages
	sudo apt-get --purge --yes remove ${DISTPKGS}
	sudo apt-get --purge --yes autoremove
	#
	cd ${WORKDIR}
}

#update
updateElyDeps () {	
	echo "update Panda3d ${BUILDMODE}"
	OPTIMIZE=3
	[ "${BUILDMODE}" = "debug" ] && OPTIMIZE=1
	WD=${WORKDIR}/panda3d
	### setup panda command ###
	PANDACMD="${PYTHONCMD}"" makepanda/makepanda.py --verbose --everything --no-fftw --no-gles --no-gles2 \
			${PANDAEIGEN} --outputdir ${PANDA3DOUTPUTDIR} --threads ${THREADS} --optimize ${OPTIMIZE} \
			--installer --wheel"
	printf "${LBLUE}${PANDACMD}${NC}\n"
	### --- ###
	[ -d "${WD}" ] && cd ${WD} || \
		git pull && \
		eval ${PANDACMD} && \
		DEBFILE=$(find . -iname "${PANDA3DPKG}_*_amd64.deb") && \
		filename=$(basename -- "${DEBFILE}") && \
		extension="${filename##*.}" && \
		filename="${filename%.*}" && \
		PKGDEB="${filename}-$(date +'%Y%m%d')-${BUILDMODE}${PYEXT}${PANDA3DALLEXT}.deb" && \
		mv ${DEBFILE} ${PKGDEB} && \
	 	${PANDA3DINSTCMD} ${PKGDEB} && \
	 	WHLFILE=$(find . -iname "${PANDA3D}*x86_64.whl") && \
		filename=$(basename -- "${WHLFILE}") && \
		extension="${filename##*.}" && \
		filename="${filename%.*}" && \
		PKGWHL="${filename}-$(date +'%Y%m%d')-${BUILDMODE}${PYEXT}${PANDA3DALLEXT}.whl" && \
		mv ${WHLFILE} ${PKGWHL} && \
		echo "Note: to install the wheel package, you should rename it as \"${filename}.${extension}\"."
	#
	cd ${WORKDIR}
}

#print usage
printUsage () {
	echo "Usage: $0 install|uninstall|update|clean|distclean [build-mode=release|debug] [threads=half cores] [work-dir=$(pwd)] [my-workspace=WORKSPACE]"
}

#MAIN
#checks
#trim spaces from CMD and checks if it's in CMDS else exit
CMD="$(echo -e "${CMD}" | tr -d '[[:space:]]')"
[ "${CMDS/$CMD}" = "${CMDS}" ] && printUsage && exit 1
#trim spaces from BUILDMODE and checks if it's in BUILDMODES else =release
BUILDMODE="$(echo -e "${BUILDMODE}" | tr -d '[[:space:]]')"
[ "${BUILDMODES/$BUILDMODE}" = "${BUILDMODES}" ] && BUILDMODE=release
#checks if THREADS is integer else =half than num of cores (>=1)
integer='^[0-9]+$'
T=$(($(cat /proc/cpuinfo | grep -i processor | wc -l) / 2))
if [ -z "$THREADS" ] 
then
	THREADS=${T}
else
	! [[ ${THREADS} =~ ${integer} ]] && THREADS=${T}
fi
[ "$((${THREADS} >= 1))" = "0" ] && THREADS=1
#checks if WORKDIR exists else =PWD
! [ -d "${WORKDIR}" ] && WORKDIR=$(pwd)
#checks if MYWORKSPACE specified and WORKDIR}/MYWORKSPACE exists else =WORKSPACE
if [ -z ${MYWORKSPACE} ] 
then
	MYWORKSPACE=WORKSPACE 
else
	! [ -d "${WORKDIR}/${MYWORKSPACE}" ] && MYWORKSPACE=WORKSPACE
fi

#check if debug build mode
[ "${BUILDMODE}" = "debug" ]  && CONFARGS="--enable-debug" \
							  && PANDA3DOUTPUTDIR="built.debug""${PYEXT}"

#start
case ${CMD} in
	install)
		installElyDeps
		updateElyDeps
	;;
	uninstall)
		uninstallElyDeps
	;;
	update)
		updateElyDeps
	;;
	clean)
		cleanElyDeps
	;;
	distclean)
		distcleanElyDeps
	;;
	*)
		printUsage
	;;
esac

exit 0

